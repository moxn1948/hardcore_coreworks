<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${ pageContext.servletContext.contextPath }" scope="application"/>
<c:if test="${ empty loginUser }">
   <jsp:forward page="WEB-INF/views/common/login.jsp"/>
</c:if>
<c:if test="${ !empty loginUser }">
	<c:if test="${ firstYn.FIRST_YN.equals('Y') and firstYn.STEP >= 8}">
   <script>
      var eno = '${ sessionScope.loginUser.empDivNo }';
      var auth = '${ sessionScope.loginUser.auth }';
      var dno = '${ sessionScope.loginUser.deptNo }';
      location.href = 'main.main?eno=' + eno + '&auth=' + auth + '&dno=' + dno;
   </script>
   </c:if>
   <c:if test="${ firstYn.FIRST_YN.equals('N') and firstYn.STEP != 8}">
			
   		<c:if test="${firstYn.STEP == 0}">
   			<script>
   			alert('초기설정이 필요합니다. 초기설정화면으로 이동합니다.');
   			location.href = 'selectFirstInfo.fr';
   			</script>
   		</c:if>
   		<c:if test="${firstYn.STEP == 1}">
   			<script>
   			alert('초기설정이 완료되지 않아 마지막 작업 시점으로 돌아갑니다.');
   			location.href = 'selectDeptStruct.fr';
   			</script>
   		</c:if>
   		<c:if test="${firstYn.STEP == 2}"> 
   		<script>
   		alert('초기설정이 완료되지 않아 마지막 작업 시점으로 돌아갑니다.');
   			location.href = 'selectListPos.fr';
   			</script>
   		</c:if>
   		<c:if test="${firstYn.STEP == 3}">
   		<script>
   		alert('초기설정이 완료되지 않아 마지막 작업 시점으로 돌아갑니다.');
   			location.href = 'selectListPos.fr';
   		</script>
   		</c:if>
   		<c:if test="${firstYn.STEP == 4}">
   		<script>
   		alert('초기설정이 완료되지 않아 마지막 작업 시점으로 돌아갑니다.');
   			location.href = 'selectEmpList.fr';
   		</script>
   		</c:if>
   		<c:if test="${firstYn.STEP == 5}">
   		<script>
   		alert('초기설정이 완료되지 않아 마지막 작업 시점으로 돌아갑니다.');
   			location.href = 'selectOperAuth.fr';
   		</script>
   		</c:if>
   		<c:if test="${firstYn.STEP == 6}">
   		<script>
   		alert('초기설정이 완료되지 않아 마지막 작업 시점으로 돌아갑니다.');
   			location.href = 'selectListformStruct.fr';
   		</script>
   		</c:if>
   		<c:if test="${firstYn.STEP == 7}">
   		<script>
   		alert('초기설정이 완료되지 않아 마지막 작업 시점으로 돌아갑니다.');
   			location.href = 'selectFormList.fr';
   		</script>
   		</c:if>
   </c:if>
</c:if>