<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/doc_menu.jsp" />
		
		<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">	
		<style>
		.hashtag_yn{color: red;}
		#container .pager_wrap .pager_com.on .buttonnot{color: #fff;}
		#container .pager_wrap .pager_com .buttonnot:hover {color: #fff;background-color: #44455B;transition: 0.25s;}
#container .pager_wrap .pager_com .buttonnot {padding: 0;text-align: center;color: #44455B;transition: 0.25s;width: 100%;height: auto;line-height: 37px; border-radius: 0;border: none;transition: 0.25s;}
		</style>
			 <div id="scroll_area" class="inner_rt">
              <!-- 메인 컨텐츠 영역 시작! -->
                  <div class="tbl_top_wrap">
                      <div class="menu_tit"><h2 id="docMenuTit"></h2></div>
                      <!-- 테이블 위 컨텐츠 시작 -->
	                        <select name="srchType" id="docSrchType">
	                        <c:choose>
				        		<c:when test="${ param.srchType == '' || param.srchType == null }">
		                            <option value="tit">제목</option>
		                            <option value="docNo">문서번호</option>
		                            <option value="empNo">기안자</option>
				        		</c:when>
				        		<c:otherwise>
				        			<c:if test="${ param.srchType.equals('tit') }">
			                            <option value="tit" selected>제목</option>
			                            <option value="docNo">문서번호</option>
			                            <option value="empNo">기안자</option>
				        			</c:if>
				        			<c:if test="${ param.srchType.equals('docNo') }">
			                            <option value="tit">제목</option>
			                            <option value="docNo" selected>문서번호</option>
			                            <option value="empNo">기안자</option>
				        			</c:if>
				        			<c:if test="${ param.srchType.equals('empNo') }">
			                            <option value="tit" selected>제목</option>
			                            <option value="docNo">문서번호</option>
			                            <option value="empNo" selected>기안자</option>
				        			</c:if>
				        		</c:otherwise>
				        	</c:choose>
	                        </select>
	                        <c:choose>
				        		<c:when test="${ param.keyword == '' || param.keyword == null }">
	                      		  <input type="search" name="keyword" id="docKeyword">
				        		</c:when>
				        		<c:otherwise>
	                      		  <input type="search" name="keyword" id="docKeyword" value="${ param.keyword }">
				        		</c:otherwise>
				        	</c:choose>
	                        <button class="btn_solid_main" onclick="docSrch()">검색</button>
                      <!-- 테이블 위 컨텐츠 끝 -->
                  </div>
                  <!-- 전자결재 테이블 시작 -->
                  <div class="tbl_common eas_wrap">
                      <div class="tbl_wrap">
                          <table class="tbl_ctn">
                          	  <colgroup>
                          	  	<col style="width: 10%">
                          	  	<col style="width: 14%">
                          	  	<col style="width: *%">
                          	  	<col style="width: 14%">
                          	  	<col style="width: 8%">
                          	  	<col style="width: 8%">
                          	  	<col style="width: 8%">
                          	  </colgroup>
                              <tr>
                                  <th>상태</th>
                                  <th>문서번호</th>
                                  <th>제목</th>
                                  <th>기안일</th>
                                  <th>기안자</th>
                                  <th>수신자</th>
                              </tr>
                              <c:if test="${ pi.getListCount() == 0 }">
                              	<tr>
                              		<td colspan="6">결재 문서가 없습니다.</td>
                              	</tr>
                              </c:if>
                              <c:if test="${ pi.getListCount() != 0 }">
	                              <c:forEach var="list" items="${ list }">
	                              <tr>
	                                  <td class="hashtag_yn">
	                                  <c:if test="${ list.EMERGENCY_YN.equals('Y') }">
	                                  긴급
	                                  </c:if>
	                                  <c:if test="${ list.PROTECT_YN != null }">
	                                  보안
	                                  </c:if>
	                                  <c:if test="${ list.LIMIT_YN.equals('Y') }">
	                                  제한
	                                  </c:if>
	                                  </td>
	                                  <td><c:out value="${ list.DOCNO }" /></td>
	                                  <td>
	                                  
		                                  <c:if test="${ param.menu.equals('dept') and list.PROTECT_YN != null }">
			                                  <p style="cursor: pointer;" onclick="chkLimitYn(${ list.EAS_NO }, 'selectEasDetail.ea?easNo=${ list.EAS_NO }&type=comp&origin=0&proxy=0&menuType=${ param.menu }');"><c:out value="${ list.DOC_TIT }" /></a>
		                                  </c:if>
		                                  <c:if test="${ !param.menu.equals('dept') or (param.menu.equals('dept') and list.PROTECT_YN == null) }">
			                                  <a href="selectEasDetail.ea?easNo=${ list.EAS_NO }&type=comp&origin=0&proxy=0&menuType=${ param.menu }"><c:out value="${ list.DOC_TIT }" /></a>
		                                  </c:if>
	                                  </td>
	                                  <td><c:out value="${ list.SDATE }" /></td>
	                                  <td><a href="#addrPop1" rel="modal:open" class="cnt_name open_modal"><c:out value="${ list.ENAME }" /><input type="hidden" value="${ list.ENO }" class="userNum"></a></td>
	                                  <td><a href="#addrPop1" rel="modal:open" class="cnt_name open_modal"><c:out value="${ list.RECENAME }" /><input type="hidden" value="${ list.RECEIVER_NAME }" class="userNum"></a></td>
	                              </tr>
	                              </c:forEach>
                              </c:if>
                          </table>
                      </div>
                  </div>
                  <!-- 전자결재 테이블 끝 -->
                  <!-- 페이저 시작 -->
                  <c:if test="${ pi.getListCount() != 0 }">
                  <div class="pager_wrap">
                      <ul class="pager_cnt clearfix">
                      <li class="pager_com pager_arr first"><button class="buttonnot" onclick="docPager(${ pi.getStartPage() });">&#x003C;&#x003C;</button></li>
                      <li class="pager_com pager_arr prev" onclick="docPager(${ pi.getCurrentPage() - 1 });"><button class="buttonnot">&#x003C;</button></li>
                      
                      <c:forEach begin="1" end="${ pi.getMaxPage() }" varStatus="status">
                      		<c:if test="${ status.index == pi.getCurrentPage() }">
                       			<li class="pager_com pager_num on"><button class="buttonnot" onclick="docPager(${status.index});"><c:out value="${status.index}" /></button></li>
                      		</c:if>
                      		<c:if test="${ status.index != pi.getCurrentPage() }">
                       			<li class="pager_com pager_num"><button class="buttonnot" onclick="docPager(${status.index});"><c:out value="${status.index}" /></button></li>
                      		</c:if>
                      </c:forEach>
                      <li class="pager_com pager_arr next"><button class="buttonnot" onclick="docPager(${ pi.getCurrentPage() + 1 });">&#x003E;</button></li>
                      <li class="pager_com pager_arr end"><button class="buttonnot" onclick="docPager(${ pi.getEndPage() });">&#x003E;&#x003E;</button></li>
                      </ul>
                  </div>
                  </c:if>
                  <!-- 페이저 끝 -->
              <!-- 메인 컨텐츠 영역 끝! -->
              </div><!-- inner_rt end -->
        </div>
    </main>
</div>


<div id="addrPop1" class="modal">
	<jsp:include page="../pop/addr_pop_1.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	var srchType = '${ param.srchType }';
	var keyword = '${ param.keyword }';
	var endPage = '${ pi.getEndPage() }';
	var empDivNo = '${ loginUser.empDivNo }';
    $(function(){
    	var menuType = '${ param.menu }';
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(2).addClass("on");
		
        if(menuType == "my"){
        	$("#menu_area .menu_list").eq(0).addClass("on");
        	$("#docMenuTit").text("내 결재 완료");
        }else if(menuType == "eas"){
        	$("#menu_area .menu_list").eq(1).addClass("on");
        	$("#docMenuTit").text("참여 결재 완료");
        }else if(menuType == "rece"){
        	$("#menu_area .menu_list").eq(2).addClass("on");
        	$("#docMenuTit").text("수신 결재 완료");
        }else if(menuType == "dept"){
        	$("#menu_area .menu_list").eq(3).addClass("on");
        	$("#docMenuTit").text("부서 결재 완료");
        }
        
    });
    
 	function docPager(cp){
 		if(cp == 0){
 			cp = 1;
 		}
 		if(cp > endPage){
 			cp = endPage;
 		}
 		
 		console.log(cp);
 		var action = 'selectDocList.ea?eno=${ param.eno }&dno=${ param.dno }&menu=${ param.menu }&currentPage=' + cp + '&srchType=' + srchType + '&keyword=' + keyword;
 		location.href = action;
 		console.log(cp);
 		
 	}
 	
 	function docSrch(){
 		srchType = $("#docSrchType").val();
 		keyword = $("#docKeyword").val();
 		var action = 'selectDocList.ea?eno=${ param.eno }&dno=${ param.dno }&menu=${ param.menu }&srchType=' + srchType + '&keyword=' + keyword;
 		
 		location.href = action;
 	}

 	$("#docKeyword").keydown(function(key) {
 		if (key.keyCode == 13) {
 			docSrch();
 		}
 	});
 	
 	function chkLimitYn(val, link){
 		
        $.ajax({
         	url:"chkLimitYn.ea",
         	type:"post",
         	data:{
         		empDivNo:empDivNo,
         		easNo:val
         	},
         	success:function(data){
         		if(data.chkLimitYn > 0){
         			
         			location.href = link;	
         		}else{
         	 		alert("접근 권한이 없는 문서입니다.");
         	 	
         		}
         	},
           	error:function(status){
           		console.log(status)
           	}
        }); 
 	}

 </script>
</body>
</html>