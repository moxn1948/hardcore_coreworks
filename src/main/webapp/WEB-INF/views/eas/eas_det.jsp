<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:if test="${ !param.type.equals('comp') }">
	<jsp:include page="../inc/eas_menu.jsp" />
</c:if>
<c:if test="${ param.type.equals('comp') }">
	<jsp:include page="../inc/doc_menu.jsp" />
</c:if>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj2.css">
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- tree -->
<!-- <link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
 -->
<style>
#phone{width: auto;}
#tab-2 .tbl_common.tbl_basic .tbl_wrap tr td{border: 2px solid #44455B;} 
</style>
<div id="scroll_area" class="inner_rt">
	<!-- 메인 컨텐츠 영역 시작! -->
	<div class="tbl_top_wrap">
		<div class="menu_tit">
			<h2>조회</h2>
		</div>
		<!-- 탭 영역 위 컨텐츠 시작 -->
		<!-- 내결재 - 결재대기 -->
		<c:if test="${ type.equals('myWait') }">
			<a href="javascript:void(0);" class="button btn_pink" onclick="beforeEasBtn('waitCan', 'updateMyWaitCancel.ea?no=${ param.easNo }')">상신취소</a>
			<a href="selectEasDetailForUpdate.ea?easNo=${ param.easNo }&type=myWait&btn=update" class="button btn_main">수정</a>
		</c:if>
		
		<!-- 내결재 - 결재진행 -->
		<c:if test="${ type.equals('myProc') }">
			<a href="javascript:void(0);" class="button btn_pink" onclick="beforeEasBtn('recall', 'updateMyProcRecall.ea?no=${ param.easNo }')">회수</a><!--  최종 결재권자가 결재하기 전까지   -->
		</c:if>
		
		<!-- 내결재 - 결재반려 -->	
		 <c:if test="${ type.equals('myRe') and (detEas.TYPE.equals('RECALL') or detEas.TYPE.equals('CANCEL') )}">
			<a href="javascript:void(0);" class="button btn_pink" onclick="beforeEasBtn('reDel', 'updateMyReDel.ea?no=${ param.easNo }')">삭제</a><!-- 회수된 건일 경우 -->
		</c:if>
		<c:if test="${ type.equals('myRe') and ( detEas.TYPE.equals('RECALL') or detEas.TYPE.equals('RETURN')  or detEas.TYPE.equals('CANCEL') )}">
			<a href="selectEasDetailForUpdate.ea?easNo=${ param.easNo }&type=myRe&btn=reinsert" class="button btn_main" onclick="beforeEasBtn('', '')">재기안</a><!-- 반려되거나, 회수된 건일 경우 -->
		</c:if>
		
		<!-- 참여결재 - 결재대기 -->
		<c:if test="${ type.equals('easWait') }">
			<!-- 결재 -->
			<c:if test="${ detProcHow.PROC_HOW.equals('AS') }">
				<a href="javascript:void(0);" class="button btn_blue" onclick='beforeEasBtn("as", "updateEasWaitAs.ea?no=${ param.easNo }&origin=${ param.origin }&proxy=${ param.proxy }&pathRank=${ detProcHow.PATH_RANK }&value=confirm")'>결재</a>
				<a href="javascript:void(0);" class="button btn_pink" onclick="beforeEasBtn('re', 'updateEasWaitAs.ea?no=${ param.easNo }&origin=${ param.origin }&proxy=${ param.proxy }&pathRank=${ detProcHow.PATH_RANK }&value=return')">반려</a>
			</c:if>
			<c:if test="${ detProcHow.PROC_HOW.equals('AGM') }">
				<a href="javascript:void(0);" class="button btn_blue" onclick="beforeEasBtn('agree', 'updateEasWaitAgm.ea?no=${ param.easNo }&origin=${ param.origin }&proxy=${ param.proxy }&pathRank=${ detProcHow.PATH_RANK }&value=agree')">찬성</a>
				<a href="javascript:void(0);" class="button btn_pink" onclick="beforeEasBtn('oppos', 'updateEasWaitAgm.ea?no=${ param.easNo }&origin=${ param.origin }&proxy=${ param.proxy }&pathRank=${ detProcHow.PATH_RANK }&value=oppos')">반대</a>
			</c:if>
			<c:if test="${ detProcHow.PROC_HOW.equals('CONFIRM') }"><!-- 확인 -->
				<a href="javascript:void(0);" class="button btn_blue" onclick="beforeEasBtn('confirm', 'updateEasWaitConfirm.ea?no=${ param.easNo }&origin=${ param.origin }&proxy=${ param.proxy }&pathRank=${ detProcHow.PATH_RANK }')">확인</a>
			</c:if>
			<c:if test="${ detProcHow.PROC_HOW.equals('REFER') }"><!-- 참조 -->
				<a href="javascript:void(0);" class="button btn_blue" onclick="beforeEasBtn('refer', 'updateEasWaitRefer.ea?no=${ param.easNo }&origin=${ param.origin }&proxy=${ param.proxy }')">확인</a>
			</c:if>
		<!-- 합의 -->
		</c:if>
		
		<!-- 참여결재 - 결재진행 -->
		<c:if test="${ type.equals('easProc') and compEmpDivNo == loginUser.empDivNo}"><!-- 다음 결재권자가 결재하기 전까지 취소가능  -->
			<a href="javascript:void(0);" class="button btn_pink" onclick="beforeEasBtn('easProcCan', 'updateEasProcCan.ea?no=${ param.easNo }&origin=${ param.origin }&proxy=${ param.proxy }&pathRank=${ detProcHow.PATH_RANK }')">결재취소</a>
		</c:if>
		<!-- 공람 - 보낸공람 -->
		<!-- <button class="btn_main">공람추가</button> -->
		
		<!-- 항상 -->
		<!-- 수정이력이 있을 경우 -->
		<!-- <button class="btn_main">이력보기</button> -->
		
		<button class="btn_white" id="pdfBtn">PC저장</button>
		<button class="btn_white" id="printBtn">인쇄</button>
		<!-- 탭 영역 위 컨텐츠 끝 -->	
	</div>
	<!-- 탭 영역 시작 -->
	<div class="main_ctn">
    	<ul class="tabs">
			<li class="tab-link current" data-tab="tab-1">결재정보</li>
			<li class="tab-link" data-tab="tab-2">본문</li>							
		</ul>
		<!-- 결재정보 탭 클릭 시 -->
        <div id="tab-1" class="tab-content current">
		<!-- 테이블 위 컨텐츠 시작 -->
        <table class="eas_information top_eas_information">
        	<tr class="eas_info_tit">
        		<td class="eas_information_title">문서정보</td>
        		<c:if test="${ detEas.EMERGENCY_YN.equals('Y') }">
        			<td><i class="emer_box">긴급</i></td>
        		</c:if>
        	</tr>
        	<tr>
        		<td>제목</td>
        		<td id="pdfTit"><c:out value="${ detDocDet.docTit }" /></td>
        	</tr>
        	<tr>
        		<td>문서번호</td>
        		<td><c:out value="${ detEas.DOCNO }" /></td>
        	</tr>
        	<tr class="eas_last_tr">
        		<td>열람제한기간</td>
        		<c:if test="${ detEas.LIMIT_YN.equals('N') }">
        			<td>영구</td>
        		</c:if>
        		<c:if test="${ detEas.LIMIT_YN.equals('Y') }">
        			<td><c:out value="${ detEas.EASSDATE }" /></td>
        		</c:if>
        	</tr>
        	<tr class="eas_last_tr">
        		<td>보안설정</td>
        		<c:choose>
        		<c:when test="${ detEas.PROTECT_YN == null }">
        			<td>미사용</td>
        		</c:when>
        		<c:otherwise>
	        		<c:if test="${ detEas.PROTECT_YN.equals('RANGE') }">
	        			<td>범위설정 (
	        				<c:if test="${ detEas.MATE_YN != null }">
	        					동급자
	        				</c:if>
	        				<c:if test="${ detEas.DEPT_YN != null }">
	        					동일부서
	        				</c:if>
	        				<c:if test="${ detEas.SENIOR_YN != null }">
	        					상급자
	        				</c:if>
	        				)
	        			</td>
	        		</c:if>
	        		<c:if test="${ detEas.PROTECT_YN.equals('PERSON') }">
	        			<td>특정인 지정
	        			</td>
	        		</c:if>
        		</c:otherwise>
        		</c:choose>
        	</tr>
        </table>
   		<c:if test="${ detEas.PROTECT_YN != null and detEas.PROTECT_YN.equals('PERSON') }">
        <!-- 보안설정 특정인 지정 테이블 시작 -->
        <div class="tbl_common tbl_basic tbl_bottom">
			<div class="tbl_wrap">
				<table class="tbl_ctn">
					<tr class="tbl_main_tit">
						<th>사원명</th>
						<th>직책/직급</th>
						<th>부서</th>
						<th>이메일</th>
					</tr>
					<c:forEach var="list" items="${ detProtList }">
						<tr class="non_doc">
							<td><a href="#addrPop1" rel="modal:open" class="cnt_name open_modal"><c:out value="${ list.ENAME }" /><input type="hidden" value="${ list.ENO }" class="userNum"></a></td>
							<td><c:out value="${ list.PNAME }" />/<c:out value="${ list.JNAME }" /></td>
							<td><c:out value="${ list.DNAME }" /></td>
							<td><c:out value="${ list.EID }" />@<c:out value="${ sessionScope.domain }" /></td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
        <!-- 보안설정 특정인 지정 테이블 끝 -->
        </c:if>
			<table class="eas_line_information">
	        	<tr>
	        		<td class="eas_information_title">결재경로</td>
	        	</tr>
			</table>
			<!-- 결재 경로 -->
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn">
						<tr class="tbl_main_tit">
							<th>순번</th>
							<th>처리방법</th>
							<th>직책/직급</th>
							<th>처리자</th>
							<th>처리 상태</th>
							<th>처리 일시</th>
						</tr>
						<c:forEach var="list" items="${ detPathList }">
							<tr class="non_doc">
								<td><c:out value="${ list.PATH_RANK }" /></td>
								<td>
									<c:if test="${ list.PROC_HOW.equals('AGM') }">
									합의
									</c:if>
									<c:if test="${ list.PROC_HOW.equals('REFER') }">
									참조
									</c:if>
									<c:if test="${ list.PROC_HOW.equals('AS') }">
									결재
									</c:if>
									<c:if test="${ list.PROC_HOW.equals('DRAFT') }">
									기안
									</c:if>
									<c:if test="${ list.PROC_HOW.equals('CONFIRM') }">
									확인
									</c:if>
								</td>
								<td><c:out value="${ list.PNAME }" />/<c:out value="${ list.JNAME }" /></td>
								<td><a href="#addrPop1" rel="modal:open" class="cnt_name open_modal"><c:out value="${ list.ENAME }" /><input type="hidden" value="${ list.ENO }" class="userNum"></a></td>
								<td>
									<c:if test="${ list.EAS_STATUE.equals('WAIT') }">
									대기
									</c:if>
									<c:if test="${ list.EAS_STATUE.equals('PROC') }">
									진행
									</c:if>
									<c:if test="${ list.EAS_STATUE.equals('COMPLETE') }">
									완료
									</c:if>
								</td>
								<td><c:out value="${ list.PROCDATE }" /> <c:out value="${ list.PROC_TIME }" /></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
			<!-- 결재 경로 끝 -->
			
			<!-- 수신자 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">수신자</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl2">
				<div class="tbl_wrap">
					<table class="tbl_ctn eas_sender_tbl">
						 <colgroup>
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
					    </colgroup>
						<tr>
							<td class="tbl_main_tit sender_border">수신자</th>
							<td class="sender_border"><a href="#addrPop1" rel="modal:open" class="cnt_name open_modal"><c:out value="${ detEas.RECE_NAME }" /><input type="hidden" value="${ detEas.RECEIVER_NAME }" class="userNum"></a></td>
							<td class="tbl_main_tit sender_border">발신 명의</td>
							<td><c:out value="${ detEas.SENDER_NAME }" /></td>
						</tr>						
					</table>
										
				</div>				
			</div>
			<!-- 수신자 끝 -->
			
			<!-- 공람자 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">공람자</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl2">
				<div class="tbl_wrap">
					<table class="tbl_ctn">				
						<tr class="tbl_main_tit">
							<th>사원명</th>
							<th>직책/직급</th>
							<th>부서</th>
							<th>이메일</th>
						</tr>
						<c:forEach var="list" items="${ detPubList }">
							<tr class="non_doc">
								<td><a href="#addrPop1" rel="modal:open" class="cnt_name open_modal"><c:out value="${ list.ENAME }" /><input type="hidden" value="${ list.ENO }" class="userNum"></a></td>
								<td><c:out value="${ list.PNAME }" />/<c:out value="${ list.JNAME }" /></td>
								<td><c:out value="${ list.DNAME }" /></td>
								<td><c:out value="${ list.EID }" />@<c:out value="${ sessionScope.domain }" /></td>
							</tr>
						</c:forEach>
					</table>
				</div>				
			</div>
			<!-- 공람자 끝 -->
			
			<!-- 관련문서 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">관련 문서</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn">						
						<tr class="tbl_main_tit">
							<th>문서번호</th>
							<th>제목</th>
							<th>기안자</th>
							<th>기안 부서</th>
							<th>기안 일시</th>
						</tr>
						<c:if test="${ detRelList.size() == 0 }">
						<tr class="non_doc">
							<td class="tit" colspan="5" style="text-align: center;">관련 문서가 없습니다.</td>
						</tr>
						</c:if>
						<c:forEach var="list" items="${ detRelList }">
						<tr class="non_doc">
							<td class="tit"><c:out value="${ list.DOCNO }"/></td>
							<td><a href="#easRelDocpop" class="rel_doc_pop" rel="modal:open"><input type="hidden" value="${ list.REL_DOC_NO }"><c:out value="${ list.DTIT }"/></a></td>
							<td><a href="#addrPop1" rel="modal:open" class="cnt_name open_modal"><c:out value="${ list.ENAME }" /><input type="hidden" value="${ list.ENO }" class="userNum"></a></td>
							<td><c:out value="${ list.DEPT_NAME }"/></td>
							<td><c:out value="${ list.EAS_SDATE }"/></td>
						</tr>
						</c:forEach>
					</table>
				</div>
			</div>
			<!-- 관련 문서 끝 -->
			
			<!-- 첨부파일 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">첨부 파일</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn fileTbl">
						<colgroup>
							<col style="width: 70%;">
							<col style="width: 13%;">
							<col style="width: 7%;">
						</colgroup>
						<tr class="tbl_main_tit">
							<th>파일명</th>
							<th>크기</th>					
						</tr>						
						<c:if test="${ detAttList.size() == 0 }">
						<tr class="non_file">
							<td class="tit"><p id="fileName" name="fileName">첨부된 파일이 없습니다.</p></td>
							<td></td>
						</tr>		
						</c:if>
						<c:forEach var="list" items="${ detAttList }">
						<tr>
							<td><a class="file_link" href="fileDownload.ea?fileNo=${ list.FILE_NO }" ><c:out value="${ list.ORIGIN_NAME }"/></a></td>
							<td><c:out value="${ list.size }"/></td>
						</tr>
						</c:forEach>
					</table>
					
				</div>
			</div>
			<!-- 첨부파일 끝 -->
		</div>
		<!-- 결재정보 탭 끝 -->
		
		<!-- 본문 탭 시작 -->
		<div id="tab-2" class="tab-content pdfContainer">
		<!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap eas_tbl_doc">
	                          <div id="easDocument">
								<h1 class="docu_tit" id="documentTit"><c:out value="${ detEas.FORMAT_NAME }" /></h1>
	                            <table class="tbl_ctn">
		                            <colgroup>
	                                    <col style="width: 10.5%;">
	                                    <col style="width: *;">
	                                    <col style="width: 7%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                </colgroup>
	                                <tr class="eas_tit">
	                                    <td rowspan="2" class="doc_con doc_tit">문서번호</td>
	                                    <td rowspan="2" class="doc_con"><c:out value="${ detEas.DOCNO }" /></td>
	                                    <td rowspan="4" class="doc_con small doc_tit">결재</td>
										<c:forEach var="list" items="${ detPathAsList }">
	                                    	<td class="doc_con gray doc_tit"><c:out value="${ list.PNAME }" /></td>
										</c:forEach>
	                                    <c:forEach var="i" begin="1" end="${ 5 - detPathAsList.size() }">
	                                    	<td class="doc_con gray"></td>
										</c:forEach>
	                                </tr>  
	                               	<tr class="eas_tit eas_tit_center">     
										<c:forEach var="list" items="${ detPathAsList }">
	                                   	 	<td rowspan="3" class="doc_con border_bottom"><c:out value="${ list.ENAME }" /><p><c:out value="${ list.PROCDATE }" /></p></td>
										</c:forEach>
										
	                                    <c:forEach var="i" begin="1" end="${ 5 - detPathAsList.size() }">
	                                    <td rowspan="3" class="doc_con border_bottom"></td>
										</c:forEach>       
	                                </tr>                             
	                                <tr class="eas_tit" >
	                                    <td rowspan="2" class="doc_con doc_tit">작성일자</td>
	                                    <td rowspan="2" class="doc_con">20/01/15</td>                         	                                                                   
	                                </tr>
	                                <tr class="eas_tit"></tr>                                
	                                <tr class="eas_tit">
	                                    <td rowspan="2" class="doc_con doc_tit">작성부서</td>
	                                    <td rowspan="2" class="doc_con"><c:out value="${ detEas.DNAME }" /></td>
	                                    <td rowspan="4" class="doc_con small doc_tit">합의</td> 
	                                           
										<c:forEach var="list" items="${ detPathAgmList }">
	                                   	 <td class="doc_con gray doc_tit"><c:out value="${ list.PNAME }" /></td>
										</c:forEach>
										
	                                    <c:forEach var="i" begin="1" end="${ 5 - detPathAgmList.size() }">
	                                    <td class="doc_con gray"></td>
										</c:forEach>                                   
	                                </tr>
	                                <tr class="eas_tit eas_tit_center">
										<c:forEach var="list" items="${ detPathAgmList }">
	                                   	 <td rowspan="3" class="doc_con border_bottom"><c:out value="${ list.ENAME }" /><p><c:out value="${ list.PROCDATE }" /></p></td>
										</c:forEach>
										
	                                    <c:forEach var="i" begin="1" end="${ 5 - detPathAgmList.size() }">
	                                    <td rowspan="3" class="doc_con border_bottom"></td>
										</c:forEach>                              
	                                </tr>
	                                <tr class="eas_tit">
	                                    <td rowspan="2" class="doc_con doc_tit">작성자</td>
	                                    <td rowspan="2" class="doc_con"><c:out value="${ detEas.ENAME }" /></td>                              
	                                </tr>  
	                                <tr class="eas_tit"></tr>
	                                <tr class="eas_tit">
	                                    <td class="doc_con doc_tit">수신자</td>
	                                    <td colspan="7" class="doc_con"><c:out value="${ detEas.RECE_NAME }" /></td>                                 
	                                </tr>
	                                <tr class="eas_tit">
	                                    <td class="doc_con doc_tit">제목</td>
	                                    <td colspan="7" class="doc_con"><c:out value="${ detDocDet.docTit }" /></td>                                
	                                </tr>
	                                <!-- 기안 본문 작성 부분 -->
	                                <tr class="eas_tit">
	                                    <td rowspan="10" colspan="8" class="eas_content">
	                                    	<c:out value="${ detDocDet.docCnt }"  escapeXml="false" />
	                                    </td>                              
	                                </tr>
	                                <!-- 기안 본문 작성 부분 끝 -->                                                       
	                            </table>
	                           </div>
                        </div>
                    </div>
        <!-- 기본 테이블 끝 -->
		</div>
		<!-- 본문 탭 끝 -->
    </div>
    <!-- 탭 영역 끝 -->
    </div>
	<!-- 메인 컨텐츠 영역 끝! -->
</div>
<!-- inner_rt end -->
</div>
</main>
</div>

<div id="addrPop1" class="modal">
	<jsp:include page="../pop/addr_pop_1.jsp" />
</div>

<!-- popup include -->
<div id="easRelDocpop" class="modal">
	<jsp:include page="../pop/eas_rel_doc_pop.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>

<!-- pdf 변환 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<!-- 인쇄 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.15.0/printThis.min.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리

        var path = '${ contextPath }';
		var type = '${ type }';
		var menuType =  '${ param.menuType }';
 		console.log(type);
 		
 		if(type == "comp"){
 	        $("#nav .nav_list").eq(2).addClass("on");
 	        
            if(menuType == "my"){
            	$("#menu_area .menu_list").eq(0).addClass("on");
            }else if(menuType == "eas"){
            	$("#menu_area .menu_list").eq(1).addClass("on");
            }else if(menuType == "rece"){
            	$("#menu_area .menu_list").eq(2).addClass("on");
            }else if(menuType == "dept"){
            	$("#menu_area .menu_list").eq(3).addClass("on");
            }
 		}else{
 	        $("#nav .nav_list").eq(1).addClass("on");
 	        
 	       if(type == "myWait"){
 	            $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
 	            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(0).addClass("on");
 	            
 	            $("#menu_area .menu_list").eq(0).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(0).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	        }else if(type == "myProc"){
 	            $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
 	            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(1).addClass("on");
 	            
 	            $("#menu_area .menu_list").eq(0).find("a").eq(1).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(1).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	        }else if(type == "myRe"){
 	            $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
 	            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(2).addClass("on");
 	            
 	            $("#menu_area .menu_list").eq(0).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(2).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	        }else if(type == "easWait"){
 	            $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
 	            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");
 	            
 	            $("#menu_area .menu_list").eq(1).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	        }else if(type == "easProc"){
 	            $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
 	            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(1).addClass("on");
 	            
 	            $("#menu_area .menu_list").eq(1).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(1).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	        }else if(type == "easRe"){
 	            $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
 	            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(2).addClass("on");
 	            
 	            $("#menu_area .menu_list").eq(1).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(2).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	        }else if(type == "pubWait"){
 	            $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
 	            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
 	            
 	            $("#menu_area .menu_list").eq(2).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	        }else if(type == "pubComp"){
 	            $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
 	            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
 	            
 	            $("#menu_area .menu_list").eq(2).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	        }else if(type == "pubSend"){
 	            $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
 	            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(2).addClass("on");
 	            
 	            $("#menu_area .menu_list").eq(2).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(2).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
 	        }
 		}
     



    	// tab 전환
    	$('ul.tabs li').click(function(){
    		var tab_id = $(this).attr('data-tab');

    		$('ul.tabs li').removeClass('current');
    		$('.tab-content').removeClass('current');

    		$(this).addClass('current');
    		$("#"+tab_id).addClass('current');
    	});
    	
        (function(){
        	var idx = 0;
        	var fileNode = '<input type="file" id="">';	
        	var fileTr = '<tr><td class="tit"><p></p></td><td><a href="#"></a></td><td></td></tr>';	
        	
        	$(".upFileBtn").on("click",function(){
        		idx++;
        		$(".file_area").eq(0).append(fileNode).css("display", "none").find("input:last-child").prop("id", "upFile"+idx).trigger("click");        		
        		
        		$(document).on("change", "#upFile"+idx, function(){
	        		if(idx == 1){
	        			$(".non_file").eq(0).css({display: "none"});
	        		}
	        		$(".fileTbl").append(fileTr);
	        		$(".fileTbl").find("tr").eq(idx+1).find(".tit p").eq(0).text($(this).val().split('/').pop().split('\\').pop());
        		});
        		
        	});
        	
        })();
        
        $("#checkAll").change(function(){
			if($("#checkAll").is(":checked")){
				console.log("체크박스 체크했음!");
			}else{
				console.log("체크박스 체크 해제!");
			}
		});
    	
        $("#printBtn").on("click", function(){
        	$("ul.tabs li:nth-child(2)").trigger("click");
        	$("#tab-2").printThis();
        	
        });
        

        $("#pdfBtn").on("click", function(){
        	/* $("ul.tabs li:nth-child(2)").trigger("click");
        	$("#tab-2").printThis(); */
        	console.log($("#tab-2").innerHeight());
        	pdfPrint();

        });
    });
    

    function pdfPrint(){
    	$("ul.tabs li:nth-child(2)").trigger("click");
    	$("#container #scroll_area").css({"overflow-x": "visible"});
    	//$(".eas_content").css({height: "800px"});
    	// $("#easDocument").css({width: "1000px"});
    	// var htmlStr = $("#easDocument").html();
    	
        // 현재 document.body의 html을 A4 크기에 맞춰 PDF로 변환
        html2canvas(document.getElementById("tab-2"), {
            onrendered: function (canvas) {
        		// 캔버스를 이미지로 변환
        		var imgData = canvas.toDataURL('image/png');
        		
        		var imgWidth = 210;	// 이미지 가로 길이(mm) A4 기준
        		var pageHeight = imgWidth * 1.414;	// 출력 페이지 세로 길이 계산 A4 기준
        		var imgHeight = canvas.height * imgWidth / canvas.width;
        		var heightLeft = imgHeight;
        		
                var doc = new jsPDF('p', 'mm');
                var position = 0;
                
                // 첫 페이지 출력
                doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                heightLeft -= pageHeight;
                
                // 한 페이지 이상일 경우 루프 돌면서 출력
                while (heightLeft >= 20) {
                	position = heightLeft - imgHeight;
                	doc.addPage();
                	doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
                	heightLeft -= pageHeight;
                }

                // 파일 저장
                doc.save($("#pdfTit").text() + '.pdf');
                
            	$("#container #scroll_area").css({"overflow-x": "auto"});
            	$(".eas_content").css({height: "auto"});
            	//$("#easDocument").css({width: "auto"});
            }
        });

    }
    
    // 관련문서 팝업 시작
	$(".rel_doc_pop").on("click",function(){ 
		var docNo = $(this).find("input").val();

       $.ajax({
        	url:"selectRelDocOne.ea",
        	type:"post",
        	data:{
        		docNo:docNo
        	},
        	async:false,
        	success:function(data){
        		console.log();
        		console.log(data);
        		
        		console.log(getTimeVal(data.detEas.EASPERIOD));
        		console.log(new Date().getTime());
        		if(data.detEas.EASPERIOD == null || getTimeVal(data.detEas.EASPERIOD) > new Date().getTime()){
        			console.log("Dd");
            		$("#relPopDocTit").text(data.detEas.FORMAT_NAME);
            		$("#relPopDocNo").text(data.detEas.DOCNO);
            		$("#relPopDocDate").text(data.detEas.EASSDATE);
            		$("#relPopDeptName").text(data.detEas.DNAME);
            		$("#relPopEname").text(data.detEas.ENAME);
            		$("#relPopReceName").text(data.detEas.RECE_NAME);
            		$("#relPopDocCntTit").text(data.detDocDet.docTit);
            		$("#relPopDocCnt").html(data.detDocDet.docCnt);
            		
            		for (var i = 0; i < data.detPathAsList.length; i++) {
            			$("#relPopAsListTit").append("<td class='doc_con gray doc_tit'>" + data.detPathAsList[i].JNAME + "</td>")
            			
					}
            		
            		for (var i = data.detPathAsList.length; i < 5; i++) {
            			$("#relPopAsListTit").append("<td class='doc_con gray'></td>")
					}
            		
            		for (var i = 0; i < data.detPathAsList.length; i++) {
            			$("#relPopAsList").append('<td rowspan="3" class="doc_con border_bottom">'+ data.detPathAsList[i].ENAME +'<p>' + data.detPathAsList[i].PROCDATE + '</p></td>')
					}
            		
            		for (var i = data.detPathAsList.length; i < 5; i++) {
            			$("#relPopAsList").append('<td rowspan="3" class="doc_con border_bottom"></td>');
					}
            		

            		for (var i = 0; i < data.detPathAgmList.length; i++) {
            			$("#relPopAgmListTit").append("<td class='doc_con gray doc_tit'>" + data.detPathAsList[i].JNAME + "</td>")
            			
					}
            		
            		for (var i = data.detPathAgmList.length; i < 5; i++) {
            			$("#relPopAgmListTit").append("<td class='doc_con gray'></td>")
					}
            		
            		
            		for (var i = 0; i < data.detPathAgmList.length; i++) {
            			$("#relPopAgmList").append('<td rowspan="3" class="doc_con border_bottom">'+ data.detPathAsList[i].ENAME +'<p>' + data.detPathAsList[i].PROCDATE + '</p></td>')
					}

            		for (var i = data.detPathAgmList.length; i < 5; i++) {
            			$("#relPopAgmList").append('<td rowspan="3" class="doc_con border_bottom"></td>');
					}
            		
            		
            		$(".rel_doc_pop").modal();
            		
        		}else{
        			alert("열람제한기간이 만료된 문서입니다.");
        		}
        	},
        	error:function(status){
        		console.log(status)
        	}
        }); 
	})
		
	// 모달 닫기 전 값 초기화
   	$(document).on("click", "#easRelDocpop .close-modal", function(){
   		$("#relPopDocTit").text("");
		$("#relPopDocNo").text("");
		$("#relPopDocDate").text("");
		$("#relPopDeptName").text("");
		$("#relPopEname").text("");
		$("#relPopReceName").text("");
		$("#relPopDocCntTit").text("");
		$("#relPopDocCnt").html("");
		
		$("#relPopAsListTit").find(".doc_con.gray").remove();
		$("#relPopAsList").find(".doc_con.border_bottom").remove();
		$("#relPopAgmListTit").find(".doc_con.gray").remove();
		$("#relPopAgmList").find(".doc_con.border_bottom").remove();
   	});

   	$(document).on("click", ".jquery-modal.blocker.current", function(){
   		if($(this).find("#easRelDocpop").length != 1){

   	   		$("#relPopDocTit").text("");
   			$("#relPopDocNo").text("");
   			$("#relPopDocDate").text("");
   			$("#relPopDeptName").text("");
   			$("#relPopEname").text("");
   			$("#relPopReceName").text("");
   			$("#relPopDocCntTit").text("");
   			$("#relPopDocCnt").html("");
   			
   			$("#relPopAsListTit").find(".doc_con.gray").remove();
   			$("#relPopAsList").find(".doc_con.border_bottom").remove();
   			$("#relPopAgmListTit").find(".doc_con.gray").remove();
   			$("#relPopAgmList").find(".doc_con.border_bottom").remove();	
   		}
   	});
   	
   	$('#easRelDocpop').on($.modal.AFTER_CLOSE, function(event, modal) {
		// console.log("ddd");
/* 
		$("#relPopDocTit").text("");
		$("#relPopDocNo").text("");
		$("#relPopDocDate").text("");
		$("#relPopDeptName").text("");
		$("#relPopEname").text("");
		$("#relPopReceName").text("");
		$("#relPopDocCntTit").text("");
		$("#relPopDocCnt").html("");
		
		$("#relPopAsListTit").find(".doc_con.gray").remove();
		$("#relPopAsList").find(".doc_con.border_bottom").remove();
		$("#relPopAgmListTit").find(".doc_con.gray").remove();
		$("#relPopAgmList").find(".doc_con.border_bottom").remove(); */
	});
    // 관련문서 팝업 끝
    
	function getTimeVal(val){
		
        var startDateArr = ("20"+val).split('/');
                  
        var time = new Date(startDateArr[0], startDateArr[1] - 1, startDateArr[2]).getTime();
        
        return time;
	}

	function beforeEasBtn(type, val){
		var swch = false;
		if(type == "as"){
			swch = confirm("결재하시겠습니까?");
		}else if(type == "re"){
			swch = confirm("반려하시겠습니까?");
		}else if(type == "agree"){
			swch = confirm("찬성하시겠습니까?");
		}else if(type == "oppos"){
			swch = confirm("반대하시겠습니까?");
		}else if(type == "confirm"){
			swch = confirm("확인하시겠습니까?");
		}else if(type == "refer"){
			swch = confirm("참조 확인하시겠습니까?");
		}else if(type == "waitCan"){
			swch = confirm("상신 취소하시겠습니까?");
		}else if(type == "recall"){
			swch = confirm("회수하시겠습니까?");
		}else if(type == "reDel"){
			swch = confirm("삭제하시겠습니까?");
		}else if(type == "easProcCan"){
			swch = confirm("결재 취소하시겠습니까?");
		}
		
		if(swch){
			location.href = val;				
		}else{
			return false;
		}
	}
	/* $('#easRelDocpop').on($.modal.BEFORE_OPEN, function(event, modal) {
		console.log(event);
		console.log(modal);
         $.ajax({
        	url:"",
        	type:"post",
        	data:{
        		docNo:docNo
        	},
        	success:function(data){
        		console.log(data);
        	},
        	error:function(status){
        		console.log(status)
        	}
        }); 
	}); */
   
    
    
  
/*     
    //보안 설정용 스크립트
    $("#security_y").click(function(){
    	$(".eas_information tr:nth-child(3) td").css("padding-bottom", "0");
    	$(".security_div").show();
    });
  	
    $("#security_n").click(function(){
    	$(".security_div").hide();
    	$(".eas_information tr:nth-child(3) td").css("padding-bottom", "20px");
    });
     */
/*     $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        nextText: '다음 달',
        prevText: '이전 달',
        dateFormat: "yy/mm/dd",
        showMonthAfterYear: true , 
        dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
    });                
    */
</script>
</body>
</html>