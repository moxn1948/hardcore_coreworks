<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/eas_menu.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/ui/trumbowyg.min.css" rel='stylesheet' type='text/css' />
<link href="${ contextPath }/resources/css/trumbowyg_table.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/trumbowyg.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/plugins/table/trumbowyg.table.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/langs/ko.min.js"></script>

<div id="scroll_area" class="inner_rt">
	<!-- 메인 컨텐츠 영역 시작! -->
	<div class="tbl_top_wrap">
		<div class="menu_tit">
			<h2>조회</h2>
		</div>
		<!-- 탭 영역 위 컨텐츠 시작 -->
			<button class="btn_pink">취소</button>
			<button class="btn_blue" onclick="updateEas();">저장</button>
		<!-- 탭 영역 위 컨텐츠 끝 -->	
	</div>
	<!-- 탭 영역 시작 -->
	<div class="main_ctn">
    	<ul class="tabs">
			<li class="tab-link current" data-tab="tab-1">결재정보</li>
			<li class="tab-link" data-tab="tab-2">본문</li>							
		</ul>
		<!-- 결재정보 탭 클릭 시 -->
        <div id="tab-1" class="tab-content current">
		<!-- 테이블 위 컨텐츠 시작 -->
        <table class="eas_information top_eas_information">
        	<tr>
        		<td class="eas_information_title">문서정보</td>
        		<td>
        		<c:if test="${ eas.EMERGENCY_YN eq 'N'}">
        		<input type="checkbox" id="eas_argent" disabled><label for="eas_argent">긴급</label>
        		</c:if>
        		<c:if test="${ eas.EMERGENCY_YN eq 'Y'}">
        		<input type="checkbox" id="eas_argent" checked disabled><label for="eas_argent">긴급</label>
        		</c:if>
        		</td>
        	</tr>
        	<tr>
        		<td>제목</td>
        		<td><c:out value="${ eas.DOC_TIT }"/></td>
        		<td>문서번호</td>
        		<td><c:out value="${ eas.DOCNO }"/></td>
        	</tr>
        	<tr class="eas_last_tr">
        		<td>열람제한</td>
        		<td>
        			<c:if test="${ eas.LIMIT_YN eq 'N' }">
        			<label for="limit"><input type="radio" class="new_eas_radio" id="limit" name="view_limit" disabled>제한종료일</label>
        			<input type="text" name="" id="datepicker" class="datepicker limit_date" disabled>
        			<label for="nolimit"><input type="radio" class="new_eas_radio" id="nolimit" name="view_limit" checked disabled>영구</label>
        			</c:if>
        			<c:if test="${ eas.LIMIT_YN eq 'Y' }">
        				<label for="limit"><input type="radio" class="new_eas_radio" id="limit" name="view_limit" checked disabled>제한종료일</label>
        				<input type="text" name="" id="datepicker" class="datepicker limit_date" value="${ eas.EAS_PERIOD }" disabled>
        				<label for="nolimit"><input type="radio" class="new_eas_radio" id="nolimit" name="view_limit" disabled>영구</label>
        			</c:if>
        		</td>
        		<td>보안설정</td>
        		<td>
        			<c:if test="${ eas.PROTECT_YN ne null }">
        			<label for="security_y"><input type="radio" class="new_eas_radio" id="security_y" name="security" checked disabled>사용</label>
        			<label for="security_n"><input type="radio" class="new_eas_radio" id="security_n" name="security" disabled>미사용</label>
        			</c:if>
        			<c:if test="${ eas.PROTECT_YN eq null }">
        			<label for="security_y"><input type="radio" class="new_eas_radio" id="security_y" name="security" disabled>사용</label>
        			<label for="security_n"><input type="radio" class="new_eas_radio" id="security_n" name="security" checked disabled>미사용</label>
        			</c:if>
        		</td>
        	</tr>
        </table>
			<c:if test="${ eas.PROTECT_YN ne null }"> 
			<!-- 보안설정 테이블 -->
			<div class="security_div2">
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn" id="protectDetail">
						 <colgroup>
						    <col style="width: 25%;">
						    <col style="width: 75%;">						    
					    </colgroup>
						<tr>
							<td class="tbl_main_tit sender_border">
								<c:if test="${ eas.PROTECT_YN eq 'RANGE' }">
								<input type="radio" class="new_eas_radio" id="security_radio" name="protectYn" value="RANGE" checked disabled>범위설정
								</c:if>
								<c:if test="${ eas.PROTECT_YN ne 'RANGE' }">
								<input type="radio" class="new_eas_radio" id="security_radio" name="protectYn" value="RANGE" disabled>범위설정
								</c:if>
							</td>
							<td>
								<div class="alertMessage">※ 범위 설정을 하게 될 경우 현재 직급에 따라서 보안 범위가 설정됩니다.</div>
								<c:if test="${ eas.SENIOR_YN eq 'Y' }">
								<input type="checkbox" class="protectDetail" id="security_1" value="up" checked disabled><label for="security_1">상급자</label>
								</c:if>
								<c:if test="${ eas.SENIOR_YN ne 'Y' }">
								<input type="checkbox" class="protectDetail" id="security_1" value="up" disabled><label for="security_1">상급자</label>
								</c:if>
								<c:if test="${ eas.MATE_YN eq 'Y' }">
								<input type="checkbox" class="protectDetail" id="security_2" value="equal" checked disabled><label for="security_2">동급자</label>
								</c:if>
								<c:if test="${ eas.MATE_YN ne 'Y' }">
								<input type="checkbox" class="protectDetail" id="security_2" value="equal" disabled><label for="security_2">동급자</label>
								</c:if>
								<c:if test="${ eas.DEPT_YN eq 'Y' }">
								<input type="checkbox" class="protectDetail" id="security_3" value="dept" checked disabled><label for="security_3">동일부서</label>
								</c:if>
								<c:if test="${ eas.DEPT_YN ne 'Y' }">
								<input type="checkbox" class="protectDetail" id="security_3" value="dept" disabled><label for="security_3">동일부서</label>
								</c:if>
								<input type="hidden" name="protectYnDetail" id="protectYnDetail">
							</td>													
						</tr>	
						<tr>
								<c:if test="${ eas.PROTECT_YN eq 'PERSON' }">
							<td class="tbl_main_tit sender_border">
								<input type="radio" class="new_eas_radio" id="security_addr" name="protectYn" value="PERSON" checked disabled>특정인 지정
							</td>
							<td>
								<a href="#protectPop" rel="modal:open" id="security_input">주소록으로 선택</a>
								<input type="hidden" name="protectDetailPerson" id="protectDetailPerson">
							</td>
							</c:if>
							<c:if test="${ eas.PROTECT_YN ne 'PERSON' }">
								<td class="tbl_main_tit sender_border">
								<input type="radio" class="new_eas_radio" id="security_addr" name="protectYn" value="PERSON" disabled>특정인 지정
							</td>
							<td>
								<a href="#protectPop" rel="modal:open" id="security_input">주소록으로 선택</a>
								<input type="hidden" name="protectDetailPerson" id="protectDetailPerson">
							</td>
							</c:if>																				
						</tr>
						<c:if test="${ eas.PROTECT_YN eq 'PERSON' }">	
						<c:forEach var="protect" items="${ protectPerson }">
						<tr>
							<td></td>
							<td><c:out value="${ protect.ENAME }" /><input type="hidden" value="${ protect.ENO }" class="userNum">
							<c:out value="(${ protect.PNAME }"/>/<c:out value="${ protect.JNAME })" /></td>
							<%-- <td><c:out value="${ protect.DNAME }" /></td> --%>
							
						</tr>
						</c:forEach>															
						</c:if>
					</table>
				</div>
			</div>
			</div>
			<!-- 보안설정 테이블 끝 -->
			</c:if>
			<!-- 보안설정 사용 클릭 시 나오는 부분 끝 -->
			<table class="eas_line_information" id="detUpMargin">
        	<tr>
        		<td class="eas_information_title">결재경로</td>
        	</tr>
		</table>
			<!-- 결재 경로 -->
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn" id="easLineTable">
						<tr class="tbl_main_tit">
							<th>순번</th>
							<th>처리방법</th>
							<th>직책(직위)</th>
							<th>처리자</th>
							<th>처리 상태</th>
							<th>처리 일시</th>
							<th>본문 버전</th>
						</tr>
						<c:forEach var="list" items="${upPathList}">
							<tr>
								<td class="tit"><c:out value="${list.PATH_RANK}"/></td>
								<td>
									<c:if test="${list.PROC_HOW eq 'CONFIRM'}">
										확인
									</c:if>
									<c:if test="${list.PROC_HOW eq 'AGM'}">
										합의
									</c:if>
									<c:if test="${list.PROC_HOW eq 'AS'}">
										결재
									</c:if>
									<c:if test="${list.PROC_HOW eq 'REFER'}">
										참조
									</c:if>
								</td>
								<td><c:out value="${list.JNAME}"/>/<c:out value="${list.PNAME}"/></td>
								<td><c:out value="${list.ENAME}"/></td>
								<td>
									<c:if test="${list.EAS_STATUE eq 'WAIT'}">
										대기
									</c:if>
									<c:if test="${list.EAS_STATUE eq 'PROC'}">
										진행
									</c:if>
									<c:if test="${list.EAS_STATUE eq 'COMPLETE'}">
										완료
									</c:if>									
								</td>
								<td><c:out value="${ list.PROCDATE }"/></td>
								<td></td>
							</tr>
						</c:forEach>												
					</table>
				</div>
			</div>
			<!-- 결재 경로 끝 -->
			
			<!-- 수신자 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">수신자</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl2">
				<div class="tbl_wrap">
					<table class="tbl_ctn eas_sender_tbl">
						 <colgroup>
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
					    </colgroup>
						<tr>
							<td class="tbl_main_tit sender_border">수신자</th>
							<td class="sender_border"><c:out value="${eas.RNAME}"/></td>
							<td class="tbl_main_tit sender_border">발신 명의</td>
							<td><c:out value="${eas.SENDER_NAME}"/></td>
						</tr>						
					</table>
										
				</div>				
			</div>
			<!-- 수신자 끝 -->
			
			<!-- 공람자 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">공람자</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl2">
				<div class="tbl_wrap">
					<table class="tbl_ctn eas_sender_tbl">
						 <colgroup>
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
					    </colgroup>
						<tr>
							<th class="tbl_main_tit sender_border">사원명</th>
							<th>직책/직급</th>
							<th>부서</th>
							<th>이메일
								<input type="hidden" id="comDocList">
							</th>
							
						</tr>
						<c:forEach var="list" items="${upPubList}">
						<tr>
							<td class="sender_border"><c:out value="${list.ENAME}"/></td>
							<td><c:out value="${list.JNAME}"/>/<c:out value="${list.PNAME}"/></td>
							<td><c:out value="${list.DNAME}"/></td>
							<td><c:out value="${ list.EID }" />@<c:out value="${ sessionScope.domain }" /></td>
						</tr>
						</c:forEach>								
					</table>
										
				</div>				
			</div>
			<!-- 공람자 끝 -->
			
			<!-- 관련문서 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">관련 문서</td>
        		<td><a href="#update_fileadd_pop" rel="modal:open" class="button btn_pink upRelDocBtn" type="button">문서선택</a></td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn" id="relDocTable">
						<colgroup>
							<col style="width: 20%;">
							<col style="width: 35%;">
							<col style="width: 10%;">
							<col style="width: 13%;">
							<col style="width: 13%;">
							<col style="width: 7%;">
						</colgroup>						
						<tr class="tbl_main_tit">
							<th>문서번호</th>
							<th>제목</th>
							<th>기안자</th>
							<th>기안 부서</th>
							<th>기안 일시</th>
							<th>삭제</th>	
						</tr>
						<c:if test="${ upRelList.size() == 0 }">
						<tr class="non_doc">
							<td class="tit" colspan="5" style="text-align: center;">관련 문서가 없습니다.</td>
						</tr>
						</c:if>
						<c:forEach var="list" items="${ upRelList }">
						<tr class="non_doc">
							<td class="tit"><c:out value="${ list.DOCNO }"/></td>
							<td><a href="#easRelDocpop" class="rel_doc_pop"><input type="hidden" value="${ list.REL_DOC_NO }"><c:out value="${ list.DTIT }"/></a></td>
							<td><c:out value="${ list.ENAME }"/></td>
							<td><c:out value="${ list.DEPT_NAME }"/></td>
							<td><c:out value="${ list.EAS_SDATE }"/></td>
							<td><button class='btn_solid_pink relDocDelete' type='button'>삭제</button></td>
						</tr>
						</c:forEach>
					</table>
				</div>
			</div>
			<!-- 관련 문서 끝 -->
			
			<form id="updateForm" action="updateEas.ea" method="post" enctype="multipart/form-data">
			<!-- 첨부파일 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">첨부 파일</td>
        		<td>
	        		<button class="btn_pink upFileBtn" id="chooseAttachment" type="button">파일선택</button>
	        		<div class="file_area"></div>
        		</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn fileTbl" id="attachTbl">
						<colgroup>
							<col style="width: 70%;">
							<col style="width: 13%;">
							<col style="width: 7%;">
						</colgroup>
						<tr class="tbl_main_tit">
							<th>파일명</th>
							<th>크기</th>	
							<th>삭제</th>					
						</tr>						
						<c:if test="${ upAttList.size() == 0 }">
						<tr class="non_file">
							<td class="tit"><p id="fileName" name="fileName">첨부된 파일이 없습니다.</p></td>
							<td></td>
							<td></td>
						</tr>		
						</c:if>
						<c:forEach var="list" items="${ upAttList }">
						<tr class="non_file">
							<td><c:out value="${ list.ORIGIN_NAME }"/></a></td>
							<td><c:out value="${ list.size }"/></td>
							<td><button class='btn_solid_pink attachDelete' type='button'>삭제</button></td>
						</tr>
						</c:forEach>
						
						
					</table>
					
				</div>
			</div>
			<!-- 첨부파일 끝 -->
		</div>
		<!-- 결재정보 탭 끝 -->
		
		<!-- 본문 탭 시작 -->
		<div id="tab-2" class="tab-content">
		<!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap eas_tbl_doc">
                        <h1 class="docu_tit" id="documentTit"><c:out value="${ detEas.FORMAT_NAME }" /></h1>
	                            <table class="tbl_ctn">
		                            <colgroup>
	                                    <col style="width: 10.5%;">
	                                    <col style="width: *;">
	                                    <col style="width: 7%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                </colgroup>
	                                <tr class="eas_tit">
	                                    <td rowspan="2" class="doc_con doc_tit">문서번호</td>
	                                    <td rowspan="2" class="doc_con"><c:out value="${ eas.DOCNO }" /></td>
	                                    <td rowspan="4" class="doc_con small doc_tit">결재</td>									
											<c:forEach var="list" items="${ upPathAsList }">
		                                    	<td class="doc_con gray doc_tit"><c:out value="${ list.PNAME }" /></td>
											</c:forEach>
											
											<c:forEach var="i" begin="1" end="${ 5 - upPathAsList.size() }">
                                       	<td class="doc_con gray"></td>
                              			</c:forEach> 
	                                </tr>  
	                               	<tr class="eas_tit eas_tit_center"> 
   
										<c:forEach var="list" items="${ upPathAsList }">
	                                   	 	<td rowspan="3" class="doc_con border_bottom"><c:out value="${ list.ENAME }" /><p><c:out value="${ list.PROCDATE }" /></p></td>
										</c:forEach>
										
	                                    <c:forEach var="i" begin="1" end="${ 5 - upPathAsList.size() }">
	                                    <td rowspan="3" class="doc_con border_bottom"></td>
										</c:forEach>  
									     
	                                </tr>                             
	                                <tr class="eas_tit" >
	                                    <td rowspan="2" class="doc_con doc_tit">작성일자</td>
	                                    <td rowspan="2" class="doc_con">20/01/15</td>                         	                                                                   
	                                </tr>
	                                <tr class="eas_tit"></tr>                                
	                                <tr class="eas_tit">
	                                    <td rowspan="2" class="doc_con doc_tit">작성부서</td>
	                                    <td rowspan="2" class="doc_con"><c:out value="${ eas.DEPT_NAME }" /></td>
	                                    <td rowspan="4" class="doc_con small doc_tit">합의</td> 
	                                 
										<c:forEach var="list" items="${ upPathAgmList }">
                                          <td class="doc_con gray doc_tit"><c:out value="${ list.PNAME }" /></td>
                              			</c:forEach>
                              
                                       	<c:forEach var="i" begin="1" end="${ 5 - upPathAgmList.size() }">
                                       	<td class="doc_con gray"></td>
                              			</c:forEach>  
								                               
	                                </tr>
	                                <tr class="eas_tit eas_tit_center">
									
										<c:forEach var="list" items="${ upPathAgmList }">
	                                   	 <td rowspan="3" class="doc_con border_bottom"><c:out value="${ list.ENAME }" /><p><c:out value="${ list.PROCDATE }" /></p></td>
										</c:forEach>
										
	                                    <c:forEach var="i" begin="1" end="${ 5 - upPathAgmList.size() }">
	                                    <td rowspan="3" class="doc_con border_bottom"></td>
										</c:forEach>  
							                      
	                                </tr>
	                                <tr class="eas_tit">
	                                    <td rowspan="2" class="doc_con doc_tit">작성자</td>
	                                    <td rowspan="2" class="doc_con"><c:out value="${ eas.ENAME }" /></td>                              
	                                </tr>
	                                <tr class="eas_tit"></tr>
	                                <tr class="eas_tit">
	                                    <td class="doc_con doc_tit">수신자</td>
	                                    <td colspan="7" class="doc_con"><c:out value="${ eas.RNAME }" /></td>                                 
	                                </tr>
	                                <tr class="eas_tit">
	                                    <td class="doc_con doc_tit">제목</td>
	                                    <td colspan="7" class="doc_con"><input type="text" value="${ upDocDet.docTit }" id="docTit"></td>                                
	                                </tr>
	                                
	                                <!-- 기안 본문 작성 부분 -->
                               		<tr class="eas_tit" id="easContentWriteBox">
                                    <td rowspan="10" colspan="8" class="eas_content" style="height:800px;">
                                    	<input type="hidden" name="docCnt" id="easContent">
                                    	<!-- 에디터 -->
										<div id="editer"></div>
									</td>                              
                                </tr>                                                       
	                            </table>
                        </div>
                    </div>
        <!-- 기본 테이블 끝 -->
		</div>
		<!-- 본문 탭 끝 -->
    </div>
    <!-- 탭 영역 끝 -->
    </div>
	<!-- 메인 컨텐츠 영역 끝! -->
		<!-- 기본 결재 테이블 -->
		<input type="hidden" name="limitYn" value="${eas.LIMIT_YN}">
		<input type="hidden" name="protectYn" value="${eas.PROTECT_YN}">
		<input type="hidden" name="emergencyYn" value="${eas.EMERGENCY_YN}">
		<input type="hidden" name="formatNo" value="${eas.FORMAT_NO}">
		<input type="hidden" name="empDivNo" value="${sessionScope.loginUser.empDivNo}">
		<%-- <input type="hidden" name="easSdate" value="${eas.EDATE}"> --%>
		<input type="hidden" name="tempYn" value="${eas.TEMP_YN}">
		<input type="hidden" name="easPeriod" value="${eas.EAS_PERIOD}">
		<input type="hidden" name="easDept" value="${eas.EAS_DEPT}">
		<input type="hidden" name="senderName" value="${eas.SENDER_NAME}">
		<input type="hidden" name="receiverName" value="${eas.RECEIVER_NAME}">
		<input type="hidden" name="type" value="${eas.TYPE}">
		<input type="hidden" name="mateYn" value="${eas.MATE_YN}">
		<input type="hidden" name="deptYn" value="${eas.DEPT_YN}">
		<input type="hidden" name="seniorYn" value="${eas.SENIOR_YN}">
		<input type="hidden" name="status" value="${eas.STATUS}">
		
		<!-- 결재 경로 -->
		<c:forEach var="list" items="${upPathList}">
			<input type="hidden" class="pathProcHow" value="${list.PROC_HOW}">
			<input type="hidden" class="pathName" value="${list.ENO}">
			<input type="hidden" class="pathRank" value="${list.PATH_RANK}">
			<input type="hidden" class="pathProcState" value="${list.EAS_STATUE}">
		</c:forEach>
		
		<input type="hidden" name="easPathRank" id="easPathRank">
		<input type="hidden" name="easProcHow" id="easProcHow">
		<input type="hidden" name="easLineEmpNo" id="easLineEmpNo">
		<input type="hidden" name="easProcState" id="easProcState">
		
		<!-- 버전 - 수정 시  -->
		<input type="hidden" id="updateTit" name="docTit">
		<input type="hidden" id="updateCnt" name="docCnt">
		<input type="hidden" name="versionNo" value="${eas.VERSION_NO}">
		<input type="hidden" id="eNoForUpdate" name="easNo" value="${eas.EAS_NO}">
		
		<!-- 관련문서 -->
		<input type="hidden" id="relDocNo" name="relDocNo">
		
		<!-- 파일선택 -->
	</form>
</div>
<!-- inner_rt end -->
</div>
</main>
</div>

<!-- popup include -->
<div id="update_fileadd_pop" class="modal">
	<jsp:include page="../pop/update_fileadd_pop.jsp" />
</div>


<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
    	$("#editer").html('${docCnt.docCnt}')
    	
    	$('#editer').trumbowyg({
			btns: [['viewHTML'],
			['undo', 'redo'], // Only supported in Blink browsers
			['formatting'],
			['strong', 'em', 'del'],
			['superscript', 'subscript'],
			['link'],
			['insertImage'],
			['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
			['unorderedList', 'orderedList'],
			['horizontalRule'],
			['removeformat'],
			['table']],
			lang: 'ko',
			plugins: {
			    table: {
				rows: 10,
				columns: 10,
				styler: 'tbl_ctn'
				}
			}
		});
    	
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(1).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");

        var path = '${ contextPath }';
	
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(0).addClass("on");

        // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
        $("#menu_area .menu_list").eq(0).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(0).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');

        	
        
       
        
		//발신명의자, 완료문서함 골라오기 위한 ajax -> 발신명의자 쿼리문 수정 필요
		var drafter = "${sessionScope.loginUser.empDivNo}"
		console.log(drafter); //기안자의 사원번호
		
		$.ajax({
			url: "selectReceiverAndDoc.ea",
			type:"post",
			async: false,
			data: {
				eNo:drafter
			},
			success:function(data){
				console.log(data);
				
				//$('#receiverNameSelect').find('option').remove();
				
				//$('#receiverNameSelect').append("<option value='" + data.receivers + "'>" + data.receivers + "</option>");
				
				$("#completeDocListTitle").append("완료함 (" + data.docList.length + ")");
				
				
 				for(var i=0; i<data.docList.length; i++) {
					$('#completeDocList').append('<tr><td><input type="checkbox" class="comDocChk" value="' + data.docList[i].EAS_NO + '"></td><td>'
													+ data.docList[i].DOCNO  + '</td><td>'
													+ data.docList[i].DOC_TIT + '</td><td>'
													+ data.docList[i].EMP_NAME + '<input type="hidden" class="easDept" value="' 
													+ data.docList[i].DEPT_NAME + '"><input type="hidden" class="easDate" value="'
													+ data.docList[i].EDATE + '"></td><td>'
													+ data.docList[i].EDATE + '</td></tr>');
 					
 					$('#completeShareDocList').append('<tr><td><input type="checkbox"></td><td>'
														+ data.docList[i].DOCNO + '</td><td>'
														+ data.docList[i].DOC_TIT + '</td><td>'
														+ data.docList[i].EMP_NAME + '<input type="hidden" class="easDept" value="' 
														+ data.docList[i].DEPT_NAME + '"><input type="hidden" class="easDate" value="'
														+ data.docList[i].EDATE + '"></td><td>'
														+ data.docList[i].EDATE + '</td></tr>');
					
				} 
				
				
			},
			error:function(status){
				console.log(status);
			}			
		});
		
        var idx = 0;
        var fileNode = '<input type="file" name="file">';	
        var fileTr = '<tr><td class="tit"><p></p></td><td></td><td></td></tr>';	
        	
        $("#chooseAttachment").click(function(){
        	idx++;
        	$(".file_area").append(fileNode).css("display", "none").find("input:last-child").prop("id", "chooseFile"+idx).trigger("click");        		
        		
        	$(document).on("change", "#chooseFile"+idx, function(){
	        	if(idx == 1){
	        		console.log($(".non_file").eq(0));
	        		$(".non_file").eq(0).css({display: "none"});
	        	}
	        	$("#attachTbl").append(fileTr);
	        	$("#attachTbl").find("tr").eq(idx+1).find(".tit p").eq(0).text($(this).val().split('/').pop().split('\\').pop());

	        	if(document.getElementById("chooseFile"+idx).value!=""){
	        		 var size = document.getElementById("chooseFile"+idx).files[0].size; //바이트 크기..?
	        		 var maxSize = 2 * 1024 * 1024;//2MB
					
	        		 if(size > maxSize){
		        		  alert("첨부파일 사이즈는 2MB 이내로 등록 가능합니다. ");
		        	 }
	        		 
	        		 var fileSize = String(size);
	        		 
	        		 if(fileSize.length <= 5) {
	        			 $("#attachTbl").find("tr").eq(idx+1).find("td").eq(1).text(fileSize.substr(0, 2) + 'KB');
		        		 $("#attachTbl").find("tr").eq(idx+1).find("td").eq(2).append('<button class="btn_solid_pink attachDelete">삭제</button>');
	        		 }else {
	        			 $("#attachTbl").find("tr").eq(idx+1).find("td").eq(1).text(fileSize.substr(0, 3) + 'KB');
		        		 $("#attachTbl").find("tr").eq(idx+1).find("td").eq(2).append('<button class="btn_solid_pink attachDelete">삭제</button>');
	        		 }
	        	}	        	    
        	});     		
        });
			
		var td = $(".pathName");
		var arr = Array();
		for(var i=0; i<td.length; i++){
			if(i!=(td.length-1)) {
				arr += td.eq(i).val() + ", ";
			}else {
				arr += td.eq(i).val();
			}
		}
		$("#easLineEmpNo").val(arr);
		
		console.log(arr);
		
		var td2 = $(".pathRank");
		var arr2 = Array();
		for(var i=0; i<td2.length; i++){
			if(i!=(td2.length-1)) {
				arr2 += td2.eq(i).val() + ", ";
			}else {
				arr2 += td2.eq(i).val();
			}
		}
		$("#easPathRank").val(arr2);
		
		console.log(arr2);
		
		var td3 = $(".pathProcHow");
		var arr3 = Array();
		for(var i=0; i<td3.length; i++){
			if(i!=(td3.length-1)) {
				arr3 += td3.eq(i).val() + ", ";
			}else {
				arr3 += td3.eq(i).val();
			}
		}
		$("#easProcHow").val(arr3);
		
		console.log(arr3);

       // $("#easLineTable tr not:first-child").find('td').eq(0).innerHTML;
        
        
        
    
    });
    
    $(document).on('click', '.relDocDelete', function(){
		$(this).parent().parent().remove();

 		if($("#relDocTable tr").length == 2) {
			$(".non_doc").show();
		}
	})
	
	$(document).on('click', '.attachDelete', function(){
		$(this).parent().parent().remove();
		console.log($("#attachTbl tr").length);

 		if($("#attachTbl tr").length <= 2) {
			$(".non_file").show();
		}
	});
    
    
    
  //탭 전환용 스크립트
    $(document).ready(function(){
    	
    	$('ul.tabs li').click(function(){
    		var tab_id = $(this).attr('data-tab');

    		$('ul.tabs li').removeClass('current');
    		$('.tab-content').removeClass('current');

    		$(this).addClass('current');
    		$("#"+tab_id).addClass('current');
    	})

    })
    
/*     //보안 설정용 스크립트
    $("#security_y").click(function(){
    	$(".eas_information tr:nth-child(3) td").css("padding-bottom", "0");
    	$(".security_div").show();
    });
  	
    $("#security_n").click(function(){
    	$(".security_div").hide();
    	$(".eas_information tr:nth-child(3) td").css("padding-bottom", "20px");
    });
    
/*     $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        nextText: '다음 달',
        prevText: '이전 달',
        dateFormat: "yy/mm/dd",
        showMonthAfterYear: true , 
        dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
    }); */ 
    
    function relDocAdd() {
    	console.log("my_wait_det_up의 모달 펑션 실행")
		$.modal.close();
		
		//완료 리스트 ... . . ... . . . . . ...
		var tr_array = Array();
		var send_array = Array();
	    var send_cnt = 0;
	    var chkbox = $('.comDocChk');
	    
	    for(var i=0; i<chkbox.length; i++) {
	    	if(chkbox[i].checked == true) {
	    		send_array[send_cnt] = chkbox[i].value;
	    		send_cnt++;
	    	}
	    }
	    
	    var str = send_array.join(",");
	    $("#relDocNo").val(str);
	    
	    console.log("relDocNo:" + $("#relDocNo").val());	//--> DB에 전달하는 값 	
	    var relDocNum = $("#relDocNo").val();
	    
 	    $.ajax({
	    	url: "selectRelDocList.ea",
	    	type: "post",
	    	data: {
	    		relDocNum:relDocNum
	    	},
	    	success: function(data) {
	    		console.log(data);
	    		
	    		//$(".non_doc").hide();
	    		
	    		for(var i=0; i<data.relDocList.length; i++) {
	    			$('#relDocTable').append("<tr><td>" + data.relDocList[i].DOCNO + "</td><td>"
	    									+ data.relDocList[i].DOC_TIT + "</td><td>"
	    									+ data.relDocList[i].EMP_NAME + "</td><td>"
	    									+ data.relDocList[i].DEPT_NAME + "</td><td>"
	    									+ data.relDocList[i].EDATE + "</td><td>"
	    									+ "<button class='btn_solid_pink relDocDelete' type='button'>삭제</button>");
	    			
	    			
	    		}
	    		
	    		
	    	},
	    	error: function(status) {
	    		console.log(status);	
	    	}   	
	    })	
	}
    
    function updateEas(){
		//수정 가능한 값 : 본문 제목, 본문 내용, 관련 문서, 첨부파일

 		//본문 제목
		var docTit = $("#docTit").val();
    	console.log(docTit);
     	$("#updateTit").val(docTit);
    	
	  	//본문 내용 넘기기
	   	var content = $('#editer').html();
		var test = $('#editer').text();
		console.log(content)
	    if(test != "") {
	    	$("#updateCnt").val(content);
	    	console.log($("#updateCnt").val());
			
	    	$("#updateForm").submit();
	    }else {
	    	alert('본문 내용 입력은 필수사항입니다.');
	    }
	    
	    
    }
    
    
   
    
</script>
</body>
</html>