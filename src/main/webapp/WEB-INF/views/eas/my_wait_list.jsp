<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/eas_menu.jsp" />


<c:if test="${ !empty loginUser }">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <!-- 테이블 위 컨텐츠 시작 -->
                	<c:if test="${menu eq 'myWait'}">
                    <div class="menu_tit"><h2>내 결재 - 결재대기</h2></div>
                    <div class="main_cnt my_wait_list_tbl_top">
                        <select name="" id="">
                            <option value="">제목</option>
                            <option value="">기안자</option>
                            <option value="">부서명</option>
                        </select>
                        <input type="search" name="" id="">
                        <button class="btn_solid_main">검색</button>
                    </div>
                    </c:if>
                    <c:if test="${menu eq 'myProc'}">
                    <div class="menu_tit"><h2>내 결재 - 결재진행</h2></div>
                    <div class="main_cnt my_wait_list_tbl_top">
                        <select name="" id="">
                           	<option value="">제목</option>
                            <option value="">기안자</option>
                            <option value="">부서명</option>
                        </select>
                        <input type="search" name="" id="">
                        <button class="btn_solid_main">검색</button>
                    </div>
                    </c:if>
                    <c:if test="${menu eq 'myRe'}">
                    <div class="menu_tit"><h2>내 결재 - 결재반려</h2></div>
                    <div class="main_cnt my_wait_list_tbl_top">
                        <select name="" id="">
                            <option value="">제목</option>
                            <option value="">기안자</option>
                            <option value="">부서명</option>
                        </select>
                        <input type="search" name="" id="">
                        <button class="btn_solid_main">검색</button>
                    </div>
                    </c:if>
                    
                    <c:if test="${menu eq 'easWait'}">
                    	<div class="menu_tit"><h2>참여 결재 - 결재대기</h2></div>                  
	                    <div class="main_cnt">
	                        <select name="" id="">
	                            <option value="">제목</option>
                            	<option value="">기안자</option>
                            	<option value="">부서명</option>
	                        </select>
	                        <input type="search" name="" id="">
	                        <button class="btn_solid_main">검색</button>
	                    </div>
                    </c:if>
                    <c:if test="${menu eq 'easProc'}">
                    	<div class="menu_tit"><h2>참여 결재 - 결재진행</h2></div>                  
	                    <div class="main_cnt">
	                        <select name="" id="">
	                            <option value="">제목</option>
                            	<option value="">기안자</option>
                            	<option value="">부서명</option>
	                        </select>
	                        <input type="search" name="" id="">
	                        <button class="btn_solid_main">검색</button>
	                    </div>
                    </c:if>
                    <c:if test="${menu eq 'easRe'}">
                    	<div class="menu_tit"><h2>참여 결재 - 결재반려</h2></div>                  
	                    <div class="main_cnt">
	                        <select name="" id="">
	                            <option value="">제목</option>
                            	<option value="">기안자</option>
                            	<option value="">부서명</option>
	                        </select>
	                        <input type="search" name="" id="">
	                        <button class="btn_solid_main">검색</button>
	                    </div>
                    </c:if>
                    <c:if test="${menu eq 'pubWait'}">
                    	<div class="menu_tit"><h2>공람 - 공람대기</h2></div>                  
	                    <div class="main_cnt">
	                        <select name="" id="">
	                            <option value="">제목</option>
                            	<option value="">기안자</option>
                            	<option value="">부서명</option>
	                        </select>
	                        <input type="search" name="" id="">
	                        <button class="btn_solid_main">검색</button>
	                    </div>
                    </c:if>
                    <c:if test="${menu eq 'pubComp'}">
                    	<div class="menu_tit"><h2>공람 - 공람완료</h2></div>                  
	                    <div class="main_cnt">
	                        <select name="" id="">
	                            <option value="">제목</option>
                            	<option value="">기안자</option>
                            	<option value="">부서명</option>
	                        </select>
	                        <input type="search" name="" id="">
	                        <button class="btn_solid_main">검색</button>
	                    </div>
                    </c:if>
                    <c:if test="${menu eq 'pubSend'}">
                    	<div class="menu_tit"><h2>공람 - 보낸공람</h2></div>                  
	                    <div class="main_cnt">
	                        <select name="" id="">
	                            <option value="">제목</option>
                           		<option value="">기안자</option>
                            	<option value="">부서명</option>
	                        </select>
	                        <input type="search" name="" id="">
	                        <button class="btn_solid_main">검색</button>
	                    </div>
                    </c:if>                  
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic" style="margin-top:15px">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" id="easList">
                                <colgroup>
                                    <col style="width: 8%;">
                                    <col style="width: 10%;">
                                    <col style="width: 15%;">
                                    <col style="width: 20%;">
                                    <col style="width: 37%;">
                                    <col style="width: 10%;">
                                </colgroup>
                                <tr class="tbl_main_tit">
                                    <th><input type="checkbox" class="eas_check"></th>
                                    <th>상태</th>
                                    <th>서식명</th>
                                    <th>문서번호</th>
                                    <th>제목</th>
                                    <th>기안 일시</th>                        
                                </tr>
                                <c:forEach var="list" items="${ list }">              	                               	
                                	<tr>                                		
                                		<td>
                                			<input type="checkbox">
                                			<input type="hidden" class="easNo" value="${ list.EAS_NO }">
                                			<c:if test="${ !empty list.ORIGIN }">
                                			<input type="hidden" class="origin" value="${ list.ORIGIN }">
                                			</c:if>
                                			<c:if test="${ empty list.ORIGIN }">
                                			<input type="hidden" class="origin">
                                			</c:if>
                                			<c:if test="${ !empty list.PROXY }">
                                			<input type="hidden" class="proxy" value="${ list.PROXY }">
                                			</c:if>
                                			<c:if test="${ empty list.PROXY }">
                                			<input type="hidden" class="proxy">
                                			</c:if>
                                		</td>
                                		<td>
                                			<c:if test="${list.TYPE eq 'RETURN'}">반려</c:if>
                                			<c:if test="${list.TYPE eq 'RECALL'}">회수</c:if>
                                			<c:if test="${list.TYPE eq 'CANCEL'}">취소</c:if>
                                			<c:if test="${list.TYPE eq 'PROC' && menu eq 'myWait'}">대기</c:if>
                                			<c:if test="${list.TYPE eq 'PROC' && menu eq 'myProc'}">진행</c:if>
                                			<c:if test="${list.TYPE eq 'PROC' && menu eq 'easWait'}">대기</c:if>
                                			<c:if test="${list.TYPE eq 'PROC' && menu eq 'easProc'}">진행</c:if>
                                			<c:if test="${list.TYPE eq 'PROC' && menu eq 'pubWait'}">대기</c:if>
                                			<c:if test="${list.TYPE eq 'COMPLETE'}">완료</c:if>	
                                		</td>
                                		<td><c:out value="${ list.FORMAT_NAME }"/></td>
                                		<td><c:out value="${ list.DOCNO }"/></td>
                                		<td><c:out value="${ list.DOC_TIT }"/></td>
                                		<td><c:out value="${ list.EDATE }"/></td>
                                	</tr>                             
                                </c:forEach>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        	<c:if test="${ pi.currentPage eq 1 }">
                        		<li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage ne 1 }">
                        		<c:url var="blistFirst" value="selectEasList.ea">
                        			<c:param name="currentPage" value="1"/>
                        		</c:url>
                        		<li class="pager_com pager_arr first"><a href="${ blistFirst }">&#x003C;&#x003C;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage <= 1 }">
								<li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage > 1 }">
								<c:url var="blistBack" value="selectEasList.ea">
									<c:param name="currentPage" value="${ pi.currentPage - 1 }"/>
								</c:url>
								<li class="pager_com pager_arr prev"><a href="${ blistBack }">&#x003C;</a></li>
							</c:if>
							<c:forEach var="p" begin="${ pi.startPage }" end="${ pi.endPage }">
								<c:if test="${ p eq pi.currentPage }">
									<li class="pager_com pager_num on"><a href="javascrpt: void(0);">${ p }</a></li>
								</c:if>
								<c:if test="${ p ne pi.currentPage }">
									<c:url var="blistCheck" value="selectEasList.ea">
										<c:param name="currentPage" value="${ p }"/>
									</c:url>
									<li class="pager_com pager_num"><a href="${ blistCheck }">${ p }</a></li>
								</c:if>
							</c:forEach>
                       		<c:if test="${ pi.currentPage >= pi.maxPage }">
								<li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage < pi.maxPage }">
								<c:url var="blistEnd" value="selectEasList.ea">
									<c:param name="currentPage" value="${ pi.currentPage + 1 }"/>
								</c:url>
								 <li class="pager_com pager_arr next"><a href="${ blistEnd }">&#x003E;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage eq pi.maxPage }">
                        		<li class="pager_com pager_arr last"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage ne pi.maxPage }">
                        		<c:url var="blistLast" value="selectEasList.ea">
                        			<c:param name="currentPage" value="${ pi.maxPage }"/>
                        		</c:url>
                        		<li class="pager_com pager_arr last"><a href="${ blistLast }">&#x003E;&#x003E;</a></li>
                        	</c:if>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(1).addClass("on");
        
        var path = '${ contextPath }';

        // 열리는 메뉴
        if("${menu}"=="myWait") {
        $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(0).addClass("on");

        // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
        $("#menu_area .menu_list").eq(0).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(0).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }else if("${menu}"=="myProc") {
        	// 열리는 메뉴
            $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(1).addClass("on");

            // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
            $("#menu_area .menu_list").eq(0).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(1).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }else if("${menu}"=="myRe"){
        	// 열리는 메뉴
            $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(2).addClass("on");

            // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
            $("#menu_area .menu_list").eq(0).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(2).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }else if("${menu}"=="myTemp"){
        	// 열리는 메뉴
            $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(3).addClass("on");

            // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
            $("#menu_area .menu_list").eq(0).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
            $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(3).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }else if("${menu}"=="easWait"){
        	// 열리는 메뉴
            $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");

            // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
            $("#menu_area .menu_list").eq(1).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }else if("${menu}"=="easProc"){
        	// 열리는 메뉴
            $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(1).addClass("on");

            // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
            $("#menu_area .menu_list").eq(1).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(1).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }else if("${menu}"=="easRe"){
        	// 열리는 메뉴
            $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(2).addClass("on");

            // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
            $("#menu_area .menu_list").eq(1).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
            $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(2).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }else if("${menu}"=="pubWait"){
        	// 열리는 메뉴
            $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");

            // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
            $("#menu_area .menu_list").eq(2).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }else if("${menu}"=="pubComp"){
        	// 열리는 메뉴
            $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");

            // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
            $("#menu_area .menu_list").eq(2).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }else if(("${menu}"=="pubSend")){
        	// 열리는 메뉴
            $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(2).addClass("on");

            // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
            $("#menu_area .menu_list").eq(2).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
            $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(2).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        }
        
        //상세보기 페이지랑 합쳐지면 바꿔야함!!!
        $(document).ready(function(){
        	$(".tbl_ctn tr").has('td').css('cursor', 'pointer');
        }).on("click", ".tbl_ctn tr", function(){
        	$('#easList').find('input[type="hidden"]').val();
        	var easNo = $(this).find('.easNo').val();
        	       	
        	var origin;
        	if($(this).find('.origin').val()==""){
        		origin = 0;
        	}else {
        		origin = $(this).find('.origin').val();
        	}
        	
			var proxy;
        	if($(this).find('.proxy').val()==""){
        		proxy = 0;
        	}else {
        		proxy = $(this).find('.proxy').val();
        	}

        	location.href="selectEasDetail.ea?easNo="+easNo+"&type=${menu}&origin="+origin+"&proxy="+proxy;
        });
    
    });

</script>
</body>
</html>

</c:if>
<c:if test="${ empty loginUser }">
	<jsp:forward page="../../../index.jsp"/>
</c:if>