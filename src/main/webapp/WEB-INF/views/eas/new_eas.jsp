<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/eas_menu.jsp" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/ui/trumbowyg.min.css" rel='stylesheet' type='text/css' />
<link href="${ contextPath }/resources/css/trumbowyg_table.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/trumbowyg.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/plugins/table/trumbowyg.table.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/langs/ko.min.js"></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<div id="scroll_area" class="inner_rt">
	<!-- 메인 컨텐츠 영역 시작! -->
	<div class="tbl_top_wrap">
		<div class="menu_tit">
			<h2>기안하기</h2>
		</div>
		<!-- 탭 영역 위 컨텐츠 시작 -->
			<c:if test="${ empty param.btn }">
			<button class="btn_solid_blue" onclick="submitNewEas();">결재 올림</button>
			</c:if>
			<c:if test="${ !empty param.btn }">
			<button class="btn_solid_blue" onclick="submitNewEas();">재기안</button>
			</c:if>
			<!-- <button class="btn_main" onclick="tempSave();">임시저장</button> -->
		<!-- 탭 영역 위 컨텐츠 끝 -->	
	</div>
	<!-- 탭 영역 시작 -->
	<div class="main_ctn">
    	<ul class="tabs">
			<li class="tab-link current" data-tab="tab-1">결재정보</li>
			<li class="tab-link" data-tab="tab-2" id="docContentTab">본문</li>							
		</ul>
		<!-- 결재정보 탭 클릭 시 -->
		<c:if test="${ empty param.btn }">
		<form id="newEasForm" action="insertNewEas.ea" method="post" enctype="multipart/form-data">		
		</c:if>
		<c:if test="${ !empty param.btn }">
		<form id="newEasForm" action="reinsert.ea?formatNo=${ eas.FORMAT_NO }" method="post" enctype="multipart/form-data">		
		</c:if>
        <div id="tab-1" class="tab-content current">
		<!-- 테이블 위 컨텐츠 시작 -->
        <table class="eas_information top_eas_information">
        	<tr>
        		<td class="eas_information_title">문서정보</td>
        		<c:if test="${ !empty param.btn }">
        			<c:if test="${ eas.EMERGENCY_YN.equals('Y') }">
        				<td><input type="checkbox" id="eas_argent" name="emergencyYn" value="Y"><label for="eas_argent" checked>긴급</label></td>
        			</c:if>
        			<c:if test="${ eas.EMERGENCY_YN.equals('N') }">
        				<td><input type="checkbox" id="eas_argent" name="emergencyYn" value="Y"><label for="eas_argent">긴급</label></td>
        			</c:if>
        		</c:if>
        		<c:if test="${ empty param.btn }">
        			<td><input type="checkbox" id="eas_argent" name="emergencyYn" value="Y"><label for="eas_argent">긴급</label></td>
        		</c:if>
        	</tr>
        	<tr>
        		<td>제목</td>
        		<c:if test="${ !empty param.btn }">
        			<td>
        				<input type="text" class="eas_title" id="eas_info_title" name="easTitle" value="${ upDocDet.docTit }">
        				<!-- <input type="hidden" id="formatNoForReinsert" name="formatNo"> -->
        			</td>
        		</c:if>
        		<c:if test="${ empty param.btn }">
        			<td><input type="text" class="eas_title" id="eas_info_title" name="easTitle"></td>
        		</c:if>
        	</tr>
        	<c:if test="${ empty param.btn }">
        	<tr class="eas_last_tr">
        		<td>열람제한</td>
        		<td>
        			<label for="limit"><input type="radio" class="new_eas_radio" id="limit" name="limit_radio">제한종료일</label>
        			<input type="text" id="datepicker" class="datepicker limit_date" autocomplete="off" disabled>     			
        			<label for="nolimit"><input type="radio" class="new_eas_radio" id="nolimit" name="limit_radio" checked>영구</label>
        			<input type="date" id="hiddenDate" name="easPeriod" hidden="true">
        		</td>
        		<td>보안설정</td>
        		<td>
        			<label for="security_y"><input type="radio" class="new_eas_radio" id="security_y" name="protectYnRadio">사용</label>
        			<label for="security_n"><input type="radio" class="new_eas_radio" id="security_n" name="protectYnRadio" checked>미사용</label>
        			<input type="hidden" value="${ sessionScope.format.formatNo }" name="formatNo">
        			<input type="hidden" value="${ sessionScope.loginUser.empDivNo }" name="empDivNo">
					<input type="hidden" value="${ sessionScope.loginUser.deptNo }" name="easDept" id="empDeptNo">        		
        		</td>
        	</tr>
        	</c:if>
        	<c:if test="${ !empty param.btn }">
        	<tr class="eas_last_tr">
        		<td>열람제한</td>
        		<c:if test="${ eas.LIMIT_YN.equals('Y') }">
        		<td>
        			<label for="limit"><input type="radio" class="new_eas_radio" id="limit" name="limit_radio" checked>제한종료일</label>
        			<input type="text" id="datepicker" class="datepicker limit_date" autocomplete="off">     			
        			<label for="nolimit"><input type="radio" class="new_eas_radio" id="nolimit" name="limit_radio">영구</label>
        			<input type="date" id="hiddenDate" name="easPeriod" hidden="true">
        		</td>
        		</c:if>
        		<c:if test="${ eas.LIMIT_YN.equals('N') }">
        		<td>
        			<label for="limit"><input type="radio" class="new_eas_radio" id="limit" name="limit_radio">제한종료일</label>
        			<input type="text" id="datepicker" class="datepicker limit_date" autocomplete="off" disabled>     			
        			<label for="nolimit"><input type="radio" class="new_eas_radio" id="nolimit" name="limit_radio" checked>영구</label>
        			<input type="date" id="hiddenDate" name="easPeriod" hidden="true">
        		</td>
        		</c:if>
        		<c:if test="${ empty eas.PROTECT_YN }">
        		<td>보안설정</td>
        		<td>
        			<label for="security_y"><input type="radio" class="new_eas_radio" id="security_y" name="protectYnRadio">사용</label>
        			<label for="security_n"><input type="radio" class="new_eas_radio" id="security_n" name="protectYnRadio" checked>미사용</label>
        			<input type="hidden" value="${ sessionScope.format.formatNo }" name="formatNo">
        			<input type="hidden" value="${ sessionScope.loginUser.empDivNo }" name="empDivNo">
					<input type="hidden" value="${ sessionScope.loginUser.deptNo }" name="easDept" id="empDeptNo">        		
        		</td>
        		</c:if>
        		<c:if test="${ !empty eas.PROTECT_YN }">
        		<td>보안설정</td>
        		<td>
        			<label for="security_y"><input type="radio" class="new_eas_radio" id="security_y" name="protectYnRadio" checked>사용</label>
        			<label for="security_n"><input type="radio" class="new_eas_radio" id="security_n" name="protectYnRadio">미사용</label>
        			<input type="hidden" value="${ sessionScope.format.formatNo }" name="formatNo">
        			<input type="hidden" value="${ sessionScope.loginUser.empDivNo }" name="empDivNo">
					<input type="hidden" value="${ sessionScope.loginUser.deptNo }" name="easDept" id="empDeptNo">        		
        		</td>
        		</c:if>
        	</tr>
        	</c:if>
        </table>
			
			<!-- 보안설정 테이블 -->
			<div class="security_div">
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
				<c:if test="${ empty param.btn }">
					<table class="tbl_ctn" id="protectDetail">
						 <colgroup>
						    <col style="width: 25%;">
						    <col style="width: 75%;">						    
					    </colgroup>
						<tr>
							<td class="tbl_main_tit sender_border">
								<input type="radio" class="new_eas_radio" id="security_radio" name="protectYn" value="RANGE">범위설정
							</td>
							<td>
								<div class="alertMessage">※ 범위 설정을 하게 될 경우 현재 직급에 따라서 보안 범위가 설정됩니다.</div>
								<input type="checkbox" class="protectDetail" id="security_1" value="up"><label for="security_1">상급자</label> 
								<input type="checkbox" class="protectDetail" id="security_2" value="equal"><label for="security_2">동급자</label>
								<input type="checkbox" class="protectDetail" id="security_3" value="dept"><label for="security_3">동일부서</label>
								<input type="hidden" name="protectYnDetail" id="protectYnDetail">
							</td>													
						</tr>	
						<tr>
							<td class="tbl_main_tit sender_border">
								<input type="radio" class="new_eas_radio" id="security_addr" name="protectYn" value="PERSON">특정인 지정
							</td>
							<td>
								<!-- <a class="security_text" id="security_input" name="protectYnDetail" disabled> -->
								<a href="#protectPop" rel="modal:open" id="security_input">주소록으로 선택</a>
								<input type="hidden" name="protectDetailPerson" id="protectDetailPerson">
							</td>													
						</tr>											
					</table>
				</c:if>
				<c:if test="${ !empty param.btn }">
					<table class="tbl_ctn" id="protectDetail">
						 <colgroup>
						    <col style="width: 25%;">
						    <col style="width: 75%;">						    
					    </colgroup>
						<tr>
							<td class="tbl_main_tit sender_border">
							<c:if test="${ eas.PROTECT_YN.equals('RANGE') }">
								<input type="radio" class="new_eas_radio" id="security_radio" name="protectYn" value="RANGE" checked>범위설정
							</c:if>
							<c:if test="${ !eas.PROTECT_YN.equals('RANGE') }">
								<input type="radio" class="new_eas_radio" id="security_radio" name="protectYn" value="RANGE">범위설정
							</c:if>
							</td>
							<td>
								<div class="alertMessage">※ 범위 설정을 하게 될 경우 현재 직급에 따라서 보안 범위가 설정됩니다.</div>
								<input type="checkbox" class="protectDetail" id="security_1" value="up"><label for="security_1">상급자</label> 
								<input type="checkbox" class="protectDetail" id="security_2" value="equal"><label for="security_2">동급자</label>
								<input type="checkbox" class="protectDetail" id="security_3" value="dept"><label for="security_3">동일부서</label>
								<input type="hidden" name="protectYnDetail" id="protectYnDetail">
							</td>													
						</tr>	
						<tr>
							<td class="tbl_main_tit sender_border">
							<c:if test="${ eas.PROTECT_YN.equals('PERSON') }">
								<input type="radio" class="new_eas_radio" id="security_addr" name="protectYn" value="PERSON" checked>특정인 지정
							</c:if>
							<c:if test="${ !eas.PROTECT_YN.equals('PERSON') }">
								<input type="radio" class="new_eas_radio" id="security_addr" name="protectYn" value="PERSON">특정인 지정
							</c:if>
							</td>
							<td>
								<!-- <a class="security_text" id="security_input" name="protectYnDetail" disabled> -->
								<a href="#protectPop" rel="modal:open" id="security_input">주소록으로 선택</a>
								<input type="hidden" name="protectDetailPerson" id="protectDetailPerson">
							</td>													
						</tr>											
					</table>
				</c:if>
				</div>
			</div>
			</div>
			<!-- 보안설정 테이블 끝 -->
			<!-- 결재 경로 -->
			<table class="eas_line_information">
        	<tr>
        		<td class="eas_information_title">결재경로
        		<input type="hidden" id="drafter" value="${ sessionScope.loginUser.empDivNo }">
        		<input type="hidden" name="easPathRank" id="easPathRank">
    			<input type="hidden" name="easProcHow" id="easProcHow">
    			<input type="hidden" name="easLineEmpNo" id="easLineEmpNo"></td>  		
        		<td><a href="#AddrPop" rel="modal:open" class="button btn_pink new_eas_btn" id="easLineBtn">결재경로설정</a></td>     
        	</tr>
			</table>
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn" id="easLineTbl">
						<colgroup>
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
					    </colgroup>
						<tr class="tbl_main_tit">
							<th>순번</th>
							<th>처리방법</th>
							<th>직책/직급</th>
							<th>처리자</th>
						</tr>
						<c:if test="${ !empty param.btn }">					
							<c:forEach var="list" items="${ upPathList }">
								<tr>
									<td><c:out value="${ list.PATH_RANK }"/></td>
									<td>
										<c:if test="${list.PROC_HOW eq 'CONFIRM'}">
											확인
										</c:if>
										<c:if test="${list.PROC_HOW eq 'AGM'}">
											합의
										</c:if>
										<c:if test="${list.PROC_HOW eq 'AS'}">
											결재
										</c:if>
										<c:if test="${list.PROC_HOW eq 'REFER'}">
											참조
										</c:if>
									</td>
									<td><c:out value="${ list.JNAME }"/>/<c:out value="${ list.PNAME }"/></td>
									<td><c:out value="${ list.ENAME }"/><td>
								</tr>
							</c:forEach>
						</c:if>											
					</table>
				</div>
			</div>
			<!-- 결재 경로 끝 -->
			
			<!-- 수신자 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">수신자</td>
        		<td><a href="#AddrPop2" rel="modal:open" class="button btn_pink new_eas_btn" id="senderEditBtn">수신자설정</a></td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl2">
				<div class="tbl_wrap">
					<table class="tbl_ctn eas_sender_tbl" id="receiverTbl">
						 <colgroup>
						    <col style="width: 25%;">
						    <col style="width: 25%;">
						    <col style="width: 25%;">
 						    <col style="width: 25%;"> 
					    </colgroup>
						<tr class="tbl_main_tit">
							<td class="tbl_main_tit sender_border">수신자</th>
							<c:if test="${ empty param.btn }">
							<td class="sender_border"></td>
							</c:if>
							<c:if test="${ !empty param.btn }">
							<td class="sender_border"><c:out value="${ eas.RNAME }"/></td>
							</c:if>
							<td class="tbl_main_tit sender_border">발신 명의</td>
							<c:if test="${ empty param.btn }">
							<td>
								<input type="hidden" name="senderName" id="senderName"> 
								<select id="receiverNameSelect">	
								</select>
							</td>
							</c:if>
							<c:if test="${ !empty param.btn }">
							<td>
								<input type="hidden" name="senderName" id="senderName"> 
								<select id="receiverNameSelect">
								<option value="${ eas.SENDER_NAME }" selected></option>	<!-- 이게 맞나 모르겠음 -->
								</select>
							</td>
							</c:if>
						</tr>						
					</table>										
				</div>				
			</div>
			<!-- 수신자 끝 -->
			
			<!-- 공람자 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">공람자</td>
        		<td><a href="#AddrPop3" rel="modal:open" class="button btn_pink new_eas_btn" id="gongramEditBtn">공람자설정</a></td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl2">
				<div class="tbl_wrap">
					<table class="tbl_ctn eas_sender_tbl" id="gongramTbl">
						<colgroup>
							<col style="width: 25%;">
							<col style="width: 25%;">
							<col style="width: 25%;">
							<col style="width: 25%;">
						</colgroup>
						<tr class="tbl_main_tit">
							<th>부서</th>
							<th>직급/직책</th>
							<th>공람자<input type="hidden" name="shareEmpNo" id="shareEmpNo"></th>
							<th>이메일</th>							
						</tr>
						<c:if test="${ empty param.btn }">					
						<tr id="shareName">					
						</tr>						
						</c:if>
						<c:if test="${ !empty param.btn }">
							<c:forEach var="list" items="${ upPubList }">					
								<tr id="shareName">	
									<td><c:out value="${ list.DNAME }"/></td>
									<td><c:out value="${ list.JNAME }"/>/<c:out value="${ list.PNAME }"/></td>
									<td><c:out value="${ list.ENAME }"/></td>
									<td><c:out value="${ list.EID }"/>@<c:out value="${ sessionScope.domain }" /></td>			
								</tr>						
							</c:forEach>
						</c:if>
					</table>
										
				</div>				
			</div>
			<!-- 공람자 끝 -->
			
			<!-- 관련문서 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">관련 문서</td>
        		<td><a href="#eas_fileadd_pop" rel="modal:open" class="button btn_pink new_eas_btn" id="selectDocBtn">문서선택</a></td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn" id="relDocTable">
						<colgroup>
						    <col style="width: 15%;">
						    <col style="width: 30%;">
						    <col style="width: 15%;">	
						    <col style="width: 15%;">	
						    <col style="width: 15%;">
						    <col style="width: 10%;">							    
					    </colgroup>						
						<tr class="tbl_main_tit">
							<th>문서번호<input type="hidden" id="comDocList" name="relDocNo"></th>
							<th>제목</th>
							<th>기안자</th>
							<th>기안 부서</th>
							<th>기안 일시</th>
							<th>삭제</th>
						</tr>
						<c:if test="${ !empty param.btn }">
						<c:if test="${ empty upRelList }">
						<tr class="non_doc">
							<td class="tit"><p id="fileName" name="fileName">첨부된 문서가 없습니다.</p></td>
							<td colspan="5"></td>							
						</tr>
						</c:if>
						<c:if test="${ !empty upRelList }">
							<c:forEach var="list" items="${ upRelList }">
								<tr class="non_doc">
									<td class="tit"><p id="fileName" name="fileName"><c:out value="${ list.REL_DOC_NO }"/></p></td>
									<td><c:out value="${ list.DTIT }"/></td>
									<td><c:out value="${ list.ENAME }"/></td>
									<td><c:out value="${ list.DEPT_NAME }"/></td>
									<td><c:out value="${ list.EAS_SDATE }"/></td>
									<td><button class='btn_solid_pink relDocDelete' type='button'>삭제</button></td>							
								</tr>
							</c:forEach>
						</c:if>
						</c:if>
						<c:if test="${ empty param.btn }">
						<tr class="non_doc">
							<td class="tit"><p id="fileName" name="fileName">첨부된 문서가 없습니다.</p></td>
							<td colspan="5"></td>							
						</tr>
						</c:if>
					</table>
				</div>
			</div>
			<!-- 관련 문서 끝 -->
			
			<!-- 첨부파일 -->
			<table class="eas_information">
			<tr>
        		<td class="eas_information_title">첨부 파일
        		<td>
	        		<button class="btn_pink new_eas_btn" id="chooseAttachment" type="button">파일선택</button>
	        		<div class="file_area"></div>
        		</td>
        	</tr>
       		</table>
			<div class="tbl_common tbl_basic new_eas_tbl">
				<div class="tbl_wrap">
					<table class="tbl_ctn fileTbl" id="attachTbl">
						<colgroup>
							<col style="width: 70%;">
							<col style="width: 13%;">
							<col style="width: 7%;">
						</colgroup>
						<tr class="tbl_main_tit">
							<th>파일명</th>
							<th>크기</th>
							<th>삭제</th>							
						</tr>					
						<c:if test="${ empty param.btn }">
						<tr class="non_file">
							<td class="tit"><p id="fileName" name="fileName">첨부된 파일이 없습니다.</p></td>							
						</tr>
						</c:if>
						<c:if test="${ !empty param.btn }">
							<c:if test="${ empty upAttList }">
							<tr class="non_file">
								<td class="tit"><p id="fileName" name="fileName">첨부된 파일이 없습니다.</p></td>							
							</tr>
							</c:if>
							<c:if test="${ !empty upAttList }">
								<c:forEach var="list" items="${ upAttList }">
								<tr class="non_file">
									<td class="tit"><p id="fileName" name="fileName"><c:out value="${ list.ORIGIN_NAME }" /></p></td>
									<td><c:out value="${ list.size }" /><input type="hidden" id="testSize" value="${ list.size }"></td>													
									<td><button class="btn_solid_pink attachDelete">삭제</button></td>			
								</tr>
								</c:forEach>
							</c:if>
						</c:if>			
					</table>				
				</div>
			</div>
			<!-- 첨부파일 끝 -->
		</div>
		<!-- 결재정보 탭 끝 -->
		
		<!-- 본문 탭 시작 -->
		<div id="tab-2" class="tab-content">
		<!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        	<div class="formatTitle">
                        		<c:out value="${ format.formatName }"/>                       		
                        	</div>
                        <div class="tbl_wrap eas_tbl_doc">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: 80px;">
                                    <col style="width: *;">
                                    <col style="width: 80px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                </colgroup>                                
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">문서번호</td>
                                    <c:if test="${ empty param.btn }">
                                    <td rowspan="2" class="doc_con" id="mainText_easNum"></td>
                                    </c:if>
                                    <c:if test="${ !empty param.btn }">
                                    <td rowspan="2" class="doc_con" id="mainText_easNum"><c:out value="${ eas.DOCNO }"/></td>
                                    </c:if>
                                    <td rowspan="4" class="doc_con small doc_tit" id="asPosition">결재</td>                                   
                                    <td class="doc_con gray doc_tit"></td>
                                    <td class="doc_con gray doc_tit"></td>
                                    <td class="doc_con gray doc_tit"></td>
                                    <td class="doc_con gray doc_tit"></td>
                                    <td class="doc_con gray doc_tit"></td>  
                                                                
                                </tr>
                               	<tr class="eas_tit" id="asName">                                 
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                </tr>                             
                                <tr class="eas_tit" >
                                    <td rowspan="2" class="doc_con doc_tit">작성일자</td>
                                    <c:if test="${ empty param.btn }">
                                    <td rowspan="2" class="doc_con" id="mainText_easDate"></td>  
                                    </c:if>
                                    <c:if test="${ !empty param.btn }">
                                    <td rowspan="2" class="doc_con" id="mainText_easDate"><c:out value="${ eas.EDATE }"/></td>
                                    </c:if>                       	                                                                   
                                </tr>
                                <tr class="eas_tit"></tr>                                
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성부서</td>
                                    <c:if test="${ empty param.btn }">
                                    	<td rowspan="2" class="doc_con" id="mainText_easDept"></td>
                                    </c:if>
                                    <c:if test="${ !empty param.btn }">
                                    	<td rowspan="2" class="doc_con" id="mainText_easDept"><c:out value="${ eas.DEPT_NAME }"/></td>
                                    </c:if>
                                    <td rowspan="4" class="doc_con small doc_tit" id="agmPosition">합의</td>
                                    <td class="doc_con gray doc_tit"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>                                 
                                </tr>
                                <tr class="eas_tit" id="agmName">
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>                                
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성자</td>
                                    <td rowspan="2" class="doc_con"><c:out value="${ sessionScope.loginUser.empName }"/></td>                              
                                </tr>  
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">수신자</td>
                                    <c:if test="${ empty param.btn }">
                                    <td colspan="7" class="doc_con" id="mainText_senderName"></td>    
                                    </c:if>
                                    <c:if test="${ !empty param.btn }">
                                    <td colspan="7" class="doc_con" id="mainText_senderName"><c:out value="${ eas.RNAME }"/></td>    
                                    </c:if>                      
                                </tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">제목</td>
                                    <c:if test="${ empty param.btn }">
                                    <td colspan="7" class="doc_con" id="mainText_easTitle"></td>  
                                    </c:if>
                                    <c:if test="${ !empty param.btn }">
                                    <td colspan="7" class="doc_con" id="mainText_easTitle"><c:out value="${ upDocDet.DOC_TIT }"/></td>  
                                    </c:if>                               
                                </tr>
                                <!-- 기안 본문 작성 부분 -->
                                <tr class="eas_tit" id="easContentWriteBox">
                                    <td rowspan="10" colspan="8" class="eas_content" style="height:800px;">
                                    	<input type="hidden" name="docCnt" id="easContent">
                                    	<c:if test="${ empty param.btn }">
                                    	<!-- 에디터 -->                                    	
										<div id="editer"></div>
										</c:if>
										<c:if test="${ !empty param.btn }">
                                    	<!-- 에디터 -->                                    	
										<div id="editer"><c:out value="${ upDocDet.DOC_CNT }"/></div>
										</c:if>
									</td>                              
                                </tr>                                                     
                            </table>
                        </div>
                    </div>
        <!-- 기본 테이블 끝 -->
		</div>
		<!-- 본문 탭 끝 -->
		</form>
    </div>
    <!-- 탭 영역 끝 -->
    </div>
	<!-- 메인 컨텐츠 영역 끝! -->
</div>
<!-- inner_rt end -->
</div>
</main>
</div>

<!-- 보안설정 특정인 지정 popup include -->
<div id="protectPop" class="modal">
	<jsp:include page="../pop/new_eas_addr_pop5.jsp" />
</div>
<!-- 결재경로 popup include -->
<div id="AddrPop" class="modal">
	<jsp:include page="../pop/new_eas_addr_pop.jsp" />
</div>
<!-- 수신자 popup include -->
<div id="AddrPop2" class="modal">
	<jsp:include page="../pop/new_eas_addr_pop2.jsp" />
</div>
<!-- 공람자 popup include -->
<div id="AddrPop3" class="modal">
	<jsp:include page="../pop/new_eas_addr_pop3.jsp" />
</div>
<!-- 관련문서 popup include -->
<div id="eas_fileadd_pop" class="modal">
	<jsp:include page="../pop/new_eas_fileadd_pop.jsp" />
</div>
<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
    	/* var editor = new FroalaEditor('.fr-view') */
    	$("#editer").html('${format.formatCnt}');
    	
    	$('#editer').trumbowyg({
			btns: [['viewHTML'],
			['undo', 'redo'], // Only supported in Blink browsers
			['formatting'],
			['strong', 'em', 'del'],
			['superscript', 'subscript'],
			['link'],
			['insertImage'],
			['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
			['unorderedList', 'orderedList'],
			['horizontalRule'],
			['removeformat'],
			['table']],
			lang: 'ko',
			plugins: {
			    table: {
				rows: 10,
				columns: 10,
				styler: 'tbl_ctn',
				}
			},
		});

        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(1).addClass("on");

        var path = '${ contextPath }';
        
        
        $(".datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });  
        
        var idx = 0;
        var fileNode = '<input type="file" name="file">';	
        var fileTr = '<tr><td class="tit"><p></p></td><td></td><td></td></tr>';	
        	
        $("#chooseAttachment").click(function(){
        	idx++;
        	$(".file_area").append(fileNode).css("display", "none").find("input:last-child").prop("id", "chooseFile"+idx).trigger("click");        		
        		
        	$(document).on("change", "#chooseFile"+idx, function(){
	        	if(idx == 1){
	        		console.log($(".non_file").eq(0));
	        		$(".non_file").eq(0).css({display: "none"});
	        	}
	        	$("#attachTbl").append(fileTr);
	        	$("#attachTbl").find("tr").eq(idx+1).find(".tit p").eq(0).text($(this).val().split('/').pop().split('\\').pop());

	        	if(document.getElementById("chooseFile"+idx).value!=""){
	        		 var size = document.getElementById("chooseFile"+idx).files[0].size; //바이트 크기..?
	        		 var maxSize = 2 * 1024 * 1024;//2MB
					
	        		 if(size > maxSize){
		        		  alert("첨부파일 사이즈는 2MB 이내로 등록 가능합니다. ");
		        	 }
	        		 
	        		 var fileSize = String(size);
	        		 
	        		 if(fileSize.length <= 5) {
	        			 $("#attachTbl").find("tr").eq(idx+1).find("td").eq(1).text(fileSize.substr(0, 2) + 'KB');
		        		 $("#attachTbl").find("tr").eq(idx+1).find("td").eq(2).append('<button class="btn_solid_pink attachDelete">삭제</button>');
	        		 }else {
	        			 $("#attachTbl").find("tr").eq(idx+1).find("td").eq(1).text(fileSize.substr(0, 3) + 'KB');
		        		 $("#attachTbl").find("tr").eq(idx+1).find("td").eq(2).append('<button class="btn_solid_pink attachDelete">삭제</button>');
	        		 }
	        	}	        	    
        	});     		
        });	
        
        //열람제한 설정 시에만 datepicker 활성화 스크립트
        $(".new_eas_radio").click(function(){
        	if($(this).attr('id')=='limit') {
        		$("#datepicker").attr('disabled', false);
        		
        	}else {
        		$("#datepicker").attr('disabled', true);
        	}
        })
        
        //문서 선택 모달용 스크립트
        $("#checkAll").change(function(){
			if($("#checkAll").is(":checked")){
				$("#completeDocList tr td input[type='checkbox']").prop('checked', true);				
			}else{
				$("#completeDocList tr td input[type='checkbox']").prop('checked', false);
			}
		});        
        $("#checkAll2").change(function(){
			if($("#checkAll2").is(":checked")){
				$("#completeShareDocList tr td input[type='checkbox']").prop('checked', true);				
			}else{
				$("#completeShareDocList tr td input[type='checkbox']").prop('checked', false);
			}
		});
        
        //작성한 내용을 본문에 옮기기 위한 스크립트 
		$("#docContentTab").click(function(){
			console.log($("#asPosition").siblings().eq(2));
			
			var dNo = $('#empDeptNo').val();
			$.ajax({
				url:"findDeptName.ea",
				type:"post",
				data:{dNo:dNo},
				success:function(data){
					console.log(data.deptName);
					
					$("#mainText_easDept").text(data.deptName);
				},
				error:function(status){
					console.log(status);
				}
			});
			var title = $("#eas_info_title").val();
			
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1;
			var yyyy = today.getFullYear();
			var today2 = '';

			if(dd<10) {
			    dd='0'+dd
			} 

			if(mm<10) {
			    mm='0'+mm
			} 

			today = yyyy + "년 " + mm + "월 " + dd + "일";
			today2 = yyyy+''+mm+''+dd;
			
			var easNum = '';
			if(title != '') {
				easNum = today2+'-'+title+'-'+'0';				
			}else {
				easNum = today2+'-미정-0';
			}
	        
	        $("#mainText_easTitle").text(title);
	        //$("#mainText_easNum").text(easNum);	//문서번호 ?
	        $("#mainText_easDate").text(today);

		});
        

		//발신명의자, 완료문서함 골라오기 위한 ajax -> 발신명의자 쿼리문 수정 필요
		var drafter = $('#drafter').val();	//기안자의 사원번호
		
		$.ajax({
			url: "selectReceiverAndDoc.ea",
			type:"post",
			async: false,
			data: {
				eNo:drafter
			},
			success:function(data){
				console.log(data);
				
				$('#receiverNameSelect').find('option').remove();
				//$('#receiverNameSelect').append("<option value='" + data.receivers + "'>" + data.receivers + "</option>");
				
				$("#completeDocListTitle").append("완료함 (" + data.docList.length + ")");
				$("#completeShareDocListTitle").append("공람완료 (" + data.docList.length + ")");
				
				for(var j=0; j<data.receivers.length; j++){
					
 					$('#receiverNameSelect').append("<option value='" + data.receivers[j].POS_NAME + "'>" + data.receivers[j].POS_NAME + "</option>");
				}
				
 				for(var i=0; i<data.docList.length; i++) {
					$('#completeDocList').append('<tr><td><input type="checkbox" class="comDocChk" value="' + data.docList[i].EAS_NO + '"></td><td>'
													+ data.docList[i].DOCNO  + '</td><td>'
													+ data.docList[i].DOC_TIT + '</td><td>'
													+ data.docList[i].EMP_NAME + '<input type="hidden" class="easDept" value="' 
													+ data.docList[i].DEPT_NAME + '"><input type="hidden" class="easDate" value="'
													+ data.docList[i].EDATE + '"></td><td>'
													+ data.docList[i].EDATE + '</td></tr>');
 					
 					$('#completeShareDocList').append('<tr><td><input type="checkbox"></td><td>'
														+ data.docList[i].DOCNO + '</td><td>'
														+ data.docList[i].DOC_TIT + '</td><td>'
														+ data.docList[i].EMP_NAME + '<input type="hidden" class="easDept" value="' 
														+ data.docList[i].DEPT_NAME + '"><input type="hidden" class="easDate" value="'
														+ data.docList[i].EDATE + '"></td><td>'
														+ data.docList[i].EDATE + '</td></tr>');
					
				} 
				
				
			},
			error:function(status){
				console.log(status);
			}			
		});


		
    });
    
  //탭 전환용 스크립트
    $(document).ready(function(){
    	
    	$('ul.tabs li').click(function(){
    		var tab_id = $(this).attr('data-tab');

    		$('ul.tabs li').removeClass('current');
    		$('.tab-content').removeClass('current');

    		$(this).addClass('current');
    		$("#"+tab_id).addClass('current');
    	})

    })
    
    //보안 설정용 스크립트
    $("#security_y").click(function(){
    	//$(".eas_information tr:nth-child(3) td").css("padding-bottom", "0");
    	$(".security_div").show();
    	$('#security_input').hide();
    	
    	$("#security_addr").click(function(){
    		$('.protectDetail').prop("checked", false);
    		$('#security_input').show();
    	})
    	$("#security_radio").click(function(){
    		$('#security_input').parent().parent().parent().find('.scdetail').remove();
    		console.log('2');
    		
    		$("#security_input").hide();
    	});
    	
    });
  	
    $("#security_n").click(function(){
    	$(".security_div").hide();
    	//$(".eas_information tr:nth-child(3) td").css("padding-bottom", "20px");
    });
    
    //보안설정 특정인 지정 시..
    $("#security_input").click(function(){
    	$("#protectPopOpen").trigger("click");
    	
    });
    
    //보안설정 특정인 지정 모달
    var spArr = Array();
   	function selectProtectPerson() {
   		
    	$.modal.close();

    	var eNo = $('#empDivNo5').val();
    	var eName = $("#empName5").text();
    	
    	//$('#security_input').hide();
    	$('#security_input').parent().parent().parent().append('<tr class="scdetail"><td class="sender_border"></td><td><span>' + eName +    														
    															'</span><input type="hidden" name="pt_eNo" value="' + eNo + '"><button class="btn_solid_pink deletePerson" type="button">삭제</button></td></tr>');
    	
    	spArr.push(eNo);
		$("#protectDetailPerson").val(spArr);
		console.log($("#protectDetailPerson").val());
		
		$("#personList tbody tr:not('.thead')").remove();
		console.log('3');
	}
    
    $(document).on('click', '.deletePerson', function(){
    	var eNo = $(this).parent().find('input[type="hidden"]').val();
    	spArr.splice(spArr.indexOf(eNo),1);
    	
    	$(this).parent().parent().remove();
    	console.log('4');
    })
   
	
	//결재경로 설정 모달 -> jsp로 값 가져오기
	function selectEasLine() {
		
		$('#easLineTbl tr:not(".tbl_main_tit")').remove();
		
		$.modal.close();
		
		var selectbox = $('.selectboxForProcess');

    	var $tr = $('#personList').children().find('tr').not('.thead');
    	
    	var arr1 = Array();
    	var arr2 = Array();
    	var arr3 = Array();
    	var aspArr = Array();
    	var asnArr = Array();
    	var agmpArr = Array();
    	var agmnArr = Array();

    	for(var i=0; i<$tr.length; i++) {
    		var index = $tr[i].children[0].innerHTML;
    		var process = '';   		    		
    		var position = $tr[i].children[2].innerHTML;
    		var name = $tr[i].children[4].innerHTML;
    		
    		if(selectbox.eq(i).val() == 'AS') {  		
    			process = '결재';
    			aspArr.push(position.substr(0,position.indexOf('/')));
    			asnArr.push(name.substr(0, name.indexOf('<')));
	    	}else if(selectbox.eq(i).val() == 'AGM') {
	    		process = '합의';
	    		agmpArr.push(position.substr(0,position.indexOf('/')));
    			agmnArr.push(name.substr(0, name.indexOf('<')));
	    	}else if(selectbox.eq(i).val() == 'CONFIRM'){
	    		process = '확인';
	    	}else if(selectbox.eq(i).val() == 'REFER'){
	    		process = '참조';
	    	}

    		var empNo = $('#personList').children().find('tr').not('.thead').eq(i).children().eq(4).children().val();

    		$('#easLineTbl').append("<tr><td>" + index 
    							  + "</td><td>" + process + "<input type='hidden' value='" + selectbox.eq(i).val() + "'>" 
    							  + "</td><td>" + position 
    							  + "</td><td>" + name + "</td></tr>");
     		
    		arr1.push(index);
    		arr2.push(selectbox.eq(i).val());
    		arr3.push(empNo);
    	}
    	
    	//본문 미리보기로 값을 넘기기 위한 코드
		for(var j=0; j<aspArr.length; j++) {
			$("#asPosition").siblings().eq(j+2).text(aspArr[j]);
			$("#asName").children().eq(j).text(asnArr[j]);
		}
		
		for(var k=0; k<agmpArr.length; k++) {

    		$("#agmPosition").siblings().eq(k+2).text(agmpArr[k]);
    		$("#agmName").children().eq(k).text(agmnArr[k]);

		}
   	
    	$("#easPathRank").val(arr1);
    	$("#easProcHow").val(arr2);
    	$("#easLineEmpNo").val(arr3);
    	
    }
	
	//수신자 설정 모달 -> jsp로 값 가져오기
	function selectReceiver() {
		
		$('#receiverTbl > tbody tr:not(".tbl_main_tit")').remove();
		console.log('6');
		
		$.modal.close();
		
		var eNo = $('#empDivNo').val();	//수신자의 사원 번호
		var senderName = $("#empName2").text();
		
		$('#receiverTbl tr td:nth-child(2)').text(senderName);
		$('#receiverTbl tr td:nth-child(2)').append('<input type="hidden" name="receiverName" value="' + eNo + '">');
		
		$("#mainText_senderName").text(senderName);
	}
	
	//공람자 설정 모달 -> jsp로 값 가져오기
	function selectShare() {

		$('#gongramTbl > tbody tr:not(".tbl_main_tit")').remove();
		console.log('7');
		
		$.modal.close();
		
		var $tr = $('#personList_share').children().find('tr').not('.thead');
		console.log($tr);
    	
		var shareArr = Array();
    	for(var i=0; i<$tr.length; i++) {
    		var dept = $tr[i].children[0].innerHTML;
    		var position = $tr[i].children[1].innerHTML;   		    		
    		var name = $tr[i].children[2].innerHTML;
    		var email = $tr[i].children[3].innerHTML;

    		$('#gongramTbl').append("<tr><td>" + dept + "</td><td>" + position + "</td><td>"
    								+ name + "</td><td>" + email + "</td></tr>");  								
     	
    		var shareEmpNo = $('#personList_share').children().find('tr').not('.thead').eq(i).children().eq(2).children().val();
    		shareArr.push(shareEmpNo);
    	}
    	console.log(shareArr);
    	$("#shareEmpNo").val(shareArr);
    	console.log($("#shareEmpNo").val());

	}
	
	//관련문서 선택 모달 -> jsp로 값 가져오기
	function relDocAdd() {			
		$.modal.close();
		
		//완료 리스트 ... . . ... . . . . . ...
		var tr_array = Array();
		var send_array = Array();
	    var send_cnt = 0;
	    var chkbox = $('.comDocChk');
	    
	    for(var i=0; i<chkbox.length; i++) {
	    	if(chkbox[i].checked == true) {
	    		send_array[send_cnt] = chkbox[i].value;
	    		send_cnt++;
	    	}
	    }		   	
	    $("#comDocList").val(send_array);
	    
	    console.log($("#comDocList").val());	//--> DB에 전달하는 값 	
	    var relDocNum = $("#comDocList").val();
	    
	    $.ajax({
	    	url: "selectRelDocList.ea",
	    	type: "post",
	    	data: {
	    		relDocNum:relDocNum
	    	},
	    	success: function(data) {
	    		console.log(data);
	    		
	    		$(".non_doc").hide();
	    		
	    		for(var i=0; i<data.relDocList.length; i++) {
	    			$('#relDocTable').append("<tr><td>" + data.relDocList[i].DOCNO + "</td><td>"
	    									+ data.relDocList[i].DOC_TIT + "</td><td>"
	    									+ data.relDocList[i].EMP_NAME + "</td><td>"
	    									+ data.relDocList[i].DEPT_NAME + "</td><td>"
	    									+ data.relDocList[i].EDATE + "</td><td>"
	    									+ "<button class='btn_solid_pink relDocDelete' type='button'>삭제</button>");
	    			
	    			
	    		}
	    		
	    		
	    	},
	    	error: function(status) {
	    		console.log(status);	
	    	}   	
	    })		
	}
	
	$(document).on('click', '.relDocDelete', function(){
		$(this).parent().parent().remove();
		console.log('8');

 		if($("#relDocTable tr").length == 2) {
			$(".non_doc").show();
		}
	})
	
	$(document).on('click', '.attachDelete', function(){
		$(this).parent().parent().remove();
		console.log($("#attachTbl tr").length);

 		if($("#attachTbl tr").length <= 2) {
			$(".non_file").show();
		}
	});
	
    //새 결재 기안 확인버튼시 DB에 insert
	function submitNewEas() {
    	var btn = "${ param.btn }";
    	//var formatNo = "${ eas.FORMAT_NO }";
		if($('#limit').prop('checked')) {
			
			//열람제한일 넘기기
			var date1 = $("#datepicker").val().split("/");
	    	$("#hiddenDate").val(date1[0] + "-" + date1[1] + "-" + date1[2]); 
			
			//보안상세 값 넘기기
			var send_array = Array();
		    var send_cnt = 0;
		    var chkbox = $('.protectDetail');
		    
		    for(var i=0; i<chkbox.length; i++) {
		    	if(chkbox[i].checked == true) {
		    		send_array[send_cnt] = chkbox[i].value;
		    		send_cnt++;
		    	}
		    }		   	
		    $("#protectYnDetail").val(send_array);  
		    
		    //발신명의자 넘기기
		    var senderName = $("#receiverNameSelect option:selected").val();	    
		    $("#senderName").val(senderName);
		    
		  	//본문 내용 넘기기
		   	var content = $('#editer').html();
			var test = $('#editer').text();

		    if(test != "") {
		    	$("#easContent").val(content);
		    	
		    	console.log(btn);
		    	
		    	if(btn!=''){		    		
		    		$("#newEasForm").submit();
		    	}else {
				    $("#newEasForm").submit();	
		    	}
		    	
		    }else {
		    	alert('본문 내용 입력은 필수사항입니다.');
		    }			
			
		}else if($('#nolimit').prop('checked')){
			
			//제한 없을 때는 과거의 날짜를 넘김..
			var date1 = ['1993', '01', '01'];
	    	$("#hiddenDate").val(date1[0] + "-" + date1[1] + "-" + date1[2]);
			
			//보안상세 값 넘기기
			var send_array = Array();
		    var send_cnt = 0;
		    var chkbox = $('.protectDetail');
		    
		    for(var i=0; i<chkbox.length; i++) {
		    	if(chkbox[i].checked == true) {
		    		send_array[send_cnt] = chkbox[i].value;
		    		send_cnt++;
		    	}
		    }
		    
		    $("#protectYnDetail").val(send_array);  
		    
		    //발신명의자 넘기기
		    var senderName = $("#receiverNameSelect option:selected").val();		    
		    $("#senderName").val(senderName);
		    
		  	//본문 내용 넘기기
		   	var content = $('#editer').html();
			var test = $('#editer').text();

		    if(test != "") {
		    	$("#easContent").val(content);
				
				if(btn!=''){
		    		$("#newEasForm").submit();
		    	}else {
				    $("#newEasForm").submit();	
		    	}

		    }else {
		    	alert('본문 내용 입력은 필수사항입니다.');
		    }
		    
		}		
	}   
</script>


</body>
</html>