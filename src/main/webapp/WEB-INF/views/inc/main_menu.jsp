<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="./header.jsp" />

<link rel="stylesheet" href="${ contextPath }/resources/asset/jqx.base.css" type="text/css" />
<script type="text/javascript" src="${ contextPath }/resources/asset/jqxcore.js"></script>
<script type="text/javascript" src="${ contextPath }/resources/asset/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="${ contextPath }/resources/asset/jqxcalendar.js"></script>
<script type="text/javascript" src="${ contextPath }/resources/asset/globalize.js"></script>
<!-- <script type="text/javascript" src="${ contextPath }/resources/asset/jqxtooltip.js"></script> -->
 <main id="container" class="main_page">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="now_time_wrap">
                    <div id="main_now_time" class="now_time"></div>
                </div>
                <div class="non_notice_wrap clearfix">
	                <a href="selectUnreadNotice.no">
	                    <div class="non_notice_tit">안 읽은 알림</div>
	                    <div class="non_notice_cnt">${ nonReadAlramCount }개</div>
	                </a>
                </div>
                <div class="cal_wrap">
                    <div id='main_menu_cal'></div>
                </div>
            </div>
        </div><!-- inner_lt end -->