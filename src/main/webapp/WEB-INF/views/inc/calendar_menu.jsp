<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="./header.jsp" />

<link rel="stylesheet" href="${ contextPath }/resources/asset/jqx.base.css" type="text/css" />
<script type="text/javascript" src="${ contextPath }/resources/asset/jqxcore.js"></script>
<script type="text/javascript" src="${ contextPath }/resources/asset/jqxdatetimeinput.js"></script>
<script type="text/javascript" src="${ contextPath }/resources/asset/jqxcalendar.js"></script>
<script type="text/javascript" src="${ contextPath }/resources/asset/globalize.js"></script>
<!-- <script type="text/javascript" src="${ contextPath }/resources/asset/jqxtooltip.js"></script> -->
 <main id="container" class="calc_page">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="menu_btn_wrap">
                    <button class="btn_solid" id="newCalBtn">새 일정 등록</button>
                </div>
                <div class="cal_wrap">
                    <div id='main_menu_cal'></div>
                </div>
            </div>
        </div><!-- inner_lt end -->
        
<div id="calNewSchPop" class="modal">
    <jsp:include page="../pop/calc_new_sch_pop.jsp" />
</div>

<script>
	$("#newCalBtn").on("click", function(){
		if(admin != "ADMIN"){

			$("#calNewSchPop").modal();
			$("#calcNewTit").text("새 일정 등록");	
		}else{
			alert("관리자 계정은 일정을 등록할 수 없습니다.");
		}
	});

</script>