<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="./header.jsp" />

<main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="menu_btn_wrap">
                    <td><a href="#newEas" rel="modal:open" class="button btn_solid" onclick="selectFormatList();">새 결재 진행</a></td>
                </div>
                <div id="menu_area" class="tree_shape">
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list add">
                            <a href="#">내 결재</a>
                            <ul class="sub_menu_ctn">
                                <li class="sub_menu_list">
                                    <a href="selectEasList.ea?menu=myWait">결재대기</a>
                                </li>                              
                                <li class="sub_menu_list">
                                	<a href="selectEasList.ea?menu=myProc">결재진행</a>
                                </li>
                                <li class="sub_menu_list">
                                	<a href="selectEasList.ea?menu=myRe">결재반려</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu_list add">
                            <a href="#">참여 결재</a>
                            <ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="selectEasList.ea?menu=easWait">결재대기</a></li>
                                <li class="sub_menu_list"><a href="selectEasList.ea?menu=easProc">결재진행</a></li>
                                <li class="sub_menu_list"><a href="selectEasList.ea?menu=easRe">결재반려</a></li>
                            </ul>
                        </li>
                        <li class="menu_list add">
                            <a href="#">공람</a>
                            <ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="selectEasList.ea?menu=pubWait">공람대기</a></li>
                                <li class="sub_menu_list"><a href="selectEasList.ea?menu=pubComp">공람완료</a></li>
                                <li class="sub_menu_list"><a href="selectEasList.ea?menu=pubSend">보낸공람</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->
        
<!-- 새 결재 진행 popup include -->
<div id="newEas" class="modal">
	<jsp:include page="../pop/new_eas_pop.jsp" />
</div>
<!-- 서식별 결재선 지정 popup include -->
<div id="myEasLine" class="modal">
	<jsp:include page="../pop/new_eas_pop2.jsp" />
</div>


<script>
	function selectFormatList() {
		$.ajax({
			url:"selectFormatList.ea",
			type:"post",
			data:{},
			async: false,
			success:function(data){
				//console.log(data);				
				
				var depth = "";
				for(var i=0; i<data.docPath.length; i++) {
					depth += "<li class='folder depth1'>" + data.docPath[i].pathName + "<ul>";	
					
					for(var j=0; j<data.formatList.length; j++) {						
						if(data.formatList[j].pathNo == data.docPath[i].pathNo) {
								depth += ("<li>" + data.formatList[j].formatName + "<input type='hidden' class='formatNo' value='" + data.formatList[j].formatNo + "'>" + "</li>");
						}
					 }
				 
				 	if(data.docPath[i].pathOrderList.length != 0) {
				 			for(var l=0; l<data.docPath[i].pathOrderList.length; l++) {
				 				depth += ("<li class='folder depth2'>" + data.docPath[i].pathOrderList[l].pathName);
				 				for(var k=0; k<data.formatList.length; k++) {
					 				
					 				if(data.formatList[k].pathNo == data.docPath[i].pathOrderList[l].pathNo) {
					 					depth += ("<ul><li>" + data.formatList[k].formatName
					 							+ "<input type='hidden' value='" + data.formatList[k].formatNo + "'>" + "</li></ul>");
					 				}
				 				}
				 				depth += "</li>";
				 			}
				 		
				 	}
					depth += "</ul></li>";
				}
				//console.log(depth);

				testAjax(data, depth);
			}, 
			error:function(status){
				console.log(status);
			}
		});
	}
</script>