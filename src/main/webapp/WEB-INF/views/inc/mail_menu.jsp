<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="./header.jsp" />
 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
             <div class="menu_btn_wrap">
                    <button class="btn_solid" onclick="location.href='writemail.ma'">메일 쓰기</button>
                </div>
                <div id="menu_area">
                    <div class="menu_area_tit">메일</div>
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list"><a href="selectMail.ma">받은메일함</a></li>
                        <li class="menu_list"><a href="selectMail.ma?type=send">보낸메일함</a></li>
                        <li class="menu_list"><a href="selectMail.ma?type=delete">휴지통</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->