<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="./header.jsp" />

<!-- tree api 관련 시작 -->
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<!-- tree api 관련 끝 -->

<script type="text/javascript">
    $(function(){
        $("#deptTree").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                } 
            },
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });
        $("#teamTree").fancytree({
            imagePath: "skin-custom/",
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });
        $("#myTree").fancytree({
            imagePath: "skin-custom/",
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });

        $(".fancytree-container").addClass("fancytree-connectors");
        
    });
  </script>

<main id="container" class="address_page"> 
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="menu_btn_wrap">
                    <a href="#addrNewPop" class="button btn_solid" rel="modal:open" class="open_modal">연락처 추가</a>
                </div>
				<!-- 주소록 메뉴 시작 -->
				<div class="tree_menu_wrap">
		             <div id="deptTree" class="tree_menu">
		                 <ul>
		                     <li class="folder" id="deptTit">사내주소록<input type="hidden" value="0">
		                  		<ul>
			                       <c:forEach var="list" items="${ deptList }">
		                       			<li class="folder dept_folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
		                       				<c:if test="${ list.deptOrderList.size() != 0 }">
		                       					<ul>
		                     						<c:forEach var="list2" items="${ list.deptOrderList }">
		                       							<li class="folder dept_folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
						                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
						                       					<ul>
						                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
						                       							<li class="folder dept_folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
						                       							</li>
						                       						</c:forEach>
						                       					</ul>
						                       				</c:if>
		                       							</li>
		                       						</c:forEach>
		                       					</ul>
		                       				</c:if>
		                       			</li>
			                       </c:forEach>
			                 	</ul>
		                     </li>
		                 </ul>
		             </div>
		             <c:if test="${ !sessionScope.loginUser.auth.equals('ADMIN') }">
		             <div id="teamTree" class="tree_menu">
		                 <ul>
		                     <li class="folder" id="teamTit">공용주소록<input type="hidden" value="0">
		                         <ul>
			                       <c:forEach var="list" items="${ teamGrpList }">
		                       			<li class="folder team_folder"><c:out value="${ list.addrgroup }" /><input type="hidden" value="${ list.addrNo }"></li>
			                       </c:forEach>
		                         </ul>
		                     </li>
		                 </ul>
		             </div>
		             <div id="myTree" class="tree_menu">
		                 <ul>
		                     <li class="folder" id="myTit">내주소록<input type="hidden" value="0">
		                         <ul>
			                       <c:forEach var="list" items="${ empGrpList }">
		                       			<li class="folder my_folder"><c:out value="${ list.addrgroup }" /><input type="hidden" value="${ list.addrNo }"></li>
			                       </c:forEach>
		                         </ul>
		                     </li>
		                 </ul>
		             </div>
		             </c:if>
		         </div>
		     </div>
		 </div><!-- inner_lt end -->
<div id="addrNewPop" class="modal">
<jsp:include page="../pop/addr_new_pop.jsp" />
</div>
<!-- 주소록 메뉴 끝 -->
                