<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="./header.jsp" />

 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div id="menu_area">
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list"><a href="selectDocList.ea?eno=${ sessionScope.loginUser.empDivNo }&dno=${ sessionScope.loginUser.deptNo }&menu=my">내 결재 완료</a></li>
                        <li class="menu_list"><a href="selectDocList.ea?eno=${ sessionScope.loginUser.empDivNo }&dno=${ sessionScope.loginUser.deptNo }&menu=eas">참여 결재 완료</a></li>
                        <li class="menu_list"><a href="selectDocList.ea?eno=${ sessionScope.loginUser.empDivNo }&dno=${ sessionScope.loginUser.deptNo }&menu=rece">수신 결재 완료</a></li>
                        <li class="menu_list"><a href="selectDocList.ea?eno=${ sessionScope.loginUser.empDivNo }&dno=${ sessionScope.loginUser.deptNo }&menu=dept">부서 결재 완료</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->