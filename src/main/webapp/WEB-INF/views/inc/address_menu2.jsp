<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="./header.jsp" />

 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div id="menu_area">
                    <div class="menu_area_tit">
                    	<c:if test="${ type.equals('team') }">
	                    	공용 주소록
                    	</c:if>
                    	<c:if test="${ type.equals('my') }">
	                    	내 주소록
                    	</c:if>
                    </div>
                </div>
            </div>
        </div><!-- inner_lt end -->