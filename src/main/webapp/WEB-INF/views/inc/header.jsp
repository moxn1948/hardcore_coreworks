<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="contextPath" value="${ pageContext.servletContext.contextPath }" scope="application"/>
<c:if test="${ loginUser eq null }">
   <script>
      alert("세션이 만료되었습니다. 다시 로그인 해주세요.")
      location.href="index.jsp";
   </script>
</c:if>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="UTF-8">
<title>COREWORKS</title>
<!-- modal cdn -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
<!-- 공통 css --> 
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/common.css">
<!-- jquery cdn -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<!-- font awesome cdn -->
<script src="https://kit.fontawesome.com/ccbb1cc624.js" crossorigin="anonymous"></script>
<!-- jQuery Modal cdn -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
<link rel="icon" type="image/png"  href="cw/favicon.ico"/>
</head>
<body>
<c:set var="url" value="${ fn:split(pageContext.request.requestURL, '/') }"/> 

<c:if test="${ url[fn:length(url) - 1] ne 'mail_detail.jsp' }">
  	<jsp:include page="../chat/chat.jsp"/>
</c:if>
    <div class="wrap">
        <header id="header">
            <div class="full_ct">
                <div class="inner_lt">
                    <h1 class="logo_wrap">
                        <a href="main.main?eno=${ sessionScope.loginUser.empDivNo }&auth=${ sessionScope.loginUser.auth }&dno=${ sessionScope.loginUser.deptNo }"><img src="${ contextPath }/resources/images/logo_2.png" alt="corework 로고" class="logo"></a>
                    </h1>
                </div><!-- inner_lt end -->
                <div class="inner_rt">
                    <div class="nav_wrap">
                        <nav id="nav">
                            <ul class="nav_ctn clearfix">
                                <li class="nav_list"><a href="selectMail.ma">메일</a></li>
                                <li class="nav_list"><a href="selectEasList.ea?menu=myWait">전자결재</a></li>
                                <li class="nav_list"><a href="selectDocList.ea?eno=${ sessionScope.loginUser.empDivNo }&dno=${ sessionScope.loginUser.deptNo }&menu=my">문서함</a></li>                              
                                <li class="nav_list">
                                     <a href="addrMain.ad">주소록</a>
                                </li>
                                <li class="nav_list">
                                    <a href="calcMain.ca">일정</a>
                                </li>
                                <li class="nav_list"><a href="comList.bo">게시판</a></li>
                                <c:if test="${ auth ne null }">
                                <li class="nav_list"><a href="selectEmpList.em">사원관리</a></li>
                                <li class="nav_list"><a href="selectVacList.cm">회사관리</a></li>
                                </c:if>
                            </ul>
                        </nav>
                    </div><!-- nav_wrap end -->
                    <div class="gnb_wrap">
                        <div class="gnb_ctn notice_wrap">
                            <!-- 알림 -->
                            <a href="selectUnreadNotice.no" class="notice_link">
                                <i class="far fa-bell"></i>
                            </a>
                        </div>
                        <div class="gnb_ctn logout_wrap">
                            <!-- 로그아웃 -->
                            <a href="logout.me" class="logout_link">
                                <i class="fas fa-power-off"></i>
                            </a></div>
                        <div class="gnb_ctn user_wrap"><a href="absList.mp" class="clearfix">
                          	<c:if test="${ sessionScope.loginUser.auth.equals('ADMIN') }">
                               <div class="info_wrap">
                                   <p class="info_ctn">${ loginUser.empName }</p>
                                   <p class="info_ctn">관리자계정</p>
                               </div>
                           </c:if>
                           <c:if test="${ !sessionScope.loginUser.auth.equals('ADMIN') }">
                               <div class="info_wrap">
                                   <p class="info_ctn">${ loginUser.empName } ${ loginUser.jobName }</p>
                                   <p class="info_ctn">${ loginUser.deptName }(${ loginUser.posName })</p>
                               </div>
                           </c:if>
                            <!-- 사용자 이미지 없을때 -->
                            <c:if test="${ loginUser.changeName eq null }">
                                 <div class="user_img_wrap user_img_none">
                                   <i class="fas fa-user-circle"></i>
                               </div>
                            </c:if>
                            <!-- 사용자 이미지 있을때 -->
                            <c:if test="${ loginUser.changeName ne null }">
                               <div class="user_img_wrap user_img_has">
                                   <img src="${contextPath}/resources/uploadFiles/${ loginUser.changeName }" alt="">
                               </div>
                            </c:if>
                        </a></div>
                    </div><!-- user_wrap end -->
                </div><!-- inner_rt end -->
            </div>
        </header>
