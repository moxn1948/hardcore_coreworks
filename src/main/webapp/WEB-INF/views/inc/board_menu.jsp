<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="./header.jsp" />
 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
            	<c:if test="${ sessionScope.loginUser.auth ne 'ADMIN' }">
	                <div class="menu_btn_wrap">
	                    <button class="btn_solid new_board" onclick="location.href='newBoard.bo';">글쓰기</button>
	                </div>
                </c:if>
                <div id="menu_area">
                   <ul class="menu_ctn clearfix head_menu" >
                      <li class="menu_list must_read_list"><a href="mustRead.bo" style="font-size: 14px;"><i class="far fa-check-circle"></i><br>필독</a></li>
                      <li class="menu_list my_board"><a href="myBoard.bo" style="font-size: 14px;">MY<br>내가 쓴 글</a></li>
                   </ul>
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list"><a href="comList.bo">전체 공지사항</a></li>
                        <c:if test="${ sessionScope.loginUser.auth ne 'ADMIN' }">
                        	<li class="menu_list"><a href="deptList.bo">부서 공지사항</a></li>
                        </c:if>
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->