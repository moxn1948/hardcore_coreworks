<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
           
            <form action="updateDocPath.cm" id="updateDocPath" method="post">
                <div class="main_ctn">
	
					<button class="btn_blue save" id="submitBtn1">저장</button>
					<a href="#" class="button btn_blue add" onclick="topAdd(this)" style="float: right">추가</a>
	
                    <div class="menu_tit"><h2>기안 서식관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn formtb">
                                <tr class="tbl_main_tit">
                                    <th>상위폴더</th>
                                    <th>하위폴더명</th>
                                    <th>하위폴더 추가</th>
                                    <th>순서변경</th>
                                    <th>수정/삭제</th>
                                </tr>
                                
					
			   <c:forEach var="list" items="${ docList }">
					<c:if test="${ list.docOrderList.size() == 0 }">
					<tr class="topform">
						<td>
						<input type="hidden" class="pathNoList" name="pathNoList"  value="<c:out value='${ list.pathNo }'/>">
						<input type="hidden" class="delformNo" name="delformNo">
						<input class="topDocPathName" name='topDocPathName' value="<c:out value='${ list.pathName}' />">
						</td>						
						<td>
						<input type="hidden" class="pathOrder" name="pathOrder" value="0">
						<input type="hidden" class="subDocPathName" name="subDocPathName" value="-1">
						</td>
						
						<td><a href='#'><button type="button"class='btn_main_plus' onclick='subAdd(this)'>+</button></a></td>
						<td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' type='button' onclick='moveDown(this)'>▼</button></a></td>
						<td><button type='button' class='btn_pink' onclick='del(this)'>삭제</button></td>								
					</tr>
					</c:if>
					<c:if test="${ list.docOrderList.size() != 0 }">
						
					<tr class="topform">
						<td>
						<input type="hidden" class="pathNoList" name="pathNoList"  value="<c:out value='${ list.pathNo }'/>">
						<input type="hidden" class="formName" name="formName">
						<input class="topDocPathName" name='topDocPathName' value="<c:out value='${ list.pathName}' />">
						</td>
						<td>
						<input type="hidden" class="subDocPathName" name="subDocPathName" value="-1">
						<input type="hidden" class="pathOrder" name="pathOrder" value="0">
						</td>
						
						<td><a href='#'><button type="button"class='btn_main_plus' onclick='subAdd(this)'>+</button></a></td>
						<td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' type='button' onclick='moveDown(this)'>▼</button></a></td>
						<td><button type='button' class='btn_pink' onclick='del(this)'>삭제</button></td>								
					</tr>
					
					
							<c:forEach var="list2" items="${ list.docOrderList }">
				   		<tr class='subform'>	
				   		<td class="arrowImg"> 
				   		<input type="hidden" class="topDocPathName" name="topDocPathName" value="-1">	
				   		<div class="arrowImg">
				   		<img src='${contextPath}/resources/images/Arrow.jpg'>
				   		</div>
				   		</td>
				   		<td>
				   		<input type='hidden' name='subOrder' class='subOrder'>
				   		<input type='hidden' name='pathOrder' class='pathOrder' value="<c:out value='${ list2.pathOrder}'/>" >
				   		<input type='hidden' name='subName' class='subName'>
				   		<input type='text' name='subDocPathName' class='subDocPathName' value="<c:out value='${ list2.pathName}'/>"style='text-align:center;'>
				   		<input type="hidden" name="pathNoList" class="pathNoList" value="<c:out value='${ list2.pathNo}'/>" >
				   		<input type="hidden" name="subformOrder" class="subformOrder" value="<c:out value='${ list2.pathOrder}'/>" >
				   		<input type="hidden" name="subdelNo" class="subdelNo">
				   		</td>
				   		<td>
				   		</td>
				   		<td>
				   		</td>
				   		<td>
				   		<button type='button' class='btn_pink' onclick="subdel(this)">삭제</button>
				   		</td>
			   		</tr>
							</c:forEach>
			   			
						</c:if>
				</c:forEach> 
						 	</table>
                    </div>
                     
                          
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                   
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            		</div>
           		  </form>
		<!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
var i = 1;
var fOrder = 0;
var idx = 0;

    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
       // $("#menu_area .menu_list").eq(1).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        
        
        
        
        
       /*   $(document).on("click", ".btn_main_plus", function(){
        	var str2= "<tr class='subform'><td><a href='#' style='text-align:center; display: inline-block;'><img src='${contextPath}/resources/images/Arrow.jpg'></a></td><td><a href='#'><input type='text' name='subform' style='text-align:center;'></input></a></td><td></td><td><a href='#'></a></td><td><button class='btn_white'>삭제</button></td></tr>"
 			$(this).parents("tr").after(str2);
        	//$(this).parents("tr").css({backgroundColor : "red"});
        	//console.log($(this).parents("tr"));	
        });  */
        
        
        fOrder = ($(".formtb").find("tr:last-child()").index());
    	idx = $(".topform").find($(".formNo:last")).val();
    	console.log()
        
    });
    function topAdd(t) {	
    	idx++;
    	console.log(idx)
    	var str = "<tr class='topform'><td><input type='hidden' name='pathOrder' class='pathOrder' value='0'><input type='hidden' name='pathNoList' class='pathNoList' value='0'><input type='hidden' name='formTop' class='formTop'><input type='text' name='topDocPathName' style='text-align:center;'></input></td><td><input type='hidden' class='subDocPathName' name='subDocPathName' value='-1'></td><td><button type='button' class='btn_main_plus' onclick='subAdd(this)'>+</button></td><td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button type='button' class='btn_main_down' onclick='moveDown(this)'>▼</button></a></td><td><button class='btn_pink' onclick='subdel(this)'>삭제</button></td></tr>";
    	$(str).appendTo($(".tbl_ctn"));
    }
    
     function subAdd(d) {
    	var count = 0;
    	var $tr = $(d).closest('tr'); 
    	//var idx = $tr.index();
    	var No = $tr.index();
    	var rank2 = $tr.children().eq(0).children().val()
    	console.log(rank2)
    	if(rank2 == 0) {
    		rank2 = '-1';
    	}
    	var str2= "<tr class='subform'><input type='hidden' name='pathNoList' class='pathNoList' value='0'><input type='hidden' name='subformOrder' class='subformOrder' value="+ rank2 +"><input type='hidden' name='subRank2' class='subRank2'><input type='hidden' name='subf' class='subf'><td><input type='hidden' class='pathOrder' name='pathOrder' value="+ rank2 +"><input type='hidden' class='topDocPathName' name='topDocPathName' value='-1'><a href='#' style='text-align:center; display: inline-block;'><img src='${contextPath}/resources/images/Arrow.jpg'></a></td><td><input type='text' name='subDocPathName' style='text-align:center;'></input></td><td></td><td><a href='#'></a></td><td><button type='button' class='btn_pink'>삭제</button></td></tr>";
    	//idx = ($(".formtb").find("tr:last-child()").index());
    	if(!$tr.next().hasClass('subform')) {
    		$(str2).insertAfter($tr)
    		
    	}else if($tr.next().hasClass('subform')){
    		for(var i = No; true; i++) {	    	
    			if($tr.parents('tbody').children().eq(i + 1).hasClass('subform')) {
    			count++;
    			}
    			else {  	
    			break;
    			}
    		}
    			for(var i = No; true; i++) {
    				if($tr.parents('tbody').children().eq(i +No + 1).hasClass('subfo')) {
    				}else {
  	 					$tr.parents('tbody').children().eq(No +count).after($(str2))   					
    				}	
    				break;
    			}
    	}
    }
    $("#submitBtn1").click(function() {
     	var top = $("[name=topDocPathName]");
    	var rank = $("[name=subformOrder]");
    	var sub = $("[name=subDocPathName]");
    	var no = $("[name=formNo]")
		
    	no.each(function() {
    		var no2 = $(this).val()
    	})
    	
    	var tf = "";
    	var fr2 = "";
    	var sf = "";
    	top.each(function() {
    		var topForm = $(this).val()
    		if(tf == "") {
    			tf = topForm
    		}else {
    			tf = tf + ", " + topForm
    		}
    		$(".formName").val(tf)
    	})
    	
    	rank.each(function() {
    		var formRank2 = $(this).val()
    		
    		if(fr2 == "") {
    			fr2 = formRank2
    		}else {
    			fr2 = fr2 + ", " + formRank2
    		}
    		$(".subOrder").val(fr2);
    })
    	 sub.each(function() {
    		var subForm = $(this).val()
    		if(sf == "") {
    			sf = subForm
    		}else {
    			sf = sf + ", " + subForm
    		}
    		$(".subName").val(sf);
    	})   
    	
    	
    		
    
    });	
    
    function del(d) {
   	 var topNo = "";
    	str = d.closest('tr')
    	var $tr = $(d).closest('tr');
    	var formNo =  $tr.find($(".pathNoList")).val();
    	var idx = $tr.index();
    	var count = 0;
    	
    	 $.ajax({
		url:"selectFormCount.cm",
		type:"get",
		data:{formNo: formNo},
		success:function(data) {
			count = data.formCount
    		console.log("count : " + count)
    		
    	if($tr.next().hasClass('subform')) {
			alert('하위폴더가 존재합니다.')
    	}else if(count >= 1){
    		alert('폴더안에 서식이 존재합니다.')
    	}else{
    		$tr.remove()
    	}
		
			
		}, 
		error:function(data) {
			console.log(data)
		}
	
		})  
    	
		
    	
    	
    	if(topNo == "") {
    		topNo = formNo;
    	}else {
    		topNo = topNo + ", " + formNo
    	}
    	$(".delformNo").val(topNo)
    	
    	//str.remove();
    	$("#submitBtn").click(function(){
    		
    })
    	
   }
    var subNo = "";
    function subdel(d) {
    	str = d.closest('tr')
    	var $tr = $(d).closest('tr');
    	
    	var formNo = $tr.find($(".pathNoList")).val();
    	var count = 0;
    	
    	$.ajax({
    		url:"selectFormCount.cm",
    		type:"get",
    		data:{formNo: formNo},
    		success:function(data) {
    			count = data.formCount
        		console.log("count : " + count)
        		
         	if(count >= 1){
        		alert('폴더안에 서식이 존재합니다.')
        	}else{
			        if(subNo == "") {
			    	subNo = formNo;
			    		
			    	}else {
			    		subNo = subNo + ", " + formNo
			    	}
			    	$(".subdelNo").val(subNo)
			    	console.log($(".subdelNo").val())
        			$tr.remove();
			    	$("#submitBtn").click(function(){
			    })
        	}
    		
    			
    		}, 
    		error:function(data) {
    			console.log(data)
    		}
    	
    		})
    	
    	
    }
    
    
    $('.table1 button:even').bind('click', function(){ moveUp(this) });
    $('.table1 button:odd').bind('click', function(){ moveDown(this) });
    
    function moveUp(el){
    	var count2 = 0;
		var count = 0;
		var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
    	var idx = $tr.index();
		var s = $tr.prev().children().eq(0).children().val();//전에꺼
		var spo = $tr.prev().children().eq(0).children();//전에꺼 위치
		var tagName = $tr.prev().attr('class'); //전에꺼 인풋태그 이름
		var idx2= $(".formtb").find("tr:last-child()").index();
		var hc = $tr.prev().hasClass('topform')
		var sf = $tr.prev().hasClass('subform')
		
		if(idx > 1){
		
		if(hc && !$tr.next().hasClass('subform')) {
			$tr.prev().before($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
			
				//선택한 클래스가 topform이고  선택한 tr 이전 클래스가 subform이고 선택한 tr의 다음클래스가 subform이 아닐면
		}else if($tr.hasClass('topform') && sf && !$tr.next().hasClass('subform')) {
			for(var i = idx; true; i--) {
				// 선택한tr의 위에 있는 tr의 클래스가 subform이면
				if($tr.parents('tbody').children().eq(i - 1).hasClass('subform')) {		
					//아니면
				}else {
					//선택한 tr위에 있는 tr
					$tr.parents('tbody').children().eq(i - 1).before($tr)
					break;
				}

			}
			
					//선택한 tr의 클래스가 topform 이고 선택한 다음 tr의 클래스가 subform이고 선택한 이전 tr의 클래스가 subform
		 }else if($tr.hasClass('topform') && $tr.next().hasClass('subform') && $tr.prev().hasClass('subform')) {
			for(var i = idx; true; i--) {
				if($tr.parents('tbody').children().eq(i - 1).hasClass('subform')) {
					count++;
				}
				else {
					break;
				}
				
				} 
			for(var i = idx; true; i++) {
				if($tr.parents('tbody').children().eq(i + 1).hasClass('subform')) {	
					count2++;
				}else {
					break;
				}
			}
			for( var i = idx; true; i++) {
				if($tr.parents('tbody').children().eq(i - 1).hasClass('subform')) {
					
				}else {		
					console.log( $tr.parents('tbody').children().eq(i-count-2))
					$tr.parents('tbody').children().eq(i-count-2).before($tr)
					for(var j = idx + 1; j <= idx + count2; j++) {
						$tr.after($tr.parents('tbody').children().eq(j))
					}
					count2 = 0;
					break;
				}
			} 
			
			
			//선택한 tr의 클래스가 topform이고 선택한 다음 클래스가 subform
		}else if($tr.hasClass('topform') && $tr.next().hasClass('subform')) {
			for(var i = idx; true; i++) {
				if($tr.parents('tbody').children().eq(i + 1).hasClass('subform')) {	
					count++;
				}else {
					break;
				}
			}
			
			for(var i = idx; true; i--) {
				if($tr.parents('tbody').children().eq(i - 1).hasClass('subform')) {		
				}else {
					$tr.parents('tbody').children().eq(i - 1).before($tr)
					for(var j = idx + 1; j <= idx + count; j++) {
						
						$tr.after($tr.parents('tbody').children().eq(j))
					}
					count = 0;
					break;
				}
				
			}
		}
		
			
		}
			
		}
	
	/* function moveDown(el) {
		var count2 = 0;
		var count = 0;
		var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
    	var idx = $tr.index();
		var s = $tr.prev().children().eq(0).children().val();//전에꺼
		var spo = $tr.prev().children().eq(0).children();//전에꺼 위치
		var tagName = $tr.prev().attr('class'); //전에꺼 인풋태그 이름
		var idx2= $(".formtb").find("tr:last-child()").index();
				
		 if(idx >= 1 && idx < idx2){
		
		if($tr.next().hasClass('topform') && !$tr.next().next().hasClass('subform')) {
			$tr.next().after($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
			
				
		}
				//선택한 tr의 클래스가 topform이고 tr의 다음 클래스가 subform이고 선택한 tr의 다다음 클래스가 subform이 아닐때
		else if($tr.hasClass('topform') && $tr.next().hasClass('subform')){
			for(var i = idx; true; i++) {
				if($tr.parents('tbody').children().eq(i + 1).hasClass('subform')) {
					count++;	
				}else {
		
					break;
				}
			}
						  
			 for(var i = idx; i < idx + count; i++) {
					if($tr.parents('tbody').children().eq(i + 1).hasClass('subform')) {
						
						$tr.parents('tbody').children().eq(i + count + 1).after($tr)
						for(var j = idx + 1; j < idx + count; j++) {
							$tr.after($tr.parents('tbody').children().eq(j))
						}
					}else {
						break;
					} 
					
				} 
						
			
			}
			
			for( var i = idx; true; i++) {
				if($tr.parents('tbody').children().eq(i - 1).hasClass('subform')) {
					
				}else {					
					$tr.parents('tbody').children().eq(i-count-2).before($tr)
					for(var j = idx + 1; j <= idx + count2; j++) {
						$tr.after($tr.parents('tbody').children().eq(j));
					}
					count2 = 0;
					break;
				}
			} 
			
		} */
		 /* else if($tr.next().next().hasClass('subform') && !$tr.next().hasClass('subform')) {
				for(var i = idx; true; i++) {
					if($tr.parents('tbody').children().eq(i+2).hasClass('subform')) {
						count++
					}else {				
						break;
					}
				}
					for(var i = idx; true; i++) {
						if($tr.parents('tbody').children().eq(i).hasClass('subform')) {
							
							break;
						}else {
							console.log($tr.parents('tbody').children().eq(i + count +1))
							$tr.parents('tbody').children().eq(i + count +1).after($tr)
						}
						count = 0;
						break;
					} 
					
					
						
			} */
		
		/* else if($tr.next().hasClass('subform')) {
				for(var i = idx + 1; i < idx2; i++) {
					if($tr.parents('tbody').children().eq(i + 1).hasClass('subform')) {
						count++;
					}else {
						console.log(count)
						 for(var j = idx; j < idx2; j++) {
							console.log(($tr.parents('tbody').children().eq(j + count)))
							//$tr.parents('tbody').children().eq(j + count).before($tr)
						} 
						break;	
					}
				
				}
					
			}  
				

			
	}*/
		
	
	
/* 	for(var i = idx; true; i--) {
		if($tr.parents('tbody').children().eq(i - 1).hasClass('subform')) {
			count++;
		}
		else {
			break;
		}
		
		} 
	for(var i = idx; true; i++) {
		if($tr.parents('tbody').children().eq(i + 1).hasClass('subform')) {	
			count2++;
		}else {
			break;
		}
	}
	for( var i = idx; true; i++) {
		if($tr.parents('tbody').children().eq(i - 1).hasClass('subform')) {
			
		}else {					
			$tr.parents('tbody').children().eq(i-count-2).before($tr)
			for(var j = idx + 1; j <= idx + count2; j++) {
				$tr.after($tr.parents('tbody').children().eq(j))
			}
			count2 = 0;
			break;
		}
		 */
		 /* for(var i = idx; i < idx2; i++) {
			if($tr.parents('tbody').children().eq(i + count).hasClass('subform')){
				console.log($tr.parents('tbody').children().eq(i + count).next())
				$tr.parents('tbody').children().eq(i + count).next().after($tr);
				for(var k = idx + 1; k <=idx + count; k++) {
					console.log($tr.after($tr.parents('tbody').children().eq(j-1)))
					//$tr.after($tr.parents('tbody').children().eq(j-1))
				}
				
			}else {
				console.log($tr.parents('tbody').children().eq(i +1+ count).next())
				//$tr.parents('tbody').children().eq(i +1+ count).next().after($tr);
				for(var j = idx + 1; j <= idx + count; j++) {
					//console.log($tr.parents('tbody').children().eq(j-1))
					//$tr.after($tr.parents('tbody').children().eq(j-1))
				}
				break;
			}
		}  */
    
</script>
</body>
</html>