<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js'></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">	
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>회사정보 입력	</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                     <form id="updateCompanyForm" action="updateCompanyForm.cm" method="post" enctype="multipart/form-data">
                     
                    <div class="main_cnt_list clearfix">
               	 		<a href="selectCompany.cm" class="button btn_pink com_can">취소</a>
						<button class="button btn_blue com_sav" onclick="submitBtn()">저장</button>
						
					<div id="logoImgArea" >
					<img src="${ contextPath }/resources/uploadFiles/${ at.changeName }" class="titleImg">
					<input type="file" name="attach" onchange="loadImg(this, 1)" style="display:none">
					<button type="button" class="btn_main btn_choise firstphoto" onclick="onclick=document.all.attach.click()">선택
                    </button>
						   	
					</div>
						<input type="hidden" name="fileNo" value="<c:out value='${at.fileNo}'/>">
                    	<input type="hidden" name="cName" value="<c:out value='${c.companyName }'/>">
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">회사명</div>
                            <div class="main_cnt_tit">
                            <input type='text' name='companyName' value="<c:out value='${c.companyName}'/>">
                            </div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사업자번호</div>
                            <div class="main_cnt_tit">
                            <input type='text' name='license' value="<c:out value='${c.license}'/>">
                            </div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">전화번호</div>
                            <div class="main_cnt_tit">
                            <input type='text' name='phone' value="<c:out value='${c.phone }'/>">
                            
                            </div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                           <div class="main_cnt_tit">
                           <input type='text' name='fax' value="<c:out value='${c.fax }'/>">
                           </div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">대표자명</div>
                            <div class="main_cnt_tit">
                            <input type='text' name='name' value="<c:out value='${c.name }'/>">
                            
                            </div>
                        </div>
                        
                        <%-- <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                             <div class="main_cnt_tit">
                              <input type='text' name='addr1' value="<c:out value="${ c.address.split('/')[0] }"/>">
                             
                             </div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_tit addr com_addr2">
                        	<input type='text' name='addr2' class="addr2" value="<c:out value="${ c.address.split('/')[1] }"/>">
                        	</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_tit addr com_addr2">
                        	<input type='text' name='addr3' class="addr2" value="<c:out value="${ c.address.split('/')[2] }"/>">
                        	</div>
                        </div> --%>
                        
                        <div class="main_cnt_list clearfix">
                        <div class="address_form_wrap">
                        <div class="main_cnt_tit">주소</div>
                            <div class=""><input type="text" name="postcode_form" id="postcode_form" value="<c:out value="${c.address.split('/')[0] }"/>" placeholder="우편번호" readonly><a href="#" class="button btn_pink" id="address_srch_btn" onclick="DaumPostcode();">검색</a></div>
                            <div class=""><input type="text" name="address_form" id="address_form" value="<c:out value="${c.address.split('/')[1] }"/>" placeholder="주소" readonly></div>
                            <div class=""><input type="text" name="detailAddress_form" value="<c:out value="${c.address.split('/')[2] }"/>" id="detailAddress_form" placeholder="상세주소"></div>
                            <div><input type="hidden" name="address" id="address"></div>
                        </div>
                       </div> 
                        
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">도메인</div>
                            <div class="main_cnt_tit">
                            <c:out value='${ c.domain }'/>
                            </div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">설립일</div>
                            <div class="main_cnt_desc">
                            <input type="text" name="estDate" id="datepicker" value="${c.estDate}" autocomplete=off></div>
                        </div>
                        
                    </div>
                
                    <!-- ** 코드 작성부 끝 -->
                    </form>
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>


<!-- 주소검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
	        // 주 메뉴 분홍색 하이라이트 처리
	        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(5).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(4).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(4).find(".sub_menu_list").eq(1).addClass("on");
        
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy-mm-dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
        
        var a1 = $("#postcode_form").val();
        var a2 = $("#address_form").val();
        var a3 = $("#detailAddress_form").val();
        $("#address").val(a1 + "/" + a2 + "/" + a3);
        console.log($("#address").val())
        console.log(a1)
    });
    
    // 주소 검색 시작
    function DaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('postcode_form').value = data.zonecode;
                document.getElementById("address_form").value = addr;
                document.getElementById("detailAddress_form").focus();
            }
        }).open();
    }

    $("#postcode_form, #address_form").on("click", function(){
        $("#address_srch_btn").trigger("click");
    });
    // 주소 검색 끝
    
     function submitBtn() {
         
       var p1 = $("#phone1").val();
       var p2 = $("#phone2").val();
       var p3 = $("#phone3").val();
       $("#phone").val(p1 + "-" + p2 + "-" + p3);
       
       
       var a1 = $("#postcode_form").val();
       var a2 = $("#address_form").val();
       var a3 = $("#detailAddress_form").val();
       $("#address").val(a1 + "/" + a2 + "/" + a3);
      
       
       var birth = $("[name=estdate]").val().split("/");
      $("[name=estDate]").val(birth[0] + "-" + birth[1] + "-" + birth[2]);     
      
      $("#signCode").val($('.fr-element.fr-view').html())
    
       
       $("#updateCompanyForm").submit();

    };
    
    function showBtn() {
    	$("#input_file_first").click();
    }
    
    function loadImg(value, num) {
		if(value.files && value.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function(e) {
				
				switch(num) {
				case 1 : $(".titleImg").attr("src", e.target.result); break;
				}
			}
			reader.readAsDataURL(value.files[0]);
		}
	}

    
</script>
</body>
</html>