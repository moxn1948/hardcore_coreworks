<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
         		
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->          
            <form action="updatePos.cm" id="udapteForm" method="post">
                <div class="main_ctn">
				<button  class="button btn_blue save" id="submitBtn">저장</button>
				<a href="#" class="button btn_blue add" onclick="topAdd()">추가</a>
                    <div class="menu_tit"><h2>직책 관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
            
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn jobtb">
                                <tr class="tbl_main_tit">
                                    <th>직책번호</th>
                                    <th>직책명</th>
                                    <th>순서변경</th>
                                    <th>삭제</th>
                                </tr>
				  <c:forEach var="p" items="${ list }">
					<tr>
						<td hidden><c:out value="${ p.posNo}"/></td>
						<td hidden><input type='text' class='posNo' name='posNo' hidden></td>
						<td hidden><input class='pNo' name='pNo'value=<c:out value="${ p.posNo}"/> readonly></td>					
						<td><input class='jobOrdertd' name='posOrder'value=<c:out value="${ p.posOrder}"/> readonly></td>
						<td><input class='jobNametd' name='posName' value=<c:out value="${ p.posName }"/>></td>
						<td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' type='button' onclick='moveDown(this)'>▼</button></a></td>
						<td><button type='button' class='btn_pink' onclick='del(this)'>삭제</button></td>			
						<td hidden><input type="text" name="posNm" class="posNm" hidden></td>
						<td hidden><input type="text" name="posOd" class="posOd" hidden></td>
						<td hidden><input type='text' class='posNo2' name='posNo2' hidden></td>
					</tr>
				</c:forEach>

<!-- 						
						
								 -->
				</table>
				
                    </div>
                    
                          
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                   
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
				</div>
            </form>
		<!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
var pOrder = 0;
var posNm = "";
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(3).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(3).find(".sub_menu_list").eq(1).addClass("on");
        
        pOrder = ($(".jobtb").find("tr:last-child()").children().eq(3).children().val() * 1);
        

        
    });
    function topAdd() {
    	var str = "<tr><td hidden></td><td hidden><input type='text'  hidden></td><td hidden><input readonly></td><td><input class='jobOrdertd' name='posOrder' type='text' readonly value='" + (pOrder +1) + "' style='text-align:center; '></td><td><input class='jobNametd' type='text' name=" + ("'posName") +"'" + " style='text-align:center;'></td><td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' type='button' onclick='moveDown(this)'>▼</button></a></td><td><button type='button' class='btn_pink' onclick='del(this)'>삭제</button></td><td hidden><input type='text' class='posNo2' name='posNo2' value='0' hidden></td></tr>";
    	pOrder++   		
    	$(str).appendTo($(".tbl_ctn"));
    	
    }
    

    $('.table1 button:even').bind('click', function(){ moveUp(this) });
    $('.table1 button:odd').bind('click', function(){ moveDown(this) });
    
    function moveUp(el){
    	var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
    	var idx = $tr.index();
    	

		if(idx > 1) {
		var s = $tr.prev().children().eq(4).children().val();//전에꺼
		var spo = $tr.prev().children().eq(4).children();//전에꺼 위치
		var prePo = $tr.children().eq(4).children();//지금꺼
		var prePoVal = $tr.children().eq(4).children().val();//지금값
		
		/* $tr.prev().before($tr); */ // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
    		//console.log($tr.children().eq(1).children().children())
    		console.log($tr.children().eq(4).children())
			prePo.val(spo.val());
			spo.val(prePoVal);
	
		}
	}
    
    function moveDown(el){
    	var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
		var idx = $tr.index();
		var idx2= $(".jobtb").find("tr:last-child()").index();
		

		
		/* $tr.next().after($tr); */ // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
		if(idx >= 1 && idx < idx2) {

		var s = $tr.next().children().eq(4).children().val();//전에꺼
		var spo = $tr.next().children().eq(4).children();//전에꺼 위치
		var prePo =$tr.children().eq(4).children();//지금꺼
		var prePoVal = $tr.children().eq(4).children().val();//지금값

		prePo.val(spo.val());
		spo.val(prePoVal);
		}
	}
    
    	var poNo = "";
    function del(d) {
    	str = d.closest('tr')
    	var $tr = $(d).closest('tr');
    	var pNo = $tr.children().eq(0).text();
    	var posNo =  $tr.find($(".pNo")).val();
		var count = 0;
   	 $.ajax({
		url:"selectPosCount.cm",
		type:"get",
		data:{posNo: posNo},
		success:function(data) {
			count = data.posCount
   		console.log("count : " + count)
   		
   		if(count >= 1){
   		alert('해당 직책을 가진 사람이 있습니다.')
   		}else{
   		$tr.remove()
   		pOrder--;
    	if(poNo == "") {
    		poNo = pNo;
    	}else {
    		
    		poNo = poNo + ", " + pNo;
    	}
  		
    	$(".posNo").val(poNo);
    	posNm = "";
    	$("#posNm").val(posNm)
    	$("#submitBtn").click(function(){
    		
    	})
   		}
    	
		}, 
		error:function(data) {
			console.log(data)
		}
	
		})  


    	
    }
   
    $("#submitBtn").click(function() {
    	var name = $("[name=posName]");
    	var order = $("[name=posOrder]");
    	var no = $("[name=posNo]");
    	var posNm = ""
    	var posOd = ""
    	var posNo = ""
    
    	name.each(function() {
    	var Name2 = $(this).val()
    	if(posNm == "") {
    		posNm = Name2
    	}else {
    		posNm = posNm + ", " + Name2 		
    	}
    		$(".posNm").val(posNm)		
    		console.log($(".posNm").val())
    	});
    	order.each(function() {
    		var Order2 = $(this).val()
    		if(posOd == "") {
    			posOd = Order2
    		}else {
    			posOd = posOd + ", " + Order2
    		}
    		$(".posOd").val(posOd)
    	})
    	 no.each(function() {
    		var No2 = $(this).val()
    		if(posNo == "") {
    			posNo = No2
    		}else {
    			posNo = posNo + ", " + No2
    		}
    		$(".posNo2").val(posNo)
    		console.log(No2)
    		console.log("posNo : " + posNo)
    	}) 
    	
    
    });
    	
</script>
</body>
</html>