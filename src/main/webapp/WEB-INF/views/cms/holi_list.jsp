<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
				<a href="insertVacForm.cm" class="button btn_blue add">추가</a>
                    <div class="menu_tit"><h2>휴무 관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
            	
		
                        
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                                <form id="updateForm" action="selectVacOne.cm" method="get">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn vacTb">
                                <tr class="tbl_main_tit">
                                	<th>휴무명</th>
                                    <th>휴무시작일</th>
                                    <th>휴무종료일</th>
                                    <th>사용여부</th>
                                    <th>반복여부</th>
                                    <th>수정</th>
                                </tr>
                                
                                
                                
                                <c:forEach	var="list" items="${ list }">
                                	<c:if test="${ list == null }">
                                		<td>저장된 휴무가 없습니다.</td>
                                	</c:if>
                                	<tr class="topvacList">
                                	
                                		<td>
                                		<input type="hidden" class="vacNo" name="vacNo" value="<c:out value='${list.VAC_NO}'/>">
                                		<div class="main_cnt_desc vacList"><c:out value=' ${list.VAC_NAME}'/></div>
                                		<%-- <input class="vacList" name="vacList" value="<c:out value=' ${list.VAC_NAME}'/>"> --%> 
                                		</td>
                                		
                                		<td>
                                		<div class="main_cnt_desc vacList"><c:out value=' ${list.VAC_SDATE}'/></div>
                                		<%-- <input class="vacList" name="vacSdate" value="<c:out value='${list.VAC_SDATE}'/>">  --%>
                                		</td>
                                		
                                		<td>
                                		<div class="main_cnt_desc vacList"><c:out value=' ${list.VAC_EDATE}'/></div>
                                		<%-- <input class="vacList" name="vacEdate" value="<c:out value='${list.VAC_EDATE}'/>"> --%> 
                                		</td>
                                		
                                		<td>
                                		<div class="main_cnt_desc vacList"><c:out value=' ${list.USING_YN}'/></div>
                                		<%-- <input class="vacList" name="vacList" value="<c:out value='${list.USING_YN}'/>"> --%> 
                                		</td>
                                		
                                		<td>
                                		<div class="main_cnt_desc vacList"><c:out value=' ${list.REPEAT_YN}'/></div>
                                		<%-- <input class="vacList" name="vacList" value="<c:out value='${list.REPEAT_YN}'/>"> --%> 
                                		</td>
                                		<td><button type="button" class="button btn_main" onclick='vacOne(this)'>수정</button></td>	
                                	</tr>
                                </c:forEach>
                               <tr id="notice-nocnt">
                               		<td></td>
                               		<td></td>
                                 	<td>휴무가 없습니다.</td>
                                 	<td></td>
                                 	<td></td>
                                 	<td></td>
                                 </tr>
						   </table>
                    </div>
                     
                          
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                                </form>
                   
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
		
		<!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
var i = 1;
var j = 1;
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
       // $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
       // $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        //상위부서 번호
        
        lastIdx =  $(".vacTb").find($("tr:last-child")).index()
 			if(lastIdx > 1) {
 				$("#notice-nocnt").hide();
 			}
    });

    	function vacOne(d) {
    		var $tr = 	$(d).closest('tr');
    		var vacNo = $tr.find("[name=vacNo]").val();
			console.log(vacNo)
			
			location.href="selectVacOne.cm?vacNo="+ vacNo;
    		
    	}
    
    
</script>
</body>
</html>