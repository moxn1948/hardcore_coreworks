<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>

.fr-wrapper {
	min-height: 600px;
}
</style>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/ui/trumbowyg.min.css" rel='stylesheet' type='text/css' />
<link href="${ contextPath }/resources/css/trumbowyg_table.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/trumbowyg.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/plugins/table/trumbowyg.table.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/langs/ko.min.js"></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->

<form action="updateFormat.cm" method="post">
                <div class="main_ctn">
						
                    <div class="menu_tit">
                    	<h2>기안서식 수정</h2>
                    	<button class="button btn_blue form_update" onclick="return topAdd()">변경</button>
                    </div>
                   
                     <!-- ** 코드 작성부 시작 -->
                     
                    <div class="main_cnt">
                            <div class="main_cnt_tit">폴더</div>
                        <div class="main_cnt_list clearfix">
                            <select id="pathName" name="pathNo">
                            <c:forEach var="d" items="${ docPathList }">
	                            <option value="${ d.pathNo }">${ d.pathName }</option>
                            </c:forEach>
                       		 </select> 
                        </div>
                        <div class="main_cnt_list clearfix2">
                            <div class="main_cnt_tit">양식명</div>
                            <input type="text" id="formatName" name="formatName" value="${ f.formatName }">
                            <input type="text" name="formatNo" value="${ f.formatNo }" hidden>
                        </div>
                        
                        <div class="main_cnt_list clearfix2 tit">
	                            <div class="main_cnt_tit">보존연한</div>
	                            <select id="period" name="period" >
		                            <option value="1">1년</option>
		                            <option value="3">3년</option>
		                            <option value="5">5년</option>
	                            </select>
	                            <div class="period_align">
		                            <input type="checkbox" name="periodYn" id="change" value="N">
									<label for="change">변경금지</label>
								</div>
                            </div> 	 
                       
                       		<div class="main_cnt_list clearfix">
                           <div class="main_cnt_tit">설명</div>
                           <textarea id="formatDetail" name="formatDetail">${ f.formatDetail }</textarea>	     
                       		</div>
                       		
                       
                       	<div class="main_cnt_list clearfix">
                           <div class="main_cnt_tit">양식형태</div>
                           <div class="box_radio">
						<input type="radio" name="form" id="provide" value="provide">
						<label for="provide">기본제공 양식</label> 
						<input type="radio" name="form" id="self" value="self">
						<label for="self">직접 편집</label>
						   </div>
							<div class="box_cnt">
								<ul class="list">
									<li>하단 툴을 활용해 결재 문서 본문의 폰트, 정렬, 이미지 첨부, 표 삽입 등을 사용할 수 있습니다</li>
									<li>디자인을 통한 고급 편집 기능 외 HTML/TEXT 기능도 사용할 수 있습니다.</li>
									<li>MS OFFICE나 한글 파일을 복사해 에디터에 붙여 넣기를 하면 원본 파일 서식과 100% 일치하지 않을 수 있습니다. 그럴 때는 사용자가 에디터의 소스 보기를 통해 수정을 해주시거나 에디터에서 직접 작성해 주셔야 합니다.</li>
								</ul>
							</div>
                      	</div>
                           
                        <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn align">
                            <colgroup>
                            	<col style="width: 5%;">
                            	<col style="width: 25%;">
                            	<col style="width: 25%;">
                            </colgroup>
                                <tr>
                                	<td></td>
                                    <td class="tit align">서식번호</td>
                                    <td class="tit align">서식 명</td>
                			   </tr>
                			   <c:forEach var="p" items="${ priFormatList }">
	                               <tr>
	                               		<td class="tit"><input type="radio" name="priFormat" class="check" value="1"></td>
	                               		<td class="priNo">${ p.priFormatNo }<input type="text" name="priFormatNo" value="${ p.priFormatNo }" hidden></td>
	                               		<td class="priName">
	                               			<a href="#popPriDetail" rel="modal:open">${ p.priName }</a>
	                               			<input type="text" class="priCnt" value='${ p.priCnt }' readonly hidden>
	                               		</td>
	                               </tr>
                			   </c:forEach>

                            </table>
                        </div>
                    </div>
                    <!-- 에디터 -->
						<!-- <div class="fr-view"></div> -->
						<table class="tbl_ctn formatSet">
                                <colgroup>
                                    <col style="width: 80px;">
                                    <col style="width: *;">
                                    <col style="width: 80px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                </colgroup>
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">문서번호</td>
                                    <td rowspan="2" class="doc_con"></td>
                                    <td rowspan="4" class="doc_con small doc_tit">결재</td>
                                    <td class="doc_con gray doc_tit"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>                             
                                </tr>
                               	<tr class="eas_tit">                                 
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                </tr>                             
                                <tr class="eas_tit" >
                                    <td rowspan="2" class="doc_con doc_tit">작성일자</td>
                                    <td rowspan="2" class="doc_con"></td>                         	                                                                   
                                </tr>
                                <tr class="eas_tit"></tr>                                
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성부서</td>
                                    <td rowspan="2" class="doc_con"></td>
                                    <td rowspan="4" class="doc_con small doc_tit">합의</td>
                                    <td class="doc_con gray doc_tit"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>                                 
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>                                
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성자</td>
                                    <td rowspan="2" class="doc_con"></td>                              
                                </tr>  
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">수신자</td>
                                    <td colspan="7" class="doc_con"></td>                                 
                                </tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">제목</td>
                                    <td colspan="7" class="doc_con"></td>                                
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="10" colspan="8" class="eas_content"><div id="editer">${ f.formatCnt }</div></td>                              
                                </tr>

                            </table>
						
						
						<input type="text" id="contents" name="formatCnt" hidden>
                    <!-- 기본 테이블 끝 -->    
                    </div>
                            </div>
                   </form>   
                          
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                                      
                    
            <!-- 메인 컨텐츠 영역 끝! -->
		
                    <div>

                    </div>
		<!-- inner_rt end -->
        </div>
        </div>

<!-- 기본제공양식 상세보기 -->
<div id="popPriDetail" class="modal">
	<jsp:include page="cms_pop_detail_pri.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){

		$('#editer').trumbowyg({
		    btns: [['viewHTML'],
		        ['undo', 'redo'], // Only supported in Blink browsers
		        ['formatting'],
		        ['strong', 'em', 'del'],
		        ['superscript', 'subscript'],
		        ['link'],
		        ['insertImage'],
		        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
		        ['unorderedList', 'orderedList'],
		        ['horizontalRule'],
		        ['removeformat'],
		        ['table']],
		    lang: 'ko',
		    plugins: {
		        table: {
		        	rows: 10,
		        	columns: 10,
		        	styler: 'tbl_ctn',
		        }
		    }
		});
    	
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(1).addClass("on");
        
        $("#provide").click(function() {
        	$(".tbl_ctn.formatSet").hide();
        	$(".tbl_common.tbl_basic").show();
        	
        });
        $("#self").click(function() {
        	$(".tbl_ctn.formatSet").show();
        	$(".tbl_common.tbl_basic").hide();
        });
        
        // 기존에 선택되있는 폴더명이 기본적으로 선택되있음
        const pathNo = "${f.pathNo}";
        $('#pathName').val(pathNo).attr('selected', true);
        
        // 변경금지라면 해당 컨테이너가 아예 안보여지게
        const periodYn = "${ f.periodYn }";
        console.log(periodYn, typeof periodYn);
        if(periodYn === 'N') {
        	console.log(1);
        	$('.main_cnt_list.clearfix2.tit').hide();
        	$('#change').attr('checked', true);
        }
        
        // priFormatNo가 0이라면 직접입력 checked, 0이상이라면 기본제공서식 checked + 해당 번호
        const priFormatNo = "${ f.priFormatNo }";
        const formatCnt = '${f.formatCnt}';
        
        if(priFormatNo == '0') {
        	$('#self').attr('checked', true);
        	$(".tbl_common.tbl_basic").hide();
        } else {
        	$('#provide').attr('checked', true);
        	$('.check').val("${priFormatNo}").attr('checked', true);
        	$(".tbl_ctn.formatSet").hide();
        }
        
    });
    
	function topAdd() {
    	
    	// 양식명이나 설명이 비어있으면 채우게함
    	if($('#formatName').val() <= 0 ) {
    		alert('양식명을 작성해주세요.');
    		$('#formatName').focus();
    		return false;
    	} else if($('#formatDetail').val() <= 0) {
    		alert('설명을 작성해주세요.');
    		$('#formatDetail').focus();
    		return false;
    	}
    	
    	if($('#provide').is(':checked')) {
    		if(!($('.check').is(':checked'))) {
    			alert('기본제공 양식을 선택해주세요.');
    			return false;
    		}
    	} else if($('#self').is(':checked')) {
    		if($('#editer').html() === '' || $('#editer').html() === '<p><br></p>') {
    			alert('내용을 채워주세요.');
    			return false;
    		}
    	}
    	
    	if(confirm('이대로 등록하시겠습니까?')) {
			$('#contents').val($('#editer').html());
			return true;
    	} else {
    		return false;
    	}
    }
    
</script>
</body>
</html>