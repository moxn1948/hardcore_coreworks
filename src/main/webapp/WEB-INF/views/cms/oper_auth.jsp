<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/cms_menu.jsp" />
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/ui/trumbowyg.min.css" rel='stylesheet' type='text/css' />
<link href="${ contextPath }/resources/css/trumbowyg_table.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/trumbowyg.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/plugins/table/trumbowyg.table.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/langs/ko.min.js"></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                
               <form action="updateOperAuth.cm" id="operAuthForm" method="post">
             
                    <div class="menu_tit empreg">
                    <h2>운영자계정 관리</h2>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        
                    </div>
                 <div class="authbtn">	
							<button class="button btn_blue authsave" id="submitBtn">저장</button>
                           <a class="button btn_blue" href="#operAuthPop" rel="modal:open" id="security_input">추가</a>
				</div>	
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
							<input type="hidden" name="protectDetailPerson" id="protectDetailPerson">
                    <div class="tbl_common tbl_basic oper_auth">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn authTb">
                            <colgroup>
                            	<col style="width: *;">
                            	<col style="width: 10%;">
                            </colgroup>
                                <tr class="tbl_main_tit">
                                    <th>사번</th>
                                    <th>이름</th>
                                    <th>ID</th>
                                    <th>부서</th>
                                    
                                    <th class="companypop">회사 관리
                                    <!-- 추가 설명 팝업 시작 -->
                                	<div class="add_desc_pop">
                                    <a href="javascript: void(0);" class="add_desc_tit"><div class="question_mark"><i class="far fa-question-circle"></i></div></a>
                                    <div class="add_desc_box">
                                        <div class="box_cnt companybox"><ul class="popList">
                                        								<li>부서 구조 생성, 수정, 삭제</li>
                                        								<li>직급/직책 생성, 수정, 삭제</li>
                                        								<li>사용자 비밀번호 변경 권한</li>
                                        								<li>회사기본정보 설정</li>
                                        								</ul>
                                        </div>
                                    </div>
                               		 </div>
                                <!-- 추가 설명 팝업 끝 -->
                                    
                                    </th>
                                    
                                     <th>인사 관리<!-- 추가 설명 팝업 시작 -->
                                <div class="add_desc_pop">
                                    <a href="javascript: void(0);" class="add_desc_tit"><div class="question_mark"><i class="far fa-question-circle"></i></div></a>
                                    <div class="add_desc_box">
                                        <div class="box_cnt companybox"><ul class="popList">
                                        								<li>인사이동</li>
                                        								<li>퇴사자 처리</li>
                                        								<li>운영자 생성, 수정, 삭제</li>
                                        								<li>운영자 권한 설정</li>
                                        								</ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- 추가 설명 팝업 끝 -->
                                    </th>
                                    <th>사원 등록<!-- 추가 설명 팝업 시작 -->
                                <div class="add_desc_pop">
                                    <a href="javascript: void(0);" class="add_desc_tit"><div class="question_mark"><i class="far fa-question-circle"></i></div></a>
                                    <div class="add_desc_box erp">
                                        <div class="box_cnt companybox"><ul class="popList">
                                        								<li>사용자 생성, 수정, 삭제</li>
                                        								</ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- 추가 설명 팝업 끝 --></th>
                                    <th>일정 관리
                                    <!-- 추가 설명 팝업 시작 -->
                                <div class="add_desc_pop">
                                    <a href="javascript: void(0);" class="add_desc_tit"><div class="question_mark"><i class="far fa-question-circle"></i></div></a>
                                    <div class="add_desc_box">
                                        <div class="box_cnt companybox"><ul class="popList">
                                        								<li>공지사항 전체 권한</li>
                                        								<li>공지사항 및 고지사항의 댓글 삭제</li>
                                        								<li>부서일정 정보 열람 권한</li>
                                        								<li>회사 일정 등록 권한</li>
                                        								</ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- 추가 설명 팝업 끝 -->
                                    </th>
                                    <th>팀 관리
                                    	<!-- 추가 설명 팝업 시작 -->
                                <div class="add_desc_pop">
                                    <a href="javascript: void(0);" class="add_desc_tit"><div class="question_mark"><i class="far fa-question-circle"></i></div></a>
                                    <div class="add_desc_box tm">
                                        <div class="box_cnt companybox"><ul class="popList">
                                        								<li>팀 공지사항 삭제 권한</li>
                                        								<li>본인 팀 사용자 생성, 수정, 삭제</li>
                                        								<li>결재 템플릿 생성, 수정, 삭제</li>
                                        								</ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- 추가 설명 팝업 끝 -->
                                    </th>
                                    <th>삭제</th>
                                </tr>
						<c:forEach var="emp" items="${list}">
							<tr>
								<td><input type="hidden" name='empDivNo' value="<c:out value='${ emp.empDivNo }'/>"><div class="main_cnt_tit"><c:out value='${ emp.empNo }'/></div><input type="hidden" class="delNo" name="delNo"></td>
								<td><div class="main_cnt_tit"><c:out value='${ emp.empName }'/></div></td>
								<td><div class="main_cnt_tit"><c:out value='${ emp.empId }'/>@coreworks.info</div></td>
								<td><div class="main_cnt_tit"><c:out value='${ emp.deptName }'/></div></td>
								<td><input type="hidden" class="cpm" name="cpm" value="<c:out value='${ emp.cpm }'/>"><input type="checkbox" class="cpm" name='cpm1' <c:if test="${ emp.cpm eq 'Y' }"> checked </c:if>/></td>
								<td><input type="hidden" class="psm" name="psm" value="<c:out value='${ emp.psm }'/>"><input type="checkbox" class='psm' name='psm1' <c:if test="${ emp.psm eq 'Y' }"> checked </c:if>/></td>
								<td><input type="hidden" class="epm" name="epm" value="<c:out value='${ emp.epm }'/>"><input type="checkbox" class='epm'name='epm1' <c:if test="${ emp.epm eq 'Y' }"> checked </c:if>/></td>
								<td><input type="hidden" class="clm" name="clm" value="<c:out value='${ emp.clm }'/>"><input type="checkbox" class='clm' name='clm1' <c:if test="${ emp.clm eq 'Y' }"> checked </c:if>/></td>
								<td><input type="hidden" class="tm"  name="tm"  value="<c:out value='${ emp.tm }'/>"><input type="checkbox" class='tm' name='tm1' <c:if test="${ emp.tm eq 'Y' }"> checked </c:if>/></td>
								<td><a class='button btn_pink' onclick='del(this)'>삭제</a></td>					
							</tr>
						</c:forEach>
					

				</table>
                    </div>
                     
                          
                    <!-- 기본 테이블 끝 -->
 <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        	<c:if test="${ pi.currentPage eq 1 }">
                        		<li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage ne 1 }">
                        		<c:url var="blistFirst" value="selectOperAuth.cm">
                        			<c:param name="currentPage" value="1"/>
                        		</c:url>
                        		<li class="pager_com pager_arr first"><a href="${ blistFirst }">&#x003C;&#x003C;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage <= 1 }">
								<li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage > 1 }">
								<c:url var="blistBack" value="selectOperAuth.cm">
									<c:param name="currentPage" value="${ pi.currentPage - 1 }"/>
								</c:url>
								<li class="pager_com pager_arr prev"><a href="${ blistBack }">&#x003C;</a></li>
							</c:if>
							<c:forEach var="p" begin="${ pi.startPage }" end="${ pi.endPage }">
								<c:if test="${ p eq pi.currentPage }">
									<li class="pager_com pager_num on"><a href="javascrpt: void(0);">${ p }</a></li>
								</c:if>
								<c:if test="${ p ne pi.currentPage }">
									<c:url var="blistCheck" value="selectOperAuth.cm">
										<c:param name="currentPage" value="${ p }"/>
									</c:url>
									<li class="pager_com pager_num"><a href="${ blistCheck }">${ p }</a></li>
								</c:if>
							</c:forEach>
                       		<c:if test="${ pi.currentPage >= pi.maxPage }">
								<li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage < pi.maxPage }">
								<c:url var="blistEnd" value="selectOperAuth.cm">
									<c:param name="currentPage" value="${ pi.currentPage + 1 }"/>
								</c:url>
								 <li class="pager_com pager_arr next"><a href="${ blistEnd }">&#x003E;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage eq pi.maxPage }">
                        		<li class="pager_com pager_arr last"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage ne pi.maxPage }">
                        		<c:url var="blistLast" value="selectOperAuth.cm">
                        			<c:param name="currentPage" value="${ pi.maxPage }"/>
                        		</c:url>
                        		<li class="pager_com pager_arr last"><a href="${ blistLast }">&#x003E;&#x003E;</a></li>
                        	</c:if>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->                   
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
			  </form> 
		<!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- 보안설정 특정인 지정 popup include -->
<div id="operAuthPop" class="modal">
	<jsp:include page="../pop/new_oper_addr_pop.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>

    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
       	$("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
      	 $("#menu_area .menu_list").eq(4).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
        
        authOrder = ($(".authTb").find("tr:last-child()").index());
        
        
    });

    
    function save() {
    	confirm("저장하시겠습니까?")
    }
    
    function next() {
    	confirm("다음 단계로 진행 시 이전 단계로 돌아갈 수 없습니다. 진행하시겠습니까? (※ 추후 관리자 메뉴에서 수정이 가능합니다.)")
    }
    

    $(document).on("click", ".cpm", function(){
        $(this).siblings(".cpm").val() == 'Y'
        if($(this).siblings(".cpm").val() == 'Y') {
    		$(this).siblings(".cpm").val('N')
        }else {
        	$(this).siblings(".cpm").val('Y')
        }
    })
    
    $(document).on("click", ".psm", function(){
    	 $(this).siblings(".psm").val() == 'Y'
    	        if($(this).siblings(".psm").val() == 'Y') {
    	    		$(this).siblings(".psm").val('N')
    	        }else {
    	        	$(this).siblings(".psm").val('Y')
    	        }
    })	
    
    $(document).on("click", ".epm", function(){
    	 $(this).siblings(".epm").val() == 'Y'
    	        if($(this).siblings(".epm").val() == 'Y') {
    	    		$(this).siblings(".epm").val('N')
    	        }else {
    	        	$(this).siblings(".epm").val('Y')
    	        }
    })
    
    $(document).on("click", ".clm", function(){
    	 $(this).siblings(".cplm").val() == 'Y'
    	        if($(this).siblings(".clm").val() == 'Y') {
    	    		$(this).siblings(".clm").val('N')
    	        }else {
    	        	$(this).siblings(".clm").val('Y')
    	        }
    })
    
    $(document).on("click", ".tm", function(){
    	 $(this).siblings(".tm").val() == 'Y'
    	        if($(this).siblings(".tm").val() == 'Y') {
    	    		$(this).siblings(".tm").val('N')
    	        }else {
    	        	$(this).siblings(".tm").val('Y')
    	        }
    })
    
    var spArr = Array();
    function selectOperPerson() {
    	
    	$.modal.close();
    	
    	var divNo = $('#empDivNo5').val();
    	var eName = $('#empName5').text();
    	var eDept = $('#deptName5').text();
    	var eId = $('#empId5').text();
    	var empNo = $('#empNo5').val();
    	
    	console.log(eDept)
    	console.log(empNo)
    	
    	
		str = "<tr><td><input type='hidden' name='empDivNo' value=" + divNo + "><div class='main_cnt_tit'> "+ empNo +"</div></td><td><div class='main_cnt_tit'>" + eName +" </div></td><td><div class='main_cnt_tit'> " + eId + " </div></td><td><div class='main_cnt_tit'> " + eDept +" </div></td><td><input type='hidden' class='cpm' name='cpm' value='N'><input type='checkbox' class='cpm' name='cpm1'/></td><td><input type='hidden' class='psm' name='psm' value='N'><input type='checkbox' class='psm' name='psm1'/></td><td><input type='hidden' class='epm' name='epm' value='N'><input type='checkbox' class='epm'name='epm1'/></td><td><input type='hidden' class='clm' name='clm' value='N'><input type='checkbox' class='clm' name='clm1'/></td><td><input type='hidden' class='tm'  name='tm' value='N'><input type='checkbox' class='tm' name='tm1'/></td><td><button type='button' class='btn_pink' onclick='del(this)'>삭제</button></td>	</tr>"
    	
    	$(str).appendTo($(".tbl_ctn"));
    	console.log(empNo)
    }
    
    var delNo = "";
    function del(d) {
    	var $tr = $(d).closest('tr');
    	var eNo = $tr.find("[name=empDivNo]").val()
    	
    	
    	if(delNo == "") {
    		delNo = eNo;
    	}else {
    		delNo = delNo + ", " + eNo;
    	}
    	
    	$(".delNo").val(delNo);
    	console.log($("[name=delNo]").val())
    	
    	
    	$("#submitBtn").click(function(){
    	    		
    	   }) 
    	
 	
    }
    
    
    	
    
    	
    
    

    
</script>
</body>
</html>