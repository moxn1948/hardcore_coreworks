<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
            <form action="updateDeptList.cm" id="updateForm" method="post">
            
                <div class="main_ctn">
            <button  class="button btn_blue save" id="submitBtn" >저장</button>
			<a href="#" class="button btn_blue add"  onclick="topAdd()">추가</a>
                    <div class="menu_tit"><h2>부서관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn deptTb">
                                <tr class="tbl_main_tit">
                                    <th>상위부서</th>
                                    <th>하위부서</th>
                                    <th>하위부서2</th>
                                    <th>하위폴더 추가</th>
                                    <th>순서변경</th>
                                    <th>수정/삭제</th>
                                </tr>
                                
                                <c:forEach var="list" items="${deptList}">
                                	<c:if test="${ list.deptOrderList.size() == 0 }">
                                	<tr class="topform">
                                		<td>
										<input type="hidden" class="deptNoList" name="deptNoList"  value="<c:out value='${ list.deptNo }'/>">
										<input type="hidden" class="delformNo" name="delformNo">
										<input type="hidden" class="DeptOrder" name="DeptOrder" value="-1">
										<input type = "hidden" class="DeptNameSubmit" name='DeptNameSubmit'>
										<input class="topDeptName" name='topDeptName' value="<c:out value='${ list.deptName}' />">
										</td>						
										<td><input type="hidden" name="subDeptName" value="-1"></td>
										<td><input type="hidden" name="sub2DeptName" value="-2"></td>	
										<td><a href='#'><button type="button"class='btn_main_plus' onclick='subAdd(this)'>+</button></a></td>
										<td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' type='button' onclick='moveDown(this)'>▼</button></a></td>
										<td><button type='button' class='btn_pink' onclick='del(this)'>삭제</button>
										</td>		
                                	</tr>

                                	</c:if>
                                	<c:if test="${ list.deptOrderList.size() != 0 }">
                                	
                                		<tr class="topform">
                                		<td>
										<input type="hidden" class="deptNoList" name="deptNoList"  value="<c:out value='${ list.deptNo }'/>">
										<input type="hidden" class="delformNo" name="delformNo">
										<input type="hidden" class="DeptOrder" name="DeptOrder" value=>
										<input type="hidden" class="DeptNameSubmit" name='DeptNameSubmit' value="-1">
										<input class="topDeptName" name='topDeptName' value="<c:out value='${ list.deptName}' />">
										</td>						
										<td><input type="hidden" name="subDeptName" value="-1"></td>
										<td><input type="hidden" name="sub2DeptName" value="-2"></td>	
										<td><a href='#'><button type="button"class='btn_main_plus' onclick='subAdd(this)'>+</button></a></td>
										<td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' type='button' onclick='moveDown(this)'>▼</button></a></td>
										<td><button type='button' class='btn_pink' onclick='del(this)'>삭제</button>
										</td>
                                		</tr>
                                	</c:if>
                                	
                                	<c:forEach var="list2" items="${ list.deptOrderList}">
                                		<c:if test="${ list2.deptOrderList.size() == 0 }">
                                		<tr class='subDept'>
                                			<td class="arrowImg"> 
                                			<input type="hidden" name="topDeptName" value="-1">	
									   		<div class="arrowImg">
									   		<img src='${contextPath}/resources/images/Arrow.jpg'>
									   		</div>
									   		</td>
									   		<td>
									   		<input type='text' name='subDeptName' class='subDeptName' value="<c:out value='${ list2.deptName}'/>">
									   		<input type="hidden" name="deptNoList" class="deptNoList" value="<c:out value='${ list2.deptNo}'/>" >
									   		<input type="hidden" name="DeptOrder" class="DeptOrder" value="<c:out value="${ list2.deptOrder}"/>" >
									   		<input type="hidden" name="subdelNo" class="subdelNo">
									   		</td>
									   		<td><input type="hidden" name="sub2DeptName" value="-2"></td>	
									   		<td><a href='#'><button type="button"class='btn_main_plus' onclick='sub2Add(this)'>+</button></a></td>
											<td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' type='button' onclick='moveDown(this)'>▼</button></a></td>
									   		<td>
									   		<button type='button' class='btn_pink' onclick="subdel(this)">삭제</button>
									   		</td>
                                		</tr>
                                		</c:if>
                                	</c:forEach>
                                	
                                	<c:forEach var="list2" items="${ list.deptOrderList}">
                                		<c:if test="${ list2.deptOrderList.size() != 0 }">
                                		<tr class='subDept'>
                                			<td class="arrowImg"> 	
                                			<input type="hidden" name="topDeptName" value="-1">	
									   		<div class="arrowImg">
									   		<img src='${contextPath}/resources/images/Arrow.jpg'>
									   		</div>
									   		</td>
									   		<td>
									   		<input type='hidden' name='subDeptOrderSubmit' class='subDeptOrderSubmit'>
									   		<input type='hidden' name='subDeptNameSubmit' class='subDeptNameSubmit'>
									   		<input type='text' name='subDeptName' class='subDeptName' value="<c:out value='${ list2.deptName}'/>">
									   		<input type="hidden" name="deptNoList" class="deptNoList" value="<c:out value='${ list2.deptNo}'/>" >
									   		<input type="hidden" name="DeptOrder" class="DeptOrder" value="<c:out value="${ list2.deptOrder}"/>" >
									   		<input type="hidden" name="subdelNo" class="subdelNo">
									   		</td>
									   		<td><input type="hidden" name="sub2DeptName" value="-2"></td>	
									   		<td><a href='#'><button type="button"class='btn_main_plus' onclick='sub2Add(this)'>+</button></a></td>
											<td><a href='#'><button class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' onclick='moveDown(this)'>▼</button></a></td>
									   		<td>
									   		<button type='button' class='btn_pink' onclick="subdel(this)">삭제</button>
									   		</td>
                                		</tr>
                                		</c:if>
                                	<c:forEach var="list3" items="${ list2.deptOrderList }">
                                		<tr class='subDept2'>
                                			<td><input type="hidden" name="topDeptName" value="-1"></td>
                                			<td class="arrowImg"> 	
									   		<div class="arrowImg">
									   		<input type="hidden" name="subDeptName" value="-1">
									   		<img src='${contextPath}/resources/images/Arrow.jpg'>
									   		</div>
									   		</td>
									   		<td>
									   		<input type='hidden' name='sub2DeptOrderSubmit' class='sub2DeptOrderSubmit'>
									   		<input type='hidden' name='sub2DeptNameSubmit' class='sub2DeptNameSubmit'>
									   		<input type='text' name='sub2DeptName' class='sub2DeptName' value="<c:out value='${ list3.deptName}'/>">
									   		<input type="hidden" name="deptNoList" class="deptNoList" value="<c:out value='${ list3.deptNo}'/>" >
									   		<input type="hidden" name="DeptOrder" class="DeptOrder" value="<c:out value="${ list3.deptOrder}"/>" >
									   		<input type="hidden" name="sub2delNo" class="sub2delNo">
									   		</td>
									   		<td></td>
									   		<td><a href='#'><button class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' onclick='moveDown(this)'>▼</button></a></td>
									   		<td>
									   		<button type='button' class='btn_pink' onclick="subdel(this)">삭제</button>
									   		</td>
                                		
                                		</tr>
                                	
                                	</c:forEach>
                                	</c:forEach>
                                	
	
                                	
                                </c:forEach>
                                
				

				</table>
                    </div>
                     
                          </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                   
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
			</form>
		<!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
var i = 1;
var idx = 0;
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
        
        /* $(document).on("click", ".btn_main_plus", function(){
        	var str2= "<tr><td><a href='#' style='text-align:center; display: inline-block;'><img src='${contextPath}/resources/images/Arrow.jpg'></a></td><<td><a href='#'><input type='text' style='text-align:center;'></input></a></td><td><input type='text' style='text-align:center;'></input></td><td><a href='#'><button class='btn_main_plus'>+</button></a></td><td><a href='#'><button class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' onclick='moveDown(this)'>▼</button></a></td><td><button class='btn_main'>수정</button><button class='btn_white'>삭제</button></td></tr>"
 			$(this).parents("tr").after(str2); */
			idx = $(".deptTb").find($(".deptNoList:last")).val();
 			lastIdx =  $(".deptTb").find($("tr:last-child")).index()
 			//상위부서 번호
			if(lastIdx > 1) {
 				$("#notice-nocnt").hide();
 			}
 			
 			
    });
        
    function topAdd() {
    	/* $.ajax({
    		url:"selectDeptNo.cm",
    		type:"get",
    		data:{},
    		success:function(data) {
    			no = data.deptNo
    		
    		}, 
    		error:function(data) {
    			console.log(data)
    		}
    	
    		})  */
    		$("#notice-nocnt").hide();
    	var str = "<tr><td><input type='hidden' class='DeptOrder' name='DeptOrder' value='-1'><input type='hidden' name='deptNoList' class='deptNoList' value='0'><input type='text' class='topDeptName' name='topDeptName' style='text-align:center;'></td><td><input type='hidden' name='subDeptName' value='-1'></td><td><input type='hidden' name='sub2DeptName' value='-2'></td><td><button type='button' class='btn_main_plus' onclick='subAdd(this)'>+</button></td><td><button  type='button' class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' onclick='moveDown(this)'>▼</button></td><td><button class='btn_white'>삭제</button></td></tr>";
    	$(str).appendTo($(".tbl_ctn"));
    		
    }
    
    function subAdd(d) {
    	////
    	
    	//
    	var count = 0;
    	var $tr = $(d).closest('tr'); 
    	//var idx = $tr.index();
    	var No = $tr.index();
    	var rank2 = $tr.find($('.deptNoList')).val();
    	if(rank2 == 0) {
    		rank2 = '-2';
    	}
  
    	console.log(rank2)
    	var str2= "<tr class='subDept'><input type='hidden' class='DeptOrder' name='DeptOrder' value=" + rank2 + "><input type='hidden' name='deptNoList' class='SubDeptNo' value='0'><input type='hidden' name='subDeptOrder' class='subDeptOrder' value="+ rank2 +"><input type='hidden' name='subRank' class='subRank'><td><input type='hidden' name='topDeptName' value='-1'><div class='arrowImg'><img src='${contextPath}/resources/images/Arrow.jpg'></div></td><td><input type='text' class='subDeptName' name='subDeptName' style='text-align:center;'></input></td><td><input type='hidden' name='sub2DeptName' value='-2'></td><td><button type='button'class='btn_main_plus' onclick='sub2Add(this)'>+</button></td><td></td><td><button type='button' class='btn_pink'>삭제</button></td></tr>";
    	//idx = ($(".formtb").find("tr:last-child()").index());
    	if(!$tr.next().hasClass('subDept')) {
    		$tr.after(str2)
    		
    	}else if($tr.next().hasClass('subDept')){
    			for(var i = No; true; i++) {
    				if($tr.parents('tbody').children().eq(i + 1).hasClass('subDept') || $tr.parents('tbody').children().eq(i + 1).hasClass('subDept2')) {	
    					count++
    				}else {   			
    					console.log(456)
						$tr.parents('tbody').children().eq(i).after(str2)
						break;
    				}
    				
    				
    			}
    		}
  	   }
    
    function sub2Add(d) {
    	var count = 0;
    	var $tr = $(d).closest('tr'); 
    	//var idx = $tr.index();
    	var No = $tr.index();
    	var rank2 = $tr.find($('.deptNoList')).val();
    	if(rank2 == null) {
    		var rank2 = '-3';
    	}
    	
    	
    	console.log(rank2)
    	console.log("rank2 : " + rank2)
    	var str2 = "<tr class='subDept2'><td><input type='hidden' class='DeptOrder' name='DeptOrder' value=" + rank2 + "><input type='hidden' name='topDeptName' value='-1'></td><td class='arrowImg'><input type='hidden' name='subDeptName' value='-1'><div class='arrowImg'><img src='${contextPath}/resources/images/Arrow.jpg'></div></td><td><input type='text' name='sub2DeptName' class='sub2DeptName'></td><td><input type='hidden' name='deptNoList' class='deptNoList' value='0'><input type='hidden' class='sub2DeptOrder' name='sub2DeptOrder' value="+ rank2 +"></td><td></td><td><button type='button' class='btn_pink' onclick='subdel(this)''>삭제</button></td></tr>"
    	var sub2 = $("[name=subDeptOrder]")
    	
    	
    	
    	
    	
    	if(!$tr.next().hasClass('subDept2')) {
    		$(str2).insertAfter($tr)
    		
    	}else if($tr.next().hasClass('subDept2')){
    		
    	
    		for(var i = No; true; i++) {	    	
    			if($tr.parents('tbody').children().eq(i + 1).hasClass('subDept2')) {
    			count++;
    			}
    			else {  	
    			break;
    			}
    		}
    			for(var i = No; true; i++) {
    				if($tr.parents('tbody').children().eq(i + No + 1).hasClass('subDet')) {
    				}else {
  	 					$tr.parents('tbody').children().eq(No +count).after($(str2))   					
    				}	
    				break;
    			}
    		}
    }
    $("#submitBtn").click(function() {
    	var topName = $("[name=topDeptName]");	
    	var subName = $("[name=subDeptName]");
    	var sub2Name = $("[name=sub2DeptName]");
    	var subOrder = $("[name=subDeptOrder]");
    	var sub2Order = $("[name=sub2DeptOrder]");
    	
    		//상위 이름
    		var tpn = "";
    		topName.each(function() {
    			var topDeptName = $(this).val()
    			if(tpn == ""){
    				tpn = topDeptName
    			}else {
    				tpn = tpn + ", " + topDeptName 
    			}
    			$(".DeptNameSubmit").val(tpn);
    		});
    		
    		
    		
    		//하위1 이름
    		var sbn = "";
    		subName.each(function() {
    			var subDeptName = $(this).val()
    			if(sbn == "") {
    				sbn = subDeptName
    			}else {
    				sbn = sbn + ", " + subDeptName
    				}
    			$(".subDeptNameSubmit").val(sbn);
    		});
    		
    		//하위1 부서순서
    		var sbo = "";
    		subOrder.each(function() {
    			var subDeptOrder = $(this).val()
    			if(sbo == "") {
    				sbo = subDeptOrder
    			}else {
    				sbo = sbo + ", " + subDeptOrder
    			}
    			$(".subDeptOrderSubmit").val(sbo)
    		});
    		
    		//하위2 이름
    		var sbn2 = ""
			sub2Name.each(function() {
				var sub2DeptName = $(this).val()
				if(sbn2 == "") {
					sbn2 = sub2DeptName
				}else {
					sbn2 = sbn2 + ", " + sub2DeptName
				}
				$(".sub2DeptNameSubmit").val(sbn2)
			});    	
    		
    		//하위2 부서순서
    		var sbo2 = "";
    		sub2Order.each(function() {
    			var sub2DeptOrder = $(this).val()
    			if(sbo2 == "") {
    				sbo2 = sub2DeptOrder
    			}else {
    				sbo2 = sbo2 + ", " + sub2DeptOrder
    			}
    			$(".sub2DeptOrderSubmit").val(sbo2)
    		});
    		
    		
			
    });
    
    
    $('.table1 button:even').bind('click', function(){ moveUp(this) });
    $('.table1 button:odd').bind('click', function(){ moveDown(this) });
    
    function moveUp(el){
    	
		var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
		if($('tr').eq(0)) {	
			console.log($('tr').eq(0))
			
			
			//$tr.prev().before($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
		}else {
			
			
		}
	}
    
    function moveDown(el){
		var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
		console.log($(el))
		console.log($tr)
		
		if($tr.eq(0).children()){
			console.log(123);			
		}else {
			console.log(567);
		}
		//$tr.next().after($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
	}
    
    
    
</script>
</body>
</html>