<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">		
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>회사정보 입력	</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    
                    <div class="main_cnt">
					<button type="button" onclick="updateCompany()"class="button btn_blue companyup">수정</button>
                     
                    <div class="main_cnt_list clearfix">
						
					<div id="logoImgArea">
					<img src="${ contextPath }/resources/uploadFiles/${ at.changeName }" class="titleImg">
						<!-- <img id="titleImg" width="120px" height="120px"> -->
						<%-- <c:if test="${ empty att }">
	                    		<i class="fas fa-user-circle" style="font-size: 125px;"></i>
	                    	</c:if> --%>
	                    		                    		
		                    	
	                    	
					</div>
                    	<input type="hidden" name="cName" value="<c:out value='${ c.companyName }'/>">
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">회사명</div>
                            <div class="main_cnt_tit"><c:out value='${ c.companyName }'/></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사업자번호</div>
                            <div class="main_cnt_tit"><c:out value='${ c.license }'/></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">전화번호</div>
                            <div class="main_cnt_tit"><c:out value='${ c.phone }'/></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                           <div class="main_cnt_tit"><c:out value='${ c.fax }'/></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">대표자명</div>
                            <div class="main_cnt_tit"><c:out value='${ c.name }'/></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                             <div class="main_cnt_tit"><c:out value="${ c.address.split('/')[0] }"/></div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_tit addr com_addr2"><c:out value="${ c.address.split('/')[1] }"/></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_tit addr com_addr2"><c:out value="${ c.address.split('/')[2] }"/></div>
                        </div>
                        
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">도메인</div>
                            <div class="main_cnt_tit"><c:out value='${ c.domain }'/></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">설립일</div>
                            <div class="main_cnt_tit"><c:out value='${ c.estDate }'/></div>
                        </div>
                        
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                </div>

            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
	             // 주 메뉴 분홍색 하이라이트 처리
	        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(5).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
    });
    
    function updateCompany() {
    	cName = $("[name=cName]").val();
    	console.log(cName)
    	
    	location.href="updateCompany.cm"
    	
    }
    
    
    
</script>
</body>
</html>