<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/chat.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.4.0/sockjs.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<div class="open-button">
	<i class="far fa-comment-alt chaticon"></i>
</div>

<jsp:include page="chat_pop_list.jsp"/>
<jsp:include page="chat_pop_room.jsp"/>
 
<script>
var wss;
var msg = $(".chathistory");

$(function() {
	openSocket();
});

/* 알림 */
let sock = new SockJS("<c:url value="/notice"/>");
sock.onmessage = onMessage;
sock.onclose = onClose;
sock.onopen = onOpen;

var cnt = 0;
var flag = true;
var objflag = false;

function onClose(event) {
	falg = true;
}

function onOpen(event) {
	sock.send('${ loginUser.empDivNo }');
}

function sendMessage() {
	
}

function onMessage(msg) {
	var data = JSON.parse(msg.data);

	if(flag) {
		cnt = data;
		flag = false;
	}
	
	var num;
	
	if(typeof(data) == 'object' && objflag) {
		cnt = num;
		toastMessage(data[0].ALRAM_TITLE);
		objflag = false;
	}
	
	if(typeof(data) == 'number') {
		num = data;
		if(cnt < num)
			objflag = true;
	}
}

function toastMessage(data) {
	toastr.options = {
			"positionClass" : "toast-bottom-left",
			"onclick": function() {
				location.href="selectNotice.no";
			}
	}
	toastr.info(data);
}
/* 채팅 */
function openSocket() {
	if(wss !== undefined && wss.readyState !== WebSocket.CLOSED) {
		return;
	}
	
	wss = new WebSocket("wss://localhost:8443/cw/echo");
	
	wss.onopen = function(event) {
		if(event.data == undefined) return;
	};
	
	wss.onmessage = function(event) {
		console.log("onmsg")
		console.log(event.data)
		var flag = false;
		var roomNo = event.data.substring(event.data.indexOf('@') + 1, event.data.lastIndexOf('@'));
		var eNo = event.data.substring(event.data.indexOf('$') + 1, event.data.lastIndexOf('$'));
		
		if(event.data.startsWith('msg')) {
			$(".chatlistline").each(function() {
				if($(this).find('input:hidden').val() == roomNo) {
					flag = true;
				}
			});
			
			$.ajax({
				url:"selectUnreadCount",
				data: {
					eNo:'${loginUser.empDivNo}',
					roomNoList: roomNo
				},
				success: function(data) {
					console.log(data)
					$(".chatlistline").each(function() {
						if($(this).find('input:hidden').val() == roomNo && eNo != '${loginUser.empDivNo}') {
							console.log(data.unread[0]);
							$(this).find('.chatcount').text(data.unread[0] + 1);
							$(this).find('.chatcount').show();
							$(this).find(".chattext").children('p').eq(1).html(event.data.split(":")[1])
						}
					});
				}
			});
			
			if(!flag) {
				$(this).find('.chatcount').text(1);
			}
		}
		
		
		writeResponse(event.data);
	}
	
	wss.onclose = function(event) {
		writeResponse("Connection closed");
	}
}

function send() {
	var text = $("#msg").val();
	var roomNo = $("#chatRoomNo").val();
	
	$.ajax({
		url:"insertChatting",
		type:"post",
		data: {
			chat:"@" + roomNo + "@" + "$" + '${ loginUser.empDivNo }' + "$" + ":" +text
		},
		success: function(data) {
			$(".chathistory").append('<div class="mychat"><div class="innerchattime">' + data.chat.chatDate.split(" ")[1] + (data.chat.chatDate.split(" ")[2] == '오후' ? 'PM' : 'AM' )  + '</div><div class="chatbox">' + data.chat.chatCnt + '</div></div>');
			$(".chathistory").scrollTop($(".chathistory").prop('scrollHeight'))
			
			$(".chatlistline").each(function() {
				if($(this).find('#roomNo').val() == roomNo) {
					$(this).children('.chattext').children('p').eq(1).html(text);
					$(this).children('.chattime').children('p').html(data.chat.chatDate.split(" ")[1].split(":")[0] + " : " + data.chat.chatDate.split(" ")[1].split(":")[1] + (data.chat.chatDate.split(" ")[2] == '오후' ? ' PM' : ' AM' ));
					$(this).find(".chatcount").hide();
				}
			});
		}
	})
	
	if(text != "") {
		wss.send("@" + roomNo + "@" + "$" + '${ loginUser.empDivNo }' + "$" + ":" +text);
		$("#msg").val("");
	}
}

function closeSocket() {
	wss.close();
}

function writeResponse(text) {
	var dt = new Date();
	var time = "";
	var ap = "";
	if(dt.getHours() / 12 > 1) {
		ap = 'PM';
	} else {
		ap = 'AM';
	}
	
	if(dt.getHours() > 12) {
		time = '0' + (dt.getHours() - 12);
	} else {
		time = dt.getHours();
	}
	
	if(dt.getMinutes() < 10) {
		time += " : 0" + dt.getMinutes() + " " + ap;
	} else {
		time += " : " + dt.getMinutes() + " " + ap;
	}
	
	if(text.substring(text.indexOf("$") + 1 , text.lastIndexOf("$")) != '${loginUser.empDivNo}') {
		$.ajax({
			url:"selectReceiveChatting",
			data:{
				text:text
			},
			success:function(data) {
				if(data.emp != null) {
					if(data.empDivNo != '${loginUser.empDivNo}' && typeof(data) == 'object') {
						$(".chathistory").append('<div class="yourchat"><div class="chatname">' + data.emp + '</div><div class="chatbox">' + text.split(":")[1] + '</div><div class="innerchattime">' + time + '</div></div>');
						$(".chathistory").scrollTop($(".chathistory").prop('scrollHeight'));
					}
				}
			}
		})
	}
}

<%-- 채팅 열고 닫기 이벤트 --%>
$(".open-button").click(function() { 
	var dis = $(".chat").css("display");
	
	if(dis === "block") {
		$(".chat").css("display","none");
	} else if (dis === "none") {
		$(".chat").css("display","block");
	}
});

<%-- esc로 채팅 닫기 이벤트 --%>
$(document).on("keydown", "body", function(e) {
	if(e.keyCode === 27) {
		if($("#filebox_pop").css("display") == "inline-block") {
			$("#filebox_pop").css("display","none");
		} else if($("#chatForm").css("display") == "block") {
			$("#chatForm").css("display","none");
			$(".chathistory").children().remove();
		} else if($(".chat").css("display") == "block") {
			$(".chat").css("display","none");
		}
	}
});
</script>