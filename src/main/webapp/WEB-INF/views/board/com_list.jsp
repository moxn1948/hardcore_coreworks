<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
	파일설명 : 전체 공지게시판
--%>

<jsp:include page="../inc/board_menu.jsp" />
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>

<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
<c:if test="${ not empty sessionScope.loginUser }">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>전체 공지사항</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <form action="searchComList.bo" method="get">
	                    <div class="search">
		                    <div class="main_cnt">
		                        <select name="searchCondition" id="searchCondition">
		                            <option value="title">제목</option>
		                            <option value="writer">작성자</option>
		                            <option value="cnt">내용</option>
		                        </select>
		                        <input type="search" name="title" id="searchValue">
		                        <!-- 색 있는 버튼 -->
		                        <button class="btn_solid_main" type="submit">검색</button>
		                    </div>
		                </div>
	                </form>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn com_tbl">
                             	<colgroup>
                                    <col style="width: 8%;">
                                    <col style="width: 82%;">
                                    <col style="width: 10%;">
                                </colgroup>
                                
                                <c:if test="${ comList.size() <= 0 }">
                                	<tr>
                                		<td colspan="2" style="text-align: center;">
                                			등록된 전체 공지사항이 없습니다.
                                		</td>
                                	</tr>
                                </c:if>
                                	<c:forEach var="r" items="${ readMustList }">
		                                <tr style="background: #f7f7f7;">
								            <td class="board_td"><div class="read_must">필독</div></td>
								            <td class="board_td">
								            	<div>
									            	<form action="detailBoard.bo" method="post">
										                <c:if test="${ r.boardReadStatus == null }">
											                <a class="board_main">
											                		${ r.btitle }
											                </a>
											                <input type="text" name="bno" value="${ r.bno }" hidden>
											                <input type="text" name="btype" value="${ r.btype }" hidden>
										                </c:if>
										                <c:if test="${ r.boardReadStatus != null }">
										                	<a class="board_main" style="color: #afb0b7;">
											                		${ r.btitle }
											                </a>
											                <input type="text" name="bno" value="${ r.bno }" hidden>
											                <input type="text" name="btype" value="${ r.btype }" hidden>
										                </c:if>
										                <div class="board_write">
											                <span>${ r.bwriter }</span>
											                <span>조회수 ${ r.hits }</span>
											                <span>댓글 ${ r.replyCount }</span>
										                </div>
									                </form>
								                </div>
								            </td>
								            <td class="board_td">
								            	<div class="board_time bdate"><fmt:formatDate value="${ r.bdate }" pattern="YY/MM/dd"/></div>
								            	<div class="board_time btime">${ r.btime }</div>
								            </td>
		                                </tr>
		                            </c:forEach>
	                            
	                           <c:forEach var="c" items="${ comList }">
	                                <tr>
	                                	<td class="board_td">
	                                		<c:if test='${ c.essenYn.equals("Y") }'>
	                                			<div class="read_must">필독</div>
	                                		</c:if>
	                                		
	                                		<c:if test='${ c.essenYn.equals("N") }'>
	                                			<div class="read_must not">필독</div>
	                                		</c:if>
	                                	</td>
	                                	<td class="board_td">
							            	<div class="no_must_read">
		                                	<form action="detailBoard.bo" method="post">
								                <c:if test="${ c.boardReadStatus == null }">
										            <a class="board_main">
										            	${ c.btitle }
										            </a>
										            <input type="text" name="bno" value="${ c.bno }" hidden>
											        <input type="text" name="btype" value="${ c.btype }" hidden>
									            </c:if>
									            
									            <c:if test="${ c.boardReadStatus != null }">
									            	<div class="board_main" style="color: #afb0b7;">
										        		${ c.btitle }
										            </div>
										            <input type="text" name="bno" value="${ c.bno }" hidden>
											        <input type="text" name="btype" value="${ c.btype }" hidden>
									            </c:if>
									                
								                <div class="board_write">
										            <span>${ c.bwriter }</span>
									                <span>조회수 ${ c.hits }</span>
									                <span>댓글 ${ c.replyCount }</span>
								                </div>
								                </form>
							                </div>
							            </td>
							            <td class="board_td">
							            	<div class="board_time bdate"><fmt:formatDate value="${ c.bdate }" pattern="YY/MM/dd"/></div>
								            <div class="board_time btime">${ c.btime }</div>
							            </td>
	                                </tr>
	                            </c:forEach>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        
                        <!-- <<, < -->
                        <c:if test="${ pi.currentPage > 1 }">
							<c:url var="blistBack" value="comList.bo">
								<c:param name="currentPage" value="${ pi.currentPage - 1 }"/>
							</c:url>
							<c:url var="blistStart" value="comList.bo">
								<c:param name="currentPage" value="${ pi.startPage }"/>
							</c:url>
							<li class="pager_com pager_arr first"><a href="${ blistStart }">&#x003C;&#x003C;</a></li>
							<li class="pager_com pager_arr prev"><a href="${ blistBack }">&#x003C;</a></li>
                        </c:if>
                        <c:if test="${ pi.currentPage <= 1 }">
							<li class="pager_com pager_arr first"><a onclick="return false;">&#x003C;&#x003C;</a></li>
							<li class="pager_com pager_arr prev"><a onclick="return false;">&#x003C;</a></li>
                        </c:if>
                        
                        <!-- 페이징 -->
                        <c:forEach var="p" begin="${ pi.startPage }" end="${ pi.endPage }">
							<c:if test="${ p eq pi.currentPage }">
								<li class="pager_com pager_num on"><a href="javascrpt: void(0);">${ p }</a></li>
							</c:if>
							<c:if test="${ p ne pi.currentPage }">
								<c:url var="blistCheck" value="comList.bo">
									<c:param name="currentPage" value="${ p }"/>
								</c:url>
								<li class="pager_com pager_num"><a href="${ blistCheck }">${ p }</a></li>
							</c:if>
						</c:forEach>
                        
                        <!-- >, >> -->
                        <c:if test="${ pi.currentPage < pi.endPage }">
							<c:url var="blistNext" value="comList.bo">
								<c:param name="currentPage" value="${ pi.currentPage + 1 }"/>
							</c:url>
							<c:url var="blistEnd" value="comList.bo">
								<c:param name="currentPage" value="${ pi.endPage }"/>
							</c:url>
							<li class="pager_com pager_arr next"><a href="${ blistNext }">&#x003E;</a></li>
	                        <li class="pager_com pager_arr end"><a href="${ blistEnd }">&#x003E;&#x003E;</a></li>
						</c:if>
						<c:if test="${ pi.currentPage >= pi.endPage }">
							<li class="pager_com pager_arr next"><a onclick="return false;">&#x003E;</a></li>
	                        <li class="pager_com pager_arr end"><a onclick="return false;">&#x003E;&#x003E;</a></li>
						</c:if>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>
</c:if>



<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>

    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        
        $('a').css('text-decoration', 'none');
        
        let bdate = document.getElementsByClassName('bdate');
        const today = new Date();
        let month = (today.getMonth()+1).toString();
        let day = today.getDate().toString();
        
        if(month < 10) {
        	month = '0' + month;
        }
        
        if(day < 10) {
        	day = '0' + day;
        }
        
        const todayFormat = today.getFullYear()+'/'+month+'/'+day;
        
        const todayFormat1 = todayFormat.substr(2, todayFormat.length-2); // 진짜 사용
        console.log(todayFormat1);
        
        for(let i=0; i<bdate.length; i++) {
        	console.log(bdate[i].innerHTML);
        	
        	if(todayFormat1 === bdate[i].innerHTML) {
        		bdate[i].setAttribute('hidden', true);
        	} else {
        		document.getElementsByClassName('btime')[i].setAttribute('hidden', true);
        	}
        }
        
        console.log(bdate);
        
        $(".board_main").click(function() {
        	$(this).parents('form').submit();
        });
    });
    

    
    $(document).on('change', '#searchCondition', function(event) {
        let condition = $('#searchCondition').val();
        console.log(condition);
        $('#searchValue').attr('name', condition);
    });
</script>
</body>
</html>
