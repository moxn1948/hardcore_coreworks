<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
	파일설명 : 팀 공지게시판
--%>

<jsp:include page="../inc/board_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
<c:if test="${ not empty sessionScope.loginUser }">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>부서 공지사항</h2></div>
                    
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="search_filter">
	                    <select>
	                    	<option>부서명</option>
	                    </select>
	                    <div class="main_cnt">
	                        <select name="" id="">
	                            <option value="">제목</option>
	                            <option value="">작성자</option>
	                            <option value="">내용</option>
	                        </select>
	                        <input type="text" name="" id="">
	                        <!-- 색 있는 버튼 -->
	                        <button class="btn_solid_main" type="submit">검색</button>
	                    </div>
                    </div>
                    
                    
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn com_tbl">
                             	<colgroup>
                                    <col style="width: 8%;">
                                    <col style="width: 82%;">
                                    <col style="width: 10%;">
                                </colgroup>
                                <c:if test="${ deptList.size() <= 0 }">
                                	<tr>
                                		<td colspan="2" style="text-align: center;">
                                			등록된 부서 공지사항이 없습니다.
                                		</td>
                                	</tr>
                                </c:if>
                                	<c:forEach var="r" items="${ readMustList }">
		                                <tr style="background: #f7f7f7;">
								            <td class="board_td"><div class="read_must">필독</div></td>
								            <td class="board_td">
								            	<div>
								            	<form action="detailBoard.bo" method="post">
									                <c:if test="${ r.boardReadStatus == null }">
										                <a class="board_main">${ r.btitle }</a>
										                <input type="text" name="bno" value="${ r.bno }" hidden>
											            <input type="text" name="btype" value="${ r.btype }" hidden>
									                </c:if>
									                <c:if test="${ r.boardReadStatus != null }">
									                	<a class="board_main" style="color: #afb0b7;">${ r.btitle }</a>
											            <input type="text" name="bno" value="${ r.bno }" hidden>
											            <input type="text" name="btype" value="${ r.btype }" hidden>
									                </c:if>
										                <div class="board_write">
											                <span>${ r.bwriter }</span>
											                <span>조회수 ${ r.hits }</span>
											                <span>댓글 ${ r.replyCount }</span>
										                </div>
									                </form>
								                </div>
								            </td>
								            <td class="board_td">
								            	<div class="board_time bdate"><fmt:formatDate value="${ r.bdate }" pattern="YY/MM/dd"/></div>
								            	<div class="board_time btime">${ r.btime }</div>
								            </td>
		                                </tr>
		                            </c:forEach>
	                            
	                           <c:forEach var="c" items="${ deptList }">
	                                <tr>
	                                	<td class="board_td">
	                                		<c:if test='${ c.essenYn.equals("Y") }'>
	                                			<div class="read_must">필독</div>
	                                		</c:if>
	                                		
	                                		<c:if test='${ c.essenYn.equals("N") }'>
	                                			<div class="read_must team_name">${ c.belongName }</div>
	                                		</c:if>
	                                	</td>
	                                	<td class="board_td">
							            	<div class="no_must_read">
							            	<form action="detailBoard.bo" method="post">
								                <c:if test="${ c.boardReadStatus == null }">
										            <a class="board_main">
										            	${ c.btitle }
										            </a>
										            <input type="text" name="bno" value="${ c.bno }" hidden>
											        <input type="text" name="btype" value="${ c.btype }" hidden>
									            </c:if>
									            
									            <c:if test="${ c.boardReadStatus != null }">
									            	<div class="board_main" style="color: #afb0b7;">
										        		${ c.btitle }
										            </div>
										            <input type="text" name="bno" value="${ c.bno }" hidden>
											        <input type="text" name="btype" value="${ c.btype }" hidden>
									            </c:if>
								                <div class="board_write">
										            <span>${ c.bwriter }</span>
									                <span>조회수 ${ c.hits }</span>
									                <span>댓글 ${ c.replyCount }</span>
									                <input type="text" name="belong" value="${ c.belong }" hidden>
								                </div>
								                </form>
							                </div>
							            </td>
							            <td class="board_td">
							            	<div class="board_time bdate"><fmt:formatDate value="${ c.bdate }" pattern="YY/MM/dd"/></div>
								            <div class="board_time btime">${ c.btime }</div>
							            </td>
	                                </tr>
	                            </c:forEach>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        
                        <!-- <<, < -->
                        <c:if test="${ pi.currentPage > 1 }">
							<c:url var="blistBack" value="deptList.bo">
								<c:param name="currentPage" value="${ pi.currentPage - 1 }"/>
							</c:url>
							<c:url var="blistStart" value="deptList.bo">
								<c:param name="currentPage" value="${ pi.startPage }"/>
							</c:url>
							<li class="pager_com pager_arr first"><a href="${ blistStart }">&#x003C;&#x003C;</a></li>
							<li class="pager_com pager_arr prev"><a href="${ blistBack }">&#x003C;</a></li>
                        </c:if>
                        <c:if test="${ pi.currentPage <= 1 }">
							<li class="pager_com pager_arr first"><a onclick="return false;">&#x003C;&#x003C;</a></li>
							<li class="pager_com pager_arr prev"><a onclick="return false;">&#x003C;</a></li>
                        </c:if>
                        
                        <!-- 페이징 -->
                        <c:forEach var="p" begin="${ pi.startPage }" end="${ pi.endPage }">
							<c:if test="${ p eq pi.currentPage }">
								<li class="pager_com pager_num on"><a href="javascrpt: void(0);">${ p }</a></li>
							</c:if>
							<c:if test="${ p ne pi.currentPage }">
								<c:url var="blistCheck" value="deptList.bo">
									<c:param name="currentPage" value="${ p }"/>
								</c:url>
								<li class="pager_com pager_num"><a href="${ blistCheck }">${ p }</a></li>
							</c:if>
						</c:forEach>
                        
                        <!-- >, >> -->
                        <c:if test="${ pi.currentPage < pi.endPage }">
							<c:url var="blistNext" value="deptList.bo">
								<c:param name="currentPage" value="${ pi.currentPage + 1 }"/>
							</c:url>
							<c:url var="blistEnd" value="deptList.bo">
								<c:param name="currentPage" value="${ pi.endPage }"/>
							</c:url>
							<li class="pager_com pager_arr next"><a href="${ blistNext }">&#x003E;</a></li>
	                        <li class="pager_com pager_arr end"><a href="${ blistEnd }">&#x003E;&#x003E;</a></li>
						</c:if>
						<c:if test="${ pi.currentPage >= pi.endPage }">
							<li class="pager_com pager_arr next"><a onclick="return false;">&#x003E;</a></li>
	                        <li class="pager_com pager_arr end"><a onclick="return false;">&#x003E;&#x003E;</a></li>
						</c:if>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>
</c:if>


<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(3).addClass("on").addClass("open");
        
        $('a').css('text-decoration', 'none');
        
        let bdate = document.getElementsByClassName('bdate');
        const today = new Date();
        const todayFormat = today.getFullYear()+'/'+today.getMonth()+1+'/'+today.getDate();
        
        const todayFormat1 = todayFormat.substr(2, todayFormat.length-2); // 진짜 사용
        
        for(let i=0; i<bdate.length; i++) {
        	console.log(bdate[i].innerHTML);
        	
        	if(todayFormat1 === bdate[i].innerHTML) {
        		bdate[i].setAttribute('hidden', true);
        	} else {
        		document.getElementsByClassName('btime')[i].setAttribute('hidden', true);
        	}
        }
        
        $(".board_main").click(function() {
        	$(this).parents('form').submit();
        })
    });
</script>
</body>
</html>
