<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	파일설명 : 게시물 작성
--%>

<jsp:include page="../inc/board_menu.jsp" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/ui/trumbowyg.min.css" rel='stylesheet' type='text/css' />
<link href="${ contextPath }/resources/css/trumbowyg_table.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/trumbowyg.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/plugins/table/trumbowyg.table.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/langs/ko.min.js"></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>공지사항 수정</h2></div>
<!--                     
	                    <div id="btnSet">
	                    	<a href="#tmp_save" rel="modal:open" class="button btn_white">임시저장함</a>
		                    <button type="button" class="btn_white tmp_save">임시저장</button>
	                    </div> -->
	                    
                   <form action="updateBoard.bo" method="post" encType="multipart/form-data">
	                    <input type="text" value="${ upBoard.bno }" name="bno" hidden>
	                    <div class="new_board_head">
	                    	<div id="check">
	                        	<input type="checkbox" id="mustRead" name="mustRead">
	                        	<label for="mustRead">필독 여부</label>
	                        	<input type="text" class="essenYn" name="essenYn" value="N" hidden>
	                        </div>
	                        
	                        <div id="kinds">
		                        <label>게시판 선택</label>
		                        <select id="btype" name="btype">
		                            <option value="ALL">전체</option>
		                            <option value="TEAM">팀</option>
		                        </select>
	                        </div>
							
							<div id="title">
		                        <label for="">제목</label>
		                        <input type="text" value="${ upBoard.btitle }" name="btitle">
	                        </div>
                        </div>
                        
                        <!-- 에디터 -->
						<div id="editer">${ upBoard.bcnt }</div>
						<input type="text" id="contents" name="bcnt" hidden>
 						
 						<!-- 첨부파일 -->
	 						<div class="attachment">
		 						첨부파일
		 						<button type="button" class="btn_solid_pink addAction" onclick="addAtt(this)"><i class="fas fa-plus"></i></button>
		 						<!--input box-->
		 						<div class="attActions">
		 						<c:if test="${ at.size() <= 0 }">
		 							<div class="attAction">
										<input type="text" class="upload_text" value="" readonly="readonly">
										<div class="upload-btn_wrap">
										  <button type="button" class="btn_main btn_choise"><input type="file" name="attachments" class="input_file" value="">선택</button>
										  <button type="button" class="btn_white btn_file_init">취소</button>
										</div>
									</div>
		 						</c:if>
		 						<c:if test="${ at.size() > 0 }">
			 						<c:forEach var="i" items="${ at }" varStatus="status" >
			 							<div class="attAction">
											<input type="text" class="upload_text" value="${ i.originName }" readonly="readonly">
											<div class="upload-btn_wrap">
												<input type="text" name="fileNo" class="fileNo" value="${ i.fileNo }" hidden>
											  <button type="button" class="btn_main btn_choise"><input type="file" name="attachments" class="input_file" value="${ contextPath }/resources/uploadFiles/${ i.changeName }">선택</button>
											  <c:if test="${ status.index == 0 }">
											  	<button type="button" class="btn_white btn_file_init">취소</button>
											  </c:if>
											  <c:if test="${ status.index > 0 }">
												  <button type="button" class="btn_white btn_delete">삭제</button>
											  </c:if>
											</div>
										</div>
									</c:forEach>
								</c:if>
								</div>
		 					</div>
							<div class="endBtnSet">
		                        <button type="button" class="btn_pink btn_cancel">취소</button>
								<button type="submit" class="btn_main btn_add" onclick="return boardUpdate()">수정</button>
							</div>
							<input type="text" name="empDivNo" value="${ sessionScope.loginUser.empDivNo }" hidden>
                    </form>
                </div>
            </div>
        </div>
    </main>
</div>

<!-- popup include -->
<div id="tmp_save" class="modal">
	<jsp:include page="board_pop_tmp.jsp" />
</div>
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>

    $(function(){
    	$('#editer').trumbowyg({
		    btns: [['viewHTML'],
		        ['undo', 'redo'], // Only supported in Blink browsers
		        ['formatting'],
		        ['strong', 'em', 'del'],
		        ['superscript', 'subscript'],
		        ['link'],
		        ['insertImage'],
		        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
		        ['unorderedList', 'orderedList'],
		        ['horizontalRule'],
		        ['removeformat'],
		        ['table']],
		    lang: 'ko',
		    plugins: {
		        table: {
		        	rows: 10,
		        	columns: 10,
		        	styler: 'tbl_form',
		        }
		    },
		});
    	
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(5).addClass("on");
        
        // 첫 첨부파일은 삭제불가
        $('.attAction:nth-of-type(1) .btn_delete').hide();
        
        // 임시저장클릭시 임시저장으로 이동됨(실 이동은 목록페이지)
        $('.tmp_save').click(function() {
        	alert('임시저장되었습니다.');
        });
        
        // 필독여부가 Y라면 체크, 아니면 미체크
        const essenYn = "${ upBoard.essenYn }";
        if(essenYn === 'Y') {
        	$("#mustRead").attr('checked', true);
        	$('.essenYn').val('Y');
        } else {
        	$("#mustRead").attr('checked', false);
        	$('.essenYn').val('N');
        }
        
    	// 취소버튼 클릭 시 file과 text는 초기화됨
        $(document).on('click', '.btn_file_init', function(event) {
        	document.getElementsByClassName('input_file')[0].value = '';
        	$(this).parents('.attAction').children().eq(0).val('');
        });
    	
    	$(document).on('change', '.input_file', function(event) {
    		$(this).parents('.upload-btn_wrap').children().eq(0).val('');
    	})
        
        // 종류가 team이라면 team 선택, all이라면 all 선택
        const btype = "${ upBoard.btype }";
        if(btype === 'ALL') {
        	$('#btype').val(btype).attr('selected', true);
        } else {
        	$('#btype').val(btype).attr('selected', true);
        }
        
        $('#mustRead').change(function() {
        	console.log($('#mustRead').is(':checked'));
        	if($(this).is(':checked')) {
        		$('.essenYn').val('Y');
        	} else {
        		$('.essenYn').val('N');
        	}
        	console.log($('.essenYn').val());
        })
	});
    
    $(document).on('change', '#mustRead', function(event) {
    	
    })
    
 	// 파일 업로드시 파일이름이 보여짐
    $(document).on("change",".input_file",function(event){
    	let i = $(this).val();
    	i = i.substring(12)
    	
		$(this).parents('.attAction').children('.upload_text').val(i);
    });
 	
 	// 첨부파일 삭제
    $(document).on("click",".btn_delete",function(event){
    	$(this).parents('.attAction').remove();
    });
    
    // 첨부파일 추가
    function addAtt() {
    	let attActionClone = '<div class="attAction">';
			attActionClone += '<input type="text" class="upload_text" readonly="readonly">'
			attActionClone += '<div class="upload-btn_wrap">'
			attActionClone += '<button class="btn_main btn_choise"><input type="file" name="attachments" class="input_file">선택</button>'
			attActionClone += '<button type="button" class="btn_white btn_delete" >삭제</button>'
			attActionClone += '</div></div>';
		

		$('.attActions').append(attActionClone);
		// 두번째부터 삭제버튼 생성
		$('.attAction:nth-of-type(n+2) .btn_delete').show();
 	}

    // 작성란이 비어있으면 저장실패, 뭐라도 작성되있다면 등록가능
    function boardUpdate() {
    	if($('#title>input').val() === '' || $('#editer').html() === '' || $('#editer').html() === '<p><br></p>') {
    		alert('작성하지 않은 항목이 있습니다.');
    		return false;
		} else {
			if(confirm('이대로 수정하시겠습니까?')) {
				$('#contents').val($('#editer').html());
				return true;	
			} else {
				return false;
			}
		}
    }
    
   
</script>
</body>
</html>
