<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
	공지사항 상세보기
--%>

<jsp:include page="../inc/board_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
<c:if test="${ not empty sessionScope.loginUser }">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>공지사항 상세내용</h2></div>
                    
                    <!--  작성자 혹은 관리자의 경우 삭제나 수정가능 -->
                    <c:if test="${ sessionScope.loginUser.auth ne 'EMP' || bdetail.empDivNo == sessionScope.loginUser.empDivNo }">
	                    <div class="btn_set">
	                        <button class="btn_pink btn_delete">삭제</button>
	                    	<button class="btn_blue btn_update">수정</button>
	                    </div>
                    </c:if>
                    
                    <div class="title">
                    <c:if test="${ bdetail.essenYn eq 'Y' }">
                    	<span class="must_read">필독</span>
                    </c:if>
                     <c:if test="${ bdetail.essenYn eq 'N' }">
                     	<c:if test="${ bdetail.btype eq 'ALL' }">
                     		<span></span>
                     	</c:if>
                     	<c:if test="${ bdetail.btype eq 'TEAM' }">
                     		<span class="read_must team_name">${ bdetail.belongName }</span>
                     	</c:if>
                    </c:if>
						<span class="detail_title">${ bdetail.btitle }</span>
					</div>						            
						
					<div class="board_info">
						<div>${ bdetail.bwriter }</div>
						<div>조회수 ${ bdetail.hits }</div>
						<div class="board_time bdate">등록일 <p:formatDate value="${ bdetail.bdate }" pattern="YY/MM/dd"/></div>
						<div class="board_time btime">등록시간 ${ bdetail.btime }</div>
					</div>
					
					<!-- 
						내용넣는 칸
						html째로 넣음 
					-->
					<div id="editer">
						<div class="board_cnt">${ bdetail.bcnt }</div>
						<c:if test="${ not empty attList }">
							<div class="att_list">
								<span>첨부파일</span>
								<div class="att">
									<c:forEach var="al" items="${ attList }">
										<div class="att_info">
											<span class="att_title"><a href="download.bo?fileNo=${ al.fileNo }">${ al.originName }</a></span>
										</div>
									</c:forEach>
								</div>
							</div>
						</c:if>
					
					
					<!-- 댓글 공간 -->
					
						<div class="comment_area">
							<div class="comment_count">댓글 ${ bdetail.replyCount }개</div>
							
							<c:if test="${ bdetail.replyCount > 0 }">
								<c:forEach var="i" items="${ replyList }">
									<!-- 댓글&답글 -->
									<div class="comment_and_reply">
										<!-- 댓글 -->
										<div class="comment">
											<div class="comment_wt">
												<span>${ i.empName }</span>
												<span class="time"><p:formatDate value="${ i.replyDate }" pattern="yy/MM/dd"/></span>
												<div class="btn_set comment_set">
													<c:if test="${ sessionScope.loginUser.auth ne 'EMP' || 
															sessionScope.loginUser.empDivNo == i.empDivNo }">
														<button class="btn_pink btn_comment_delete">삭제</button>
														<button class="btn_blue btn_comment_update">수정</button>
													</c:if>
												</div>
												<div class="replyNo" hidden>${ i.replyNo }</div>
											</div>
											<div class="comment_content">${ i.replyCnt }</div>
										</div> <!-- 댓글 -->
										<!-- 답글 -->
									</div> <!-- 댓글&답글 -->
								</c:forEach>
							</c:if>
								<!-- 댓글 작성란 -->
									<div class="write_comment_area reply_area">
										<textarea class="insert_comment" id="replyCnt" placeholder="댓글을 입력하세요."></textarea>
										<div class="btn_area">
											<button class="btn_pink in_reply btn_comment_add">등록</button>
										</div>
									</div> <!-- 댓글 작성란 -->
							</div> <!-- 댓글 공간 -->
					</div>         
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- 답글 폼 -->
<div class="reply reply_ex">
	<img src="${ contextPath }/resources/images/Arrow.jpg" class="arrow">
	<div class="reply_wt">
	 	 <span>${ sessionScope.loginUser.empName }</span>
	 </div>
	<div class="write_comment_area write_reply_area">
		<textarea class="insert_rereply" placeholder="답글을 입력하세요."></textarea>
		<div class="btn_area reply_btn_area">
			<button class="btn_white in_reply btn_reply_cancel">취소</button>
			<button class="btn_blue btn_reply_add">저장</button>
		</div>
	</div>
</div>
</c:if>
<script>
$(function(){
    // 주 메뉴 분홍색 하이라이트 처리
    $("#nav .nav_list").eq(5).addClass("on");

    // 서브 메뉴 처리
    // 열리지 않는 메뉴
    //$("#menu_area .menu_list").eq(0).addClass("on");
    
    // 열리는 메뉴
    $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
    $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    
    // 서브메뉴 밑줄제거
    $('#container #menu_area .menu_list.on>a').css('text-decoration', 'none');
    
    // 날짜에 따른 날짜보여지기
    
    // 등록된 날짜
    const bdate = document.getElementsByClassName('bdate')[0].innerHTML.substr(4);
    // 현재 날짜
    const today = new Date();
    let month = (today.getMonth()+1).toString();
    let day = today.getDate().toString();
    
    console.log(typeof month)
    
    if(month < 10) {
    	month = '0' + month;
    }
    
    if(day < 10) {
    	day = '0' + day;
    }
    
    const todayFormat = today.getFullYear()+'/'+month+'/'+day;
    
    const todayFormat1 = todayFormat.substr(2, todayFormat.length-2); // 진짜 사용
    	
    	if(todayFormat1 === bdate) {
    		document.getElementsByClassName('bdate')[0].setAttribute('hidden', true);
    	} else {
    		document.getElementsByClassName('btime')[0].setAttribute('hidden', true);
    	}
    
    $(".btn_update").click(function() {
    	location.href="showUpdateBoard.bo?bno=${ bdetail.bno }"
    })
    
    $(".btn_delete").click(function() {
    	if(confirm('정말 삭제하시겠습니까?')) {
	    	location.href="deleteBoard.bo?bno=${ bdetail.bno }"
    	}
    })
    
    // 답글생성
    $(document).on("click",".btn_reply",function(event){
    	const write_reply_area = $('.reply.reply_ex').clone().css('display', 'flex');
    	
    	$(this).parents('.comment_and_reply').append(write_reply_area);
    	
    	$('.btn_reply').remove();
    });
    
    // 답글취소 클릭 시 작성 폼 제거
    $(document).on("click",".btn_reply_cancel",function(event){
    	$('.comment_and_reply').children('.comment').append('<button class="btn_white btn_reply">답글</button>');
    	$(this).parents('.reply_ex').remove();
    });
    
    // 댓글 등록
    $(document).on('click', '.btn_comment_add', function(event) {
    	const comment_count0 = $('.comment_count').html();
		const comment_count1 = comment_count0.substr(3);
		let replyCount = Number(comment_count1.replace('개', ''));
		console.log(replyCount);
		
    	if($(this).parents('.write_comment_area').children('.insert_comment').val().length <= 0) {
    		alert('내용을 입력해주세요.');
    	} else {
    		$.ajax({
    			type: "post",
    			url: "insertReply.bo",
    			data: {
    				bno: "${ bdetail.bno }",
    				replyCnt: $('#replyCnt').val(),
    			},
    			success: function(data) {
    				const reply = JSON.parse(data);
    				alert('댓글이 등록되었습니다.');
    				replyCount++;
    				$(".insert_comment").val("");
    				
    				const date0 = reply.replyDate;
    				const date1 = date0.substring(2);
    				
    				function replaceAll(str, searchStr, replaceStr) {
    					return str.split(searchStr).join(replaceStr);
    				}

    				const date2 = replaceAll(date1, '-', '/');
    				
    				console.log(reply);
    				
    				let html = "";
    				html += "<div class='comment_and_reply'>";
    				html += "<div class='comment'>";
    				html += "<div class='comment_wt'>";
    				html += "<span>"+reply.empName+"</span>";
    				html += "<span class='time'>"+date2+"</span>";
    				html += "<div class='btn_set comment_set'>";
    				html += "<button class='btn_pink btn_comment_delete'>삭제</button>";
    				html += "<button class='btn_blue btn_comment_update'>수정</button>";
    				html += "</div><div class='replyNo' hidden>"+reply.replyNo+"</div></div>";
    				html += "<div class='comment_content'>"+reply.replyCnt+"</div>";
    				html += "</div></div>";
				
    				$('.write_comment_area').before(html);
    				
    				$('.comment_count').html("댓글 "+replyCount+"개");
    			},
    			error: function(error) {
    				console.log(error)
    				alert('등록실패!');
    			}
    		})
    	} 
    })
    
    // 댓글 수정폼 생성
    $(document).on('click', '.btn_comment_update', function(event) {
    	const replyCnt = $(this).parents('.comment').children().eq(1).html();
    	
    	let clone = "";
    	clone += "<div class='write_comment_area reply_area'>";
    	clone += "<textarea class='insert_comment update_reply_cnt' id='replyCnt'>"+replyCnt+"</textarea>";
    	clone += "<div class='btn_area'>";
    	clone += "<button class='btn_blue in_reply btn_reply_update'>수정</button>";
    	clone += "</div></div>";
    	
    	
    	$('.btn_comment_update').hide();
    	$(this).parents('.comment').children().eq(1).remove();
    	$(this).parents('.comment').children().eq(0).after(clone);
    });
    
    // 댓글 수정
    $(document).on('click', '.btn_reply_update', function(event) {
    	const $comment = $(this).parents('.comment');
    	const $replyArea = $(this).parents('.reply_area');
    	$.ajax({
    		url: "replyUpdate.bo",
    		type: "post",
    		data: {
    			replyCnt: $('.update_reply_cnt').val(),
    			replyNo: $comment.children('.comment_wt').children('.replyNo').html(),
    		},
    		success: function(data) {
    			const reply = JSON.parse(data);
    			console.log(reply);
    			if(reply.message === 'success') {
    				alert('댓글이 수정되었습니다.');
    				// 변경내용 변수에 저장
    				const replyCnt = reply.replyCnt;
    				
    				// 내용컨테이너에 변경내용 이식
    				$comment.children('.comment_wt').after("<div class='comment_content'>"+replyCnt+"</div>");
    				
    				// 댓글수정창 제거
    				$replyArea.remove();
    				
    				// 수정버튼 생성
    				$('.btn_blue.btn_comment_update').show();
    				
    			} else {
    				alert('댓글이 수정되지않았습니다.');
    			}
    		},
    		error: function(error) {
    			console.log(2)
    		}
    	})
    });

    // 댓글제거
    $(document).on('click', '.btn_pink.btn_comment_delete', function(event) {
    	if(confirm('정말 삭제하시겠습니까?')) {
    		const $comment_and_reply = $(this).parents('.comment_and_reply');
    		const $comment = $(this).parents('.comment');
    		const replyNo = $comment.children('.comment_wt').children('.replyNo').html();
    		
    		const comment_count0 = $('.comment_count').html();
    		const comment_count1 = comment_count0.substr(3);
    		let replyCount = Number(comment_count1.replace('개', ''));
    		
    		console.log(replyCount)
    		
    		$.ajax({
    			url: "replyDelete.bo",
        		type: "post",
        		data: {
        			replyNo: Number(replyNo),
        		},
        		success: function(data) {
        			const reply = JSON.parse(data);
        			console.log(reply);
        			if(reply === 'success') {
        				alert('댓글이 삭제되었습니다.');
        				
        				replyCount--;
        				// 해당 댓글area 제거
        				$comment_and_reply.remove();
        				
        				$('.comment_count').html("댓글 "+replyCount+"개");
        				
        			} else {
        				alert('댓글이 삭제되지않았습니다.');
        			}
        		},
        		error: function(error) {
        			console.log(2)
        		}
    		});
    	}
    });
});

</script>
</body>
</html>