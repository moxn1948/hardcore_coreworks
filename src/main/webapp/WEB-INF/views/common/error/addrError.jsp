<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	
	<script>
		var msg = '${ msg }';
		var view = '${ view }';
		
		if(view == "addr_list"){
			var eNo = '${ sessionScope.loginUser.empDivNo }';
			var dNo = '${ sessionScope.loginUser.deptNo }';
			view = "addrMain.ad?eNo=" + eNo + "&dNo=" + dNo;
		}
		
		alert(msg);
		location.href = view;
	</script>
</body>
</html>