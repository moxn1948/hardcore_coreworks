<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wk.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/common.css">
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
</head>
<body>
	<div class="loginbox">
		<div class="loginlogo">
			<img src="${ contextPath }/resources/images/logo_3.png" alt="회사로고" />
		</div>
		<div class="loginform">
			<form action="login.me" method="post">
				<div class="loginid">
					<input type="text" name="empId" id="empId2" placeholder="  계정" />
				</div>
				<div class="loginpwd">
					<input type="password" name="empPwd" id="empPwd" placeholder="  비밀번호" />
				</div>
				<button class="btn_solid" id="loginbtn">로그인</button>
			</form>
		</div>
		<div class="savelogin">
			<input type="checkbox" name="saveid" id="saveid" value="계정 저장">
                     <label for="saveid">계정저장</label>
		</div>
	</div>
	<div class="logincopyright">
		@ copyright 2020 COREWORKS All right reserved
	</div>
	
<script>
   $(document).ready(function() {
      var key = getCookie("key");
      $("#empId2").val(key);
      
      if($("#empId2").val() != "") {
         $("#saveid").attr("checked", true);
      }
      
      $("#saveid").change(function() {
         if($("#saveid").is(":checked")) {
            setCookie("key", $("#empId2").val(), 7);
         } else {
            deleteCookie("key");
         }
      });
      
      $("#empId2").keyup(function() {
         if($("#saveid").is(":checked")) {
            if($("#saveid").is(":checked")) {
               setCookie("key", $("#empId2").val(), 7);
            }
         }
      }) 
   })
   
   function setCookie(cookieName, value, exdays){
       var exdate = new Date();
       exdate.setDate(exdate.getDate() + exdays);
       var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
       document.cookie = cookieName + "=" + cookieValue;
   }
    
   function deleteCookie(cookieName){
       var expireDate = new Date();
       expireDate.setDate(expireDate.getDate() - 1);
       document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
   }
    
   function getCookie(cookieName) {
       cookieName = cookieName + '=';
       var cookieData = document.cookie;
       var start = cookieData.indexOf(cookieName);
       var cookieValue = '';
       if(start != -1){
           start += cookieName.length;
           var end = cookieData.indexOf(';', start);
           if(end == -1)end = cookieData.length;
           cookieValue = cookieData.substring(start, end);
       }
       return unescape(cookieValue);
   }
</script>
</body>
</html>