<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<jsp:include page="../inc/address_menu2.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
<style>
	.flr{float: right;}
	.add_grp_btn{padding: 10px 0;font-size: 13px;font-weight: bold;color: #6382D4;}
</style>
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
            <form action="updateGroup.ad" id="updateForm" method="post">
            	<input type="hidden" name="empDivNo" value="${ sessionScope.loginUser.empDivNo }">
            	<input type="hidden" name="deptNo" value="${ sessionScope.loginUser.deptNo }">
            	<input type="hidden" name="type" value="${ type }">
                <div class="main_ctn clearfix">
                	<div class="flr">
						<button type="button" class="button btn_pink" id="canBtn">취소</button>
						<button class="button btn_main" id="submitBtn">저장</button>
                	</div>
                    <div class="menu_tit">
                    	<h2>그룹관리</h2>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt clearfix">
						<a href="javascript: void(0);" class="flr add_grp_btn" onclick="topAdd()">새 그룹 추가 +</a>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" id="addrGroupTbl">
                                <tr class="tbl_main_tit">
                                    <th>그룹명</th>
                                    <th>삭제</th>
                                </tr>
                           <c:forEach var="list" items="${ list }" varStatus="status">
								<tr>
									<td>
										<input type="hidden" name="addrNo" value="${ list.addrNo }">
										<c:if test="${status.index == 0}">
										<input type="text" class="baseGrp" name="addrgroup" value="${ list.addrgroup }" placeholder="그룹명을 입력하세요." readonly>
										</c:if>
										<c:if test="${status.index != 0}">
										<input type="text" class="" name="addrgroup" value="${ list.addrgroup }" placeholder="그룹명을 입력하세요.">
										</c:if>
									</td>
									<td><button type='button' class='btn_pink' onclick='delGrp(this)'>삭제</button></td>					
								</tr>
							</c:forEach>
			              	                  
				                                	
							</table>
	                    </div>
	                </div>
	            <!-- 메인 컨텐츠 영역 끝! -->
				</div>
            </form>
		<!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(3).addClass("on");

  		  
        $("#canBtn").on("click", function(){
    	    if(confirm("그룹 수정을 취소하시겠습니까?")){
    	 	   $("#addrMenuHeader").submit();
    	    }
        })
    });
    
    function topAdd() {
    	var str = '<tr><td><input type="hidden" name="addrNo" value="0"><input type="text" class="" name="addrgroup" placeholder="그룹명을 입력하세요."></td><td><button type="button" class="btn_pink" onclick="delGrp(this)">삭제</button></td></tr>';
    	// jOrder++
    	$(str).appendTo($(".tbl_ctn"));
    }
    
    function delGrp(val){
    	
    	if($(val).parent("td").parent("tr").index() != 1){
    		
    		var addrNo = $(val).parent("td").parent("tr").find("td:first-child").find("input[name=addrNo]").val();
	    	if(addrNo != 0){
	    		

	    		$.ajax({
	    			url:"selectGrpExistOutsider.ad",
	    			type:"post",
	    			data: {
	    				addrNo:addrNo
	    			},
	    			success:function(data) {
	    				console.log(data);	
	    				
	    				if(data.existSwitch == false){
	    		    		$(val).parent("td").parent("tr").find("td:first-child").find("input[name=addrNo]").prop("name", "delAddrNo");
	    		    		$(val).parent("td").parent("tr").find("td:first-child").find("input[name=addrgroup]").prop("name", "delAddrgroup");
	    		    		$(val).parent("td").parent("tr").hide();
	    				}else{
	    					alert("그룹에 연락처가 존재하면 삭제가 불가능합니다.");
	    				}
	   				},
	   				error:function(status) {
	   					console.log(status);
	   				}
	   			});	
	    		
	    	}else{
	    		$(val).parent("td").parent("tr").remove();	
	    	}
	    	
	    	
    	}else{
    		alert("기본 그룹은 삭제하실 수 없습니다.");
    	}
    	
    	console.log();
    }
    
    
</script>
</body>
</html>