<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${ !empty loginUser }">

<jsp:include page="../inc/address_menu.jsp" />
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- 주소검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">

            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit">
                        <h2>주소록</h2>
                        <!-- 공용주소록/내주소록일 때만 노출 -->
                        <form action="teamGroupList.ad" method="post">
                        	<input type="hidden" name="deptNo" value="${ sessionScope.loginUser.getDeptNo() }">
	                        <button type="submit" class="btn_blue main_tit_rt_btn" id="teamGrpEditBtn">그룹수정</button>
                        </form>
                        <form action="myGroupList.ad" method="post">
                        	<input type="hidden" name="empDivNo" value="${ sessionScope.loginUser.getEmpDivNo() }">
                        	<button class="btn_blue main_tit_rt_btn" id="myGrpEditBtn">그룹수정</button>
                        </form>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        <div class="main_srch_wrap">
                            <select name="" id="addrSel">
                                <option value="name">이름</option>
                                <option value="email">이메일</option>
                            </select>
                            <input type="text" name="" id="addrCtnIpt">
                            <button class="btn_solid_main" id="addrSrchBtn">검색</button>
                        </div>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" id="addrListTbl">
                                <colgroup>
                                    <col style="width: 15%;">
                                    <col style="width: 20%;">
                                    <col style="width: 20%;">
                                    <col style="width: 25%;">
                                    <col style="width: 20%;">
                                </colgroup>
                                <!-- <tr class="tbl_main_tit">
                                    <th>이름</th>
                                    <th>직책/직급</th>
                                    <th>부서</th>
                                    <th>이메일</th>
                                    <th>번호</th>
                                </tr> -->
                               <!--  <tr>
                                    <td><a href="#deptAddrPop" rel="modal:open">사내주소록팝업</a></td>
                                    <td><a href="#deptAddrPop" rel="modal:open">매니저/대리</a></td>
                                    <td><a href="#deptAddrPop" rel="modal:open">개발부</a></td>
                                    <td><a href="#deptAddrPop" rel="modal:open">bap@coreworks.info</a></td>
                                    <td><a href="#deptAddrPop" rel="modal:open">023423432</a></td>
                                </tr>
                                <tr>
                                    <td><a href="#comAddrPop" rel="modal:open">부서/내주소록팝업</a></td>
                                    <td><a href="#comAddrPop" rel="modal:open">매니저/대리</a></td>
                                    <td><a href="#comAddrPop" rel="modal:open">개발부</a></td>
                                    <td><a href="#comAddrPop" rel="modal:open">bap@coreworks.info</a></td>
                                    <td><a href="#comAddrPop" rel="modal:open">023423432</a></td>
                                </tr> -->
                            </table>
                        </div>
                    </div> 
                    <div class="pager_wrap" id="pagerArea">
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
<!--                    
                        <ul class="pager_cnt clearfix dept">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                     -->
                    </div>
                    <input type="hidden" id="currPageNum" value="1">
                    <input type="hidden" id="currAddrType" value="dept">
                    <input type="hidden" id="currAddrDeptNum" value="0">
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- popup include -->
<div id="deptAddrPop" class="modal">
	<jsp:include page="../pop/addr_dept_info_pop.jsp" />
</div>
<div id="comAddrPop" class="modal">
	<jsp:include page="../pop/addr_com_info_pop.jsp" />
</div>
<div id="addrUpdatePop" class="modal">
	<jsp:include page="../pop/addr_update_pop.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	var addrCurrentPage = 1;
	var addrCurrentType = "dept";
	var addrCurrentDept = 0;
	var addrCurrentGrp = 0;
	var endPage = 0;
	var myDeptNo = 0;
	var myEmpNo = 0;
	// var addrgroup = null;
	var addrSel = "";
	var addrCtnIpt = "";
	var srchSwitch = false;
	var updateOutAlreadyAddrNo = 0;
	var sendMailStr;
	

	var path = "${ contextPath }";
	var domain = "${ sessionScope.domain }";
	var pageS = '<ul class="pager_cnt clearfix"><li class="pager_com pager_arr first"><a href="javascrpt: void(0);"><input type="hidden" value="first">&#x003C;&#x003C;</a></li><li class="pager_com pager_arr prev"><a href="javascrpt: void(0);"><input type="hidden" value="prev">&#x003C;</a></li>';
	var pageE = '<li class="pager_com pager_arr next"><a href="javascrpt: void(0);"><input type="hidden" value="next">&#x003E;</a></li><li class="pager_com pager_arr end"><a href="javascrpt: void(0);"><input type="hidden" value="end">&#x003E;&#x003E;</a></li></ul>';
	var pageNumS = '<li class="pager_com pager_num"><a href="javascrpt: void(0);"><input type="hidden" value="';
	var pageNumB = '">';
	var pageNumE = '</a></li>';
	
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(3).addClass("on");

        var tree = $.ui.fancytree.getTree("#deptTree");
        tree.visit(function(node){
            if(node.key == "deptTit"){
                node.setExpanded(true);
            }
        });


        $(".tbl_common table tr td").on("mouseover",function(){
            $(this).parents("tr").css({
                backgroundColor : "#efefef"
            });
        }).on("mouseleave", function(){
            $(this).parents("tr").css({
                backgroundColor : "transparent"
            });
        });
        
        empAddrList(addrCurrentDept);
        $("#teamGrpEditBtn").hide();
        $("#myGrpEditBtn").hide();
    });

     
/*     $(document).on("click", "#deptTree .fancytree-title", function(){
    	var val = $(this).find("input[type=hidden]").val();
    	// console.log(addrCurrentPage);
    	addrCurrentDept = val;
    	addrCurrentType = "dept";
    	empAddrList(val);
    }); */

    $(document).on("click", "#deptTree .fancytree-title", function(){
    	addrSel = "";
    	addrCtnIpt = "";
    	srchSwitch = false;
    	$("#addrCtnIpt").val("");
    	$("#addrSel").find("option:first-child").prop("selected", true);
    	
    	var val = $(this).find("input[type=hidden]").val();
    	// console.log(addrCurrentPage);
    	console.log("addrCurrentDept 영ㅇ기: " + val);
    	addrCurrentDept = val;
    	addrCurrentType = "dept";
    	addrCurrentPage = 1;

        $("#teamGrpEditBtn").hide();
        $("#myGrpEditBtn").hide();
    	
    	
    	empAddrList(val);
    });

    $(document).on("click", "#teamTree .fancytree-title", function(){
    	addrSel = "";
    	addrCtnIpt = "";
    	srchSwitch = false;
    	$("#addrCtnIpt").val("");
    	$("#addrSel").find("option:first-child").prop("selected", true);
    	
    	var val = $(this).find("input[type=hidden]").val();
    	// console.log(addrCurrentPage);
    	addrCurrentDept = val;
    	addrCurrentType = "grp";
    	addrCurrentPage = 1;
    	myDeptNo = '${ sessionScope.loginUser.deptNo }';
    	myEmpNo = 0;
    	addrCurrentGrp = val;
        $("#teamGrpEditBtn").show();
        $("#myGrpEditBtn").hide();
    	grpAddrList(val);
    });

    $(document).on("click", "#myTree .fancytree-title", function(){
    	addrSel = "";
    	addrCtnIpt = "";
    	srchSwitch = false;
    	$("#addrCtnIpt").val("");
    	$("#addrSel").find("option:first-child").prop("selected", true);
    	
    	var val = $(this).find("input[type=hidden]").val();
    	// console.log(addrCurrentPage);
    	addrCurrentDept = val;
    	addrCurrentType = "grp";
    	addrCurrentPage = 1;
    	myDeptNo = 0;
    	myEmpNo = '${ sessionScope.loginUser.empDivNo }';
    	addrCurrentGrp = val;
        $("#teamGrpEditBtn").hide();
        $("#myGrpEditBtn").show();
    	grpAddrList(val);
    });
    
    $(document).on("click", "#pagerArea li", function(){
    	// console.log(endPage);
    	// console.log($(this).hasClass("disabled"));
    	if(!$(this).hasClass("disabled")){

        	
	    	if(addrCurrentType == "dept"){
	    		if($(this).hasClass("first")){
	    			addrCurrentPage = 1;
	    		}else if($(this).hasClass("end")){
	    			addrCurrentPage = endPage;
	    		}else if($(this).hasClass("prev")){
	    			addrCurrentPage--;
	    		}else if($(this).hasClass("next")){
	    			addrCurrentPage++;
	    		}else{
	    			addrCurrentPage = $(this).find("input").val();
	    		}
	    		
	    		empAddrList(addrCurrentDept);
	    	}
	    	

	    	if(addrCurrentType == "grp"){
	    		if($(this).hasClass("first")){
	    			addrCurrentPage = 1;
	    		}else if($(this).hasClass("end")){
	    			addrCurrentPage = endPage;
	    		}else if($(this).hasClass("prev")){
	    			addrCurrentPage++;
	    		}else if($(this).hasClass("next")){
	    			addrCurrentPage++;
	    		}else{
	    			addrCurrentPage = $(this).find("input").val();
	    		}
	    		
	    		grpAddrList(addrCurrentGrp);
	    	}
    	}
    });
    
    $(document).on("click", ".deptType", function(){
    	var val = $(this).parent("td").parent("tr").find("td:first-child").find(".eNoList").val();
    	console.log("val : " + val)
    	deptAddrPop(val);
    });

    $(document).on("click", ".grpType", function(){
    	var val = $(this).parent("td").parent("tr").find("td:first-child").find(".eNoList").val();

    	grpAddrPop(val);
    });
    
    // 검색
    $("#addrSrchBtn").on("click", function(){

    	console.log("ddd : addrCurrentDept : " + $("#addrCtnIpt").val());
    	if($("#addrCtnIpt").val() != ""){
        	addrSel = $("#addrSel").val();
        	addrCtnIpt = $("#addrCtnIpt").val();
        	srchSwitch = true;
        	addrCurrentPage = 1;
        	if(addrCurrentType == "dept"){
        		empAddrList(addrCurrentDept);
        	}else if(addrCurrentType == "grp"){
        		grpAddrList(addrCurrentGrp);
        	}
    	}else{
    		srchSwitch = false;
    		addrSel = "";
        	addrCtnIpt = "";

        	if(addrCurrentType == "dept"){
        		empAddrList(addrCurrentDept);
        	}else if(addrCurrentType == "grp"){
        		grpAddrList(addrCurrentGrp);
        	}
    	}
    });
    
    $("#addrCtnIpt").on("keyup", function(){
		$("#addrSrchBtn").trigger("click");    	
    });
    // 외부인 수정 버튼 클릭
/*     $("#outUpdateBtn").on("click", function(){
    	$("#grpOutNo").val();
    	
    }); */

    /* //$("#outUpdateBtn").on("click", function(){  */
   	$('#addrUpdatePop').on($.modal.BEFORE_OPEN, function(event, modal) {
    	updateOutPop($("#grpOutNo").val());
   	});
   	
   	$('#addrUpdatePop').on($.modal.AFTER_CLOSE, function(event, modal) {
    	updateOutAlreadyAddrNo = 0;
   	});
    
   	// 외부인 삭제 서브밋
 /*   	$("#delOutBtn").on("click", function(){
   		location.href = "deleteOutsiderOne.ad?grpOutNo=" + $("#grpOutNo").val();
  	});
   	 */
   	
    // 사내 주소록 사원 목록 ajax
    function empAddrList(deptNo){
    	$.ajax({
			url:"selectEmpAddrList.ad",
			type:"post",
			data: {
				deptNo:deptNo,
				currentPage:addrCurrentPage,
				addrSel:addrSel,
				addrCtnIpt:addrCtnIpt
			},
			success:function(data) {
				// console.log(data);
				// addrCurrentPage = data.pi.currentPage;
				endPage = data.pi.endPage;
				// console.log("data.pi.endPage : " + data.pi.endPage);
				// console.log("ndPage : " + endPage);
				var trS ="<tr>";
				var trE ="</tr>";
				$("#addrListTbl").find("tr").remove();
				$("#addrListTbl").append('<tr class="tbl_main_tit"><th>이름</th><th>직책/직급</th><th>부서</th><th>이메일</th><th>번호</th></tr>');
				
				if(data.list.length != 0){
					$("#addrListTbl").find("tr:not(.tbl_main_tit)").remove();
					
					var tdS = "<td><a href='#deptAddrPop' rel='modal:open' class='deptType'>";
					var tdE = "</a></td>";
					var hiddenS = "<input type='hidden' class='eNoList' value='"
					var hiddenE = "'>"
					for(var i = 0; i < data.list.length; i++){
						var listOne = data.list[i];
						
						if(listOne.PHONE == null){
							listOne.PHONE = "-";
						}
						
						var nodeStr = trS + tdS + hiddenS + listOne.ENO + hiddenE + listOne.NAME + tdE + tdS + listOne.POS + "/" + listOne.JOB + tdE + tdS + listOne.DEPT + tdE + tdS + listOne.ID + "@" + domain + tdE + tdS + listOne.PHONE + tdE + trE;
						$("#addrListTbl").append(nodeStr);
					}
					
					var pageStr = pageS;
					for(var i = data.pi.startPage; i <= data.pi.endPage; i++){
						pageStr += pageNumS; 
						pageStr += i;
						pageStr += pageNumB;
						pageStr += i;
						pageStr += pageNumE;
					}
					pageStr += pageE;
					
					// console.log(pageStr)
					
					$("#pagerArea").find("ul").remove();
					$("#pagerArea").append(pageStr);
					
					$("#pagerArea li.pager_num").eq(addrCurrentPage - 1).addClass("on");
					
					if(data.pi.currentPage <= 1){
						$("#pagerArea li.prev").addClass("disabled");
					}
					
					if(data.pi.currentPage >= data.pi.maxPage){
						// 다음 죽이기
						$("#pagerArea li.next").addClass("disabled");
					}
					
				}else{
					$("#pagerArea").find("ul").remove();
					$("#addrListTbl").find("tr").remove();
					
					var nodeStr = "";
					if(srchSwitch){
						nodeStr = trS + "<td colspan='5'>검색 결과가 없습니다.</td>" + trE;
					}else{
						nodeStr = trS + "<td colspan='5'>부서에 속한 사원이 없습니다.</td>" + trE;
					}
					
					$("#addrListTbl").append(nodeStr);
				}
				
				
			},
			error:function(status) {
				console.log(status);
			}
		});	
    }
    function empAddrList(deptNo){
    	$.ajax({
			url:"selectEmpAddrList.ad",
			type:"post",
			data: {
				deptNo:deptNo,
				currentPage:addrCurrentPage,
				addrSel:addrSel,
				addrCtnIpt:addrCtnIpt
			},
			success:function(data) {
				// console.log(data);
				// addrCurrentPage = data.pi.currentPage;
				endPage = data.pi.endPage;
				// console.log("data.pi.endPage : " + data.pi.endPage);
				// console.log("ndPage : " + endPage);
				var trS ="<tr>";
				var trE ="</tr>";
				$("#addrListTbl").find("tr").remove();
				$("#addrListTbl").append('<tr class="tbl_main_tit"><th>이름</th><th>직책/직급</th><th>부서</th><th>이메일</th><th>번호</th></tr>');
				
				if(data.list.length != 0){
					$("#addrListTbl").find("tr:not(.tbl_main_tit)").remove();
					
					var tdS = "<td><a href='#deptAddrPop' rel='modal:open' class='deptType'>";
					var tdE = "</a></td>";
					var hiddenS = "<input type='hidden' class='eNoList' value='"
					var hiddenE = "'>"
					for(var i = 0; i < data.list.length; i++){
						var listOne = data.list[i];
						
						if(listOne.PHONE == null){
							listOne.PHONE = "-";
						}
						
						var nodeStr = trS + tdS + hiddenS + listOne.ENO + hiddenE + listOne.NAME + tdE + tdS + listOne.POS + "/" + listOne.JOB + tdE + tdS + listOne.DEPT + tdE + tdS + listOne.ID + "@" + domain + tdE + tdS + listOne.PHONE + tdE + trE;
						$("#addrListTbl").append(nodeStr);
					}
					
					var pageStr = pageS;
					for(var i = data.pi.startPage; i <= data.pi.endPage; i++){
						pageStr += pageNumS; 
						pageStr += i;
						pageStr += pageNumB;
						pageStr += i;
						pageStr += pageNumE;
					}
					pageStr += pageE;
					
					// console.log(pageStr)
					
					$("#pagerArea").find("ul").remove();
					$("#pagerArea").append(pageStr);
					
					$("#pagerArea li.pager_num").eq(addrCurrentPage - 1).addClass("on");
					
					if(data.pi.currentPage <= 1){
						$("#pagerArea li.prev").addClass("disabled");
					}
					
					if(data.pi.currentPage >= data.pi.maxPage){
						// 다음 죽이기
						$("#pagerArea li.next").addClass("disabled");
					}
					
				}else{
					$("#pagerArea").find("ul").remove();
					$("#addrListTbl").find("tr").remove();
					
					var nodeStr = "";
					if(srchSwitch){
						nodeStr = trS + "<td colspan='5'>검색 결과가 없습니다.</td>" + trE;
					}else{
						nodeStr = trS + "<td colspan='5'>부서에 속한 사원이 없습니다.</td>" + trE;
					}
					
					$("#addrListTbl").append(nodeStr);
				}
				
				
			},
			error:function(status) {
				console.log(status);
			}
		});	
    }
    // 공유&내 주소록 사원 목록 ajax
    function grpAddrList(addrNo){
	 console.log("?addrCurrentPage : " + addrCurrentPage);
    	$.ajax({
			url:"selectGrpAddrList.ad",
			type:"post",
			data: {
				myDeptNo:myDeptNo,
				myEmpNo:myEmpNo,
				addrNo:addrNo,
				currentPage:addrCurrentPage,
				addrSel:addrSel,
				addrCtnIpt:addrCtnIpt
			},
			success:function(data) {
				console.log(data);
				// addrCurrentPage = data.pi.currentPage;
				endPage = data.pi.endPage;
				// console.log("data.pi.endPage : " + data.pi.endPage);
				// console.log("ndPage : " + endPage);
				var trS ="<tr>";
				var trE ="</tr>";

				$("#addrListTbl").find("tr").remove();
				$("#addrListTbl").append('<tr class="tbl_main_tit"><th>이름</th><th>회사</th><th>직책/직급</th><th>이메일</th><th>번호</th></tr>');
				
				if(data.list.length != 0){
					$("#addrListTbl").find("tr:not(.tbl_main_tit)").remove();	
					
					var tdS = "<td><a href='#comAddrPop' rel='modal:open' class='grpType'>";
					var tdE = "</a></td>";
					var hiddenS = "<input type='hidden' class='eNoList' value='"
					var hiddenE = "'>"
					for(var i = 0; i < data.list.length; i++){
						var listOne = data.list[i];
						
						if(listOne.PHONE == null){
							listOne.PHONE = "-";
						}

						if(listOne.OCOM == null){
							listOne.OCOM = "-";
						}
						
						if(listOne.OPOS == null){
							listOne.OPOS = "-";
						}

						if(listOne.OJOB == null){
							listOne.OJOB = "-";
						}

						if(listOne.OEMAIL == null){
							listOne.OEMAIL = "-";
						}
						
						if(listOne.OPHONE == null){
							listOne.OPHONE = "-";
						}
						
						var nodeStr = trS + tdS + hiddenS + listOne.ONO + hiddenE + listOne.ONAME + tdE+ tdS + listOne.OCOM + tdE + tdS + listOne.OPOS + "/" + listOne.OJOB + tdE + tdS + listOne.OEMAIL + tdE + tdS + listOne.OPHONE + tdE + trE;
						$("#addrListTbl").append(nodeStr);
					}
					
					var pageStr = pageS;
					for(var i = data.pi.startPage; i <= data.pi.endPage; i++){
						pageStr += pageNumS; 
						pageStr += i;
						pageStr += pageNumB;
						pageStr += i;
						pageStr += pageNumE;
					}
					pageStr += pageE;
					
					$("#pagerArea").find("ul").remove();
					$("#pagerArea").append(pageStr);
					
					$("#pagerArea li.pager_num").eq(addrCurrentPage - 1).addClass("on");
					
					if(data.pi.currentPage <= 1){
						$("#pagerArea li.prev").addClass("disabled");
					}
					
					if(data.pi.currentPage >= data.pi.maxPage){
						// 다음 죽이기
						$("#pagerArea li.next").addClass("disabled");
					}
					
					
				}else{
					$("#pagerArea").find("ul").remove();
					$("#addrListTbl").find("tr").remove();
					
					var nodeStr = "";
					if(srchSwitch){
						nodeStr = trS + "<td colspan='5'>검색 결과가 없습니다.</td>" + trE;
					}else{
						nodeStr = trS + "<td colspan='5'>그룹에 속한 사람이 없습니다.</td>" + trE;
					}
					
					$("#addrListTbl").append(nodeStr);
				}
				
			},
			error:function(status) {
				console.log(status);
			}
		});	
    }

	// moxn : 사내 주소록 사원 상세정보 : ajax
    function deptAddrPop(val){
    	$.ajax({
			url:"selectDeptEmpOne.ad",
			type:"post",
			data: {
				empDivNo:val
			},
			success:function(data) {
				console.log(data);			
				
				$("#deptEmpName").text(data.info.EMP_NAME);
				$("#deptPosName").text(data.info.POS_NAME + "/" + data.info.JOB_NAME);
				$("#deptDeptName").text(data.info.DEPT_NAME);
				$("#deptEmpId").text(data.info.EMP_ID + "@" + domain);
				if(data.info.EXT_PHONE != null){
					$("#deptExtPhone").text(data.info.EXT_PHONE);
					
				}else{
					$("#deptExtPhone").text("-");
				}
				if(data.info.PHONE != null){
					$("#deptPhone").text(data.info.PHONE);
					
				}else{
					$("#deptPhone").text("-");
				}
				if(data.info.FAX != null){
					$("#deptFax").text(data.info.FAX);
					
				}else{
					$("#deptFax").text("-");
				}
				
				if(data.profile == null){
					$("#deptProfile").find(".user_img_has").hide();
					$("#deptProfile").find(".user_img_none").show();
				}else{
					$("#deptProfile").find(".user_img_has").show();
					$("#deptProfile").find(".user_img_none").hide();
					$("#deptProfileImg").prop("src", path+"/resources/uploadFiles/"+data.profile.CHANGE_NAME);
				}
				
				sendMailStr = "writemail.ma?per=" + data.info.EMP_NAME + " <" + data.info.EMP_ID + "@" + domain + ">";
		
			},
			error:function(status) {
				console.log(status);
			}
		});	
    }

	$("#addrDeptEmailSendBtn").on("click",function(){
		location.href = sendMailStr;
	});
	
    function grpAddrPop(val){
    	$.ajax({
			url:"selectGrpOutOne.ad",
			type:"post",
			data: {
				outNo:val
			},
			success:function(data) {
				console.log(data);

				$("#grpOutNo").val(data.info.outNo);
				$("#grpOutName").text(data.info.outName);

				if(data.info.outCom != ""){
					$("#grpOutCom").text(data.info.outCom);
				}else{
					$("#grpOutCom").text("-");
				}

				if(data.info.outPos != "" && data.info.outJob != ""){
					$("#grpOutPos").text(data.info.outPos + "/" + data.info.outJob);
				}else if(data.info.outPos == "" && data.info.outJob != ""){
					$("#grpOutPos").text("- /" + data.info.outJob);
				}else if(data.info.outPos != "" && data.info.outJob == ""){
					$("#grpOutPos").text(data.info.outPos + "/ -");
				}else{
					$("#grpOutPos").text("-");
				}

				if(data.info.outDept != ""){
					$("#grpOutDept").text(data.info.outDept);
				}else{
					$("#grpOutDept").text("-");
				}

				if(data.info.outPhone != ""){
					$("#grpOutPhone").text(data.info.outPhone);
				}else{
					$("#grpOutPhone").text("-");
				}
				
				if(data.info.outEmail != ""){
					$("#grpOutEmail").text(data.info.outEmail);
				}else{
					$("#grpOutEmail").text("-");
				}
				
				if(data.info.outFax != ""){
					$("#grpOutFax").text(data.info.outFax);
				}else{
					$("#grpOutFax").text("-");
				}
				
				if(data.info.outAddr != ""){
					$("#grpOutAddr").text(data.info.outAddr);
				}else{
					$("#grpOutAddr").text("-");
				}
		 		
				var memoryTblStr = "";
				if(data.memory.length != 0){
					for (var i = 0; i < data.memory.length; i++) {
						memoryTblStr += "<tr>";
						memoryTblStr += "<td>";
						memoryTblStr += data.memory[i].MEMORY_NAME;
						memoryTblStr += "</td><td>";
						memoryTblStr += data.memory[i].MDATE;
						memoryTblStr += "</td><td>";
						if(data.memory[i].SUNMOON == "MOON"){
							memoryTblStr += "음력";
						}else if(data.memory[i].SUNMOON == "SUN"){
							memoryTblStr += "양력";
						}
						memoryTblStr += "</td>";
						memoryTblStr += "</tr>";
					}
				}else{
					memoryTblStr = "<tr><td colspan='3'>기념일이 없습니다.</td></tr>" 
						
				}
				$("#grpOutMemory").find("tr:not(:first-child)").remove();
				$("#grpOutMemory").append(memoryTblStr);
			},
			error:function(status) {
				console.log(status);
			}
		});	
    }
    
    function updateOutPop(val){

    	$.ajax({
			url:"selectUpOutInfo.ad",
			type:"post",
			data: {
				outNo:val
			},
			success:function(data) {
				console.log(data);
				 
				console.log(data.addr.type);
				$("#upOutNo").val(data.info.outNo);
				
				// 주소록 구분
				if(data.addr.type == "DEPT"){
					$("#upAddrType").find("option").eq(1).prop("selected", true);
					updateOutAlreadyAddrNo = data.addr.addrNo;
					$("#upAddrType").find("option").eq(1).trigger("change");
				
				}else{
					$("#upAddrType").find("option").eq(2).prop("selected", true);
					$("#upAddrType").find("option").eq(2).trigger("change");
					
				}
				
				// 기념일
				if(data.memory.length == 0){
					up_use_spe_day = false;	
				}else{
					up_use_spe_day = true;
					up_use_spe_day_count = data.memory.length;
					alreadySpeDay();
					
					for(var i = 0; i < data.memory.length; i++){
						console.log(data.memory.MEMORY_NO);
						$(".up_spe_day_ctn .spe_day_list").eq(i).find('.memoryNoIpt').val(data.memory[i].MEMORY_NO);
						$(".up_spe_day_ctn .spe_day_list").eq(i).find('.spe_name').val(data.memory[i].MEMORY_NAME);
						$(".up_spe_day_ctn .spe_day_list").eq(i).find('.datepicker').val(data.memory[i].MDATE);
						if(data.memory[i].SUNMOON == "SUN"){
							$(".up_spe_day_ctn .spe_day_list").eq(i).find('.solar').prop("checked", true);
						}else{
							$(".up_spe_day_ctn .spe_day_list").eq(i).find('.lunar').prop("checked", true);
						}
					}
				}
				
				
				console.log(up_use_spe_day);
				console.log(up_use_spe_day_count);
				
				$("#upOutName").val(data.info.outName);

				if(data.info.outCom != ""){
					$("#upOutCom").val(data.info.outCom);
				}

				if(data.info.outPos != ""){
					$("#upOutPos").val(data.info.outPos);
				}
				
				if(data.info.outJob != ""){
					$("#upOutJob").val(data.info.outJob);
				}

				if(data.info.outDept != ""){
					$("#upOutDept").val(data.info.outDept);
				}

				if(data.info.outPhone != ""){
					$("#upOutPhone").val(data.info.outPhone);
				}
				
				if(data.info.outEmail != ""){
					$("#upOutEmail").val(data.info.outEmail);
				}
				
				if(data.info.outFax != ""){
					$("#upOutFax").val(data.info.outFax);
				}
				
				if(data.info.outAddr != ""){
					var outAddrArr = data.info.outAddr.split("/");
					
					$("#up_postcode_form").val(outAddrArr[0]);
					$("#up_address_form").val(outAddrArr[1]);
					if(outAddrArr.length == 3){
						$("#up_detailAddress_form").val(outsyAddrArr[2]);
						
					}
				}
				
			},
			error:function(status) {
				console.log(status);
			}
		});	
    }
    
</script>

</body>
</html>

</c:if>
<c:if test="${ empty loginUser }">
	<jsp:forward page="../../../index.jsp"/>
</c:if>
