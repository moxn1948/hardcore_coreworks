<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<link href="${ contextPath }/resources/css/style_hr.css" rel="stylesheet">
<style>
.modal{max-width: 865px;}
</style>
<h3 class="main_tit">결재 경로 설정</h3>
<div class="addr_info_pop_wrap addr_de_pop_wrap">
    <div class="addr_de_pop_srch">
        <select name="" id="new_eas_addr_pop_srch_sel">
            <option value="emp">사원명</option>
            <option value="dept">부서명</option>
        </select>
        <input type="text" name="" id="new_eas_addr_pop_srch_cnt">
        <button class="btn_solid_main" id="new_eas_addr_pop_srch_btn">검색</button>
    </div>
    <div class="addr_de_pop_ctn clearfix">
        <div class="addr_dept_pop_ctn">
            <div class="addr_dept_pop easAddrPop">
                <div id="newEasAddrPop" class="tree_menu">
					<ul>
                       <c:forEach var="list" items="${ deptList }">
                      			<li class="folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
                    			    <ul>
	                    			    <c:forEach var="empList" items="${ empList }">
		                      					<c:if test="${ list.deptNo == empList.DEPT_NO }">
		                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
		                     						</li>
		                      					</c:if>
	                     				</c:forEach>
	                      				<c:if test="${ list.deptOrderList.size() != 0 }">
	                    						<c:forEach var="list2" items="${ list.deptOrderList }">
	                      							<li class="folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
				                       					<ul>
						                    			    <c:forEach var="empList" items="${ empList }">
							                      					<c:if test="${ list2.deptNo == empList.DEPT_NO }">
							                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
							                     						</li>
							                      					</c:if>
						                     				</c:forEach>
						                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
						                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
						                       							<li class="folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
										                       				<ul>
											                    			    <c:forEach var="empList" items="${ empList }">
												                      					<c:if test="${ list3.deptNo == empList.DEPT_NO }">
												                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
												                     						</li>
												                      					</c:if>
											                     				</c:forEach>
										                     				</ul>
						                       							</li>
						                       						</c:forEach>
						                       				</c:if>
				                       					</ul>
	                      							</li>
	                      						</c:forEach>
	                      				</c:if>
                   					</ul>
                      			</li>
                       </c:forEach>
                 	</ul>
                </div>
            </div>
        </div>
        <div class="addr_info_pop_ctn eas_line_select_div">
            <div class="addr_info_pop">
                <div class="tit_wrap clearfix">
                </div>
                <div class="desc_wrap clearfix top0">
                        <table class="eas_line_select_div_tbl" id="personList">
                        	<colgroup>
                        		<col style="width:8%;">
                        		<col style="width:15%;">
                        		<col style="width:18%;">
                        		<col style="width:15%;">
                        		<col style="width:10%;">
                        		<col style="width:20%;">
                        		<col style="width:14%;">
                        	</colgroup>
                        	<tr class="thead">
                        		<th>순번</th>
                        		<th>부서</th>
                        		<th>직급/직책</th>
                        		<th>처리방법</th>
                        		<th>처리자</th>
                        		<th>이메일</th>
                        		<th>순서변경</th>
                        	</tr>
                        	<c:if test="${ !empty param.btn }">
                        	<c:forEach var="list" items="${ upPathList }">
	                        	<tr>
	                        		<td><c:out value="${ list.PATH_RANK }"/></td>
	                        		<td><c:out value="${ list.DNAME }"/></td>
	                        		<td><c:out value="${ list.JNAME }"/>/<c:out value="${ list.PNAME }"/></td>
	                        		<td>
	                        			<c:if test="${ list.PROC_HOW.equals('AGM') }">
										<select style='width:60px;' class='selectboxForProcess'>
											<option value='AS'>결재</option>
											<option value='AGM' selected>합의</option>
											<option value='CONFIRM'>확인</option>
											<option value='REFER'>참조</option>
										</select>
										</c:if>
										<c:if test="${ list.PROC_HOW.equals('REFER') }">
										<select style='width:60px;' class='selectboxForProcess'>
											<option value='AS'>결재</option>
											<option value='AGM'>합의</option>
											<option value='CONFIRM'>확인</option>
											<option value='REFER' selected>참조</option>
										</select>
										</c:if>
										<c:if test="${ list.PROC_HOW.equals('AS') }">
										<select style='width:60px;' class='selectboxForProcess'>
											<option value='AS' selected>결재</option>
											<option value='AGM'>합의</option>
											<option value='CONFIRM'>확인</option>
											<option value='REFER'>참조</option>
										</select>
										</c:if>
										<c:if test="${ list.PROC_HOW.equals('DRAFT') }">
										<select style='width:60px;' class='selectboxForProcess'>
											<option value='AS'>결재</option>
											<option value='AGM'>합의</option>
											<option value='CONFIRM'>확인</option>
											<option value='REFER'>참조</option>
										</select>
										</c:if>
										<c:if test="${ list.PROC_HOW.equals('CONFIRM') }">
										<select style='width:60px;' class='selectboxForProcess'>
											<option value='AS'>결재</option>
											<option value='AGM'>합의</option>
											<option value='CONFIRM' selected>확인</option>
											<option value='REFER'>참조</option>
										</select>
										</c:if>
	                        		</td>
	                        		<td><c:out value="${ list.ENAME }"/></td>
	                        		<td><c:out value="${ list.EMP_ID }"/>@<c:out value="${ sessionScope.domain }"/></td>
	                        		<td>
	                        			<button class='new_eas_addr_pop_btn' id='upBtn'>▲</button>
			                			<button class='new_eas_addr_pop_btn' id='downBtn'>▼</button>
	                        		</td>
	                        	</tr>
                        	</c:forEach>
                        	</c:if>                        	
                        </table>
                </div>
            </div>
        </div>  
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main"><a onclick="selectEasLine();">확인</a></button>
</div>
<script>
	var array = new Array();
	var idx;
	
    $(function(){
        $("#newEasAddrPop").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                }
            },
            click: function(event, data){
                var node = data.node;
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");
      	
        
        $(document).on("click", "#personList tr > td:nth-child(2)", function(){
            idx = $('#personList').children().find('tr').length;
            $(this).parent().next().find('.tdIdx').text(idx-2);
        	
            $(this).parent().remove();

        });
        
        
        $(this).on("click",".addr_dept_pop.easAddrPop .fancytree-title", function(){
            if(!$(this).parent().hasClass("fancytree-folder")){

                var node = $(this)[0].innerHTML;
                
                var userNo = $(this).find('.emp').val();
                selectOneEmp(userNo);                  
         	}
        });
        
      	//결재선 순서변경 스크립트
		$(document).on('click', '.new_eas_addr_pop_btn', function() {

			if ($(this).attr('id') == 'upBtn') {
				if($(this).parent().parent().prev().hasClass('thead')) {
					alert('상위 결재선이 없습니다.');
				}else {
				var $tr = $(this).parent().parent(); // 클릭한 버튼이 속한 tr 요소
				$tr.prev().before($tr); // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
				$tr.children().eq(0).text($tr.closest('tr').prevAll().length);
				
				var $nextTr = $(this).parent().parent().next();
				$nextTr.children().eq(0).text($tr.closest('tr').prevAll().length+1);
				}

			} else {

				var $tr = $(this).parent().parent(); // 클릭한 버튼이 속한 tr 요소
				$tr.next().after($tr); // 현재 tr 의 다음 tr 뒤에 선택한 tr 넣기
				$tr.children().eq(0).text($tr.closest('tr').prevAll().length);
				
				var $prevTr = $(this).parent().parent().prev();
				$prevTr.children().eq(0).text($tr.closest('tr').prevAll().length-1);
			}

		});
      	
      	$(document).on('mouseover', '#personList tbody tr td', function() {
      		$(this).parents("tr").css({
                backgroundColor : "#efefef"
            });
      	}).on('mouseleave', '#personList tbody tr td', function(){
      		$(this).parents("tr").css({
                backgroundColor : "transparent"
            });
      	});
		
	});
    
	// 검색
	$("#new_eas_addr_pop_srch_cnt").on("keyup", function(){
		$("#new_eas_addr_pop_srch_btn").trigger("click");
	});
	
	$("#new_eas_addr_pop_srch_btn").on("click", function(){
		var sel = $("#new_eas_addr_pop_srch_sel").val();
		var srchVal = $("#new_eas_addr_pop_srch_cnt").val();
		
        var tree = $.ui.fancytree.getTree("#newEasAddrPop");
        tree.visit(function(node){
        	var htmlCode = $.parseHTML(node.title);
        	var srchCode = node.title.split("<input")[0];
        	
        	if(srchCode.search(" ") != -1){
        		srchCode = srchCode.split(" ")[0];
        	}

         	if(htmlCode[1].className == sel && srchCode.search(srchVal) != -1){
         		
        		node.setActive(true);
        		
        		if(sel == "emp"){
             		var user = $.parseHTML(node.title)[1];
             		userNum = user.value;
            		node.parent.setExpanded(true);
        			
        		}else{
            		node.setExpanded(true);
        			
        		}
        		
        	}
         	
        });
	});
    
    //사원 클릭 시 정보 가져오기
    function selectOneEmp(eNo) {
    	
    	$.ajax({
    		url:"selectEmpOne.ad",
    		type:"post",
    		data:{
    			eNo:eNo
    		},
    		success:function(data){
   				var selectbox = "<select style='width:60px;' class='selectboxForProcess'><option value='AS'>결재</option><option value='AGM'>합의</option><option value='CONFIRM'>확인</option><option value='REFER'>참조</option></select>";
    			
    			if(!$('#personList').has('td')) {
    				
    				$("#personList").append("<tr><td class='tdIdx'>1</td><td>" 
							+ data.info.DEPT_NAME + "</td><td>"
							+ (data.info.POS_NAME + "/" + data.info.JOB_NAME) + "</td><td>"
							+ selectbox + "</td><td>"
							+ data.info.EMP_NAME + "<input type='hidden' name='easLineEmpNo' value='" + data.info.ENO + "'></td><td>"
							+ data.info.EMP_ID + "@" + "${domain}" + "</td><td>"
							+ "<button class='new_eas_addr_pop_btn' id='upBtn'>▲</button>"
			                + "<button class='new_eas_addr_pop_btn' id='downBtn'>▼</button>" + "</td>");
    			}else{
    				idx = $('#personList').children().find('tr').length;  
    				$("#personList").append("<tr><td class='tdIdx'>" + idx + "</td><td>" 
	    									+ data.info.DEPT_NAME + "</td><td>"
	    									+ (data.info.POS_NAME + "/" + data.info.JOB_NAME) + "</td><td>"
	    									+ selectbox + "</td><td>"
	    									+ data.info.EMP_NAME + "<input type='hidden' name='easLineEmpNo' value='" + data.info.ENO + "'></td><td>"
	    									+ data.info.EMP_ID + "@" + "${domain}" + "</td><td>"
	    									+ "<button class='new_eas_addr_pop_btn' id='upBtn'>▲</button>"
	    					                + "<button class='new_eas_addr_pop_btn' id='downBtn'>▼</button>" + "</td>");
    			}
    			
    		},
    		error:function(status){
    			console.log(status);
    		}
    		
    	});
    }
    
 	//모달 닫기 전 값 초기화
 	$('#addrPop1').on($.modal.BEFORE_CLOSE, function(event, modal) {
	    $("#addr_pop_srch_sel").find("option").eq(0).prop("selected", true);
		$("#addr_pop_srch_cnt").val("");
		
	}); 
    
    
</script>