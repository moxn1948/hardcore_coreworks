<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<style>
    /* 모달 크기 변경 */
    .chatmodal{max-width: 800px;}
</style>
<h3 class="mail_main_tit">받는사람 선택</h3> 
<div class="mail_pop mail_new_sch">
    <div class="mail_ctn_list clearfix mail_type_ctn mail_type_ctn_2">
        <div class="cnt">
			<div class="addr_info_pop_wrap addr_de_pop_wrap">
			    <div class="addr_de_pop_srch">
		        	<select name="" id="addr_pop_srch_sel">
		            	<option value="emp" selected>사원명</option>
		            	<option value="dept">부서명</option>
		       	 	</select>
		        <input type="text" name="" id="mail_pop_srch_cnt" autocomplete="off">
		        <button class="btn_solid_main" id="mail_pop_srch_btn">검색</button>
		    </div>
			    <div class="addr_de_pop_ctn clearfix">
			        <div class="addr_dept_pop_ctn">
			            <div class="addr_dept_pop">
			                <div id="mailDefaltPop" class="tree_menu">
               		<ul>
                       <c:forEach var="list" items="${ deptList }">
                      			<li class="folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
                    			    <ul>
	                    			    <c:forEach var="empList" items="${ empList }">
		                      					<c:if test="${ list.deptNo == empList.DEPT_NO }">
		                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
		                     						</li>
		                      					</c:if>
	                     				</c:forEach>
	                      				<c:if test="${ list.deptOrderList.size() != 0 }">
	                    						<c:forEach var="list2" items="${ list.deptOrderList }">
	                      							<li class="folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
				                       					<ul>
						                    			    <c:forEach var="empList" items="${ empList }">
							                      					<c:if test="${ list2.deptNo == empList.DEPT_NO }">
							                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
							                     						</li>
							                      					</c:if>
						                     				</c:forEach>
						                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
						                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
						                       							<li class="folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
										                       				<ul>
											                    			    <c:forEach var="empList" items="${ empList }">
												                      					<c:if test="${ list3.deptNo == empList.DEPT_NO }">
												                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
												                     						</li>
												                      					</c:if>
											                     				</c:forEach>
										                     				</ul>
						                       							</li>
						                       						</c:forEach>
						                       				</c:if>
				                       					</ul>
	                      							</li>
	                      						</c:forEach>
	                      				</c:if>
                   					</ul>
                      			</li>
                       </c:forEach>
                 	</ul>
                </div>
			            </div>
			        </div>
			        <div class="addr_info_pop_ctn">
			            <div class="tbl_common tbl_basic">
			                <p class="tbl_tit"></p>
			                <div class="tbl_wrap">
			                    <table class="tbl_ctn" id="mailPersonList">
			                        <colgroup>
			                            <col style="width: 15%;">
			                            <col style="width: 30%;">
			                            <col style="width: 15%;">
			                            <col style="width: 40%;">
			                        </colgroup>
			                        <tr>
			                            <th>사원명</th>
			                            <th>직책/직급</th>
			                            <th>부서</th>
			                            <th>이메일</th>
			                        </tr>
			                    </table>
			                </div>
			            </div>
			        </div>  
			    </div>
			</div>
        </div>
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_white"><a href="#" rel="modal:close">취소</a></button>
    <button class="btn_main" id="close_btn"><a href="#" rel="modal:close">등록</a></button>
</div>

<script>
	var pathPop = "${ contextPath }";
	var domainPop = "${ domain }";
	
	
	
    $(function(){
        $(document).on("click", "#mailPersonList tr td", function(){
            $(this).parent("tr").remove();
        });
        
        $(document).on("mouseover","#mailPersonList tr td",function(){
            $(this).parent("tr").css({
                backgroundColor : "#efefef"
            });
        }).on("mouseleave","#mailPersonList tr td", function(){
            $(this).parent("tr").css({
                backgroundColor : "transparent"
            });
        });

        $("#mailDefaltPop").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                } 
            },
            click: function(event, data){
                var node = data.node;
                
                var htmlCode = $.parseHTML(node.title)[1];
                
                if(htmlCode.className == "emp"){
                    console.log(htmlCode.value);
                    userNum = htmlCode.value;
                    selectMailEmpOne(userNum);	
            	}
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");
      
    });
    
	
    // tree에서 사원 클릭 시
	var userNum = 0;
    
 	// 검색
	$("#mail_pop_srch_cnt").on("keyup", function(){
		$("#mail_pop_srch_btn").trigger("click");
	});
	
	$("#mail_pop_srch_btn").on("click", function(){
		var sel = $("#addr_pop_srch_sel").val();
		var srchVal = $("#mail_pop_srch_cnt").val();
		
        var tree = $.ui.fancytree.getTree("#mailDefaltPop");
        tree.visit(function(node){
        	var htmlCode = $.parseHTML(node.title);
        	var srchCode = node.title.split("<input")[0];
        	
        	if(srchCode.search(" ") != -1){
        		srchCode = srchCode.split(" ")[0];
        	}

        	console.log("srchCode" + srchCode);
         	if(htmlCode[1].className == sel && srchCode.search(srchVal) != -1){
         		
        		node.setActive(true);
        		
        		if(sel == "emp"){
             		var user = $.parseHTML(node.title)[1];
             		userNum = user.value;
            		node.parent.setExpanded(true);
            		// selectEmpOne(userNum);
        			
        		}else{
            		node.setExpanded(true);
        			
        		}
        		
        	}
         	
        });
	});
	
	// 사원 클릭 시 정보 노출 함수
	function selectMailEmpOne(eNo){
		$.ajax({
		url:"selectEmpOne.ad",
		type:"post",
		data: {
			eNo:eNo
		},
		success:function(data) {
			var listLen = $("#mailPersonList").find("tr:not(:first-child)").length;
			var listSwitch = true;
			
			for(var i = 0; i < listLen; i++) {
				if(data.info.ENO == $("#mailPersonList").find("tr:not(:first-child)").eq(i).find(".eNo")[0].value) {
					alert("이미 명단에 존재하는 사원입니다.");
					listSwitch = false;
					break;
				}
			}
			
			if(listSwitch) {
				 $("#mailPersonList").append("<tr><td class='empName'><input type='hidden' name='eNo' class='eNo' value='" +
							data.info.ENO
							+"'>" +
							data.info.EMP_NAME
                         + "</td><td>" +
                         data.info.POS_NAME + "/" + data.info.JOB_NAME
                         + "</td><td>" +
                         data.info.DEPT_NAME
                         + "</td><td class='empEmail'>" +
                         data.info.EMP_ID + "@" + domainPop
                         + "</td></tr>");
			}
			
		},
		error:function(status) {
			console.log(status);
		}
		});
	}
	
	var namearr = [];
	var emailarr = [];
	$("#close_btn").click(function() {
		$(".empName").each(function(index) {
			namearr[index] = $(this).text();
		})
		$(".empEmail").each(function(index) {
			emailarr[index] = $(this).text();
		})
		
		console.log(type)
		
		var receiver = "";
		if(type.hasClass("rec")) {
			if($("#mail_receiver").val() != "") {
				receiver = $("#mail_receiver").val() + ", ";
			}
		} else if (type.hasClass("ref")) {
			if($("#mail_ref").val() != "") {
				receiver = $("#mail_ref").val() + ", ";
			}
		} else if (type.hasClass("hid")) {
			if($("#mail_hidden_ref").val() != "") {
				receiver = $("#mail_hidden_ref").val() + ", ";
			}
		}
		
		for(var i = 0; i < namearr.length; i++) {
			if(i == 0) {
				receiver += namearr[i] + " <" + emailarr[i] + "> ";
			} else {
				receiver += ", " + namearr[i] + " <" + emailarr[i] + "> ";
			}
		}
		
		if(type.hasClass("rec")) {
			$("#mail_receiver").val(receiver);
		} else if (type.hasClass("ref")) {
			$("#mail_ref").val(receiver);
		} else if (type.hasClass("hid")) {
			$("#mail_hidden_ref").val(receiver);
		}
		
		
	});
	
	// 모달 닫기 전 값 초기화
	$('#addrPop1').on($.modal.BEFORE_CLOSE, function(event, modal) {
	    $("#addr_pop_srch_sel").find("option").eq(0).prop("selected", true);
		$("#mail_pop_srch_cnt").val("");
		$("#mailPersonList").find('tr').eq(0).nextAll().remove();
	});

    
    // 모달 닫은 후 접기
	$('#addrPop1').on($.modal.AFTER_CLOSE, function(event, modal) {

        var tree = $.ui.fancytree.getTree("#mailDefaltPop");
        tree.visit(function(node){
    		node.setExpanded(false);
        });
        
	});

</script>