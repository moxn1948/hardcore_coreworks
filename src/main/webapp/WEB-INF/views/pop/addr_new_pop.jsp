<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style>
    /* 모달 안에 데이트 피커 넣을 때 */
    .ui-datepicker{z-index: 6000 !important;}
</style>

<form action="newOutsider.ad" method="post" id="addrNewPop">
	<input type="hidden" value="${ sessionScope.loginUser.empDivNo }" name="empDivNo">
	<input type="hidden" value="${ sessionScope.loginUser.deptNo }" name="deptNo">
	<h3 class="main_tit">연락처 추가</h3>
	<div class="addr_pop_cnt">
       <div class="main_ctn">
           <div class="menu_tit"></div>
           <!-- ** 코드 작성부 시작 -->
           <div class="main_cnt">
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">주소록</div>
	                   <div class="main_cnt_desc">
	                       <select id="addrType" name="type">
	                           <option value="" hidden>선택</option>
	                           <option value="DEPT">공용 주소록</option>
	                           <option value="EMP">내 주소록</option>
	                       </select>
	                   </div>
 	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">그룹</div>
	                   <div class="main_cnt_desc">
	                       <select id="addrNo" name="addrNo" disabled>
	                       </select>
	                   </div>
	               </div> 
               </div>
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">이름</div>
	                   <div class="main_cnt_desc"><input type="text" name="outName" id="outName"></div>
	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">직책</div>
	                   <div class="main_cnt_desc"><input type="text" name="outPos" id="outPos"></div>
	               </div>
               </div>
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">회사명</div>
	                   <div class="main_cnt_desc"><input type="text" name="outCom" id="outCom"></div>
	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">직급</div>
	                   <div class="main_cnt_desc"><input type="text" name="outJob" id="outJob"></div>
	               </div>
               </div>
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">부서</div>
	                   <div class="main_cnt_desc"><input type="text" name="outDept" id="outDept"></div>
	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">번호</div>
	                   <div class="main_cnt_desc"><input type="text" name="outPhone" id="outPhone"></div>
	               </div>
               </div>
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">이메일</div>
	                   <div class="main_cnt_desc"><input type="email" name="outEmail" id="outEmail"></div>
	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">팩스번호</div>
	                   <div class="main_cnt_desc"><input type="text" name="outFax" id="outFax"></div>
	               </div>
               </div>
               <div class="main_cnt_list clearfix">
                   <div class="main_cnt_tit">회사주소</div>
                   <div class="main_cnt_desc">
                       <!-- 주소 검색 폼 시작 -->
                   	<div class="address_form_wrap">
                   		<div class=""><input type="text" name="" id="postcode_form" placeholder="우편번호" readonly><a href="#" class="button btn_pink" id="address_srch_btn" onclick="DaumPostcode();">검색</a></div>
                   		<div class=""><input type="text" name="" id="address_form" placeholder="주소" readonly></div>
                   		<div class=""><input type="text" name="" id="detailAddress_form" placeholder="상세주소"></div>
                   		<input type="hidden" name="outAddr" id="outAddr">
                   	</div>
                       <!-- 주소 검색 폼 끝 -->
                   </div>
               </div>
               <div class="main_cnt_list clearfix">
                   <input type="checkbox" id="spe_day"><label for="spe_day">기념일 사용</label>
                   <div class="spe_day_wrap">
                       <ul class="spe_day_ctn"></ul>
                       <button class="btn_main spe_day_add_btn" type="button">추가</button>
                   </div>
               </div>
               <input type="hidden" name="calNum" id="calNum">
           </div>
           <!-- ** 코드 작성부 끝 -->
       </div>
   <!-- 메인 컨텐츠 영역 끝! -->
	</div>
	
	<div class="pop_close_wrap">
	    <button class="btn_pink" type="button"><a href="#" rel="modal:close">취소</a></button>
	    <button class="btn_solid_main" type="submit" onclick="return newOutsider();">추가</button>
	</div>
</form>



<script>
	var dNo = '${ sessionScope.loginUser.deptNo }';
	var eNo = '${ sessionScope.loginUser.empDivNo }';
    var nodeArr = new Array(); // 기념일 속성 idx 판별 위한 배열, 추후 자바 쪽에서 이용
    var node_count = 1;
    var real_node = 1; // 실제 기념일 개수
    $(function(){

        // 기념일
        (function(){

            var base_node = "<li class='spe_day_list'><div class='spe_day_tit'><input type='text' name='memoryName' id='' class='spe_name' placeholder='기념일명을 입력하세요.'><input type='text' name='memoryDate' id='' class='datepicker' placeholder='날짜를 선택하세요.' autocomplete='off'><div class='spe_day_date_btn'><button class='btn_white' type='button'>삭제</button></div></div><div class='spe_day_date_type'><input type='radio' name='' class='solar' value='SUN' checked><label for='' class='solar_txt'>양력</label><input type='radio' name='' id='' class='lunar' value='MOON'><label for='' class='lunar_txt'>음력</label></div></li>";
            
            $("#spe_day").on("change", function(){
                if($(this).prop("checked") == true){
                    $(".spe_day_wrap").show();
                    $(".spe_day_ctn").append(base_node);
                    $(".spe_day_ctn").find(".spe_day_list").eq(0).find(".datepicker").eq(0).prop("id", "datepicker1").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        nextText: '다음 달',
                        prevText: '이전 달',
                        dateFormat: "yy/mm/dd",
                        showMonthAfterYear: true , 
                        dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                    });
                        
                    $(".spe_day_ctn").find(".spe_day_list").eq(0).find(".solar").eq(0).prop({
                                                                                                "name" : "cal_type1",
                                                                                                "id" : "solar1"
                                                                                            }).siblings(".solar_txt").prop("for", "solar1");
                    $(".spe_day_ctn").find(".spe_day_list").eq(0).find(".lunar").eq(0).prop({
                                                                                                "name" : "cal_type1",
                                                                                                "id" : "lunar1"
                                                                                            }).siblings(".lunar_txt").prop("for", "lunar1");
                    nodeArr[0] = 1;
                }else{
                    var spe_use_yn = confirm("기념일 사용을 해제하시면 기존의 기념일이 삭제됩니다.");
                    if(spe_use_yn){

                        $(".spe_day_wrap").hide();
                        $(".spe_day_ctn").find(".spe_day_list").remove();
                        node_count = 1;
                        real_node = 1;
                        nodeArr = new Array();
                    } else{
                        $(this).prop("checked", true);
                        return false;
                    }
                }
            });


            $(document).on("click", ".spe_day_add_btn", function(){
                node_count++;
                real_node++;
                nodeArr[node_count - 1] = 1;
                $(".spe_day_ctn").append(base_node);
                // $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".spe_name").eq(0).prop("name", "spe_name" + node_count);
                $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".solar").eq(0).prop({
                                                                                                        "name" : "cal_type" + node_count,
                                                                                                        "id" : "solar" + node_count
                                                                                                    }).siblings(".solar_txt").prop("for", "solar"+node_count);
                $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".lunar").eq(0).prop({
                                                                                                        "name" : "cal_type" + node_count,
                                                                                                        "id" : "lunar" + node_count
                                                                                                    }).siblings(".lunar_txt").prop("for", "lunar"+node_count);
                
                $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".datepicker").eq(0).prop("id", "datepicker" + node_count);
                $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".datepicker").eq(0).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    nextText: '다음 달',
                    prevText: '이전 달',
                    dateFormat: "yy/mm/dd",
                    showMonthAfterYear: true , 
                    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                });
            });

            $(document).on("click", ".spe_day_date_btn button", function(){
                var list_count = $(".spe_day_ctn").find(".spe_day_list").length;

                if(list_count > 1){
                    real_node--;
                    $(this).parents(".spe_day_list").remove();
                    var node_idx = $(this).parents(".spe_day_list").find(".datepicker").prop("id").split("datepicker")[1];
                    nodeArr[node_idx - 1] = 0;
                }else{
                    alert("기념일은 최소 1개 이상 등록해야합니다.");
                }
            });


        })();
        
    });


	// 그룹 종류 ajax
	$("#addrType").on("change", function(){
		var type = $(this).val();
		
		$("#addrgroup").prop("disabled", true);
		addr_Grp(type);
	});
	
	
	function addr_Grp(type){

		$.ajax({
			url:"outsiderGrpList.ad",
			type:"post",
			data: {
				type:type,
				dNo:dNo,
				eNo:eNo
				
			},
			success:function(data) {
				console.log(data.grpList);
				
				$("#addrNo").find("option").remove();
				
				for (var i = 0; i < data.grpList.length; i++) {
					
					$("#addrNo").append("<option value=" + data.grpList[i].addrNo + ">" + data.grpList[i].addrgroup + "</option>")
				}
				$("#addrNo").prop("disabled", false);
				
			},
			error:function(status) {
				console.log(status);
			}
		});	
	}
	
    // 주소 검색 시작
    function DaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('postcode_form').value = data.zonecode;
                document.getElementById("address_form").value = addr;
                document.getElementById("detailAddress_form").focus();
            }   
        }).open();
    }

    $("#postcode_form, #address_form").on("click", function(){
        $("#address_srch_btn").trigger("click");
    });
    // 주소 검색 끝
    
    // submit 전 처리
    function newOutsider(){
    	
    	if($("#addrType").val() == ""){
    		alert("주소록을 선택해주세요.");
    		
    		return false;
    	}
    	if($("#outName").val() == ""){
    		alert("이름을 입력해주세요.");
    		
    		$("#outName").focus();
    		
    		return false;
    	}

    	if($("#outPhone").val() == "" && $("#outEmail").val() == ""){
    		alert("번호 또는 이메일 중 하나는 필수 기입사항입니다.");
    		
    		if($("#outPhone").val() == ""){
	    		$("#outPhone").focus();
    		}else{
    			$("#outEmail").focus();
    		}
    		
    		return false;
    	}
    	
    	if($("#postcode_form").val() != "" && $("#address_form").val() != ""){
    		var addrStr = "";
    		
    		addrStr = $("#postcode_form").val() + "/" + $("#address_form").val();
    		
    		if($("#detailAddress_form").val() != ""){
    			addrStr += "/" + $("#detailAddress_form").val();
    		}
    		
    		$("#outAddr").val(addrStr);
    		
    	}
    	
    	if($("#spe_day").prop("checked")){
    		$("#calNum").val(nodeArr.toString());
    	}
    	
    	if(confirm("연락처를 추가하시겠습니까?")){
    		
    		return true;
    	}else{
    		
    		return false;
    	}
    }

	
	// 모달 닫기 전 값 초기화
	$('#addrNewPop').on($.modal.BEFORE_CLOSE, function(event, modal) {
	    $("#addrType").find("option").eq(0).prop("selected", true);
	    $("#addrNo").find("option").remove();
		$("#outName").val("");
		$("#outPos").val("");
		$("#outCom").val("");
		$("#outJob").val("");
		$("#outDept").val("");
		$("#outPhone").val("");
		$("#outEmail").val("");
		$("#outFax").val("");
		$("#postcode_form").val("");
		$("#address_form").val("");
		$("#detailAddress_form").val("");
		$("#spe_day").prop("checked", false);
        $(".spe_day_wrap").hide();
        $(".spe_day_ctn").find(".spe_day_list").remove();
        node_count = 1;
        real_node = 1;
        nodeArr = new Array();
		
	});
</script>