<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- tree api 관련 끝 -->
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">

<div class="addr_info_pop_wrap addr_de_pop_wrap">
    <div class="addr_de_pop_srch">
        <select name="" id="calc_pop_srch_sel">
            <option value="emp">사원명</option>
            <option value="dept">부서명</option>
        </select>
        <input type="text" name="" id="calc_pop_srch_cnt" autocomplete="off">
        <button class="btn_solid_main" id="calc_pop_srch_btn" type="button">검색</button>
    </div>
    <div class="addr_de_pop_ctn clearfix">
        <div class="addr_dept_pop_ctn">
            <div class="addr_dept_pop">
                <div id="calcNewPerson" class="tree_menu">
                    <ul>
                       <c:forEach var="list" items="${ deptList }">
                      			<li class="folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
                    			    <ul>
	                    			    <c:forEach var="empList" items="${ empList }">
		                      					<c:if test="${ list.deptNo == empList.DEPT_NO }">
		                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
		                     						</li>
		                      					</c:if>
	                     				</c:forEach>
	                      				<c:if test="${ list.deptOrderList.size() != 0 }">
	                    						<c:forEach var="list2" items="${ list.deptOrderList }">
	                      							<li class="folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
				                       					<ul>
						                    			    <c:forEach var="empList" items="${ empList }">
							                      					<c:if test="${ list2.deptNo == empList.DEPT_NO }">
							                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
							                     						</li>
							                      					</c:if>
						                     				</c:forEach>
						                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
						                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
						                       							<li class="folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
										                       				<ul>
											                    			    <c:forEach var="empList" items="${ empList }">
												                      					<c:if test="${ list3.deptNo == empList.DEPT_NO }">
												                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
												                     						</li>
												                      					</c:if>
											                     				</c:forEach>
										                     				</ul>
						                       							</li>
						                       						</c:forEach>
						                       				</c:if>
				                       					</ul>
	                      							</li>
	                      						</c:forEach>
	                      				</c:if>
                   					</ul>
                      			</li>
                       </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <div class="addr_info_pop_ctn">
            <div class="tbl_common tbl_basic">
                <p class="tbl_tit">명단</p>
                <div class="tbl_wrap">
                    <table class="tbl_ctn" id="personList">
                        <colgroup>
                            <col style="width: 15%;">
                            <col style="width: 30%;">
                            <col style="width: 15%;">
                            <col style="width: 40%;">
                        </colgroup>
                        <tr>
                            <th>사원명</th>
                            <th>직책/직급</th>
                            <th>부서</th>
                            <th>이메일</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>  
    </div>
</div>
<script>
    
    $(function(){
        $("#calcNewPerson").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                } 
            },
            click: function(event, data){
                var node = data.node;

            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");


    });

	
	// 검색
	$("#calc_pop_srch_cnt").on("keyup", function(){
		$("#calc_pop_srch_btn").trigger("click");
	});
	
	$("#calc_pop_srch_btn").on("click", function(){
		var sel = $("#calc_pop_srch_sel").val();
		var srchVal = $("#calc_pop_srch_cnt").val();
		
        var tree = $.ui.fancytree.getTree("#calcNewPerson");
        tree.visit(function(node){
        	var htmlCode = $.parseHTML(node.title);
        	var srchCode = node.title.split("<input")[0];
        	
        	if(srchCode.search(" ") != -1){
        		srchCode = srchCode.split(" ")[0];
        	}

        	console.log("srchCode" + srchCode);
         	if(htmlCode[1].className == sel && srchCode.search(srchVal) != -1){
         		
        		node.setActive(true);
        		
        		if(sel == "emp"){
             		var user = $.parseHTML(node.title)[1];
             		userNum = user.value;
            		node.parent.setExpanded(true);
            		// selectEmpOne(userNum);
        			
        		}else{
            		node.setExpanded(true);
        			
        		}
        		
        	}
         	
        });
	});

	// 모달 닫기 전 값 초기화
	$('#calNewSchPop').on($.modal.BEFORE_CLOSE, function(event, modal) {
		// 일정 기본 정보 초기화
		$("#newCalKind").find("option").eq(0).prop("selected", true);
		$("#newCalTitle").val("")
		$("#newCalCnt").val("")
		$("#upCalNo").val(0);
		
		// 기간 선택 박스 초기화
		$("#newAllYn").prop("selected", false);
    	$dates = $('#newCalSdate, #newCalEdate').datepicker();
        $(".parti_day").css({display : "block"});
        $(".all_day").css({display : "none"});
        $('#newCalSdate, #newCalEdate').val("");
        $dates.datepicker('destroy');
        datepickerRange();
        $('#newAllDay').val("");
        $dates.datepicker('destroy');
        datepickerAll();
        $("#newCalStime").find("option").eq(0).prop("selected", true);
        $("#newCalEtime").find("option").eq(0).prop("selected", true);
        $("#newCalEtime").find("option:not(:first-child)").remove();
		
	
        // 사람 선택 부분 초기화
	    $("#calc_pop_srch_sel").find("option").eq(0).prop("selected", true);
		$("#calc_pop_srch_cnt").val("");
		
        var tree = $.ui.fancytree.getTree("#calcNewPerson");
        tree.visit(function(node){
    		node.setExpanded(false);
        });
        
        $("#calc_type").find("option").eq(0).prop("selected", true);
        $(".calc_pop .calc_type_ctn").hide();

		$("#newCalcUpBtn").hide();
		$("#newCalcInsertBtn").show();
		$("#newAllYn").prop("checked", false);
		for (var i = 0; i < $("#newCalKind").find("option").length; i++) {
			$("#newCalKind").find("option").eq(i).prop("hidden", false);
		}
		
        $("#personList").find("tr:not(:first-child)").remove();
	});
	
</script>