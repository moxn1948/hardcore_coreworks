<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<link href="${ contextPath }/resources/css/style_hr.css" rel="stylesheet">
<style>
.modal{max-width: 700px;}
</style>
<h3 class="main_tit">결재양식 선택</h3>
<div class="addr_info_pop_wrap addr_de_pop_wrap">
    <div class="addr_de_pop_srch">
        <input type="text" placeholder="제목으로 검색하세요">
        <button class="btn_solid_pink">검색</button>
    </div>
    <div class="addr_de_pop_ctn clearfix">
        <div class="addr_dept_pop_ctn eas_addr_dept_pop_ctn">
            <div class="addr_dept_pop easPop">
                <div id="newEasPop" class="tree_menu">
                </div>
            </div>
        </div>
        <div class="addr_info_pop_ctn eas_addr_info_pop_ctn">
            <div class="addr_info_pop eas_addr_info_pop">
                <div class="tit_wrap clearfix">
                    <div class="name_area">
                        <p class="name_cnt eas_name_cnt">상세 정보</p>
                    </div>                   
                </div>
                <div class="desc_wrap clearfix">
                    
                    <div class="info_area">
                        <ul class="info_ctn">
                            <li class="info_list clearfix">
                                <p class="tit eas_pop_tit">제목</p>
                                <p class="cnt"></p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit eas_pop_tit">전사 문서함</p>
                                <p class="cnt"></p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit eas_pop_tit">보존 연한</p>
                                <p class="cnt"><span></span></p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit eas_pop_tit">기안 부서</p>
                                <p class="cnt">
                                <input type="hidden" value="${sessionScope.loginUser.deptNo}" id="deptNo">
                                	<select>
										<option></option>                              											
                                	</select>
                                </p>
                            </li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main" id="startEas"><a href="#" rel="modal:close">확인</a></button>
</div>
<script>
    
    $(function(){ 
    	
    	$(this).on("click",".addr_dept_pop.easPop .fancytree-title", function(){
            if(!$(this).parent().hasClass("fancytree-folder")){
                var selectFormatNo = $(this).find('input[type="hidden"]').val();
                var selectFormatPathName = $(this).parent().parent().parent().siblings().text();
                var drafterDeptNo = $('#deptNo').val();
 
				selectFormat(selectFormatNo, selectFormatPathName, drafterDeptNo);
            }
        });
        
    	//모달 내에서 확인 클릭 시
        $("#startEas").click(function(){
        	var formatNo = $("#formatNo").val();
        	console.log(formatNo);
        	location.href = "viewNewEasPage.ea?formatNo="+formatNo;
        	
        });
    	
        
    });
    
    function testAjax(data, depth) {		

		//먼저 depth 데이터를 뿌린 다음에./.,,
		$("#newEasPop").append("<ul>"+depth+"</ul>");
		    		
    	// tree api 실행
		$("#newEasPop").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                } 
            },
            click: function(event, data){
                var node = data.node;
                console.log(node);
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");

    }
    
    function selectFormat(selectFormatNo, selectFormatPathName, drafterDeptNo) {
    	
    	$.ajax({
    		url: "selectOneFormat.ea",
    		type: "post",
    		async: false,
    		data: {
    			fNo:selectFormatNo,
    			deptNo:drafterDeptNo
    		},
    		success: function(data){
    			console.log(data);
    			
    			 $(".info_area ul > li:first-child > p:nth-child(2)").text(data.format.formatName);
    			 $(".info_area ul > li:first-child > p:nth-child(2)").append("<input type='hidden' id='formatNo' value='" + data.format.formatNo + "'>");
    			 $(".info_area ul > li:nth-child(2) > p:nth-child(2)").text(selectFormatPathName);
    			 
    			 if(data.format.period == null && data.format.periodYn == 'N') {
    				 $(".info_area ul > li:nth-child(3) > p:nth-child(2)").text('제한 없음');
    			 }else {
    			 	$(".info_area ul > li:nth-child(3) > p:nth-child(2)").find('span').text(data.format.period);
    			 }
   			 	
    			 $(".info_area ul > li:last-child > p:nth-child(2)").find('option').text(data.deptName);

    		},
    		error: function(status){
    			console.log(status);
    		}
    	});

    }

</script>