<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<h3 class="main_tit">주소록 상세보기</h3>
<div class="addr_info_pop_wrap">
    <div class="addr_info_pop addr_com_info_pop">
        <div class="tit_wrap clearfix">
            <div class="name_area">
                <p class="name_cnt"></p>
            </div>
            <div class="link_area">
            <form action="deleteOutsiderOne.ad" method="post" class="delOutBtnFrom">
            	<input type="hidden" id="grpOutNo" name="grpOutNo">
                <button type="submit" class="btn_pink" id="delOutBtn">삭제</button>
            </form>
                <button class="btn_main" id="outUpdateBtn"><a href="#addrUpdatePop" rel='modal:open'>수정</a></button>
            </div>
        </div>
        <div class="desc_wrap clearfix">
            <div class="info_area">
                <ul class="info_ctn">
                    <li class="info_list clearfix">
                        <p class="tit">이름</p>
                        <p class="cnt" id="grpOutName"></p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">회사명</p>
                        <p class="cnt" id="grpOutCom"></p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">직책/직급</p>
                        <p class="cnt" id="grpOutPos"></p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">부서</p>
                        <p class="cnt" id="grpOutDept"></p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">번호</p>
                        <p class="cnt" id="grpOutPhone"></p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">이메일</p>
                        <p class="cnt" id="grpOutEmail"></p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">팩스</p>
                        <p class="cnt" id="grpOutFax"></p>
                    </li>
                    <li class="info_list clearfix">
                        <p class="tit">회사주소</p>
                        <p class="cnt" id="grpOutAddr"></p>
                    </li>
                    <li class="info_list clearfix">
                        <div class="spe_day_tbl_wrap">
                            <table class="spe_day_tbl" id="grpOutMemory">
                                <colgroup>
                                    <col style="width: 40%;">
                                    <col style="width: 30%;">
                                    <col style="width: 30%;">
                                </colgroup>
                                <tr>
                                    <th>기념일명</th>
                                    <th>날짜</th>
                                    <th>양/음력</th>
                                </tr>
                            </table>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main"><a href="#" rel="modal:close">확인</a></button>
</div>