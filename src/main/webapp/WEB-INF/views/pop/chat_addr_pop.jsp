<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">

<style>
    /* 모달 크기 변경 */
    .chatmodal{max-width: 800px;}
</style>
<h3 class="chat_main_tit">받는사람 선택</h3>
<c:forEach var="item" items="${ empList }">
	<c:if test="${ item.EMP_DIV_NO eq loginUser.empDivNo }">
		<input type="hidden" id="jname" value="${ item.JNAME }"/>
	</c:if> 
</c:forEach>
<div class="chat_pop chat_new_sch">
    <div class="chat_ctn_list clearfix chat_type_ctn chat_type_ctn_2">
        <div class="cnt">
			<div class="addr_info_pop_wrap addr_de_pop_wrap">
			    <div class="addr_de_pop_srch">
		        	<select name="" id="chat_pop_srch_sel">
		            	<option value="emp">사원명</option>
		            	<option value="dept">부서명</option>
		       	 	</select>
		        <input type="text" name="" id="chat_pop_srch_cnt" autocomplete="off">
		        <button class="btn_solid_main" id="chat_pop_srch_btn">검색</button>
		        <div class="roomName">
		        	<h3>채팅방 이름 입력 : </h3>
		        	<input type="text" id="setRoomName" />
		        </div>
		    </div>
			    <div class="addr_de_pop_ctn clearfix">
			        <div class="addr_dept_pop_ctn">
			            <div class="addr_dept_pop">
			                <div id="chatDefaltPop" class="tree_menu">
               		<ul>
                       <c:forEach var="list" items="${ deptList }">
                      			<li class="folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
                    			    <ul>
	                    			    <c:forEach var="empList" items="${ empList }">
		                      					<c:if test="${ list.deptNo == empList.DEPT_NO }">
		                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
		                     						</li>
		                      					</c:if>
	                     				</c:forEach>
	                      				<c:if test="${ list.deptOrderList.size() != 0 }">
	                    						<c:forEach var="list2" items="${ list.deptOrderList }">
	                      							<li class="folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
				                       					<ul>
						                    			    <c:forEach var="empList" items="${ empList }">
							                      					<c:if test="${ list2.deptNo == empList.DEPT_NO }">
							                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
							                     						</li>
							                      					</c:if>
						                     				</c:forEach>
						                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
						                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
						                       							<li class="folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
										                       				<ul>
											                    			    <c:forEach var="empList" items="${ empList }">
												                      					<c:if test="${ list3.deptNo == empList.DEPT_NO }">
												                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
												                     						</li>
												                      					</c:if>
											                     				</c:forEach>
										                     				</ul>
						                       							</li>
						                       						</c:forEach>
						                       				</c:if>
				                       					</ul>
	                      							</li>
	                      						</c:forEach>
	                      				</c:if>
                   					</ul>
                      			</li>
                       </c:forEach>
                 	</ul>
                </div>
			            </div>
			        </div>
			        <div class="addr_info_pop_ctn">
			            <div class="tbl_common tbl_basic">
			                <p class="tbl_tit"></p>
			                <div class="tbl_wrap">
			                    <table class="tbl_ctn" id="chatPersonList">
			                        <colgroup>
			                            <col style="width: 15%;">
			                            <col style="width: 30%;">
			                            <col style="width: 15%;">
			                            <col style="width: 40%;">
			                        </colgroup>
			                        <tr>
			                            <th>사원명</th>
			                            <th>직책/직급</th>
			                            <th>부서</th>
			                            <th>이메일</th>
			                        </tr>
			                    </table>
			                </div>
			            </div>
			        </div>  
			    </div>
			</div>
        </div>
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_white"><a href="#" rel="modal:close">취소</a></button>
    <button class="btn_main" id="close_btn2"><a href="#" rel="modal:close">등록</a></button>
</div>

<script>
	var pathPop = "${ contextPath }";
	var domainPop = "${ domain }";
	
	
	
    $(function(){
        $(document).on("click", "#chatPersonList tr td", function(){
            $(this).parent("tr").remove();
        });
        
        $(document).on("mouseover","#chatPersonList tr td",function(){
            $(this).parent("tr").css({
                backgroundColor : "#efefef"
            });
        }).on("mouseleave","#chatPersonList tr td", function(){
            $(this).parent("tr").css({
                backgroundColor : "transparent"
            });
        });

        $("#chatDefaltPop").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                } 
            },
            click: function(event, data){
                var node = data.node;
                
                var htmlCode = $.parseHTML(node.title)[1];
                
                if(htmlCode.className == "emp"){
                    console.log(htmlCode.value);
                    userNum = htmlCode.value;
                    selectChatEmpOne(userNum);	
            	}
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");
        
    });
    
	
    // tree에서 사원 클릭 시
	var userNum = 0;
    
	// 검색
	$("#chat_pop_srch_cnt").on("keyup", function(){
		$("#chat_pop_srch_btn").trigger("click");
	});
	
	$("#chat_pop_srch_btn").on("click", function(){
		var sel = $("#chat_pop_srch_sel").val();
		var srchVal = $("#chat_pop_srch_cnt").val();
		
        var tree = $.ui.fancytree.getTree("#chatDefaltPop");
        tree.visit(function(node){
        	var htmlCode = $.parseHTML(node.title);
        	var srchCode = node.title.split("<input")[0];
        	
        	if(srchCode.search(" ") != -1){
        		srchCode = srchCode.split(" ")[0];
        	}

        	console.log("srchCode" + srchCode);
         	if(htmlCode[1].className == sel && srchCode.search(srchVal) != -1){
         		
        		node.setActive(true);
        		
        		if(sel == "emp"){
             		var user = $.parseHTML(node.title)[1];
             		userNum = user.value;
            		node.parent.setExpanded(true);
            		// selectEmpOne(userNum);
        			
        		}else{
            		node.setExpanded(true);
        			
        		}
        		
        	}
         	
        });
	});
	
	// 사원 클릭 시 정보 노출 함수
	function selectChatEmpOne(eNo){
		$.ajax({
		url:"selectEmpOne.ad",
		type:"post",
		data: {
			eNo:eNo
		},
		success:function(data) {
			var listLen2 = $("#chatPersonList").find("tr:not(:first-child)").length;
			var listSwitch2 = true;
			
			for(var i = 0; i < listLen2; i++) {
				if(data.info.ENO == $("#chatPersonList").find("tr:not(:first-child)").eq(i).find(".eNo2")[0].value) {
					alert("이미 명단에 존재하는 사원입니다.");
					listSwitch2 = false;
					break;
				}
			}
			
			if(listSwitch2) {
				 $("#chatPersonList").append("<tr><td><input type='hidden' name='eNo2' class='eNo2' value='" +
							data.info.ENO
							+"'>" +
							data.info.EMP_NAME
                         + "</td><td>" +
                         data.info.POS_NAME + "/" + data.info.JOB_NAME
                         + "</td><td>" +
                         data.info.DEPT_NAME
                         + "</td><td>" +
                         data.info.EMP_ID + "@" + domainPop
                         + "</td></tr>");
			}
		},
		error:function(status) {
			console.log(status);
		}
		});
	}
	
	// 모달 닫기 전 값 초기화
	$('#chatPop').on($.modal.BEFORE_CLOSE, function(event, modal) {
	    $("#chat_pop_srch_sel").find("option").eq(0).prop("selected", true);
		$("#chat_pop_srch_cnt").val("");
		$("#chatPersonList").find('tr').eq(0).nextAll().remove();
	});

    
    // 모달 닫은 후 접기
	$('#chatPop').on($.modal.AFTER_CLOSE, function(event, modal) {

        var tree = $.ui.fancytree.getTree("#chatDefaltPop");
        tree.visit(function(node){
    		node.setExpanded(false);
        });
        
	});
    
    $("#close_btn2").click(function() {
    	var empList = "${loginUser.empDivNo}";
    	var jname = $("#jname").val();
    	var empNameList = "${loginUser.empName}" + " " + jname;
    	var RoomName = $("#setRoomName").val();
    	
    	if($(".eNo2").length != 0) {
    	
	    	$(".eNo2").each(function(index) {
	   			empList += ", " + $(this).val();
				empNameList += ", " + $(this).parent().text() + " " + $(this).parents('tr').children('td').eq(1).text().split("/")[1];
	    	});
    	
    	
	    	if(RoomName == "") {
	    		RoomName = empNameList;
	    	}	
	    	
	    	$.ajax({
	    		url:"createChatRoom",
	    		data: {
	    			empList:empList,
	    			roomName : RoomName
	    		},
	    		success:function(data) {
	    			$("#chatRoomNo").val(data.roomNo);
	    			$("#chatForm").css("display","block");
	    			
	    			var room = RoomName.split(", ");
	    			var room2 = "";
	    			for(var i = 0; i < room.length; i++) {
	    				if(room[i] == ('${loginUser.empName}'+ ' ' + jname)) {
	    					for(var j = 0; j < room.length; j++) {
	    						if(i != j) {
	    							if(room2 == "") {
	    								room2 += room[j];
	    							} else {
	    								room2 += ", " + room[j];
	    							}
	    						}
	    					}
	    				}
	    			}
	    			
	    			var dt = new Date();
	    			var time = "";
	    			var ap = "";
	    			if(dt.getHours() / 12 > 1) {
	    				ap = 'PM';
	    			} else {
	    				ap = 'AM';
	    			}
	    			
	    			if(dt.getHours() > 12) {
	    				time = '0' + (dt.getHours() - 12);
	    			} else {
	    				time = dt.getHours();
	    			}
	    			
	    			if(dt.getMinutes() < 10) {
	    				time += " : 0" + dt.getMinutes() + " " + ap;
	    			} else {
		    			time += " : " + dt.getMinutes() + " " + ap;
	    			}

	    			$(".chatlist").prepend('<div class="chatlistline clearfix"><input type="hidden" id="roomNo" value=' + 
							data.roomNo + 
							'><div class="chatusericon"><i class="fas fa-user-circle"></i></div><div class="chattext"><p><b>'+ 
							room2 + 
							'</b></p><p>' + 
							'</p></div><div class="chattime"><p>' + 
							time +
							'</p><div class="chatcount">' +
							'</div></div></div>');
	    			
	    			$(".multichatusername").html(room2);
	    		}
	    	});
    	} else {
    		alert('사원을 선택하세요!');
    	}
    });

</script>