<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<!-- tree api 관련 끝 -->
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">

<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
    /* 모달 크기 변경 */
    .modal{min-width: 800px;max-width: 800px;}
    /* 모달 안에 데이트 피커 넣을 때 */
    .ui-datepicker{z-index: 6000 !important;}
</style>
<h3 class="main_tit" id="calcNewTit">새 일정 등록</h3>
<form action="" method="post" id="userCalcInfo">
	<input type="hidden" name="empDivNo" value="${ sessionScope.loginUser.empDivNo }">	
	<input type="hidden" name="newDeptNo" value="${ sessionScope.loginUser.deptNo }">	
	<input type="hidden" name="calNo" id="upCalNo" value="0">
	
	<div class="calc_pop calc_new_sch">
	    <div class="calc_ctn_list clearfix">
	        <div class="tit">종류</div>
	        <div class="cnt">
	            <select name="calKind" id="newCalKind">
	                <option value="SOLO">개인일정</option>
	                <option value="DEPT">부서일정</option>
	                <option value="GROUP">단체일정</option>
	                <c:if test="${  sessionScope.auth.getClm().equals('Y') }">
	                	<option value="COMP">회사일정</option>
	                </c:if>
	            </select>
	        </div>
	    </div>
	    <div class="calc_ctn_list clearfix calc_day">
	        <div class="tit">기간</div>
	        <div class="cnt parti_day">
	            <input type="text" id="newCalSdate" name="newCalSdate" class="date" placeholder="시작일" autoComplete="off" readonly>
	            <select name="calStime" id="newCalStime" class="date_time date_time_1">
	            </select>
	            <input type="text" id="newCalEdate" name="newCalEdate" class="date" placeholder="종료일" autoComplete="off" readonly>
	            <select name="calEtime" id="newCalEtime" class="date_time date_time_2">
	                <option value="">시간선택</option>
	            </select>
	        </div>
	        <div class="cnt all_day">
	            <input type="text" id="newAllDay" name="newAllDay" class="date" placeholder="날짜" autoComplete="off" readonly>
	        </div>
	        <div class="cnt day_chk">
	            <input type="checkbox" name="allYn" id="newAllYn"><label for="newAllYn">종일</label>
	        </div>
	    </div>
	    <div class="calc_ctn_list clearfix">
	        <div class="tit">제목</div>
	        <div class="cnt"><input type="text" name="calTitle" id="newCalTitle" placeholder="제목을 작성해주세요." class="calc_tit"></div>
	    </div>
	    <div class="calc_ctn_list clearfix">
	        <div class="tit">설명</div>
	        <div class="cnt"><textarea name="calCnt" id="newCalCnt" placeholder="설명을 입력해주세요." rows="6"></textarea></div>
	    </div>
	    <!-- <div class="calc_ctn_list clearfix calc_type_ctn calc_type_ctn_1">
	        <div class="tit">개인</div>
	        <div class="cnt"><input type="text" name="" id=""></div>
	    </div> -->
	    <div class="calc_ctn_list clearfix calc_type_ctn calc_type_ctn_2">
	        <div class="tit attendant_name">공람자</div>
	        <div class="cnt">
	            <jsp:include page="../pop/calc_new_person.jsp"/>
	        </div>
	    </div>
	</div>
	<div class="pop_close_wrap">
	    <button class="btn_white" id="newCalcCanBtn" type="button">취소</button>
	    <button class="btn_main" type="submit" onclick="return newCalcSubmit()" id="newCalcInsertBtn">등록</button>
	    <button class="btn_main" type="submit" onclick="return updateCalcSubmit()" id="newCalcUpBtn">수정</button>
	</div>
</form>
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	var domain = '${ sessionScope.domain }';

    $(function(){
        $(".calc_type_ctn_1").css({display : "block"});  

        $("#newCalKind").on("change", function(){
            var type = $(this).val();
            if(type == "SOLO"){
                $(".calc_type_ctn").css({display : "none"});
                $(".calc_type_ctn_1").css({display : "block"});    
            }else if(type == "DEPT"){
                $(".calc_type_ctn").css({display : "none"});
                $(".calc_type_ctn_2").css({display : "block"});
                $(".attendant_name").text("참석자");
            }else if(type == "GROUP"){
                $(".calc_type_ctn").css({display : "none"});
                $(".calc_type_ctn_2").css({display : "block"});
                $(".attendant_name").text("공유자");
            }else if(type == "COMP"){
                $(".calc_type_ctn").css({display : "none"});
                $(".calc_type_ctn_2").css({display : "block"});
                $(".attendant_name").text("참석자");
            }
        });

        // 일정 시간 설정
        setTimeSel("#newCalStime", 23, 0);
        
        $("#newCalStime").on("change", function(){
/* 
        	if($("#newCalStime").val() == "allday"){
        		// setTimeSel("#newCalStime", 23, 0);
        		//$("#newCalStime").find("option").eq(1).prop("selected", true);
        		// $("#newCalEtime").find("option").remove();
        		$("#newCalEtime").append("<option value='' hidden>시간선택</option>");
        		$("#newCalEtime").append("<option value='allday' selected>종일</option>");
        	}else if($("#newCalEtime").val() == "allday"){
        		// $("#newCalStime").find("option").remove();
        		$("#newCalStime").append("<option value='' hidden>시간선택</option>");
        		$("#newCalStime").append("<option value='allday' selected>종일</option>");
        	}else{ */
        		
        	
				if(!(!$("#newCalEtime").find("option").eq(0).prop("selected") && $("#newCalSdate").val() != $("#newCalEdate").val())){
					$("#newCalEtime").find("option").remove();
		            
		        	var idx = 0;
		        	

		        	if($("#newCalStime").val() != "allday"){

			        	for (var i = 0; i < $("#newCalStime").find("option").length; i++) {
							if($("#newCalStime").find("option").eq(i).prop("selected")){
								idx = i + 1;
							}
							
							if(i == $("#newCalStime").find("option").length - 1){
					            setTimeSel("#newCalEtime", 23, idx);
							}
						}
		        	}else{
		        		 setTimeSel("#newCalEtime", 23, idx);
		        	}
		        		
				}

        	/* } */
			
        });

        $("#newCalSdate").on("change", function(){
        	$("#newCalStime").trigger("change");
        });
        
        $("#newCalEdate").on("change", function(){
        	if($("#newCalSdate").val() != $("#newCalEdate").val()){
        		$("#newCalEtime").find("option").remove();
				setTimeSel("#newCalEtime", 23, 0);
			}else{
	        	$("#newCalStime").trigger("change");
			}
        });
        
        function setTimeSel(sel, num, idx){
        	$(sel).append("<option value='' selected>시간선택</option>");
        	$(sel).append("<option value='allday'>종일</option>");
            for(var i = 0; i <= num; i++){
                
                var hour;
                var day;
                if(i > 12){
                   day = i - 12;
                }else{
                    day = i;
                }

                if(day < 10){
                    if(i < 12){

                        hour = "0" + day + ":00 AM";
                    }else{

                        hour = "0" + day + ":00 PM";
                    }
                }else{
                    if(i < 12){
                        hour = day + ":00 AM";
                    }else{
                        hour = day + ":00 PM";
                    }
                }

                $(sel).append("<option value='" + hour + "'>" + hour + "</option>");
                
                if(day < 10){
                    if(i < 12){
                        hour = "0" + day + ":30 AM";
                    }else{
                        hour = "0" + day + ":30 PM";
                    }
                }else{
                    if(i < 12){
                        hour = day + ":30 AM";
                    }else{
                        hour = day + ":30 PM";
                    }
                }

                $(sel).append("<option value='" + hour + "'>" + hour + "</option>");
            }
            
            if(idx != 0){
				
				if($("#newCalSdate").val() != "" && $("#newCalSdate").val() == $("#newCalEdate").val()){

	            	for (var i = 0; i < idx; i++) {
	            		$("#newCalEtime").find("option").eq(i).prop("hidden", true);
		    			/* $("#newCalEtime").find("option").eq(i).remove(); */
	            			
	    			}
				}
            }
        }

        $("#newAllYn").on("change",function(){
        	var $dates = "";
            if($(this).prop("checked") == true){
            	$dates = $('#newCalSdate, #newCalEdate').datepicker();
                $(".parti_day").css({display : "none"});
                $(".all_day").css({display : "block"});
                $('#newCalSdate, #newCalEdate').val("");
                $dates.datepicker('destroy');
                datepickerRange();
                $("#newCalStime").find("option").eq(0).prop("selected", true);
                $("#newCalEtime").find("option").eq(0).prop("selected", true);
            }else{
            	$dates = $('#newAllDay').datepicker();
                $(".parti_day").css({display : "block"});
                $(".all_day").css({display : "none"});
                $('#newAllDay').val("");
                $dates.datepicker('destroy');
                datepickerAll();
            }
        });

        $(document).on("click", "#personList tr td", function(){
            $(this).parent("tr").remove();
        });
        
        $(document).on("mouseover","#personList tr td",function(){
            $(this).parent("tr").css({
                backgroundColor : "#efefef"
            });
        }).on("mouseleave","#personList tr td", function(){
            $(this).parent("tr").css({
                backgroundColor : "transparent"
            });
        });

        $(this).on("click",".calc_pop .fancytree-title", function(){
            if(!$(this).parent().hasClass("fancytree-folder")){
                var node = $(this)[0].innerHTML;
            	calcAddrOnePop($(this).find("input[type=hidden]").val());

            }
        });
	
        $("#newCalcCanBtn").on("click", function(){
        	if(confirm("취소하시겠습니까?")){
	        	$.modal.close();
        	}
        });
        
    });

    // submit 전 처리
    function newCalcSubmit(){
  		console.log($("#newCalStime").find("option").eq(0).prop("selected"));
  		
		if(!$("#newAllYn").prop("checked")){

	    	if($("#newCalSdate").val() == ""){
	    		alert("시작일을 선택해주세요.");
	    		
	    		$("#newCalSdate").focus();
	    		
	    		return false;
	    	}

	    	if($("#newCalStime").find("option").eq(0).prop("selected")){
	    		alert("시작시간을 선택해주세요.");
	    		
	    		return false;
	    	}
	    	
	    	if($("#newCalEdate").val() == ""){
	    		alert("종료일을 선택해주세요.");
	    		
	    		$("#newCalEdate").focus();
	    		
	    		return false;
	    	}
	    		
	    	if($("#newCalEtime").find("option").eq(0).prop("selected")){
	    		alert("종료시간을 선택해주세요.");
	    		
	    		return false;
	    	}

			if($("#newCalStime").val() == "allday" && $("#newCalEtime").val() != "allday"){
	    		alert("시간을 종일로 선택하신 경우 시작일과 종료일이 동일해야합니다.");
	    		
	    		$("#newCalEtime").focus();
	    		
	    		return false;
			}

			if($("#newCalStime").val() != "allday" && $("#newCalEtime").val() == "allday"){
	    		alert("시간을 종일로 선택하신 경우 시작일과 종료일이 동일해야합니다.");
	    		
	    		$("#newCalEtime").focus();
	    		
	    		return false;
			}
	 		
		}else{
			if($("#newAllDay").val() == ""){
	    		alert("날짜를 선택해주세요.");
	    		
	    		$("#newAllDay").focus();
	    		
	    		return false;
			}
		}

		if($("#newCalTitle").val() == ""){
    		alert("제목을 입력해주세요.");
    		
    		$("#newCalTitle").focus();
    		
    		return false;
		}

    	if(confirm("일정을 추가하시겠습니까?")){
    		$("#userCalcInfo").prop("action", "insertNewCalc.ca");
    		
    		return true;
    	}else{
    		
    		return false;
    	}
    }
    
    function datepickerAll(){
        $("#newAllDay").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
    }

    function datepickerRange(){

        from = $( "#newCalSdate" )
                .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat : "yy/mm/dd",
                nextText: '다음 달',
                prevText: '이전 달',
                dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                })
                .on( "change", function() {
                    to.datepicker( "option", "minDate", getDate( this ) );
                }),
                to = $( "#newCalEdate" ).datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat : "yy/mm/dd",
                    nextText: '다음 달',
                    prevText: '이전 달',
                    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                })
                .on( "change", function() {
                    from.datepicker( "option", "maxDate", getDate( this ) );
                });
    }

    function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( "yy/mm/dd", element.value );
        } catch( error ) {
            date = null;
        }
    
        return date;
    }

	// moxn : 사내 주소록 사원 상세정보 : ajax
    function calcAddrOnePop(val){
    	$.ajax({
			url:"selectDeptEmpOne.ad",
			type:"post",
			data: {
				empDivNo:val
			},
			success:function(data) {
				console.log(data);			
				var listLen = $("#personList").find("tr:not(:first-child)").length;
				var listSwitch = true;
				for (var i = 0; i < listLen; i++) {
					if(data.info.ENO == $("#personList").find("tr:not(:first-child)").eq(i).find(".eNo")[0].value){
						alert("이미 명단에 존재하는 사원입니다.");
						listSwitch = false;
						break;
					}
				}
				
				if(listSwitch){
	                $("#personList").append("<tr><td><input type='hidden' name='eNo' class='eNo' value='" +
	                							data.info.ENO
	                							+"'>" +
	                							data.info.EMP_NAME
	                                            + "</td><td>" +
	                                            data.info.POS_NAME + "/" + data.info.JOB_NAME
	                                            + "</td><td>" +
	                                            data.info.DEPT_NAME
	                                            + "</td><td>" +
	                                            data.info.EMP_ID + "@" + domain
	                                            + "<input type='hidden' name='assoCalNo' value='0'></td></tr>");
				}
			},
			error:function(status) {
				console.log(status);
			}
		});	
    }
	
</script>
