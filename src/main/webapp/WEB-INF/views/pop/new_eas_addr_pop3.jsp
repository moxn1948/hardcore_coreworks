<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<style>
.modal{max-width: 800px;}
</style>

<h3 class="main_tit">공람자 설정</h3>
<div class="addr_info_pop_wrap addr_de_pop_wrap">
    <div class="addr_de_pop_srch">
        <select name="" id="addr_pop_srch_sel3">
            <option value="emp">사원명</option>
            <option value="dept">부서명</option>
        </select>
        <input type="text" name="" id="addr_pop_srch_cnt3" autocomplete="off">
        <button class="btn_solid_main" id="addr_pop_srch_btn">검색</button>
    </div>
    <div class="addr_de_pop_ctn clearfix">
        <div class="addr_dept_pop_ctn">
            <div class="addr_dept_pop">
                <div id="addrDefaltPop3" class="tree_menu">
               		<ul>
                       <c:forEach var="list" items="${ deptList }">
                      			<li class="folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
                    			    <ul>
	                    			    <c:forEach var="empList" items="${ empList }">
		                      					<c:if test="${ list.deptNo == empList.DEPT_NO }">
		                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
		                     						</li>
		                      					</c:if>
	                     				</c:forEach>
	                      				<c:if test="${ list.deptOrderList.size() != 0 }">
	                    						<c:forEach var="list2" items="${ list.deptOrderList }">
	                      							<li class="folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
				                       					<ul>
						                    			    <c:forEach var="empList" items="${ empList }">
							                      					<c:if test="${ list2.deptNo == empList.DEPT_NO }">
							                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
							                     						</li>
							                      					</c:if>
						                     				</c:forEach>
						                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
						                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
						                       							<li class="folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
										                       				<ul>
											                    			    <c:forEach var="empList" items="${ empList }">
												                      					<c:if test="${ list3.deptNo == empList.DEPT_NO }">
												                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
												                     						</li>
												                      					</c:if>
											                     				</c:forEach>
										                     				</ul>
						                       							</li>
						                       						</c:forEach>
						                       				</c:if>
				                       					</ul>
	                      							</li>
	                      						</c:forEach>
	                      				</c:if>
                   					</ul>
                      			</li>
                       </c:forEach>
                 	</ul>
                </div>
            </div>
        </div>
        <div class="addr_info_pop_ctn">
            <div class="addr_info_pop">
                <div class="tit_wrap clearfix">
                </div>
                <div class="desc_wrap clearfix" id="personDetail">
                	<table class="eas_line_select_div_tbl" id="personList_share">
                   			<colgroup>
                        		<col style="width:25%;">
                        		<col style="width:25%;">
                        		<col style="width:25%;">
                        		<col style="width:25%;">
                        	</colgroup>
                        	<tr class="thead" id="publisherTr">
                        		<th>부서</th>
                        		<th>직급/직책</th>
                        		<th>공람자</th>
                        		<th style="width:200px">이메일</th>
                        	</tr> 
                        	<c:if test="${ !empty param.btn }">
                        	<c:forEach var="list" items="${ upPubList }">                     	
                        	<tr>
                        		<td><c:out value="${ list.DNAME }"/></td>
                        		<td><c:out value="${ list.JNAME }"/>/<c:out value="${ list.PNAME }"/></td>
                        		<td><c:out value="${ list.ENAME }"/></td>
                        		<td><c:out value="${ list.EID }"/>@<c:out value="${ sessionScope.domain }"/></td>
                        	</tr>
                        	</c:forEach>
                        	</c:if>                       	
                        </table>
                </div>
            </div>
        </div>  
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main"><a onclick="selectShare();">확인</a></button>
</div>
<script>
	var pathPop = "${ contextPath }";
	var domainPop = "${ domain }";
	
	
	
    $(function(){
        $("#addrDefaltPop3").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                }
            },
            click: function(event, data){
                var node = data.node;
                
                var htmlCode = $.parseHTML(node.title)[1];
                
                if(htmlCode.className == "emp"){
                    console.log(htmlCode.value);
                    userNum = htmlCode.value;
                    selectEmpOne3(userNum);	
            	}
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");
        
        $(document).on("click", "#personList_share tr > td:first-child", function(){
            $(this).parent("tr").remove();
        });
        
        $('#gongramEditBtn').click(function(){
        	$('#personDetail div:nth-child(2) > ul li p:not(.tit)').text("");
        })
        
        $(document).on('mouseover', '#personList_share tbody tr td', function() {
      		$(this).parents("tr").css({
                backgroundColor : "#efefef"
            });
      	}).on('mouseleave', '#personList_share tbody tr td', function(){
      		$(this).parents("tr").css({
                backgroundColor : "transparent"
            });
      	});
  
    });
    
    // tree에서 사원 클릭 시
	var userNum = 0;
    
    // 모달 닫은 후 접기
	$('#AddrPop3').on($.modal.AFTER_CLOSE, function(event, modal) {

        var tree = $.ui.fancytree.getTree("#addrDefaltPop3");
        tree.visit(function(node){
    		node.setExpanded(false);
        });
        
	});
	
	// 모달 열 때 active
	$(document).on("click", ".open_modal", function(){
		userNum = $(this).find(".userNum")[0].value;
		console.log("userNum : " + userNum)
		
		// 특정 사람 active
        var tree = $.ui.fancytree.getTree("#addrDefaltPop3");
        tree.visit(function(node){
        	
        	var htmlCode = $.parseHTML(node.title)[1];
        	if(htmlCode.className == "emp" && htmlCode.value == userNum){
        		
        		node.parent.setExpanded(true);
        		
        		if(node.parent.parent != null){
        			node.parent.parent.setExpanded(true);	
        		}
        		if(node.parent.parent.parent != null){
            		node.parent.parent.parent.setExpanded(true);
        		}
        		node.setActive(true);
        		
        		selectEmpOne3(userNum);
        		
        	}
        });
	});
	
	// 검색
	$("#addr_pop_srch_cnt3").on("keyup", function(){
		$("#addr_pop_srch_btn").trigger("click");
	});
	
	$("#addr_pop_srch_btn").on("click", function(){
		var sel = $("#addr_pop_srch_sel3").val();
		var srchVal = $("#addr_pop_srch_cnt3").val();
		
		console.log("srchVal : " + srchVal);
		
        var tree = $.ui.fancytree.getTree("#addrDefaltPop3");
        tree.visit(function(node){
        	var htmlCode = $.parseHTML(node.title);
        	var srchCode = node.title.split("<input")[0];
        	
        	if(srchCode.search(" ") != -1){
        		srchCode = srchCode.split(" ")[0];
        	}

        	console.log("srchCode" + srchCode);
         	if(htmlCode[1].className == sel && srchCode.search(srchVal) != -1){
         		
        		node.setActive(true);
        		
        		if(sel == "emp"){
             		var user = $.parseHTML(node.title)[1];
             		userNum = user.value;
            		node.parent.setExpanded(true);
            		selectEmpOne3(userNum);
        			
        		}else{
            		node.setExpanded(true);
        			
        		}
        		
        	}
         	
        });
	});
	
	//사원 클릭 시 정보 가져오기
    function selectEmpOne3(eNo) {
    	
    	$.ajax({
    		url:"selectEmpOne.ad",
    		type:"post",
    		data:{
    			eNo:eNo
    		},
    		success:function(data){
    			console.log("ajax : " + data);  				
    			
    			var idx = $('#personList_share').children().find('tr').length;
    			$("#personList_share").append("<tr><td>" 
    									+ data.info.DEPT_NAME + "</td><td>"
    									+ (data.info.POS_NAME + "/" + data.info.JOB_NAME) + "</td>"
    									+ "<td>"
    									+ data.info.EMP_NAME + "<input type='hidden' class='shareEmpNo' value='" + eNo + "'>"
    									+ "</td><td>" 
    									+ data.info.EMP_ID + "@" + "${domain}" + "</td>");   			
    			
    		},
    		error:function(status){
    			console.log(status);
    		}
    		
    	});
    }
	
	
	// 모달 닫기 전 값 초기화
	$('#AddrPop3').on($.modal.BEFORE_CLOSE, function(event, modal) {
	    $("#addr_pop_srch_sel3").find("option").eq(0).prop("selected", true);
		$("#addr_pop_srch_cnt3").val("");
		
	});
	
	
    
</script>