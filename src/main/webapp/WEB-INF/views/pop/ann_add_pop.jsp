<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">

<div class="tbl_top_wrap">
                      <div class="menu_tit"><h2>주소록</h2></div>
                      <!-- 테이블 위 컨텐츠 시작 -->

                        <!-- 부서 셀렉트 박스 시작 -->
                        <div class="addr_sel_wrap">
                            <div class="add_sel ann_add_sel">
                                <input type="text" name="" id="" placeholder="부서를 선택하세요." readonly class="add_sel_ipt">
                                <div class="add_sel_opt">
                                    <div id="addrSelTree" class="tree_menu">
                                        <ul>
                                            <li class="folder">회장실
                                                <ul>
                                                    <li class="folder">홍보부
                                                        <li class="folder">회장실
                                                            <ul>
                                                                <li class="folder">홍보부</li>
                                                            </ul>
                                                        </li>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li class="folder">총무부
                                                <ul>
                                                    <li class="folder">인사부
                                                        <ul>
                                                            <li class="folder">홍보부</li>
                                                            <li class="folder">홍보부</li>
                                                        </ul>
                                                    </li>
                                                    <li class="folder">홍보부</li>
                                                </ul>
                                            </li>
                                            <li class="folder">회장실
                                                <ul>
                                                    <li class="folder">홍보부</li>
                                                    <li class="folder">홍보부</li>
                                                </ul>
                                            </li>
                                            <li class="folder">총무부
                                                <ul>
                                                    <li class="folder">인사부
                                                        <ul>
                                                            <li class="folder">홍보부</li>
                                                            <li class="folder">홍보부</li>
                                                        </ul>
                                                    </li>
                                                    <li class="folder">홍보부</li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
   <div class="main_cnt_desc2">
      <button class="btn_solid_pink add test">검색</button><input type="text"
         name="" id="">
   </div>
                            </div>
                        </div>
                        <!-- 부서 셀렉트 박스 끝 -->
                      <!-- 테이블 위 컨텐츠 끝 -->
                  </div>


 <!-- 기본 테이블 시작 -->


<div class="tbl_common tbl_basic ann_add">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn align">
                                <tr>
                                    <td class="tit"><input type="checkbox" name="check1" id="check1" value="1"></td>
                                    <td class="tit align">이름</td>
                                    <td class="tit align">ID</td>
                                    <td class="tit align">부서</td>
                                    <td class="tit align">연차</td>
                                    <td class="tit align">추가 연차</td>
                                    <td class="tit align">추가 일수</td>
                            </tr>
                               <tr>
                                     <td></td>
                                     <td></td>
                                     <td></td>
                                     <td></td>
                                     <td></td>
                                     <td></td>
                                     <td></td>
                                     
                               </tr>

                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->

<script>
//부서 셀렉트 박스
$("#addrSelTree").fancytree({
    imagePath: "skin-custom/",
    click: function(event, data){
        var node = data.node;
        console.log(node);
    }
});
$(".fancytree-container").addClass("fancytree-connectors");

$(".add_sel_ipt").on("click", function(){
    $(this).siblings(".add_sel_opt").slideToggle();
});
</script>
<button class="btn_main ann_add_close"><a href="#" rel="modal:close">Close</a></button>
