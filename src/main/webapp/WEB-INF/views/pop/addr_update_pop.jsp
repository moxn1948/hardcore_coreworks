<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style>
    /* 모달 안에 데이트 피커 넣을 때 */
    .ui-datepicker{z-index: 6000 !important;}
</style>

<form action="updateOutsider.ad" method="post" id="addrUpdatePop">
	<input type="hidden" value="${ sessionScope.loginUser.empDivNo }" name="empDivNo">
	<input type="hidden" value="${ sessionScope.loginUser.deptNo }" name="deptNo">
	<input type="hidden" name="outNo" id="upOutNo">
	<h3 class="main_tit">연락처 수정</h3>
	<div class="addr_pop_cnt">
       <div class="main_ctn">
           <div class="menu_tit"></div>
           <!-- ** 코드 작성부 시작 -->
           <div class="main_cnt">
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">주소록</div>
	                   <div class="main_cnt_desc">
	                       <select id="upAddrType" name="type">
	                           <option value="" hidden>선택</option>
	                           <option value="DEPT">공용 주소록</option>
	                           <option value="EMP">내 주소록</option>
	                       </select>
	                   </div>
 	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">그룹</div>
	                   <div class="main_cnt_desc">
	                       <select id="upAddrNo" name="addrNo" disabled>
	                       </select>
	                   </div>
	               </div> 
               </div>
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">이름</div>
	                   <div class="main_cnt_desc"><input type="text" name="outName" id="upOutName"></div>
	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">직책</div>
	                   <div class="main_cnt_desc"><input type="text" name="outPos" id="upOutPos"></div>
	               </div>
               </div>
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">회사명</div>
	                   <div class="main_cnt_desc"><input type="text" name="outCom" id="upOutCom"></div>
	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">직급</div>
	                   <div class="main_cnt_desc"><input type="text" name="outJob" id="upOutJob"></div>
	               </div>
               </div>
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">부서</div>
	                   <div class="main_cnt_desc"><input type="text" name="outDept" id="upOutDept"></div>
	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">번호</div>
	                   <div class="main_cnt_desc"><input type="text" name="outPhone" id="upOutPhone"></div>
	               </div>
               </div>
               <div class="clearfix main_cnt_half">
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">이메일</div>
	                   <div class="main_cnt_desc"><input type="email" name="outEmail" id="upOutEmail"></div>
	               </div>
	               <div class="main_cnt_list clearfix">
	                   <div class="main_cnt_tit">팩스번호</div>
	                   <div class="main_cnt_desc"><input type="text" name="outFax" id="upOutFax"></div>
	               </div>
               </div>
               <div class="main_cnt_list clearfix">
                   <div class="main_cnt_tit">회사주소</div>
                   <div class="main_cnt_desc">
                       <!-- 주소 검색 폼 시작 -->
                   	<div class="address_form_wrap">
                   		<div class=""><input type="text" name="" id="up_postcode_form" placeholder="우편번호" readonly><a href="#" class="button btn_pink" id="up_address_srch_btn" onclick="upDaumPostcode();">검색</a></div>
                   		<div class=""><input type="text" name="" id="up_address_form" placeholder="주소" readonly></div>
                   		<div class=""><input type="text" name="" id="up_detailAddress_form" placeholder="상세주소"></div>
                   		<input type="hidden" name="outAddr" id="upOutAddr">
                   	</div>
                       <!-- 주소 검색 폼 끝 -->
                   </div>
               </div>
               <div class="main_cnt_list clearfix">
                   <input type="checkbox" id="up_spe_day"><label for=up_spe_day>기념일 사용</label>
                   <div class="up_spe_day_wrap">
                       <ul class="up_spe_day_ctn"></ul>
                       <button class="btn_main up_spe_day_add_btn" type="button">추가</button>
                   </div>
               </div>
               <input type="hidden" name="calNum" id="upCalNum">
           </div>
           <!-- ** 코드 작성부 끝 -->
       </div>
   <!-- 메인 컨텐츠 영역 끝! -->
	</div>
	
	<div class="pop_close_wrap">
	    <button class="btn_pink update_out_can_btn" type="button">취소</button>
	    <button class="btn_solid_main" type="submit" onclick="return upOutsider();">수정</button>
	</div>
</form>



<script>
/* 	var dNo = '${ sessionScope.loginUser.deptNo }';
	var eNo = '${ sessionScope.loginUser.empDivNo }'; */
    var upNodeArr = new Array(); // 기념일 속성 idx 판별 위한 배열, 추후 자바 쪽에서 이용
    var up_use_spe_day = false; // 추후 기념일 있는지 없는 지 판별 후 넣기
    var up_use_spe_day_count = 0; // 기존 기념일 개수
    var up_node_count = 1;
    var up_real_node = 1; // 실제 기념일 개수

    var up_base_node = "<li class='spe_day_list'><input type='hidden' class='memoryNoIpt' name='memoryNo' value='0'><div class='spe_day_tit'><input type='text' name='memoryName' id='' class='spe_name' placeholder='기념일명을 입력하세요.'><input type='text' name='memoryDate' id='' class='datepicker' placeholder='날짜를 선택하세요.' autocomplete='off'><div class='up_spe_day_date_btn'><button class='btn_white' type='button'>삭제</button></div></div><div class='spe_day_date_type'><input type='radio' name='' class='solar' value='SUN' checked><label for='' class='solar_txt'>양력</label><input type='radio' name='' id='' class='lunar' value='MOON'><label for='' class='lunar_txt'>음력</label></div></li>";
    $(function(){

        // 기념일
        (function(){
            
            $("#up_spe_day").on("change", function(){
                if($(this).prop("checked") == true){
                    $(".up_spe_day_wrap").show();
                    $(".up_spe_day_ctn").append(up_base_node);
                    $(".up_spe_day_ctn").find(".spe_day_list").eq(0).find(".datepicker").eq(0).prop("id", "upDatepicker1").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        nextText: '다음 달',
                        prevText: '이전 달',
                        dateFormat: "yy/mm/dd",
                        showMonthAfterYear: true , 
                        dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                    });
                        
                    $(".up_spe_day_ctn").find(".spe_day_list").eq(0).find(".solar").eq(0).prop({
                                                                                                "name" : "cal_type1",
                                                                                                "id" : "upSolar1"
                                                                                            }).siblings(".solar_txt").prop("for", "upSolar1");
                    $(".up_spe_day_ctn").find(".spe_day_list").eq(0).find(".lunar").eq(0).prop({
                                                                                                "name" : "cal_type1",
                                                                                                "id" : "upLunar1"
                                                                                            }).siblings(".lunar_txt").prop("for", "upLunar1");
                    upNodeArr[0] = 1;
                }else{
                    var spe_use_yn = confirm("기념일 사용을 해제하시면 기존의 기념일이 삭제됩니다.");
                    if(spe_use_yn){

                        $(".up_spe_day_wrap").hide();
                        $(".up_spe_day_ctn").find(".spe_day_list").remove();
                        up_node_count = 1;
                        real_node = 1;
                        upNodeArr = new Array();
                    } else{
                        $(this).prop("checked", true);
                        return false;
                    }
                }
            });


            $(document).on("click", ".up_spe_day_add_btn", function(){
            	up_node_count++;
                up_real_node++;
                upNodeArr[up_node_count - 1] = 1;
                $(".up_spe_day_ctn").append(up_base_node);
                // $(".up_spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".spe_name").eq(0).prop("name", "spe_name" + node_count);
                $(".up_spe_day_ctn").find(".spe_day_list").eq(up_real_node - 1).find(".solar").eq(0).prop({
                                                                                                        "name" : "cal_type" + up_node_count,
                                                                                                        "id" : "upSolar" + up_node_count
                                                                                                    }).siblings(".solar_txt").prop("for", "upSolar"+up_node_count);
                $(".up_spe_day_ctn").find(".spe_day_list").eq(up_real_node - 1).find(".lunar").eq(0).prop({
                                                                                                        "name" : "cal_type" + up_node_count,
                                                                                                        "id" : "upLunar" + up_node_count
                                                                                                    }).siblings(".lunar_txt").prop("for", "upLunar"+up_node_count);
                
                $(".up_spe_day_ctn").find(".spe_day_list").eq(up_real_node - 1).find(".datepicker").eq(0).prop("id", "upDatepicker" + up_node_count);
                $(".up_spe_day_ctn").find(".spe_day_list").eq(up_real_node - 1).find(".datepicker").eq(0).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    nextText: '다음 달',
                    prevText: '이전 달',
                    dateFormat: "yy/mm/dd",
                    showMonthAfterYear: true , 
                    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                });
            }); 
            
	            $(document).on("click", ".up_spe_day_date_btn button", function(){
	                var list_count = $(".up_spe_day_ctn").find(".spe_day_list").length;
	
	                if(list_count > 1){
	                	up_real_node--;
	                    $(this).parents(".spe_day_list").remove();
	                    var node_idx = $(this).parents(".spe_day_list").find(".datepicker").prop("id").split("upDatepicker")[1];
	                    upNodeArr[node_idx - 1] = 0;
	                }else{
	                    alert("기념일은 최소 1개 이상 등록해야합니다.");
	                }

					console.log(upNodeArr);
	            });
	
        })();
        
    });


	// 그룹 종류 ajax
	$("#upAddrType").on("change", function(){
		var type = $(this).val();
		
		$("#upAddrgroup").prop("disabled", true);
		up_addr_Grp(type);
	});
	
	
	function up_addr_Grp(type){

		$.ajax({
			url:"outsiderGrpList.ad",
			type:"post",
			data: {
				type:type,
				dNo:dNo,
				eNo:eNo
				
			},
			success:function(data) {
				console.log(data.grpList);
 				
				$("#upAddrNo").find("option").remove();
				
				for (var i = 0; i < data.grpList.length; i++) {
					
					$("#upAddrNo").append("<option value=" + data.grpList[i].addrNo + ">" + data.grpList[i].addrgroup + "</option>")
				}
				$("#upAddrNo").prop("disabled", false);
				
				if(updateOutAlreadyAddrNo != 0){

					for(var i = 0; i < $("#upAddrNo").find("option").length; i++){
						if($("#upAddrNo").find("option").eq(i).val() == updateOutAlreadyAddrNo){
							$("#upAddrNo").find("option").eq(i).prop("selected", true);
						}
					}
				}
				
			},
			error:function(status) {
				console.log(status);
			}
		});	
	}
	
    // 주소 검색 시작
    function upDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('up_postcode_form').value = data.zonecode;
                document.getElementById("up_address_form").value = addr;
                document.getElementById("up_detailAddress_form").focus();
            }   
        }).open();
    }

    $("#up_postcode_form, #up_address_form").on("click", function(){
        $("#up_address_srch_btn").trigger("click");
    });
    // 주소 검색 끝
    
    // submit 전 처리
    function upOutsider(){
    	
    	if($("#upAddrType").val() == ""){
    		alert("주소록을 선택해주세요.");
    		
    		return false;
    	}
    	if($("#upOutName").val() == ""){
    		alert("이름을 입력해주세요.");
    		
    		$("#upOutName").focus();
    		
    		return false;
    	}

    	if($("#upOutPhone").val() == "" && $("#upOutEmail").val() == ""){
    		alert("번호 또는 이메일 중 하나는 필수 기입사항입니다.");
    		
    		if($("#upOutPhone").val() == ""){
	    		$("#upOutPhone").focus();
    		}else{
    			$("#upOutEmail").focus();
    		}
    		
    		return false;
    	}
    	
    	if($("#up_postcode_form").val() != "" && $("#up_address_form").val() != ""){
    		var addrStr = "";
    		
    		addrStr = $("#up_postcode_form").val() + "/" + $("#up_address_form").val();
    		
    		if($("#up_detailAddress_form").val() != ""){
    			addrStr += "/" + $("#up_detailAddress_form").val();
    		}
    		
    		$("#upOutAddr").val(addrStr);
    		
    	}
    	
    	if($("#up_spe_day").prop("checked")){
    		$("#upCalNum").val(upNodeArr.toString());
    	}
    	
    	if(confirm("연락처를 수정하시겠습니까?")){
    		
    		return true;
    	}else{
    		
    		return false;
    	}
    }

    	$(".update_out_can_btn").on("click", function(){
    		if(confirm("수정을 취소하시겠습니까?")){
    			$.modal.close();	
    		}
    	});
	
    	function alreadySpeDay(){

            // 기존 기념일이 있다면
                $("#up_spe_day").prop("checked", true);
                $(".up_spe_day_wrap").show();

                for(var i = 1; i <= up_use_spe_day_count; i++){

                    if(i != 1){
                    	up_node_count++;
                        up_real_node++;
                    }

                    upNodeArr[up_node_count - 1] = 1;
                    $(".up_spe_day_ctn").append(up_base_node);
                    $(".up_spe_day_ctn").find(".spe_day_list").eq(up_real_node - 1).find(".already").eq(0).prop("value", "already");

                    $(".up_spe_day_ctn").find(".spe_day_list").eq(up_real_node - 1).find(".solar").eq(0).prop({
                                                                                                            "name" : "cal_type" + up_node_count,
                                                                                                            "id" : "upSolar" + up_node_count
                                                                                                        }).siblings(".solar_txt").prop("for", "upSolar"+up_node_count);
                    $(".up_spe_day_ctn").find(".spe_day_list").eq(up_real_node - 1).find(".lunar").eq(0).prop({
                                                                                                            "name" : "cal_type" + up_node_count,
                                                                                                            "id" : "upLunar" + up_node_count
                                                                                                        }).siblings(".lunar_txt").prop("for", "upLunar"+up_node_count);
                    
                    $(".up_spe_day_ctn").find(".spe_day_list").eq(up_real_node - 1).find(".datepicker").eq(0).prop("id", "upDatepicker" + up_node_count);
                    $(".up_spe_day_ctn").find(".spe_day_list").eq(up_real_node - 1).find(".datepicker").eq(0).datepicker({
                        changeMonth: true,
                        changeYear: true,
                        nextText: '다음 달',
                        prevText: '이전 달',
                        dateFormat: "yy/mm/dd",
                        showMonthAfterYear: true , 
                        dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                    });
                    
                }
                console.log("node_count:"+up_node_count);
                console.log("real_node:"+up_real_node);
                console.log("nodeArr:"+upNodeArr);
    	}
        
	// 모달 닫기 전 값 초기화
 	$('#addrUpdatePop').on($.modal.BEFORE_CLOSE, function(event, modal) {
	    $("#upAddrType").find("option").eq(0).prop("selected", true);
	    $("#upAddrNo").find("option").remove();
		$("#upOutName").val("");
		$("#upOutPos").val("");
		$("#upOutCom").val("");
		$("#upOutJob").val("");
		$("#upOutDept").val("");
		$("#upOutPhone").val("");
		$("#upOutEmail").val("");
		$("#upOutFax").val("");
		$("#up_postcode_form").val("");
		$("#up_address_form").val("");
		$("#up_detailAddress_form").val("");
		$("#up_spe_day").prop("checked", false);
        $(".up_spe_day_wrap").hide();
        $(".up_spe_day_ctn").find(".spe_day_list").remove();
        up_node_count = 1;
        up_real_node = 1;
        upNodeArr = new Array();
		
	});
	
</script>