<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
<style>
.modal{max-width: 680px;}
</style>

<h3 class="main_tit">주소록</h3>
<div class="addr_info_pop_wrap addr_de_pop_wrap">
    <div class="addr_de_pop_srch">
        <select name="" id="addr_pop_srch_sel3">
            <option value="emp">사원명</option>
            <option value="dept">부서명</option>
        </select>
        <input type="text" name="" id="addr_pop_srch_cnt4" autocomplete="off">
        <button class="btn_solid_main" id="addr_pop_srch_btn">검색</button>
    </div>
    <div class="addr_de_pop_ctn clearfix">
        <div class="addr_dept_pop_ctn">
            <div class="addr_dept_pop">
                <div id="addrDefaltPop4" class="tree_menu">
               		<ul>
                       <c:forEach var="list" items="${ deptList }">
                      			<li class="folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
                    			    <ul>
	                    			    <c:forEach var="empList" items="${ empList }">
		                      					<c:if test="${ list.deptNo == empList.DEPT_NO }">
		                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
		                     						</li>
		                      					</c:if>
	                     				</c:forEach>
	                      				<c:if test="${ list.deptOrderList.size() != 0 }">
	                    						<c:forEach var="list2" items="${ list.deptOrderList }">
	                      							<li class="folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
				                       					<ul>
						                    			    <c:forEach var="empList" items="${ empList }">
							                      					<c:if test="${ list2.deptNo == empList.DEPT_NO }">
							                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
							                     						</li>
							                      					</c:if>
						                     				</c:forEach>
						                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
						                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
						                       							<li class="folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
										                       				<ul>
											                    			    <c:forEach var="empList" items="${ empList }">
												                      					<c:if test="${ list3.deptNo == empList.DEPT_NO }">
												                      						<li data-cstrender="true"><c:out value="${ empList.EMP_NAME } ${ empList.JNAME }" /><input type="hidden" value="${ empList.EMP_DIV_NO }" class="emp">
												                     						</li>
												                      					</c:if>
											                     				</c:forEach>
										                     				</ul>
						                       							</li>
						                       						</c:forEach>
						                       				</c:if>
				                       					</ul>
	                      							</li>
	                      						</c:forEach>
	                      				</c:if>
                   					</ul>
                      			</li>
                       </c:forEach>
                 	</ul>
                </div>
            </div>
        </div>
        <div class="addr_info_pop_ctn">
            <div class="addr_info_pop">
                <div class="tit_wrap clearfix">
                    <div class="name_area">
                        <p class="name_cnt"></p>
                    </div>
                    <div class="link_area">
                        <button class="btn_main">이메일</button>
                        <button class="btn_main">1:1대화</button>
                    </div>
                </div>
                <div class="desc_wrap clearfix" id="docPersonDetail">
                    <div class="img_area" id="profile4">
                        <!-- 사용자 이미지 없을때 -->
                        <div class="user_img_wrap user_img_none">
                            <i class="fas fa-user-circle"></i>
                        </div>
                        <!-- 사용자 이미지 있을때 -->
                        <div class="user_img_wrap user_img_has">
                            <img src="" alt="" id="profileImg4">
                        </div> 
                    </div>
                    <div class="info_area">
                        <ul class="info_ctn">
                            <li class="info_list clearfix">
                                <p class="tit">이름</p>
                                <p class="cnt" id="empName4"></p>
                                <input type="hidden" id="empDivNo3" name="empDivNo3">
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">직책/직급</p>
                                <p class="cnt" id="posName4"></p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">부서</p>
                                <p class="cnt" id="deptName4"></p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">이메일</p>
                                <p class="cnt" id="empId4"></p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">내선번호</p>
                                <p class="cnt" id="extPhone4"></p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">핸드폰번호</p>
                                <p class="cnt" id="phone4"></p>
                            </li>
                            <li class="info_list clearfix">
                                <p class="tit">팩스</p>
                                <p class="cnt" id="fax4"></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
<div class="pop_close_wrap">
    <button class="btn_main"><a onclick="selectDrafter();" rel="modal:close" class="onlyAddrPopClose">확인</a></button>
</div>
<script>
	var pathPop = "${ contextPath }";
	var domainPop = "${ domain }";
	
	
	
    $(function(){
        $("#addrDefaltPop4").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                }
            },
            click: function(event, data){
                var node = data.node;
                
                var htmlCode = $.parseHTML(node.title)[1];
                
                if(htmlCode.className == "emp"){
                    console.log(htmlCode.value);
                    userNum = htmlCode.value;
                    selectEmpOne4(userNum);	
            	}
            }
        });
        $(".fancytree-container").addClass("fancytree-connectors");
        
        $('#searchDraftPop').click(function(){
        	$('#docPersonDetail div:nth-child(2) > ul li p:not(.tit)').text("");
        })
  
    });
    
    // tree에서 사원 클릭 시
	var userNum = 0;
    
    // 모달 닫은 후 접기
	$('#AddrPop4').on($.modal.AFTER_CLOSE, function(event, modal) {

        var tree = $.ui.fancytree.getTree("#addrDefaltPop4");
        tree.visit(function(node){
    		node.setExpanded(false);
        });
        
	});
	
	// 모달 열 때 active
	$(document).on("click", ".open_modal", function(){
		userNum = $(this).find(".userNum")[0].value;
		console.log("userNum : " + userNum)
		
		// 특정 사람 active
        var tree = $.ui.fancytree.getTree("#addrDefaltPop4");
        tree.visit(function(node){
        	
        	var htmlCode = $.parseHTML(node.title)[1];
        	if(htmlCode.className == "emp" && htmlCode.value == userNum){
        		
        		node.parent.setExpanded(true);
        		
        		if(node.parent.parent != null){
        			node.parent.parent.setExpanded(true);	
        		}
        		if(node.parent.parent.parent != null){
            		node.parent.parent.parent.setExpanded(true);
        		}
        		node.setActive(true);
        		
        		selectEmpOne4(userNum);
        		
        	}
        });
	});
	
	// 검색
	$("#addr_pop_srch_cnt4").on("keyup", function(){
		$("#addr_pop_srch_btn").trigger("click");
	});
	
	$("#addr_pop_srch_btn").on("click", function(){
		var sel = $("#addr_pop_srch_sel3").val();
		var srchVal = $("#addr_pop_srch_cnt4").val();
		
		console.log("srchVal : " + srchVal);
		
        var tree = $.ui.fancytree.getTree("#addrDefaltPop4");
        tree.visit(function(node){
        	var htmlCode = $.parseHTML(node.title);
        	var srchCode = node.title.split("<input")[0];
        	
        	if(srchCode.search(" ") != -1){
        		srchCode = srchCode.split(" ")[0];
        	}

        	console.log("srchCode" + srchCode);
         	if(htmlCode[1].className == sel && srchCode.search(srchVal) != -1){
         		
        		node.setActive(true);
        		
        		if(sel == "emp"){
             		var user = $.parseHTML(node.title)[1];
             		userNum = user.value;
            		node.parent.setExpanded(true);
            		selectEmpOne4(userNum);
        			
        		}else{
            		node.setExpanded(true);
        			
        		}
        		
        	}
         	
        });
	});
	
	// 사원 클릭 시 정보 노출 함수
	function selectEmpOne4(eNo){
		$.ajax({
		url:"selectEmpOne.ad",
		type:"post",
		data: {
			eNo:eNo
		},
		success:function(data) {
			console.log(data)
			
			$("#empDivNo3").val(eNo);
			$("#empName4").text(data.info.EMP_NAME);
			$("#posName4").text(data.info.POS_NAME + "/" + data.info.JOB_NAME);
			$("#deptName4").text(data.info.DEPT_NAME);
			$("#empId4").text(data.info.EMP_ID + "@" + domainPop);
			if(data.info.EXT_PHONE != null){
				$("#extPhone4").text(data.info.EXT_PHONE);
				
			}else{
				$("#empPhone4").text("-");
			}
			if(data.info.PHONE != null){
				$("#phone4").text(data.info.PHONE);
				
			}else{
				$("#phone4").text("-");
			}
			if(data.info.FAX != null){
				$("#fax4").text(data.info.FAX);
				
			}else{
				$("#fax4").text("-");
			}
			
			if(data.profile == null){
				$("#profile4").find(".user_img_has").hide();
				$("#profile4").find(".user_img_none").show();
			}else{
				$("#profile4").find(".user_img_has").show();
				$("#profile4").find(".user_img_none").hide();
				$("#profileImg4").prop("src", pathPop+"/resources/uploadFiles/"+data.profile.CHANGE_NAME);
			}
		},
		error:function(status) {
			console.log(status);
		}
		});
	}
	
	// 모달 닫기 전 값 초기화
	$('#AddrPop4').on($.modal.BEFORE_CLOSE, function(event, modal) {
	    $("#addr_pop_srch_sel3").find("option").eq(0).prop("selected", true);
		$("#addr_pop_srch_cnt4").val("");
		
	});
	
	
	$(document).on('click', '.onlyAddrPopClose', function(){
		$('#selectDocBtn').trigger('click');
	})
	
	
    
</script>