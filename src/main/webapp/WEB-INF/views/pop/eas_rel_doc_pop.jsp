<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<style>
.modal{max-width: 800px;}
</style>

<h3 class="main_tit">관련문서 상세보기</h3>
<!-- 기본 테이블 시작 -->
	<!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
						<h2 class="docu_tit" id="relPopDocTit"></h2>
                        <div class="tbl_wrap eas_tbl_doc">
	                          <div>
	                            <table class="tbl_ctn">
		                            <colgroup>
	                                    <col style="width: 10.5%;">
	                                    <col style="width: *;">
	                                    <col style="width: 7%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                    <col style="width: 9%;">
	                                </colgroup>
	                                <tr class="eas_tit" id="relPopAsListTit">
	                                    <td rowspan="2" class="doc_con doc_tit">문서번호</td>
	                                    <td rowspan="2" class="doc_con" id="relPopDocNo"></td>
	                                    <td rowspan="4" class="doc_con small doc_tit">결재</td>
	                                </tr>  
	                               	<tr class="eas_tit eas_tit_center" id="relPopAsList">    
	                                </tr>                             
	                                <tr class="eas_tit" >
	                                    <td rowspan="2" class="doc_con doc_tit">작성일자</td>
	                                    <td rowspan="2" class="doc_con" id="relPopDocDate"></td>                         	                                                                   
	                                </tr>
	                                <tr class="eas_tit"></tr>                                
	                                <tr class="eas_tit" id="relPopAgmListTit">
	                                    <td rowspan="2" class="doc_con doc_tit">작성부서</td>
	                                    <td rowspan="2" class="doc_con" id="relPopDeptName"></td>
	                                    <td rowspan="4" class="doc_con small doc_tit">합의</td> 
	                                </tr>
	                                <tr class="eas_tit eas_tit_center" id="relPopAgmList">

	                                </tr>
	                                <tr class="eas_tit">
	                                    <td rowspan="2" class="doc_con doc_tit">작성자</td>
	                                    <td rowspan="2" class="doc_con" id="relPopEname"></td>                              
	                                </tr>  
	                                <tr class="eas_tit"></tr>
	                                <tr class="eas_tit">
	                                    <td class="doc_con doc_tit">수신자</td>
	                                    <td colspan="7" class="doc_con" id="relPopReceName"></td>                                 
	                                </tr>
	                                <tr class="eas_tit">
	                                    <td class="doc_con doc_tit">제목</td>
	                                    <td colspan="7" class="doc_con" id="relPopDocCntTit"></td>                                
	                                </tr>
	                                <!-- 기안 본문 작성 부분 -->
	                                <tr class="eas_tit">
	                                    <td rowspan="10" colspan="8" class="eas_content" id="relPopDocCnt">
	                                    </td>                              
	                                </tr>
	                                <!-- 기안 본문 작성 부분 끝 -->                                                       
	                            </table>
	                           </div>
                        </div>
                    </div>
        <!-- 기본 테이블 끝 -->
