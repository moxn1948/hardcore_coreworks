<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<h3 class="main_tit">문서함 수정용</h3>

    	<ul class="eas_tabs" id="eas_pop_tabs">
			<li class="eas_tab-link current" data-tab="eas_tab-1">완료함</li>										
		</ul>
		<!-- 완료함 시작 -->
		<div class="eas_tab-content current" id="eas_tab-1">
			<div class="eas_pop_top">
				<div class="eas_pop_top1">
					<div class="eas_pop_top_tit" id="completeDocListTitle"></div>
					<div class="eas_pop_top_select">년도<select><option>2020</option></select></div>
				</div>
				<div class="eas_pop_top2">
					<div>
						문서번호 <input type="text" class="pop_input">
						제목 <input type="text" class="pop_input">
						기안자 <i class="far fa-user"></i><input type="text" class="pop_input" id="searchDrafter">
						<a href="#AddrPop4" rel="modal:open" class="fileadd_Atag" id="searchDraftPop" hidden="true"></a>
						등록일자 <input type="text" name="" id="datepicker1" class="datepicker pop_input">
					</div>
					<div class="eas_pop_top_btn">
						<button class="btn_solid_pink">검색</button>
					</div>					
				</div>
			</div>
		
		<div class="eas_pop_padding_div">
			<!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic eas_pop_tbl">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" id="completeDocList">
                                <colgroup>
                                    <col style="width: 5%;">
                                    <col style="width: 25%;">
                                    <col style="width: 40%;">
                                    <col style="width: 15%;">
                                    <col style="width: 15%;">
                                </colgroup>
                                <thead class="eas_pop_tbl_head">
	                                <tr class="tbl_main_tit">
	                                    <th><input type="checkbox" id="checkAll"></th>
	                                    <th>문서번호</th>
	                                    <th>제목</th>
	                                    <th>기안(접수)자</th>
	                                    <th>결재일</th>
	                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="eas_pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                    
                    <div class="eas_pop_btn_div">
                    	<button class="btn_white" rel="modal:close">취소</button>
                    	<button class="btn_main"><a onclick="relDocAdd();">확인</a></button>
                    </div>
		</div>
		</div>
		<!-- 완료함 끝  -->
		

<!-- popup include -->
<div id="AddrPop4" class="modal">
	<jsp:include page="../pop/new_eas_addr_pop4.jsp" />
</div>

<script>
//탭 전환용 스크립트
$(document).ready(function(){
	
	$('ul.eas_tabs li').click(function(){
		console.log(this);
		var tab_id = $(this).attr('data-tab');
		console.log(tab_id);

		$('ul.eas_tabs li').removeClass('current');
		$('.eas_tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});
	
});

//문서 검색창 내에서 주소록 모달 띄우기
$(document).on('click', '#searchDrafter', function(){
	$('#searchDraftPop').trigger('click');
});

//주소록을 통해 기안자 정보 가져오기
function selectDrafter() {
	var name = $("#empName4").text();
	var eNo = $("#empDivNo3").val();
	
	$('#searchDrafter').val(name);
	$('#searchDrafter').append('<input type="hidden" id="searchedEno" value="' + eNo + '">');
}

</script>


<!-- <button class="btn_main"><a href="#" rel="modal:close">Close</a></button> -->
