<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!-- date picker css -->
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->

<style>
    /* 모달 크기 변경 */
    .modal{max-width: 500px;}
</style>
<h3 class="main_tit">일정 상세보기</h3>
<div class="calc_pop calc_sch_det">
	<input type="hidden" value="" id="detCalTypeHidden" name="">
    <div class="calc_ctn_list clearfix">
        <div class="tit">종류</div>
        <div class="cnt"><p class="txt" id="detCalType"></p></div>
    </div>
    <div class="calc_ctn_list clearfix">
        <div class="tit">날짜</div>
        <div class="cnt"><p class="txt" id="detDateArea"></p></div>
    </div>
    <div class="calc_ctn_list clearfix">
        <div class="tit">제목</div>
        <div class="cnt"><p class="txt" id="detCalTitle">아 졸라 피곤해요</p></div>
    </div>
    <div class="calc_ctn_list clearfix">
        <div class="tit">설명</div>
        <div class="cnt"><p class="txt" id="detCalCnt"></p></div>
    </div>
    <div class="calc_ctn_list clearfix" id="detCalAssoArea">
        <div class="tit" id="detShareTit"></div>
        <div class="cnt">
            <div class="tbl_common tbl_basic">
                <div class="tbl_wrap">
                    <table class="tbl_ctn" id="detCalAssoTbl">
                        <colgroup>
                            <col style="width: 15%;">
                            <col style="width: 30%;">
                            <col style="width: 15%;">
                            <col style="width: 40%;">
                        </colgroup>
                        <tr>
                            <th>사원명</th>
                            <th>직책/직급</th>
                            <th>부서</th>
                            <th>이메일</th>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="pop_close_wrap">
	<form action="deleteCalcOne.ca" method="post" class="detCalNoForm" id="detCalNoForm">
		<input type="hidden" value="" id="detCalNo" name="calNo">
	    <button class="btn_pink" id="detDelBtn" type="button" onclick="return detCalNoSubmit();">삭제</button>
	</form>
    <button class="btn_main" id="detUpBtn">수정</button>
    <button class="btn_white"><a href="#" rel="modal:close">확인</a></button>
</div>

<!-- datepicker api -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->
<script>
	$(function(){
		$("#detUpBtn").on("click", function(){
			updateCalInfo($("#detCalNo").val());
		});
	});
	
	function detCalNoSubmit(){
		if(confirm("일정을 삭제하시겠습니까?")){
			$("#detCalNoForm").submit();	
			
			return true;
		}else{
			return false;
		}
			
	}
	

	// 수정 버튼 클릭 시	
	function updateCalInfo(val){
		$("#newCalcUpBtn").show();
		$("#newCalcInsertBtn").hide();
		var type = $("#detCalTypeHidden").val();
		console.log("type : "  + type);
		$("#calcNewTit").text("일정 수정");
		
		$.ajax({
			url:"selectDetCalOne.ca",
			type:"post",
			data: {
				calNo:val
			},
			success:function(data) {
				console.log(data);
				
				for (var i = 0; i < $("#newCalKind").find("option").length; i++) {
					if($("#newCalKind").find("option").eq(i).val() == type.toUpperCase()){
						$("#newCalKind").find("option").eq(i).prop("selected", true);
						$("#newCalKind").trigger("change");
					}else{
						$("#newCalKind").find("option").eq(i).prop("hidden", true);
					}
				}
				
				$("#upCalNo").val(val);
				
				if(data.map.ALL_YN == 'Y'){
					$("#newAllYn").prop("checked", true);
					$("#newAllYn").trigger("change");
					$("#newAllDay").val(data.map.SDATE);
					
				}else{
					$("#newAllYn").prop("checked", false);
					$("#newAllYn").trigger("change");
					
					$("#newCalSdate").val(data.map.SDATE);
					$("#newCalSdate").trigger("change");
					$("#newCalEdate").val(data.map.EDATE);
					$("#newCalEdate").trigger("change");
					for (var i = 0; i < $("#newCalStime").find("option").length; i++) {
						if($("#newCalStime").find("option").eq(i).val() == data.map.CAL_STIME){
							$("#newCalStime").find("option").eq(i).prop("selected", true);
							break;
						}
					}
					$("#newCalStime").trigger("change");
					for (var i = 0; i < $("#newCalEtime").find("option").length; i++) {
						if($("#newCalEtime").find("option").eq(i).val() == data.map.CAL_ETIME){
							$("#newCalEtime").find("option").eq(i).prop("selected", true);
							break;
						}
					}
					$("#newCalEtime").trigger("change");
				}
				
				$("#newCalTitle").val(data.map.CAL_TITLE);
				$("#newCalCnt").val(data.map.CAL_CNT);
				
				if(data.asso.length != 0){
					for (var i = 0; i < data.asso.length; i++) {
						$("#personList").append("<tr><td><input type='hidden' name='eNo' class='eNo' value='" +
							data.asso[i].ENO
							+"'>" +
							data.asso[i].EMP_NAME
                            + "</td><td>" +
                            data.asso[i].PNAME + "/" + data.asso[i].JNAME
                            + "</td><td>" +
                            data.asso[i].DNAME
                            + "</td><td>" +
                            data.asso[i].EMP_ID + "@" + domain
                            + "<input type='hidden' name='assoCalNo' value='"+ data.asso[i].CAL_NO +"'></td></tr>");
					}				
				}
				
				// modal open
				$("#calNewSchPop").modal();
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
	}

	// 모달 닫기 전 값 초기화
	$('#calDetSch').on($.modal.BEFORE_CLOSE, function(event, modal) {
		console.log("Ddd");
		$("#detCalAssoTbl").find("tr:not(:first-child)").remove();
	});
	  // submit 전 처리
	  function updateCalcSubmit(){
			console.log($("#newCalStime").find("option").eq(0).prop("selected"));
			
			if(!$("#newAllYn").prop("checked")){
	
		    	if($("#newCalSdate").val() == ""){
		    		alert("시작일을 선택해주세요.");
		    		
		    		$("#newCalSdate").focus();
		    		
		    		return false;
		    	}
	
		    	if($("#newCalStime").find("option").eq(0).prop("selected")){
		    		alert("시작시간을 선택해주세요.");
		    		
		    		return false;
		    	}
		    	
		    	if($("#newCalEdate").val() == ""){
		    		alert("종료일을 선택해주세요.");
		    		
		    		$("#newCalEdate").focus();
		    		
		    		return false;
		    	}
		    		
		    	if($("#newCalEtime").find("option").eq(0).prop("selected")){
		    		alert("종료시간을 선택해주세요.");
		    		
		    		return false;
		    	}
	
				if($("#newCalStime").val() == "allday" && $("#newCalEtime").val() != "allday"){
		    		alert("시간을 종일로 선택하신 경우 시작일과 종료일이 동일해야합니다.");
		    		
		    		$("#newCalEtime").focus();
		    		
		    		return false;
				}
	
				if($("#newCalStime").val() != "allday" && $("#newCalEtime").val() == "allday"){
		    		alert("시간을 종일로 선택하신 경우 시작일과 종료일이 동일해야합니다.");
		    		
		    		$("#newCalEtime").focus();
		    		
		    		return false;
				}
		 		
			}else{
				if($("#newAllDay").val() == ""){
		    		alert("날짜를 선택해주세요.");
		    		
		    		$("#newAllDay").focus();
		    		
		    		return false;
				}
			}
	
			if($("#newCalTitle").val() == ""){
	  		alert("제목을 입력해주세요.");
	  		
	  		$("#newCalTitle").focus();
	  		
	  		return false;
			}
	
	  	if(confirm("일정을 수정하시겠습니까?")){
	  		$("#userCalcInfo").prop("action", "updateCalcOne.ca");
	  		
	  		return true;
	  	}else{
	  		
	  		return false;
	  	}
	  }
</script>