<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    /* 모달 크기 변경 */
    .modal{max-width: 500px;}   
    /* 모달 안에 데이트 피커 넣을 때 */
    .ui-datepicker{z-index: 6000 !important;}
</style>
<h3 class="main_tit">예약 시간</h3>

<div class="reservation">
	<div class="option">
		<input type="text" id="datepicker1" />
	</div>
	<div class="option">
		<select id="timeselect">
			<c:forEach begin="0" end="23" step="1" var="i">
					<option value=${ i }>${ i }시</option>
			</c:forEach>			
		</select>
	</div>
	<div class="option">
		<select id="minuteselect">
			<c:forEach begin="0" end="45" step="15" var="i">
				<option value=${ i }>${ i }분</option>
			</c:forEach>
		</select>
	</div>
</div>

<script>
	$("#datepicker1").datepicker({
	     changeMonth: true,
	     changeYear: true,
	     nextText: '다음 달',
	     prevText: '이전 달',
	     dateFormat: "yy/mm/dd",
	     showMonthAfterYear: true , 
	     dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
	     monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
	 });
	
	
</script>

<button class="btn_main" id="selectResv"><a href="#" rel="modal:close">확인</a></button>
<button class="btn_main" id="closeResv"><a href="#" rel="modal:close">취소</a></button>