<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page="../inc/main_menu.jsp" />

<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">
<style>
.main_page .contents_wrap .cnt .list .cnt_tit.notice{width: 82%;}
</style>

<div id="scroll_area" class="inner_rt">
	<!-- 메인 컨텐츠 영역 시작! -->
	<div class="main_ctn">
		<!-- ** 코드 작성부 시작 -->
		<div class="main_cnt">
			<div class="top_wrap">
				<div class="notice_wrap contents_wrap">
					<p class="tit">알림</p>
					<ul class="cnt">
                       	<c:if test="${ sessionScope.loginUser.auth.equals('ADMIN') }">
							<li class="clearfix list">
								<a class="cnt_tit ellipsis"></a>
								<a></a>
								<p></p>
							</li>
							<li class="clearfix list">
								<a class="cnt_tit ellipsis"></a>
								<a></a>
								<p></p>
							</li>  
							<li class="clearfix list">
								<a class="cnt_tit ellipsis">관리자 계정입니다.</a>
								<a></a>
								<p></p>
							</li>  
							<li class="clearfix list">
								<a class="cnt_tit ellipsis"></a>
								<a></a>
								<p></p>
							</li>  
							<li class="clearfix list">
								<a class="cnt_tit ellipsis"></a>
								<a></a>
								<p></p>
							</li>        		
                       	</c:if>
                       	<c:if test="${ !sessionScope.loginUser.auth.equals('ADMIN') }">
	                       	<c:if test="${ noticeList != null }">
								<c:forEach begin="1" end="${ 5 - noticeList.size() }">
									<li class="clearfix list">
										<a class="cnt_tit ellipsis">최신 알림이 없습니다.</a>
										<a></a>
										<p></p>
									</li>
								</c:forEach>
								<c:forEach var="i" items="${ noticeList }">
									<jsp:useBean id="now" class="java.util.Date"/>
	                                <fmt:parseDate value="${ i.alramDate }" pattern="yy/MM/dd" var="alramDate"/>
	                                <fmt:formatDate value="${ now }" pattern="yyyy/MM/dd" var="today"/>
	                                <fmt:formatDate value="${ alramDate }" pattern="yyyy/MM/dd" var="alram"/>
	                               	<c:set var="d" value="${ fn:split(i.alramDate , ' ') }"/>
	                               	<c:if test="${ today == alram }">
                                		<c:set var="time" value="${ d[1] } ${ d[2] == '오후' ? 'PM' : 'AM' }" />
	                                </c:if>
	                                <c:if test="${ today != alram }">
	                                	<c:set var="time" value="${ d[0] }"/>
	                                </c:if>
									<li class="clearfix list">
										<input type="hidden" class="alramNo" value="${ i.alramNo }" />
											<c:if test="${ i.alramType eq 'MAIL' }">
                                    			<c:if test="${ i.senderType eq 'IN' }">
													<a href="selectMail.ma" class="cnt_tit notice ellipsis">
                                    				[새 메일] ${ i.senderName } ${ i.jobName }(으)로부터 '${ i.mailTitle }' ${ i.alramTitle }
													</a>
                                    			</c:if>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'BOARD' }">
                                    			<a href="comList.bo" class="cnt_tit notice ellipsis">
                                    			[필독 게시물] '${ i.btitle }' ${ i.alramTitle }
                                    			</a>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'CALC' }">
                                    			<a href="comList.bo" class="cnt_tit notice ellipsis">
                                    			[일정 참석] '${ i.calTitle }' ${ i.alramTitle }
                                    			</a>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'EAS' }">
                                    			<a href="comList.bo" class="cnt_tit notice ellipsis">
                                    			[결재] '${ i.easTitle }' ${ i.alramTitle }
                                    			</a>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'REPLY' }">
                                    			<a href="comList.bo" class="cnt_tit notice ellipsis">
                                    			[댓글 등록] '${ i.replyBoard}' ${ i.alramTitle }
                                    			</a>
                                    		</c:if>
										
										<p class="cnt_time">${ time }</p>
									</li>
								</c:forEach>
							</c:if>
                       	</c:if>
					</ul>
				</div>
			</div>
			<div class="bottom_wrap clearfix">
				<div class="bottom_lt_wrap">
					<div class="eas_wait_wrap contents_wrap">
						<p class="tit">결재대기</p>
						<ul class="cnt">
                        	<c:if test="${ sessionScope.loginUser.auth.equals('ADMIN') }">
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis">관리자 계정입니다.</a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>        		
                        	</c:if>
                        	<c:if test="${ !sessionScope.loginUser.auth.equals('ADMIN') }">
								<c:if test="${ easList != null }">
									<c:forEach begin="1" end="${ 5 - easList.size() }">
										<li class="clearfix list">
											<a class="cnt_tit ellipsis">최신 결재대기가 없습니다.</a>
											<a></a>
											<p></p>
										</li>
									</c:forEach>
									<c:forEach var="list" items="${ easList }">
										<li class="clearfix list">
											<a href="selectEasDetail.ea?easNo=${ list.EASNO }&type=easWait&origin=${ list.ENO }&proxy=${ list.PROXYNO }" class="cnt_tit ellipsis">${ list.DOCTIT }</a>
											<a href="#addrPop1" rel="modal:open" class="cnt_name open_modal">${ list.SENDNAME }<input type="hidden" value="${ list.SENDNO }" class="userNum"></a>
											<p class="cnt_time">${ list.EASDATE }</p>
										</li>
									</c:forEach>
								</c:if>
                        	</c:if>
						</ul>
					</div>
					<div class="company_bd_wrap contents_wrap">
						<p class="tit">사내 공지사항</p>
						<ul class="cnt">
                        	<c:if test="${ sessionScope.loginUser.auth.equals('ADMIN') }">
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis">관리자 계정입니다.</a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>        		
                        	</c:if>
                        	<c:if test="${ !sessionScope.loginUser.auth.equals('ADMIN') }">
								<c:if test="${ allList != null }">
									<c:forEach begin="1" end="${ 5 - allList.size() }">
										<li class="clearfix list">
											<a class="cnt_tit ellipsis">최신 사내 공지사항이 없습니다.</a>
											<a></a>
											<p></p>
										</li>
									</c:forEach>
									<c:forEach var="list" items="${ allList }">
										<c:if test="${ list.READNO == 0 }">
										<li class="clearfix list">
										</c:if>
										<c:if test="${ list.READNO != 0 }">
										<li class="clearfix list read">
										</c:if>
											<form action="detailBoard.bo" method="post">
												<input type="hidden" value="${ list.BNO }" name="bno">
												<input type="hidden" value="${ list.BTYPE }" name="btype">
												<a href="javascript: void(0);" onclick="$(this).parent('form').submit();" class="cnt_tit ellipsis">${ list.BTITLE }</a>
											</form>
											<a href="#addrPop1" rel="modal:open" class="cnt_name open_modal">${ list.ENAM }<input type="hidden" value="${ list.BWRITER }" class="userNum"></a>
											<p class="cnt_time">${ list.SDATE }</p>
										</li>
									</c:forEach>
								</c:if>
                        	</c:if>
						</ul>
					</div>
				</div>
				<div class="bottom_rt_wrap">
					<div class="mail_wrap contents_wrap">
						<p class="tit">메일</p>
						<ul class="cnt">
                        	<c:if test="${ sessionScope.loginUser.auth.equals('ADMIN') }">
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis">관리자 계정입니다.</a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>        		
                        	</c:if>
                        	<c:if test="${ !sessionScope.loginUser.auth.equals('ADMIN') }">
								<c:if test="${ mailList != null }">
									<c:forEach begin="1" end="${ 5 - mailList.size() }">
										<li class="clearfix list">
											<a class="cnt_tit ellipsis">최신 메일이 없습니다.</a>
											<a></a>
											<p></p>
										</li>
									</c:forEach>
									<c:forEach var="list" items="${ mailList }">
										<c:if test="${ list.READYN == 0 }">
										<li class="clearfix list">
										</c:if>
										<c:if test="${ list.READYN != 0 }">
										<li class="clearfix list read">
										</c:if>
											<a href="selectOneMail.ma?mailNo=${ list.MNO }&empNo=${ list.RNO }" class="cnt_tit ellipsis">${ list.MAILTIT }</a>
											<a href="#addrPop1" rel="modal:open" class="cnt_name open_modal">${ list.ENAME }<input type="hidden" value="${ list.SENDNO }" class="userNum"></a>
											<p class="cnt_time">${ list.SDATE }</p>
										</li>
									</c:forEach>
								</c:if>
                        	</c:if>
						</ul>
					</div>
					<div class="team_bd_wrap contents_wrap">
						<p class="tit">팀 공지사항</p>
						<ul class="cnt">
                        	<c:if test="${ sessionScope.loginUser.auth.equals('ADMIN') }">
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis">관리자 계정입니다.</a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>  
								<li class="clearfix list">
									<a class="cnt_tit ellipsis"></a>
									<a></a>
									<p></p>
								</li>        		
                        	</c:if>
                        	<c:if test="${ !sessionScope.loginUser.auth.equals('ADMIN') }">
								<c:if test="${ teamList != null }">
									<c:forEach begin="1" end="${ 5 - teamList.size() }">
										<li class="clearfix list">
											<a class="cnt_tit ellipsis">최신 팀 공지사항이 없습니다.</a>
											<a></a>
											<p></p>
										</li>
									</c:forEach>
									<c:forEach var="list" items="${ teamList }">
										<c:if test="${ list.READNO == 0 }">
										<li class="clearfix list">
										</c:if>
										<c:if test="${ list.READNO != 0 }">
										<li class="clearfix list read">
										</c:if>
											<form action="detailBoard.bo" method="post">
												<input type="hidden" value="${ list.BNO }" name="bno">
												<input type="hidden" value="${ list.BTYPE }" name="btype">
												<a href="javascript: void(0);" onclick="$(this).parent('form').submit();" class="cnt_tit ellipsis">${ list.BTITLE }</a>
											</form>
											<a href="#addrPop1" rel="modal:open" class="cnt_name open_modal">${ list.ENAM }<input type="hidden" value="${ list.BWRITER }" class="userNum"></a>
											<p class="cnt_time">${ list.SDATE }</p>
										</li>
									</c:forEach>
								</c:if>
                        	</c:if>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- ** 코드 작성부 끝 -->
	</div>
	<!-- 메인 컨텐츠 영역 끝! -->
</div><!-- inner_rt end -->
</div>
</main>
</div>
<!-- 추후 주소록 팝업 include -->
<div id="addrPop1" class="modal">
    <jsp:include page="../pop/addr_pop_1.jsp" />
</div>


<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	
	$(function () {
		// 캘린더 api
        $("#main_menu_cal").jqxCalendar({width: 218, height: 220});

		// 현재시간 표시하는 함수
		printClock();
		

	});

	// 현재시간 표시하는 함수
	function printClock() {

		var clock = document.getElementById("main_now_time");
		var currentDate = new Date(); // 현재시간
		var calendar = currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getDate() // 현재 날짜
		var amPm = 'AM'; 
		var currentHours = addZeros(currentDate.getHours(), 2);
		var currentMinute = addZeros(currentDate.getMinutes(), 2);
		var currentSeconds = addZeros(currentDate.getSeconds(), 2);

		if (currentHours >= 12) { // 시간이 12보다 클 때 PM으로 세팅, 12를 빼줌
			amPm = 'PM';
			currentHours = addZeros(currentHours - 12, 2);
		}

		clock.innerHTML = currentHours + " : " + currentMinute + " : " + currentSeconds +
			" " + amPm; //날짜를 출력

		setTimeout(printClock, 1000); // 1초마다 printClock() 함수 호출
	}

	function addZeros(num, digit) { 
		var zero = '';
		num = num.toString();

		if (num.length < digit) {
			for (i = 0; i < digit - num.length; i++) {
				zero += '0';
			}
		}

		return zero + num;
	}
</script>
</body>

</html>