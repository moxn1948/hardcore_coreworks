<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/first_menu.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">	
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<!-- 주소검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>사원 등록</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                      
                  <form action="insertOneEmp.fr" id="insertForm" method="post" enctype="multipart/form-data">
                    <div class="main_cnt_list clearfix">
		               <div id="logoImgArea">
					<div style="width: 120px; height: 120px;overflow: hidden;">
						<img class="titleImg" style="width: 100%;height: auto;">
					</div>
						<div class="upload-btn_wrap">
                                <input type="file" name="attachments" onchange="loadImg(this, 1)" style="display:none">
                                <button type="button" class="btn_main btn_choise firstphoto" onclick="document.all.attachments.click()">선택
                                </button>
                              </div>
					</div>
                    
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사번</div>
                            <div class="main_cnt_desc emp_reg_pd">
                           		<input type="text" name="empNo" id="empNo">
                           		<div>
                           			<span id="dupNo"></span>
                           		</div>
                            </div>
                            <div class="main_cnt_desc"><button type="button" class="btn_main" onclick="duplicateNo()">중복확인</button></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">아이디</div>
                            <div class="main_cnt_desc emp_reg_pd">
                            	<input type="text" name="empId" id="empId2">
                            	<div>
                            		<span id="dupId"></span>
                            	</div>
                           	</div>
                            <div class="main_cnt_desc"><button type="button" class="btn_main" onclick="duplicateId()">중복확인</button></div>
                        </div>                       
                        <div class="main_cnt_list clearfix">
                           <div class="main_cnt_tit">비밀번호</div>
                           <div class="main_cnt_desc"><input type="password" name="empPwd" id="empPwd"></div>
                       </div> 
                       <div class="main_cnt_list clearfix">
                           <div class="main_cnt_tit">비밀번호 확인</div>
                           <div class="main_cnt_desc">
                           		<input type="password" name="empPwd2" id="empPwd2">
                           		<div>
                           			<span id="dupPwd"></span>
                           		</div>
                        	</div>
                       </div> 
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이름</div>
                            <div class="main_cnt_desc"><input type="text" name="empName" id="empName"></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">부서</div>
                            <div class="main_cnt_desc">
	                        	<div class="addr_sel_wrap" style="display:inline-block;">
                            <div class="add_sel">
                                <input type="text" name="depttitle" id="deptinput" placeholder="부서를 선택하세요." readonly class="add_sel_ipt">
                                <input type="hidden" name="deptNo" id="deptNo" />
                                <div class="add_sel_opt">
                                    <div id="addrSelTree" class="tree_menu">
				                  		<ul>
				                  			<li data-cstrender="true">전체보기<input type="hidden" value="0" class="dept"></li>
					                       <c:forEach var="list" items="${ deptList }">
				                       			<li class="folder dept_folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
				                       				<c:if test="${ list.deptOrderList.size() != 0 }">
				                       					<ul>
				                     						<c:forEach var="list2" items="${ list.deptOrderList }">
				                       							<li class="folder dept_folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
								                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
								                       					<ul>
								                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
								                       							<li class="folder dept_folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
								                       							</li>
								                       						</c:forEach>
								                       					</ul>
								                       				</c:if>
				                       							</li>
				                       						</c:forEach>
				                       					</ul>
				                       				</c:if>
				                       			</li>
					                       </c:forEach>
					                 	</ul>
                                    </div>
                                </div>
                            </div>
                        </div>
	                      	</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직급</div>
                            <div class="main_cnt_tit">
                            	<select name="jobNo" id="jobNo">
                            		<option value="none">직급선택</option>
	                            	<c:forEach items="${ jobList }" var="job">
	                            		<option value="${ job.jobNo }">${ job.jobName }</option>
	                            	</c:forEach>
	                        	</select>
                            </div>
                            <div class="main_cnt_tit">직책</div>
                            <div class="main_cnt_desc">
                            	<select name="posNo" id="posNo">
                            		<option value="none">직책선택</option>
	                            	<c:forEach items="${ posList }" var="pos">
	                            		<option value="${ pos.posNo }">${ pos.posName }</option>
	                            	</c:forEach>
	                        	</select>
                            </div>
                        </div>                      
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">내선전화</div>
                            <div class="main_cnt_desc"><input type="text" name="extPhone" id="extPhone"></div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">휴대전화</div>
                            <div class="main_cnt_desc">
                            	<input type="text" name="phone1" id="phone1" maxlength="3"> - <input type="text" name="phone2" id="phone2" maxlength="4"> - <input type="text" name="phone3" id="phone3" maxlength="4">
                            	<input type="hidden" name="phone" id="phone" />
                           	</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">생일</div>
                            <div class="main_cnt_desc">
                            	<input type="text" name="birth1" id="datepicker" class="datepicker" autocomplete="off">
                            	<input type="date" name="birth" hidden="true" />
                           	</div>
                        </div>                
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                            <div class="main_cnt_desc">
                            	<!-- 주소 검색 폼 시작 -->
                               <div class="address_form_wrap">
                                    <div class=""><input type="text" name="address1" id="postcode_form" placeholder="우편번호" readonly><a href="#" class="button btn_pink" id="address_srch_btn" onclick="DaumPostcode();">검색</a></div>
                                  	<div class=""><input type="text" name="address2" id="address_form" placeholder="주소" readonly></div>
                                  	<div class=""><input type="text" name="address3" id="detailAddress_form" placeholder="상세주소"></div>
                                  	<input type="hidden" name="address" id="address" />
                               </div>
                                <!-- 주소 검색 폼 끝 -->
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">성별</div>
                            <div class="main_cnt_tit">
                            	<input type="radio" name="gender" id="male" value="M"><label for="male">남</label>
                           	</div>
                           	<div class="main_cnt_tit">
                            	<input type="radio" name="gender" id="female" value="F"><label for="female">여</label>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                            <div class="main_cnt_desc"><input type="text" name="fax" id="fax"></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">입사일</div>
                            <div class="main_cnt_desc">
                            	<input type="text" name="enrollDate1" id="datepicker2" class="datepicker" autocomplete="off">
                            	<input type="date" name="enrollDate" hidden="true" />
                           	</div>
                        </div>
                        <div class="insert_btn">
                        	<button type="reset" class="btn_main">취소</button>
                			<button type="button" class="btn_main" id="submitBtn">확인</button>
                   		</div>                                              
                    </div>
                    </form>
                    <!-- ** 코드 작성부 끝 -->
                    <!-- <div style="float:right;">
                        <button class="btn_pink">퇴사처리</button>
                		<button class="btn_main">수정하기</button>
                    </div> -->              
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	var flag1 = false;
	var flag2 = false;
	var flag3 = false;

    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
       // $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(4).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
        
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            yearRange: 'c-70:c+10',
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
        
        $("#datepicker2").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
        
    
        $("#addrSelTree").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                } 
            }, 
            click: function(event, data){
                var node = data.node;
				
                console.log(event)
                console.log(data.targetType)
                console.log(node)
                
				
				if(data.targetType != 'expander') {
					$(".add_sel_ipt").click()
					$("#deptinput").val(node.title.split("<")[0])
					$("#deptNo").val(node.title.split(" ")[2].split("\"")[1])
				}
            }
        });
        
        $(".fancytree-container").addClass("fancytree-connectors");
    	
	    $(".add_sel_ipt").on("click", function(){
	        $(this).siblings(".add_sel_opt").slideToggle();
	    });
    });
    
 	//주소 검색 시작
    function DaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('postcode_form').value = data.zonecode;
                document.getElementById("address_form").value = addr;
                document.getElementById("detailAddress_form").focus();
            }
        }).open();
    }

    $("#postcode_form, #address_form").on("click", function(){
        $("#address_srch_btn").trigger("click");
    });
    // 주소 검색 끝
    
    $("#imgBtn").click(function() {
    	$("#empImg").click();
    });
    
    function loadImg(value) {
    	if(value.files && value.files[0]) {
    		var reader = new FileReader();
    		reader.onload = function(e) {
    			$("#titleImg").attr("src", e.target.result);
    		}
    		reader.readAsDataURL(value.files[0]);
    	}
    }
    
    $("#submitBtn").click(function() {
    	var p1 = $("#phone1").val();
    	var p2 = $("#phone2").val();
    	var p3 = $("#phone3").val();
    	$("#phone").val(p1 + "-" + p2 + "-" + p3);
    	
    	var a1 = $("#postcode_form").val();
    	var a2 = $("#address_form").val();
    	var a3 = $("#detailAddress_form").val();
    	$("#address").val(a1 + '/' + a2 + '/' + a3);
    	
    	var enroll = $("[name=enrollDate1]").val().split("/");
    	$("[name=enrollDate]").val(enroll[0] + "-" + enroll[1] + "-" + enroll[2]);
    	
    	var birth = $("[name=birth1]").val().split("/");
		$("[name=birth]").val(birth[0] + "-" + birth[1] + "-" + birth[2]);    	
    	
		if(flag1 && flag2 && flag3) {
	    	$("#insertForm").submit();
		} else {
			alert("입력값을 확인해주세요.");
		}
    });
    
    function duplicateNo() {
    	var no = $("#empNo").val();
    	
    	$.ajax({
    		url:"duplicateNo.em",
    		type:"post",
    		data: {
    			no:no
    		},
    		success:function(data) {
    			if(data.result == 'eq') {
    				$("#dupNo").text('사용중인 사번입니다.').css({"color":"tomato", "font-size":"10pt"});
    				flag1 = false;	
    			} else if(data.result == 'neq') {
    				$("#dupNo").text('사용 가능한 사번입니다.').css({"color":"green", "font-size":"10pt"});
    				flag1 = true;
    			}
    		},
    		error:function(status) {
    			console.log(status);
    		}
    	});
    }
    
    function duplicateId() {
    	var id = $("#empId2").val();
    	
    	$.ajax({
    		url:"duplicateId.em",
    		type:"post",
    		data: {
    			id:id
    		},
    		success:function(data) {
    			if(data.result == 'eq') {
    				$("#dupId").text('사용중인 아이디입니다.').css({"color":"tomato", "font-size":"10pt"});
    				flag2 = false;
    			} else if(data.result == 'neq') {
    				$("#dupId").text('사용 가능한 아이디입니다.').css({"color":"green", "font-size":"10pt"});
    				flag2 = true;
    			}
    		},
    		error:function(status) {
    			console.log(status);
    		}
    	});
    }
    
    $("#empPwd2").keyup(function() {
    	var pwd = $("#empPwd").val();
    	
    	var pwd2 = $("#empPwd2").val();
    	
    	if(pwd == pwd2) {
    		$("#dupPwd").text('비밀번호가 일치합니다.').css({"color":"green", "font-size":"10pt"});
    		flag3 = true;
    	} else {
    		$("#dupPwd").text('비밀번호가 일치하지 않습니다.').css({"color":"tomato", "font-size":"10pt"});
    		flag3 = false;
    	}
    });
    
    function loadImg(value, num) {
		if(value.files && value.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function(e) {
				
				switch(num) {
				case 1 : $(".titleImg").attr("src", e.target.result); break;
				}
			}
			reader.readAsDataURL(value.files[0]);
		}
	}
</script>
</body>
</html>