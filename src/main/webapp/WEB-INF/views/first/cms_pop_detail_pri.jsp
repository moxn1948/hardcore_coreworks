<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style>
.modal { 
	min-width: 60%;
}

.priformatSet {
	width: 100%;
}
</style>
    
<h3 class="main_tit">문서보기</h3>

                    <div class="tbl_common tbl_basic pri_status">
                    	<table class="tbl_ctn priformatSet">
                                <colgroup>
                                    <col style="width: 80px;">
                                    <col style="width: *;">
                                    <col style="width: 80px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                    <col style="width: 100px;">
                                </colgroup>
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">문서번호</td>
                                    <td rowspan="2" class="doc_con"></td>
                                    <td rowspan="4" class="doc_con small doc_tit">결재</td>
                                    <td class="doc_con gray doc_tit"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>                             
                                </tr>
                               	<tr class="eas_tit">                                 
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                    <td rowspan="3" class="doc_con"></td>
                                </tr>                             
                                <tr class="eas_tit" >
                                    <td rowspan="2" class="doc_con doc_tit">작성일자</td>
                                    <td rowspan="2" class="doc_con"></td>                         	                                                                   
                                </tr>
                                <tr class="eas_tit"></tr>                                
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성부서</td>
                                    <td rowspan="2" class="doc_con"></td>
                                    <td rowspan="4" class="doc_con small doc_tit">합의</td>
                                    <td class="doc_con gray doc_tit"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>
                                    <td class="doc_con gray"></td>                                 
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>
                                    <td rowspan="3" class="doc_con border_bottom"></td>                                
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="2" class="doc_con doc_tit">작성자</td>
                                    <td rowspan="2" class="doc_con"></td>                              
                                </tr>  
                                <tr class="eas_tit"></tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">수신자</td>
                                    <td colspan="7" class="doc_con"></td>                                 
                                </tr>
                                <tr class="eas_tit">
                                    <td class="doc_con doc_tit">제목</td>
                                    <td colspan="7" class="doc_con"></td>
                                </tr>
                                <tr class="eas_tit">
                                    <td rowspan="10" colspan="8"  height="500" class="eas_content" style="vertical-align: baseline;"></td>                              
                                </tr>
                            </table>
                    </div>
                    
                    <div class="close_area">
                    	<button class="btn_main detail_close"><a href="#" rel="modal:close">확인</a></button>
                    </div>
                    
<script>
	
	$('.priName').on('click', function() {
		const priNo = $(this).parent().children('.priCntTd').eq(0).val();
		const priCnt2 = $(this).children('input').val();

		$('.priformatSet .eas_content').html(priCnt2);
	})
</script>