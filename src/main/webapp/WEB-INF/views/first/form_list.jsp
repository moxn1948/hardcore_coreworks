<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../inc/first_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->

                <div class="main_ctn">
                    <div class="menu_tit firstdiv"><h2>기안 서식관리</h2>
                    
                  <a href="formNew.fr" class="button btn_blue firstadd">추가</a>
                    </div>
                  <a  class="button btn_main firstcomplete">완료</a>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                 <tr class="tbl_main_tit">
                                    <th>폴더</th>
                                    <th>문서번호</th>
                                    <th>서식명</th>
                                    <th>보존연한</th>
                                </tr>
                                 
                               <c:forEach var="f" items="${list}">
                                <tr class="format">
                                   <td class="tit">${ f.pathName }</td>
                                   <td class="tit">${ f.formatNo }</td>
                                   <td class="tit">${ f.formatName }</td>
                                   <td class="tit">${ f.period }년</td>
                                   <td class="tit" hidden>${ f.priFormatNo }</td>
                                </tr>
                               </c:forEach>
            </table>
                    </div>
                    </div>
                          
                    <!-- 기본 테이블 끝 -->
                   <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        
                        <!-- <<, < -->
							<c:url var="blistBack" value="selectFormList.fr">
								<c:param name="currentPage" value="${ pi.currentPage - 1 }"/>
							</c:url>
							<c:url var="blistStart" value="selectFormList.fr">
								<c:param name="currentPage" value="${ pi.startPage }"/>
							</c:url>
							<li class="pager_com pager_arr first"><a href="${ blistStart }">&#x003C;&#x003C;</a></li>
							<li class="pager_com pager_arr prev"><a href="${ blistBack }">&#x003C;</a></li>
                        
                        <!-- 페이징 -->
                        <c:forEach var="p" begin="${ pi.startPage }" end="${ pi.endPage }">
							<c:if test="${ p eq pi.currentPage }">
								<li class="pager_com pager_num on"><a href="javascrpt: void(0);">${ p }</a></li>
							</c:if>
							<c:if test="${ p ne pi.currentPage }">
								<c:url var="blistCheck" value="selectFormList.fr">
									<c:param name="currentPage" value="${ p }"/>
								</c:url>
								<li class="pager_com pager_num"><a href="${ blistCheck }">${ p }</a></li>
							</c:if>
						</c:forEach>
                        
                        <!-- >, >> -->
							<c:url var="blistNext" value="selectFormList.fr">
								<c:param name="currentPage" value="${ pi.currentPage + 1 }"/>
							</c:url>
							<c:url var="blistEnd" value="selectFormList.fr">
								<c:param name="currentPage" value="${ pi.endPage }"/>
							</c:url>
							<li class="pager_com pager_arr next"><a href="${ blistNext }">&#x003E;</a></li>
	                        <li class="pager_com pager_arr end"><a href="${ blistEnd }">&#x003E;&#x003E;</a></li>

                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                   
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
      
      <!-- inner_rt end -->
        </div>
    </main>
</div>


<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
       // $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(6).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(6).find(".sub_menu_list").eq(1).addClass("on");
        
        $('.format').click(function() {
           const formatNo = $(this).children().eq(1).html();
           const priFormatNo = $(this).children().eq(4).html();
           console.log(formatNo);
           console.log(priFormatNo);
           location.href="formatDetail.fr?formatNo="+formatNo+'&priFormatNo='+priFormatNo;
        }).css('cursor', 'pointer');
        
        $('.next').css('float', 'none');
    });
    
    $(".firstcomplete").click(function() {
	  	   if(confirm('초기설정이 완료되었습니다 설정하지 않은 부분은 추후 회사관리 메뉴에서 설정 가능합니다.')) {
	  		   location.href="selectMain.fr"
	  	   }else {
	  		   location.href='#'
	  	   }
	  	   
	  	   
	     })
    
    
    
</script>
</body>
</html>