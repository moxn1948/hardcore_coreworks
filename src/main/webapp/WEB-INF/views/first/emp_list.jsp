<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<link href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/skin-lion/ui.fancytree.min.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.fancytree/2.34.0/jquery.fancytree.min.js"></script>
<jsp:include page="../inc/first_menu.jsp" />
<link href="${ contextPath }/resources/css/tree.css" rel="stylesheet">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
					        <div>
                     <button type="button" class="button btn_main skip">다음 단계</button>
					        </div>        
                    <div class="menu_tit skipArea"><h2>계정관리</h2>
                     <div class="main_cnt_tit firstnotice">※ 현재 페이지부터는 다음 단계 버튼을 통해 건너뛰실수 있습니다.</div>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt" style="margin-bottom: 15px;">
                    	<select name="" id="selectbox" style="float:left">
                            <option value="name" selected>이름</option>
                            <option value="dept">부서</option>
                        </select>
						<!--  -->
						<div class="addr_sel_wrap" style="display:inline-block;">
                            <div class="add_sel">
                                <input type="text" name="" id="deptinput" placeholder="부서를 선택하세요." readonly class="add_sel_ipt">
                                <div class="add_sel_opt">
                                    <div id="addrSelTree" class="tree_menu">
				                  		<ul>
				                  			<li data-cstrender="true">전체보기<input type="hidden" value="0" class="dept"></li>
					                       <c:forEach var="list" items="${ deptList }">
				                       			<li class="folder dept_folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
				                       				<c:if test="${ list.deptOrderList.size() != 0 }">
				                       					<ul>
				                     						<c:forEach var="list2" items="${ list.deptOrderList }">
				                       							<li class="folder dept_folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
								                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
								                       					<ul>
								                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
								                       							<li class="folder dept_folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
								                       							</li>
								                       						</c:forEach>
								                       					</ul>
								                       				</c:if>
				                       							</li>
				                       						</c:forEach>
				                       					</ul>
				                       				</c:if>
				                       			</li>
					                       </c:forEach>
					                 	</ul>
                                    </div>
                                </div>
                            </div> 
                        </div>
						<!--  -->
                        <input type="search" name="" id="namesearch" style="margin-left: 5px;" placeholder="이름으로 검색하세요.">
                        <button class="btn_solid_main" id="namesearchbtn">검색</button>
                        <a href="insertOneEmpForm.fr" class="button btn_main ems_btn">사원 등록</a>
                        <a href="insertAllEmpForm.fr" class="button btn_main ems_btn">사원 일괄 등록</a>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" style="align:center">
                                <colgroup>
                                    <col style="width: *;">
                                    <col style="width: 16%;">
                                </colgroup>
                                <tr class="tbl_main_tit">
                                    <th>사번</th>
                                    <th>이름</th>
                                    <th>이메일</th>
                                    <th>입사일</th>
                                    <th>휴대전화</th>
                                    <th>직급/직책</th>                        
                                </tr>
                                <c:forEach var="emp" items="${ eList }" varStatus="st">
	                                <tr>
	                                    <td>${ emp.empNo } <input type="hidden" id="eNo" value="${ emp.empDivNo }" /></td>
	                                    <td><a href="#">${ emp.empName }</a></td>
	                                    <td>${ emp.empId }@${ domain.domain }</td>
	                                    <td>${ emp.enrollDate }</td>
	                                    <td>${ emp.phone }</td>
	                                    <td>${ emp.jobName } / ${ emp.posName }</td>
	                                </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        	<c:if test="${ pi.currentPage eq 1 }">
                        		<li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage ne 1 }">
                        		<c:url var="blistFirst" value="selectEmpList.em">
                        			<c:param name="currentPage" value="1"/>
                        		</c:url>
                        		<li class="pager_com pager_arr first"><a href="${ blistFirst }">&#x003C;&#x003C;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage <= 1 }">
								<li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage > 1 }">
								<c:url var="blistBack" value="selectEmpList.em">
									<c:param name="currentPage" value="${ pi.currentPage - 1 }"/>
								</c:url>
								<li class="pager_com pager_arr prev"><a href="${ blistBack }">&#x003C;</a></li>
							</c:if>
							<c:forEach var="p" begin="${ pi.startPage }" end="${ pi.endPage }">
								<c:if test="${ p eq pi.currentPage }">
									<li class="pager_com pager_num on"><a href="javascrpt: void(0);">${ p }</a></li>
								</c:if>
								<c:if test="${ p ne pi.currentPage }">
									<c:url var="blistCheck" value="selectEmpList.em">
										<c:param name="currentPage" value="${ p }"/>
									</c:url>
									<li class="pager_com pager_num"><a href="${ blistCheck }">${ p }</a></li>
								</c:if>
							</c:forEach>
                       		<c:if test="${ pi.currentPage >= pi.maxPage }">
								<li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage < pi.maxPage }">
								<c:url var="blistEnd" value="selectEmpList.em">
									<c:param name="currentPage" value="${ pi.currentPage + 1 }"/>
								</c:url>
								 <li class="pager_com pager_arr next"><a href="${ blistEnd }">&#x003E;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage eq pi.maxPage }">
                        		<li class="pager_com pager_arr last"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage ne pi.maxPage }">
                        		<c:url var="blistLast" value="selectEmpList.em">
                        			<c:param name="currentPage" value="${ pi.maxPage }"/>
                        		</c:url>
                        		<li class="pager_com pager_arr last"><a href="${ blistLast }">&#x003E;&#x003E;</a></li>
                        	</c:if>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
     //   $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $(".firstMenu").eq(4).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
        
        //$(".addr_sel_wrap").hide();
        //$("#addrSelTree").hide();
        
        $("#addrSelTree").fancytree({
            imagePath: "skin-custom/",
            renderNode: function(event, data) {
                var node = data.node;
                if(node.data.cstrender){
                    var $span = $(node.span);
                    $span.find("> span.fancytree-title").css({
                        backgroundImage: "none"
                    });
                    $span.find("> span.fancytree-icon").css({
                        backgroundImage: "none",
                        display: "none"
                    });
                } 
            },
            click: function(event, data){
                var node = data.node;
				
				if(data.targetType != 'expander') {
					$(".add_sel_ipt").click();
					$("#deptinput").val(node.title.split("<")[0]);
					var deptNo = node.title.split(" ")[2].split("\"")[1];
					
					if(deptNo == 0) {
						location.href='selectEmpList.em';
					}
					
					$.ajax({
						url:"searchDeptEmpList.em",
						data: {
							deptNo:deptNo
						},
						success: function(data) {
							$(".tbl_main_tit").nextAll().remove();
							$(".pager_wrap").remove();
							
							for(var i = data.list.length - 1; i >= 0; i--) {
								$(".tbl_main_tit").after('<tr><td>' + data.list[i].EMP_NO + '<input type="hidden" id="eNo" value=' + data.list[i].EMP_DIV_NO + '/></td>' + 
														 '<td><a href="">' + data.list[i].EMP_NAME + '</a></td>' + 
														 '<td>' + data.list[i].EMP_ID + '@${domain}' + '</td>' + 
														 '<td>' + (1900 + data.list[i].ENROLL_DATE.year) + '-' + data.list[i].ENROLL_DATE.month + 1 + '-' + (data.list[i].ENROLL_DATE.date > 9 ? data.list[i].ENROLL_DATE.date : '0' + data.list[i].ENROLL_DATE.date) +  '</td>' + 
														 '<td>' + ((data.list[i].PHONE == undefined) ? '없음' : data.list[i].PHONE) + '</td>' + 
														 '<td>' + data.list[i].JOB_NAME + '/' + data.list[i].POS_NAME + '</td></tr>');
							}
						},
						error: function(status) {
							console.log(status)
						}
					});
				}
            }
        });
        
	    $(".fancytree-container").addClass("fancytree-connectors");
	
	    $(".add_sel_ipt").on("click", function(){
	        $(this).siblings(".add_sel_opt").slideToggle();
	    });
	    
	    $(".addr_sel_wrap").hide();
	    
    });
    
    
    
   
   $("#selectbox").change(function() {
    	if($(this).val() == 'name') {
    		$("#namesearch").show();
    		$("#namesearchbtn").show();
    		$(".addr_sel_wrap").hide();
    	} else if ($(this).val() == 'dept') {
    		$(".addr_sel_wrap").show();
    		$("#namesearch").hide();
    		$("#namesearchbtn").hide();
    	}
    });
   
   $(document).on("keydown", "#namesearch", function(e) {
	   if(e.keyCode == 13) {
		  $("#namesearchbtn").click();
	   }
   }); 
   
   $("#namesearchbtn").click(function() {
	   var searchValue = $("#namesearch").val();
	   
	   $.ajax({
		   url:"searchNameEmpList.em",
		   data: {
			   searchValue : searchValue
		   },
		   success: function(data) {
			   $(".tbl_main_tit").nextAll().remove();
				$(".pager_wrap").remove();
				
				for(var i = data.list.length - 1; i >= 0; i--) {
					$(".tbl_main_tit").after('<tr><td>' + data.list[i].EMP_NO + '<input type="hidden" id="eNo" value=' + data.list[i].EMP_DIV_NO + '/></td>' + 
											 '<td><a href="">' + data.list[i].EMP_NAME + '</a></td>' + 
											 '<td>' + data.list[i].EMP_ID + '@${domain}' + '</td>' + 
											 '<td>' + (1900 + data.list[i].ENROLL_DATE.year) + '-' + data.list[i].ENROLL_DATE.month + 1 + '-' + (data.list[i].ENROLL_DATE.date > 9 ? data.list[i].ENROLL_DATE.date : '0' + data.list[i].ENROLL_DATE.date) +  '</td>' + 
											 '<td>' + ((data.list[i].PHONE == undefined) ? '없음' : data.list[i].PHONE) + '</td>' + 
											 '<td>' + data.list[i].JOB_NAME + '/' + data.list[i].POS_NAME + '</td></tr>');
				}
		   }
	   })
   });
   
   $(".skip").click(function() {
	   if(confirm('다음 단계로 진행 시 이전 단계로 돌아갈 수 없습니다.\n진행하시겠습니까? \n (※ 추후 관리자 메뉴에서 수정이 가능합니다.)')) {
		   location.href="selectOperAuth.fr"
	   }else {
		   location.href='#'
	   }
	   
	   
   })
</script>

</body>
</html>