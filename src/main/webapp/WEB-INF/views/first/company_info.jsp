<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="../inc/first_menu.jsp" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js'></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">	
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>회사정보 입력	</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                    <div class="main_cnt_list clearfix">
					<form action="insertCompany.fr" id="insertForm" method="post" enctype="multipart/form-data">
                     <button type="button" class="button btn_main com_det skip" id="submitBtn">다음 단계</button>
						
					<div id="logoImgArea">
					<div style="width: 120px; height: 120px;overflow: hidden;">
						<img class="titleImg" style="width: 100%;height: auto;">
					</div>
						<div class="upload-btn_wrap">
                                <input type="file" name="attachments" onchange="loadImg(this, 1)" style="display:none">
                                <button type="button" class="btn_main btn_choise firstphoto" onclick="document.all.attachments.click()">선택
                                </button>
                              </div>
					</div>
                    
                     
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">회사명</div>
                            <div class="main_cnt_desc"><input type="text" name="companyName" id="companyName" autocomplete=off></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사업자번호</div>
                            <div class="main_cnt_desc"><input type="text" name="license" id="license" autocomplete=off></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">전화번호</div>
                            <div class="main_cnt_desc"><input type="text" name="phone1" id="phone1" maxlength="3" autocomplete=off> - <input type="text" name="phone2" id="phone2" maxlength="4" autocomplete=off> - <input type="text" name="phone3" id="phone3" maxlength="4" autocomplete=off></div>
                            <div class="" hidden><input type="text" name="phone" id="phone" hidden></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                            <div class="main_cnt_desc"><input type="text" name="fax" id="fax" autocomplete=off></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">대표자명</div>
                            <div class="main_cnt_desc"><input type="text" name="name" id="name" autocomplete=off></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                            <div class="main_cnt_desc">
                            <!-- 주소 검색 폼 시작 -->
                               <div class="address_form_wrap">
                                  <div class=""><input type="text" class="firstpost" name="postcode_form" id="postcode_form" placeholder="우편번호" readonly><a href="#" class="button btn_pink" id="address_srch_btn" onclick="DaumPostcode();">검색</a></div>
                                  <div class=""><input type="text" class="firstaddress" name="address_form" id="address_form" placeholder="주소" readonly></div>
                                  <div class=""><input type="text"  class="firstdetail"name="detailAddress_form" id="detailAddress_form" placeholder="상세주소"></div>
                                  <div class="" hidden><input type="text" name="address" id="address" hidden></div>
                               </div>
                                <!-- 주소 검색 폼 끝 -->
                            
                        </div>
                        </div>
                        
                        
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">설립일</div>
                            <div class="main_cnt_desc"><input type="text" name="estdate" id="datepicker" autocomplete=off></div>
                            <div class="main_cnt_desc" hidden><input type="text" name="estDate" id="estDate"></div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">도메인</div>
                            <div class="main_cnt_desc"><input type="text" name="domain" id="domain" autocomplete=off></div>
                           
                        </div>
                        
                        
                         <div class="main_cnt_list clearfix signYn">
                            <div class="main_cnt_tit signYn">서명사용여부</div>
                             <div class="box_radio">
						<input type="radio" name="signYn" id="notUse" value="N" checked>
						<label for="notUse">사용안함</label>
						<input type="radio" name="signYn" id="use" value="Y">
						<label for="use">사용</label> 
						   </div>
                        </div>
                        
                        <div class="main_cnt_list clearfix sign" hidden>
                            <div class="main_cnt_tit sing_name">사용할 서명</div>
                            <div class="upload-btn_wrap">

								<div class="companybtn">
                                <input type="file" name="attachments2" onchange="loadImg2(this, 1)" style="display:none">
                                <button type="button" class="btn_main btn_choise secondphoto" onclick="document.all.attachments2.click()">선택
                                </button>
                           	 <img class="titleImg2" width="450px" height="450px">
                              </div>
                              
                        </div>
                        </div>
                        
                        <div class="main_cnt_list clearfix self" hidden>
                              <div class="fr-view"></div>
                              <input type="text" class="signCode" id="signCode" name="signCode" hidden>
                              
                        </div>
                        
                        
                        
					</form>	
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 주소검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
    	var editor = new FroalaEditor('.fr-view')
	        /* // 주 메뉴 분홍색 하이라이트 처리
	        $("#nav .nav_list").eq(7).addClass("on"); */

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(4).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(4).find(".sub_menu_list").eq(1).addClass("on");
        
        $("#use").click(function() {
        	  	$(".main_cnt_list.clearfix.sign").show();
        	  	$(".main_cnt_list.clearfix.self").hide();
        });
        
        $("#notUse").click(function() {
        	$(".main_cnt_list.clearfix.sign").hide();
        	$(".main_cnt_list.clearfix.self").show();
        });
        
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
        
    });
    
    
    function loadImg(value, num) {
		if(value.files && value.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function(e) {
				
				switch(num) {
				case 1 : $(".titleImg").attr("src", e.target.result); break;
				}
			}
			reader.readAsDataURL(value.files[0]);
		}
	}
    
    function loadImg2(value, num) {
		if(value.files && value.files[0]) {
			var reader = new FileReader();
			
			reader.onload = function(e) {
				
				switch(num) {
				case 1 : $(".titleImg2").attr("src", e.target.result); break;
				}
			}
			reader.readAsDataURL(value.files[0]);
			
		}
	}
    
 // 주소 검색 시작
    function DaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('postcode_form').value = data.zonecode;
                document.getElementById("address_form").value = addr;
                document.getElementById("detailAddress_form").focus();
            }
        }).open();
    }

    $("#postcode_form, #address_form").on("click", function(){
        $("#address_srch_btn").trigger("click");
    });
    // 주소 검색 끝
    
     $("#submitBtn").click(function() {
    	 
       var p1 = $("#phone1").val();
       var p2 = $("#phone2").val();
       var p3 = $("#phone3").val();
       $("#phone").val(p1 + "-" + p2 + "-" + p3);
       
       
       var a1 = $("#postcode_form").val();
       var a2 = $("#address_form").val();
       var a3 = $("#detailAddress_form").val();
       $("#address").val(a1 + '/' + a2 + '/' + a3);
       
       var birth = $("[name=estdate]").val().split("/");
      $("[name=estDate]").val(birth[0] + "-" + birth[1] + "-" + birth[2]);     
      
      $("#signCode").val($('.fr-element.fr-view').html())
    
       
       $("#insertForm").submit();
    });
    
     $(".skip").click(function() {
  	   if(confirm('다음 단계로 진행 시 이전 단계로 돌아갈 수 없습니다.\n진행하시겠습니까? \n (※ 추후 관리자 메뉴에서 수정이 가능합니다.)')) {
  		   location.href="selectDeptStruct.fr"
  	   }else {
  		   location.href='#'
  	   }
  	   
  	   
     })

    
    
</script>
</body>
</html>