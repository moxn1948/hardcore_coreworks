<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    
<jsp:include page="../inc/first_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
            <form action="updateJob.fr" id="updateForm" method="post">
                <div class="main_ctn">
               
               
                    <div class="menu_tit deptnext"><h2>직급관리</h2>
               		 <div>
                     <button type="button" class="button btn_main skipdept">다음 단계</button>
					        </div>
                    
                    </div>
                    
                   
					              
                    <!-- 테이블 위 컨텐츠 시작 -->
				<button class="button btn_blue save" id="submitBtn">저장</button>
				<a href="#" class="button btn_blue add" onclick="topAdd()">추가</a>
                    <div class="main_cnt">
      
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn jobtb">
                                <tr class="tbl_main_tit">
                                    <th>직급번호</th>
                                    <th>직급명</th>
                                    <th>순서변경</th>
                                    <th>삭제</th>
                                </tr>
                           <c:forEach var="j" items="${ list }">
					<tr>
						<td><input class='jobOrdertd' name='jobOrder'value=<c:out value="${ j.jobOrder}"/> readonly></td>
						<td><input class='jobNametd' name='jobName' value=<c:out value="${ j.jobName }"/>></td>
						<td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' type='button' onclick='moveDown(this)'>▼</button></a></td>
						<td><button type='button' class='btn_pink' onclick='del(this)'>삭제</button></td>					
						<td hidden><input type="text" name="jobNm" class="jobNm"></td>
						<td hidden><input type="text" name="jobOd" class="jobOd"></td>
					</tr>
				</c:forEach>
                                
                                	
				</table>
                    </div>
                     
                          </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                   
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
			</form>
		<!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>

var jOrder = 0;

    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        //$("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(4).addClass("on").addClass("open");
       // $("#menu_area .menu_list").eq(4).find(".sub_menu_list").eq(0).addClass("on");
        
        //jOrder = ($(".jobtb").find("tr:last-child()").children().eq(3).children().val() * 1)+1;
       

    });
    function topAdd() {
    	var str = "<tr><td hidden></td><td hidden><input type='text' hidden></td><td hidden><input readonly></td><td><input class='jobOrdertd' name='jobOrder' type='text' readonly value='" + (jOrder +1) + "' style='text-align:center; '></td><td><input class='jobNametd' type='text' name=" + ("'jobName") +"'" + " style='text-align:center;'></td><td><a href='#'><button type='button'class='btn_main_up' onclick='moveUp(this)'>▲</button><button class='btn_main_down' type='button' onclick='moveDown(this)'>▼</button></a></td><td><button type='button' class='btn_white' onclick='del(this)'>삭제</button></td></tr>";
    	jOrder++
    	$(str).appendTo($(".tbl_ctn"));
    }

	var joNo = "";
    function del(d) {
    	str = d.closest('tr')
    	var $tr = $(d).closest('tr');   	
    	var jNo = $tr.children().eq(0).text();
    	
    	
    	jOrder--;
    	if(joNo == "") {
    		joNo = jNo
    	}else {
    		
    	joNo = joNo + ", " + jNo ;
    	}
    	
    	$(".jobNo").val(joNo)
    	
    	str.remove();
    	$("#submitBtn").click(function(){
    		
    	}) 
    		
    	}
  
    
    
    $('.table1 button:even').bind('click', function(){ moveUp(this) });
    $('.table1 button:odd').bind('click', function(){ moveDown(this) });
    
    function moveUp(el){
    	
		var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
    	var idx = $tr.index();
		if(idx > 1) {
		var s = $tr.prev().children().eq(4).children().val();//전에꺼
		var spo = $tr.prev().children().eq(4).children();//전에꺼 위치
		var prePo =$tr.children().eq(4).children();//지금꺼
		var prePoVal = $tr.children().eq(4).children().val();//지금값
		
		/* $tr.prev().before($tr); */ // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
			prePo.val(spo.val());
			spo.val(prePoVal);
		
		}
	
	
	}
    
    function moveDown(el){
		var $tr = $(el).closest('tr'); // 클릭한 버튼이 속한 tr 요소
		var idx = $tr.index();
		var idx2= $(".jobtb").find("tr:last-child()").index();
		

		
		/* $tr.next().after($tr); */ // 현재 tr 의 이전 tr 앞에 선택한 tr 넣기
		if(idx >= 1 && idx < idx2) {

		var s = $tr.next().children().eq(4).children().val();//전에꺼
		var spo = $tr.next().children().eq(4).children();//전에꺼 위치
		var prePo =$tr.children().eq(4).children();//지금꺼
		var prePoVal = $tr.children().eq(4).children().val();//지금값

		prePo.val(spo.val());
		spo.val(prePoVal);
		}
					
	}
    
    $("#submitBtn").click(function() {
    	var name = $("[name=jobName]");
    	var order = $("[name=jobOrder]");
    	var jobNm = ""
    	var jobOd = ""
    	console.log(name)
    	console.log(order)
    	name.each(function() {
    	var Name2 = $(this).val()
    	if(jobNm == "") {
    		jobNm = Name2
	
    	}else {
    		jobNm = jobNm + ", " + Name2 		
    	}
    		$(".jobNm").val(jobNm)	
    		console.log($(".jobNm").val(jobNm))
    	});
    	
    	order.each(function() {
    		var Order2 = $(this).val()
    		if(jobOd == "") {
    			jobOd = Order2
    		}else {
    			jobOd = jobOd + ", " + Order2
    		}
    		$(".jobOd").val(jobOd)
    		console.log($(".jobOd").val(jobOd))
    	})	    
    	
     alert('입력하신 직급 정보가 저장 되었습니다.')
    });
    
/*     function deleteBtn(el) {
    	str = d.closest('tr')
    	var $tr = $(d).closest('tr');   	
    	var jNo = $tr.children().eq(0).text();
    	
    	
    	
    	
    	
    	console.log(jNo)
    	
    	
    	str.remove();
    	
    } */
    $(".skipdept").click(function() {
	  	   if(confirm('다음 단계로 진행 시 이전 단계로 돌아갈 수 없습니다.\n진행하시겠습니까? \n (※ 추후 관리자 메뉴에서 수정이 가능합니다.)')) {
	  		   location.href="selectListPos.fr"
	  	   }else {
	  		   location.href='#'
	  	   }
	  	   
	  	   
	     })
    
    
    
</script>
</body>
</html>