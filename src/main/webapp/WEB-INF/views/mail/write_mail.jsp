<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/mail_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wk.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/ui/trumbowyg.min.css" rel='stylesheet' type='text/css' />
<link href="${ contextPath }/resources/css/trumbowyg_table.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/trumbowyg.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/plugins/table/trumbowyg.table.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.20.0/langs/ko.min.js"></script>
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit">
                    	<div id="mail_h2">
                    		<h2>메일 쓰기</h2>
                    	</div>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                    	<div class="mail_btn">
                        	<button class="btn_blue" id="send_mail">보내기</button>
                        	<button class="btn_main">미리보기</button>
                        </div>
                      
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" id="mail_tbl">
                                <colgroup>
                                	<col style="width: 10%;">
                                    <col style="width: *;">
                                </colgroup>
                                <tr>
                                	<th>
                                		<input type="checkbox" id="to_me" name="to_me" /><label for="to_me">나에게</label>
                                	</th>
                                  	<td>
                                  		<c:if test="${ m.type eq 'IN' }">
                                  			<input type="text" id="mail_receiver" name="mail_receiver" value="${ m.empName } / ${m.empId}@${domain}" />
                                  		</c:if>
                                  		<c:if test="${ m.type eq 'OUT' }">
                                  			<input type="text" id="mail_receiver" name="mail_receiver" value="${ m.outSend }" />
                                  		</c:if>
                                  		<c:if test="${ m eq null }">
                                  			<c:if test="${ per eq null }">
	                                  			<input type="text" id="mail_receiver" name="mail_receiver" />
                                  			</c:if>
                                  			<c:if test="${ per ne null }">
	                                  			<input type="text" id="mail_receiver" name="mail_receiver" value="${ per } "/>
                                  			</c:if>
                                  		</c:if>
                                  		<a href="#addrPop1" rel="modal:open" class="button btn_main cnt_name rec open_modal">주소록</a>
                               		</td>
                                </tr>
                                <tr>
                                	<th>
                                		참조
                                		<span id="add_hidden_ref"><i class="far fa-plus-square"></i></span>
                                	</th>
                                  	<td>
                                  		<input type="text" id="mail_ref" name="mail_ref" />
                                  		<a href="#addrPop1" rel="modal:open" class="button btn_main cnt_name ref open_modal">주소록</a>
                               		</td>
                                </tr>
                                <tr id="hid_ref_tr">
                                  	<td>숨은 참조</td>
                                  	<td>
                                  		<input type="text" id="mail_hidden_ref" name="mail_hidden_ref" />
                                  		<a href="#addrPop1" rel="modal:open" class="button btn_main cnt_name hid open_modal">주소록</a>
                                  	</td>
                                </tr>
                                <tr>
                                	<th>
                                		제목
                                		<input type="checkbox" name="imp" id="imp" /><label for="imp">중요</label>
                                	</th>
                                  	<td>
                                  	<c:if test="${ m.mailTitle ne null }">
                                  		<input type="text" id="mail_tit" name="mail_tit" value="RE : ${m.mailTitle }" />
                                  	</c:if>
                                  	<c:if test="${ m.mailTitle eq null }">
                                  		<input type="text" id="mail_tit" name="mail_tit" />
                                  	</c:if>
                               		</td>
                                </tr>
                                <tr>
                                	<th>
                                		파일첨부
                                		<span id="filebox_btn"><i class="far fa-plus-square"></i></span>
                                	</th>
                                  	<td colspan="2">
                                  		<form action="sendmail.ma" id="mail" method="post" enctype="multipart/form-data">
	                                  		<input type="text" id="sender" name="inSend" value="${ sessionScope.loginUser.empDivNo }" hidden="true" />
	                                  		<input type="text" id="sendmail" name="sendemail" value="${ sessionScope.loginUser.empId }@${ domain }" hidden="true"/>
	                                  		<input type="text" id="TO" name="receiver" hidden="true" />
	                                  		<input type="text" id="CC" name="referer" hidden="true" />
	                                  		<input type="text" id="BCC" name="hiddenRef" hidden="true" />
	                                  		<input type="text" id="TITLE" name="mailTitle" hidden="true" />
	                                  		<input type="text" id="CONTENT" name="mailCnt" hidden="true" />
	                                  		<input type="text" id="impYn" name="importantYn" value="N" hidden="true" />
	                                  		<input type="text" id="proYn" name="protectYn" value="N" hidden="true" />
	                                  		<input type="text" id="resvYn" name="resvYn" value="N" hidden="true" />
	                                  		<input type="text" id="reservation" name="reservation" hidden="true" />
	                                  		<input type="text" id="mailPwd" name="mailPwd" hidden="true" />
                                  		<div class="mail_file">
	                                  		<div class="mail_file_btn">
		                                  		<button type="button" class="btn_main" id="select_file_btn">파일 선택</button>
		                               			<button type="button" class="btn_pink" id="deleteAllfile">전체 삭제</button>
	                               			</div>
	                               			<div class="mail_file_size">
	                               				<!-- <p class="file_size">
	                               					<span>
														<span>0Byte</span> 색 넣을거면 클래스 넣고 바꿀 것
														/
														<strong>50MB</strong>
													</span>
	                               				</p> -->
	                               			</div>
                               			</div>
                               			<div class="file_zone"> <!-- 파일 첨부 영역 -->
                               				<div class="area_txt">
                               					<span></span>
                               				</div>
	                               			<div class="file_list_zone">
	                               				<ul class="file_attach_list" id="file_attach_list"> <!-- 파일 첨부시 여기다 목록 보여주기 -->
	                               					<!-- <li>대용량 파일 첨부 ? 가능 ?
	                               						<input type="checkbox" /> display:none;
	                               						<span class="file">
	                               							<span class="file_del_btn"></span> onclick으로 클릭시 삭제
	                               							<span class="file_name">bapbird.jpg</span> 파일명
	                               						</span>
	                               						<span class="fileinfo">
	                               							<span class="size"></span>
	                               						</span>
	                               					</li> -->
	                               				</ul>
	                               			</div>
                               			</div>
                           				</form>
                               		</td>
                                </tr>
                            	<tr>
                            		<td colspan="2">
                            			<div id="editer"></div>
                            		</td>
                            	</tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>


<div id="addrPop1" class="modal chatmodal">
	<jsp:include page="../pop/mail_receiver_select_pop.jsp" />
</div>

<div id="resvPop" class="modal chatmodal">
	<jsp:include page="../pop/mail_reservation_timeselect_pop.jsp"/>
</div>

<div id="pwdPop" class="modal chatmodal">
	<jsp:include page="../pop/mail_password_pop.jsp"/>
</div>


<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	var type;
	$(document).on("click", "a", function() {
		 type = $(this);
	})

	
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
        
        $("#hid_ref_tr").hide();
        
        $(".file_zone").hide();
        
        if("${m}" != "") {
    	    $("#editer").html("<br>-----Original Message-----<br>${ m.mailCnt }")
	        $("#editer").focus();
        }
    });

	$('#editer').trumbowyg({
		btns: [['viewHTML'],
		['undo', 'redo'], // Only supported in Blink browsers
		['formatting'],
		['strong', 'em', 'del'],
		['superscript', 'subscript'],
		['link'],
		['insertImage'],
		['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
		['unorderedList', 'orderedList'],
		['horizontalRule'],
		['removeformat'],
		['table']],
		lang: 'ko',
		plugins: {
		    table: {
			rows: 10,
			columns: 10,
			styler: 'tbl_form',
			}
		},
	});
    
    $(document).on("change", "#to_me", function(){
    	if($(this).is(":checked")) {
    		$("#mail_receiver").attr("disabled", true);
    		$("#mail_ref").attr("disabled", true);
    		$("#mail_hidden_ref").attr("disabled", true);
    		$("#mail_receiver").val($("#sendmail").val())
    		$("#TO").val(($("#mail_receiver").val()));
    	} else {
    		$("#mail_receiver").attr("disabled", false);
    		$("#mail_ref").attr("disabled", false);
    		$("#mail_hidden_ref").attr("disabled", false);
    		$("#mail_receiver").val("");
    		$("#TO").val("");
    	}
    });
    
    $("#add_hidden_ref").click(function() {
    	if($("#hid_ref_tr").css("display") == 'none') {
    		$("#hid_ref_tr").show();
    	} else if ($("#hid_ref_tr").css("display") == 'table-row') {
    		$("#hid_ref_tr").hide();
    	}
    })
    
    $("#filebox_btn").click(function() {
		if($(".file_zone").css("display") == 'none') {
			$(".file_zone").show();
		} else if($(".file_zone").css("display") == 'block') {
			$(".file_zone").hide();			
		}
    });
    
    var idx = 0;
    $("#select_file_btn").click(function() {
    	if($("#"+idx).val() != "") {
	    	$(this).after("<input type='file' id=" + idx + " name='file' class='selected_file' hidden />");
    	}
    	
    	$("#"+idx).click();
    });
    
   	var files = [];
    $(document).on("change", ".selected_file", function(e) {
    	var size = e.target.files[0].size;
    	var i = 0;
    	var CalcuSize = "";
    	
    	while(size >= 1024 && i < 5) {
    		size = size / 1024;
    		i++;
    	}
    	
    	switch (i) {
        case 0:
            CalcuSize = size.toFixed(1) + "Byte";
            break;
        case 1:
            CalcuSize = size.toFixed(1) + "KB";
            break;
        case 2:
            CalcuSize = size.toFixed(1) + "MB";
            break;
        case 3:
            CalcuSize = size.toFixed(1) + "GB";
            break;
        case 4:
            CalcuSize = size.toFixed(1) + "TB";
            break;
        default:
            CalcuSize="ZZ"; //용량표시 불가
    	}
    	
    	var $file = "<li> <span class='file_del_btn'><input type='hidden' value= " + idx + "><i class='far fa-trash-alt'></i></span> <span class='file'><span class='file_name'>" + 
    				e.target.files[0].name + "</span></span><span class='fileinfo'><span class='size'>" + CalcuSize +"</span></span></li>";
    	
    	$("#file_attach_list").append($file);
    	
    	idx++;
    	$(".file_zone").show();
    });
    
    $(document).on("click", ".file_del_btn", function() {
		var index = $(this).children('input').val();
		$(this).parents('li').remove();
		$("#"+index).remove();
		idx--;
    });
    
	$("#deleteAllfile").click(function() {
		$(".selected_file").each(function() {
			$(this).val("");
		});
		
		$("#file_attach_list").empty();
		files = [];
		idx = 0;
	});
    
    $(document).on("change", "#imp", function() {
    	if($(this).is(":checked")) {
    		$("#impYn").val("Y");
    	} else {
    		$("#impYn").val("N");
    	}
    });
    
	$("#selectResv").click(function() {
		var reservation = $("#datepicker1").val() + " " + $("#timeselect").val() + "-" + $("#minuteselect").val();
		
		$("#reservation").val(reservation);
		$("#resvYn").val('Y');
	});
	
	$("#closeResv").click(function() {
		$("#datepicker1").val("");
		$("#timeselect").find('option').eq(0).prop("selected", true);
		$("#minuteselect").find('option').eq(0).prop("selected", true);
		$("#resvYn").val('N');
	});
	
	$("#pwdClose").click(function() {
		$("#mailPwd").val($("#encPwd").val());
		$("#proYn").val('Y');
	});
	
	$("#pwdReset").click(function() {
		$("#mailPwd").val("");
		$("#encPwd").val("");
		$("proYn").val("N");
	});
   
	
    $("#send_mail").click(function() {
    	if(!($("#to_me").is(":checked"))) { 
	    	$("#TITLE").val($("#mail_tit").val())
	    	$("#CONTENT").val($("#editer").html());
	    	var receiver = $("#mail_receiver").val().split(",");
	    	
	    	var to = "";
	    	
	   		var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
	   		
	   		for(i = 0; i < receiver.length; i++) {
	   			var temp = $.trim(receiver[i])
	   			if(temp.indexOf('<') > 0) {
		   			temp = temp.substring(temp.indexOf("<") + 1 , temp.lastIndexOf(">"));
	   			} 
				
	   			if(regExp.test(temp) && to == "") {
	   				to += temp;
	   			} else if(regExp.test(temp) && to != "") {
	   				to += ", " + temp;
	   			}
	   		}
	   		
	   		$("#TO").val(to);
	   		
	   		var referer = $("#mail_ref").val().split(/\/|,/);
	   		
	   		var cc = "";
	   		
	   		for(i = 0; i < referer.length; i++) {
	   			var temp = $.trim(referer[i]);
	   			temp = temp.substring(temp.indexOf("<") + 1 , temp.lastIndexOf(">"));
	   			
	   			if(regExp.test(temp) && cc == "") {
	   				cc += temp;
	   			} else if(regExp.test(temp) && cc != "") {
	   				cc += ", " + temp;
	   			}
	   		}
	   		
	   		$("#CC").val(cc);
	    	
	   		var hidref = $("#mail_hidden_ref").val().split(/\/|,/);
	   		
	   		var bcc = "";
	   		
	   		for(i = 0; i < hidref.length; i++) {
	   			var temp = $.trim(hidref[i]);
	   			temp = temp.substring(temp.indexOf("<") + 1 , temp.lastIndexOf(">"));
	
	   			if(regExp.test(temp) && bcc == "") {
	   				bcc += temp;
	   			} else if(regExp.test(temp) && bcc != "") {
	   				bcc += ", " + temp;
	   			}
	   		}
	   		
	   		$("#BCC").val(bcc);
	    	
    	}
    	console.log($("#TO").val())
    	$("#mail").submit();
    });
</script>

</body>
</html>
