<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/mail_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wk.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit">
                    	<div id="mail_h2">
                    		<h2>전체 메일</h2>
                    	</div>
                    	<div class="mail_count">
	                    	안읽은 메일 <strong>${ TotalListCount - TotalReadMailCount }</strong>
	                    	<span>/</span>
	                    	전체 메일 <strong>${ TotalListCount }</strong>
                    	</div>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                    	<div class="mail_btn">
	                        <button class="btn_blue" id="replyBtn">답장</button>
	                        <button class="btn_pink" id="deleteBtn">삭제</button>
                        	<button class="btn_main" id="unReadBtn">안읽음</button>
                        </div>
                      
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" id="mail_tbl">
                                <colgroup>
                                	<col style="width: 1%;">
                                    <col style="width: *;">
                                    <col style="width: 15%;">
                                </colgroup>
                                <tr>
                                	<td>
                                		<input type="hidden" id="mailNo" value="<c:out value='${ m.mailNo }'/>" />
                            			<c:if test="${ m.keepYn == 'Y' }">
                            				<img src="${contextPath}/resources/images/star.svg" class="mail_star">
                            			</c:if>
                            			<c:if test="${ m.keepYn == 'N' }">
                            				<img src="${contextPath}/resources/images/empty_star.svg" class="mail_star">
                            			</c:if>
                            		</td>
                                  	<td id="mailDetailTitle"><strong><c:out value="${ m.mailTitle }"/></strong></td>
                                  	<td>
                            			<c:if test="${ m.sendTime.indexOf('오후') != -1 }">
                            				<c:out value="${ m.sendTime.replace('오후', 'PM') }"/>
                            			</c:if>
                            			<c:if test="${ m.sendTime.indexOf('오전') != -1 }">
                            				<c:out value="${ m.sendTime.replace('오전', 'AM') }"/>
                            			</c:if>
									</td>
                                </tr>
                                <tr>
                                	<td colspan="3">
                                		<c:if test="${ m.type eq 'IN' }">
                                		<div class="mailDetailSender">
  	                             			<strong>보낸 사람</strong><span id="sender"> <c:out value="${ m.empName } ${ m.jobName }/${ m.posName } <${ m.empId }@${ domain }>"/></span>
                                		</div>
                                		</c:if>
                                	</td>
                                </tr>
                                <tr>
                                	<td colspan="3">
                                		<div class="mailDetailReceiver">
                                		<strong>받는 사람</strong> 
                                		<c:forEach items="${ empList }" var="emp" varStatus="st">
                                			<c:if test="${ emp.type eq 'IN' }">
                                				<c:out value="${ emp.empName } ${ emp.jobName }/${ emp.posName } <${ emp.empId }@${ domain }>"/>
                                			</c:if>
                                			<c:if test="${ emp.type eq 'OUT' }">
                                				<c:out value="${ emp.outRec }"/>
                                			</c:if>
                                			<c:if test="${ !st.last }">
                                				<c:out value=", "/>
                                			</c:if>
                                		</c:forEach>
                                		</div>
                                	</td>
                                </tr>
                                <tr>
                                	<td colspan="3">
                                	<div class="mailDetail">
                                		${ m.mailCnt }
                               		</div>
                                	</td>
                                 <c:if test="${ fileList != null }">
                                <tr>
									<td colspan="3">
										<div>
											<strong>첨부 파일</strong> <c:out value="${ fileList.size() } 개" />
										</div>
										<div>
											<c:forEach items="${ fileList }" var="f">
											<div>
												<span><i class="far fa-file"></i></span><a class="mailDetailFile" href="fileDownload.ma?fileNo=${ f.fileNo }" ><c:out value="${ f.originName }"/></a>											
											</div>
											</c:forEach>
										</div>
									</td>                                
                                </tr>
                                </c:if>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>
<!-- popup include -->
<div id="popex" class="modal">
	<jsp:include page="../pop/pop.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    });
    
    $(document).on("click", ".mail_star", function() {
    	if($(this).prop('src').indexOf('empty') != -1) {
    		$(this).attr('src', '${contextPath}/resources/images/star.svg');
    		var mailNo = $(this).parents('tr').find('input:hidden').val();
    		
    		$.ajax({
    			url:"updateKeepYn.ma",
    			type:"post",
    			data: {
    				mailNo:mailNo,
    				state:'Y'
    			}
    		}); 
    	} else {
    		$(this).attr('src', '${contextPath}/resources/images/empty_star.svg');
			var mailNo = $(this).parents('tr').find('input:hidden').val();
    		$.ajax({
    			url:"updateKeepYn.ma",
    			type:"post",
    			data: {
    				mailNo:mailNo,
    				state:'N'
    			}
    		}); 
    	}
    });
    
    $("#deleteBtn").click(function() {
    	$.ajax({
    		url:"updateStatus.ma",
    		type:"post",
    		traditional : true,
    		data:{
    			deleteArr:$("body").find('#mailNo').val(),
    			type:' '
    		},
    		success: function() {
    			location.href='selectMail.ma';
    		}
    	});
    });
    
    $("#unReadBtn").click(function() {
    	$.ajax({
			url:"deleteRead.ma",
			type:"post",
			data: {
				mailNo:$("body").find('#mailNo').val(),
				eNo:${ loginUser.empDivNo }
			}, 
			success:function(data) {
				location.href='selectMail.ma';
			}
    	});
    });
    
    $("#replyBtn").click(function() {
    	location.href='sendReplyMail.ma?mailNo=' + $("body").find('#mailNo').val();
    });

</script>