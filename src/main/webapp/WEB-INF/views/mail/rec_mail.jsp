<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../inc/mail_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wk.css">
<c:if test="${ fn:indexOf(pageContext.request.queryString, 'type') > -1 }">
	<c:set var="q" value="${ fn:split(pageContext.request.queryString, '/=|&/')}" />
	<c:forEach items="${ q }" var="qr" varStatus="st">
		<c:if test="${ qr eq 'type' }">
			<c:set var="typeVar" value="${ q[st.index + 1] }"/>
				<c:if test="${ typeVar == 'searchCondition' }">
					<c:set var="typeVar" value=""/>
				</c:if>
		</c:if>
	</c:forEach>
</c:if>
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit">
                    	<div id="mail_h2">
                    		<c:if test="${typeVar == null || typeVar == ''}">
                    			<h2>받은 메일함</h2>
                    		</c:if>
                    		<c:if test="${typeVar == 'send'}">
                    			<h2>보낸 메일함</h2>
                    		</c:if>
                    		<c:if test="${typeVar == 'delete' }">
                    			<h2>휴지통</h2>
                    		</c:if>
                    	</div>
                    	<c:if test="${typeVar == null || typeVar == ''}">
                    	<div class="mail_count">
	                    	안읽은 메일 <strong>${ TotalListCount - TotalReadMailCount }</strong>
	                    	<span>/</span>
	                    	전체 메일 <strong>${ TotalListCount }</strong>
                    	</div>
                    	</c:if>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                    	<div class="mail_search">
                    		<c:if test="${typeVar != 'delete' }">
	                    	<form action="searchMail.ma" method="get">
                    			<input type="text" name="type" value="${ typeVar }" hidden="true" />
	                    		<select name="searchCondition" id="searchCondition">
	                    			<option value="title" selected>제목</option>
	                    			<option value="content">내용</option>
	                    			<option value="date">기간</option>
	                    		</select>
	                    		<div class="mail_search_box" id="searchbox">
	                       			<input type="text" name="mail_search" id="mail_search" value="${ searchValue }" placeholder="메일검색">
	                       		</div>
	                       		<div class="mail_search_box" id="datebox">
	                       			<input type="text" id="datepicker1" name="date1" value="${ date1 }" autocomplete="off"/> -
	                       			<input type="text" id="datepicker2" name="date2" value="${ date2 }" autocomplete="off"/>
	                       		</div>
	                        	<button class="btn_solid_white" id="searchBtn">검색</button>
                        	</form>
                        	</c:if>
                    	</div>
                    	<div class="mail_btn">
	                    	<span class="mail_allchk">
	                    		<input type="checkbox" id="mail_allchk" />
	                    	</span>
	                        <button class="btn_pink" id="deleteBtn">삭제</button>
	                        <c:if test="${ typeVar == null }">
                        		<button class="btn_main" id="readBtn">읽음</button>
                        	</c:if>
                        </div>
                      
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn" id="mail_tbl">
                                <colgroup>
                                	<col style="width: 3%;">
                                	<col style="width: 3%;">
                                	<col style="width: 1%;">
                                	<col style="width: 15%;">
                                    <col style="width: *;">
                                    <col style="width: 15%;">
                                </colgroup>
                                <%-- <tr>
                                  	<td><input type="checkbox" /></td>
                                  	<td><img src="${contextPath}/resources/images/empty_star.svg" class="mail_star"></td>
                                  	<td><img src="${contextPath}/resources/images/clip.svg" class="mail_clip"></td>
                                  	<td>taco from trello</td>
                                  	<td><a href="#">메일 제목</a></td>
                                  	<td>01-17 15:11</td>
                                </tr>
                                <tr>
                                  	<td><input type="checkbox" /></td>
                                  	<td><img src="${contextPath}/resources/images/star.svg" class="mail_star"></td>
                                  	<td><img src="${contextPath}/resources/images/clip.svg" class="mail_clip"></td>
                                  	<td>taco from trello</td>
                                  	<td><a href="#">메일 제목</a></td>
                                  	<td>01-17 15:11</td>
                                </tr> --%>
                            	<c:forEach items="${ list }" var="i" varStatus="st">
                            	<c:if test="${ list[st.index].mailNo != list[st.index+1].mailNo }">
                            	<tr class="mailList">
                            		<td><input type="checkbox" class="mailCheck" /><input type="hidden" value='<c:out value="${ i.mailNo }"/>' /></td>
                            		<td>
                            			<c:if test="${ i.keepYn == 'Y' }">
                            				<img src="${contextPath}/resources/images/star.svg" class="mail_star">
                            			</c:if>
                            			<c:if test="${ i.keepYn == 'N' }">
                            				<img src="${contextPath}/resources/images/empty_star.svg" class="mail_star">
                            			</c:if>
                            		</td>
                                  	<td>
                                  		<c:if test="${ attachCount[st.index] > 0 }">
	                                  		<img src="${contextPath}/resources/images/clip.svg" class="mail_clip">
                                  		</c:if>
                                  	</td>
                            		<td>
                            		<c:if test="${ i.type eq 'IN' }">
                            			<c:out value="${ i.empName } ${ i.jobName }/${ i.posName }"/>
                            		</c:if>
                            		<c:if test="${ i.type eq 'OUT' }">
                            			<c:out value="${ i.outSend }"/>
                            		</c:if>
                            		</td>
                           			<c:if test="${ i.readNo == 0 }">
                            			<td class="mailTitle">
                            				<strong><c:out value="${ i.mailTitle }"/></strong>
                            			</td>
                           			</c:if>
                           			<c:if test="${ i.readNo > 0 }">
                           				<td class="lightMailTitle">
                           					<strong><c:out value="${ i.mailTitle }"/></strong>
                           				</td>
                           			</c:if>
                            		<td>
                            			<c:if test="${ i.sendTime.indexOf('오후') != -1 }">
                            				<c:out value="${ i.sendTime.replace('오후', 'PM') }"/>
                            			</c:if>
                            			<c:if test="${ i.sendTime.indexOf('오전') != -1 }">
                            				<c:out value="${ i.sendTime.replace('오전', 'AM') }"/>
                            			</c:if>
									</td>
                           		</tr>
                           		</c:if>
                            	</c:forEach>
                            	<tr>
                                 	<td id="notice-nocnt">메일이 없습니다.</td>
                                 </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        	<c:if test="${ pi.currentPage eq 1 }">
                        		<li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage ne 1 }">
                        		<c:url var="blistFirst" value="${ path }">
                        			<c:if test="${ path.equals('selectMail.ma') }">
											<c:param name="currentPage" value="1"/>
											<c:param name="type" value="${ type }"/>
										</c:if>
										<c:if test="${ path.equals('searchMail.ma') }">
											<c:param name="currentPage" value="1"/>
											<c:param name="searchCondition" value="${ searchCondition }"/>
											<c:param name="mail_search" value="${ searchValue }"/>
											<c:param name="date1" value="${ date1 }"/>
											<c:param name="date2" value="${ date2 }"/>
											<c:param name="type" value="${ type }"/>
										</c:if>
                        		</c:url>
                        		<li class="pager_com pager_arr first"><a href="${ blistFirst }">&#x003C;&#x003C;</a></li>
                        	</c:if>
                   			<c:if test="${ pi.currentPage <= 1 }">
								<li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage > 1 }">
								<c:url var="blistBack" value="${ path }">
									<c:if test="${ path.equals('selectMail.ma') }">
											<c:param name="currentPage" value="${ pi.currentPage - 1 }"/>
											<c:param name="type" value="${ type }"/>
										</c:if>
										<c:if test="${ path.equals('searchMail.ma') }">
											<c:param name="currentPage" value="${ pi.currentPage - 1 }"/>
											<c:param name="searchCondition" value="${ searchCondition }"/>
											<c:param name="mail_search" value="${ searchValue }"/>
											<c:param name="date1" value="${ date1 }"/>
											<c:param name="date2" value="${ date2 }"/>
											<c:param name="type" value="${ type }"/>
										</c:if>
								</c:url>
								<li class="pager_com pager_arr prev"><a href="${ blistBack }">&#x003C;</a></li>
							</c:if>
							
							<c:forEach var="p" begin="${ pi.startPage }" end="${ pi.endPage }">
								<c:if test="${ p eq pi.currentPage }">
									<li class="pager_com pager_num on"><a href="javascrpt: void(0);">${ p }</a></li>
								</c:if>
								<c:if test="${ p ne pi.currentPage }">
									<c:url var="blistCheck" value="${ path }">
										<c:if test="${ path.equals('selectMail.ma') }">
											<c:param name="currentPage" value="${ p }"/>
											<c:param name="type" value="${ type }"/>
										</c:if>
										<c:if test="${ path.equals('searchMail.ma') }">
											<c:param name="currentPage" value="${ p }"/>
											<c:param name="searchCondition" value="${ searchCondition }"/>
											<c:param name="mail_search" value="${ searchValue }"/>
											<c:param name="date1" value="${ date1 }"/>
											<c:param name="date2" value="${ date2 }"/>
											<c:param name="type" value="${ type }"/>
										</c:if>
									</c:url>
									<li class="pager_com pager_num"><a href="${ blistCheck }">${ p }</a></li>
								</c:if>
							</c:forEach> 
							
							<c:if test="${ pi.currentPage >= pi.maxPage }">
								<li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
							</c:if>
							<c:if test="${ pi.currentPage < pi.maxPage }">
								<c:url var="blistEnd" value="${ path }">
									<c:if test="${ path.equals('selectMail.ma') }">
										<c:param name="currentPage" value="${ pi.currentPage + 1 }"/>
										<c:param name="type" value="${ type }"/>
									</c:if>
									<c:if test="${ path.equals('searchMail.ma') }">
										<c:param name="currentPage" value="${ pi.currentPage + 1 }"/>
										<c:param name="searchCondition" value="${ searchCondition }"/>
										<c:param name="mail_search" value="${ searchValue }"/>
										<c:param name="date1" value="${ date1 }"/>
										<c:param name="date2" value="${ date2 }"/>
										<c:param name="type" value="${ type }"/>
									</c:if>
								</c:url>
								<li class="pager_com pager_arr next"><a href="${ blistEnd }">&#x003E;</a></li>
							</c:if>
                      
                        	<c:if test="${ pi.currentPage eq pi.maxPage }">
                        		<li class="pager_com pager_arr last"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        	</c:if>
                        	<c:if test="${ pi.currentPage ne pi.maxPage }">
                        		<c:url var="blistLast" value="${ path }">
                        			<c:if test="${ path.equals('selectMail.ma') }">
										<c:param name="currentPage" value="${ pi.maxPage }"/>
										<c:param name="type" value="${ type }"/>
									</c:if>
									<c:if test="${ path.equals('searchMail.ma') }">
										<c:param name="currentPage" value="${ pi.maxPage }"/>
										<c:param name="searchCondition" value="${ searchCondition }"/>
										<c:param name="mail_search" value="${ searchValue }"/>
										<c:param name="date1" value="${ date1 }"/>
										<c:param name="date2" value="${ date2 }"/>
										<c:param name="type" value="${ type }"/>
									</c:if>
                        		</c:url>
                        		<li class="pager_com pager_arr last"><a href="${ blistLast }">&#x003E;&#x003E;</a></li>
                        	</c:if>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>


<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        $("#datebox").hide();
        if("${searchCondition}" != null) {
        	$("#searchCondition option").each(function() {
        		if($(this).val() == "${searchCondition}") {
        			$(this).prop("selected", true);
        			if($(this).val() == "date") {
        				$("#searchbox").hide();
        				$("#datebox").show();
        			} else {
        				$("#datebox").hide();
        			}
        		}
        	});
        }

        if('${ pageContext.request.queryString }'.substring(0,6) == 'type=&' || '${ pageContext.request.queryString }' == '') {
        	$("#menu_area .menu_list").eq(0).addClass("on");
        } else if('${ pageContext.request.queryString }' != '') {
        	if('${ pageContext.request.queryString }'.substring(0,9) == 'type=send') {
        		$("#menu_area .menu_list").eq(1).addClass("on");
        	} else if('${ pageContext.request.queryString }'.substring(0,12) == 'type=delete') {
        		$("#menu_area .menu_list").eq(3).addClass("on");
        	}
        }
        
		$("#notice-nocnt").hide();
        
        if(${ fn:length(list) eq 0 }) {
        	$("#notice-nocnt").show();
        }
    });

    
    $(document).on("click", ".mail_star", function() {
    	if($(this).prop('src').indexOf('empty') != -1) {
    		$(this).attr('src', '${contextPath}/resources/images/star.svg');
    		var mailNo = $(this).parents('tr').find('input:hidden').val();
    		$.ajax({
    			url:"updateKeepYn.ma",
    			type:"post",
    			data: {
    				mailNo:mailNo,
    				state:'Y'
    			}
    		});
    	} else {
    		$(this).attr('src', '${contextPath}/resources/images/empty_star.svg');
			var mailNo = $(this).parents('tr').find('input:hidden').val();
    		
    		$.ajax({
    			url:"updateKeepYn.ma",
    			type:"post",
    			data: {
    				mailNo:mailNo,
    				state:'N'
    			}
    		});
    	}
    });

    $("#mail_allchk").click(function() {
    	$(".mailCheck").click();
    });
    
    var deleteArr = [];
    $("#deleteBtn").click(function() {
    	$(".mailCheck").each(function() {
    		if($(this).is(":checked")) {
    			deleteArr.push($(this).parents('tr').find('input:hidden').val());
    			$(this).parents('tr').hide();
    		}
    	});
    	
    	console.log(deleteArr);
    	
    	$.ajax({
    		url:"updateStatus.ma",
    		type:"post",
    		traditional : true,
    		data:{
    			deleteArr:deleteArr,
    			type: '${ typeVar }'
    		}
    	});
    	
    	deleteArr = [];
    });
    
    var readArr = [];
    $("#readBtn").click(function() {
    	$(".mailCheck").each(function() {
    		if($(this).is(":checked")) {
    			readArr.push($(this).parents('tr').find('input:hidden').val());
    			
    			var temp = $(this).parents('tr').find('.mailTitle').text();
    			console.log($(this).parents('tr').children().eq(4).removeClass('mailTitle').addClass('lightMailTitle'));
    			
    			$(this).prop("checked", false);
    		}
    	});
    	
    	var eNo = ${ loginUser.empDivNo };
    	
    	$.ajax({
			url:"updateRead.ma",
			type:"post",
			traditional: true,
			data: {
				readArr:readArr,
				eNo:eNo
			}
    	});
    	
    	readArr = [];
    });
    
    $(document).on("click", ".mailTitle", function() {
    	location.href = 'selectOneMail.ma?mailNo=' + $(this).parents('tr').find('input:hidden').val();
    });
    
    $(document).on("click", ".lightMailTitle", function() {
    	location.href = 'selectOneMail.ma?mailNo=' + $(this).parents('tr').find('input:hidden').val();
    });
    
	$("#datepicker1").datepicker({
	     changeMonth: true,
	     changeYear: true,
	     nextText: '다음 달',
	     prevText: '이전 달',
	     dateFormat: "yy/mm/dd",
	     showMonthAfterYear: true , 
	     dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
	     monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
	 });
	
	$("#datepicker2").datepicker({
	     changeMonth: true,
	     changeYear: true,
	     nextText: '다음 달',
	     prevText: '이전 달',
	     dateFormat: "yy/mm/dd",
	     showMonthAfterYear: true , 
	     dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
	     monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
	 });
	
	
	$(document).on("change", "#searchCondition", function() {
		if($(this).val() == 'date') {
			$("#datebox").show();
			$("#searchbox").hide();
			$("#mail_search").val("");
		} else {
			$("#datebox").hide();
			$("#searchbox").show();
			$("#datepicker1").val("");
			$("#datepicker2").val("");
		}
	});
</script>


</body>
</html>
