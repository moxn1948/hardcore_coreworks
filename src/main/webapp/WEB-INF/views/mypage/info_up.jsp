<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
	파일설명 : 개인정보 설정-정보확인  페이지
--%>
<jsp:include page="../inc/myPage_menu.jsp" />
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>개인정보 설정</h2></div>

	                <form action="updateInfo.mp" method="post" encType="multipart/form-data">
                    <div class="main_info">
	                    	<div class="img_area">
	                    		<c:if test="${ empty att }">
		                    		<img src="${ contextPath }/resources/images/primary_picture.png" class="main_img">
		                    	</c:if>
		                    	<c:if test="${ not empty att }">	                    		
			                    	<img src="${ contextPath }/resources/uploadFiles/${ att.changeName }" class="main_img">
		                    	</c:if>
								<div class="file_input_div">
									<button class="btn_white file_input_button">등록</button>
									<input type="file" class="file_input_hidden" name="attachments" accept="image/*" />
								</div>
	                    	</div>
	                    	
	                    	<table class="emp_info">
	                    		<tr>
	                                <td>이름</td>
	                                <td>${ sessionScope.loginUser.empName }</td>
	                            </tr>
	                            <tr>
	                                <td>아이디</td>
	                                <td>${ sessionScope.loginUser.empId }</td>
	                            </tr>
	                            <tr>
	                                <td>사번</td>
	                                <td>${ sessionScope.loginUser.empNo }</td>
	                            </tr>
	                            <tr>
	                                <td>부서</td>
	                                <td>${ belong.deptName }</td>
	                            </tr>
	                            <tr>
	                                <td>직책/직급</td>
	                                <td>${ belong.posName }/${ belong.jobName }</td>
	                            </tr>
	                            <tr>
	                                <td>내선전화</td>
	                                <td><input type="text" name="extPhone" value="${ empNew.extPhone }"></td>
	                            </tr>
	                            <tr>
	                                <td>휴대전화</td>
	                                <td>
	                                	<input type="text" size="2" class="call_phone" name="phones" maxlength="3" value="${ phone[0] }">-
	                                	<input type="text" size="3" class="call_phone" name="phones" maxlength="4" value="${ phone[1] }">-
	                                	<input type="text" size="3" class="call_phone" name="phones" maxlength="4" value="${ phone[2] }">
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>생일</td>
	                                <td>
	                                <p:formatDate value="${ empNew.birth }" var="birth" pattern="yyyy/MM/dd"/>
	                                	<input type="text" id="birth" name="birthday" class="date"
											placeholder="생일" value="${ birth }" readonly>	
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>주소</td>
	                                <td>
	                                	<div class="address_form_wrap">
		                                    <div class="">
		                                    	<input type="text" name="addresses" id="postcode_form" placeholder="우편번호"
		                                    		value="${ empNew.address.split('/')[0] }" readonly>
		                                    		<a href="#" class="button btn_pink" id="address_srch_btn" onclick="DaumPostcode();">검색</a>
		                                    	</div>
		                                    <div class=""><input type="text" name="addresses" id="address_form" placeholder="주소" 
		                                    	value="${ empNew.address.split('/')[1] }" readonly></div>
		                                    <div class=""><input type="text" name="addresses" id="detailAddress_form" placeholder="상세주소"
		                                    	value="${ empNew.address.split('/')[2] }"></div>
	                               		</div>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>성별</td>
	                                <td>
	                                	<div class="presence">
				                        	<input type="radio" name="gender" value="M" id="male">
				                            <label for="male">남</label>
				                            <input type="radio" name="gender" value="F" id="female">
				                            <label for="female">여</label>
				                            <input type="text" id="gender" value="${ empNew.gender }" hidden>
			                            </div>
	                               	</td>
	                            </tr>
	                            <tr>
	                                <td>팩스번호</td>
	                                <c:if test="${ empty empNew.fax }">
	                            		<td><input type="text" name="fax"></td>
	                            	</c:if>
	                            	<c:if test="${ not empty empNew.fax }">
	                            		<td><input type="text" name="fax" value="${ empNew.fax }"></td>
	                            	</c:if>
	                            </tr>
	                            <tr>
	                                <td>입사일</td>
	                                <td><p:formatDate value="${ empNew.enrollDate }" pattern="yy/MM/dd"/></td>
	                            </tr>
	                        </table>
	                    </div>
	                   
                        <div id="btnSet">
                            <button type="submit" class="btn_main">취소</button>
                            <button class="btn_main">확인</button>
                        </div>
                	 </form>
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');

        $("#postcode_form, #address_form").on("click", function(){
            $("#address_srch_btn").trigger("click");
        });
        // 주소 검색 끝
        
        $(".date").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            yearRange: '1900:2020',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
        });
        
        const gender = "${ empNew.gender }";
        console.log(gender + 1);
        
        if(gender === 'M') {
        	$('#male').prop('checked', true);
        } else {
        	$('#female').prop('checked', true);
        }
        
        
    });
    
    $(".file_input_hidden").on('change', function(){
    	loadImg(this);
    });
    
    function loadImg(value) {
    	if(value.files && value.files[0]) {
    		const reader = new FileReader();
    		
    		reader.onload = function(e) {
    			$(".main_img").prop("src", e.target.result);
    		}
    		
    		reader.readAsDataURL(value.files[0]);
    	}
    }
    
    function DaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('postcode_form').value = data.zonecode;
                document.getElementById("address_form").value = addr;
                document.getElementById("detailAddress_form").focus();
            }
        }).open();
    }
</script>
</body>
</html>
