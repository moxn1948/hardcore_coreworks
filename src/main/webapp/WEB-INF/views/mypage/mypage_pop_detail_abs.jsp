<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<h3 class="main_tit">부재 상세보기</h3>

                    <div class="tbl_common tbl_basic absence_detail">
                    	<div class="tbl_wrap">
                            <table class="tbl_ctn">
	                                <tr>
										<th>시작일시</th>
										<th>종료일시</th>
										<th>사유</th>
										<th>대리결재자</th>
	                                </tr>
	                                <tr>
	                                	<td class="startDateTime"></td>
	                                	<td class="endDateTime"></td>
	                                	<td class="reason"></td>
	                                	<td class="abs_realName"></td>
	                                </tr>
                            </table>
                        </div>
                    </div>
                    
                    <h4 class="main_tit">참고사항</h4>
                    <div class="ann_reason"></div>
                    
                    <div class="close_area">
                    	<button class="btn_main detail_close"><a href="#" rel="modal:close">확인</a></button>
                    </div>