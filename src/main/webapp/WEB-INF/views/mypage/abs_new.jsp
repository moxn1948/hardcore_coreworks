<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
	파일설명 : 부재설정-부재이력 페이지
--%>
<style>
	.ui-datepicker{z-index: 6000 !important;}
</style>
<jsp:include page="../inc/myPage_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>부재설정</h2></div>

                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <form action="insertAbs.mp" method="post">
	                        <div class="tbl_wrap tbl_abs_new">
		                            <table class="tbl_ctn abs_set">
		                           		<colgroup>
		                                    <col style="width: 16%;">
		                                    <col style="width: 84%;">
		                                </colgroup>
			                        	<tr>
			                                <td>부재기간</td>
			                                <td>
			                                	<div class="main_cnt_list clearfix set_abs_time">
				                                	<div class="cnt sel_day">                                
											            <input type="text" id="from" name="from" class="date date_1"
											             placeholder="시작일" readonly>
											            <select name="absStime" id="start_time" class="date_time date_time_1">
											                <option value="" hidden>시간선택</option>
											                <option value="allday">종일</option>
											            </select>
											            <input type="text" id="to" name="to" class="date date_2"
											             placeholder="종료일" readonly>
											            <select name="absEtime" id="end_time" class="date_time date_time_2" disabled>
											                <option value="" hidden>시간선택</option>
											                <option value="allday">종일</option>
											            </select>
	                                    			</div>
					                                <div class="cnt all_day">
											 	       <input type="datetime" id="allDay" name="allDay" class="date date_3" placeholder="날짜" readonly>
											        </div>
											        <div class="cnt day_chk">
											            <input type="checkbox" name="allYn" id="allDayChk"><label for="allDayChk">종일</label>
											        </div>
										        </div>
			                                </td>
			                            </tr>
			                            <tr>
			                                <td>부재사유</td>
			                                <td>
			                                	<select name="absReason">
			                                		<option value="" hidden>사유선택</option>
			                                		<option value="교육">교육</option>
		                                            <option value="출장">출장</option>
		                                            <option value="외출">외출</option>
		                                            <option value="휴가">휴가</option>
		                                            <option value="조퇴">조퇴</option>
		                                            <option value="병가">병가</option>
		                                            <option value="공가">공가</option>
		                                            <option value="특별휴가">특별휴가</option>
		                                            <option value="결근">결근</option>
		                                            <option value="휴직">휴직</option>
		                                            <option value="퇴직">퇴직</option>
			                                	</select>
			                                </td>
			                            </tr>
			                            <tr>
			                                <td>대리 결재자 유무</td>
			                                <td>
			                                	<div class="presence">
			                                        <input type="radio" name="proxyYn" value="Y" id="yes" checked>
			                                        <label for="yes">유</label>
			                                        <input type="radio" name="proxyYn" value="N" id="no">
			                                        <label for="no">무</label>
		                                        </div>
 		                                        <div class="add_presence">
 		                                        	<div style="display: flex;">
			                                        	<a href="#popAddAbs" rel="modal:open">
			                                        		<button type="button" class="btn_main btn_presence">대리 결재자</button>
			                                        	</a>
			                                        	<input type="text" name="addPresence" id="myPresence" readonly>
			                                        	<input type="text" name="proxyName" id="myPresenceNo" readonly hidden>
		                                        	</div>
		                                        </div>
		                                    </td>
			                            </tr>
			                            <tr>
			                                <td>참고사항</td>
			                                <td>
			                                	<textarea class="note" name="absCnt"></textarea>
			                                </td>
			                            </tr>
		                            </table>
	                        </div>
		                            <div id="btnSet" class="btnSet">
	                        			<button type="button" class="btn_white abs_cancel">취소</button>
		                            	<button type="submit" class="btn_main btn_insert" onclick="return absInsert()">등록</button>
		                            </div>
                        </form>
                    </div>
                    <!-- 기본 테이블 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>


<!-- 주소록 팝업 -->
<div id="popAddAbs" class="modal">
	<jsp:include page="mypage_pop_add_abs.jsp" />
</div>



<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');
        
        $('.tbl_ctn').css('text-align-last', 'left');
        
        $(".date_1").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
            minDate: 0,
        });
        
        $(".date_3").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
            minDate: 0,
        });
        
    });
    
    // 날짜설정관련
    $(document).on('change', '.date_1', function() {
    	$('#to').remove();
    	$('#start_time').after('<input type="text" id="to" name="to" class="date date_2" placeholder="종료일" readonly="">');
    	const startDateStr = $('.date_1').val();
    	const startDate = Date.parse(startDateStr);
    	const nowDateYear = new Date().getFullYear();
    	const nowDateMonth = new Date().getMonth();
    	const nowDateDay = new Date().getDate();
    	
    	const nowDateStr = nowDateYear + "/" + (nowDateMonth+1) + "/" + nowDateDay;
    	
    	const nowDate = Date.parse(nowDateStr);
    	
    	const dateDiff = (startDate - nowDate) / 86400000;
    	
    	$(".date_2").datepicker({
            changeMonth: true,
            changeYear: true,
            nextText: '다음 달',
            prevText: '이전 달',
            dateFormat: "yy/mm/dd",
            showMonthAfterYear: true , 
            dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
            monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
            minDate: dateDiff,
        });
    });
    
    function optionIndex(startTime) {
       	const startTimeIndex = $("#start_time option[value='"+startTime+"']").index();
       	
       	return startTimeIndex;

    }
    
    // 시간설정관련
    $(document).on('change', '#to', function() {
    	$('#end_time').attr('disabled', false);
    	
    	const startTime = $('#start_time').val();
    	console.log(startTime);
    	
    	const index = optionIndex(startTime)+1;
    	const $from = $('#from').val();
    	const $to = $('#to').val();
    	
    	if($from === $to) {
	    	$('#end_time option:lt('+index+')').attr('disabled', true);
    	}
    })
    
    function absInsert() {
    	// 시간종합
      	if(!($('#allDayChk').prop('checked'))) {
	        const day1 = $('#from').val();
	        const day2 = $('#to').val();
	        const startTime = $('#start_time').val();
	        const endTime = $('#end_time').val();
	        const time1 = new Date(day1 + " " + startTime);
	        const time2 = new Date(day2 + " " + endTime);
	        
	        console.log(day1);
	        console.log(day2);
	        console.log(startTime);
	        console.log(endTime);
	        
	        console.log(time1)
	        console.log(time2)
	        
	        // 시간설정관련1
	         if(time1>time2 || day1 === null || day2 === null || startTime === '' || endTime === '') {
	        	alert('시작시간과 종료시간을 확인해주세요.');
	        	return false;
	        }
	        
	        // 시간설정관련2
	        if(startTime === 'allday' || endTime === 'allday') {
	        	if(!(startTime === 'allday' && endTime === 'allday')) {
	        		alert("시작일이나 종료일 시간이 종일로 설정되있다면 동일하게 맞춰져야합니다.");
	            	return false;
	        	}
	        }
    	} else {
    		if($('#allDay').val() === null) {
    			alert('날짜를 확인해주세요');
    			return false;
    		}
    	}
      	
    	// 대결자 관련
    	if($('#yes').prop('checked')) {
    		if($('#myPresence').val() === null || $('#myPresence').val() === '') {
    			alert('대리 결재자를 지정해주세요.');
    			return false;
    		}
    	}
      	
      	return true;
    }
    
    setTimeSel("#start_time", 23);
    setTimeSel("#end_time", 23);
    function setTimeSel(sel, num){
        for(var i = 0; i <= num; i++){
            
            var hour;
            var day;
            if(i > 12){
               day = i - 12;
            }else{
                day = i;
            }

            if(day < 10){
                if(i < 12){

                    hour = "0" + day + ":00 오전";
                }else{

                    hour = "0" + day + ":00 오후";
                }
            }else{
                if(i < 12){
                    hour = day + ":00 오전";
                }else{
                    hour = day + ":00 오후";
                }
            }

            $(sel).append("<option value='" + hour + "'>" + hour + "</option>");
            
            if(day < 10){
                if(i < 12){
                    hour = "0" + day + ":30 오전";
                }else{
                    hour = "0" + day + ":30 오후";
                }
            }else{
                if(i < 12){
                    hour = day + ":30 오전";
                }else{
                    hour = day + ":30 오후";
                }
            }

            $(sel).append("<option value='" + hour + "'>" + hour + "</option>");
        }
    }
    
    $('#no').on('change', function() {
		$('.add_presence>div').hide();
    });
    
    $('#yes').on('change', function() {
		$('.add_presence>div').show();
    })


    $("#allDayChk").on("change",function(){
        if($(this).prop("checked") == true){
            $(".sel_day").css({display : "none"});
            $(".all_day").css({display : "block"});
        }else{
            $(".sel_day").css({display : "block"});
            $(".all_day").css({display : "none"});
        }
    });
</script>
</body>
</html>
