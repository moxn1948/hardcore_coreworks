<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
	파일설명 : 개인정보 설정-정보확인  페이지
--%>

<jsp:include page="../inc/myPage_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>개인정보 설정</h2></div>

                    <div class="main_info">
	                    <div class="img_area">
	                    	<c:if test="${ empty att }">
	                    		<i class="fas fa-user-circle" style="font-size: 125px;"></i>
	                    	</c:if>
	                    	<c:if test="${ not empty att }">	                    		
		                    	<img src="${ contextPath }/resources/uploadFiles/${ att.changeName }" class="main_img">
	                    	</c:if>
	                    </div>
                    	
                    	<table class="emp_info">
                    		<tr>
                                <td>이름</td>
                                <td>${ empNew.empName }</td>
                            </tr>
                            <tr>
                                <td>아이디</td>
                                <td>${ empNew.empId }</td>
                            </tr>
                            <tr>
                                <td>사번</td>
                                <td>${ empNew.empNo }</td>
                            </tr>
                            <tr>
                                <td>부서</td>
                                <td>${ belong.deptName }</td>
                            </tr>
                            <tr>
                                <td>직책/직급</td>
                                <td>${ belong.posName }/${ belong.jobName }</td>
                            </tr>
                            <tr>
                                <td>내선전화</td>
                                <td>${ empNew.extPhone }</td>
                            </tr>
                            <tr>
                                <td>휴대전화</td>
                                <td>${ empNew.phone }</td>
                            </tr>
                            <tr>
                                <td>생일</td>
                                <td><p:formatDate value="${ empNew.birth }" pattern="yy/MM/dd"/></td>
                            </tr>
                            <tr>
                                <td>주소</td>
                                <td>${ empNew.address }</td>
                            </tr>
                            <tr>
                                <td>성별</td>
                                <td>
                                	<c:if test="${ empNew.gender eq 'M' }">
                                		남
                                	</c:if> 
                                	<c:if test="${ empNew.gender eq 'F' } ">
                                		여
                                	</c:if>
                                </td>
                            </tr>
                            <tr>
                                <td>팩스번호</td>
	                                <c:if test="${ empty empNew.fax }">
	                            		<td>없음</td>
	                            	</c:if>
	                            	<c:if test="${ not empty empNew.fax }">
	                            		<td>${ empNew.fax }</td>
	                            	</c:if>
                            </tr>
                            <tr>
                                <td>입사일</td>
                                <td><p:formatDate value="${ empNew.enrollDate }" pattern="yy/MM/dd"/></td>
                            </tr>
                        </table>
                    </div>
                        <div id="btnSet">
                            <button class="btn_main" onclick="location.href='infoUp.mp'">수정하기</button>
                        </div>
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');

        console.log("${ att.changeName }");
    });
</script>
</body>
</html>
