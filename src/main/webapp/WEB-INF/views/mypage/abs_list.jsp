<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="p" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%-- 
	파일설명 : 부재설정-부재이력 페이지
--%>

<jsp:include page="../inc/myPage_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>부재설정</h2></div>
                    
                    <!-- 년도 선택 -->
                    <div class="add_area">
                    	<a class="button btn_main abs_add" href="absNew.mp">추가</a>
                    </div>

                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap abs_table">
                            <table class="tbl_ctn">
                                <tr>
                                	<th>시작일</th>
                                	<th>종료일</th>
                                	<th>사유</th>
                                	<th>대리결재자</th>
                                	<th>결재이력</th>
                                </tr>
                                <!-- 없을 시 한줄로 "설정한 부재가 없습니다." 표기 -->
                                
	                            	<c:choose>
	                            		<c:when test="${ absList.size() == 0 }">
	                            			<tr>
	                            				<td colspan="5">추가한 부재가 없습니다. 왼쪽 상단의 "추가"버튼을 클릭해서 추가해주세요.</td>
	                            			</tr>
	                            		</c:when>
	                            		<c:otherwise>
		                            		<c:forEach var="a" items="${ absList }">	                            			
			                            		<tr>
					                                <td><p:formatDate value="${ a.absSdate }" pattern="yyyy/MM/dd"/></td>
					                                <td><p:formatDate value="${ a.absEdate }" pattern="yyyy/MM/dd"/></td>
				                                	<td>${ a.absReason }</td>
				                                	<c:if test="${ a.proxyYn eq 'Y' }">
					                                	<td>
					                                		<a href="#popDetailAbsence" rel="modal:open" class="detail_abs">${ a.proxyRealName }</a>
					                                		<input type="text" class="abs_no" value="${ a.absNo }" hidden>
					                                	</td>
					                                	<td>
					                                		<a href="#popListAbsence" rel="modal:open">
					                                			<button class="btn_main abs_history">결재이력</button>
					                                		</a>
					                                		<input type="text" class="abs_no" value="${ a.absNo }" hidden>
					                                	</td>
				                                	</c:if>
				                                	<c:if test="${ a.proxyYn eq 'N' }">
				                                		<td>대결자 없음</td>
					                                	<td>

					                                	</td>
				                                	</c:if>
			                               		</tr>
		                            		</c:forEach>
	                            		</c:otherwise>
	                            	</c:choose>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <c:if test="${ absList.size() > 0 }">
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        
                        <!-- <<, < -->
                        <c:if test="${ pi.currentPage > 1 }">
							<c:url var="blistBack" value="absList.mp">
								<c:param name="currentPage" value="${ pi.currentPage - 1 }"/>
							</c:url>
							<c:url var="blistStart" value="absList.mp">
								<c:param name="currentPage" value="${ pi.startPage }"/>
							</c:url>
							<li class="pager_com pager_arr first"><a href="${ blistStart }">&#x003C;&#x003C;</a></li>
							<li class="pager_com pager_arr prev"><a href="${ blistBack }">&#x003C;</a></li>
                        </c:if>
                        <c:if test="${ pi.currentPage <= 1 }">
							<li class="pager_com pager_arr first"><a onclick="return false;">&#x003C;&#x003C;</a></li>
							<li class="pager_com pager_arr prev"><a onclick="return false;">&#x003C;</a></li>
                        </c:if>
                        
                        <!-- 페이징 -->
                        <c:forEach var="p" begin="${ pi.startPage }" end="${ pi.endPage }">
							<c:if test="${ p eq pi.currentPage }">
								<li class="pager_com pager_num on"><a href="javascrpt: void(0);">${ p }</a></li>
							</c:if>
							<c:if test="${ p ne pi.currentPage }">
								<c:url var="blistCheck" value="absList.mp">
									<c:param name="currentPage" value="${ p }"/>
								</c:url>
								<li class="pager_com pager_num"><a href="${ blistCheck }">${ p }</a></li>
							</c:if>
						</c:forEach>
                        
                        <!-- >, >> -->
                        <c:if test="${ pi.currentPage < pi.endPage }">
							<c:url var="blistNext" value="absList.mp">
								<c:param name="currentPage" value="${ pi.currentPage + 1 }"/>
							</c:url>
							<c:url var="blistEnd" value="absList.mp">
								<c:param name="currentPage" value="${ pi.endPage }"/>
							</c:url>
							<li class="pager_com pager_arr next"><a href="${ blistNext }">&#x003E;</a></li>
	                        <li class="pager_com pager_arr end"><a href="${ blistEnd }">&#x003E;&#x003E;</a></li>
						</c:if>
						<c:if test="${ pi.currentPage >= pi.endPage }">
							<li class="pager_com pager_arr next"><a onclick="return false;">&#x003E;</a></li>
	                        <li class="pager_com pager_arr end"><a onclick="return false;">&#x003E;&#x003E;</a></li>
						</c:if>
						
                        </ul>
                    </div>
                    </c:if>
                    <!-- 페이저 끝 -->
                </div>
            </div>
        </div>
    </main>
</div>

<!-- 결재이력상세보기 팝업 -->
<div id="popListAbsence" class="modal">
	<jsp:include page="mypage_pop_list_abs.jsp" />
</div>

<!-- 대결자 상세 팝업 -->
<div id="popDetailAbsence" class="modal">
	<jsp:include page="mypage_pop_detail_abs.jsp" />
</div>


<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');
    });
    
    const easHistoryClone = $(".tbl_ctn.eas_history").html();
    
    $(document).on('click', '.abs_history', function(event) {
    	const absNo = $(this).parents('td').children().eq(1).val();
    	
    	$.ajax({
    		url: "absHistory.mp",
    		type: "post",
    		data: {
    			absNo: absNo,
    		},
    		success: function(data) {
    			console.log(data)
    			const map = JSON.parse(data);
    			const easSize = map.easHistoryList.length;
    			const docSize = map.docDetailList.length;

	            let html = "";
	                                
    			if(easSize <= 0) {
    				html = "<td class='none_history' colspan='4'>진행된 결재가 없습니다.</td>";
    				
    			} else {
    				for(let i=0; i < easSize; i++) {
    					html += '<tr><td class="eas_no">'+map.easHistoryList[i].easNo+'</td>';
    					
    					for(let j=0; j < docSize; j++) {
    						
    						if(map.docDetailList[j].easNo === map.easHistoryList[i].easNo) {
    							html += '<td class="doc_tit">'+map.docDetailList[j].docTit+'</td>';
    						}
    					}
    					
    					function replaceAll(str, searchStr, replaceStr) {
        					return str.split(searchStr).join(replaceStr);
        				}
    					
    					const state = map.easHistoryList[i].procState;
    					let msg = "";
    					
    					switch(state) {
    					case 'AGREE' : 
    						msg = "찬성";
    						break;
    					
    					case 'OPPOS' :
    						msg = "반대";
    						break;
    						
    					case 'RETURN' :
    						msg = "반려";
    						break;
    					
    					case 'CANCEL' :
    						msg = "취소";
    						break;
    					
    					case 'CONFIRM' :
    						msg = '확인';
    						break;
    						
    					case 'AS' :
    						msg = '결재';
    						break;
    					}
    					
        				html += '<td class="proc_state">'+msg+'</td>';
        				html += '<td class="proc_dateTime">'+replaceAll(map.easHistoryList[i].procDate, '-', '/')+' '+map.easHistoryList[i].procTime+'</td></tr>';
    				}
    			}
    			
    			$(".tbl_ctn.eas_history").html('');
				$('.tbl_ctn.eas_history').html(easHistoryClone);
				$('.tbl_ctn.eas_history').append(html);
    			
    		}
    	});
    });
    
    $(document).on('click', '.detail_abs', function(event) {
    	const $detailAbs = $(this);
    	const absNo = $(this).parents('td').children().eq(1).val();
    	const sdate = $(this).parents('tr').children().eq(0).html();
    	const edate = $(this).parents('tr').children().eq(1).html();
    	const realName = $(this).parents('td').children().eq(0).html();
    	
    	$.ajax({
    		url: "absDetail.mp",
    		type: "post",
    		data: {
    			absNo: absNo,
    		},
    		success: function(data) {
    			const abs = JSON.parse(data);
    			
    			console.log(abs);
    			
    			$('.startDateTime').html(sdate + " " + abs.absStime);
    			$('.endDateTime').html(edate + " " + abs.absEtime);
    			$('.reason').html(abs.absReason);
    			$('.abs_realName').html(realName);
    			
    			if(abs.absCnt === null) {
    				$('.ann_reason').html("작성된 참고사항이 없습니다.");
    			} else {
	    			$('.ann_reason').html(abs.absCnt);
    			}
    		},
    		error: function(error) {
    			alert('알수없는 에러가 발생했습니다.');
    		}
    	});
    });
</script>
</body>
</html>
