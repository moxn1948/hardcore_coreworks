<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<h3 class="main_tit">결재이력 상세보기</h3>

                    <div class="tbl_common tbl_basic absence_status">
                    	<div class="tbl_wrap">
                            <table class="tbl_ctn eas_history">
                            	<colgroup>
                                    <col style="width: *;">
                                    <col style="width: 40%;">
                                    <col style="width: *;">
                                    <col style="width: *;">
                                </colgroup>
                                <thead>
	                                <tr>
										<th>기안번호</th>
										<th>문서제목</th>
										<th>처리상태</th>
										<th>결재일시</th>
	                                </tr>
	                            </thead>
                            </table>
                        </div>
                    </div>
                    
                    <div class="close_area">
                    	<button class="btn_main detail_close"><a href="#" rel="modal:close">확인</a></button>
                    </div>