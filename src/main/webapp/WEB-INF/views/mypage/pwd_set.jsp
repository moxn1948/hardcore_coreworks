<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	파일설명 : 암호 설정 페이지
--%>

<jsp:include page="../inc/myPage_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit pwd_tit"><h2>암호 설정</h2></div>

                    <div class="main_pwd">
						<table class="chg_pwd">
							<tr>
								<td>이전 암호</td>
								<td class="have_message">
									<input type="password" id="old_password" class="password" name="old_password">
									<label id="before_status" class="status"></label>
								</td>
							</tr>
							<tr>
								<td>새 암호</td>
								<td class="have_message"><input type="password" id="new_password" class="password" name="new_password"></td>
							</tr>
							<tr>
								<td>새 암호 확인</td>
								<td class="have_message">
									<input type="password" id="new_password_check" class="password" name="new_password_check">
									<label id="change_status" class="status"></label>
								</td>
							</tr>
							<tr>
								<td colspan="2"><button class="btn_solid_pink chg_pwd_end" onclick="pwdChange();">수정</button></td>
							</tr>
						</table>
	                </div>
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>


<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');

        $("#postcode_form, #address_form").on("click", function(){
            $("#address_srch_btn").trigger("click");
        });
    });
    
    $(document).on('keyup', '#old_password', function() {
    	const oldPwd = $('#old_password').val();
    	const $td = $(this).parents('tr').children().eq(0);
    	
    	$.ajax({
    		url:"checkPwd.mp",
    		type:"post",
    		data: {
    			old_password:oldPwd,
    		},
    		success:function(data) {
    			const message = JSON.parse(data);
    			
    			if(message === 'error') {
    				$td.css('padding-bottom', '20px');
    				$('#before_status').html("기존의 비밀번호와 일치하지 않습니다.").css('color', 'tomato');
    			} else {
    				$('#before_status').html("기존의 비밀번호와 일치합니다.").css('color', 'green');
    			}
    		},
    		
    		error:function(status) {
    			console.log(status);
    			console.log(2);
    		}
    	});
    })
    
    $(document).on('keyup', '#new_password_check', function() {
    	const newPwd = $('#new_password').val();
    	const newPwdChk = $('#new_password_check').val();
    	const $td = $(this).parents('tr').children().eq(0);
    	
    	if(newPwd.length > 0) {
    		if(newPwd === newPwdChk) {
    			$('#change_status').html("새 암호와 일치합니다.").css('color', 'green');
    			$td.css('padding-bottom', '20px');
    		} else {
    			$('#change_status').html("새 암호와 일치하지 않습니다.").css('color', 'tomato');
    			$td.css('padding-bottom', '20px');
    		}
    	}
    })
    
    function pwdChange() {
    	const msg1 = $('#before_status').html();
    	const msg2 = $('#change_status').html();
    	
    	if(msg1 === '기존의 비밀번호와 일치합니다.' && msg2 === '새 암호와 일치합니다.') {
    		const newPwd = $('#new_password').val();
    		$.ajax({
				url: "updatePwd.mp",
				type: "post",
				data: { new_password:newPwd, },
				success: function(data) {
					console.log(11);
					alert('비밀번호가 변경되었습니다.\n다시 로그인해주세요.');
					location.href='logout.mp';
				},
				error: function(error) {
					console.log(21);
					alert(error);
				}
			})
    	}
    }
    
    /* function pwdChange() {
    	const oldPwd = $('#old_password').val();
    	const newPwd = $('#new_password').val();
    	const newPwdChk = $('#new_password_check').val();
    	
    	$.ajax({
    		url:"checkPwd.mp",
    		type:"post",
    		data: {
    			old_password:oldPwd,
    			new_password:newPwd,
    			new_password_check:newPwdChk,
    		},
    		success:function(data) {
    			console.log(data);
    			console.log(typeof data);
    			if(data.indexOf('error') > 0) {
    				alert('비밀번호를 다시 확인해주세요.');
    			} else {
    				$.ajax({
    					url: "updatePwd.mp",
    					type: "post",
    					data: { new_password:newPwd, },
    					success: function(data) {
    						console.log(11);
    						alert('비밀번호가 변경되었습니다.\n다시 로그인해주세요.');
    						location.href='logout.mp';
    					},
    					error: function(error) {
    						console.log(21);
    						alert(error);
    					}
    				})
    			}
    		},
    		error:function(status) {
    			console.log(status);
    			console.log(2);
    		}
    	});
    }  */
</script>
</body>
</html>
