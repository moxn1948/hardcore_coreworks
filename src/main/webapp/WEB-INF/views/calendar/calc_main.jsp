<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/bootstrap/main.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/bootstrap/main.min.css" /> -->

<c:if test="${ !empty loginUser }">

<jsp:include page="../inc/calendar_menu.jsp" />
    
<!-- tree api 관련 시작 -->

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales/ko.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/interaction/main.min.js"></script>

<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">
			<div id="scroll_area" class="inner_rt">
				<!-- 메인 컨텐츠 영역 시작! -->
				<div class="main_ctn calendar_page">
					<!-- ** 코드 작성부 시작 -->
					<div class="main_cnt">
						<div class="calc_wrap">
							<div class="auth_wrap">
	                			<c:if test="${ !sessionScope.loginUser.getAuth().equals('ADMIN') }">
								<ul class="auth_cnt">
									<li class="auth_list solo"><input type="checkbox" id="soloChk" checked><label for="soloChk">개인일정</label></li>
									<li class="auth_list dept"><input type="checkbox" id="deptChk" checked><label for="deptChk">부서일정</label></li>
									<li class="auth_list grp"><input type="checkbox" id="groupChk" checked><label for="groupChk">단체일정</label></li>
									<li class="auth_list comp"><input type="checkbox" id="compChk" checked><label for="compChk">회사일정</label></li>
								</ul>
								</c:if>
	                			<c:if test="${  sessionScope.auth.getClm().equals('Y') }">
			                        <!-- 부서 셀렉트 박스 시작 -->
			                        <div class="addr_sel_wrap">
			                            <div class="add_sel">
			                                <input type="text" name="" id="" placeholder="부서를 선택하세요." readonly class="add_sel_ipt">
			                                <div class="add_sel_opt">
			                                    <div id="addrSelTree" class="tree_menu">
							                  		<ul>
							                  			<li data-cstrender="true">전체보기<input type="hidden" value="0" class="dept"></li>
								                       <c:forEach var="list" items="${ deptList }">
							                       			<li class="folder dept_folder"><c:out value="${ list.deptName }" /><input type="hidden" value="${ list.deptNo }" class="dept">
							                       				<c:if test="${ list.deptOrderList.size() != 0 }">
							                       					<ul>
							                     						<c:forEach var="list2" items="${ list.deptOrderList }">
							                       							<li class="folder dept_folder"><c:out value="${ list2.deptName }" /><input type="hidden" value="${ list2.deptNo }" class="dept">
											                       				<c:if test="${ list2.deptOrderList.size() != 0 }">
											                       					<ul>
											                     						<c:forEach var="list3" items="${ list2.deptOrderList }">
											                       							<li class="folder dept_folder"><c:out value="${ list3.deptName }" /><input type="hidden" value="${ list3.deptNo }" class="dept">
											                       							</li>
											                       						</c:forEach>
											                       					</ul>
											                       				</c:if>
							                       							</li>
							                       						</c:forEach>
							                       					</ul>
							                       				</c:if>
							                       			</li>
								                       </c:forEach>
								                 	</ul>
			                                    </div>
			                                </div>
			                            </div>
			                        </div>
			                        <!-- 부서 셀렉트 박스 끝 -->
								</c:if>
							</div>
				   			<div id='calendar'></div>
						</div>			
						
						<a href="#calDetSch" rel="modal:open" class="calDetSchPop">일정상세 팝업</a>
					</div>
					<!-- ** 코드 작성부 끝 -->
				</div>
				<!-- 메인 컨텐츠 영역 끝! -->
			</div><!-- inner_rt end -->
		</div>
	</main>
</div>

<div id="calDetSch" class="modal">
    <jsp:include page="../pop/calc_sch_det_pop.jsp" />
</div>
<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	var eNo = "${ sessionScope.loginUser.empDivNo }";
	var dNo = "${ sessionScope.loginUser.deptNo }";
	var auth = '${ sessionScope.auth }';
	var admin = '${ sessionScope.loginUser.getAuth() }';
	
	var calcSdate = "";
	var calcEdate = "";
	var nowYear = "";
	var adminDeptNo = "";
	var adminDeptSwitch = false;
	
	var calendar;
	$(function () {
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(4).addClass("on");

		// 캘린더 api
        $("#main_menu_cal").jqxCalendar({width: 218, height: 220});
		
	     // 부서 셀렉트 박스
	        $("#addrSelTree").fancytree({
	            imagePath: "skin-custom/",
	            renderNode: function(event, data) {
	                var node = data.node;
	                if(node.data.cstrender){
	                    var $span = $(node.span);
	                    $span.find("> span.fancytree-title").css({
	                        backgroundImage: "none"
	                    });
	                    $span.find("> span.fancytree-icon").css({
	                        backgroundImage: "none",
	                        display: "none"
	                    });
	                } 
	            },
	            click: function(event, data){
	                var node = data.node;
	                //console.log(node);
	            	//console.log(event);
	            	//console.log(data);
	            	console.log("dd")
	            	console.log(data.targetType)
	            	if(data.targetType == "title"){
	          			var arrCal = calendar.getEvents();
	          			for (var i = 0; i < arrCal.length; i++) {
	        				arrCal[i].remove();
	        			}
	          			
		            	var title = node.title.split("<input")[0];
		            	adminDeptNo = $.parseHTML(node.title)[1].value;
	    	            $(".add_sel_ipt").siblings(".add_sel_opt").slideToggle();
	            		$(".add_sel_ipt").val(title)
	            		adminDeptSwitch = true;
	            		adminDeptCalcList();
		            	
						if($("#soloChk").prop("checked")){
							$("#soloChk").prop("checked", false);
			    			$("#soloChk").trigger("change");
						}
	
						if($("#deptChk").prop("checked")){
							$("#deptChk").prop("checked", false);
			    			$("#deptChk").trigger("change");
						}
						
						if($("#groupChk").prop("checked")){
							$("#groupChk").prop("checked", false);
			    			$("#groupChk").trigger("change");
						}
						if($("#compChk").prop("checked")){
							$("#compChk").prop("checked", false);
			    			$("#compChk").trigger("change");
						}
	            	}
					
	            }
	        });
	        $(".fancytree-container").addClass("fancytree-connectors");
	
	        $(".add_sel_ipt").on("click", function(){
	            $(this).siblings(".add_sel_opt").slideToggle();
	        });
	        
	        
	        // 캘린더 기본 세팅
			var calendarEl = document.getElementById('calendar');
			calendar = new FullCalendar.Calendar(calendarEl, {
				plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
			    editable: true,
			    locale: 'ko',
			    eventClick: function(info) { // 일정 클릭
					/* 
					console.log(info.view.description);
					console.log('Evㅇent: ' + info.event.title);
					console.log('Coordinates: ' + info.jsEvent.pageX + ',' + info.jsEvent.pageY);
					console.log('View: ' + info.view.type);
					 */
					 
					 
					 
					// 상세 보기 팝업 open
					if(info.event.extendedProps.detail == undefined){
						console.log(info.event.extendedProps.id); // cal_no
						console.log(info.event.extendedProps.type); // cal_no
						selectDetCalOne(info.event.extendedProps.id, info.event.extendedProps.type);
					}

					if(info.event.extendedProps.type == "admin" && info.event.extendedProps.detail == "dept"){

						selectDetCalOne(info.event.extendedProps.id, info.event.extendedProps.detail);
					}
			
			   }, eventDrop: function(info) {
				   console.log(info);
				   console.log(info.delta.days);
				   console.log(info.event.title);
				   console.log(info.event.start.toISOString());
				   console.log(info.event.extendedProps.id);
				   updateCalcDrag(info.event.extendedProps.id, info.delta.days)
			   /*  if (!confirm("Are you sure about this change?")) {
			      info.revert();
			    } */
			  },
			  eventLimit: true
			/* ,
			   events : function(info){
				   calcSdate = getFormatDate(info.start);
				   calcEdate = getFormatDate(info.end);
				   console.log("Dd");
				   console.log(calcSdate);
				   console.log(calcEdate);
			   } */
			});
			
			
  	 		calendar.render();
			GetCalendarDateRange();
  	 		
  	  	    calendar.setOption('height', $(window).innerHeight() - 70);
  			
  			// view 현재 날짜
  			function GetCalendarDateRange() {
  				
  		        var view = calendar.view.dayTable.daySeries.dates;
  		        
  		      	calcSdate = getFormatDate(view[0])
  		      	calcEdate = getFormatDate(view[view.length - 1]);
				nowYear = calendar.view.title.substring(0, 4);
				
  		    }
  		 	calendar.on('dateClick', function(info) {
  		 		
  		 		if(admin != "ADMIN"){
  	  		 		$("#calcNewTit").text()
  	  	  			$("#newAllYn").prop("checked", true);
  	  	  			$("#newAllYn").trigger("change");
  	  	  		 	$("#newAllDay").val(info.dateStr.replace(/-/gi, '/'));
  	  	  		  	$("#calNewSchPop").modal();
  		 		}
  	  		});

  	 		
  /* 	  // 일정 삽입
  	   var arr = [ {'title':'evt4', 'start':'2020-01-28', 'end':'2020-01-30'}];
  	  
  	  calendar.addEvent( arr[0]);
  	  // date 클릭시 ,
  	  calendar.on('dateClick', function(info) {
  		  console.log('clicked on ' + info.dateStr);
  		  console.log('clicked on ' + info.dateStr);
  		});
  	  
  	  //- getEvents() 메소드를 통해 calendar 객체가 갖고있는 이벤트를 모두 배열 타입으로 가져올 수 있다.
  	  var arrCal = calendar.getEvents();    
  	  // alert(arrCal[0].title);
  	  $.each( arrCal, function(index, item){ 
  		  console.log(arrCal[index].title); 
  		});
  	  
  	   */
  	   
  	   // calendar view 변할 때 ajax 재호출
	  	 $('.fc-prev-button').on("click", function(){
	  		GetCalendarDateRange();
  			var arrCal = calendar.getEvents();
  			for (var i = 0; i < arrCal.length; i++) {
				arrCal[i].remove();
			}
			
			if(!adminDeptSwitch){
				$("#soloChk").trigger("change");
				$("#deptChk").trigger("change");
				$("#groupChk").trigger("change");
				$("#compChk").trigger("change");
			}else{
        		adminDeptCalcList();
			}
		});
	
	  	$('.fc-next-button').on("click", function(){
	  		GetCalendarDateRange();
  			var arrCal = calendar.getEvents();
  			for (var i = 0; i < arrCal.length; i++) {
				arrCal[i].remove();
			}

			if(!adminDeptSwitch){
				$("#soloChk").trigger("change");
				$("#deptChk").trigger("change");
				$("#groupChk").trigger("change");
				$("#compChk").trigger("change");
			}else{
        		adminDeptCalcList();
			}
	  	});

	  	$(".fc-today-button").on("click", function() {
	  		GetCalendarDateRange();
				var arrCal = calendar.getEvents();
				for (var i = 0; i < arrCal.length; i++) {
				arrCal[i].remove();
			}
			if(!adminDeptSwitch){
				$("#soloChk").trigger("change");
				$("#deptChk").trigger("change");
				$("#groupChk").trigger("change");
				$("#compChk").trigger("change");
			}else{
        		adminDeptCalcList();
			}
	  	});

	  	
 		// 모달 열기 전 데이트 피커 재 생성
 		$('#calNewSchPop').on($.modal.BEFORE_OPEN, function(event, modal) {
 	        datepickerAll();
 	        datepickerRange();
 	        
 		});
  	   
  	   // 캘린더 일정 출력
  	   $("#soloChk").on("change", function(){
  		  if($(this).prop("checked")){
   			  var arrCal = calendar.getEvents();
   			  for (var i = 0; i < arrCal.length; i++) {
  			  	  if(arrCal[i]._def.extendedProps.type == "admin"){
  					  arrCal[i].remove();
  				  }
  			  }
    		  $(".add_sel_ipt").val("");
    		  adminDeptSwitch = false;
  		  	loadSoloCalc();
  		  }else{
  			var arrCal = calendar.getEvents();
  			for (var i = 0; i < arrCal.length; i++) {
				if(arrCal[i].extendedProps.type == "solo"){
					arrCal[i].remove();
				}				
			}
  		  }
  	   });

  	   $("#deptChk").on("change", function(){
  		  if($(this).prop("checked")){
   			  var arrCal = calendar.getEvents();
   			  for (var i = 0; i < arrCal.length; i++) {
  			  	  if(arrCal[i]._def.extendedProps.type == "admin"){
  					  arrCal[i].remove();
  				  }
  			  }
    		  $(".add_sel_ipt").val("");
    		  adminDeptSwitch = false;
  		  	  loadDeptCalc();  
  		  }else{
  			var arrCal = calendar.getEvents();
  			for (var i = 0; i < arrCal.length; i++) {
				if(arrCal[i].extendedProps.type == "dept"){
					arrCal[i].remove();
				}				
			}
  		  }
  	   });
  	   
  	   $("#groupChk").on("change", function(){
   		  if($(this).prop("checked")){
   			  var arrCal = calendar.getEvents();
   			  for (var i = 0; i < arrCal.length; i++) {
  			  	  if(arrCal[i]._def.extendedProps.type == "admin"){
  					  arrCal[i].remove();
  				  }
  			  }
    		  $(".add_sel_ipt").val("");
    		  adminDeptSwitch = false;
   		  	  loadGroupCalc();  
   		  }else{
   			var arrCal = calendar.getEvents();
   			for (var i = 0; i < arrCal.length; i++) {
 				if(arrCal[i].extendedProps.type == "group"){
 					arrCal[i].remove();
 				}				
 			}
   		  }
   	   });

  	   $("#compChk").on("change", function(){
   		  if($(this).prop("checked")){
   			  var arrCal = calendar.getEvents();
   			  for (var i = 0; i < arrCal.length; i++) {
  			  	  if(arrCal[i]._def.extendedProps.type == "admin"){
  					  arrCal[i].remove();
  				  }
  			  }
    		  $(".add_sel_ipt").val("");
    		  adminDeptSwitch = false;
   		  	  loadCompCalc();  
   		  }else{
   			var arrCal = calendar.getEvents();
   			for (var i = 0; i < arrCal.length; i++) {
 				if(arrCal[i].extendedProps.type == "comp"){
 					arrCal[i].remove();
 				}				
 			}
   		  }
   	   });
  	   
  	   // 첫 캘린더 페이지 진입시 일정 노출
  	   if(admin != "ADMIN"){
  	  	   $("#soloChk").trigger("change");
  	  	   $("#deptChk").trigger("change");
  	  	   $("#groupChk").trigger("change");
  	  	   $("#compChk").trigger("change");   
  	   }else{
  		   $(".add_sel_ipt").val("전체보기");
	       	adminDeptNo = 0;
   			adminDeptSwitch = true;
   			adminDeptCalcList();
  	   }
  	   


	});
	
	
	// 개인일정 로딩 ajax
	function loadSoloCalc(){
		$.ajax({
			url:"selectSoloCalcList.ca",
			type:"post",
			data: {
				empDivNo:eNo,
				calcSdate:calcSdate,
				calcEdate:calcEdate
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
					var sdate = getFormatDate(new Date(data.list[i].CAL_SDATE.time));
					if(data.list[i].ALL_YN == 'Y'){
					  	arr = {
					  		   'title':data.list[i].CAL_TITLE,
					  		   'start':sdate,
					           color : "#F5EFD8",
					           extendedProps: {
								               id: data.list[i].CAL_NO,
								               type: "solo"
									           },
					           className: "pointer"
					           };
						
					}else{
						var orginStime = data.list[i].CAL_STIME;
						var orginEtime = data.list[i].CAL_ETIME;
						var stime = "";
						var etime = "";
						if(orginStime != "allday"){
							if(orginStime.split(" ")[1] == "AM"){
								stime = "T" + orginStime.split(" ")[0] + ":00"
							}else{
								stime = "T" + (orginStime.split(" ")[0].split(":")[0]*1 + 12) + ":" + orginStime.split(" ")[0].split(":")[1] + ":00"
							}
						}

						/* if(orginEtime != "allday"){
							if(orginEtime.split(" ")[1] == "AM"){
								stime = "T" + orginEtime.split(" ")[0] + ":00"
							}else{
								stime = "T" + (orginEtime.split(" ")[0].split(":")[0]*1 + 12) + ":" + orginEtime.split(" ")[0].split(":")[1] + ":00"
							}
						} */
						
						var edate = getFormatDate(new Date(data.list[i].CAL_EDATE.time + 86400000));
					  	arr = {
						       'title':data.list[i].CAL_TITLE,
				  			   'start':sdate + stime,
				  			   'end':edate,
					           color : "#F5EFD8",
					           extendedProps: {
									               id: data.list[i].CAL_NO,
									               type: "solo"
									           },
					           className: "pointer"
				          	   };
					}
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
	}

	// 부서일정 로딩 ajax
	function loadDeptCalc(){
		$.ajax({
			url:"selectDeptCalcList.ca",
			type:"post",
			data: {
				deptNo:dNo,
				calcSdate:calcSdate,
				calcEdate:calcEdate
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
					var sdate = getFormatDate(new Date(data.list[i].CAL_SDATE.time));
					if(data.list[i].ALL_YN == 'Y'){
					  	arr = {
					  		   'title':data.list[i].CAL_TITLE,
					  		   'start':sdate,
					           color : "#D3E7D2",
							   editable: false,
					           extendedProps: {
								               id: data.list[i].CAL_NO,
								               type: "dept"
									           },
					           className: "pointer"
					           };
						
					}else{
						var orginStime = data.list[i].CAL_STIME;
						var orginEtime = data.list[i].CAL_ETIME;
						var stime = "";
						var etime = "";
						if(orginStime != "allday"){
							if(orginStime.split(" ")[1] == "AM"){
								stime = "T" + orginStime.split(" ")[0] + ":00"
							}else{
								stime = "T" + (orginStime.split(" ")[0].split(":")[0]*1 + 12) + ":" + orginStime.split(" ")[0].split(":")[1] + ":00"
							}
						}
						
						var edate = getFormatDate(new Date(data.list[i].CAL_EDATE.time + 86400000));
					  	arr = {
						       'title':data.list[i].CAL_TITLE,
				  			   'start':sdate + stime,
				  			   'end':edate,
					           color : "#D3E7D2",
							   editable: false,
					           extendedProps: {
									               id: data.list[i].CAL_NO,
									               type: "dept"
									           },
					           className: "pointer"
				          	   };
					}
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
		

		// 시스템 일정 로딩 : 부재
		$.ajax({
			url:"selectSystemAbsCalcList.ca",
			type:"post",
			data: {
				deptNo:dNo,
				calcSdate:calcSdate,
				calcEdate:calcEdate
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
					// var sdate = getFormatDate(new Date(data.list[i].SDATE.time));
					
					// console.log(data.list[i].SDATE);
					if(data.list[i].ALL_YN == 'Y'){
					  	arr = {
					  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] " + data.list[i].REASON,
					  		   'start':data.list[i].SDATE,
					           color : "#fff",
					           textColor : "#44455b",
							   editable: false,
					           extendedProps: {
								               id: data.list[i].ABS_NO,
								               type: "dept",
								               detail : "abs"
									           }
					           };
						
					}else{
						var orginStime = data.list[i].STIME;
						var orginEtime = data.list[i].ETIME;
						var stime = "";
						var etime = "";
						if(orginStime != "allday"){
							if(orginStime.split(" ")[1] == "AM"){
								stime = "T" + orginStime.split(" ")[0] + ":00"
							}else{
								stime = "T" + (orginStime.split(" ")[0].split(":")[0]*1 + 12) + ":" + orginStime.split(" ")[0].split(":")[1] + ":00"
							}
						}
						
						var edate = getFormatDate(new Date(data.list[i].EDATE.time + 86400000));
					  	arr = {
					  		   'title':"[" + data.list[i].ENAME + data.list[i].JNAME + "] " + data.list[i].REASON,
				  			   'start':data.list[i].SDATE + stime,
				  			   'end':edate,
					           color : "#fff",
					           textColor : "#44455b",
							   editable: false,
					           extendedProps: {
					              			   id: data.list[i].ABS_NO,
								               type: "dept",
								               detail : "abs"
									           }
				          	   };
					}
					
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
		


		// 시스템 일정 로딩 : 연차
		$.ajax({
			url:"selectSystemAnnCalcList.ca",
			type:"post",
			data: {
				deptNo:dNo,
				calcSdate:calcSdate,
				calcEdate:calcEdate
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
		
					
					if(data.list[i].SDATE == data.list[i].EDATE){
						if(data.list[i].TYPE == "AM"){

						  	arr = {
						  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] 오전 반차",
						  		   'start': data.list[i].SDATE,
						           color : "#fff",
						           textColor : "#44455b",
								   editable: false,
						           extendedProps: {
									               id: data.list[i].ANN_NO,
									               type: "dept",
									               detail : "ann"
										           }
						           };
						}else if(data.list[i].TYPE == "PM"){
						  	arr = {
						  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] 오후 반차",
						  		   'start':data.list[i].SDATE,
						           color : "#fff",
						           textColor : "#44455b",
								   editable: false,
						           extendedProps: {
									               id: data.list[i].ANN_NO,
									               type: "dept",
									               detail : "ann"
										           }
						           };
						}else{
						  	arr = {
						  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] 연차",
					  			   'start':data.list[i].SDATE,
						           color : "#fff",
						           textColor : "#44455b",
								   editable: false,
						           extendedProps: {
						              			   id: data.list[i].ANN_NO,
									               type: "dept",
									               detail : "ann"
										           }
					          	   };
						}
						
					}else{
						
					  	arr = {
					  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] 연차",
				  			   'start':data.list[i].SDATE,
				  			   'end':data.list[i].EDATE,
					           color : "#fff",
					           textColor : "#44455b",
							   editable: false,
					           extendedProps: {
					              			   id: data.list[i].ANN_NO,
								               type: "dept",
								               detail : "ann"
									           }
				          	   };
					}
					
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
	}

	// 단체일정 로딩 ajax
	function loadGroupCalc(){
		$.ajax({
			url:"selectGroupCalcList.ca",
			type:"post",
			data: {
				empDivNo:eNo,
				calcSdate:calcSdate,
				calcEdate:calcEdate
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
					var sdate = getFormatDate(new Date(data.list[i].CAL_SDATE.time));
					if(data.list[i].ALL_YN == 'Y'){
					  	arr = {
					  		   'title':data.list[i].CAL_TITLE,
					  		   'start':sdate,
					           color : "#D8EDF5",
							   editable: false,
					           extendedProps: {
								               id: data.list[i].CAL_NO,
								               type: "group"
									           },
					           className: "pointer"
					           };
						
					}else{
						var orginStime = data.list[i].CAL_STIME;
						var orginEtime = data.list[i].CAL_ETIME;
						var stime = "";
						var etime = "";
						if(orginStime != "allday"){
							if(orginStime.split(" ")[1] == "AM"){
								stime = "T" + orginStime.split(" ")[0] + ":00"
							}else{
								stime = "T" + (orginStime.split(" ")[0].split(":")[0]*1 + 12) + ":" + orginStime.split(" ")[0].split(":")[1] + ":00"
							}
						}
						
						var edate = getFormatDate(new Date(data.list[i].CAL_EDATE.time + 86400000));
					  	arr = {
						       'title':data.list[i].CAL_TITLE,
				  			   'start':sdate + stime,
				  			   'end':edate,
					           color : "#D8EDF5",
							   editable: false,
					           extendedProps: {
									               id: data.list[i].CAL_NO,
									               type: "group"
									           },
					           className: "pointer"
				          	   };
					}
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
		

	}
	
	// 회사일정 로딩 ajax
	function loadCompCalc(){
		$.ajax({
			url:"selectCompCalcList.ca",
			type:"post",
			data: {
				calcSdate:calcSdate,
				calcEdate:calcEdate
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
					var sdate = getFormatDate(new Date(data.list[i].CAL_SDATE.time));
					if(data.list[i].ALL_YN == 'Y'){
					  	arr = {
					  		   'title':data.list[i].CAL_TITLE,
					  		   'start':sdate,
					           color : "#EAD8F5",
							   editable: false,
					           extendedProps: {
								               id: data.list[i].CAL_NO,
								               type: "comp"
									           }
					           };
						
					}else{
						var orginStime = data.list[i].CAL_STIME;
						var orginEtime = data.list[i].CAL_ETIME;
						var stime = "";
						var etime = "";
						if(orginStime != "allday"){
							if(orginStime.split(" ")[1] == "AM"){
								stime = "T" + orginStime.split(" ")[0] + ":00"
							}else{
								stime = "T" + (orginStime.split(" ")[0].split(":")[0]*1 + 12) + ":" + orginStime.split(" ")[0].split(":")[1] + ":00"
							}
						}
						
						var edate = getFormatDate(new Date(data.list[i].CAL_EDATE.time + 86400000));
					  	arr = {
						       'title':data.list[i].CAL_TITLE,
				  			   'start':sdate + stime,
				  			   'end':edate,
					           color : "#EAD8F5",
							   editable: false,
					           extendedProps: {
									               id: data.list[i].CAL_NO,
									               type: "comp"
									           }
				          	   };
					}
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
		
		// 회사 휴가 일정
		$.ajax({
			url:"selectSystemVaCalcList.ca",
			type:"post",
			data: {
				calcSdate:calcSdate,
				calcEdate:calcEdate,
				nowYear:nowYear
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
					var sdate = getFormatDate(new Date(data.list[i].SDATE.time));
					var edate = getFormatDate(new Date(data.list[i].EDATE.time + 86400000));
				  	arr = {
				  		   'title':data.list[i].VNAME,
				  		   'start':sdate,
			  			   'end':edate,
				           color : "#fff",
				           textColor : "#44455b",
						   editable: false,
				           extendedProps: {
							               id: data.list[i].VNO,
							               type: "comp",
							               detail: "vac"
								           }
				           };
						
					
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
	}

	function adminDeptCalcList(){

		// 관리자 부서 일정
		$.ajax({
			url:"selectAdmDeptCalcList.ca",
			type:"post",
			data: {
				deptNo:adminDeptNo,
				calcSdate:calcSdate,
				calcEdate:calcEdate
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
					var sdate = getFormatDate(new Date(data.list[i].CAL_SDATE.time));
					if(data.list[i].ALL_YN == 'Y'){
					  	arr = {
					  		   'title': "[" + data.list[i].DNAME+ "] " + data.list[i].CAL_TITLE,
					  		   'start':sdate,
					           color : "#D3E7D2",
							   editable: false,
					           extendedProps: {
								               id: data.list[i].CAL_NO,
								               type: "admin",
								               detail: "dept"
									           },
					           className: "pointer"
					           };
						
					}else{
						var orginStime = data.list[i].CAL_STIME;
						var orginEtime = data.list[i].CAL_ETIME;
						var stime = "";
						var etime = "";
						if(orginStime != "allday"){
							if(orginStime.split(" ")[1] == "AM"){
								stime = "T" + orginStime.split(" ")[0] + ":00"
							}else{
								stime = "T" + (orginStime.split(" ")[0].split(":")[0]*1 + 12) + ":" + orginStime.split(" ")[0].split(":")[1] + ":00"
							}
						}
						
						var edate = getFormatDate(new Date(data.list[i].CAL_EDATE.time + 86400000));
					  	arr = {
					  		   'title': "[" + data.list[i].DNAME+ "] " + data.list[i].CAL_TITLE,
				  			   'start':sdate + stime,
				  			   'end':edate,
					           color : "#D3E7D2",
							   editable: false,
					           extendedProps: {
									               id: data.list[i].CAL_NO,
									               type: "admin",
									               detail: "dept"
									           },
					           className: "pointer"
				          	   };
					}
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});

		// 시스템 일정 로딩 : 부재
		$.ajax({
			url:"selectSystemAbsCalcList.ca",
			type:"post",
			data: {
				deptNo:adminDeptNo,
				calcSdate:calcSdate,
				calcEdate:calcEdate
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
					// var sdate = getFormatDate(new Date(data.list[i].SDATE.time));
					
					// console.log(data.list[i].SDATE);
					if(data.list[i].ALL_YN == 'Y'){
					  	arr = {
					  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] " + data.list[i].REASON,
					  		   'start':data.list[i].SDATE,
					           color : "#fff",
					           textColor : "#44455b",
							   editable: false,
					           extendedProps: {
								               id: data.list[i].ABS_NO,
								               type: "admin",
								               detail : "abs"
									           }
					           };
						
					}else{
						var orginStime = data.list[i].STIME;
						var orginEtime = data.list[i].ETIME;
						var stime = "";
						var etime = "";
						if(orginStime != "allday"){
							if(orginStime.split(" ")[1] == "AM"){
								stime = "T" + orginStime.split(" ")[0] + ":00"
							}else{
								stime = "T" + (orginStime.split(" ")[0].split(":")[0]*1 + 12) + ":" + orginStime.split(" ")[0].split(":")[1] + ":00"
							}
						}
						
						var edate = getFormatDate(new Date(data.list[i].EDATE.time + 86400000));
					  	arr = {
					  		   'title':"[" + data.list[i].ENAME + data.list[i].JNAME + "] " + data.list[i].REASON,
				  			   'start':data.list[i].SDATE + stime,
				  			   'end':edate,
					           color : "#fff",
					           textColor : "#44455b",
							   editable: false,
					           extendedProps: {
					              			   id: data.list[i].ABS_NO,
								               type: "admin",
								               detail : "abs"
									           }
				          	   };
					}
					
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
		


		// 시스템 일정 로딩 : 연차
		$.ajax({
			url:"selectSystemAnnCalcList.ca",
			type:"post",
			data: {
				deptNo:adminDeptNo,
				calcSdate:calcSdate,
				calcEdate:calcEdate
			},
			success:function(data) {
				console.log(data);
				for (var i = 0; i < data.list.length; i++) {
					var arr;
		
					
					if(data.list[i].SDATE == data.list[i].EDATE){
						if(data.list[i].TYPE == "AM"){

						  	arr = {
						  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] 오전 반차",
						  		   'start': data.list[i].SDATE,
						           color : "#fff",
						           textColor : "#44455b",
								   editable: false,
						           extendedProps: {
									               id: data.list[i].ANN_NO,
									               type: "admin",
									               detail : "ann"
										           }
						           };
						}else if(data.list[i].TYPE == "PM"){
						  	arr = {
						  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] 오후 반차",
						  		   'start':data.list[i].SDATE,
						           color : "#fff",
						           textColor : "#44455b",
								   editable: false,
						           extendedProps: {
									               id: data.list[i].ANN_NO,
									               type: "admin",
									               detail : "ann"
										           }
						           };
						}else{
						  	arr = {
						  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] 연차",
					  			   'start':data.list[i].SDATE,
						           color : "#fff",
						           textColor : "#44455b",
								   editable: false,
						           extendedProps: {
						              			   id: data.list[i].ANN_NO,
									               type: "admin",
									               detail : "ann"
										           }
					          	   };
						}
						
					}else{
						
					  	arr = {
					  		   'title':"[" + data.list[i].ENAME + " " + data.list[i].JNAME + "] 연차",
				  			   'start':data.list[i].SDATE,
				  			   'end':data.list[i].EDATE,
					           color : "#fff",
					           textColor : "#44455b",
							   editable: false,
					           extendedProps: {
					              			   id: data.list[i].ANN_NO,
								               type: "admin",
								               detail : "ann"
									           }
				          	   };
					}
					
				  	calendar.addEvent(arr);
				}
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
	}
	
	function selectDetCalOne(val, type){
		console.log("type : "  + type);
		$.ajax({
			url:"selectDetCalOne.ca",
			type:"post",
			data: {
				calNo:val
			},
			success:function(data) {
				console.log(data);
				$("#detCalTypeHidden").val(type);
				
				if(type == "solo"){
					$("#detCalType").text("개인일정");
				}else if(type == "dept"){
					$("#detCalType").text("부서일정");
					$("#detShareTit").text("참석자");
					
					if(data.map.EMP_DIV_NO == eNo){
						$("#detDelBtn").show();
						$("#detUpBtn").show();
					}else{
						if(auth.includes("tm=Y") || auth.includes("clm=Y")){
							$("#detDelBtn").show();
							$("#detUpBtn").hide();
						}else{
							$("#detDelBtn").hide();
							$("#detUpBtn").hide();
						}
					}

				}else if(type == "group"){
					$("#detCalType").text("단체일정");
					$("#detShareTit").text("공유자");

					if(data.map.EMP_DIV_NO == eNo){
						$("#detDelBtn").show();
						$("#detUpBtn").show();
					}else{
						if(auth.includes("tm=Y") || auth.includes("clm=Y")){
							$("#detDelBtn").show();
							$("#detUpBtn").hide();
						}else{
							$("#detDelBtn").hide();
							$("#detUpBtn").hide();
						}
					}
				}else if(type == "comp"){
					$("#detCalType").text("회사일정");
					$("#detShareTit").text("참석자");

					if(data.map.EMP_DIV_NO == eNo){
						$("#detDelBtn").show();
						$("#detUpBtn").show();
					}else{
						if(auth.includes("tm=Y") || auth.includes("clm=Y")){
							$("#detDelBtn").show();
							$("#detUpBtn").hide();
						}else{
							$("#detDelBtn").hide();
							$("#detUpBtn").hide();
						}
					}
				}
				
				// 공통
				$("#detCalNo").val(val);
				
				var dateArea = "";
				dateArea += data.map.SDATE;

				if(data.map.CAL_STIME != null){
					if(data.map.CAL_STIME == "allday"){
						dateArea += "종일";
					}else{
						dateArea += " " + data.map.CAL_STIME;
					}
				}
				
				if(data.map.EDATE != null){
					dateArea += " - " + data.map.EDATE;
				}

				if(data.map.CAL_ETIME != null){
					if(data.map.CAL_ETIME == "allday"){
						dateArea += " 종일";
					}else{
						dateArea += " " + data.map.CAL_ETIME;
					}
				}
				$("#detDateArea").text(dateArea);
				
				$("#detCalTitle").text(data.map.CAL_TITLE);
				if(data.map.CAL_CNT != null){
					$("#detCalCnt").text(data.map.CAL_CNT);
				}else{
					$("#detCalCnt").text(" - ");
				}
				
				if(type != "solo"){
					$("#detCalAssoArea").show();
					var clone = "<tr><td></td><td></td><td></td><td></td></tr>";
					if(data.asso.length != 0){
						console.log(data.asso.length);
						
						for (var i = 0; i < data.asso.length; i++) {
							$("#detCalAssoTbl").append(clone);
							$("#detCalAssoTbl").find("tr:last-child").find("td").eq(0).html("<input type='hidden' value='" + data.asso[i].ENO + "'>" + data.asso[i].EMP_NAME);
							$("#detCalAssoTbl").find("tr:last-child").find("td").eq(1).text(data.asso[i].PNAME + data.asso[i].JNAME);
							$("#detCalAssoTbl").find("tr:last-child").find("td").eq(2).text(data.asso[i].DNAME);
							$("#detCalAssoTbl").find("tr:last-child").find("td").eq(3).text(data.asso[i].EMP_ID + "@" + domain);
						}
							
					}else{
						$("#detCalAssoTbl").append("<tr><td colspan='4'>태그된 사원이 없습니다.</td></tr>");
					}
				}else{
					$("#detCalAssoArea").hide();
				}
				
				// modal open
				$(".calDetSchPop").modal();
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
	}
	
	function getFormatDate(date){
	    var year = date.getFullYear();              
	    var month = (1 + date.getMonth());       
	    month = month >= 10 ? month : '0' + month;  
	    var day = date.getDate();                  
	    day = day >= 10 ? day : '0' + day;          
	    return  year + '-' + month + '-' + day;
	}
	
	function updateCalcDrag(calNo, days){
		$.ajax({
			url:"updateCalcDrag.ca",
			type:"post",
			data: {
				calNo:calNo,
				days:days
			},
			success:function(data) {
				console.log(data);
				alert("일정이 수정되었습니다");
			  	  
			},
			error:function(status) {
				console.log(status);
			}
		});	
	}
	
</script>        
</body>
</html>

</c:if>
<c:if test="${ empty loginUser }">
	<jsp:forward page="../../../index.jsp" />
</c:if>