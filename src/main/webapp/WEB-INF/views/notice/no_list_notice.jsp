<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/notice_menu.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wk.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit tit_rel"><h2>안읽은 알림</h2><button id="readBtn" class="btn_blue tit_abs2">전체 읽음</button></div>
                    
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                       <div class="tbl_wrap">
                            <table class="tbl_ctn">
                            	<colgroup>
                                    <col style="width: *;">
                                    <col style="width: 5%;">
                                </colgroup>
                                <c:forEach items="${ list }" var="i">
                                <jsp:useBean id="now" class="java.util.Date"/>
                                <fmt:parseDate value="${ i.alramDate }" pattern="yy/MM/dd" var="alramDate"/>
                                <fmt:formatDate value="${ now }" pattern="yyyy/MM/dd" var="today"/>
                                <fmt:formatDate value="${ alramDate }" pattern="yyyy/MM/dd" var="alram"/>
                               	<c:set var="d" value="${ fn:split(i.alramDate , ' ') }"/>
                                <c:if test="${ today == alram }">
                                	<c:set var="time" value="${ d[1] } ${ d[2] == '오후' ? 'PM' : 'AM' }" />
                                </c:if>
                                <c:if test="${ today != alram }">
                                	<c:set var="time" value="${ d[0] }"/>
                                </c:if>
                                <tr class="notice_list">
                                    <td class="tit">
	                                	<input type="hidden" class="alramNo" value="${ i.alramNo }" />
                                    	<div class="notice_icon">
                                    		<c:if test="${ i.alramType eq 'MAIL' }">
                                    			<i class="far fa-envelope"></i>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'BOARD' }">
                                    			<i class="fas fa-info"></i>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'CALC' }">
                                    			<i class="far fa-calendar-check"></i>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'EAS' }">
                                    			<i class="far fa-clipboard"></i>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'REPLY' }">
                                    			<i class="far fa-comment-dots"></i>
                                    		</c:if>
                                    	</div>
                                    	<div class="notice_cnt">
                                    		<c:if test="${ i.alramType eq 'MAIL' }">
                                    			<span class="notice_type <c:if test="${ i.readNo ne 0 }">readcheck</c:if>"><a <c:if test="${ i.readNo ne 0 }">class="readcheck"</c:if> href="selectMail.ma">[새 메일] ${ i.alramTitle }</a></span>
                                    			<c:if test="${ i.senderType eq 'IN' }">
                                    			<span class="notice_content <c:if test="${ i.readNo ne 0 }">readcheck</c:if>">${ i.senderName } ${ i.jobName }(으)로부터 '${ i.mailTitle }'</span> 
                                    			</c:if>
                                    			<span class="notice_suffix <c:if test="${ i.readNo ne 0 }">readcheck</c:if>">메일이 도착했습니다.</span> 
                                    			<span class="notice_time">${ time }</span> 
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'BOARD' }">
                                    			<span class="notice_type <c:if test="${ i.readNo ne 0 }">readcheck</c:if>"><a <c:if test="${ i.readNo ne 0 }">class="readcheck"</c:if> href="">[필독 게시물 등록] ${ i.alramTitle }</a></span>
                                    			<span class="notice_content <c:if test="${ i.readNo ne 0 }">readcheck</c:if>">${ i.btitle }</span> <span class="notice_time">${ time }</span>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'CALC' }">
                                    			<span class="notice_type <c:if test="${ i.readNo ne 0 }">readcheck</c:if>"><a <c:if test="${ i.readNo ne 0 }">class="readcheck"</c:if> href="#">[일정 참석] ${ i.alramTitle }</a></span>
                                    			<span class="notice_content <c:if test="${ i.readNo ne 0 }">readcheck</c:if>">${ i.calTitle }</span> <span class="notice_time">${ time }</span>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'EAS' }">
                                    			<span class="notice_type <c:if test="${ i.readNo ne 0 }">readcheck</c:if>"><a <c:if test="${ i.readNo ne 0 }">class="readcheck"</c:if> href="">[결재 알림] ${ i.alramTitle }</a></span>
                                    			<span class="notice_content <c:if test="${ i.readNo ne 0 }">readcheck</c:if>">${ i.easTitle }</span> <span class="notice_time">${ time }</span>
                                    		</c:if>
                                    		<c:if test="${ i.alramType eq 'REPLY' }">
                                    			<span class="notice_type <c:if test="${ i.readNo ne 0 }">readcheck</c:if>"><a <c:if test="${ i.readNo ne 0 }">class="readcheck"</c:if> href="">[댓글 등록] ${ i.alramTitle }</a></span>
                                    			<span class="notice_content <c:if test="${ i.readNo ne 0 }">readcheck</c:if>">${ i.replyBoard }</span> <span class="notice_time">${ time }</span>
                                    		</c:if>
                                    		<!-- <span class="notice_type">[댓글 등록]</span> <span class="notice_content">‘[폼-판매자] YellowGreen Company - 배송완료처리요청'</span> <span class="notice_suffix">댓글이 등록되었습니다.</span><br />
                                    		<span class="notice_time">07:13 PM</span> <span class="notice_name">조문정 사원</span> -->
                                    	</div>
                                   	</td>
                                    <td>
                                    	<div class="notice_xbtn">
											<i class="fas fa-times"></i>
										</div>
									</td>
                                </tr>
                                </c:forEach>
                            <!--  
                                <colgroup>
                                    <col style="width: *;">
                                    <col style="width: 5%;">
                                </colgroup>
                                <tr>
                                    <td class="tit">
                                    	<div class="notice_icon">
                                    		<i class="far fa-comment-dots"></i>
                                    	</div>
                                    	<div class="notice_cnt">
                                    		<span class="notice_type">[댓글 등록]</span> <span class="notice_content">‘[폼-판매자] YellowGreen Company - 배송완료처리요청'</span> <span class="notice_suffix">댓글이 등록되었습니다.</span><br />
                                    		<span class="notice_time">07:13 PM</span> <span class="notice_name">조문정 사원</span>
                                    	</div>
                                   	</td>
                                    <td>
                                    	<div>
											<i class="fas fa-times"></i>
										</div>
									</td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                    	<div class="notice_icon">
	                                    	<i class="far fa-clipboard"></i>
                                    	</div>
                                    	<div class="notice_cnt">
                                    		<span class="notice_type">[댓글 등록]</span> <span class="notice_content">‘[폼-판매자] YellowGreen Company - 배송완료처리요청'</span> <span class="notice_suffix">댓글이 등록되었습니다.</span><br />
                                    		<span class="notice_time">07:13 PM</span> <span class="notice_name">조문정 사원</span>
                                    	</div>
                                   	</td>
                                    <td>
                                    	<div>
											<i class="fas fa-times"></i>
										</div>
									</td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                    	<div class="notice_icon">
	                                    	<i class="far fa-envelope"></i>
                                    	</div>
                                    	<div class="notice_cnt">
                                    		<span class="notice_type">[댓글 등록]</span> <span class="notice_content">‘[폼-판매자] YellowGreen Company - 배송완료처리요청'</span> <span class="notice_suffix">댓글이 등록되었습니다.</span><br />
                                    		<span class="notice_time">07:13 PM</span> <span class="notice_name">조문정 사원</span>
                                    	</div>
                                   	</td>
                                    <td>
                                    	<div>
											<i class="fas fa-times"></i>
										</div>
									</td>
                                </tr>
                                <tr>
                                    <td class="tit">
                                    	<div class="notice_icon">
	                                    	<i class="far fa-calendar-check"></i>
                                    	</div>
                                    	<div class="notice_cnt">
                                    		<span class="notice_type">[댓글 등록]</span> <span class="notice_content">‘[폼-판매자] YellowGreen Company - 배송완료처리요청'</span> <span class="notice_suffix">댓글이 등록되었습니다.</span><br />
                                    		<span class="notice_time">07:13 PM</span> <span class="notice_name">조문정 사원</span>
                                    	</div>
                                   	</td>
                                    <td>
                                    	<div>
											<i class="fas fa-times"></i>
										</div>
									</td>
                                </tr>
                                 -->
                                 <tr>
                                 	<td id="notice-nocnt">알림이 없습니다.</td>
                                 </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        //$("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
        
        $("#notice-nocnt").hide();
        
        if(${ fn:length(list) eq 0 }) {
        	$("#notice-nocnt").show();
        }
    });
    
    $("#readBtn").click(function() {
    	var con = confirm("읽음처리 하시겠습니까 ?");
    	var readArr = [];
    	$(".notice_list").each(function() {
			readArr.push($(this).find('input:hidden').val());
		});
    	var eNo = '${ loginUser.empDivNo }';
    	
    	if(con) {
    		$.ajax({
    			url:"updateRead.no",
    			traditional : true,
    			data: {
    				readArr:readArr,
    				eNo:eNo
    			},
    			success: function(data) {
    				$(".notice_list").remove();
    				$("#notice-nocnt").show();
    			}
    		});
    		
    		readArr = [];
    	}
    });
</script>

</body>
</html>
