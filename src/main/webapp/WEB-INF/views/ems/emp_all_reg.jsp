<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/ems_menu.jsp" />

<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<style>
	input.incorrect {
		border-color:red !important;
	}
	
	.csvinput {
		width: 100%;
	}
</style>
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>사원 일괄 등록</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                       	<div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사용자 일괄 등록</div>                            
                        </div>
                        <div class="main_cnt_list clearfix">
                        	<p>신규 등록할 사용자 정보를 엑셀파일(CSV)로 업로드 하여, 최대 100건까지 일괄 등록할 수 있습니다.<br>
							등록 양식 샘플을 다운로드 받아, 신규 구성원 정보를 등록하세요.&nbsp;&nbsp;<a href="<c:url value='fileDownload.em'/>" class="sample_download">샘플 다운로드</a><br>
							양식에 맞지 않거나, 유효하지 않은 정보는 빨간 글씨로 표시됩니다. 이 경우 상단 메뉴에서 선택 또는 리스트에서 직접 수정한 후 등록하세요.<br>
							관리자가 설정한 비밀번호는 임시비밀번호이며, 사용자가 직접 비밀번호를 변경한 후 이용할 수 있습니다.<br></p>
                        </div>
                        <div class="main_cnt_list clearfix">
                        	<div class="main_cnt_tit">파일 선택</div>
                        	<div class="main_cnt_desc">
                        	<form enctype="multipart/form-data" id="uploadForm">
                        		<input type="file" id="csvfile" name="csvfile" accept=".csv">
                       		</form>
                        	</div>
                        	<div class="main_cnt_desc">
                        		<button class="btn_main" id="find_file_btn" onclick="tbl_show();">찾기</button>
                        	</div>
                        	<div class="accept_btn">
                        		<button id="insertBtn">적용</button>
                        	</div>
                        </div>                       
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic" id="file_table">                  	
                        <div class="tbl_wrap">
                        	<form action="insertCSV.em" method="post" id="insertcsv">
                            <table class="tbl_ctn" id="tbl">
                                <colgroup>
                                    <col style="width: 3%;">
                                    <col style="width: 12%;">
                                    <col style="width: 12%;">
                                    <col style="width: 12%;">
                                    <col style="width: 12%;">
                                    <col style="width: 12%;">
                                    <col style="width: 12%;">
                                    <col style="width: 12%;">
                                    <col style="width: 12%;">
                                </colgroup>
                         		<tr>
                         			<th></th>
                         			<th>사번</th>
                         			<th>이름</th>
                         			<th>아이디</th>
                         			<th>비밀번호</th>
                         			<th>입사일</th>
                         			<th>부서</th>
                         			<th>직급</th>
                         			<th>직책</th>
                         		</tr>
                            </table>
                    </form>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	var path ="${ contextPath }";

    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
       	
        //$("#file_table").hide();
  
    });
    
    function tbl_show(){
    	$("#file_table").show();
    }
    
    $(document).on("change", "#csvfile", function() {
    	var form = new FormData(document.getElementById('uploadForm'));
    	$.ajax({
    		url:"uploadcsv.em",
    		data:form,
    		dataType: 'text',
    		processData: false,
    		contentType: false,
    		type: 'POST',
    		success: function(data) {
    			var result = JSON.parse(data);
		
    			console.log(result.list)
    			
    			var regEnroll = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;
    			
    			var $table = $("#tbl");
    			for(var i = 1; i < result.list.length; i++) {
					var $tr = $("<tr>");
					var $td1 = $("<td><input type='checkbox' class='empcheck'><input type='hidden' value='" + result.list[i][0] + "'>");
					
					var $td2 = $("<td><input class='csvinput' type='text' name='empNo' value='" + result.list[i][1] + "'>");
					var $td3 = $("<td><input class='csvinput' type='text' name='empName' value='" + result.list[i][2] + "'>");
					var $td4 = $("<td><input class='csvinput' type='text' name='empId' value='" + result.list[i][3] + "'>");
					
					var $td5 = $("<td><input class='csvinput' type='text' name='empPwd' value='" + result.list[i][4] + "'>");
					
					if(regEnroll.test(result.list[i][5])) {
						var $td6 = $("<td><input class='csvinput' type='text' name='enrollDate' value='" + result.list[i][5] + "'>");
					} else {
						var $td6 = $("<td><input class='csvinput' type='text' class='incorrect' name='enrollDate' value='" + result.list[i][5]+ "'>");
					}
					
					var $td7;
					for(var j = 0; j < result.deptList.length; j++) {
	    				if(result.deptList[j] === result.list[i][6]) {
							$td7 = $("<td><input class='csvinput' type='text' name='deptName' value='" + result.list[i][6] + "'>");
							break;
	    				} else if(j == result.deptList.length - 1) {
							$td7 = $("<td><input class='csvinput' type='text' class='incorrect' name='deptName' value='" + result.list[i][6] + "'>");
	    				}
					}

					var $td8;
					for(var j = 0; j < result.jobList.length; j++) {
						console.log(result.jobList[j])
						if(result.jobList[j] === result.list[i][7]) {
							$td8 = $("<td><input class='csvinput' type='text' name='jobName' value='" + result.list[i][7] + "'>");
							break;
						} else if (j == result.jobList.length - 1) {
							$td8 = $("<td><input class='csvinput' type='text' class='incorrect' name='jobName' value='" + result.list[i][7] + "'>");
						}
					}
					
					var $td9;
					for(var j = 0; j < result.posList.length; j++) {
						if(result.posList[j] === result.list[i][8]) {
							$td9 = $("<td><input class='csvinput' type='text' name='posName' value='" + result.list[i][8] + "'>");
							break;
						} else if (j == result.posList.length - 1) {
							$td9 = $("<td><input class='csvinput' type='text' class='incorrect' name='posName' value='" + result.list[i][8] + "'>");
						}
					}
					
					
					$tr.append($td1);
					$tr.append($td2);
					$tr.append($td3);
					$tr.append($td4);
					$tr.append($td5);
					$tr.append($td6);
					$tr.append($td7);
					$tr.append($td8);
					$tr.append($td9);
					$table.append($tr);
    			}
    		},
    		error: function() {
    			console.log('error');
    		}
    	});
    });

  	$(document).on("click", ".empcheck", function() {
  		console.log($(this).siblings().val()); 
  	});
    
    $("#insertBtn").click(function() {
    	$("#insertcsv").submit();
    });
</script>
</body>
</html>