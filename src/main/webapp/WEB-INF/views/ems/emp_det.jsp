<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/ems_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<style>
#scroll_area .user_img_wrap{margin-bottom: 20px;}
#scroll_area .user_img_wrap.user_img_none {font-size: 100px;line-height: 100px;}
#scroll_area .user_img_wrap.user_img_has {width: 100px;height: 100px;border-radius: 50%;overflow: hidden;}
#scroll_area .user_img_wrap.user_img_has img{width: 100%;}
</style>
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>상세보기</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                     
                    <div class="main_cnt_list clearfix">
                  
		               <div class="img_area" id="entImgWrap">
	                        <!-- 사용자 이미지 없을때 -->
	                        <div class="user_img_wrap user_img_none">
	                            <i class="fas fa-user-circle"></i>
	                        </div>
	                        <!-- 사용자 이미지 있을때 -->
	                        <div class="user_img_wrap user_img_has">
	                            <img src="" alt="" id="entImg">
	                        </div> 
	                    </div>
                    
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사번</div>
                            <div class="main_cnt_desc">${ emp.empNo }</div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">아이디</div>
                            <div class="main_cnt_desc">${ emp.empId }</div>
                        </div> 
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이름</div>
                            <div class="main_cnt_desc">${ emp.empName }</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">부서</div>
                            <div class="main_cnt_desc">${ emp.deptName }</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직급/직책</div>
                            <div class="main_cnt_desc">${ emp.posName }/${ emp.jobName }</div>
                        </div>                      
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">내선전화</div>
                            <div class="main_cnt_desc">${ emp.extPhone }</div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">휴대전화</div>
                            <div class="main_cnt_desc">${ emp.phone }</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">생일</div>
                            <div class="main_cnt_desc">${ emp.birth }</div>
                        </div>                
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                            <div class="main_cnt_desc">${ emp.address }</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">성별</div>
                            <div class="main_cnt_tit">${ emp.gender == 'M' ? '남' : '여' }</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                            <div class="main_cnt_desc">${ emp.fax }</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">입사일</div>
                            <div class="main_cnt_desc">${ emp.enrollDate }</div>
                        </div>
                        <div class="insert_btn">
                        	<button class="btn_pink" onclick="retire();">퇴사처리</button>
                			<button class="btn_main">수정하기</button>
                   		</div>                                              
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                    <!-- <div style="float:right;">
                        <button class="btn_pink">퇴사처리</button>
                		<button class="btn_main">수정하기</button>
                    </div> -->              
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
		
        if(profile == ""){
			$("#entImgWrap").find(".user_img_has").hide();
			$("#entImgWrap").find(".user_img_none").show();
		}else{
			$("#entImgWrap").find(".user_img_has").show();
			$("#entImgWrap").find(".user_img_none").hide();
			$("#entImg").prop("src", pathPop+"/resources/uploadFiles/"+profile);
		}
        
    });
    
    function retire() {
    	confirm("퇴사처리 하시겠습니까?");
    	location.href="updateEntYn.em?eNo=${emp.empNo}";
    	
    }
</script>
</body>
</html>