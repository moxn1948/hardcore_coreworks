<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/ems_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<style>
tr:not(.tbl_main_tit){cursor: pointer;}
#container .pager_wrap .pager_com.on .buttonnot{color: #fff;}
#container .pager_wrap .pager_com .buttonnot:hover {color: #fff;background-color: #44455B;transition: 0.25s;}
#container .pager_wrap .pager_com .buttonnot {padding: 0;text-align: center;color: #44455B;transition: 0.25s;width: 100%;height: auto;line-height: 37px; border-radius: 0;border: none;transition: 0.25s;}
.tbl_common.tbl_basic .tbl_wrap tr th{text-align: center}
.tbl_common.tbl_basic .tbl_wrap tr td{text-align: center}
.tbl_common.tbl_basic{padding-top: 10px;}
</style>
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>퇴사자 관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        <select name="srchType" id="entSrchType">
	                        <c:choose>
				        		<c:when test="${ param.srchType == '' || param.srchType == null }">
		                            <option value="name">사원명</option>
		                            <option value="email">이메일</option>
		                            <option value="dept">부서</option>
				        		</c:when>
				        		<c:otherwise>
				        			<c:if test="${ param.srchType.equals('name') }">
			                            <option value="name" selected>사원명</option>
			                            <option value="email">이메일</option>
			                            <option value="dept">부서</option>
				        			</c:if>
				        			<c:if test="${ param.srchType.equals('email') }">
			                            <option value="name">사원명</option>
			                            <option value="email" selected>이메일</option>
			                            <option value="dept">부서</option>
				        			</c:if>
				        			<c:if test="${ param.srchType.equals('dept') }">
			                            <option value="name">사원명</option>
			                            <option value="email">이메일</option>
			                            <option value="dept" selected>부서</option>
				        			</c:if>
				        		</c:otherwise>
				        	</c:choose>
	                        </select>
	                        <c:choose>
				        		<c:when test="${ param.keyword == '' || param.keyword == null }">
	                      		  <input type="search" name="keyword" id="entKeyword">
				        		</c:when>
				        		<c:otherwise>
	                      		  <input type="search" name="keyword" id="entKeyword" value="${ param.keyword }">
				        		</c:otherwise>
				        	</c:choose>
	                        <button class="btn_solid_main" onclick="entSrch()">검색</button>
	                                            
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                          	  <colgroup>
                          	  	<col style="width: 8%">
                          	  	<col style="width: *%">
                          	  	<col style="width: 10%">
                          	  	<col style="width: 20%">
                          	  	<col style="width: 20%">
                          	  	<col style="width: 10%">
                          	  	<col style="width: 14%">
                          	  </colgroup>
                                <tr class="tbl_main_tit">
                                    <th>사번</th>
                                    <th>이름</th>
                                    <th>부서</th>
                                    <th>이메일</th>
                                    <th>휴대전화</th>
                                    <th>입사일</th>
                                    <th>퇴사일</th>
                                </tr>
	                              <c:if test="${ pi.getListCount() == 0 }">
	                              	<tr>
	                              		<td colspan="7">결과가 없습니다.</td>
	                              	</tr>
	                              </c:if>
	                              <c:if test="${ pi.getListCount() != 0 }">
	                                <c:forEach var="list" items="${ list }">
	                                <tr onclick="location.href='selectEntOne.em?eno=${ list.EMP_DIV_NO }'">
	                                    <td><c:out value="${ list.EMP_NO }"/></td>
	                                    <td><c:out value="${ list.EMP_NAME }"/></td>
	                                    <td><c:out value="${ list.DEPT_NAME }"/></td>
	                                    <td><c:out value="${ list.EID }"/></td>
	                                    <td><c:out value="${ list.PHONE }"/></td>
	                                    <td><c:out value="${ list.ENROLLDATE }"/></td>
	                                    <td><c:out value="${ list.ENTDATE }"/></td>
	                                </tr>
	                                </c:forEach>
                              	 </c:if>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                       <!-- 페이저 시작 -->
                  <c:if test="${ pi.getListCount() != 0 }">
                  <div class="pager_wrap">
                      <ul class="pager_cnt clearfix">
                      <li class="pager_com pager_arr first"><button class="buttonnot" onclick="entPager(${ pi.getStartPage() });">&#x003C;&#x003C;</button></li>
                      <li class="pager_com pager_arr prev" onclick="entPager(${ pi.getCurrentPage() - 1 });"><button class="buttonnot">&#x003C;</button></li>
                      
                      <c:forEach begin="1" end="${ pi.getMaxPage() }" varStatus="status">
                      		<c:if test="${ status.index == pi.getCurrentPage() }">
                       			<li class="pager_com pager_num on"><button class="buttonnot" onclick="entPager(${status.index});"><c:out value="${status.index}" /></button></li>
                      		</c:if>
                      		<c:if test="${ status.index != pi.getCurrentPage() }">
                       			<li class="pager_com pager_num"><button class="buttonnot" onclick="entPager(${status.index});"><c:out value="${status.index}" /></button></li>
                      		</c:if>
                      </c:forEach>
                      <li class="pager_com pager_arr next"><button class="buttonnot" onclick="entPager(${ pi.getCurrentPage() + 1 });">&#x003E;</button></li>
                      <li class="pager_com pager_arr end"><button class="buttonnot" onclick="entPager(${ pi.getEndPage() });">&#x003E;&#x003E;</button></li>
                      </ul>
                  </div>
                  </c:if>
                  <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	var srchType = '${ param.srchType }';
	var keyword = '${ param.keyword }';
	var endPage = '${ pi.getEndPage() }';
	var empDivNo = '${ loginUser.empDivNo }';
	
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    });

 	function entPager(cp){
 		if(cp == 0){
 			cp = 1;
 		}
 		if(cp > endPage){
 			cp = endPage;
 		}
 		
 		console.log(cp);
 		var action = 'selectEntList.em?&currentPage=' + cp + '&srchType=' + srchType + '&keyword=' + keyword;
 		location.href = action;
 		console.log(cp);
 		
 	}
 	
 	function entSrch(){
 		srchType = $("#entSrchType").val();
 		keyword = $("#entKeyword").val();
 		var action = 'selectEntList.em?srchType=' + srchType + '&keyword=' + keyword;
 		
 		location.href = action;
 	}

 	$("#entKeyword").keydown(function(key) {
 		if (key.keyCode == 13) {
 			entSrch();
 		}
 	});
</script>
</body>
</html>