<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../inc/ems_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
<style>
#scroll_area .user_img_wrap{margin-bottom: 20px;}
#scroll_area .user_img_wrap.user_img_none {font-size: 100px;line-height: 100px;}
#scroll_area .user_img_wrap.user_img_has {width: 100px;height: 100px;border-radius: 50%;overflow: hidden;}
#scroll_area .user_img_wrap.user_img_has img{width: 100%;}
</style>
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>퇴사자 상세보기</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                     
                    <div class="main_cnt_list clearfix">
	                    <div class="img_area" id="entImgWrap">
	                        <!-- 사용자 이미지 없을때 -->
	                        <div class="user_img_wrap user_img_none">
	                            <i class="fas fa-user-circle"></i>
	                        </div>
	                        <!-- 사용자 이미지 있을때 -->
	                        <div class="user_img_wrap user_img_has">
	                            <img src="" alt="" id="entImg">
	                        </div> 
	                    </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">사번</div>
                            <div class="main_cnt_desc">
                           		<c:out value="${ info.EMP_NO }" />
                            </div>
                        </div>
                        
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">아이디</div>
                            <div class="main_cnt_desc">
                           		<c:out value="${ info.EMP_ID }" />
                            </div>
                        </div> 
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이름</div>
                            <div class="main_cnt_desc">
                           		<c:out value="${ info.EMP_NAME }" />
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">부서</div>
                            <div class="main_cnt_desc">
                           		<c:out value="${ info.DEPT_NAME }" />
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직책/직급</div>
                            <div class="main_cnt_desc">
                           		<c:out value="${ info.POS_NAME }" /> / <c:out value="${ info.JOB_NAME }" />
                            </div>
                        </div>                      
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">내선전화</div>
                            <div class="main_cnt_desc">
                            	<c:if test="${ info.EXT_PHONE != null }">
                            		<c:out value="${ info.EXT_PHONE }" />
                            	</c:if>
                            	<c:if test="${ info.FAX == null }">
                            		-
                            	</c:if>
                            </div>
                            
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">휴대전화</div>
                            <div class="main_cnt_desc">
                            	<c:if test="${ info.PHONE != null }">
                            		<c:out value="${ info.PHONE }" />
                            	</c:if>
                            	<c:if test="${ info.FAX == null }">
                            		-
                            	</c:if>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">생일</div>
                            <div class="main_cnt_desc">
                            	<c:if test="${ info.BIRTH != null }">
                            		<c:out value="${ info.BIRTH }" />
                            	</c:if>
                            	<c:if test="${ info.FAX == null }">
                            		-
                            	</c:if>
                            </div>
                        </div>                
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소</div>
                            <div class="main_cnt_desc">
                            	<c:if test="${ info.ADDRESS != null }">
                            		<c:out value="${ info.ADDRESS }" />
                            	</c:if>
                            	<c:if test="${ info.FAX == null }">
                            		-
                            	</c:if>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">성별</div>
                            <div class="main_cnt_desc">
                            	<c:if test="${ info.GEN != null }">
                            		<c:out value="${ info.GEN }" />
                            	</c:if>
                            	<c:if test="${ info.FAX == null }">
                            		-
                            	</c:if>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">팩스번호</div>
                            <div class="main_cnt_desc">
                            	<c:if test="${ info.FAX != null }">
                            		<c:out value="${ info.FAX }" />
                            	</c:if>
                            	<c:if test="${ info.FAX == null }">
                            		-
                            	</c:if>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">입사일</div>
                            <div class="main_cnt_desc">
                            	<c:if test="${ info.ENROLLDATE != null }">
                            		<c:out value="${ info.ENROLLDATE }" />
                            	</c:if>
                            	<c:if test="${ info.ENROLLDATE == null }">
                            		-
                            	</c:if>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">퇴사일</div>
                            <div class="main_cnt_desc">
                            	<c:if test="${ info.ENTDATE != null }">
                            		<c:out value="${ info.ENTDATE }" />
                            	</c:if>
                            	<c:if test="${ info.FAX == null }">
                            		-
                            	</c:if>
                            </div>
                        </div>
                        
                        <div class="insert_btn">
                        	<div class="main_cnt_desc">
                                <!-- 추가 설명 팝업 시작 -->
                                <div class="add_desc_pop">
                                    <a href="javascript: void(0);" class="add_desc_tit"><div class="question_mark"><i class="far fa-question-circle"></i></div></a>
                                    <div class="add_desc_box">
                                        <div class="box_cnt">재입사 시 입사이력이 남으며 복구의 경우 추가 이력없이 퇴사 이전 상태로 돌아갑니다.</div>
                                    </div>
                                </div>
                                <!-- 추가 설명 팝업 끝 -->
                            </div>
                        	<button type="button" class="btn_white recoverBtn">복구</button>
                        	<button type="button" class="btn_blue reEnrollBtn">재입사</button>
                			<button type="button" class="btn_pink delBtn">삭제</button>
                   		</div>                                              
                    </div>
                    <!-- ** 코드 작성부 끝 -->                               
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	var pathPop = "${ contextPath }";
	var profile = '${ info.CHANGE_NAME }';
	var eno = '${ param.eno }';
	
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(1).addClass("on");
        
        if(profile == ""){
			$("#entImgWrap").find(".user_img_has").hide();
			$("#entImgWrap").find(".user_img_none").show();
		}else{
			$("#entImgWrap").find(".user_img_has").show();
			$("#entImgWrap").find(".user_img_none").hide();
			$("#entImg").prop("src", pathPop+"/resources/uploadFiles/"+profile);
		}
        
		$(".recoverBtn").on("click", function(){
			if(confirm("사원을 복구하시겠습니까?")){
				location.href="updateRecover.em?eno=" + eno;
			}
		});

		$(".reEnrollBtn").on("click", function(){
			if(confirm("사원을 재입사 처리하시겠습니까?")){
				location.href="updateReEnroll.em?eno=" + eno;
			}
		});

		$(".delBtn").on("click", function(){
			if(confirm("사원을 삭제하시겠습니까? 삭제 후에는 복구 또는 재입사 처리가 불가능합니다.")){
				location.href="updateDel.em?eno=" + eno;
			}
		});
    });
</script>
</body>
</html>