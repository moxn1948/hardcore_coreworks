jQuery(function($){
    
    // 서브 메뉴 있는 경우
    $(".menu_list.add>a").on("click", function(){
        if($(this).parent(".menu_list.add").hasClass("open")){
          $(this).parent(".menu_list.add").removeClass("open");
          
          if($(this).parents("#menu_area").hasClass("tree_shape")){
            $(this).parent(".menu_list.add").find("a").eq(0).css("background-image", 'url(./resources/images/w_folder.svg)');
          }
        }else{
          $(this).parent(".menu_list.add").removeClass("open");
          $(this).parent(".menu_list.add").siblings(".menu_list").removeClass("open");
          $(this).parent(".menu_list.add").addClass("open");

          if($(this).parents("#menu_area").hasClass("tree_shape")){
            $(this).parent(".menu_list.add").siblings(".menu_list.add").find("a").eq(0).css("background-image", 'url(./resources/images/w_folder.svg)');
            $(this).parent(".menu_list.add").find("a").eq(0).css("background-image", 'url(./resources/images/w_folder_open.svg)');
          }

        }
        $(this).parent(".menu_list.add").siblings(".menu_list").find(".sub_menu_ctn").slideUp();
        $(this).parent(".menu_list.add").find(".sub_menu_ctn ").slideToggle();
    });
  
});