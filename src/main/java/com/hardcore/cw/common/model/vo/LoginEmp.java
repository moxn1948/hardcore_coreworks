package com.hardcore.cw.common.model.vo;

import java.io.Serializable;
import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
 
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class LoginEmp implements Serializable {
	private int empDivNo;
	private int empNo;
	private String empId;
	private String empPwd;
	private String empName;
	private String extPhone;
	private String phone;
	private Date birth;
	private String address;
	private String gender;
	private String fax;
	private Date enrollDate;
	private Date entDate;
	private int jobNo;
	private int posNo;
	private int deptNo;
	private String auth;
	private String entYn;
	private String status;
	private String jobName;
	private String posName;
	private String deptName;
	private String changeName;
}
