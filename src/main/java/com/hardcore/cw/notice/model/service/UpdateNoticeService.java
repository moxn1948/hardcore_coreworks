package com.hardcore.cw.notice.model.service;

public interface UpdateNoticeService {

	void updateAlramStatus(int alramNo);

	void updateAllAlramStatus(int eNo);

	void updateNoticeToRead(int nNo, int eNo);

	void updateRead(int[] readArr, int eNo);

}
