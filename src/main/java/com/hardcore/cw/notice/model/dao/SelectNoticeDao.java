package com.hardcore.cw.notice.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.notice.model.vo.SearchAlram;

public interface SelectNoticeDao {
 
	List<SearchAlram> selectAllNotice(SqlSessionTemplate sqlSession, int i);

	int selectUnreadCount(SqlSessionTemplate sqlSession, int eNo);

	List<SearchAlram> selectNotice(SqlSessionTemplate sqlSession, int eNo);

	int selectAlramCount(SqlSessionTemplate sqlSession, int eNo);

	List<SearchAlram> selectUnreadNotice(SqlSessionTemplate sqlSession, int eNo);

}
