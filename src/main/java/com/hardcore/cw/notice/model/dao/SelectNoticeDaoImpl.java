package com.hardcore.cw.notice.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.notice.model.vo.SearchAlram;

@Repository
public class SelectNoticeDaoImpl implements SelectNoticeDao {

	@Override 
	public List<SearchAlram> selectAllNotice(SqlSessionTemplate sqlSession, int i) {
		return sqlSession.selectList("Notice.selectAllList", i);
	}

	@Override
	public int selectUnreadCount(SqlSessionTemplate sqlSession, int eNo) {
		return sqlSession.selectOne("Notice.selectUnreadCount", eNo);
	}

	@Override
	public List<SearchAlram> selectNotice(SqlSessionTemplate sqlSession, int eNo) {
		return sqlSession.selectList("Notice.selectNotice", eNo);
	}

	@Override
	public int selectAlramCount(SqlSessionTemplate sqlSession, int eNo) {
		return sqlSession.selectOne("Notice.selectAlramCount", eNo);
	}

	@Override
	public List<SearchAlram> selectUnreadNotice(SqlSessionTemplate sqlSession, int eNo) {
		return sqlSession.selectList("Notice.selectUnreadList", eNo);
	}

}
