package com.hardcore.cw.notice.model.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString 
public class SearchAlram implements Serializable {
	private int alramNo;
	private String alramType;
	private String alramDate;
	private String alramTitle;
	private int empDivNo;
	private String status;
	private int easNo;
	private int calNo;
	private int replyNo;
	private int bno;
	private int mailNo;
	private String easTitle;
	private String emergencyYn;
	private String easType;
	private String procState;
	private String replyBoard;
	private String btitle;
	private String calTitle;
	private String mailTitle;
	private String senderType;
	private String senderName;
	private String senderId;
	private String jobName;
	private String outSend;
	private int readNo;
}
