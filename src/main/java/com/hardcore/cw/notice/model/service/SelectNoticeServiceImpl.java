package com.hardcore.cw.notice.model.service;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.notice.model.dao.SelectNoticeDao;
import com.hardcore.cw.notice.model.vo.SearchAlram;

@Service
public class SelectNoticeServiceImpl implements SelectNoticeService {

	@Autowired 
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private SelectNoticeDao snd;
	
	@Override
	public List<SearchAlram> selectAllNotice(int i) {
		return snd.selectAllNotice(sqlSession, i);
	}

	public int selectUnreadCount(int eNo) {
		return snd.selectUnreadCount(sqlSession, eNo);
	}

	@Override
	public List<SearchAlram> selectNotice(int eNo) {
		return snd.selectNotice(sqlSession, eNo);
	}

	@Override
	public int selectAlramCount(int eNo) {
		return snd.selectAlramCount(sqlSession, eNo);
	}
	
	@Override
	public List<SearchAlram> selectUnreadNotice(int eNo) {
		return snd.selectUnreadNotice(sqlSession, eNo);
	}

}
