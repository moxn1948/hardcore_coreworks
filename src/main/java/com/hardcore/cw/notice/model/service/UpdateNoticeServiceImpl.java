package com.hardcore.cw.notice.model.service;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.notice.model.dao.UpdateNoticeDao;

@Service
public class UpdateNoticeServiceImpl implements UpdateNoticeService {

	@Autowired
	private UpdateNoticeDao und;
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Override
	public void updateAlramStatus(int alramNo) {
		und.updateAlramStatus(sqlSession, alramNo);
	}

	@Override
	public void updateAllAlramStatus(int eNo) {
		und.updateAllAlramStatus(sqlSession, eNo);
	}

	@Override
	public void updateNoticeToRead(int nNo, int eNo) {
		und.updateNoticeToRead(sqlSession, nNo, eNo);
	}

	@Override
	public void updateRead(int[] readArr, int eNo) {
		for(int i : readArr) {
			und.updateNoticeToRead(sqlSession, i, eNo);
		}
	}

}
