package com.hardcore.cw.notice.model.service;

import java.util.List;

import com.hardcore.cw.notice.model.vo.SearchAlram;

public interface SelectNoticeService {

	List<SearchAlram> selectAllNotice(int i);
 
	public int selectUnreadCount(int eNo);

	List<SearchAlram> selectNotice(int eNo);

	int selectAlramCount(int eNo);

	List<SearchAlram> selectUnreadNotice(int eNo);
}
