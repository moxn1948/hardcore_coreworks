package com.hardcore.cw.notice.model.dao;

import org.mybatis.spring.SqlSessionTemplate;

public interface UpdateNoticeDao {

	void updateAlramStatus(SqlSessionTemplate sqlSession, int alramNo);

	void updateAllAlramStatus(SqlSessionTemplate sqlSession, int eNo);

	void updateNoticeToRead(SqlSessionTemplate sqlSession, int nNo, int eNo);

}
