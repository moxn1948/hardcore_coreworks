package com.hardcore.cw.notice.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Alram implements java.io.Serializable{
	private int alramNo;
	private String type;
	private Date alramDate;
	private String alramTitle;
	private int empDivNo;
	private String status;
	private int easNo;
	private int calNo;
	private int replyNo;
	private int bno;
	private int mailNo;
}
