package com.hardcore.cw.notice.model.dao;

import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UpdateNoticeDaoImpl implements UpdateNoticeDao {

	@Override
	public void updateAlramStatus(SqlSessionTemplate sqlSession, int alramNo) {
		sqlSession.update("Notice.updateAlramStatus", alramNo);
	}

	@Override
	public void updateAllAlramStatus(SqlSessionTemplate sqlSession, int eNo) {
		sqlSession.update("Notice.updateAllAlramStatus", eNo);
	}

	@Override
	public void updateNoticeToRead(SqlSessionTemplate sqlSession, int nNo, int eNo) {
		Map<String, Object> map = new HashMap<>();
		map.put("nNo", nNo);
		map.put("eNo", eNo);
		
		sqlSession.update("Notice.updateNoticeToRead", map);
	}

}
