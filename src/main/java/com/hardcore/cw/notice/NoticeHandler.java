package com.hardcore.cw.notice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hardcore.cw.notice.model.service.SelectNoticeService;
import com.hardcore.cw.notice.model.vo.SearchAlram;

import net.sf.json.JSONArray;

public class NoticeHandler extends TextWebSocketHandler {

	private final Logger logger = LoggerFactory.getLogger(NoticeHandler.class);

	static Boolean runCheck = false;

	private List<WebSocketSession> sessionList = new ArrayList<WebSocketSession>();

	@Autowired
	private SelectNoticeService sns;

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		sessionList.add(session);
		runCheck = false;
	}

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		int eNo = Integer.parseInt(message.getPayload());

		if (!runCheck) {
			TimerTask task = new TimerTask() {

				@Override
				public void run() {
					int count = sns.selectAlramCount(eNo);
					List<SearchAlram> list = sns.selectNotice(eNo);
					ObjectMapper mapper = new ObjectMapper();
					try {
						session.sendMessage(new TextMessage(String.valueOf(count)));
						session.sendMessage(new TextMessage(mapper.writeValueAsBytes(list)));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			};
			runCheck = true;
			Timer timer = new Timer(true);
			timer.scheduleAtFixedRate(task, 0, 1 * 1000);

		}

		
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {

	}

}
