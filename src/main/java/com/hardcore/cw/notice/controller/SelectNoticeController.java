package com.hardcore.cw.notice.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.notice.model.service.SelectNoticeService;
import com.hardcore.cw.notice.model.vo.SearchAlram;

@Controller
public class SelectNoticeController {
 
	@Autowired
	private SelectNoticeService sns;
	
	@RequestMapping("selectNotice.no")
	public ModelAndView selectNotice(ModelAndView mv, HttpServletRequest request) {
		
		LoginEmp e = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		List<SearchAlram> list = sns.selectAllNotice(e.getEmpDivNo());
		
		
		mv.addObject("list", list);
		mv.setViewName("list_notice");
		return mv;
	}
	
	@RequestMapping("selectUnreadNotice.no") 
	public ModelAndView selectUnreadNotice(ModelAndView mv, HttpServletRequest request) {
		LoginEmp e = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		List<SearchAlram> list = sns.selectUnreadNotice(e.getEmpDivNo());
		
		mv.addObject("list", list);
		mv.setViewName("no_list_notice");
		return mv;
	}
}
