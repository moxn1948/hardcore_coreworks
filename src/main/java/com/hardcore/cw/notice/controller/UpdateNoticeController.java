package com.hardcore.cw.notice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.notice.model.service.UpdateNoticeService;

@Controller
public class UpdateNoticeController {

	@Autowired
	private UpdateNoticeService uns;
	
	@RequestMapping("updateAlramStatus.no")
	public ModelAndView updateAlramStatus(ModelAndView mv, int alramNo) {
		
		uns.updateAlramStatus(alramNo);
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("updateAllAlramStatus.no")
	public ModelAndView updateAllAlramStatus(ModelAndView mv, int eNo) {
		uns.updateAllAlramStatus(eNo);
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("updateNoticeToRead.no")
	public ModelAndView updateNoticeToRead(ModelAndView mv, int nNo, int eNo) {
		uns.updateNoticeToRead(nNo, eNo);
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("updateRead.no")
	public ModelAndView updateRead(ModelAndView mv, int[] readArr, int eNo) {
		uns.updateRead(readArr, eNo);
		
		mv.setViewName("jsonView");
		return mv;
	}
}
