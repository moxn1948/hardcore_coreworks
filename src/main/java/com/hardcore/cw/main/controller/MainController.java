package com.hardcore.cw.main.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.main.model.service.MainService;

@Controller
public class MainController {

	@Autowired
	private AddrMenuService ams;
	@Autowired
	private MainService ms;

	@GetMapping("main.main")
	public ModelAndView addrMain(ModelAndView mv, String eno, String auth, String dno) {
		int empDivNo = Integer.parseInt(eno);
		int deptNo = Integer.parseInt(dno);
		
		
		System.out.println("empDivNo : " + empDivNo);
		System.out.println("auth : " + auth);
		
		int nonReadAlramCount = 0;
		List<HashMap<String, Object>> easList = null;
		List<HashMap<String, Object>> mailList = null;
		List<HashMap<String, Object>> allList = null;
		List<HashMap<String, Object>> teamList = null;
		List<HashMap<String, Object>> noticeList = null;

		nonReadAlramCount = ms.selectNonReadAlramCount(empDivNo);
		if(!auth.equals("ADMIN")) {
			mailList = ms.selectMailList(empDivNo);
			allList = ms.selectBoardAllList(empDivNo);
			noticeList = ms.selectNoticeList(empDivNo);
			easList = ms.selectEasList(empDivNo);
			teamList = ms.selectBoardTeamList(deptNo);
			
			
			System.out.println("easList : " + easList);
			System.out.println("mailList : " + mailList);
			System.out.println("allList : " + allList);
			System.out.println("teamList : " + teamList);
			System.out.println("noticeList : " + noticeList);
			
		}
		
		// 사내 부서 구조 조회
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();
		
		if(deptList != null && empList != null) {
			mv.addObject("nonReadAlramCount", nonReadAlramCount);
			mv.addObject("easList", easList);
			mv.addObject("deptList", deptList);
			mv.addObject("empList", empList);
			mv.addObject("mailList", mailList);
			mv.addObject("allList", allList);
			mv.addObject("teamList", teamList);
			mv.addObject("noticeList", noticeList);
			
			mv.setViewName("main");
			
			
		}else {
			mv.addObject("msg", "메인 로딩에 실패했습니다.");
			mv.addObject("view", "index.jsp");
			mv.setViewName("../common/error/addrError");
		}
		
		return mv;
	}
	
}
