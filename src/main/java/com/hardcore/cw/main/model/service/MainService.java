package com.hardcore.cw.main.model.service;

import java.util.HashMap;
import java.util.List;

public interface MainService {

	int selectNonReadAlramCount(int empDivNo);

	List<HashMap<String, Object>> selectEasList(int empDivNo);

	List<HashMap<String, Object>> selectMailList(int empDivNo);

	List<HashMap<String, Object>> selectBoardAllList(int empDivNo);

	List<HashMap<String, Object>> selectBoardTeamList(int empDivNo);

	List<HashMap<String, Object>> selectNoticeList(int empDivNo);

}
