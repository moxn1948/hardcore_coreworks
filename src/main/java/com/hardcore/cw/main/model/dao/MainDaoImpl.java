package com.hardcore.cw.main.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MainDaoImpl implements MainDao{

	@Override
	public int selectNonReadAlramCount(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.selectOne("Main.selectNonReadAlramCount", empDivNo);
	}

	@Override
	public List<HashMap<String, Object>> selectEasList(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.selectList("Main.selectEasList", empDivNo);
	}

	@Override
	public List<HashMap<String, Object>> selectMailList(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.selectList("Main.selectMailList", empDivNo);
	}

	@Override
	public List<HashMap<String, Object>> selectBoardAllList(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.selectList("Main.selectBoardAllList", empDivNo);
	}

	@Override
	public List<HashMap<String, Object>> selectBoardTeamList(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.selectList("Main.selectBoardTeamList", empDivNo);
	}

	@Override
	public List<HashMap<String, Object>> selectNoticeList(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.selectList("Main.selectNoticeList", empDivNo);
	}

}
