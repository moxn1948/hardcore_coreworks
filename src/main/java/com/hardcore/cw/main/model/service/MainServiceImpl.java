package com.hardcore.cw.main.model.service;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.main.model.dao.MainDao;

@Service
public class MainServiceImpl implements MainService{

	@Autowired
	private MainDao md;
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Override
	public int selectNonReadAlramCount(int empDivNo) {
		
		return md.selectNonReadAlramCount(sqlSession, empDivNo);
	}
	@Override
	public List<HashMap<String, Object>> selectEasList(int empDivNo) {

		return md.selectEasList(sqlSession, empDivNo);
	}
	@Override
	public List<HashMap<String, Object>> selectMailList(int empDivNo) {

		return md.selectMailList(sqlSession, empDivNo);
	}
	@Override
	public List<HashMap<String, Object>> selectBoardAllList(int empDivNo) {

		return md.selectBoardAllList(sqlSession, empDivNo);
	}
	@Override
	public List<HashMap<String, Object>> selectBoardTeamList(int empDivNo) {

		return md.selectBoardTeamList(sqlSession, empDivNo);
	}
	@Override
	public List<HashMap<String, Object>> selectNoticeList(int empDivNo) {

		return md.selectNoticeList(sqlSession, empDivNo);
	}
	
}
