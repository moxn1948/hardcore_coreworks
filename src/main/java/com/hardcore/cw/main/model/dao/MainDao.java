package com.hardcore.cw.main.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

public interface MainDao {

	int selectNonReadAlramCount(SqlSessionTemplate sqlSession, int empDivNo);

	List<HashMap<String, Object>> selectEasList(SqlSessionTemplate sqlSession, int empDivNo);

	List<HashMap<String, Object>> selectMailList(SqlSessionTemplate sqlSession, int empDivNo);

	List<HashMap<String, Object>> selectBoardAllList(SqlSessionTemplate sqlSession, int empDivNo);

	List<HashMap<String, Object>> selectBoardTeamList(SqlSessionTemplate sqlSession, int empDivNo);

	List<HashMap<String, Object>> selectNoticeList(SqlSessionTemplate sqlSession, int empDivNo);

}
