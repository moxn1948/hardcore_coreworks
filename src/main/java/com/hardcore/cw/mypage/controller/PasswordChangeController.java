package com.hardcore.cw.mypage.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.mypage.model.service.MyPageService;

@Controller
@SessionAttributes("loginUser")
public class PasswordChangeController {
	private final MyPageService ms;
	private final BCryptPasswordEncoder passwordEncoder;

	@Autowired
	public PasswordChangeController(MyPageService ms, BCryptPasswordEncoder passwordEncoder) {
		this.ms = ms;
		this.passwordEncoder = passwordEncoder;
	}
	
	@PostMapping("updatePwd.mp")
	public void updatePassword(HttpServletRequest request, HttpServletResponse response) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		ObjectMapper mapper = new ObjectMapper();
		
		response.setCharacterEncoding("UTF-8");
		
		String newPwd = passwordEncoder.encode(request.getParameter("new_password"));

		Map<String, Object> map = new HashMap<>();
		map.put("emp", emp.getEmpDivNo());
		map.put("pwd", newPwd);
		
		ms.updatePassword(map);
		String message = "success";
		try {
			response.getWriter().println(mapper.writeValueAsString(message));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
