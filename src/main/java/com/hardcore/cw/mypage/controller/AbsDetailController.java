package com.hardcore.cw.mypage.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.mypage.model.service.MyPageService;
import com.hardcore.cw.mypage.model.vo.Absence;

@SessionAttributes("loginUser")
@Controller
public class AbsDetailController {
	private final MyPageService ms;

	public AbsDetailController(MyPageService ms) {
		this.ms = ms;
	}
	
	@PostMapping("absDetail.mp")
	public void detailAbs(Absence a, HttpServletResponse response) {
		a = ms.selectAbsOne(a);
		
		ObjectMapper mapper = new ObjectMapper();
		
		response.setCharacterEncoding("UTF-8");
		
		try {
			response.getWriter().println(mapper.writeValueAsString(a));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
