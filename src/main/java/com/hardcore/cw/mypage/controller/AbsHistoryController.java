package com.hardcore.cw.mypage.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.EasHistory;
import com.hardcore.cw.mypage.model.service.MyPageService;
import com.hardcore.cw.mypage.model.vo.Absence;

@SessionAttributes("loginUser")
@Controller
public class AbsHistoryController {
	private final MyPageService ms;

	public AbsHistoryController(MyPageService ms) {
		this.ms = ms;
	}
	
	@PostMapping("absHistory.mp")
	public void absHistoryList(Absence a, HttpServletResponse response) {
		ObjectMapper mapper = new ObjectMapper();
		
		// 특정 부재의 정보 조회(FRONT에서 가져오기엔 너무 번거로움)
		a = ms.selectAbsOne(a);
		
		System.out.println(a);
		
//		String absSdate = String.valueOf(a.getAbsSdate());
//		String absStime = a.getAbsStime();
//		
//		String absEdate = String.valueOf(a.getAbsEdate());
//		String absEtime = a.getAbsEtime();
		
		/* 부재자 설정기간동안 들어오는 결재 중 대리결재자가 결재한 결재내역을 보여주기 */
		
		// 일단 오전오후를 hh24로 변경 -> 계산을 목적
		
//		long startMili = dateMiliTime(absSdate, absStime);
//		long endMili = dateMiliTime(absEdate, absEtime);
		
		// 위에 얻은 가공날짜들 사이에 결재 시간이 껴있으면서 대결자번호가 설정한 대결자와 일치한 내역만 뽑아오기(정보는 전부 다)
		ArrayList<EasHistory> easHistoryList = ms.selectEasHistoryList(a); // 1차
		
		//ArrayList<EasHistory> easHistoryListReal = new ArrayList<>();
		ArrayList<DocDetail> docDetailList = new ArrayList<>();
		
		HashMap<String, Object> tempMap = new HashMap<>();
		
//		if(!ObjectUtils.isEmpty(easHistoryList)) {
//			for (EasHistory easHistory : easHistoryList) {
//				long date = dateMiliTime(String.valueOf(easHistory.getProcDate()), easHistory.getProcTime());
//				
//				if(startMili <= date && endMili >= date) {
//					easHistoryListReal.add(easHistory);
//				}
//			}
//		} 
		
		tempMap.put("list", easHistoryList);
		
		docDetailList = ms.selectDocDetailList(tempMap);
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("easHistoryList", easHistoryList);
		map.put("docDetailList", docDetailList);
		
		response.setCharacterEncoding("UTF-8");
		
		// 해당 정보 내보내기
		try {
			response.getWriter().println(mapper.writeValueAsString(map));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// 날짜와 시간을 넣어서 밀리세컨즈값 나오게 함
	private long dateMiliTime(String date, String time) {
		
	String dateArr[] = date.split("-");
			
		// 시 추출
		String hh = time.substring(0, 2);
			
		// 분 추출
		String mm = time.substring(3, 5);
			
		if(time.substring(6).equals("PM") && Integer.parseInt(hh) >= 1 && Integer.parseInt(hh) <= 11 ) {
			hh = String.valueOf(Integer.parseInt(hh) + 12);
		}
			
		time = hh + ":" + mm;
		String timeArr[] = time.split(":");
			
		int year = Integer.parseInt(dateArr[0]);
		int month = Integer.parseInt(dateArr[1]);
		int day = Integer.parseInt(dateArr[2]);
		int hour = Integer.parseInt(timeArr[0]);
		int min = Integer.parseInt(timeArr[1]);
			
		DateTime startTime = new DateTime(year, month, day, hour, min);
		
		return startTime.getMillis(); // 시작 순수시간 -> 이걸로 비교함
	}
}
