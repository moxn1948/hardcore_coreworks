package com.hardcore.cw.mypage.controller;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.mypage.model.service.MyPageService;

@Controller
@SessionAttributes("loginUser")
public class InfomationUpdate {
	private final MyPageService ms;

	public InfomationUpdate(MyPageService ms) {
		this.ms = ms;
	}
	
	@PostMapping("updateInfo.mp")
	public String updateInfomation(Model m, Employee emp, HttpServletRequest request, MultipartHttpServletRequest mtfRequest) {
		LoginEmp sessionEmp = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		MultipartFile file = mtfRequest.getFile("attachments");
		emp.setEmpDivNo(sessionEmp.getEmpDivNo());
		
		if(!file.isEmpty()) {
			String root = mtfRequest.getSession().getServletContext().getRealPath("resources");
			String filePath = root + "\\uploadFiles";
			
			if(!file.getOriginalFilename().equals("")) {
				Attachment at = new Attachment();
				
				// 파일명 변경
				
				// 파일명 추출
				String originFileName = file.getOriginalFilename();
				// 확장자 분리
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				// 랜덤문자열 추출
				String changeName = UUID.randomUUID().toString().replace("-", "");
				
				try {
					file.transferTo(new File(filePath + "\\" + changeName + ext));
					
					// 첨부파일 전 이름, 후 이름, 경로, 타입 
					at.setOriginName(originFileName);
					at.setChangeName(changeName + ext);
					at.setFilePath(filePath);
					at.setEmpDivNo(sessionEmp.getEmpDivNo());
					
					// 내 프로필사진 조회
					Attachment myAtt = ms.selectMyProfile(sessionEmp);
					
					// 있으면 수정, 없으면 삽입
					if(ObjectUtils.isEmpty(myAtt)) {
						ms.insertAttachment(at);
					} else {
						at.setFileNo(myAtt.getFileNo());
						ms.updateAttachment(at);
					}
					
					myAtt = ms.selectMyProfile(sessionEmp);
					
					m.addAttribute("att", myAtt);
					
				} catch (IllegalStateException e) {
					m.addAttribute("msg", "메소드관련 에러가 발생했습니다.");
					return "redirect:errorPage";
				} catch (IOException e) {
					m.addAttribute("msg", "파일관련 에러가 발생했습니다.");
					return "redirect:errorPage";
				} catch (Exception e) {
					m.addAttribute("msg", "알수없는 에러가 발생했습니다.");
					return "redirect:errorPage";
				}
			}
		}
		
		String phone = "";
		String phones[] = request.getParameterValues("phones");
		
		for (int i = 0; i < 3; i++) {
			if(i != 2) {
				phone += phones[i] + "-";
			} else {
				phone += phones[i];
			}
		}
		
		if(phone.equals("--")) {
			phone = null;
		}
		
		emp.setPhone(phone);
		
		String birthday = request.getParameter("birthday");
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		
		try {
			Date b = format.parse(birthday);
			long bb = b.getTime();
				
			java.sql.Date birth = new java.sql.Date(bb);
				
			emp.setBirth(birth);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String addr[] = request.getParameterValues("addresses");
		String address = "";
		
		for (int i = 0; i < 3; i++) {
			if(i != 0) {
				if(addr[i].equals("")) {
					break;
				}
				
				address += "/" + addr[i];
			} else {
				
				address += addr[i];
			}
		}
		
		if(address.equals("//")) {
			address = null;
		}
		
		emp.setAddress(address);
		
		ms.updateInfomation(emp);
		
		return "redirect:infoDet.mp";
	}
}
