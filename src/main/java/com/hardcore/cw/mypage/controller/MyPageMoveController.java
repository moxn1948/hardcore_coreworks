package com.hardcore.cw.mypage.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.board.controller.BoardInsertController;
import com.hardcore.cw.board.model.vo.Board;
import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.mypage.model.service.MyPageService;
import com.hardcore.cw.mypage.model.vo.Absence;
import com.hardcore.cw.mypage.model.vo.Annual;
import com.hardcore.cw.mypage.model.vo.Belong;

@Controller
@SessionAttributes("loginUser")
// MyPage 이동 관련 Controller
public class MyPageMoveController {
	private final MyPageService ms;

	public MyPageMoveController(MyPageService ms) {
		this.ms = ms;
	}

	@GetMapping("annList.mp")
	public String showAnnList(Model m, HttpServletRequest request) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		
		
		if(emp != null) {
			Annual ann = ms.selectAnn(emp);
			
			m.addAttribute("annual", ann);
			return "ann_list";
		} else {
			m.addAttribute("msg", "로그인이 되어있지않습니다.");
			return "redirect:index.jsp";
		}
	}
	
	@GetMapping("absList.mp")
	// 마이페이지로 이동(기본적으로 부재 목록 페이지)
	public String showAbsList(Model m, HttpServletRequest request) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		//currentPage에 대한 처리
		int currentPage = 1;
				
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
				
		int listCount = ms.getAbsListCount(emp);
				
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
				
		// 자기껏만 조회
		List<Absence> absList = ms.selectAbsList(pi, emp);
		
		m.addAttribute("absList", absList);
		m.addAttribute("pi", pi);
		
		return "abs_list";
	}
	
	// 개인정보 상세보기
	@GetMapping("infoDet.mp")
	public String  showInfoDet(Model m, HttpServletRequest request) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		// 프로필사진 뽑아오기
		Attachment at = ms.selectMyProfile(emp);
		Belong b = ms.selectMyBelong(emp);
		
		Employee empNew = ms.selectEmployee(emp);
		
		m.addAttribute("empNew", empNew);
		m.addAttribute("att", at);
		m.addAttribute("belong", b);
		
		return "info_det";
	}
	
	// 개인정보 수정페이지로 이동
	@GetMapping("infoUp.mp")
	public String showInfoUp(Model m, HttpServletRequest request) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		// 프로필사진 뽑아오기
		Attachment at = ms.selectMyProfile(emp);
		Belong b = ms.selectMyBelong(emp);
		
		Employee empNew = ms.selectEmployee(emp);
		
		if(emp.getPhone() != null) {
			
			String[] phone = emp.getPhone().split("-");
			
			m.addAttribute("phone", phone);
		}
		
		m.addAttribute("empNew", empNew);
		m.addAttribute("att", at);
		m.addAttribute("belong", b);
		
		return "info_up";
	}
	
	@GetMapping("pwdSet.mp")
	public String showSetPwd(Model m, HttpServletRequest request) {
		return "pwd_set";
	}
	
	
}
