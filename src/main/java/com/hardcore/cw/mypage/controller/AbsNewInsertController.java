package com.hardcore.cw.mypage.controller;

import java.util.Date;
import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.board.controller.BoardInsertController;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.mypage.model.service.MyPageService;
import com.hardcore.cw.mypage.model.vo.Absence;

@SessionAttributes("loginUser")
@Controller
public class AbsNewInsertController {
	private final MyPageService ms;
	private final AddrMenuService ams;

	public AbsNewInsertController(MyPageService ms, AddrMenuService ams) {
		this.ms = ms;
		this.ams = ams;
	}
	
	@GetMapping("absNew.mp")
	public String showAbsNew(Model m) {
		// 사내 부서 구조 조회
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();

		List<HashMap<String, Object>> empList = ams.selectEmpList();
		
		m.addAttribute("deptList", deptList);
		m.addAttribute("empList", empList);
		
		return "abs_new";
	}
	
	@PostMapping("insertAbs.mp")
	public String insertAbsNew(Model m, Absence a, HttpServletRequest request) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		String from = request.getParameter("from");
		String to = request.getParameter("to");
		String allDay = request.getParameter("allDay");
		String allYn = request.getParameter("allYn");
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		
		
		
		try {
			if(ObjectUtils.isEmpty(allYn)) {
				Date s = format.parse(from);
				Date e = format.parse(to);
				
				long ss = s.getTime();
				long ee = e.getTime();
				
				java.sql.Date start = new java.sql.Date(ss);
				java.sql.Date end = new java.sql.Date(ee);
				
				a.setAbsSdate(start);
				a.setAbsEdate(end);
				
				if((a.getAbsStime()).equals("allday")) {
					a.setAbsStime("00:00 오전");
				} 
				
				if((a.getAbsEtime()).equals("allday")) {
					a.setAbsEtime("11:59 오후");
				}
				
				Map<String, Object> map = new HashMap<>();
				map.put("abs", a);
				map.put("emp", emp);
				
				ms.insertAbsenceTypeOne(map);
			} else {
				Date ad = format.parse(allDay);
				
				long ss = ad.getTime();
				
				java.sql.Date start = new java.sql.Date(ss);
				
				a.setAbsSdate(start);
				
				Map<String, Object> map = new HashMap<>();
				map.put("abs", a);
				map.put("emp", emp);
				ms.insertAbsenceTypeTwo(map);
			}
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			String msg = "알 수 없는 에러가 발생했습니다.";
			m.addAttribute("msg", msg);
			return "redirect:index.jsp";
		}
		
		return "redirect:absList.mp";
	}
}
