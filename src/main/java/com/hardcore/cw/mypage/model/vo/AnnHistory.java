package com.hardcore.cw.mypage.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AnnHistory implements java.io.Serializable{
   private int annNo;
   private int useAnn;
   private Date annSdate;
   private Date annEdate;
   private String annCnt;
   private String type;
   private int empDivNo;
   private String empYear;
}