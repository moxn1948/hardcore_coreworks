package com.hardcore.cw.mypage.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.EasHistory;
import com.hardcore.cw.mypage.model.vo.Absence;
import com.hardcore.cw.mypage.model.vo.Annual;
import com.hardcore.cw.mypage.model.vo.Belong;


public interface MyPageDao {

	Annual selectAnn(SqlSessionTemplate sqlSession, LoginEmp emp);

	int insertAbsenceTypeOne(SqlSessionTemplate sqlSession, Map<String, Object> map);

	int insertAbsenceTypeTwo(SqlSessionTemplate sqlSession, Map<String, Object> map);

	int getAbsListCount(SqlSessionTemplate sqlSession, LoginEmp emp);

	List<Absence> selectAbsList(SqlSessionTemplate sqlSession, PageInfo pi, LoginEmp emp);

	Attachment selectMyProfile(SqlSessionTemplate sqlSession, LoginEmp emp);

	Belong selectMyBelong(SqlSessionTemplate sqlSession, LoginEmp emp);

	Employee selectMyInfo(SqlSessionTemplate sqlSession, LoginEmp emp);

	int updateInfomation(SqlSessionTemplate sqlSession, Employee emp);

	Employee selectEmployee(SqlSessionTemplate sqlSession, LoginEmp emp);

	int insertAttachment(SqlSessionTemplate sqlSession, Attachment at);

	int updatePassword(SqlSessionTemplate sqlSession, Map<String, Object> map);

	int updateAttachment(SqlSessionTemplate sqlSession, Attachment at);

	Absence selectAbsOne(SqlSessionTemplate sqlSession, Absence a);

	ArrayList<EasHistory> selectEasHistoryList(SqlSessionTemplate sqlSession, Absence a);

	ArrayList<DocDetail> selectDocDetailList(SqlSessionTemplate sqlSession, HashMap<String, Object> tempMap);

}
