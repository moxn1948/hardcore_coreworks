package com.hardcore.cw.mypage.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.EasHistory;
import com.hardcore.cw.mypage.model.vo.Absence;
import com.hardcore.cw.mypage.model.vo.Annual;
import com.hardcore.cw.mypage.model.vo.Belong;

public interface MyPageService {

	Annual selectAnn(LoginEmp emp);

	int insertAbsenceTypeOne(Map<String, Object> map);

	int insertAbsenceTypeTwo(Map<String, Object> map);

	int getAbsListCount(LoginEmp emp);

	List<Absence> selectAbsList(PageInfo pi, LoginEmp emp);

	Attachment selectMyProfile(LoginEmp emp);

	Belong selectMyBelong(LoginEmp emp);

	Employee selectMyInfo(LoginEmp emp);

	int updateInfomation(Employee emp);

	Employee selectEmployee(LoginEmp emp);

	int insertAttachment(Attachment at);

	int updatePassword(Map<String, Object> map);

	int updateAttachment(Attachment at);

	Absence selectAbsOne(Absence a);

	ArrayList<EasHistory> selectEasHistoryList(Absence a);

	ArrayList<DocDetail> selectDocDetailList(HashMap<String, Object> tempMap);

}
