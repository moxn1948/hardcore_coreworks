package com.hardcore.cw.mypage.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Annual implements java.io.Serializable{
	private int priAnn;
	private int useAnn;
	private int receiveAnn;
	private int empDivNo;
	private String empYear;
}
