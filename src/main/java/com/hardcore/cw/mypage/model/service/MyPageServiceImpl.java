package com.hardcore.cw.mypage.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.EasHistory;
import com.hardcore.cw.mypage.model.dao.MyPageDao;
import com.hardcore.cw.mypage.model.vo.Absence;
import com.hardcore.cw.mypage.model.vo.Annual;
import com.hardcore.cw.mypage.model.vo.Belong;

@Service
public class MyPageServiceImpl implements MyPageService {
	private final MyPageDao md;
	private final SqlSessionTemplate sqlSession;

	@Autowired
	public MyPageServiceImpl(MyPageDao md, SqlSessionTemplate sqlSession) {
		this.md = md;
		this.sqlSession = sqlSession;
	}

	@Override
	public Annual selectAnn(LoginEmp emp) {
		return md.selectAnn(sqlSession, emp);
	}

	@Override
	public int insertAbsenceTypeOne(Map<String, Object> map) {
		return md.insertAbsenceTypeOne(sqlSession, map);
	}

	@Override
	public int insertAbsenceTypeTwo(Map<String, Object> map) {
		return md.insertAbsenceTypeTwo(sqlSession, map);
	}

	@Override
	public int getAbsListCount(LoginEmp emp) {
		return md.getAbsListCount(sqlSession, emp);
	}

	@Override
	public List<Absence> selectAbsList(PageInfo pi, LoginEmp emp) {
		return md.selectAbsList(sqlSession, pi, emp);
	}

	@Override
	public Attachment selectMyProfile(LoginEmp emp) {
		return md.selectMyProfile(sqlSession, emp);
	}

	@Override
	public Belong selectMyBelong(LoginEmp emp) {
		return md.selectMyBelong(sqlSession, emp);
	}

	@Override
	public Employee selectMyInfo(LoginEmp emp) {
		return md.selectMyInfo(sqlSession, emp);
	}

	@Override
	public int updateInfomation(Employee emp) {
		return md.updateInfomation(sqlSession, emp);
	}

	@Override
	public Employee selectEmployee(LoginEmp emp) {
		return md.selectEmployee(sqlSession, emp);
	}

	@Override
	public int insertAttachment(Attachment at) {
		return md.insertAttachment(sqlSession, at);
	}

	@Override
	public int updatePassword(Map<String, Object> map) {
		return md.updatePassword(sqlSession, map);
	}

	@Override
	public int updateAttachment(Attachment at) {
		
		return md.updateAttachment(sqlSession, at);
	}

	@Override
	public Absence selectAbsOne(Absence a) {
		return md.selectAbsOne(sqlSession, a);
	}

	@Override
	public ArrayList<EasHistory> selectEasHistoryList(Absence a) {
		return md.selectEasHistoryList(sqlSession, a);
	}

	@Override
	public ArrayList<DocDetail> selectDocDetailList(HashMap<String, Object> tempMap) {
		
		return md.selectDocDetailList(sqlSession, tempMap);
	}

}
