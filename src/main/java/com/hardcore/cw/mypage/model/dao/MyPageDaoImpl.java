package com.hardcore.cw.mypage.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.EasHistory;
import com.hardcore.cw.mypage.model.vo.Absence;
import com.hardcore.cw.mypage.model.vo.Annual;
import com.hardcore.cw.mypage.model.vo.Belong;

@Repository
public class MyPageDaoImpl implements MyPageDao {

	@Override
	public Annual selectAnn(SqlSessionTemplate sqlSession, LoginEmp emp) {
		return sqlSession.selectOne("Mypage.selectAnn", emp);
	}

	// 종일이 아닌 경우
	@Override
	public int insertAbsenceTypeOne(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		return sqlSession.insert("Mypage.insertAbsenceTypeOne", map);
	}

	// 종일인 경우
	@Override
	public int insertAbsenceTypeTwo(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		return sqlSession.insert("Mypage.insertAbsenceTypeTwo", map);
	}

	// 부재 갯수
	@Override
	public int getAbsListCount(SqlSessionTemplate sqlSession, LoginEmp emp) {
		return sqlSession.selectOne("Mypage.getAbsListCount", emp);
	}

	// 부재 목록
	@Override
	public List<Absence> selectAbsList(SqlSessionTemplate sqlSession, PageInfo pi, LoginEmp emp) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return sqlSession.selectList("Mypage.selectAbsList", emp, rowBounds);
	}

	// 프로필사진 조회
	@Override
	public Attachment selectMyProfile(SqlSessionTemplate sqlSession, LoginEmp emp) {
		return sqlSession.selectOne("Mypage.selectMyProfile", emp);
	}

	@Override
	public Belong selectMyBelong(SqlSessionTemplate sqlSession, LoginEmp emp) {
		return sqlSession.selectOne("Mypage.selectMyBelong", emp);
	}

	@Override
	public Employee selectMyInfo(SqlSessionTemplate sqlSession, LoginEmp emp) {
		return sqlSession.selectOne("Mypage.selectMyInfo", emp);
	}

	// 사원정보 수정
	@Override
	public int updateInfomation(SqlSessionTemplate sqlSession, Employee emp) {
		return sqlSession.update("Mypage.updateInfomation", emp);
	}

	@Override
	public Employee selectEmployee(SqlSessionTemplate sqlSession, LoginEmp emp) {
		return sqlSession.selectOne("Mypage.selectEmployee", emp);
	}

	@Override
	public int insertAttachment(SqlSessionTemplate sqlSession, Attachment at) {
		return sqlSession.insert("Mypage.insertAttachment", at);
	}

	// 비밀번호 변경
	@Override
	public int updatePassword(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		return sqlSession.update("Mypage.updatePassword", map);
	}

	@Override
	public int updateAttachment(SqlSessionTemplate sqlSession, Attachment at) {
		
		return sqlSession.update("Mypage.updateAttachment", at);
	}

	@Override
	public Absence selectAbsOne(SqlSessionTemplate sqlSession, Absence a) {
		return sqlSession.selectOne("Mypage.selectAbsOne", a);
	}

	@Override
	public ArrayList<EasHistory> selectEasHistoryList(SqlSessionTemplate sqlSession, Absence a) {
		return (ArrayList)sqlSession.selectList("Mypage.selectEasHistoryList", a);
	}

	@Override
	public ArrayList<DocDetail> selectDocDetailList(SqlSessionTemplate sqlSession, HashMap<String, Object> tempMap) {
		
		return (ArrayList)sqlSession.selectList("Mypage.selectDocDetailList", tempMap);
	}

}
