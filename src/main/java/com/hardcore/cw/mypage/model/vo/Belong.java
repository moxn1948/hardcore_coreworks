package com.hardcore.cw.mypage.model.vo;

import java.io.Serializable;
import java.sql.Date;

import com.hardcore.cw.common.model.vo.Employee;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Belong implements Serializable {
   private String jobName;
   private String posName;
   private String deptName;
}