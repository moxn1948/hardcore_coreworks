package com.hardcore.cw.mail.model.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.mail.model.vo.Mail;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.Receiver;

@Repository
public class MailSenderDaoImpl implements MailSenderDao {

	@Override
	public int insertMailAttachment(SqlSessionTemplate sqlSession, Attachment a) {
		return sqlSession.insert("Mail.insertMailAttachment", a);
	}

	@Override
	public int insertMail(SqlSessionTemplate sqlSession, Mail m) {
		return sqlSession.insert("Mail.insertMail", m);
	}

	@Override
	public String selectJobName(SqlSessionTemplate sqlSession, int jobNo) {
		return sqlSession.selectOne("Mail.selectJobName", jobNo);
	}

	@Override
	public String selectPosName(SqlSessionTemplate sqlSession, int posNo) {
		return sqlSession.selectOne("Mail.selectPosName", posNo);
	}

	@Override
	public int selectEmpNo(SqlSessionTemplate sqlSession, String empId) {
		return sqlSession.selectOne("Mail.selectEmpNo", empId);
	}

	@Override
	public void insertReceiver(SqlSessionTemplate sqlSession, Receiver r) {
		sqlSession.insert("Mail.insertReceiver", r);
	}

	@Override
	public MailAndRead selectMail(SqlSessionTemplate sqlSession, int mailNo) {
		return sqlSession.selectOne("Mail.selectMail", mailNo);
	}

}
