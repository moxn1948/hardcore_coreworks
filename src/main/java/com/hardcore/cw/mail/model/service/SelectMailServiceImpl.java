package com.hardcore.cw.mail.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.mail.model.dao.SelectMailDao;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.MailReceiver;

@Service
public class SelectMailServiceImpl implements SelectMailService {

	@Autowired
	private SelectMailDao smd;
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Override
	public List<MailAndRead> selectReceiveMailList(Map<String, Object> dataMap) {
		return smd.selectReceiveMailList(sqlSession, dataMap);
	}

	@Override
	public List<Integer> selectAttachmentCount(List<MailAndRead> mailList) {
		List<Integer> attachCount = new ArrayList<>();
		
		for(int i = 0; i < mailList.size(); i++) {
			attachCount.add(smd.selectAttachCount(sqlSession, mailList.get(i).getMailNo()));
		}
		
		return attachCount;
	}

	@Override
	public MailAndRead selectOneMail(Map<String, Object> map) {
		return smd.selectOneMail(sqlSession, map);
	}

	@Override
	public List<Attachment> selectMailAttachment(int no) {
		List<Attachment> fileList = null;
		
		int count = smd.selectAttachCount(sqlSession, no);
		
		if(count > 0) {
			fileList = smd.selectMailAttachment(sqlSession, no);
		}
		
		return fileList;
	}

	@Override
	public Attachment selectOneAttachment(int no) {
		return smd.selectOneAttachment(sqlSession, no);
	}

	@Override
	public int insertMailtoRead(int no, int empNo) {
		Map<String, Object> map = new HashMap<>();
		map.put("no", no);
		map.put("empDivNo", empNo);
		return smd.insertMailtoRead(sqlSession, map);
	}

	@Override
	public int getListCount(Map<String, Object> map) {
		return smd.getListCount(sqlSession, map);
	}

	@Override
	public int getReadMailCount(int empDivNo) {
		return smd.getReadMailCount(sqlSession, empDivNo);
	}

	@Override
	public List<MailReceiver> selectReceiverList(int no) {
		return smd.selectReceiverList(sqlSession, no);
	}

}
