package com.hardcore.cw.mail.model.dao;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.mail.model.vo.Mail;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.Receiver;

public interface MailSenderDao {

	int insertMailAttachment(SqlSessionTemplate sqlSession, Attachment a);

	int insertMail(SqlSessionTemplate sqlSession, Mail m);

	String selectJobName(SqlSessionTemplate sqlSession, int jobNo);

	String selectPosName(SqlSessionTemplate sqlSession, int posNo);

	int selectEmpNo(SqlSessionTemplate sqlSession, String empId);

	void insertReceiver(SqlSessionTemplate sqlSession, Receiver r);

	MailAndRead selectMail(SqlSessionTemplate sqlSession, int mailNo);

}
