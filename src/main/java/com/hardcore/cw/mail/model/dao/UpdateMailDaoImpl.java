package com.hardcore.cw.mail.model.dao;

import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UpdateMailDaoImpl implements UpdateMailDao {

	@Override
	public void updateKeepYn(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		sqlSession.update("Mail.updateKeepYn", map);
	}

	@Override
	public void updateStatus(SqlSessionTemplate sqlSession, int i) {
		sqlSession.update("Mail.updateStatus", i);
	}

	@Override
	public void insertRead(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		sqlSession.insert("Mail.insertMailtoRead", map);
	}

	@Override
	public void deleteRead(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		sqlSession.delete("Mail.deleteRead", map);
	}

	@Override
	public void deleteMail(SqlSessionTemplate sqlSession, int i) {
		sqlSession.delete("Mail.deleteReceiver", i);
		int alramNo = sqlSession.selectOne("Mail.selectAlramNo", i);
		sqlSession.delete("Mail.deleteRead2", alramNo);
		sqlSession.delete("Mail.deleteAlram", alramNo);
		sqlSession.delete("Mail.deleteAlramRead", i);
		sqlSession.delete("Mail.deleteMailAlram", i);
		sqlSession.delete("Mail.deleteMail", i);
	}

}
