package com.hardcore.cw.mail.model.dao;

import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;

public interface UpdateMailDao {

	void updateKeepYn(SqlSessionTemplate sqlSession, Map<String, Object> map);

	void updateStatus(SqlSessionTemplate sqlSession, int i);

	void insertRead(SqlSessionTemplate sqlSession, Map<String, Object> map);

	void deleteRead(SqlSessionTemplate sqlSession, Map<String, Object> map);

	void deleteMail(SqlSessionTemplate sqlSession, int i);

}
