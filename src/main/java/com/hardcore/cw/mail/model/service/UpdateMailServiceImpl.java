package com.hardcore.cw.mail.model.service;

import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.mail.model.dao.UpdateMailDao;

@Service
public class UpdateMailServiceImpl implements UpdateMailService {

	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private UpdateMailDao umd;
	
	@Override
	public void updateKeepYn(Map<String, Object> map) {
		umd.updateKeepYn(sqlSession, map);
	}

	@Override
	public void updateStatus(int[] deleteArr) {
		for(int i : deleteArr) {
			umd.updateStatus(sqlSession, i);
		}
	}

	@Override
	public void updateRead(int[] readArr, int eNo) {
		Map<String, Object> map = new HashMap<>();
		for(int i : readArr) {
			map.put("no", i);
			map.put("empDivNo", eNo);
			umd.insertRead(sqlSession, map);
			map.clear();
		}
	}

	@Override
	public void deleteRead(int mailNo, int eNo) {
		Map<String, Object> map = new HashMap<>();
		map.put("mailNo", mailNo);
		map.put("eNo", eNo);
		
		umd.deleteRead(sqlSession, map);
	}

	@Override
	public void deleteMail(int[] deleteArr) {
		for(int i : deleteArr) {
			umd.deleteMail(sqlSession, i);
		}
	}

}
