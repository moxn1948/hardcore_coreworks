package com.hardcore.cw.mail.model.dao;

import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.MailReceiver;

public interface SelectMailDao {

	List<MailAndRead> selectReceiveMailList(SqlSessionTemplate sqlSession, Map<String, Object> dataMap);

	Integer selectAttachCount(SqlSessionTemplate sqlSession, int mailNo);

	MailAndRead selectOneMail(SqlSessionTemplate sqlSession, Map<String, Object> map);

	List<Attachment> selectMailAttachment(SqlSessionTemplate sqlSession, int no);

	Attachment selectOneAttachment(SqlSessionTemplate sqlSession, int no);

	int insertMailtoRead(SqlSessionTemplate sqlSession, Map<String, Object> map);

	int getListCount(SqlSessionTemplate sqlSession, Map<String, Object> map);

	int getReadMailCount(SqlSessionTemplate sqlSession, int empDivNo);

	List<MailReceiver> selectReceiverList(SqlSessionTemplate sqlSession, int no);

}
