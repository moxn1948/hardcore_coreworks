package com.hardcore.cw.mail.model.service;

import java.util.List;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.mail.model.vo.Mail;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.Receiver;

public interface MailSenderService {

	int insertMailAttachment(List<Attachment> attachList);

	int insertMail(Mail m);

	String selectJobName(int jobNo);

	String selectPosName(int posNo);

	int selectEmpNo(String empId);

	void insertReceiver(List<Receiver> receiverList);

	MailAndRead selectMail(int mailNo);

}
