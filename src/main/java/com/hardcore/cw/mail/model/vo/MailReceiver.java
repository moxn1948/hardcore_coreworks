package com.hardcore.cw.mail.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MailReceiver {
	private int receiveNo;
	private String type;
	private String outRec;
	private int inRec;
	private int empDivNo;
	private String empId;
	private String empName;
	private String jobName;
	private String posName;
}
