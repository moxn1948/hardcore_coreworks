package com.hardcore.cw.mail.model.service;

import java.util.Map;

public interface UpdateMailService {

	void updateKeepYn(Map<String, Object> map);

	void updateStatus(int[] deleteArr);

	void updateRead(int[] readArr, int eNo);

	void deleteRead(int mailNo, int eNo);

	void deleteMail(int[] deleteArr);

}
