package com.hardcore.cw.mail.model.service;

import java.util.List;
import java.util.Map;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.MailReceiver;

public interface SelectMailService {

	List<MailAndRead> selectReceiveMailList(Map<String, Object> dataMap);

	List<Integer> selectAttachmentCount(List<MailAndRead> mailList);

	MailAndRead selectOneMail(Map<String, Object> map);

	List<Attachment> selectMailAttachment(int no);

	Attachment selectOneAttachment(int no);

	int insertMailtoRead(int no, int empNo);

	int getListCount(Map<String, Object> map);

	int getReadMailCount(int empDivNo);

	List<MailReceiver> selectReceiverList(int no);

	

}
