package com.hardcore.cw.mail.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Mail implements java.io.Serializable{
	private int mailNo;
	private String referer;
	private String hiddenRef;
	private String mailTitle;
	private String mailCnt;
	private String importantYn;
	private String protectYn;
	private String resvYn;
	private String mailPwd;
	private String sendTime;
	private String keepYn;
	private String status;
	private String type;
	private String outSend;
	private int inSend;
}
