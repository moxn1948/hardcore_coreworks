package com.hardcore.cw.mail.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TempMail implements java.io.Serializable{
	private int tempNo;
	private int mailNo;
	private int sender;
	private String mailTitle;
	private String mailCnt;
}
