package com.hardcore.cw.mail.model.service;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.mail.model.dao.MailSenderDao;
import com.hardcore.cw.mail.model.vo.Mail;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.Receiver;

@Service
public class MailSenderServiceImpl implements MailSenderService {

	@Autowired
	private MailSenderDao msd;
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Override
	public int insertMailAttachment(List<Attachment> attachList) {
		int result = 0;
		for(Attachment a : attachList) {
			result += msd.insertMailAttachment(sqlSession, a);
		}
		
		if(result == attachList.size()) {
			return result;
		} else {
			return 0;
		}
	}

	@Override
	public int insertMail(Mail m) {
		return msd.insertMail(sqlSession, m);
	}
	

	@Override
	public int selectEmpNo(String empId) {
		return msd.selectEmpNo(sqlSession, empId);
	}
	
	@Override
	public void insertReceiver(List<Receiver> receiverList) {
		for(Receiver r : receiverList) {
			msd.insertReceiver(sqlSession, r);
		}
	}

	@Override
	public String selectJobName(int jobNo) {
		return msd.selectJobName(sqlSession, jobNo);
	}

	@Override
	public String selectPosName(int posNo) {
		return msd.selectPosName(sqlSession, posNo);
	}

	@Override
	public MailAndRead selectMail(int mailNo) {
		return msd.selectMail(sqlSession, mailNo);
	}

}
