package com.hardcore.cw.mail.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Receiver {
   private int mailNo;
   private String outRec;
   private int inRec;
   private String type;
}
