package com.hardcore.cw.mail.model.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.MailReceiver;

@Repository
public class SelectMailDaoImpl implements SelectMailDao {

	@Override
	public List<MailAndRead> selectReceiveMailList(SqlSessionTemplate sqlSession, Map<String, Object> dataMap) {
		
		int offset = (((PageInfo) dataMap.get("pi")).getCurrentPage() - 1) * ((PageInfo) dataMap.get("pi")).getLimit();
		
		RowBounds rowBounds = new RowBounds(offset, ((PageInfo) dataMap.get("pi")).getLimit());

		return sqlSession.selectList("Mail.selectReceiveMailList", dataMap, rowBounds);
	}
	
	@Override
	public int getListCount(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		return sqlSession.selectOne("Mail.getListCount", map);
	}


	@Override
	public Integer selectAttachCount(SqlSessionTemplate sqlSession, int mailNo) {
		return sqlSession.selectOne("Mail.selectAttachCount", mailNo);
	}

	@Override
	public MailAndRead selectOneMail(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		return sqlSession.selectOne("Mail.selectOneMail", map);
	}

	@Override
	public List<Attachment> selectMailAttachment(SqlSessionTemplate sqlSession, int no) {
		return sqlSession.selectList("Mail.selectMailAttachment", no);
	}

	@Override
	public Attachment selectOneAttachment(SqlSessionTemplate sqlSession, int no) {
		return sqlSession.selectOne("Mail.selectOneAttachment", no);
	}

	@Override
	public int insertMailtoRead(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		return sqlSession.insert("Mail.insertMailtoRead", map);
	}

	@Override
	public int getReadMailCount(SqlSessionTemplate sqlSession, int empDivNo) {
		return sqlSession.selectOne("Mail.getReadMailCount", empDivNo);
	}

	@Override
	public List<MailReceiver> selectReceiverList(SqlSessionTemplate sqlSession, int no) {
		return sqlSession.selectList("Mail.selectReceiverList", no);
	}

}
