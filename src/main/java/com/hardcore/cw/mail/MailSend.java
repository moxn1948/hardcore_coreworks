package com.hardcore.cw.mail;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;

public class MailSend {

	static final String FROM = "test@coreworks.info";
	static final String TO = "wonkyong1022@gmail.com";
	static final String SUBJECT = "ses 메일 테스트";
	static final String HTMLBODY = "<h1>Amazon SES test (AWS SDK for Java)</h1>"
		      + "<p>This email was sent with <a href='https://aws.amazon.com/ses/'>"
		      + "Amazon SES</a> using the <a href='https://aws.amazon.com/sdk-for-java/'>" 
		      + "AWS SDK for Java</a>";
	static final String TEXTBODY = "이건 안가나 ?ㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁㅁ";
	
	public static void main(String[] args) {
		try {
			String accessKey = "";
			String secretKey = "";
			
			AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
			
			AmazonSimpleEmailService client = 
					AmazonSimpleEmailServiceClientBuilder.standard()
					.withRegion(Regions.US_WEST_2)
					.withCredentials(new AWSStaticCredentialsProvider(credentials))
					.build();
			SendEmailRequest request = new SendEmailRequest()
						  .withDestination(new Destination().withToAddresses(TO))
			              .withMessage(new Message().withBody(new Body().withHtml(new Content().withCharset("UTF-8").withData(HTMLBODY)).withText(new Content().withCharset("UTF-8").withData(TEXTBODY)))
		                  .withSubject(new Content().withCharset("UTF-8").withData(SUBJECT)))
			              .withSource(FROM);
			client.sendEmail(request);
			System.out.println("send");
		} catch (Exception ex) {
		      System.out.println("The email was not sent. Error message: " 
		              + ex.getMessage());
        }
	}
}
