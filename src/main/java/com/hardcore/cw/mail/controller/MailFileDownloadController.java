package com.hardcore.cw.mail.controller;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.mail.model.service.SelectMailService;

@Controller
public class MailFileDownloadController {

	@Autowired
	private SelectMailService sms;
	
	@RequestMapping("fileDownload.ma")
	public ModelAndView reDocumentDown(String fileNo) {
		int no = Integer.parseInt(fileNo);
		Attachment a = sms.selectOneAttachment(no);
		
		String filePath = a.getFilePath();
		String fileName = a.getChangeName();
		String fullPath = filePath + fileName;
		File downFile = new File(fullPath);
		
		Map<String, Object> map = new HashMap<>();
		
		map.put("downFile", downFile);
		map.put("originName", a.getOriginName());
		
		return new ModelAndView("downloadView", "downloadFile", map);
	}
}
