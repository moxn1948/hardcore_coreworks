package com.hardcore.cw.mail.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;
import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.common.CommonsUtils;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.mail.model.service.MailSenderService;
import com.hardcore.cw.mail.model.vo.Mail;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.Receiver;

@Controller
public class MailSenderController {

	@Autowired
	private AddrMenuService ams;
	
	@Autowired
	private MailSenderService mss;

	@PostMapping("sendmail.ma")
	public String mailSend(Mail m, Model model, MultipartHttpServletRequest request, String sender, String receiver,
			@RequestParam("sendemail") String sendemail, @RequestParam("reservation") String reservation) {
		// 여기서부터
		List<MultipartFile> fileList = request.getFiles("file");
		List<Attachment> attachList = new ArrayList<>();

		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");

		mss.insertMail(m);

		List<Receiver> receiverList = new ArrayList<>();
		for(String r : receiver.split(", ")) {
			Receiver rc = new Receiver();
			if(r.contains("coreworks.info")) {
				String empId = r.split("@")[0];
				rc.setInRec(mss.selectEmpNo(empId));
				rc.setType("IN");
			} else {
				rc.setOutRec(r);
				rc.setType("OUT");
			}
			rc.setMailNo(m.getMailNo());
			receiverList.add(rc);
		}
		
		mss.insertReceiver(receiverList);
		
		for (MultipartFile mf : fileList) {
			if (mf.getOriginalFilename() != "") {
				String root = request.getSession().getServletContext().getRealPath("resources");
				String filePath = root + "\\uploadFiles";
				String originFileName = mf.getOriginalFilename();
				String ext = originFileName.substring(originFileName.lastIndexOf("."));
				String changeName = CommonsUtils.getRandomString();

				Attachment a = new Attachment();
				a.setOriginName(originFileName);
				a.setChangeName(changeName + ext);
				a.setFilePath(filePath);
				a.setMailNo(m.getMailNo());

				try {
					mf.transferTo(new File(filePath + "\\" + changeName + ext));
					attachList.add(a);
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		String attachFile = "";
		for (int i = 0; i < attachList.size(); i++) {
			if(i == 0) {
				attachFile += attachList.get(i).getFilePath() + "\\" + attachList.get(i).getChangeName();
			} else {
				attachFile += ", " + attachList.get(i).getFilePath() + "\\" + attachList.get(i).getChangeName();
			}
		}

		mss.insertMailAttachment(attachList);

		// 여기까지 메일 디비 저장

		// 여기서부터 메일 발송
		try {
			Session session = Session.getDefaultInstance(new Properties());
			
			String accessKey = "";
			String secretKey = "";

			AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);

			MimeMessage message = new MimeMessage(session);
			
			if(m.getImportantYn().equals("Y")) {
				message.setHeader("importance", "High");
			}
			
			if(m.getResvYn().equals("Y")) {
				Date date = new SimpleDateFormat("yyyy/MM/dd HH-mm").parse(reservation);
				System.out.println(date); // 예약메일 아직 미구현 ..
			}
			
			message.setSubject(m.getMailTitle(), "UTF-8");
			
			String jobName = mss.selectJobName(emp.getJobNo());
			String posName = mss.selectPosName(emp.getPosNo());
			
			message.setFrom(new InternetAddress(sendemail, emp.getEmpName() + " " + jobName + "/" + posName));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(receiver));
			
			if (m.getReferer() != null) {
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(m.getReferer()));
			}
			
			if (m.getReferer() != null) {
				message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(m.getReferer()));
			}

			MimeMultipart msg_body = new MimeMultipart("alternative");

			MimeBodyPart wrap = new MimeBodyPart();

			MimeBodyPart textPart = new MimeBodyPart();
	        textPart.setContent("", "text/plain; charset=UTF-8");
			
			MimeBodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(m.getMailCnt(), "text/html; charset=UTF-8");

			msg_body.addBodyPart(htmlPart);

			wrap.setContent(msg_body);

			MimeMultipart msg = new MimeMultipart("mixed");

			message.setContent(msg);

			msg.addBodyPart(wrap);

			if (attachFile != "") {
				String[] filename = attachFile.split(", ");
				
				MimeBodyPart att = null;
				FileDataSource fds = null;

				for (int i = 0; i < filename.length; i++) {
					att = null;
					fds = null;
					att = new MimeBodyPart();
					fds = new FileDataSource(filename[i]);
					att.setDataHandler(new DataHandler(fds));
					att.setFileName(MimeUtility.encodeText(attachList.get(i).getOriginName(), "euc-kr", "B"));
					msg.addBodyPart(att);
				}
			}

			try {
				System.out.println("Attempting to send an email through Amazon SES " + "using the AWS SDK for Java...");

				AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
						.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_WEST_2)
						.build();

				PrintStream out = System.out;
	            message.writeTo(out);
				
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
				message.writeTo(outputStream);
				RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

				SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);

				client.sendRawEmail(rawEmailRequest);
			} catch (Exception ex) {
				System.err.println("Error message: " + ex.getMessage());
				ex.printStackTrace();
			}
		} catch (Exception e) {
			e.getMessage();
		}
		
		return "redirect:selectMail.ma";
	}
	
	@RequestMapping("sendReplyMail.ma")
	public ModelAndView sendReplyMail(ModelAndView mv, int mailNo) {
		MailAndRead m = mss.selectMail(mailNo);
		
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();
		
		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		
		mv.addObject("m", m);
		mv.setViewName("write_mail");
		return mv;
	}
}
