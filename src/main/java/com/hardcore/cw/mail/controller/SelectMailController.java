package com.hardcore.cw.mail.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.mail.model.service.SelectMailService;
import com.hardcore.cw.mail.model.vo.MailAndRead;
import com.hardcore.cw.mail.model.vo.MailReceiver;

@Controller
public class SelectMailController {
	
	@Autowired
	private AddrMenuService ams;
	
	@Autowired
	private SelectMailService sms;
	
	@RequestMapping("selectMail.ma")
	public ModelAndView selectMailList(ModelAndView mv, HttpServletRequest request, String type) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		System.out.println(type);
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		Map<String, Object> listMap = new HashMap<>();
		listMap.put("type", type);
		listMap.put("empNo", emp.getEmpDivNo());
		int listCount = sms.getListCount(listMap);
		int readMailCount = sms.getReadMailCount(emp.getEmpDivNo());
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put("empNo", emp.getEmpDivNo());
		dataMap.put("pi", pi);
		dataMap.put("type", type);
		List<MailAndRead> mailList = sms.selectReceiveMailList(dataMap);
		List<Integer> attachCount = sms.selectAttachmentCount(mailList);
		
		mv.addObject("type", type);
		mv.addObject("TotalListCount", listCount);
		mv.addObject("TotalReadMailCount", readMailCount);
		mv.addObject("pi", pi);
		mv.addObject("attachCount", attachCount);
		mv.addObject("list", mailList);
		mv.addObject("path", "selectMail.ma");

		mv.setViewName("rec_mail");
		return mv;
	}
	
	@RequestMapping("writemail.ma")
	public ModelAndView writeMail(ModelAndView mv) {
		
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();
		
		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		mv.setViewName("write_mail");
		
		return mv;
	}
	
	@RequestMapping("selectOneMail.ma")
	public ModelAndView selectOneMail(ModelAndView mv, String mailNo, HttpServletRequest request) {
		int no = Integer.parseInt(mailNo);
		
		LoginEmp e = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		int empNo = e.getEmpDivNo();
		
		Map<String, Object> map = new HashMap<>();
		map.put("mailNo", no);
		map.put("empNo", empNo);
		MailAndRead m = sms.selectOneMail(map);
		List<Attachment> fileList = sms.selectMailAttachment(no);
		List<MailReceiver> empList = sms.selectReceiverList(no);

		sms.insertMailtoRead(no, empNo);
		
		Map<String, Object> listMap = new HashMap<>();
		listMap.put("type", null);
		listMap.put("empNo", empNo);
		int listCount = sms.getListCount(listMap);
		int readMailCount = sms.getReadMailCount(empNo);
		
		mv.addObject("TotalListCount", listCount);
		mv.addObject("TotalReadMailCount", readMailCount);
		mv.addObject("empList", empList);
		mv.addObject("fileList", fileList);
		mv.addObject("m", m);
		mv.setViewName("mail_detail");
		return mv;
	}
	
	@RequestMapping("searchMail.ma")
	public ModelAndView searchMail(HttpServletRequest request, ModelAndView mv, String type, String searchCondition, String mail_search, String date1, String date2) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		String domain = (String) request.getSession().getAttribute("domain");
		
		Map<String, Object> map = new HashMap<>();
		map.put("empNo", emp.getEmpDivNo());
		map.put("searchCondition", searchCondition);
		map.put("type", type);
		
		if(searchCondition.equals("date")) {
			map.put("before", date1.substring(2));
			map.put("after", date2.substring(2));
		} else {
			map.put("searchValue", mail_search);
		}
		
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		int listCount = sms.getListCount(map);
	
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		map.put("pi", pi);
		
		List<MailAndRead> mailList = sms.selectReceiveMailList(map);
		
		if(searchCondition.equals("content")) {
			listCount = 0;
			List<MailAndRead> tempList = new ArrayList<>();
			for(MailAndRead m : mailList) {
				if(m.getMailCnt().contains(mail_search)) {
					listCount++;
					tempList.add(m);
				}
			}
			pi = Pagination.getPageInfo(currentPage, listCount);
			mailList = tempList;
		}
		
		List<Integer> attachCount = sms.selectAttachmentCount(mailList);
		
		Map<String, Object> listMap = new HashMap<>();
		listMap.put("type", null);
		listMap.put("empNo", emp.getEmpDivNo());
		listCount = sms.getListCount(listMap);
		int readMailCount = sms.getReadMailCount(emp.getEmpDivNo());
		
		mv.addObject("TotalListCount", listCount);
		mv.addObject("TotalReadMailCount", readMailCount);
		mv.addObject("type", type);
		mv.addObject("pi", pi);
		mv.addObject("attachCount", attachCount);
		mv.addObject("list", mailList);
		mv.addObject("path", "searchMail.ma");
		mv.addObject("searchCondition", searchCondition);
		mv.addObject("searchValue", mail_search);
		mv.addObject("date1", date1);
		mv.addObject("date2", date2);
		mv.setViewName("rec_mail");
		return mv;
	}
	
}
