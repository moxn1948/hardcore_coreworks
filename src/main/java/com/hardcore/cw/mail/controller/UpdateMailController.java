package com.hardcore.cw.mail.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.mail.model.service.UpdateMailService;

@Controller
public class UpdateMailController {
	
	@Autowired
	private UpdateMailService ums;
	
	@PostMapping("updateKeepYn.ma")
	public ModelAndView updateKeepYn(ModelAndView mv, String mailNo, String state) {
		Map<String, Object> map = new HashMap<>();
		map.put("mailNo", mailNo);
		map.put("state", state);
		
		ums.updateKeepYn(map);
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("updateStatus.ma")
	public ModelAndView updateStatus(ModelAndView mv, int[] deleteArr, String type) {
		System.out.println(type);
		if(!type.equals("delete")) {
			ums.updateStatus(deleteArr);
		} else {
			ums.deleteMail(deleteArr);
		}
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("updateRead.ma")
	public ModelAndView updateRead(ModelAndView mv, int[] readArr, int eNo) {
		ums.updateRead(readArr, eNo);
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("deleteRead.ma")
	public ModelAndView deleteRead(ModelAndView mv, int mailNo, int eNo) {
		ums.deleteRead(mailNo, eNo);
		
		mv.setViewName("jsonView");
		return mv;
	}
}
