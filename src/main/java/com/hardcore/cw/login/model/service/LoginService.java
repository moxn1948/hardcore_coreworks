package com.hardcore.cw.login.model.service;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;

public interface LoginService {
 
	LoginEmp loginEmployee(Employee e);

	Authority selectAuth(int empDivNo);

	String selectDomain();
	
}
