package com.hardcore.cw.login.model.service;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.login.model.dao.LoginDao;

@Service
public class LoginServiceImpl implements LoginService {
	 
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private LoginDao ld;
	
	@Override
	public LoginEmp loginEmployee(Employee e) {
		LoginEmp loginUser = null;
		
		String encPassword = ld.selectEncPassword(sqlSession, e);
		
		if(passwordEncoder.matches(e.getEmpPwd(), encPassword)) {
			loginUser = ld.selectEmployee(sqlSession, e);
		}
		
		return loginUser;
	}

	@Override
	public Authority selectAuth(int empDivNo) {
		return ld.selectAuth(sqlSession, empDivNo);
	}

	@Override
	public String selectDomain() {
		return ld.selectDomain(sqlSession);
	}

}
