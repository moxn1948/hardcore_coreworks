package com.hardcore.cw.login.model.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;

@Repository 
public class LoginDaoImpl implements LoginDao {

	@Override
	public String selectEncPassword(SqlSessionTemplate sqlSession, Employee e) {
		return sqlSession.selectOne("Employee.selectPwd", e.getEmpId());
	}

	@Override
	public LoginEmp selectEmployee(SqlSessionTemplate sqlSession, Employee e) {
		return sqlSession.selectOne("Employee.selectLoginUser", e);
	}

	@Override
	public Authority selectAuth(SqlSessionTemplate sqlSession, int empDivNo) {
		return sqlSession.selectOne("Employee.selectAuth", empDivNo);
	}

	@Override
	public String selectDomain(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Employee.selectDomain");
	}

}
