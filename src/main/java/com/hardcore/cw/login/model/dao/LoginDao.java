package com.hardcore.cw.login.model.dao;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;

public interface LoginDao { 

	String selectEncPassword(SqlSessionTemplate sqlSession, Employee e);

	LoginEmp selectEmployee(SqlSessionTemplate sqlSession, Employee e);

	Authority selectAuth(SqlSessionTemplate sqlSession, int empDivNo);

	String selectDomain(SqlSessionTemplate sqlSession);
}
