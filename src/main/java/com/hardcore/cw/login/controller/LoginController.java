package com.hardcore.cw.login.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.first.model.service.FirstService;
import com.hardcore.cw.login.model.service.LoginService;

@Controller
@SessionAttributes({"loginUser", "auth", "domain", "firstYn"})
public class LoginController {
	private static Logger logger = LoggerFactory.getLogger(LoginController.class);
	 
	@Autowired
	private LoginService ls;
	
	@Autowired
	private FirstService fs;

	
	@PostMapping("login.me")
	public String loginCheck(Employee e, Model model) {
		LoginEmp loginUser = ls.loginEmployee(e);
		HashMap<String, Object> firstYn  = fs.selectFirstYn();
		if(loginUser == null) {
			model.addAttribute("msg", "로그인 실패!");
			return "common/errorPage";
		}
		
		if(!loginUser.getAuth().equals("EMP")) {
			Authority auth = ls.selectAuth(loginUser.getEmpDivNo());
			model.addAttribute("auth", auth);
		}
		
		String domain = ls.selectDomain();
		model.addAttribute("firstYn", firstYn);
		model.addAttribute("loginUser", loginUser);
		model.addAttribute("domain", domain); 
		return "redirect:index.jsp";		
	}
	
	
	@RequestMapping("logout.me")
	public ModelAndView logout(ModelAndView mv, HttpServletRequest request, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		HttpSession session = request.getSession();
		
		session.invalidate();
		
		mv.addObject("loginUser", null);
		mv.addObject("auth", null);
		mv.addObject("firstYn", null);
		mv.addObject("domain", null);
		mv.addObject("msg", "로그아웃 되었습니다.");
		mv.setViewName("common/logout");
		return mv;
	}
}
