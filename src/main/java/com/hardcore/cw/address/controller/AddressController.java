package com.hardcore.cw.address.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrGroupService;
import com.hardcore.cw.address.model.service.AddrListService;
import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.address.model.service.AddrOutsiderService;
import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.address.model.vo.Memory;
import com.hardcore.cw.address.model.vo.Outsider;
import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;

@Controller
public class AddressController {
	private static final Logger logger = LoggerFactory.getLogger(AddressController.class);
	
	@Autowired
	private AddrMenuService ams;
	@Autowired
	private AddrOutsiderService aos;
	@Autowired
	private AddrListService als;
	@Autowired
	private AddrGroupService ags;
	
	// moxn : 주소록 메뉴 조회
	@RequestMapping("addrMain.ad")
	public ModelAndView addrMain(ModelAndView mv, HttpServletRequest request) {
		LoginEmp e = (LoginEmp) request.getSession().getAttribute("loginUser");
		int empDivNo = e.getEmpDivNo(); 
		int deptNo = e.getDeptNo(); 
		
		// 사내 부서 구조 조회 서비스
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		
		// 공용주소록 그룹 조회 서비스
		List<Address> teamGrpList = ams.selectTeamGrpList(deptNo);
		
		// 내주소록 그룹 조회 서비스
		List<Address> empGrpList = ams.selectEmpGrpList(empDivNo);
		
		if(deptList != null && teamGrpList != null && empGrpList != null) {
			mv.addObject("deptList", deptList);
			mv.addObject("teamGrpList", teamGrpList);
			mv.addObject("empGrpList", empGrpList);
			mv.setViewName("addr_list");
			
			
		}else {
			mv.addObject("msg", "주소록 로드에 실패했습니다.");
			mv.addObject("view", "index.jsp");
			mv.setViewName("addrAlert");
		}
		
		return mv;
	}
	
	// moxn : 주소록 사원 정보 조회 : ajax
	@PostMapping("selectEmpOne.ad")
	public ModelAndView selectEmpOne(String eNo, ModelAndView mv, HttpServletResponse response) {
		int empDivNo = Integer.parseInt(eNo);
		
		
		// 사원 기본 정보 조회 서비스
		HashMap<String, Object> empOneInfo = ams.selectEmpOne(empDivNo);
		// 사원 프로필 사진 조회 서비스
		HashMap<String, Object> empProfile = ams.selectEmpProfile(empDivNo);
		
		mv.addObject("info", empOneInfo);
		mv.addObject("profile", empProfile);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	// moxn : 연락처 추가 그룹 목록 불러오기 : ajax
	@PostMapping("outsiderGrpList.ad")
	public ModelAndView insertOutsider(ModelAndView mv, String type, String eNo, String dNo, HttpServletResponse response) {
		int empDivNo = Integer.parseInt(eNo);
		int deptNo = Integer.parseInt(dNo);
		
		List<Address> grpList;
		
		if(type.equals("DEPT")) {
			grpList = aos.selectOutDeptGrpList(deptNo);
		}else {
			grpList = aos.selectOutEmpGrpList(empDivNo);
		}
		
		mv.addObject("grpList", grpList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	// moxn : 새 연락처 추가 
	@PostMapping("newOutsider.ad")
	public ModelAndView insertOutsider(ModelAndView mv, Address addr, Outsider out, 
										String[] memoryName, String[] memoryDate, String calNum, HttpServletRequest request) {
		// 외부인 정보 가공
		out.setAddrNo(addr.getAddrNo());
		
		// 기념일 가공
		ArrayList<Memory> memoryList = new ArrayList<>();
		if(memoryName != null) {

			// 음력&양력 가공
			ArrayList<String> sunmoon = new ArrayList<>();
			
			String[] calNumArr = calNum.split(",");
			for (int i = 1; i <= calNumArr.length; i++) {
				if(calNumArr[i-1].equals("1")) {
					sunmoon.add(request.getParameter("cal_type"+i));
				}
			}
			

			SimpleDateFormat transFormat = new SimpleDateFormat("yyyy/MM/dd");
			for (int i = 0; i < memoryName.length; i++) {
				Memory m = new Memory();
				
				// String to Date Format
				Date parsedDate = new Date();
				try {
					parsedDate = transFormat.parse(memoryDate[i]);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				m.setMemoryName(memoryName[i]);
				m.setSunmoon(sunmoon.get(i));
				m.setMemoryDate(new java.sql.Date(parsedDate.getTime()));

				memoryList.add(m);
			}
		}
		
		// 외부인&기념일 저장 서비스
		int result = aos.insertOutsider(out, memoryList);
		
		if(result > 0) {
			mv.addObject("msg", "연락처가 추가되었습니다.");
			mv.addObject("view", "addr_list");
			mv.setViewName("addrAlert");
			
		}else {
			mv.addObject("msg", "연락처 추가에 실패했습니다.");
			mv.addObject("view", "index.jsp");
			mv.setViewName("addrAlert");
		}
		
		return mv;
	}
	
	// moxn : 사내주소록 사원 목록 : ajax
	@PostMapping("selectEmpAddrList.ad")
	public ModelAndView selectEmpAddrList(ModelAndView mv, int deptNo, int currentPage, String addrSel, String addrCtnIpt, HttpServletResponse response) {
		if(addrCtnIpt != null && addrCtnIpt.contains("@")) {
			addrCtnIpt = addrCtnIpt.split("@")[0];
		}
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("deptNo", deptNo);
		map.put("addrSel", addrSel);
		map.put("addrCtnIpt", addrCtnIpt);
		
		int listCount = als.selectEmpListCount(map);

		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		List<HashMap<String, Object>> list = als.selectEmpList(pi, map);
		
		mv.addObject("list", list);
		mv.addObject("pi", pi);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
	}

	// moxn : 공용&팀 주소록 사원 목록 : ajax
	@PostMapping("selectGrpAddrList.ad")
	public ModelAndView selectGrpAddrList(ModelAndView mv, int myDeptNo, int myEmpNo, int addrNo, String addrSel, String addrCtnIpt, int currentPage, HttpServletResponse response) {
		HashMap<String, Object> map = new HashMap<>();
		map.put("deptNo", myDeptNo);
		map.put("empDivNo", myEmpNo);
		map.put("addrNo", addrNo);
		map.put("addrSel", addrSel);
		map.put("addrCtnIpt", addrCtnIpt);
		
		int listCount = als.selectGrpListCount(map);
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		List<HashMap<String, Object>> list = als.selectGrpList(pi, map);
		
		mv.addObject("list", list);
		mv.addObject("pi", pi);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
	}

	// moxn : 사내 주소록 사원 상세정보 : ajax
	@PostMapping("selectDeptEmpOne.ad")
	public ModelAndView selectDeptEmpOne(ModelAndView mv, int empDivNo, HttpServletResponse response) {

		// 사원 기본 정보 조회 서비스
		HashMap<String, Object> empOneInfo = ams.selectEmpOne(empDivNo);
		// 사원 프로필 사진 조회 서비스
		HashMap<String, Object> empProfile = ams.selectEmpProfile(empDivNo);
		
		mv.addObject("info", empOneInfo);
		mv.addObject("profile", empProfile);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
	}

	// moxn : 공용 & 내 주소록 외부인 상세정보 : ajax
	@PostMapping("selectGrpOutOne.ad")
	public ModelAndView selectGrpOutOne(ModelAndView mv, int outNo, HttpServletResponse response) {
		

		// 외부인 기본 정보 조회 서비스
		Outsider outOneInfo = als.selectOutOne(outNo); 

		// 외부인 기념일 정보 조회 서비스
		List<HashMap<String, Object>> memoryOneInfo = als.selectMemoryOne(outNo);

		mv.addObject("info", outOneInfo);
		mv.addObject("memory", memoryOneInfo);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("selectUpOutInfo.ad")
	public ModelAndView selectUpOutInfo(ModelAndView mv, int outNo, HttpServletResponse response) {

		// 외부인 기본 정보 조회 서비스
		Outsider outOneInfo = als.selectOutOne(outNo); 
		
		// 외부인 주소록 정보 조회 서비스
		Address outAddrInfo = als.selectOutAddrInfo(outOneInfo.getAddrNo());

		// 외부인 기념일 정보 조회 서비스
		List<HashMap<String, Object>> memoryOneInfo = als.selectMemoryOne(outNo);
		
		mv.addObject("info", outOneInfo);
		mv.addObject("addr", outAddrInfo);
		mv.addObject("memory", memoryOneInfo);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("updateOutsider.ad")
	public ModelAndView updateOutsiderOne(ModelAndView mv, Address addr, Outsider out, 
										int[] memoryNo, String[] memoryName, String[] memoryDate, String calNum, HttpServletRequest request) {
		// 외부인 정보 가공
		out.setAddrNo(addr.getAddrNo());
		
		// 기념일 가공
		ArrayList<Memory> memoryList = new ArrayList<>();
		if(memoryName != null) {

			// 음력&양력 가공
			ArrayList<String> sunmoon = new ArrayList<>();
			
			String[] calNumArr = calNum.split(",");
			for (int i = 1; i <= calNumArr.length; i++) {
				if(calNumArr[i-1].equals("1")) {
					sunmoon.add(request.getParameter("cal_type"+i));
				}
			}
			

			SimpleDateFormat transFormat = new SimpleDateFormat("yyyy/MM/dd");
			for (int i = 0; i < memoryName.length; i++) {
				Memory m = new Memory();
				
				// String to Date Format
				Date parsedDate = new Date();
				try {
					parsedDate = transFormat.parse(memoryDate[i]);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				m.setMemoryNo(memoryNo[i]);
				m.setMemoryName(memoryName[i]);
				m.setSunmoon(sunmoon.get(i));
				m.setMemoryDate(new java.sql.Date(parsedDate.getTime()));
				m.setOutNo(out.getOutNo());
				
				memoryList.add(m);
			}
		}
		
		// 외부인&기념일 저장 서비스
		int result = als.updateOutsiderOne(out, memoryList);
		
		if(result > 0) {
			mv.addObject("msg", "연락처가 수정되었습니다.");
			mv.addObject("view", "addr_list");
			mv.setViewName("addrAlert");
			
		}else {
			mv.addObject("msg", "연락처 수정에 실패했습니다.");
			mv.addObject("view", "index.jsp");
			mv.setViewName("addrAlert");
		}
		
		return mv;
	}
	
	// moxn : 외부인 삭제 메소드
	@PostMapping("deleteOutsiderOne.ad")
	public ModelAndView deleteOutsiderOne(ModelAndView mv, int grpOutNo) {
		
		int result = als.deleteOutsiderOne(grpOutNo);
		
		if(result > 0) {
			mv.addObject("msg", "연락처가 삭제되었습니다.");
			mv.addObject("view", "addr_list");
			mv.setViewName("addrAlert");
			
		}else {
			mv.addObject("msg", "연락처 삭제에 실패했습니다.");
			mv.addObject("view", "index.jsp");
			mv.setViewName("addrAlert");
		}
		
		return mv;
	}

	// moxn : 부서 주소록 그룹 목록 조회
	@PostMapping("teamGroupList.ad")
	public ModelAndView selectTeamGroupList(ModelAndView mv, int deptNo) {
		List<Address> list = ags.selectTeamGroupList(deptNo);
		
		mv.addObject("list", list);
		mv.addObject("type", "team");
		mv.setViewName("addr_struct");
		
		return mv;
	}

	// moxn : 부서 주소록 그룹 목록 조회
	@PostMapping("myGroupList.ad")
	public ModelAndView selectMyGroupList(ModelAndView mv, int empDivNo) {
		List<Address> list = ags.selectMyGroupList(empDivNo);

		mv.addObject("list", list);
		mv.addObject("type", "my");
		mv.setViewName("addr_struct");
		
		return mv;
	}
	
	// moxn : 기존 주소록 그룹에 사람 존재하는지 판별 : ajax
	@PostMapping("selectGrpExistOutsider.ad")
	public ModelAndView selectGrpExistOutsider(ModelAndView mv, int addrNo, HttpServletResponse response) {
		
		Boolean existSwitch = ags.selectGrpExistOutsider(addrNo);
				
		mv.addObject("existSwitch", existSwitch);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	// moxn : 그룹 수정
	@PostMapping("updateGroup.ad")
	public ModelAndView updateGroup(ModelAndView mv, String type, int empDivNo, int deptNo, 
					int[] addrNo, int[] delAddrNo, String[] addrgroup, String[] delAddrgroup) {
		
	
		ArrayList<Address> updateGroupList = new ArrayList<Address>();
		ArrayList<Address> insertGroupList = new ArrayList<Address>();
		for (int i = 0; i < addrNo.length; i++) {
			Address addr = new Address();
			addr.setAddrNo(addrNo[i]);
			addr.setAddrgroup(addrgroup[i]);
			if(addrNo[i] != 0) {
				updateGroupList.add(addr);
			}else {
				insertGroupList.add(addr);
			}
		}

	
		int result = 0;

		if(type.equals("team")) {
			result = ags.updateTeamGroup(deptNo, delAddrNo, insertGroupList, updateGroupList);
		}else {
			result = ags.updateMyGroup(empDivNo, delAddrNo, insertGroupList, updateGroupList);
		}
				
		
		if(result > 0) {
			mv.addObject("msg", "그룹이 수정되었습니다.");
			mv.addObject("view", "addr_list");
			mv.setViewName("addrAlert");
			
		}else {
			mv.addObject("msg", "그룹 수정에 실패했습니다.");
			mv.addObject("view", "addr_list");
			mv.setViewName("addrAlert");
		}
		
		return mv;
	}
	
	
}
