package com.hardcore.cw.address.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.cms.model.vo.Dept;

public interface AddrMenuService {

	ArrayList<HashMap<String, Object>> selectDeptList();

	List<Address> selectTeamGrpList(int deptNo);

	List<Address> selectEmpGrpList(int empDivNo);

	List<HashMap<String, Object>> selectEmpList();

	HashMap<String, Object> selectEmpOne(int empDivNo);

	HashMap<String, Object> selectEmpProfile(int empDivNo);

}
