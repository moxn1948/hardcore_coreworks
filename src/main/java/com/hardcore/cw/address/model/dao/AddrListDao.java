package com.hardcore.cw.address.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.address.model.vo.Memory;
import com.hardcore.cw.address.model.vo.Outsider;
import com.hardcore.cw.common.model.vo.PageInfo;

public interface AddrListDao {

	int selectEmpListCount(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectEmpList(SqlSessionTemplate sqlSession, PageInfo pi, HashMap<String, Object> map);

	int selectGrpListCount(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectGrpList(SqlSessionTemplate sqlSession, PageInfo pi, HashMap<String, Object> map);

	Outsider selectOutOne(SqlSessionTemplate sqlSession, int outNo);

	List<HashMap<String, Object>> selectMemoryOne(SqlSessionTemplate sqlSession, int outNo);

	Address selectOutAddrInfo(SqlSessionTemplate sqlSession, int addrNo);

	int updateOutsiderOneInfo(SqlSessionTemplate sqlSession, Outsider out);

	int updateOutsiderOneMemory(SqlSessionTemplate sqlSession, Memory memory);

	int deleteOutsiderOneMemory(SqlSessionTemplate sqlSession, int memoryNo);

	int deleteOutsiderOne(SqlSessionTemplate sqlSession, int grpOutNo);

	int deleteOutsiderOneMemoryAll(SqlSessionTemplate sqlSession, int grpOutNo);

}
