package com.hardcore.cw.address.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.cms.model.vo.Dept;

public interface AddrMenuDao {

	List<Dept> selectDeptList(SqlSessionTemplate sqlSession);

	List<Address> selectTeamGrpList(SqlSessionTemplate sqlSession, int deptNo);

	List<Address> selectEmpGrpList(SqlSessionTemplate sqlSession, int empDivNo);

	List<HashMap<String, Object>> selectEmpList(SqlSessionTemplate sqlSession);

	HashMap<String, Object> selectEmpOne(SqlSessionTemplate sqlSession, int empDivNo);

	HashMap<String, Object> selectEmpProfile(SqlSessionTemplate sqlSession, int empDivNo);


}
