package com.hardcore.cw.address.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.address.model.dao.AddrMenuDao;
import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.cms.model.vo.Dept;

@Service
public class AddrMenuServiceImpl implements AddrMenuService {
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private AddrMenuDao amd;
	
	
	// moxn : 사내 부서 구조 조회 메소드	
	@Override
	public ArrayList<HashMap<String, Object>> selectDeptList() {
		
		List<Dept> deptList = amd.selectDeptList(sqlSession);
		
		ArrayList<HashMap<String, Object>> deptListEdit = new ArrayList<>();
		
		
		for (int i = 0; i < deptList.size(); i++) {
			
			if(deptList.get(i).getDeptOrder() == 0) {
				HashMap<String, Object> cntMap = new HashMap<>();
				ArrayList<HashMap<String, Object>> deptOrderList = new ArrayList<>();
				
				cntMap.put("deptNo", deptList.get(i).getDeptNo());
				cntMap.put("deptName", deptList.get(i).getDeptName());
				cntMap.put("deptOrderList", deptOrderList);
				
				deptListEdit.add(cntMap);
			}
			
		}

		for (int i = 0; i < deptListEdit.size(); i++) {
			for (int j = 0; j < deptList.size(); j++) {
				if(deptList.get(j).getDeptOrder() == (Integer) deptListEdit.get(i).get("deptNo")) {
					HashMap<String, Object> cntMap = new HashMap<>();
					ArrayList<HashMap<String, Object>> deptOrderList = new ArrayList<>();
					
					cntMap.put("deptNo", deptList.get(j).getDeptNo());
					cntMap.put("deptName", deptList.get(j).getDeptName());
					cntMap.put("deptOrderList", deptOrderList);

					((ArrayList<HashMap<String, Object>>) deptListEdit.get(i).get("deptOrderList")).add(cntMap);
					
				}
			}
		}
		
		
		for (int i = 0; i < deptListEdit.size(); i++) {
			ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) deptListEdit.get(i).get("deptOrderList");
			
			
			for (int j = 0; j < list.size(); j++) {
				
				for (int k = 0; k < deptList.size(); k++) {

					if(deptList.get(k).getDeptOrder() == (Integer) list.get(j).get("deptNo")) {
						
						HashMap<String, Object> cntMap = new HashMap<>();
						
						cntMap.put("deptNo", deptList.get(k).getDeptNo());
						cntMap.put("deptName", deptList.get(k).getDeptName());

						((ArrayList<HashMap<String, Object>>) list.get(j).get("deptOrderList")).add(cntMap);
						
					}
				}
			}
		}
		
		return deptListEdit;
	}


	// moxn : 공용 주소록 그룹 조회
	@Override
	public List<Address> selectTeamGrpList(int deptNo) {
		
		return amd.selectTeamGrpList(sqlSession, deptNo);
	}


	// moxn : 내 주소록 부서 조회 메소드
	@Override
	public List<Address> selectEmpGrpList(int empDivNo) {
		
		return amd.selectEmpGrpList(sqlSession, empDivNo);
	}


	// moxn : 사원 목록 조회 메소드 : 시퀀스사번, 이름, 부서코드
	@Override
	public List<HashMap<String, Object>> selectEmpList() {
		
		return amd.selectEmpList(sqlSession);
	}

	// moxn : 사원 한명 정보 조회 메소드
	@Override
	public HashMap<String, Object> selectEmpOne(int empDivNo) {
		
		return amd.selectEmpOne(sqlSession, empDivNo);
	}

	// moxn : 사원 한명 프로필 사진 조회
	@Override
	public HashMap<String, Object> selectEmpProfile(int empDivNo) {
		
		return amd.selectEmpProfile(sqlSession, empDivNo);
	}

}
