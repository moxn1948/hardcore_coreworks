package com.hardcore.cw.address.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Address implements java.io.Serializable{
   private int addrNo;
   private String addrgroup;
   private String type;
   private int deptNo;
   private int empDivNo;
}