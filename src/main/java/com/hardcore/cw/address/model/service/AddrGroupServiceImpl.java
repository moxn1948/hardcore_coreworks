package com.hardcore.cw.address.model.service;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.address.model.dao.AddrGroupDao;
import com.hardcore.cw.address.model.vo.Address;

@Service
public class AddrGroupServiceImpl implements AddrGroupService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private AddrGroupDao agd;
	
	// moxn : 공용 주소록 : 그룹 목록
	@Override
	public List<Address> selectTeamGroupList(int deptNo) {
		
		return agd.selectTeamGroupList(sqlSession, deptNo);
	}
	
	// moxn : 내 주소록 : 그룹 목록
	@Override
	public List<Address> selectMyGroupList(int empDivNo) {
		
		return agd.selectMyGroupList(sqlSession, empDivNo);
	}
	
	// moxn : 기존 그룹에 외부인 있는지 없는지 판별
	@Override
	public Boolean selectGrpExistOutsider(int addrNo) {
		int count = agd.selectGrpExistOutsider(sqlSession, addrNo); 
		
		if(count > 0) {
			return true;
		}else {
			return false;
		}
	}
	
	// moxn : 공용주소록 그룹 수정
	@Override
	public int updateTeamGroup(int deptNo, int[] delAddrNo, ArrayList<Address> insertGroupList,
			ArrayList<Address> updateGroupList) {
		int result = 0;
		
		if(delAddrNo != null) {
			for (int i = 0; i < delAddrNo.length; i++) {
				result += agd.deleteGroupOne(sqlSession, delAddrNo[i]);
			}	
		}
		
		for (int i = 0; i < updateGroupList.size(); i++) {
			result += agd.updateGroupOne(sqlSession, updateGroupList.get(i));
		}
		
		for (int i = 0; i < insertGroupList.size(); i++) {
			insertGroupList.get(i).setDeptNo(deptNo);
			result += agd.insertGroupOne(sqlSession, insertGroupList.get(i));
		}
		
		return result;
	}

	// moxn : 내 주소록 그룹 수정
	@Override
	public int updateMyGroup(int empDivNo, int[] delAddrNo, ArrayList<Address> insertGroupList,
			ArrayList<Address> updateGroupList) {
		int result = 0;

		if(delAddrNo != null) {
			for (int i = 0; i < delAddrNo.length; i++) {
				result += agd.deleteGroupOne(sqlSession, delAddrNo[i]);
			}	
		}
		
		for (int i = 0; i < updateGroupList.size(); i++) {
			result += agd.updateGroupOne(sqlSession, updateGroupList.get(i));
		}
		
		for (int i = 0; i < insertGroupList.size(); i++) {
			insertGroupList.get(i).setEmpDivNo(empDivNo);
			result += agd.insertGroupOne(sqlSession, insertGroupList.get(i));
		}
		
		return result;
	}
	
}
