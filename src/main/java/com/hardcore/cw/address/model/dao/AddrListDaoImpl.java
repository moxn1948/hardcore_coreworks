package com.hardcore.cw.address.model.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.address.model.vo.Memory;
import com.hardcore.cw.address.model.vo.Outsider;
import com.hardcore.cw.common.model.vo.PageInfo;

@Repository
public class AddrListDaoImpl implements AddrListDao{

	// moxn : 사내 주소록 목록 개수 조회 서비스
	@Override
	public int selectEmpListCount(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {
		
		return sqlSession.selectOne("Address.selectPagingEmpListCount", map);
	}

	// moxn : 사내 주소록 목록 리스트 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectEmpList(SqlSessionTemplate sqlSession, PageInfo pi, HashMap<String, Object> map) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		
		return sqlSession.selectList("Address.selectPagingEmpList", map, rowBounds);
	}

	// moxn : 공용&내 주소록 목록 개수 조회 서비스
	@Override
	public int selectGrpListCount(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {
		
		return sqlSession.selectOne("Address.selectPagingGrpListCount", map);
	}

	// moxn : 사내 주소록 목록 리스트 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectGrpList(SqlSessionTemplate sqlSession, PageInfo pi, HashMap<String, Object> map) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		
		return sqlSession.selectList("Address.selectPagingGrpList", map, rowBounds);
	}

	// moxn :외부인 한명 조회 서비스
	@Override
	public Outsider selectOutOne(SqlSessionTemplate sqlSession, int outNo) {

		return sqlSession.selectOne("Address.selectOutOne", outNo);
	}

	// moxn : 외부인 한명 기념일 조회 서비스
	@Override	
	public List<HashMap<String, Object>> selectMemoryOne(SqlSessionTemplate sqlSession, int outNo) {

		return sqlSession.selectList("Address.selectMemoryOne", outNo);
	}

	// moxn : 외부인 수정 팝업 주소록 정보 조회 서비스
	@Override
	public Address selectOutAddrInfo(SqlSessionTemplate sqlSession, int addrNo) {
		
		return sqlSession.selectOne("Address.selectOutAddrInfo", addrNo);
	}

	// moxn : 외부인 기본 정보 수정 서비스
	@Override
	public int updateOutsiderOneInfo(SqlSessionTemplate sqlSession, Outsider out) {
		
		return sqlSession.update("Address.updateOutsiderOneInfo", out);
	}

	// moxn : 외부인 기존 기념일 수정 서비스
	@Override
	public int updateOutsiderOneMemory(SqlSessionTemplate sqlSession, Memory memory) {
		
		return sqlSession.update("Address.updateOutsiderOneMemory", memory);
	}

	// moxn : 외부인 기존 기념일 삭제 서비스
	@Override
	public int deleteOutsiderOneMemory(SqlSessionTemplate sqlSession, int memoryNo) {
		
		return sqlSession.delete("Address.deleteOutsiderOneMemoryAll", memoryNo);
	}

	// moxn : 외부인 한명 삭제 서비스
	@Override
	public int deleteOutsiderOne(SqlSessionTemplate sqlSession, int grpOutNo) {

		return sqlSession.update("Address.updateOutsiderOneDel", grpOutNo);
	}

	// moxn : 외부인 한명의 모든 기념일 삭제 메소드
	@Override
	public int deleteOutsiderOneMemoryAll(SqlSessionTemplate sqlSession, int grpOutNo) {
		
		return sqlSession.delete("Address.deleteOutsiderOneMemoryAll", grpOutNo);
	}
	


}
