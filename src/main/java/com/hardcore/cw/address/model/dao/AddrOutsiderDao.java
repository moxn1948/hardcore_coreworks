package com.hardcore.cw.address.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.address.model.vo.Memory;
import com.hardcore.cw.address.model.vo.Outsider;

public interface AddrOutsiderDao {

	List<Address> selectOutDeptGrpList(SqlSessionTemplate sqlSession, int deptNo);

	List<Address> selectOutEmpGrpList(SqlSessionTemplate sqlSession, int empDivNo);

	int insertOutsiderInfo(SqlSessionTemplate sqlSession, Outsider out);

	int insertOutsiderMemory(SqlSessionTemplate sqlSession, Memory memory);

}
