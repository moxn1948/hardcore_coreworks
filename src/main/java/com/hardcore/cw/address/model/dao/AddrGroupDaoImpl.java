package com.hardcore.cw.address.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.address.model.vo.Address;

@Repository
public class AddrGroupDaoImpl implements AddrGroupDao{

	// moxn : 공용 주소록 : 그룹 목록
	@Override
	public List<Address> selectTeamGroupList(SqlSessionTemplate sqlSession, int deptNo) {
		
		return sqlSession.selectList("Address.selectTeamGroupList", deptNo);
	}

	// moxn : 내 주소록 : 그룹 목록
	@Override
	public List<Address> selectMyGroupList(SqlSessionTemplate sqlSession, int empDivNo) {
		
		return sqlSession.selectList("Address.selectMyGroupList", empDivNo);
	}

	// moxn : 기존 그룹에 외부인 있는지 없는지 판별
	@Override
	public int selectGrpExistOutsider(SqlSessionTemplate sqlSession, int addrNo) {
		
		return sqlSession.selectOne("Address.selectGrpExistOutsider", addrNo);
	}
	
	// moxn : 기존 그룹 삭제
	@Override
	public int deleteGroupOne(SqlSessionTemplate sqlSession, int delAddrNo) {

		return sqlSession.delete("Address.deleteGroupOne", delAddrNo);
	}

	// moxn : 기존 그룹명 수정
	@Override
	public int updateGroupOne(SqlSessionTemplate sqlSession, Address address) {

		return sqlSession.update("Address.updateGroupOne", address);
	}

	// moxn : 그룹 추가
	@Override
	public int insertGroupOne(SqlSessionTemplate sqlSession, Address address) {

		return sqlSession.insert("Address.insertGroupOne", address);
	}

}
