package com.hardcore.cw.address.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.address.model.vo.Memory;
import com.hardcore.cw.address.model.vo.Outsider;

@Repository
public class AddrOutsiderDaoImpl implements AddrOutsiderDao{

	// moxn : 연락처 추가 : 그룹 목록 조회 : 공용 주소록
	@Override
	public List<Address> selectOutDeptGrpList(SqlSessionTemplate sqlSession, int deptNo) {
		
		return sqlSession.selectList("Address.selectOutDeptGrpList", deptNo);
	}

	// moxn : 연락처 추가 : 그룹 목록 조회 : 내 주소록
	@Override
	public List<Address> selectOutEmpGrpList(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.selectList("Address.selectOutEmpGrpList", empDivNo);
	}
	
	// moxn : 외부인 추가 : 외부인 정보 추가
	@Override
	public int insertOutsiderInfo(SqlSessionTemplate sqlSession, Outsider out) {
		sqlSession.insert("Address.insertOutsiderInfo", out);
		
		// 외부인 시퀀스 currval 리턴 
		return out.getOutNo();
	}
	
	// moxn : 외부인 추가 : 기념일 추가
	@Override
	public int insertOutsiderMemory(SqlSessionTemplate sqlSession, Memory memory) {
		
		return sqlSession.insert("Address.insertOutsiderMemory",memory);
	}

}
