package com.hardcore.cw.address.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.address.model.vo.Memory;
import com.hardcore.cw.address.model.vo.Outsider;
import com.hardcore.cw.common.model.vo.PageInfo;

public interface AddrListService {

	int selectEmpListCount(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectEmpList(PageInfo pi, HashMap<String, Object> map);

	int selectGrpListCount(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectGrpList(PageInfo pi, HashMap<String, Object> map);

	Outsider selectOutOne(int outNo);

	List<HashMap<String, Object>> selectMemoryOne(int outNo);

	Address selectOutAddrInfo(int addrNo);

	int updateOutsiderOne(Outsider out, ArrayList<Memory> memoryList);

	int deleteOutsiderOne(int grpOutNo);

}
