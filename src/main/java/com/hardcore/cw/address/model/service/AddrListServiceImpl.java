package com.hardcore.cw.address.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.address.model.dao.AddrListDao;
import com.hardcore.cw.address.model.dao.AddrOutsiderDao;
import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.address.model.vo.Memory;
import com.hardcore.cw.address.model.vo.Outsider;
import com.hardcore.cw.common.model.vo.PageInfo;

@Service
public class AddrListServiceImpl implements AddrListService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private AddrListDao ald;
	@Autowired
	private AddrOutsiderDao aod;

	// moxn : 사내 주소록 목록 개수 조회 서비스
	@Override
	public int selectEmpListCount(HashMap<String, Object> map) {
		
		return ald.selectEmpListCount(sqlSession, map);
	}

	// moxn : 사내 주소록 목록 리스트 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectEmpList(PageInfo pi, HashMap<String, Object> map) {
		
		return ald.selectEmpList(sqlSession, pi, map);
	}
	
	// moxn : 공용&내 주소록 목록 개수 조회 서비스
	@Override
	public int selectGrpListCount(HashMap<String, Object> map) {
		
		return ald.selectGrpListCount(sqlSession, map);
	}

	// moxn : 공용&내 주소록 목록 리스트 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectGrpList(PageInfo pi, HashMap<String, Object> map) {
		
		return ald.selectGrpList(sqlSession, pi, map);
	}

	// moxn :외부인 한명 조회 서비스
	@Override
	public Outsider selectOutOne(int outNo) {

		return ald.selectOutOne(sqlSession, outNo);
	}
	
	// moxn : 외부인 한명 기념일 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectMemoryOne(int outNo) {

		return ald.selectMemoryOne(sqlSession, outNo);
	}
	
	// moxn : 외부인 수정 팝업 주소록 정보 조회 서비스
	@Override
	public Address selectOutAddrInfo(int addrNo) {
		
		return ald.selectOutAddrInfo(sqlSession, addrNo);
	}
	
	// moxn : 외부인 수정 서비스
	@Override
	public int updateOutsiderOne(Outsider out, ArrayList<Memory> memoryList) {
		int result = 0;
		
		// 외부인 정보 수정
		int result1 = ald.updateOutsiderOneInfo(sqlSession, out);

		// 기념일 정보 삽입
		int result2 = 0;
		if(memoryList.size() != 0) {
			// 기존 기념일 정보
			// List<HashMap<String, Object>> alreadyMemory = ald.selectMemoryOne(sqlSession, out.getOutNo());
			result2 += ald.deleteOutsiderOneMemory(sqlSession, memoryList.get(0).getOutNo());
			
			for (int i = 0; i < memoryList.size(); i++) {
				result2 += aod.insertOutsiderMemory(sqlSession, (Memory) memoryList.get(i));
			}
			
		}
		if(memoryList.size() != 0) {
			if(result1 > 0 && result2 > 0) {
				result++;
			}
		}else {
			if(result1 > 0) {
				result++;
			}
		}
		
		return result;
	}
	
	// moxn : 외부인 삭제 서비스
	@Override
	public int deleteOutsiderOne(int grpOutNo) {

		int result = 0;
		
		result += ald.deleteOutsiderOne(sqlSession, grpOutNo);
		result += ald.deleteOutsiderOneMemoryAll(sqlSession, grpOutNo);
		
		return result;
	}

}
