package com.hardcore.cw.address.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.cms.model.vo.Dept;

@Repository
public class AddrMenuDaoImpl implements AddrMenuDao{

	// moxn : 사내 부서 구조 조회 메소드
	@Override
	public List<Dept> selectDeptList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Address.selectDeptList");
	}

	// moxn : 공용 주소록 부서 조회 메소드
	@Override
	public List<Address> selectTeamGrpList(SqlSessionTemplate sqlSession, int deptNo) {
		return sqlSession.selectList("Address.selectTeamGrpList", deptNo);
	}
	
	// moxn : 내 주소록 부서 조회 메소드
	@Override
	public List<Address> selectEmpGrpList(SqlSessionTemplate sqlSession, int empDivNo) {
		return sqlSession.selectList("Address.selectEmpGrpList", empDivNo);
	}

	// moxn : 사원 목록 조회 메소드 : 시퀀스사번, 이름, 부서코드
	@Override
	public List<HashMap<String, Object>> selectEmpList(SqlSessionTemplate sqlSession) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("Address.selectEmpList");
	}
	
	// moxn : 사원 한명 정보 조회 메소드
	@Override
	public HashMap<String, Object> selectEmpOne(SqlSessionTemplate sqlSession, int empDivNo) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Address.selectEmpOne", empDivNo);
	}

	// moxn : 사원 한명 프로필 사진 조회
	@Override
	public HashMap<String, Object> selectEmpProfile(SqlSessionTemplate sqlSession, int empDivNo) {
		
		return sqlSession.selectOne("Address.selectEmpProfile", empDivNo);
	}

}
