package com.hardcore.cw.address.model.service;

import java.util.ArrayList;
import java.util.List;

import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.address.model.vo.Memory;
import com.hardcore.cw.address.model.vo.Outsider;

public interface AddrOutsiderService {

	List<Address> selectOutDeptGrpList(int deptNo);

	List<Address> selectOutEmpGrpList(int empDivNo);

	int insertOutsider(Outsider out, ArrayList<Memory> memoryList);

}
