package com.hardcore.cw.address.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.address.model.vo.Address;

public interface AddrGroupDao {

	List<Address> selectTeamGroupList(SqlSessionTemplate sqlSession, int deptNo);

	List<Address> selectMyGroupList(SqlSessionTemplate sqlSession, int empDivNo);

	int selectGrpExistOutsider(SqlSessionTemplate sqlSession, int addrNo);

	int deleteGroupOne(SqlSessionTemplate sqlSession, int delAddrNo);

	int updateGroupOne(SqlSessionTemplate sqlSession, Address address);

	int insertGroupOne(SqlSessionTemplate sqlSession, Address address);

}
