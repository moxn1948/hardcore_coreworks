package com.hardcore.cw.address.model.service;

import java.util.ArrayList;
import java.util.List;

import com.hardcore.cw.address.model.vo.Address;

public interface AddrGroupService {

	List<Address> selectTeamGroupList(int deptNo);

	List<Address> selectMyGroupList(int empDivNo);

	Boolean selectGrpExistOutsider(int addrNo);

	int updateTeamGroup(int deptNo, int[] delAddrNo, ArrayList<Address> insertGroupList,
			ArrayList<Address> updateGroupList);

	int updateMyGroup(int empDivNo, int[] delAddrNo, ArrayList<Address> insertGroupList,
			ArrayList<Address> updateGroupList);

}
