package com.hardcore.cw.address.model.service;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.address.model.dao.AddrOutsiderDao;
import com.hardcore.cw.address.model.vo.Address;
import com.hardcore.cw.address.model.vo.Memory;
import com.hardcore.cw.address.model.vo.Outsider;

@Service
public class AddrOutsiderServiceImpl implements AddrOutsiderService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private AddrOutsiderDao aod;
	
	// moxn : 연락처 추가 : 그룹 목록 조회 : 공용 주소록
	@Override
	public List<Address> selectOutDeptGrpList(int deptNo) {
		
		return aod.selectOutDeptGrpList(sqlSession, deptNo);
	}

	// moxn : 연락처 추가 : 그룹 목록 조회 : 내 주소록
	@Override
	public List<Address> selectOutEmpGrpList(int empDivNo) {
		
		return aod.selectOutEmpGrpList(sqlSession, empDivNo);
	}
	
	// moxn : 외부인 추가
	@Override
	public int insertOutsider(Outsider out, ArrayList<Memory> memoryList) {
		int result = 0;
		
		// 외부인 정보 삽입
		int outNo = aod.insertOutsiderInfo(sqlSession, out);
		
		// 기념일 정보 삽입
		int result1 = 0;
		if(memoryList.size() != 0) {
			for (int i = 0; i < memoryList.size(); i++) {
				memoryList.get(i).setOutNo(outNo);
				result1 += aod.insertOutsiderMemory(sqlSession, memoryList.get(i));
			}
		}
		
		if(outNo > 0 && result1 == memoryList.size()) {
			result++;
		}
		
		return result;
	}
}
