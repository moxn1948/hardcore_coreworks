package com.hardcore.cw.calendar.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Associate implements java.io.Serializable{
	private int empDivNo;
	private int calNo;
}
