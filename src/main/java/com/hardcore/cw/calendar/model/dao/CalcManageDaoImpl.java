package com.hardcore.cw.calendar.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.calendar.model.vo.Associate;
import com.hardcore.cw.calendar.model.vo.Calendar;

@Repository
public class CalcManageDaoImpl implements CalcManageDao{

	// moxn : 새 일정 등록 : 일정 기본 정보
	@Override
	public int insertCalcInfo(SqlSessionTemplate sqlSession, Calendar calc) {
		sqlSession.insert("Calendar.insertCalcInfo", calc);
		
		return calc.getCalNo();
	}

	// moxn : 새 일정 등록 : 관련자 정보
	@Override
	public int insertCalcAsso(SqlSessionTemplate sqlSession, Associate asso) {
		
		return sqlSession.insert("Calendar.insertCalcAsso", asso);
	}
	
	// moxn : 개인 일정 조회
	@Override
	public List<HashMap<String, Object>> selectSoloCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {
		
		return sqlSession.selectList("Calendar.selectSoloCalcList", map);
	}

	// moxn : 부서 일정 조회
	@Override
	public List<HashMap<String, Object>> selectDeptCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {
		
		return sqlSession.selectList("Calendar.selectDeptCalcList", map);
	}

	// moxn : 단체 일정 조회
	@Override
	public List<HashMap<String, Object>> selectGroupCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {
		
		return sqlSession.selectList("Calendar.selectGroupCalcList", map);
	}

	// moxn : 회사 일정 조회
	@Override
	public List<HashMap<String, Object>> selectCompCalcList(SqlSessionTemplate sqlSession,
			HashMap<String, Object> map) {
		
		return sqlSession.selectList("Calendar.selectCompCalcList", map);
	}

	// moxn : 시스템일정 : 부재 리스트 조회
	@Override
	public List<HashMap<String, Object>> selectSystemAbsCalcList(SqlSessionTemplate sqlSession,
			HashMap<String, Object> map) {
		
		return sqlSession.selectList("Calendar.selectSystemAbsCalcList", map);
	}

	// moxn : 시스템일정 : 휴가 리스트 조회
	@Override
	public List<HashMap<String, Object>> selectSystemVaCalcList(SqlSessionTemplate sqlSession,
			HashMap<String, Object> map) {

		return sqlSession.selectList("Calendar.selectSystemVaCalcList", map);
	}

	// moxn : 시스템일정 : 연차 리스트 조회
	@Override
	public List<HashMap<String, Object>> selectSystemAnnCalcList(SqlSessionTemplate sqlSession,
			HashMap<String, Object> map) {
		
		return sqlSession.selectList("Calendar.selectSystemAnnCalcList", map);
	}

	// moxn : 관리자용 부서 일정 리스트 조회
	@Override
	public List<HashMap<String, Object>> selectAdmDeptCalcList(SqlSessionTemplate sqlSession,
			HashMap<String, Object> map) {

		return sqlSession.selectList("Calendar.selectAdmDeptCalcList", map);
	}

	// moxn : 일정 한개 상세 조회 서비스
	@Override
	public HashMap<String, Object> selectDetCalOne(SqlSessionTemplate sqlSession, int calNo) {

		return sqlSession.selectOne("Calendar.selectDetCalOne", calNo);
	}

	// moxn : 일정 한개 상세 조회 서비스 : 관련자
	@Override
	public List<HashMap<String, Object>> selectDetCalOneAsso(SqlSessionTemplate sqlSession, int calNo) {

		return sqlSession.selectList("Calendar.selectDetCalOneAsso", calNo);
	}
	
	// moxn : 일정 한개 삭제 서비스
	@Override
	public int deleteCalcOne(SqlSessionTemplate sqlSession, int calNo) {

		return sqlSession.update("Calendar.deleteCalcOne", calNo);
	}
	
	// moxn : 일정 한개 수정 : 일정 기본 정보
	@Override
	public int updateCalcOneInfo(SqlSessionTemplate sqlSession, Calendar calc) {
		
		return sqlSession.update("Calendar.updateCalcOneInfo", calc);
	}
	
	// moxn : 일정 한개 수정 : 기존 관련자 삭제
	@Override
	public int deleteCalcOneAsso(SqlSessionTemplate sqlSession, int calNo) {

		return sqlSession.delete("Calendar.deleteCalcOneAsso", calNo);
	}

	// moxn : 일정 한개 수정 : 관련자 재 등록
	@Override
	public int updateCalcOneNewAsso(SqlSessionTemplate sqlSession, HashMap<String, Object> hashMap) {

		return sqlSession.insert("Calendar.updateCalcOneNewAsso", hashMap);
	}

	@Override
	public int updateCalcDrag(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.update("Calendar.updateCalcDrag", map);
	}

}
