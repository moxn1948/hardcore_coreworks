package com.hardcore.cw.calendar.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.calendar.model.vo.Associate;
import com.hardcore.cw.calendar.model.vo.Calendar;

public interface CalcManageDao {

	int insertCalcInfo(SqlSessionTemplate sqlSession, Calendar calc);

	int insertCalcAsso(SqlSessionTemplate sqlSession, Associate asso);

	List<HashMap<String, Object>> selectSoloCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectDeptCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectGroupCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectCompCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectSystemAbsCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectSystemVaCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectSystemAnnCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectAdmDeptCalcList(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	HashMap<String, Object> selectDetCalOne(SqlSessionTemplate sqlSession, int calNo);

	List<HashMap<String, Object>> selectDetCalOneAsso(SqlSessionTemplate sqlSession, int calNo);

	int deleteCalcOne(SqlSessionTemplate sqlSession, int calNo);

	int updateCalcOneInfo(SqlSessionTemplate sqlSession, Calendar calc);

	int deleteCalcOneAsso(SqlSessionTemplate sqlSession, int calNo);

	int updateCalcOneNewAsso(SqlSessionTemplate sqlSession, HashMap<String, Object> hashMap);

	int updateCalcDrag(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

}
