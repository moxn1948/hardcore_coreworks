package com.hardcore.cw.calendar.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.calendar.model.dao.CalcManageDao;
import com.hardcore.cw.calendar.model.vo.Associate;
import com.hardcore.cw.calendar.model.vo.Calendar;

@Service
public class CalcManageServiceImpl implements CalcManageService{
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private CalcManageDao cmd;
	
	// moxn : 새 일정 등록 서비스
	@Override
	public int insertCalc(Calendar calc, int[] eNo) {
		int result  = 0;
		
		int calNo  = 0;
		int result2  = 0;
		
		calNo = cmd.insertCalcInfo(sqlSession, calc);
		
		if(eNo != null) {
			for (int i = 0; i < eNo.length; i++) {
				Associate asso = new Associate();
				
				asso.setCalNo(calNo);
				asso.setEmpDivNo(eNo[i]);
				
				result2 += cmd.insertCalcAsso(sqlSession, asso);
			}
			
		}
		
		if(eNo != null) {
			if(calNo > 0 && result2 > 0) {
				result++;
			}	
		}else {
			if(calNo > 0) {
				result++;
			}
		}
		
		return result;
	}
	
	
	// moxn : 개인 일정 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectSoloCalcList(HashMap<String, Object> map) {
		return cmd.selectSoloCalcList(sqlSession, map);
	}


	// moxn : 부서 일정 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectDeptCalcList(HashMap<String, Object> map) {
		
		return cmd.selectDeptCalcList(sqlSession, map);
	}

	// moxn : 단체 일정 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectGroupCalcList(HashMap<String, Object> map) {
		
		return cmd.selectGroupCalcList(sqlSession, map);
	}


	// moxn : 회사 일정 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectCompCalcList(HashMap<String, Object> map) {

		return cmd.selectCompCalcList(sqlSession, map);
	}


	// moxn : 시스템일정 : 부재 리스트 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectSystemAbsCalcList(HashMap<String, Object> map) {

		return cmd.selectSystemAbsCalcList(sqlSession, map);
	}


	// moxn : 시스템일정 : 휴가 리스트 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectSystemVaCalcList(HashMap<String, Object> map) {
		
		return cmd.selectSystemVaCalcList(sqlSession, map);
	}

	// moxn : 시스템일정 : 연차 리스트 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectSystemAnnCalcList(HashMap<String, Object> map) {

		return cmd.selectSystemAnnCalcList(sqlSession, map);
	}

	// moxn : 관리자용 부서 일정 리스트 조회 서비스
	@Override
	public List<HashMap<String, Object>> selectAdmDeptCalcList(HashMap<String, Object> map) {

		return cmd.selectAdmDeptCalcList(sqlSession, map);
	}

	// moxn : 일정 한개 상세 조회 서비스
	@Override
	public HashMap<String, Object> selectDetCalOne(int calNo) {
		
		return cmd.selectDetCalOne(sqlSession, calNo);
	}

	// moxn : 일정 한개 상세 조회 서비스 : 관련자
	@Override
	public List<HashMap<String, Object>> selectDetCalOneAsso(int calNo) {
		
		return cmd.selectDetCalOneAsso(sqlSession, calNo);
	}

	// moxn : 일정 한개 삭제 서비스
	@Override
	public int deleteCalcOne(int calNo) {
		
		return cmd.deleteCalcOne(sqlSession, calNo);
	}

	// moxn : 일정 한개 수정 서비스
	@Override
	public int updateCalcOne(Calendar calc, ArrayList<HashMap<String, Object>> newAsso) {
		int result  = 0;
		
		int result1  = 0;
		int result2  = 0;
		
		result1 = cmd.updateCalcOneInfo(sqlSession, calc);
		
		result2 += cmd.deleteCalcOneAsso(sqlSession, calc.getCalNo());
		if(newAsso.size() != 0) {
			for (int i = 0; i < newAsso.size(); i++) {
				result2 += cmd.updateCalcOneNewAsso(sqlSession, newAsso.get(i));
			}
		}
		
		if(newAsso.size() != 0) {
			if(result1 > 0 && result2 > 0) {
				result++;
			}	
		}else {
			if(result1 > 0) {
				result++;
			}
		}
		
		return result;
	}


	// moxn : 개인 일정 드래그 시 수정
	@Override
	public int updateCalcDrag(HashMap<String, Object> map) {

		return cmd.updateCalcDrag(sqlSession, map);
	}

}
