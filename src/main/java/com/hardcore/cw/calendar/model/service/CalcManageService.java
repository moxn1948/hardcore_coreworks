package com.hardcore.cw.calendar.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hardcore.cw.calendar.model.vo.Calendar;

public interface CalcManageService {

	int insertCalc(Calendar calc, int[] eNo);

	List<HashMap<String, Object>> selectSoloCalcList(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectDeptCalcList(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectGroupCalcList(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectCompCalcList(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectSystemAbsCalcList(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectSystemVaCalcList(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectSystemAnnCalcList(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectAdmDeptCalcList(HashMap<String, Object> map);

	HashMap<String, Object> selectDetCalOne(int calNo);

	List<HashMap<String, Object>> selectDetCalOneAsso(int calNo);

	int deleteCalcOne(int calNo);

	int updateCalcOne(Calendar calc, ArrayList<HashMap<String, Object>> newAsso);

	int updateCalcDrag(HashMap<String, Object> map);


}
