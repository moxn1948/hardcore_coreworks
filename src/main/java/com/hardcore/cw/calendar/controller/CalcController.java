package com.hardcore.cw.calendar.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.calendar.model.service.CalcManageService;
import com.hardcore.cw.calendar.model.vo.Calendar;
import com.hardcore.cw.common.model.vo.LoginEmp;

@Controller
public class CalcController {
	private static final Logger logger = LoggerFactory.getLogger(CalcController.class);
	
	@Autowired
	private AddrMenuService ams;
	
	@Autowired
	private CalcManageService cms;

	// moxn : 일정 메인 페이지 연결
	@RequestMapping("calcMain.ca")
	public ModelAndView addrMain(ModelAndView mv, HttpServletRequest request) {
		
		LoginEmp e = (LoginEmp) request.getSession().getAttribute("loginUser");
		int empDivNo = e.getEmpDivNo(); 
		int deptNo = e.getDeptNo(); 
		
		// 사내 부서 구조 조회
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();


		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		
		mv.setViewName("calc_main");
		
		return mv;
	}

	// moxn : 새 일정 등록
	@PostMapping("insertNewCalc.ca")
	public ModelAndView insertNewCalc(ModelAndView mv, Calendar calc, int[] eNo,
									String newCalSdate, String newCalEdate, String newAllDay,
									int newDeptNo
									) {
		
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy/MM/dd");
		
		// String to Date Format
		try {
			if(calc.getAllYn() == null) {
				calc.setCalSdate(new java.sql.Date(transFormat.parse(newCalSdate).getTime()));
				calc.setCalEdate(new java.sql.Date(transFormat.parse(newCalEdate).getTime()));
			}else if(calc.getAllYn().equals("on")) {
				calc.setCalSdate(new java.sql.Date(transFormat.parse(newAllDay).getTime()));
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if(calc.getAllYn() == null) {
			calc.setAllYn("N");
		}else if(calc.getAllYn().equals("on")) {
			calc.setAllYn("Y");
			calc.setCalStime(null);
			calc.setCalEtime(null);
		}
		
		if(calc.getCalKind().equals("DEPT")) {
			calc.setDeptNo(newDeptNo);
		}
		
		int result = cms.insertCalc(calc, eNo);
		
		if(result > 0) {
			mv.addObject("msg", "일정이 추가되었습니다.");
			mv.addObject("view", "calc_main");
			mv.setViewName("calcAlert");
			
		}else {
			mv.addObject("msg", "일정 추가에 실패했습니다.");
			mv.addObject("view", "calc_main");
			mv.setViewName("calcAlert");
		}
		
		
		return mv;
	}
	
	// moxn : 개인 일정 리스트 조회 : ajax
	@PostMapping("selectSoloCalcList.ca")
	public ModelAndView selectSoloCalcList(ModelAndView mv, int empDivNo, String calcSdate, String calcEdate, HttpServletResponse response) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("empDivNo", empDivNo);
		map.put("calcSdate", calcSdate);
		map.put("calcEdate", calcEdate);
		
		List<HashMap<String, Object>> soloList = cms.selectSoloCalcList(map);
		
		mv.addObject("list", soloList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
		
	}

	// moxn : 부서 일정 리스트 조회 : ajax
	@PostMapping("selectDeptCalcList.ca")
	public ModelAndView selectDeptCalcList(ModelAndView mv, int deptNo, String calcSdate, String calcEdate, HttpServletResponse response) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("deptNo", deptNo);
		map.put("calcSdate", calcSdate);
		map.put("calcEdate", calcEdate);
		
		List<HashMap<String, Object>> deptList = cms.selectDeptCalcList(map);
		
		mv.addObject("list", deptList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
		
	}

	// moxn : 단체 일정 리스트 조회 : ajax
	@PostMapping("selectGroupCalcList.ca")
	public ModelAndView selectGroupCalcList(ModelAndView mv, int empDivNo, String calcSdate, String calcEdate, HttpServletResponse response) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("empDivNo", empDivNo);
		map.put("calcSdate", calcSdate);
		map.put("calcEdate", calcEdate);
		
		List<HashMap<String, Object>> groupList = cms.selectGroupCalcList(map);
		
		mv.addObject("list", groupList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
		
	}

	// moxn : 회사 일정 리스트 조회 : ajax
	@PostMapping("selectCompCalcList.ca")
	public ModelAndView selectCompCalcList(ModelAndView mv, String calcSdate, String calcEdate, HttpServletResponse response) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("calcSdate", calcSdate);
		map.put("calcEdate", calcEdate);
		
		List<HashMap<String, Object>> compList = cms.selectCompCalcList(map);
		
		mv.addObject("list", compList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
		
	}
	

	// moxn : 시스템일정 : 부재 리스트 조회 : ajax
	@PostMapping("selectSystemAbsCalcList.ca")
	public ModelAndView selectSystemAbsCalcList(ModelAndView mv, int deptNo, String calcSdate, String calcEdate, HttpServletResponse response) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("deptNo", deptNo);
		map.put("calcSdate", calcSdate);
		map.put("calcEdate", calcEdate);
		
		List<HashMap<String, Object>> sysList = cms.selectSystemAbsCalcList(map);
		
		mv.addObject("list", sysList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
		
	}
	

	// moxn : 시스템일정 : 휴가 리스트 조회 : ajax
	@PostMapping("selectSystemVaCalcList.ca")
	public ModelAndView selectSystemVaCalcList(ModelAndView mv, String calcSdate, String calcEdate, String nowYear, HttpServletResponse response) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("calcSdate", calcSdate);
		map.put("calcEdate", calcEdate);
		map.put("nowYear", nowYear);
		
		List<HashMap<String, Object>> sysList = cms.selectSystemVaCalcList(map);
		
		mv.addObject("list", sysList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
		
	}

	// moxn : 시스템일정 : 연차 리스트 조회 : ajax
	@PostMapping("selectSystemAnnCalcList.ca")
	public ModelAndView selectSystemAnnCalcList(ModelAndView mv, String calcSdate, String calcEdate, int deptNo, HttpServletResponse response) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("deptNo", deptNo);
		map.put("calcSdate", calcSdate);
		map.put("calcEdate", calcEdate);
		
		List<HashMap<String, Object>> sysList = cms.selectSystemAnnCalcList(map);
		
		mv.addObject("list", sysList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
		
	}

	// moxn : 관리자용 부서 일정 리스트 조회 : ajax
	@PostMapping("selectAdmDeptCalcList.ca")
	public ModelAndView selectAdmDeptCalcList(ModelAndView mv, int deptNo, String calcSdate, String calcEdate, HttpServletResponse response) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("deptNo", deptNo);
		map.put("calcSdate", calcSdate);
		map.put("calcEdate", calcEdate);
		
		List<HashMap<String, Object>> sysList = cms.selectAdmDeptCalcList(map);
		
		mv.addObject("list", sysList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		return mv;
		
	}

	// moxn : 일정 한개 상세 조회 : ajax
	@PostMapping("selectDetCalOne.ca")
	public ModelAndView selectDetCalOne(ModelAndView mv, int calNo, HttpServletResponse response) {
		
		HashMap<String, Object> map = cms.selectDetCalOne(calNo);
		List<HashMap<String, Object>> asso = cms.selectDetCalOneAsso(calNo);
		
		mv.addObject("map", map);
		if(asso != null) {
			mv.addObject("asso", asso);
		}
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		
		return mv;
	}
	
	// moxn : 일정 한개 삭제
	@PostMapping("deleteCalcOne.ca")
	public ModelAndView deleteCalcOne(ModelAndView mv, int calNo) {

		int result = cms.deleteCalcOne(calNo);
		
		if(result > 0) {
			mv.addObject("msg", "일정이 삭제되었습니다.");
			mv.addObject("view", "calc_main");
			mv.setViewName("calcAlert");
			
		}else {
			mv.addObject("msg", "일정 삭제에 실패했습니다.");
			mv.addObject("view", "calc_main");
			mv.setViewName("calcAlert");
		}
		
		return mv;
	}
	
	//moxn : 일정 수정
	@PostMapping("updateCalcOne.ca")
	public ModelAndView updateCalcOne(ModelAndView mv, Calendar calc, int[] eNo, int[] assoCalNo,
									String newCalSdate, String newCalEdate, String newAllDay,
									int newDeptNo
									) {
		
		ArrayList<HashMap<String, Object>> newAsso = new ArrayList<HashMap<String, Object>>();
		if(eNo != null) {
			for (int i = 0; i < eNo.length; i++) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("calNo", calc.getCalNo());
				map.put("empDivNo", eNo[i]);
				newAsso.add(map);
			}
		}
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy/MM/dd");
		// String to Date Format
		try {
			if(calc.getAllYn() == null) {
				calc.setCalSdate(new java.sql.Date(transFormat.parse(newCalSdate).getTime()));
				calc.setCalEdate(new java.sql.Date(transFormat.parse(newCalEdate).getTime()));
			}else if(calc.getAllYn().equals("on")) {
				calc.setCalSdate(new java.sql.Date(transFormat.parse(newAllDay).getTime()));
			}
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if(calc.getAllYn() == null) {
			calc.setAllYn("N");
		}else if(calc.getAllYn().equals("on")) {
			calc.setAllYn("Y");
			calc.setCalStime(null);
			calc.setCalEtime(null);
		}
		
		if(calc.getCalKind().equals("DEPT")) {
			calc.setDeptNo(newDeptNo);
		}
		
		int result = cms.updateCalcOne(calc, newAsso);
		
		if(result > 0) {
			mv.addObject("msg", "일정이 수정되었습니다.");
			mv.addObject("view", "calc_main");
			mv.setViewName("calcAlert");
			
		}else {
			mv.addObject("msg", "일정 수정에 실패했습니다.");
			mv.addObject("view", "calc_main");
			mv.setViewName("calcAlert");
		}
		
		return mv;
	}

	// moxn :  : ajax
		@PostMapping("updateCalcDrag.ca")
		public ModelAndView updateCalcDrag(ModelAndView mv, int calNo, int days, HttpServletResponse response) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("calNo", calNo);
			map.put("days", days);
			
			int result = cms.updateCalcDrag(map);
			
			if(result > 0) {
				mv.addObject("msg", "일정이 수정되었습니다.");
				response.setCharacterEncoding("UTF-8");
				
				mv.setViewName("jsonView");
				
			}else {
				mv.addObject("msg", "일정 수정에 실패했습니다.");
				mv.addObject("view", "calc_main");
				mv.setViewName("calcAlert");
			}
			
			return mv;
			
		}
}
