package com.hardcore.cw.board.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.Request;
import com.hardcore.cw.board.model.service.BoardService;
import com.hardcore.cw.board.model.service.ReplyService;
import com.hardcore.cw.board.model.vo.Reply;
import com.hardcore.cw.common.model.vo.LoginEmp;

import net.sf.json.JSONObject;

@Controller
@SessionAttributes("loginUser")
public class ReplyInsertController {
	private final ReplyService rs;
	
	public ReplyInsertController(ReplyService rs) {
		this.rs = rs;
	}
	
	@PostMapping("insertReply.bo")
	public void insertReply(Reply r, Model m, HttpServletRequest request, HttpServletResponse response) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		ObjectMapper mapper = new ObjectMapper();
		 
		r.setEmpDivNo(emp.getEmpDivNo());
		
		response.setCharacterEncoding("UTF-8");
		rs.insertReply(r);
		
		// 방금올린 댓글 조회
		Reply rr = rs.selectReplyOne(r);
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("empName", rr.getEmpName());
		map.put("replyCnt", rr.getReplyCnt());
		map.put("replyDate", rr.getReplyDate());
		
		try {
			response.getWriter().println(mapper.writeValueAsString(rr));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
