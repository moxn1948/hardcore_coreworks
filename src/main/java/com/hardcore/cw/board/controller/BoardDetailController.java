package com.hardcore.cw.board.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.board.model.service.BoardService;
import com.hardcore.cw.board.model.vo.Board;
import com.hardcore.cw.board.model.vo.Reply;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.Read;

@Controller
@SessionAttributes("loginUser")
public class BoardDetailController {
	private final BoardService bs;
	
	public BoardDetailController(BoardService bs) {
		this.bs = bs;
	}
	
	@PostMapping("detailBoard.bo")
	public String showDetailBoard(Model m, Board b, HttpServletRequest request) {
		String type = b.getBtype();
		Board detail = null;
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		b.setEmpDivNo(emp.getEmpDivNo());
		
		if(type.equals("TEAM")) {
			detail = bs.selectTeamBoardDetail(b);
		} else {
			detail = bs.selectBoardDetail(b);
		}

		if(!ObjectUtils.isEmpty(detail)) {
			// 게시물 조회수 1증가 -> update
			bs.updateBoardCount(b);
			
			// 게시물 읽음 처리 -> select 후 없으면 insert, 있으면 pass
			Map<String, Object> map = new HashMap<>();
			
			map.put("bno", b.getBno());
			map.put("empDivNo", emp.getEmpDivNo());
			
			Read br = bs.selectReadStatus(map);
			
			if(br == null) {
				bs.insertReadStatus(map);
			}
			
			// 댓글(답글포함) 끌어오기 -> select
			List<Reply> replyList = bs.selectReplyList(b);
			
			// 첨부파일 끌어오기 -> select
			List<Attachment> attList = bs.selectAttList(b);
			
			m.addAttribute("bdetail", detail);
			m.addAttribute("replyList", replyList);
			m.addAttribute("attList", attList);
			
			return "detail_board";
		} else {
			m.addAttribute("msg", "존재하지 않는 페이지입니다.");
			return "redirect:index.jsp";
		}

	}
}
