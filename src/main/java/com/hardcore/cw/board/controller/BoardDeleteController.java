package com.hardcore.cw.board.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.board.model.service.BoardService;
import com.hardcore.cw.board.model.vo.Board;

@Controller
@SessionAttributes("loginUser")
public class BoardDeleteController {
	private final BoardService bs;
	
	public BoardDeleteController(BoardService bs) {
		this.bs = bs;
	}
	
	@GetMapping("deleteBoard.bo")
	public String boardDelete(Model m, Board b) {
		
		bs.deleteBoard(b);

		return "redirect:comList.bo";
	}
}
