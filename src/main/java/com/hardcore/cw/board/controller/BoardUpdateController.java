package com.hardcore.cw.board.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hardcore.cw.board.model.service.BoardService;
import com.hardcore.cw.board.model.vo.Board;
import com.hardcore.cw.common.model.vo.Attachment;

@Controller
@SessionAttributes("loginUser")
public class BoardUpdateController {
	private final BoardService bs;
	
	public BoardUpdateController(BoardService bs) {
		this.bs = bs;
	}
	
	@GetMapping("showUpdateBoard.bo")
	public String showUpdateForm(Board b, Model m) {
		b = bs.selectBoardDetail(b);
		
		List<Attachment> at = bs.selectAttList(b);
		
		m.addAttribute("upBoard", b);
		m.addAttribute("at", at);
		
		return "up_board";
	}
	
	@PostMapping("updateBoard.bo")
	public String insertBoard(Model model, Board b, 
								HttpServletRequest request,
								RedirectAttributes rttr,
								MultipartHttpServletRequest mtfRequest) {
		// 받아온 파일들 리스트
		List<MultipartFile> fileList = mtfRequest.getFiles("attachments");

		// 첨부파일수정 우짜지...
		String fileNo[] = request.getParameterValues("fileNo");
		
		if(fileList.size() > 0) {
			long fileSize = 0;
			for(int i=0; i<fileList.size(); i++) {
				fileSize += fileList.get(i).getSize();
			}
			
			if(fileSize > 10000000) { 
				model.addAttribute("msg", "파일용량이 초과되었습니다. 10MB이하로 맞춰주세요.");
				return "redirect:errorPage.jsp";
			}
			
			String root = request.getSession().getServletContext().getRealPath("resources");
			String filePath = root + "\\uploadFiles";
			
			for(MultipartFile file : fileList) {
				if(!file.getOriginalFilename().equals("")) {
					Attachment at = new Attachment();
					
					// 파일명 변경
					
					// 파일명 추출
					String originFileName = file.getOriginalFilename();
					// 확장자 분리
					String ext = originFileName.substring(originFileName.lastIndexOf("."));
					// 랜덤문자열 추출
					String changeName = UUID.randomUUID().toString().replace("-", "");
					
					try {
						file.transferTo(new File(filePath + "\\" + changeName + ext));
						
						// 첨부파일 전 이름, 후 이름, 경로, 타입 
						at.setOriginName(originFileName);
						at.setChangeName(changeName + ext);
						at.setFilePath(filePath);
						at.setEmpDivNo(Integer.parseInt(request.getParameter("empDivNo")));
						at.setBno(b.getBno());
						
					} catch (IllegalStateException e) {
						model.addAttribute("msg", "메소드관련 에러가 발생했습니다.");
						return "redirect:errorPage.jsp";
					} catch (IOException e) {
						model.addAttribute("msg", "파일관련 에러가 발생했습니다.");
						return "redirect:errorPage.jsp";
					} catch (Exception e) {
						model.addAttribute("msg", "알수없는 에러가 발생했습니다.");
						return "redirect:errorPage.jsp";
					}
				}
			}
		}
		
		bs.updateBoard(b);
		
		if (b.getBtype().equals("ALL")) {
			return "redirect:comList.bo";
		} else {
			return "redirect:deptList.bo";
		}
	}
}
