package com.hardcore.cw.board.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.board.model.service.BoardService;
import com.hardcore.cw.board.model.vo.Board;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.board.model.vo.SearchCondition;
import com.hardcore.cw.common.Pagination;

@Controller
@SessionAttributes("loginUser")
public class BoardSearchController {
	
	private final BoardService bs;
	
	public BoardSearchController(BoardService bs) {
		super();
		this.bs = bs;
	}
	
	@GetMapping("searchComList.bo")
	public String searchComList(Model m, SearchCondition sc, HttpServletRequest request) {
		
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		// 검색 결과 갯수
		int listCount = bs.getSearchResultListCount(sc);
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		List<Board> comList = bs.selectSearchResultList(sc, pi);
		
		m.addAttribute("comList", comList);
		m.addAttribute("pi", pi);
		
		return "com_list";
	}
}
