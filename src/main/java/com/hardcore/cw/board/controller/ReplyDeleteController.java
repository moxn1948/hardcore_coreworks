package com.hardcore.cw.board.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.board.model.service.ReplyService;
import com.hardcore.cw.board.model.vo.Reply;

@Controller
@SessionAttributes("loginUser")
public class ReplyDeleteController {
	private final ReplyService rs;
	
	public ReplyDeleteController(ReplyService rs) {
		this.rs = rs;
	}
	
	@PostMapping("replyDelete.bo")
	public void deleteReply(Reply r, HttpServletResponse response) {
		ObjectMapper mapper = new ObjectMapper();
		
		rs.deleteReply(r);
		
		String msg = "success";
		
		try {
			response.getWriter().println(mapper.writeValueAsString(msg));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
