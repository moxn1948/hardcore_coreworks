package com.hardcore.cw.board.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.board.model.service.BoardService;
import com.hardcore.cw.board.model.vo.Board;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.Pagination;

@Controller
@SessionAttributes("loginUser")
// 공지사항메뉴부분 이동 Controller
public class BoardMenuController {
	private final BoardService bs;
	
	public BoardMenuController(BoardService bs) {
		this.bs = bs;
	}

	// 전체공지사항으로 이동(메뉴에서 게시판 클릭 시 전체 공지사항 페이지로 이동)
	@GetMapping("comList.bo")
	public String showComBoardList(Model m, Board b, HttpServletRequest request) {
		
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		
		//currentPage에 대한 처리
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		int listCount = bs.getComListCount();
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		// 필독상관없이 전부 다 추출
		List<Board> comList = bs.selectComList(pi, emp);
		
		// 필독 5개만 추출
		List<Board> readMustList = bs.selectReadMustList(emp);
		
		m.addAttribute("comList", comList);
		m.addAttribute("readMustList", readMustList);
		m.addAttribute("pi", pi);
		
		return "com_list";
	}
	
	// 팀 공지사항으로 이동
	@GetMapping("deptList.bo")
	public String showDeptBoardList(Model m, Board b, HttpServletRequest request) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		// Dept 리스트 저장용
		List<Dept> list = new ArrayList<>();
		// 여부확인용
		Dept d = new Dept();
		
		// 내 부서에 속한 부서가 있는지 조회
		list = bs.selectSubDeptJud(emp);
		
		// d == null 이라면 최하위 조회진행
		if(!ObjectUtils.isEmpty(list)) {
			// 아니라면 최상위 여부 진행
			d = bs.selectTopDeptJud(emp);
						
			// 만약 order이 null이라면 최상위
			if(String.valueOf(d.getDeptOrder()) == null) {
				list = bs.selectTopDept(emp);
			} else {
				// 아니라면 1dept
				list = bs.selectMiddleDept(emp);
			}
		} else {
			list = bs.selectLastDept(emp);
		}
		
		
		//currentPage에 대한 처리
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		
		// 해당 부서들의 게시물갯수
		int listCount = bs.getDeptListCount(map);
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		// Map에 사용자정보넣기 -> 읽음여부전용
		map.put("emp", emp);
		
		// 필독상관없이 전부 다 추출
		List<Board> deptList = bs.selectDeptList(pi, map);
			
		// 필독 5개만 추출
		List<Board> readMustList = bs.selectDeptMustList(map);
			
		m.addAttribute("deptList", deptList);
		m.addAttribute("readMustList", readMustList);
		
		m.addAttribute("pi", pi);
		
		return "dept_list";
	}
	
	// 필독 공지사항으로 이동
	@GetMapping("mustRead.bo")
	public String showMustRead(HttpServletRequest request, Model m) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		// Dept 리스트 저장용
		List<Dept> list = new ArrayList<>();
		// 여부확인용
		Dept d = new Dept();
		
		// 내 부서에 속한 부서가 있는지 조회
		list = bs.selectSubDeptJud(emp);
		
		// d == null 이라면 최하위 조회진행
		if(!ObjectUtils.isEmpty(list)) {
			// 아니라면 최상위 여부 진행
			d = bs.selectTopDeptJud(emp);
						
			// 만약 order이 null이라면 최상위
			if(String.valueOf(d.getDeptOrder()) == null) {
				list = bs.selectTopDept(emp);
			} else {
				// 아니라면 1dept
				list = bs.selectMiddleDept(emp);
			}
		} else {
			list = bs.selectLastDept(emp);
		}
		
		//currentPage에 대한 처리
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("list", list);
		
		int listCount = bs.getMustReadListCount(map);
		
		map.put("emp", emp);
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		List<Board> readMustList = bs.selectReadMustAllList(pi, map);
			
		m.addAttribute("readMustList", readMustList);
		m.addAttribute("pi", pi);
		
		return "key_list";
	}
	
	// 내가 쓴 공지사항으로 이동
	@GetMapping("myBoard.bo")
	public String showMyBoard(HttpServletRequest request, Model m) {
		LoginEmp emp = (LoginEmp) request.getSession().getAttribute("loginUser");
		
		//currentPage에 대한 처리
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		int listCount = bs.getMyBoardListCount(emp);
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		List<Board> myBoardList = bs.selectMyBoardList(pi, emp);

		m.addAttribute("myBoardList", myBoardList);
		m.addAttribute("pi", pi);
		
		return "my_list";
	}
	
	// 공지사항 작성페이지로 이동
	@GetMapping("newBoard.bo")
	public String showNewBoard() {
		return "new_board";
	}
}
