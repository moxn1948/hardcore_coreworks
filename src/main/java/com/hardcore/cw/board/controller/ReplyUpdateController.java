package com.hardcore.cw.board.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.hardcore.cw.board.model.service.ReplyService;
import com.hardcore.cw.board.model.vo.Reply;
import com.hardcore.cw.common.model.vo.LoginEmp;

@Controller
@SessionAttributes("loginUser")
public class ReplyUpdateController {
	
	private final ReplyService rs;
	
	public ReplyUpdateController(ReplyService rs) {
		this.rs = rs;
	}
	
	@PostMapping("replyUpdate.bo")
	public void replyUpdate(Reply r, HttpServletRequest request, HttpServletResponse response) {
		ObjectMapper mapper = new ObjectMapper();
		
		rs.updateReply(r);
		
		String message = "success";
		
		HashMap<String, Object> map = new HashMap<>();
		
		response.setCharacterEncoding("UTF-8");
		
		map.put("replyCnt", r.getReplyCnt());
		map.put("message", message);
		
		try {
			response.getWriter().println(mapper.writeValueAsString(map));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
