package com.hardcore.cw.board.controller;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.board.controller.BoardInsertController;
import com.hardcore.cw.board.model.service.BoardService;
import com.hardcore.cw.common.model.vo.Attachment;

@Controller
public class DownloadController implements ApplicationContextAware {
	private WebApplicationContext context = null;
	private final BoardService bs;

	public DownloadController(BoardService bs) {
		this.bs = bs;
	}
	
	@GetMapping("download.bo")
	public ModelAndView boardAttDownload(Attachment at, ModelAndView mv) {
		Attachment fileInfo = bs.selectOneFile(at);
		
		File file = new File(fileInfo.getFilePath() + "\\" + fileInfo.getChangeName());
		
		Map<String, Object> map = new HashMap<>();
		map.put("file", file);
		map.put("fileName", fileInfo.getOriginName());
		
		return new ModelAndView("download", "downloadFile", map);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = (WebApplicationContext) applicationContext;
	}
}
