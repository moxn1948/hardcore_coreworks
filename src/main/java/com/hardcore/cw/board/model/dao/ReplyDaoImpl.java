package com.hardcore.cw.board.model.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.board.model.vo.Reply;

@Repository
public class ReplyDaoImpl implements ReplyDao {

	@Override
	public int insertReply(SqlSessionTemplate sqlSession, Reply r) {
		return sqlSession.insert("Board.insertReply", r);
	}

	@Override
	public Reply selectReplyOne(SqlSessionTemplate sqlSession, Reply r) {
		return sqlSession.selectOne("Board.selectReplyOne", r);
	}

	@Override
	public int updateReply(SqlSessionTemplate sqlSession, Reply r) {
		return sqlSession.update("Board.updateReply", r);
	}

	@Override
	public int deleteReply(SqlSessionTemplate sqlSession, Reply r) {
		return sqlSession.update("Board.deleteReply", r);
	}

}
