package com.hardcore.cw.board.model.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.board.model.dao.BoardDao;
import com.hardcore.cw.board.model.vo.Board;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.common.model.vo.Read;
import com.hardcore.cw.board.model.vo.Reply;
import com.hardcore.cw.board.model.vo.SearchCondition;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;

@Service
public class BoardServiceImpl implements BoardService{
	private SqlSessionTemplate sqlSession;
	private BoardDao bd;

	@Autowired
	public BoardServiceImpl(SqlSessionTemplate sqlSession, BoardDao bd) {
		this.sqlSession = sqlSession;
		this.bd = bd;
	}

	// board 데이터 삽입(Board만)
	@Override
	public int insertBoard(Board b) {
		return bd.insertBoard(sqlSession, b);
	}

	// 첨부파일 데이터 삽입
	@Override
	public int insertAttachment(Attachment at) {
		return bd.insertAttachment(sqlSession, at);
	}

	// 전체 공지사항 갯수 조회
	@Override
	public int getComListCount() {
		return bd.selectComCount(sqlSession);
	}

	// 전체 공지사항 리스트 조회
	@Override
	public List<Board> selectComList(PageInfo pi, LoginEmp emp) {
		return bd.selectComList(sqlSession, pi, emp);
	}

	// 전체 공지사항 필독 5개만 추출
	@Override
	public List<Board> selectReadMustList(LoginEmp emp) {
		return bd.selectReadMustList(sqlSession, emp);
	}

	// 전체 공지사항 검색결과 갯수
	@Override
	public int getSearchResultListCount(SearchCondition sc) {
		return bd.getSearchResultListCount(sqlSession, sc);
	}

	// 전체 공지사항 검색결과에 따른 리스트 
	@Override
	public List<Board> selectSearchResultList(SearchCondition sc, PageInfo pi) {
		return bd.selectSearchResultList(sqlSession, sc, pi);
	}

	// 조회수 1증가
	@Override
	public int updateBoardCount(Board b) {
		
		return bd.updateBoardCount(sqlSession, b);
	}

	// 읽음여부 테이블에 있는지 없는지 조회
	@Override
	public Read selectReadStatus(Map<String, Object> map) {
		
		return bd.selectReadStatus(sqlSession, map);
	}

	// 읽음여부 테이블에 없다면 읽음여부 추가
	@Override
	public int insertReadStatus(Map<String, Object> map) {
		return bd.insertReadStatus(sqlSession, map);
	}

	// 댓글 리스트 조회
	@Override
	public List<Reply> selectReplyList(Board b) {
		return bd.selectReplyList(sqlSession, b);
	}

	// 첨부파일 리스트 조회
	@Override
	public List<Attachment> selectAttList(Board b) {
		return bd.selectAttList(sqlSession, b);
	}

	// 게시물 정보 추출
	@Override
	public Board selectBoardDetail(Board b) {
		return bd.selectBoardDetail(sqlSession, b);
	}

	// 첨부파일 추출
	@Override
	public Attachment selectOneFile(Attachment at) {
		return bd.selectOneFile(sqlSession, at);
	}

	// 최하위여부 판단용
	@Override
	public List<Dept> selectSubDeptJud(LoginEmp emp) {
		
		return bd.selectSubDeptJud(sqlSession, emp);
	}

	// 최하위부서에서 보여질 부서 조회
	@Override
	public List<Dept> selectLastDept(LoginEmp emp) {
		
		return bd.selectLastDept(sqlSession, emp);
	}

	// 최상위여부 판단
	@Override
	public Dept selectTopDeptJud(LoginEmp emp) {
		
		return bd.selectTopDeptJud(sqlSession, emp);
	}

	@Override
	public List<Dept> selectTopDept(LoginEmp emp) {
		
		return bd.selectTopDept(sqlSession, emp);
	}

	@Override
	public List<Dept> selectMiddleDept(LoginEmp emp) {
		
		return bd.selectMiddleDept(sqlSession, emp);
	}

	@Override
	public int getDeptListCount(Map<String, Object> map) {
		
		return bd.getDeptListCount(sqlSession, map);
	}

	@Override
	public List<Board> selectDeptList(PageInfo pi, Map<String, Object> map) {
		
		return bd.selectDeptList(sqlSession, pi, map);
	}

	@Override
	public List<Board> selectDeptMustList(Map<String, Object> map) {
		
		return bd.selectDeptMustList(sqlSession, map);
	}

	@Override
	public Board selectTeamBoardDetail(Board b) {
		
		return bd.selectTeamBoardDetail(sqlSession, b);
	}

	@Override
	public int getMustReadListCount(Map<String, Object> map) {
		
		return bd.getMustReadListCount(sqlSession, map);
	}

	@Override
	public List<Board> selectReadMustAllList(PageInfo pi, Map<String, Object> map) {
		
		return bd.selectReadMustAllList(sqlSession, map, pi);
	}

	@Override
	public int getMyBoardListCount(LoginEmp emp) {
		
		return bd.getMyBoardListCount(sqlSession, emp);
	}

	@Override
	public List<Board> selectMyBoardList(PageInfo pi, LoginEmp emp) {
		
		return bd.selectMyBoardList(sqlSession, emp, pi);
	}

	@Override
	public int updateBoard(Board b) {
		return bd.updateBoard(sqlSession, b);
	}

	@Override
	public int updateAttachment(Attachment at) {
		return bd.updateAttachment(sqlSession, at);
	}

	@Override
	public int deleteBoard(Board b) {
		return bd.deleteBoard(sqlSession, b);
	}
}
