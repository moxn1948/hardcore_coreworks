package com.hardcore.cw.board.model.dao;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.board.model.vo.Reply;

public interface ReplyDao {

	int insertReply(SqlSessionTemplate sqlSession, Reply r);

	Reply selectReplyOne(SqlSessionTemplate sqlSession, Reply r);

	int updateReply(SqlSessionTemplate sqlSession, Reply r);

	int deleteReply(SqlSessionTemplate sqlSession, Reply r);

}
