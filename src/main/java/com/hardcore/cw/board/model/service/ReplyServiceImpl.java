package com.hardcore.cw.board.model.service;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Service;

import com.hardcore.cw.board.model.dao.ReplyDao;
import com.hardcore.cw.board.model.vo.Reply;

@Service
public class ReplyServiceImpl implements ReplyService {
	
	private final ReplyDao rd;
	private final SqlSessionTemplate sqlSession;

	public ReplyServiceImpl(ReplyDao rd, SqlSessionTemplate sqlSession) {
		this.rd = rd;
		this.sqlSession = sqlSession;
	}

	@Override
	public int insertReply(Reply r) {
		return rd.insertReply(sqlSession, r);
	}

	@Override
	public Reply selectReplyOne(Reply r) {
		return rd.selectReplyOne(sqlSession, r);
	}

	@Override
	public int updateReply(Reply r) {
		return rd.updateReply(sqlSession, r);
	}

	@Override
	public int deleteReply(Reply r) {
		return rd.deleteReply(sqlSession, r);
	}

}
