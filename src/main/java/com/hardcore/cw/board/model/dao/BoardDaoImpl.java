package com.hardcore.cw.board.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.board.model.vo.Board;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.common.model.vo.Read;
import com.hardcore.cw.board.model.vo.Reply;
import com.hardcore.cw.board.model.vo.SearchCondition;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;

@Repository
public class BoardDaoImpl implements BoardDao{

	// 공지사항 게시물 삽입(첨부파일 없는 버전)
	@Override
	public int insertBoard(SqlSessionTemplate sqlSession, Board b) {
		return sqlSession.insert("Board.insertBoard", b);
	}

	// 첨부파일 데이터 삽입
	@Override
	public int insertAttachment(SqlSessionTemplate sqlSession, Attachment at) {
		return sqlSession.insert("Board.insertAttachment", at);
	}

	// 전체 공지사항 갯수 조회
	@Override
	public int selectComCount(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Board.selectComCount");
	}

	// 전체 공지사항 리스트 조회
	@Override
	public List<Board> selectComList(SqlSessionTemplate sqlSession, PageInfo pi, LoginEmp emp) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		
		return sqlSession.selectList("Board.selectComBoard", emp, rowBounds);
	}

	// 필독5개만 추출
	@Override
	public List<Board> selectReadMustList(SqlSessionTemplate sqlSession, LoginEmp emp) {
		return sqlSession.selectList("Board.selectComReadMustBoard", emp);
	}

	// 전체 공지사항 갯수 추출
	@Override
	public int getSearchResultListCount(SqlSessionTemplate sqlSession, SearchCondition sc) {
		
		return sqlSession.selectOne("Board.selectSearchResultCount", sc);
	}

	// 전체 공지사항 검색결과에 따른 리스트 추출
	@Override
	public List<Board> selectSearchResultList(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi) {
		
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		
		return sqlSession.selectList("Board.selectSearchResultList", sc, rowBounds);
	}

	// 조회수 1 증가
	@Override
	public int updateBoardCount(SqlSessionTemplate sqlSession, Board b) {
		
		return sqlSession.update("Board.updateBoardCount", b);
	}

	// 읽음여부 추출
	@Override
	public Read selectReadStatus(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		
		return sqlSession.selectOne("Board.selectReadStatus", map);
	}

	// 읽음 추가
	@Override
	public int insertReadStatus(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		return sqlSession.insert("Board.insertReadStatus", map);
	}

	// 댓글리스트 조회
	@Override
	public List<Reply> selectReplyList(SqlSessionTemplate sqlSession, Board b) {
		return sqlSession.selectList("Board.selectReplyList", b);
	}

	// 첨부파일 리스트 추출
	@Override
	public List<Attachment> selectAttList(SqlSessionTemplate sqlSession, Board b) {
		return sqlSession.selectList("Board.selectAttList", b);
	}

	// 게시물 상세정보
	@Override
	public Board selectBoardDetail(SqlSessionTemplate sqlSession, Board b) {
		return sqlSession.selectOne("Board.selectBoardDetail", b);
	}

	// 파일 정보 추출
	@Override
	public Attachment selectOneFile(SqlSessionTemplate sqlSession, Attachment at) {
		return sqlSession.selectOne("Board.selectOneFile", at);
	}

	// 최하위부서 여부 파악
	@Override
	public List<Dept> selectSubDeptJud(SqlSessionTemplate sqlSession, LoginEmp emp) {
		
		return sqlSession.selectList("Board.selectSubDeptJud", emp);
	}

	// 최하위부서에서 보여질 부서 조회
	@Override
	public List<Dept> selectLastDept(SqlSessionTemplate sqlSession, LoginEmp emp) {
		
		return sqlSession.selectList("Board.selectLastDept", emp);
	}

	// 최상위부서 판단용
	@Override
	public Dept selectTopDeptJud(SqlSessionTemplate sqlSession, LoginEmp emp) {
		
		return sqlSession.selectOne("Board.selectTopDeptJud", emp);
	}

	@Override
	public List<Dept> selectTopDept(SqlSessionTemplate sqlSession, LoginEmp emp) {
		
		return sqlSession.selectList("Board.selectTopDept", emp);
	}

	@Override
	public List<Dept> selectMiddleDept(SqlSessionTemplate sqlSession, LoginEmp emp) {
		
		return sqlSession.selectList("Board.selectMiddleDept", emp);
	}

	@Override
	public int getDeptListCount(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		
		return sqlSession.selectOne("Board.getDeptListCount", map);
	}

	@Override
	public List<Board> selectDeptList(SqlSessionTemplate sqlSession, PageInfo pi, Map<String, Object> map) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		
		return sqlSession.selectList("Board.selectDeptList", map, rowBounds);
	}

	@Override
	public List<Board> selectDeptMustList(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		
		return sqlSession.selectList("Board.selectDeptMustList", map);
	}

	@Override
	public Board selectTeamBoardDetail(SqlSessionTemplate sqlSession, Board b) {
		
		return sqlSession.selectOne("Board.selectTeamBoardDetail", b);
	}

	@Override
	public int getMustReadListCount(SqlSessionTemplate sqlSession, Map<String, Object> map) {
		
		return sqlSession.selectOne("Board.getMustReadListCount", map);
	}

	@Override
	public List<Board> selectReadMustAllList(SqlSessionTemplate sqlSession, Map<String, Object> map, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		
		return sqlSession.selectList("Board.selectReadMustAllList", map, rowBounds);
	}

	@Override
	public int getMyBoardListCount(SqlSessionTemplate sqlSession, LoginEmp emp) {
		
		return sqlSession.selectOne("Board.getMyBoardListCount", emp);
	}

	@Override
	public List<Board> selectMyBoardList(SqlSessionTemplate sqlSession, LoginEmp emp, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		
		return sqlSession.selectList("Board.selectMyBoardList", emp, rowBounds);
	}

	@Override
	public int updateBoard(SqlSessionTemplate sqlSession, Board b) {
		return sqlSession.update("Board.updateBoard", b);
	}

	@Override
	public int updateAttachment(SqlSessionTemplate sqlSession, Attachment at) {
		return sqlSession.update("Board.updateAttachment", at);
	}

	@Override
	public int deleteBoard(SqlSessionTemplate sqlSession, Board b) {
		return sqlSession.update("Board.deleteBoard", b);
	}

}
