package com.hardcore.cw.board.model.vo;

import java.sql.Date;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Board {
   private int bno;
   private String btitle;
   private String bcnt;
   private String bwriter;
   private int hits;
   private Date bdate;
   private String btime;
   private String essenYn;
   private int belong;
   private String belongName;
   private ArrayList<Object> deptOrderList;
   private int deptNo;
   private String deptName;
   private String btype;
   private int empDivNo;
   private String status;
   private int replyCount;
   private ArrayList<Reply> replyList;
   private String boardReadStatus;
}