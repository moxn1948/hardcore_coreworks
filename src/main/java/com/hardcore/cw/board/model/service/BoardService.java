package com.hardcore.cw.board.model.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.hardcore.cw.board.model.vo.Board;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.common.model.vo.Read;
import com.hardcore.cw.board.model.vo.Reply;
import com.hardcore.cw.board.model.vo.SearchCondition;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;

public interface BoardService {

	int insertBoard(Board b);

	int insertAttachment(Attachment at);

	int getComListCount();
	
	List<Board> selectComList(PageInfo pi, LoginEmp emp);

	List<Board> selectReadMustList(LoginEmp emp);

	int getSearchResultListCount(SearchCondition sc);

	List<Board> selectSearchResultList(SearchCondition sc, PageInfo pi);

	int updateBoardCount(Board b);

	Read selectReadStatus(Map<String, Object> map);

	List<Reply> selectReplyList(Board b);

	List<Attachment> selectAttList(Board b);

	int insertReadStatus(Map<String, Object> map);

	Board selectBoardDetail(Board b);

	Attachment selectOneFile(Attachment at);

	List<Dept> selectSubDeptJud(LoginEmp emp);

	List<Dept> selectLastDept(LoginEmp emp);

	Dept selectTopDeptJud(LoginEmp emp);

	List<Dept> selectTopDept(LoginEmp emp);

	List<Dept> selectMiddleDept(LoginEmp emp);

	int getDeptListCount(Map<String, Object> map);

	List<Board> selectDeptList(PageInfo pi, Map<String, Object> map);

	List<Board> selectDeptMustList(Map<String, Object> map);

	Board selectTeamBoardDetail(Board b);

	int getMyBoardListCount(LoginEmp emp);

	List<Board> selectMyBoardList(PageInfo pi, LoginEmp emp);

	int getMustReadListCount(Map<String, Object> map);

	List<Board> selectReadMustAllList(PageInfo pi, Map<String, Object> map);

	int updateBoard(Board b);

	int updateAttachment(Attachment at);

	int deleteBoard(Board b);
}
