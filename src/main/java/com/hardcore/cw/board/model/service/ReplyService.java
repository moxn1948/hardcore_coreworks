package com.hardcore.cw.board.model.service;

import com.hardcore.cw.board.model.vo.Reply;

public interface ReplyService {

	int insertReply(Reply r);

	Reply selectReplyOne(Reply r);

	int updateReply(Reply r);

	int deleteReply(Reply r);

}
