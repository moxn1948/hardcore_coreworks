package com.hardcore.cw.board.model.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.board.model.vo.Board;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.common.model.vo.Read;
import com.hardcore.cw.board.model.vo.Reply;
import com.hardcore.cw.board.model.vo.SearchCondition;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;

public interface BoardDao {

	int insertBoard(SqlSessionTemplate sqlSession, Board b);

	int insertAttachment(SqlSessionTemplate sqlSession, Attachment at);

	int selectComCount(SqlSessionTemplate sqlSession);
	
	List<Board> selectComList(SqlSessionTemplate sqlSession, PageInfo pi, LoginEmp emp);

	List<Board> selectReadMustList(SqlSessionTemplate sqlSession, LoginEmp emp);

	int getSearchResultListCount(SqlSessionTemplate sqlSession, SearchCondition sc);

	List<Board> selectSearchResultList(SqlSessionTemplate sqlSession, SearchCondition sc, PageInfo pi);

	int updateBoardCount(SqlSessionTemplate sqlSession, Board b);

	Read selectReadStatus(SqlSessionTemplate sqlSession, Map<String, Object> map);

	List<Reply> selectReplyList(SqlSessionTemplate sqlSession, Board b);

	List<Attachment> selectAttList(SqlSessionTemplate sqlSession, Board b);

	int insertReadStatus(SqlSessionTemplate sqlSession, Map<String, Object> map);

	Board selectBoardDetail(SqlSessionTemplate sqlSession, Board b);

	Attachment selectOneFile(SqlSessionTemplate sqlSession, Attachment at);

	List<Dept> selectSubDeptJud(SqlSessionTemplate sqlSession, LoginEmp emp);

	List<Dept> selectLastDept(SqlSessionTemplate sqlSession, LoginEmp emp);

	Dept selectTopDeptJud(SqlSessionTemplate sqlSession, LoginEmp emp);

	List<Dept> selectTopDept(SqlSessionTemplate sqlSession, LoginEmp emp);

	List<Dept> selectMiddleDept(SqlSessionTemplate sqlSession, LoginEmp emp);

	int getDeptListCount(SqlSessionTemplate sqlSession, Map<String, Object> map);

	List<Board> selectDeptList(SqlSessionTemplate sqlSession, PageInfo pi, Map<String, Object> map);

	List<Board> selectDeptMustList(SqlSessionTemplate sqlSession, Map<String, Object> map);

	Board selectTeamBoardDetail(SqlSessionTemplate sqlSession, Board b);

	int getMyBoardListCount(SqlSessionTemplate sqlSession, LoginEmp emp);

	List<Board> selectMyBoardList(SqlSessionTemplate sqlSession, LoginEmp emp, PageInfo pi);

	int getMustReadListCount(SqlSessionTemplate sqlSession, Map<String, Object> map);

	List<Board> selectReadMustAllList(SqlSessionTemplate sqlSession, Map<String, Object> map, PageInfo pi);

	int updateBoard(SqlSessionTemplate sqlSession, Board b);

	int updateAttachment(SqlSessionTemplate sqlSession, Attachment at);

	int deleteBoard(SqlSessionTemplate sqlSession, Board b);
}
