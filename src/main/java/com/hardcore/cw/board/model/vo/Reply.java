package com.hardcore.cw.board.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Reply implements java.io.Serializable{
	private int replyNo;
	private String replyCnt;
	private Date replyDate;
	private int rereply;
	private int bno;
	private int empDivNo;
	private String empName;
	private String status;
}
