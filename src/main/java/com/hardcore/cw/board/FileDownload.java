package com.hardcore.cw.board;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractView;

import com.hardcore.cw.board.controller.BoardInsertController;

public class FileDownload extends AbstractView {
	private static final Logger logger = LoggerFactory.getLogger(BoardInsertController.class);
	
	public void download() {
		setContentType("application/download; utf-8");
	}

	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Map<String, Object> map = (Map)model.get("downloadFile");
		
		File file = (File)map.get("file");
		
		String originName = (String)map.get("fileName");
		
		logger.info(file.getPath());
		logger.info(originName);
		
		response.setContentType(getContentType());
		response.setContentLength((int)file.length());
		
		String fileName = null;
		
		if(request.getHeader("User-Agent").indexOf("MSIE") > -1){
            
            fileName = URLEncoder.encode(file.getName(), "utf-8");
                         
        } else {
             
            fileName = new String(file.getName().getBytes("utf-8"));
        }// end if;
        
        response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\";");
        response.setHeader("Content-Transfer-Encoding", "binary");
        OutputStream out = response.getOutputStream();
        FileInputStream fis = null;
        
        //파일 카피 후 마무리
        try {
            fis = new FileInputStream(file);
            FileCopyUtils.copy(fis, out);
        } catch(Exception e){
            e.printStackTrace();
        }finally{
            if(fis != null){
                try{
                    fis.close();
                }catch(Exception e){}
            }
        }// try end;
        out.flush();
		
	}

}
