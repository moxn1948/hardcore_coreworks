package com.hardcore.cw.cms.model.service;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.cms.model.dao.FormDao;
import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.PriFormat;
import com.hardcore.cw.eas.model.vo.DocPath;

@Service
public class FormServiceImpl implements FormService {
	private final SqlSessionTemplate sqlSession;
	private final FormDao fd;
	
	public FormServiceImpl(SqlSessionTemplate sqlSession, FormDao fd) {
		this.sqlSession = sqlSession;
		this.fd = fd;
	}

	@Override
	public List<DocPath> selectDocPathList() {
		return fd.selectDocPathList(sqlSession);
	}

	@Override
	public List<PriFormat> selectPriFormatList() {
		return fd.selectPriFormatList(sqlSession);
	}

	@Override
	public int InsertFormat(Format f) {
		return fd.InsertFormat(sqlSession, f);
	}

	@Override
	public PriFormat selectPriFormatDetail(int priFormatNo) {
		return fd.selectPriFormatDetail(sqlSession, priFormatNo);
	}

	@Override
	public Format selectFormatDetail(Format f) {
		return fd.selectFormatDetail(sqlSession, f);
	}

	@Override
	public int updateFormat(Format f) {
		return fd.updateFormat(sqlSession, f);
	}

	@Override
	public int getFormatListCount() {
		return fd.getFormatListCount(sqlSession);
	}

	@Override
	public int deleteFormat(Format f) {
		return fd.deleteFormat(sqlSession, f);
	}
}
