package com.hardcore.cw.cms.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Job implements java.io.Serializable{
	private int jobNo;
	private String jobName;
	private String status;
	private int jobOrder;
}
