package com.hardcore.cw.cms.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.cms.model.vo.Vacation;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;

@Repository
public class CmsDaoImpl implements CmsDao{

	@Override
	public int insertJob(SqlSessionTemplate sqlSession, Job j) {
		
		return sqlSession.insert("Cms.insertJob", j);
	}
	
	//직급목록 조회
	@Override
	public List<Object> selectJobList(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Cms.selectJob");
	}

	//직책목록 조회
	@Override
	public List<Object> selectPosList(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Cms.selectPos");
	}
	

	//직급 삭제
	@Override
	public int deleteJob(SqlSessionTemplate sqlSession, Job j) {
		
		return sqlSession.update("Cms.deleteJob", j);
	}
	//직급 순서 변경
	@Override
	public int updateJobOrder(SqlSessionTemplate sqlSession, Job j) {
		
		return sqlSession.update("Cms.updateJobOrder",j);
	}
	

	@Override
	public int deletePos(SqlSessionTemplate sqlSession, Position p) {
		
		return sqlSession.update("Cms.deletePos", p);
	}

	@Override
	public int updatePosOrder(SqlSessionTemplate sqlSession, Position p) {
		
		return sqlSession.update("Cms.updatePosOrder", p);
	}


	@Override
	public int updatePos(SqlSessionTemplate sqlSession, Position p) {
		return sqlSession.update("Cms.updatePos", p);
	}

	@Override
	public int updateJob(SqlSessionTemplate sqlSession, Job j) {
		return sqlSession.update("Cms.updateJob", j);
	}

	@Override
	public List<Object> selectDocPathList(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Cms.selectDocPath");
	}

	@Override
	public int updateForm(SqlSessionTemplate sqlSession, ArrayList<DocPath> list) {
		
		return sqlSession.insert("Cms.insertDocPath", list);
	}


	@Override
	public List<Object> selectDocPathList2(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Cms.selectDocPath2");
	}

	@Override
	public List<DocPath> selectForm(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Cms.selectForm");
	}


	@Override
	public int updateFormList(SqlSessionTemplate sqlSession, DocPath d) {
		
		return sqlSession.update("Cms.updateDocPath", d);
	}

	@Override
	public int updatesubFormList(SqlSessionTemplate sqlSession, DocPath d) {
		
		return sqlSession.update("Cms.updatesubFormList", d);
	}

	@Override
	public int deleteSubPath(SqlSessionTemplate sqlSession, DocPath d) {
		
		return sqlSession.update("Cms.deleteSubPath", d);
	}

	@Override
	public int deleteTopPath(SqlSessionTemplate sqlSession, DocPath d) {
		return sqlSession.update("Cms.deleteTopPath", d);
	}

	@Override
	public List<Format> selectFormList(SqlSessionTemplate sqlSession, PageInfo pi) {
		//몇 개의 게시글을 건너 뛰고 조회를 할 지에 대한 계산
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		
		return sqlSession.selectList("Cms.selectFormList", null, rowBounds);
	}

	@Override
	public List<Dept> selectListDept(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Cms.selectListDept");
	}

	@Override
	public int updateDeptList(SqlSessionTemplate sqlSession, Dept d) {
		
		return sqlSession.update("Cms.updateDeptList", d);
	}

	@Override
	public int updateSubDeptList(SqlSessionTemplate sqlSession, Dept d) {
		return sqlSession.update("Cms.updateSubDeptList", d);
	}

	@Override
	public int updateSub2DeptList(SqlSessionTemplate sqlSession, Dept d) {
		return sqlSession.update("Cms.updateSub2DeptList", d);
	}

	@Override
	public List<Object> selectDeptNo(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Cms.selectDeptNo");
	}

	@Override
	public int insertDeptList(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		return sqlSession.insert("Cms.insertDeptList", cntMap);
	}

	@Override
	public int insertDeptList2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		
		return sqlSession.insert("Cms.insertDeptList2", cntMap);
	}

	@Override
	public int insertDeptList3(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		return sqlSession.insert("Cms.insertDeptList3", cntMap);
	}

	@Override
	public int insertDeptList4(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		
		return sqlSession.insert("Cms.insertDeptList4", cntMap);
	}

	@Override
	public int insertDocPath(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		
		return sqlSession.insert("Cms.insertDocPath", cntMap);
	}

	@Override
	public int insertDocPath2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		
		return sqlSession.insert("Cms.insertDocPath2", cntMap);
	}

	@Override
	public int updateDocPath(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		
		return sqlSession.update("Cms.updateDocPath", cntMap);
	}

	@Override
	public int updateDocPath2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		return sqlSession.update("Cms.updateDocPath", cntMap);
	}

	@Override
	public int insertVac(SqlSessionTemplate sqlSession, Vacation v) {
		return sqlSession.insert("Cms.insertVac", v);
	}

	@Override
	public List<Object> selectVacList(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Cms.selectVac");
	}

	@Override
	public List<Object> selectVacOne(SqlSessionTemplate sqlSession, int vacNo) {
		
		return sqlSession.selectList("Cms.selectVacOne", vacNo);
	}

	@Override
	public int updateVac(SqlSessionTemplate sqlSession, Vacation v) {
		return sqlSession.update("Cms.updateVac", v);
	}

	@Override
	public int deleteVac(SqlSessionTemplate sqlSession, int vacNo) {
		return sqlSession.update("Cms.deleteVac", vacNo);
	}

	@Override
	public Company selectCompany(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Cms.selectCompany");
	}


	@Override
	public Attachment selectAttachment(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Cms.selectAttachment");
	}

	@Override
	public int updateCompany(SqlSessionTemplate sqlSession, Company c) {
		return sqlSession.update("Cms.updateCompany", c);
	}

	@Override
	public int selectFormCount(SqlSessionTemplate sqlSession, int formNo) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("Cms.selectFormCount", formNo);
	}

	@Override
	public int updateCompanyAttachment(SqlSessionTemplate sqlSession, Attachment at) {
		return sqlSession.update("Cms.updateCompanyAttachment", at);
	}

	@Override
	public int selectPosCount(SqlSessionTemplate sqlSession, int posNo) {
		return sqlSession.selectOne("Cms.selectPosCount", posNo);
	}

	@Override
	public int selectJobCount(SqlSessionTemplate sqlSession, int jobNo) {
		return sqlSession.selectOne("Cms.selectJobCount", jobNo);
	}

	@Override
	public List<Object> selectAuth(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Cms.selectAuth");
	}

	@Override
	public List<Object> selectOperAuth(SqlSessionTemplate sqlSession, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		
		return sqlSession.selectList("Cms.selectOperAuth", null, rowBounds);
	}

	@Override
	public int updateOperAuth(SqlSessionTemplate sqlSession, Authority ah) {
		return sqlSession.update("Cms.updateOperAuth", ah);
	}

	@Override
	public int insertOperAuth(SqlSessionTemplate sqlSession, Authority ah) {
		return sqlSession.insert("Cms.insertOperAuth", ah);
	}

	@Override
	public int getListCount(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Cms.selectOperAuthCount");
	}

	@Override
	public int deleteOperAuth(SqlSessionTemplate sqlSession, Authority ah) {
			
		
		return sqlSession.delete("Cms.deleteOperAuth", ah);
	}

	@Override
	public int deleteOperAuthName(SqlSessionTemplate sqlSession, Authority ah) {
		return sqlSession.update("Cms.deleteOperAuthName", ah);
	}

	@Override
	public int insertDeptListCommon(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		// TODO Auto-generated method stub
		return sqlSession.insert("Cms.insertDeptListCommon", cntMap);
	}

	@Override
	public int insertDeptListCommon4(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		
		return sqlSession.insert("Cms.insertDeptListCommon4", cntMap);
	}

	@Override
	public int insertDeptListCommon3(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		// TODO Auto-generated method stub
		return sqlSession.insert("Cms.insertDeptListCommon3", cntMap);
	}

	@Override
	public int insertDeptListCommon2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		// TODO Auto-generated method stub
		return sqlSession.insert("Cms.insertDeptListCommon2", cntMap);
	}








	
	


}
