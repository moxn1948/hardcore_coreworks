package com.hardcore.cw.cms.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.cms.model.dao.CmsDao;
import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.cms.model.vo.Vacation;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;

@Service
public class CmsServiceImpl implements CmsService{
	@Autowired SqlSessionTemplate sqlSession;
	
	@Autowired CmsDao cd;
	


	@Override
	public List<Object> selectJobList() {
		List<Object> list = null;
		
		list = cd.selectJobList(sqlSession);
		
		return list;
	}
	
	@Override
	public List<Format> selectFormList(PageInfo pi) {
		List<Format> list = null;
		
		list = cd.selectFormList(sqlSession, pi);
		
		return list;
	}

	
	@Override
	public List<Object> selectPostList() {
		List<Object> list = null;
		
		list = cd.selectPosList(sqlSession);
		return list;
	}
	
	@Override
	public List<Object> selectDocPathList() {
		List<Object> list = null;
		
		list = cd.selectDocPathList(sqlSession);
		
		return list;
	}
	@Override
	public List<Object> selectDocPathList2() {
		List<Object> list = null;
		
		list = cd.selectDocPathList2(sqlSession);
		
		return list;
	}
	@Override
	public List<Object> selectVacList() {
		List<Object> list = null;
		
		list = cd.selectVacList(sqlSession);
		return list;
	}



	@Override
	public int deleteJob(Job j) {
		
		return cd.deleteJob(sqlSession, j);
	}


	@Override
	public int updateJobOrder(Job j) {
		
		return cd.updateJobOrder(sqlSession, j);
	}



	@Override
	public int deletePos(Position p) {
		
		return  cd.deletePos(sqlSession, p);
	}


	@Override
	public int updatePosOrder(Position p) {
		
		return cd.updatePosOrder(sqlSession, p);
	}



	@Override
	public int updatePos(Position p) {
		
		return cd.updatePos(sqlSession, p);
	}


	@Override
	public int updateJob(Job j) {
		
		return cd.updateJob(sqlSession, j);
	}


	@Override
	public int updateForm(ArrayList<DocPath> list) {
		int result1 = 0;
		int result2 = 0; 
		int result = 0 ;
		
		result1 = cd.updateForm(sqlSession, list);
		
		return result;
		
		
		
	}



	@Override
	public ArrayList<HashMap<String, Object>> selectForm() {
		List<DocPath> docList = cd.selectForm(sqlSession);
		
		ArrayList<HashMap<String, Object>> docListEdit = new ArrayList<>();
		
		for (int i = 0; i < docList.size(); i++) {
			if(docList.get(i).getPathOrder() == 0) {
				HashMap<String, Object> cntMap = new HashMap<>();
				ArrayList<HashMap<String, Object>> docOrderList = new ArrayList<>();
				
				cntMap.put("pathNo", docList.get(i).getPathNo());
				cntMap.put("pathName", docList.get(i).getPathName());
				cntMap.put("docOrderList", docOrderList);
				
				docListEdit.add(cntMap);
			}
			
		}
		
		for (int i = 0; i < docListEdit.size(); i++) {
			ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) docListEdit.get(i).get("docOrderList");
			
			for(int j = 0; j < docList.size(); j++) {
				if(docList.get(j).getPathOrder() == (Integer) docListEdit.get(i).get("pathNo")) {
					HashMap<String, Object> cntMap = new HashMap<>();
					
					cntMap.put("pathNo", docList.get(j).getPathNo());
					cntMap.put("pathName", docList.get(j).getPathName());
					cntMap.put("pathOrder", docList.get(j).getPathOrder());
					cntMap.put("status", docList.get(j).getStatus());
					
					((ArrayList<HashMap<String, Object>>) docListEdit.get(i).get("docOrderList")).add(cntMap);
				}
			}
		}
		
		return docListEdit;
	}




	@Override
	public int updateFormList(DocPath d) {
		
		return cd.updateFormList(sqlSession, d);
	}


	@Override
	public int updatesubFormList(DocPath d) {
		
		return cd.updatesubFormList(sqlSession, d);
	}


	@Override
	public int deleteSubPath(DocPath d) {
		
		return cd.deleteSubPath(sqlSession, d);
	}


	@Override
	public int deleteTopPath(DocPath d) {
		
		return cd.deleteTopPath(sqlSession, d);
	}


	@Override
	public ArrayList<HashMap<String, Object>> selectListDept() {
		List<Dept> deptList = cd.selectListDept(sqlSession);
		
		ArrayList<HashMap<String, Object>> deptListEdit = new ArrayList<>();
		
		for(int i =0; i < deptList.size(); i++) {
			
			if(deptList.get(i).getDeptOrder() == 0) {
				HashMap<String, Object> cntMap = new HashMap<>();
				ArrayList<HashMap<String, Object>> deptOrderList = new ArrayList<>();
				
				cntMap.put("deptNo", deptList.get(i).getDeptNo());
				cntMap.put("deptName", deptList.get(i).getDeptName());
				cntMap.put("deptOrderList", deptOrderList);
				
				deptListEdit.add(cntMap);
			}
		}
		
		for(int i = 0; i < deptListEdit.size(); i++) {
			for(int j = 0; j < deptList.size(); j++) {
				if(deptList.get(j).getDeptOrder() == (Integer) deptListEdit.get(i).get("deptNo")) {
					HashMap<String, Object> cntMap = new HashMap<>();
					ArrayList<HashMap<String, Object>> deptOrderList = new ArrayList<>();
					
					cntMap.put("deptNo", deptList.get(j).getDeptNo());
					cntMap.put("deptName", deptList.get(j).getDeptName());
					cntMap.put("deptOrder", deptList.get(j).getDeptOrder());
					cntMap.put("deptOrderList", deptOrderList);
					
					((ArrayList<HashMap<String, Object>>) deptListEdit.get(i).get("deptOrderList")).add(cntMap);
				}
			}
		}
		
		for(int i = 0; i <deptListEdit.size(); i++) {
			ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) deptListEdit.get(i).get("deptOrderList");
			
			for(int j = 0; j < list.size(); j++) {
				
				for(int k = 0; k < deptList.size(); k++) {
					
					if(deptList.get(k).getDeptOrder() == (Integer) list.get(j).get("deptNo")) {
						HashMap<String, Object> cntMap = new HashMap<>();
						
						cntMap.put("deptNo", deptList.get(k).getDeptNo());
						cntMap.put("deptName", deptList.get(k).getDeptName());
						cntMap.put("deptOrder", deptList.get(k).getDeptOrder());
						
						
						((ArrayList<HashMap<String, Object>>) list.get(j).get("deptOrderList")).add(cntMap);
					}
				}
			}
		}
		
		return deptListEdit;
	}


	@Override
	public int updateDeptList(Dept d) {
		return cd.updateDeptList(sqlSession, d);
	}


	@Override
	public int updateSubDeptList(Dept d) {
		return cd.updateSubDeptList(sqlSession, d);
	}


	@Override
	public int updateSub2DeptList(Dept d) {
		return cd.updateSub2DeptList(sqlSession, d);
	}


	@Override
	public List<Object> selectDeptNo() {
		
		return cd.selectDeptNo(sqlSession);
	}


	@Override
	public int insertDeptList(HashMap<String, Object> cntMap) {
		
		//상위부서 인서트
		if(cntMap.get("deptNo").equals("0") && cntMap.get("DeptOrder").equals("-1")) {
			cd.insertDeptList(sqlSession, cntMap);
			cd.insertDeptListCommon(sqlSession, cntMap);
		}
		
		//order없는 하위부서1 인서트
		if(cntMap.get("DeptOrder").equals("-2") && cntMap.get("deptNo").equals("0") && !cntMap.get("DeptOrder").equals("-1") && !cntMap.get("DeptOrder").equals("-3")) {
			cd.insertDeptList2(sqlSession, cntMap);
			cd.insertDeptListCommon2(sqlSession, cntMap);
		}
		
		//order없는 하위부서2 인서트
		if(cntMap.get("DeptOrder").equals("-3") && cntMap.get("deptNo").equals("0") && !cntMap.get("DeptOrder").equals("-1") && !cntMap.get("DeptOrder").equals("-2")) {
			cd.insertDeptList4(sqlSession, cntMap);
			cd.insertDeptListCommon4(sqlSession, cntMap);
		}
		
		//order있는 하위부서 인서트
		if(!cntMap.get("DeptOrder").equals("-2") && cntMap.get("deptNo").equals("0") && !cntMap.get("DeptOrder").equals("-1") && !cntMap.get("DeptOrder").equals("-3")) {
			cd.insertDeptList3(sqlSession, cntMap);
			cd.insertDeptListCommon3(sqlSession, cntMap);
		}
		return 0;
	}


	@Override
	public int insertDocPathList(HashMap<String, Object> cntMap) {
		if(cntMap.get("pathNoList").equals("0") && cntMap.get("pathOrder").equals("0")) {
			cd.insertDocPath(sqlSession, cntMap);
		}else {
			cd.updateDocPath(sqlSession, cntMap);
		}
		if(cntMap.get("pathNoList").equals("0") && cntMap.get("pathOrder").equals("-1")) {
			cd.insertDocPath2(sqlSession, cntMap);
		}else {
			cd.updateDocPath2(sqlSession, cntMap);
		}
		
		return 0;
	}


	@Override
	public int insertVac(Vacation v) {
		
		return cd.insertVac(sqlSession, v);
	}


	@Override
	public List<Object> selectVacOne(int vacNo) {
		List<Object> list = null;
		
		
		list = cd.selectVacOne(sqlSession, vacNo);
		return list;
	}


	@Override
	public int updateVac(Vacation v) {
		
		return cd.updateVac(sqlSession, v);
	}


	@Override
	public int deleteVac(int vacNo) {
		return cd.deleteVac(sqlSession, vacNo);
	}


	@Override
	public Company selectCompany() {
		Company c = cd.selectCompany(sqlSession);
		
		return c;
	}


	@Override
	public Attachment selectAttachment() {
		Attachment at = cd.selectAttachment(sqlSession);
		return at;
	}


	@Override
	public int updateCompany(Company c) {
		return cd.updateCompany(sqlSession, c);
	}

	@Override
	public int selectFormCount(int formNo) {
		return cd.selectFormCount(sqlSession, formNo);
	}

	@Override
	public int updateCompanyAttachment(Attachment at) {
		return cd.updateCompanyAttachment(sqlSession, at);
	}

	@Override
	public int selectPosCount(int posNo) {
		return cd.selectPosCount(sqlSession, posNo);
	}

	@Override
	public List<Object> selectOperAuth(PageInfo pi) {
		return cd.selectOperAuth(sqlSession, pi);
	}

	@Override
	public int selectJobCount(int jobNo) {
		return cd.selectJobCount(sqlSession, jobNo);
	}

	@Override
	public List<Object> selectAuth() {
		return cd.selectAuth(sqlSession);
	}

	@Override
	public int updateOperAuth(Authority ah) {
		return cd.updateOperAuth(sqlSession, ah);
	}

	@Override
	public int insertOperAuth(Authority ah) {
		return cd.insertOperAuth(sqlSession, ah);
	}

	@Override
	public int getListCount() {
		return cd.getListCount(sqlSession);
	}

	@Override
	public int deleteOperAuth(Authority ah) {
		int result = 0;
		int result1 = 0;
		int result2 = 0;
		
		result1 = cd.deleteOperAuth(sqlSession, ah);
		result2 = cd.deleteOperAuthName(sqlSession, ah);
		
		if(result1 > 0 && result2 > 0 ) {
			return result;
		}
		return result;
	}


















	

	

}
