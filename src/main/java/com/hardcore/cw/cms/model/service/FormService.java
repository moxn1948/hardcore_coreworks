package com.hardcore.cw.cms.model.service;

import java.util.List;

import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.PriFormat;
import com.hardcore.cw.eas.model.vo.DocPath;

public interface FormService {

	List<DocPath> selectDocPathList();

	List<PriFormat> selectPriFormatList();

	int InsertFormat(Format f);

	PriFormat selectPriFormatDetail(int priFormatNo);

	Format selectFormatDetail(Format f);

	int updateFormat(Format f);

	int getFormatListCount();

	int deleteFormat(Format f);

}
