package com.hardcore.cw.cms.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.cms.model.vo.Vacation;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;

public interface CmsService {


	List<Object> selectJobList();

	int updateJob(Job j);

	int deleteJob(Job j);

	int updateJobOrder(Job j);

	List<Object> selectPostList();

	int updatePos(Position p);

	int deletePos(Position p);

	int updatePosOrder(Position p);

	List<Object> selectDocPathList();

	int updateForm(ArrayList<DocPath> list);


	List<Object> selectDocPathList2();

	ArrayList<HashMap<String, Object>> selectForm();

	int updateFormList(DocPath d);

	int updatesubFormList(DocPath d);

	int deleteSubPath(DocPath d);

	int deleteTopPath(DocPath d);

	List<Format> selectFormList(PageInfo pi);

	ArrayList<HashMap<String, Object>> selectListDept();

	int updateDeptList(Dept d);

	int updateSubDeptList(Dept d);

	int updateSub2DeptList(Dept d);

	List<Object> selectDeptNo();

	int insertDeptList(HashMap<String, Object> cntMap);

	int insertDocPathList(HashMap<String, Object> cntMap);

	int insertVac(Vacation v);

	List<Object> selectVacList();

	List<Object> selectVacOne(int vacNo);

	int updateVac(Vacation v);

	int deleteVac(int vacNo);

	Company selectCompany();

	Attachment selectAttachment();

	int updateCompany(Company c);

	int selectFormCount(int formNo);

	int updateCompanyAttachment(Attachment at);

	List<Object> selectOperAuth(PageInfo pi);

	int selectPosCount(int posNo);

	int selectJobCount(int jobNo);

	List<Object> selectAuth();

	int updateOperAuth(Authority ah);

	int insertOperAuth(Authority ah);

	int getListCount();

	int deleteOperAuth(Authority ah);








}
