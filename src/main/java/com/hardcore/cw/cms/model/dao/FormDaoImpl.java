package com.hardcore.cw.cms.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.PriFormat;
import com.hardcore.cw.eas.model.vo.DocPath;

@Repository
public class FormDaoImpl implements FormDao {

	@Override
	public List<DocPath> selectDocPathList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Cms.selectDocPathList");
	}

	@Override
	public List<PriFormat> selectPriFormatList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Cms.selectPriFormatList");
	}

	@Override
	public int InsertFormat(SqlSessionTemplate sqlSession, Format f) {
		return sqlSession.insert("Cms.InsertFormat", f);
	}

	@Override
	public PriFormat selectPriFormatDetail(SqlSessionTemplate sqlSession, int priFormatNo) {
		return sqlSession.selectOne("Cms.selectPriFormatDetail", priFormatNo);
	}

	@Override
	public Format selectFormatDetail(SqlSessionTemplate sqlSession, Format f) {
		return sqlSession.selectOne("Cms.selectFormatDetail", f);
	}

	@Override
	public int updateFormat(SqlSessionTemplate sqlSession, Format f) {
		return sqlSession.update("Cms.updateFormat", f);
	}

	@Override
	public int getFormatListCount(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Cms.getFormatListCount");
	}

	@Override
	public int deleteFormat(SqlSessionTemplate sqlSession, Format f) {
		return sqlSession.update("Cms.deleteFormat", f);
	}

}
