package com.hardcore.cw.cms.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.cms.model.vo.Vacation;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;

public interface CmsDao {

	int insertJob(SqlSessionTemplate sqlSession, Job j);

	List<Object> selectJobList(SqlSessionTemplate sqlSession);

	int updateJob(SqlSessionTemplate sqlSession, Job j);

	int deleteJob(SqlSessionTemplate sqlSession, Job j);

	int updateJobOrder(SqlSessionTemplate sqlSession, Job j);

	List<Object> selectPosList(SqlSessionTemplate sqlSession);

	
	int updatePos(SqlSessionTemplate sqlSession, Position p);

	int deletePos(SqlSessionTemplate sqlSession, Position p);

	int updatePosOrder(SqlSessionTemplate sqlSession, Position p);


	List<Object> selectDocPathList(SqlSessionTemplate sqlSession);

	int updateForm(SqlSessionTemplate sqlSession, ArrayList<DocPath> list);

	List<Object> selectDocPathList2(SqlSessionTemplate sqlSession);

	List<DocPath> selectForm(SqlSessionTemplate sqlSession);


	int updateFormList(SqlSessionTemplate sqlSession, DocPath d);

	int updatesubFormList(SqlSessionTemplate sqlSession, DocPath d);

	int deleteSubPath(SqlSessionTemplate sqlSession, DocPath d);

	int deleteTopPath(SqlSessionTemplate sqlSession, DocPath d);

	List<Format> selectFormList(SqlSessionTemplate sqlSession, PageInfo pi);

	List<Dept> selectListDept(SqlSessionTemplate sqlSession);

	int updateDeptList(SqlSessionTemplate sqlSession, Dept d);

	int updateSubDeptList(SqlSessionTemplate sqlSession, Dept d);

	int updateSub2DeptList(SqlSessionTemplate sqlSession, Dept d);

	List<Object> selectDeptNo(SqlSessionTemplate sqlSession);

	int insertDeptList(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int insertDeptList2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int insertDeptList3(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int insertDeptList4(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int insertDocPath(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int insertDocPath2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int updateDocPath(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int updateDocPath2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int insertVac(SqlSessionTemplate sqlSession, Vacation v);

	List<Object> selectVacList(SqlSessionTemplate sqlSession);

	List<Object> selectVacOne(SqlSessionTemplate sqlSession, int vacNo);

	int updateVac(SqlSessionTemplate sqlSession, Vacation v);

	int deleteVac(SqlSessionTemplate sqlSession, int vacNo);

	Company selectCompany(SqlSessionTemplate sqlSession);



	Attachment selectAttachment(SqlSessionTemplate sqlSession);

	int updateCompany(SqlSessionTemplate sqlSession, Company c);

	int selectFormCount(SqlSessionTemplate sqlSession, int formNo);

	int updateCompanyAttachment(SqlSessionTemplate sqlSession, Attachment at);

	int selectPosCount(SqlSessionTemplate sqlSession, int posNo);


	int selectJobCount(SqlSessionTemplate sqlSession, int jobNo);

	List<Object> selectAuth(SqlSessionTemplate sqlSession);

	List<Object> selectOperAuth(SqlSessionTemplate sqlSession, PageInfo pi);

	int updateOperAuth(SqlSessionTemplate sqlSession, Authority ah);

	int insertOperAuth(SqlSessionTemplate sqlSession, Authority ah);

	int getListCount(SqlSessionTemplate sqlSession);

	int deleteOperAuth(SqlSessionTemplate sqlSession, Authority ah);

	int deleteOperAuthName(SqlSessionTemplate sqlSession, Authority ah);

	int insertDeptListCommon(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int insertDeptListCommon4(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int insertDeptListCommon3(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);

	int insertDeptListCommon2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);




}
