package com.hardcore.cw.cms.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Authority implements java.io.Serializable{
	private String cpm;
	private String psm;
	private String epm;
	private String clm;
	private String tm;
	private int empDivNo;
}
