package com.hardcore.cw.cms.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.PriFormat;
import com.hardcore.cw.eas.model.vo.DocPath;

public interface FormDao {

	List<DocPath> selectDocPathList(SqlSessionTemplate sqlSession);

	List<PriFormat> selectPriFormatList(SqlSessionTemplate sqlSession);

	int InsertFormat(SqlSessionTemplate sqlSession, Format f);

	PriFormat selectPriFormatDetail(SqlSessionTemplate sqlSession, int priFormatNo);

	Format selectFormatDetail(SqlSessionTemplate sqlSession, Format f);

	int updateFormat(SqlSessionTemplate sqlSession, Format f);

	int getFormatListCount(SqlSessionTemplate sqlSession);

	int deleteFormat(SqlSessionTemplate sqlSession, Format f);
}
