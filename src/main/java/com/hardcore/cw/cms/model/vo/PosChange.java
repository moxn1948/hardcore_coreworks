package com.hardcore.cw.cms.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PosChange implements java.io.Serializable{
	private Date changeDate;
	private int empDivNo;
	private int changeName;
	private int posName;
}
