package com.hardcore.cw.cms.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Dept implements java.io.Serializable{
   private int deptRank;
   private int deptNo;
   private String deptName;
   private String status;
   private int deptOrder;
}