package com.hardcore.cw.cms.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.cms.model.service.CmsService;
import com.hardcore.cw.cms.model.service.FormService;
import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.PriFormat;
import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;

@Controller
@SessionAttributes("loginUser")
public class FormController {
	
	private final FormService fs;
	private final CmsService cs;
	
	public FormController(FormService fs, CmsService cs) {
		this.fs = fs;
		this.cs = cs;
	}
	
	@RequestMapping("selectFormList.cm")
	public String selectForm(Model model, Format f, HttpServletRequest request) {
		
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		int listCount = fs.getFormatListCount();
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		List<Format> list = cs.selectFormList(pi);
		
		model.addAttribute("list", list);
		model.addAttribute("pi", pi);

		return "form_list";
	}
	
	@GetMapping("formNew.cm")
	public String showFormWriter(Model m) {
		List<PriFormat> priList = fs.selectPriFormatList();
		List<DocPath> docList = fs.selectDocPathList();
		
		m.addAttribute("docPathList", docList);
		m.addAttribute("priFormatList", priList);
		
		return "form_new";
	}
	
	@PostMapping("formInsert.cm")
	public String insertForm(Model m, HttpServletRequest request, Format f) {
		String form = request.getParameter("form");
		
		// 기본양식으로 선택했으면 기존의 작성내용은 없는채로 데이터베이스에 넣어지며
		if(form.equals("provide")) {
			f.setFormatCnt(null);
		} else {
			// 직접 입력으로 선택했으면 기본양식에서 선택한 항목은 없는걸로 함
			f.setPriFormatNo(0);
		}
		
		// PERIOD_YN -> 값이 있으면 N, 없으면 Y
		if(f.getPeriodYn() != null) {
			f.setPeriodYn("N");
		} else {
			f.setPeriodYn("Y");
		}
		
		fs.InsertFormat(f);
			
		return "redirect:selectFormList.cm";
	}
	
	@GetMapping("formatDetail.cm")
	public String detailForm(Format f, Model m) {
		
		if(f.getPriFormatNo() > 0) {
			PriFormat p = fs.selectPriFormatDetail(f.getPriFormatNo());
			
			m.addAttribute("p", p);
		}
		
		Format ff = fs.selectFormatDetail(f);
		
		m.addAttribute("f", ff);
		
		return "form_det";
	}
	
	@PostMapping("showUpdateFormat.cm")
	public String showUpdateFormat(Format f, Model m) {
		
		Format ff = fs.selectFormatDetail(f);
		
		List<PriFormat> priList = fs.selectPriFormatList();
		List<DocPath> docList = fs.selectDocPathList();
		
		m.addAttribute("docPathList", docList);
		m.addAttribute("priFormatList", priList);
		m.addAttribute("f", ff);
		
		return "form_up";
	}
	
	@PostMapping("updateFormat.cm")
	public String updateFormat(Format f, Model m, HttpServletRequest request) {
		
		String form = request.getParameter("form");
		
		// 기본양식으로 선택했으면 기존의 작성내용은 없는채로 데이터베이스에 넣어지며
		if(form.equals("provide")) {
			f.setFormatCnt(null);
		} else {
			// 직접 입력으로 선택했으면 기본양식에서 선택한 항목은 없는걸로 함
			f.setPriFormatNo(0);
		}
		
		if(f.getPeriodYn() != null) {
			f.setPeriodYn("N");
		} else {
			f.setPeriodYn("Y");
		}
		
		fs.updateFormat(f);
		
		return "redirect:selectFormList.cm";
	}
	
	@GetMapping("showDeleteFormat.cm")
	public String deleteFormat(Format f) {
		fs.deleteFormat(f);
		
		return "redirect:selectFormList.cm";
	}
}
