package com.hardcore.cw.cms.controller;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.cms.model.service.CmsService;
import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.cms.model.vo.Vacation;
import com.hardcore.cw.common.CommonsUtils;
import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;
import com.hardcore.cw.ems.model.service.SelectEmpService;
import com.hardcore.cw.first.model.service.FirstService;

@Controller

public class CmsController {
	private static final Logger logger = LoggerFactory.getLogger(CmsController.class);
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
@Autowired
private CmsService cs;

@Autowired
private FirstService fs;

@Autowired
private SelectEmpService ses;

@Autowired
private AddrMenuService ams;
	
	@ExceptionHandler(RuntimeException.class)
	public String exceptionHandler(Model model, Exception e) {
		model.addAttribute("exception", e);
		return "errorPage";
	}
	
	@RequestMapping("selectOperAuth.cm")
	public ModelAndView selectOperAuth(HttpServletRequest request, ModelAndView mv) {
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();

		List<HashMap<String, Object>> empList = ams.selectEmpList();
		
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		int listCount = cs.getListCount();
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		List<Object> list = cs.selectOperAuth(pi);
		
		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		mv.addObject("pi", pi);
		mv.addObject("list", list);
		mv.setViewName("oper_auth");
		return mv;
	}
	
	@RequestMapping("selectVacOne.cm")
	public ModelAndView selectVacOne(ModelAndView mv, int vacNo) {
		
		List<Object> list = cs.selectVacOne(vacNo);
		
		if(list != null) {
			mv.addObject("list", list);
			mv.setViewName("holi_det");
		}else {
			mv.addObject("msg", "로드 실패!");
			mv.addObject("view", "index.jsp");
		}
		return mv;
	}
	@RequestMapping("selectCompany.cm")
	public ModelAndView selectCompany(ModelAndView mv) {
		Company c = cs.selectCompany();
		Attachment at = cs.selectAttachment();
		if(c != null && at != null) {
			mv.addObject("c", c);
			mv.addObject("at", at);
			mv.setViewName("company_det");
		}
		return mv;
	}
	@RequestMapping("updateCompany.cm")
	public ModelAndView updateCompany(ModelAndView mv, String companyName) {
		Company c = cs.selectCompany();
		Attachment at = cs.selectAttachment();
		if(c != null && at != null) {
			mv.addObject("c", c);
			mv.addObject("at", at);
			mv.setViewName("company_up");
		}
		
		return mv;
	}
	
	@RequestMapping("insertVacForm.cm")
	public String insertVacForm(Model model) {
		
		return "holi_set";
	}
	@RequestMapping("selectVacList.cm")
	public String selectVac(Model model, Vacation v) {
		List<Object> list = cs.selectVacList();
		
		
		model.addAttribute("list", list);
		
		
		return "holi_list";
	}

	@RequestMapping("selectListJob.cm")
	public String selectJob(Model model, Job j) {
		
		List<Object> list = cs.selectJobList();
		
		model.addAttribute("list", list);
		
		return "job_struct";
	}
	
	@GetMapping("selectListformStruct.cm")
	public ModelAndView selectForm(ModelAndView mv) {
		ArrayList<HashMap<String,Object>> docList = cs.selectForm();
		
		if(docList != null) {
			mv.addObject("docList", docList);
			mv.setViewName("form_struct");
		}else {
			mv.addObject("msg", "로드 실패!");
			mv.addObject("view", "index.jsp");
		}
		
		return mv;
		
	}
	@GetMapping("selectListDept.cm")
	public ModelAndView selectListDept(ModelAndView mv) {
		ArrayList<HashMap<String, Object>> deptList = cs.selectListDept();
		
		if(deptList != null) {
			mv.addObject("deptList", deptList);
			mv.setViewName("dept_struct");
		}else {
			mv.addObject("msg", "조회 실패");
			mv.addObject("view", "index.jsp");
		}
		return mv;
	}

	@RequestMapping("selectListPos.cm")
	public String selectPos(Model model, Position p) {
		
		List<Object> list = cs.selectPostList();
		model.addAttribute("list", list);
		
		return "pos_struct";
	}
	
	
	@PostMapping("updateJob.cm")
	public ModelAndView updateJob(ModelAndView mv, HttpServletRequest request) {
		String jobName[] = request.getParameterValues("jobName");
		String jNo[] = request.getParameterValues("jNo");
		String jobNo = request.getParameter("jobNo");
		String joNo[] = jobNo.split(", ");
		String jobOd = request.getParameter("jobOd"); 
		String jobNm = request.getParameter("jobNm"); 
		String joOd[] = jobOd.split(", "); 
		String joNm[] = jobNm.split(", ");
		

		List<Job> jobList = new ArrayList<>();
		
		if(jobNo.equals("")) {		
			for(int i = 0; i < jobName.length; i++) {	
				Job j = new Job();
				
				j.setJobName(joNm[i]);		
				j.setJobOrder(Integer.parseInt(joOd[i]));
				
				if(i < jNo.length) {
					j.setJobNo(Integer.parseInt(jNo[i]));
				}else {
					j.setJobNo(0);
				}
				
			
				
				cs.updateJob(j);	
			}						
		}else {						
			for(int i = 0; i < joNo.length; i++) {	
				Job j = new Job();
				j.setJobNo(Integer.parseInt(joNo[i]));							
				
				cs.deleteJob(j);
				cs.updateJobOrder(j);
		}
	}

	mv.setViewName("redirect:selectListJob.cm");
	return mv;
}
	
	
	@PostMapping("updateDeptList.cm")
	public ModelAndView updateDeptList(ModelAndView mv, HttpServletRequest request) {
		//상위부서 이름
		String DeptNameSubmit = request.getParameter("DeptNameSubmit");
		String dnsSplit[] = DeptNameSubmit.split(", ");
		String topDeptName[] = request.getParameterValues("topDeptName");
		String DeptOrder[] = request.getParameterValues("DeptOrder");
		//
		
		//하위부서1 이름
		String subDeptNameSubmit = request.getParameter("subDeptNameSubmit");
		String subDeptName[] = request.getParameterValues("subDeptName");
		//
		
		//하위부서1 부서순서
		String subDeptOrderSubmit = request.getParameter("subDeptOrderSubmit");
		//
		
		//하위부서2 이름
		String sub2DeptNameSubmit = request.getParameter("sub2DeptNameSubmit");
		String sub2DeptName[] = request.getParameterValues("sub2DeptName");
		
		//
				
		//하위부서2 부서순서
		String sub2DeptOrderSubmit = request.getParameter("sub2DeptOrderSubmit");
		
		//
		
		//상위 하위1 하위2 부서번호
		String deptNoList[] = request.getParameterValues("deptNoList");
		String subDeptNo[] = request.getParameterValues("subDeptNo");
		String sub2DeptNo[] = request.getParameterValues("sub2DeptNo");
		//
		
		for(int i =0; i < topDeptName.length; i++) {
			HashMap<String, Object> cntMap = new HashMap<>();
				if(!topDeptName[i].equals("-1")) {
				cntMap.put("deptNo", deptNoList[i]);
				cntMap.put("deptName", topDeptName[i]);
				cntMap.put("DeptOrder", DeptOrder[i]);
				}			
				
				if(!subDeptName[i].equals("-1") && !subDeptName[i].equals("")) {
					cntMap.put("deptNo", deptNoList[i]);
					cntMap.put("deptName", subDeptName[i]);
					cntMap.put("DeptOrder",  DeptOrder[i]);
				}
				
				if(!sub2DeptName[i].equals("-2") && !sub2DeptName[i].equals("")) {
					cntMap.put("deptNo", deptNoList[i]);
					cntMap.put("deptName", sub2DeptName[i]);
					cntMap.put("DeptOrder", DeptOrder[i]);
				}
				cs.insertDeptList(cntMap);
				
		}
		

		mv.setViewName("redirect:selectListDept.cm");
		return mv;
		
	}
	
	
	@PostMapping("updatePos.cm")
	public ModelAndView updatePos(ModelAndView mv, HttpServletRequest request) {
		String pNo[] = request.getParameterValues("pNo");
		String posNo = request.getParameter("posNo");
		String poNo[] = posNo.split(", ");		
		String posOd = request.getParameter("posOd"); 
		String posNm = request.getParameter("posNm"); 
		String poOd[] = posOd.split(", "); 
		String psNm[] = posNm.split(", ");
		
		 
		List<Position> posList = new ArrayList<>();
		
		if(posNo.equals("")) {
			for(int i = 0; i < psNm.length; i++) {
				Position p = new Position();	
				p.setPosName(psNm[i]);		
				p.setPosOrder(Integer.parseInt(poOd[i]));
				if(i < pNo.length) {
					p.setPosNo(Integer.parseInt(pNo[i]));
				}else {
					p.setPosNo(0);
				}
				
				
				cs.updatePos(p);
			}
		}else {
			for(int i =0; i < poNo.length; i++) {
				Position p = new Position();
				p.setPosNo(Integer.parseInt(poNo[i]));
				
				cs.deletePos(p);
				cs.updatePosOrder(p);
			}
		}
		
		mv.setViewName("redirect:selectListPos.cm");
	
		return mv;	
	}
	
	@PostMapping("updateDocPath.cm")
	public ModelAndView updateForm(HttpServletRequest request, DocPath d, ModelAndView mv ,String formNo[], String subformNo[], String subStatus[]) {
		String formName = request.getParameter("formName");
		String subName = request.getParameter("subName");
		String subOrder = request.getParameter("subOrder");
		String subdelNo = request.getParameter("subdelNo");
		String delformNo = request.getParameter("delformNo");
		String formNameSplit[] = formName.split(", ");
		String pathNoList[] = request.getParameterValues("pathNoList");
		String topDocPathName[] = request.getParameterValues("topDocPathName");
		String subDocPathName[] = request.getParameterValues("subDocPathName");
		String pathOrder[] = request.getParameterValues("pathOrder");
		
		for(int i = 0; i < topDocPathName.length; i++) {
			HashMap<String, Object> cntMap = new HashMap<>();
			if(!topDocPathName[i].equals("-1")) {
				cntMap.put("pathName", topDocPathName[i]);
				cntMap.put("pathNoList", pathNoList[i]);
				cntMap.put("pathOrder", pathOrder[i]);
			}
			if(!subDocPathName[i].equals("-1")) {
				cntMap.put("pathName", subDocPathName[i]);
				cntMap.put("pathNoList", pathNoList[i]);
				cntMap.put("pathOrder", pathOrder[i]);
			}
			cs.insertDocPathList(cntMap);
		}
		
		
//		for(int i = 0; i < formNameSplit.length; i++) {
//			d.setPathName(formNameSplit[i]);
//			
//			if(i < formNo.length) {
//				d.setPathNo(Integer.parseInt(formNo[i]));
//			}else {
//				d.setPathNo(0);
//			}
//			
//			//cs.updateFormList(d);
//		}
		
//		if(!subOrder.equals("") && !subName.equals("")) {
//			String subNameSplit[] = subName.split(", ");
//			String subOrderSplit[] = subOrder.split(", ");
//			
//			
//			
//		for(int i = 0; i < subOrderSplit.length; i++) {
//			d.setPathOrder(Integer.parseInt(subOrderSplit[i]));
//			d.setPathName(subNameSplit[i]);
//			d.setPathNo(Integer.parseInt(subformNo[i]));
//		
//			
//			//cs.updatesubFormList(d);
//		}
//		
//		}
//		
		if(!subdelNo.equals("")) {
			String subdelNoSplit[] = subdelNo.split(", ");
			for(int i = 0; i < subdelNoSplit.length; i++) {
				
				d.setPathNo(Integer.parseInt(subdelNoSplit[i]));
				
				cs.deleteSubPath(d);
				
			}
		}
		
		if(!delformNo.equals("")) {
			String delformNoSplit[] = delformNo.split(", ");
			for(int i = 0; i < delformNoSplit.length; i++) {
				
			
				d.setPathNo(Integer.parseInt(delformNoSplit[i]));
				cs.deleteTopPath(d);
			}
			
		}
		
		if(d != null) {
			mv.setViewName("redirect:selectListformStruct.cm");
		}else {
			mv.addObject("msg", "로드 실패!");
			mv.addObject("view", "index.jsp");
		}
		
		return mv;
		
	}
	
	
	@PostMapping("insertVac.cm")
	public ModelAndView insertVac(HttpServletRequest request, ModelAndView mv, Vacation v, String vacName, String useYn, String enforceYear, String repeatYn, String startDate, String endDate) {
			
			try {
				SimpleDateFormat sDate = new SimpleDateFormat("yy/mm/dd");
				Date sDate1 = null;
				Date sDate2 = null;
				sDate1 = sDate.parse(startDate);
				sDate2 = sDate.parse(endDate);
				
				
				java.sql.Date sdate = new java.sql.Date(sDate1.getTime());
				java.sql.Date edate = new java.sql.Date(sDate2.getTime());
				
				if(useYn == null) {
					v.setUsingYn("N");
				}else {
					v.setUsingYn(useYn);
					
				}
				
				if(repeatYn.equals("Y")) {
					v.setEnforceYear(null);
				}else {					
					v.setEnforceYear(enforceYear);
				}
				
				v.setVacName(vacName);
				v.setVacSdate(sdate);
				v.setVacEdate(edate);
				v.setRepeatYn(repeatYn);
				
				int result = cs.insertVac(v);
				
				if(result > 0) {
					mv.setViewName("redirect:selectVacList.cm");
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		return mv;
		
	}
	
	@PostMapping("updateVac.cm")
	public ModelAndView updateVac(HttpServletRequest request, ModelAndView mv, Vacation v, String vacName, String useYn, String enforceYear, String repeatYn, String startDate, String endDate, int vacNo) {
		int result = 0;
		
		try {
			SimpleDateFormat sDate = new SimpleDateFormat("yy/mm/dd");
			Date sDate1 = null;
			Date sDate2 = null;
			sDate1 = sDate.parse(startDate);
			sDate2 = sDate.parse(endDate);
			
			
			java.sql.Date sdate = new java.sql.Date(sDate1.getTime());
			java.sql.Date edate = new java.sql.Date(sDate2.getTime());
			
			if(useYn == null) {
				v.setUsingYn("N");
			}else {
				v.setUsingYn(useYn);
				
			}
			
			if(repeatYn.equals("Y")) {
				v.setEnforceYear(null);
			}else {					
				v.setEnforceYear(enforceYear);
			}
			
			v.setVacNo(vacNo);
			v.setVacName(vacName);
			v.setVacSdate(sdate);
			v.setVacEdate(edate);
			v.setRepeatYn(repeatYn);
			
			
			result = cs.updateVac(v);
			
			if(result > 0) {
				mv.setViewName("redirect:selectVacList.cm");
			}else {
				
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return mv;
		
	}
	
	@RequestMapping("deleteVac.cm")
	public ModelAndView deleteVac(ModelAndView mv, int vacNo) {
		int result = cs.deleteVac(vacNo);
		
		if(result > 0) {
			mv.setViewName("redirect:selectVacList.cm");
		}
		
		return mv;
		
	}
	
	@PostMapping("updateCompanyForm.cm")
	public ModelAndView updateCompanyForm(HttpServletRequest request, ModelAndView mv,
			@RequestParam(name="attach") MultipartFile attach,
			int fileNo,
			Company c) {
		
			Attachment at = new Attachment();
			String root = request.getSession().getServletContext().getRealPath("resources");
			
			String filePath = root + "\\uploadFiles";
			String originFileName1 = attach.getOriginalFilename();
			
			String ext1 = originFileName1.substring(originFileName1.lastIndexOf("."));
			
			String changeName = CommonsUtils.getRandomString();
			//at.setFileNo(fileNo);
			at.setFileNo(fileNo);
			at.setOriginName(originFileName1);
			at.setChangeName(changeName + ext1);
			at.setFilePath(filePath);
			try {
				attach.transferTo(new File(filePath + "\\" + changeName + ext1));
				
				
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
			int result = cs.updateCompany(c);
			int result2 = cs.updateCompanyAttachment(at);
			
			
			if(result > 0 ) {
				mv.setViewName("redirect:selectCompany.cm");
			}
		 
		
		
		
		mv.setViewName("redirect:selectCompany.cm");
		
		return mv;
	}
	
	@GetMapping("selectFormCount.cm")
	public ModelAndView selectFormCount(ModelAndView mv, int formNo) {
		int formCount = cs.selectFormCount(formNo);
//		
		mv.setViewName("jsonView");
		mv.addObject("formCount", formCount);
//		
		return mv;
	}
	@GetMapping("selectPosCount.cm")
	public ModelAndView selectPosCount(ModelAndView mv, int posNo) {
		int posCount = cs.selectPosCount(posNo);
		
		mv.setViewName("jsonView");
		mv.addObject("posCount", posCount);
		
		return mv;
	}
	
	@GetMapping("selectJobCount.cm")
	public ModelAndView selectJobCount(ModelAndView mv, int jobNo) {
		int jobCount = cs.selectJobCount(jobNo);
		
		mv.setViewName("jsonView");
		mv.addObject("jobCount", jobCount);
		
		return mv;
	}

	
	
	
	@PostMapping("updateOperAuth.cm")
	public ModelAndView updateOperAuth(HttpServletRequest request, ModelAndView mv, Authority ah, Employee e, int empDivNo[], String cpm[], String psm[], String epm[], String tm[], String clm[]) {
		int result = 0;
		int result2 = 0;
		String delNo = request.getParameter("delNo");
		System.out.println("delNo : " + delNo);
		
		if(delNo == null || delNo.equals("")) {
			for(int i = 0; i < cpm.length; i++) {
				ah.setCpm(cpm[i]);
				ah.setPsm(psm[i]);
				ah.setEpm(epm[i]);
				ah.setTm(tm[i]);
				ah.setClm(clm[i]);
				ah.setEmpDivNo(empDivNo[i]);
				
				result = cs.insertOperAuth(ah);
				
				result2 = fs.updateEmpAuth(ah);
			}
		
			}else {
			String delNoSplit[] = delNo.split(", ");
			
			for(int i = 0; i < delNoSplit.length; i++) {
				ah.setEmpDivNo(Integer.parseInt(delNoSplit[i]));
				cs.deleteOperAuth(ah);
				}
		
			}
		
		
			
			//}
		mv.setViewName("redirect:selectOperAuth.cm");
		
		return mv;
	}

	

}