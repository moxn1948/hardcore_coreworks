package com.hardcore.cw.chat.controller;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint.Basic;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

@Controller
@ServerEndpoint("/echo")
public class ChatEndPoint {

	private static final List<Session> sessionList = new ArrayList<>();
	private static final Logger logger = LoggerFactory.getLogger(ChatEndPoint.class);
	
	public ChatEndPoint() {
		System.out.println("websocket open");
	} 
	
	@OnOpen
	public void onOpen(Session session) {
		logger.info("open session id : " + session.getId());
		
		final Basic basic = session.getBasicRemote();
		
		sessionList.add(session);
	}
	
	private void sendAllSessionToMessage(Session self, String msg) {
		try {
			for(Session session : ChatEndPoint.sessionList) {
				if(self.getId() != session.getId()) {
					session.getBasicRemote().sendText("msg" + msg);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@OnMessage
	public void onMessage(String msg, Session session) {
		logger.info("Message From "+ msg);
		
		try {
            final Basic basic = session.getBasicRemote();
           // basic.sendText("msg" +msg);
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
        sendAllSessionToMessage(session, msg);
	}
	
	@OnError
	public void onError(Throwable e, Session session) {
		System.out.println(e);
		System.out.println(e.getMessage());
		System.out.println(e.getCause());
		System.out.println(e.getLocalizedMessage());
		System.out.println(e.getStackTrace());
	}
	
	@OnClose
	public void onClose(Session session) {
		logger.info("Session : " + session.getId() + "has ended");
		sessionList.remove(session);
	}
	
}
