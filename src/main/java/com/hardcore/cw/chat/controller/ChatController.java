package com.hardcore.cw.chat.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.chat.model.service.ChatService;
import com.hardcore.cw.chat.model.vo.Chat;
import com.hardcore.cw.chat.model.vo.ChatAndEmp;
import com.hardcore.cw.chat.model.vo.ChatAndRoom;

@Controller
public class ChatController {
	private static final Logger logger = LoggerFactory.getLogger(ChatController.class);
	
	@Autowired
	private ChatService cs; 
	
	@RequestMapping("createChatRoom")
	public ModelAndView createChatRoom(ModelAndView mv, String empList, String roomName) {
		String[] empNoList = empList.split(", ");

		int result = cs.insertChatRoom(empNoList, roomName);

		mv.addObject("roomNo", result);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("selectChatRoomList")
	public ModelAndView selectChatRoomList(ModelAndView mv, int eNo, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		List<ChatAndRoom> list = cs.selectChatRoomList(eNo);
		
		int idx = 0;
		String roomNoList = "";
		for(ChatAndRoom cr : list) {
			if(idx == 0) {
				roomNoList += cr.getRoomNo();
			} else {
				roomNoList += ", " + cr.getRoomNo();
			}
			idx++;
		}
		
		List<Integer> unreadCountList = cs.selectUnreadCount(eNo, roomNoList);
		
		mv.addObject("unread", unreadCountList);
		mv.addObject("list", list);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("insertChatting")
	public ModelAndView insertChatting(ModelAndView mv, String chat, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		int roomNo = Integer.parseInt(chat.substring(chat.indexOf("@") + 1, chat.lastIndexOf("@")));
		int eNo = Integer.parseInt(chat.substring(chat.indexOf("$") + 1, chat.lastIndexOf("$")));
		String message = chat.split(":")[1];
		
		Chat c = new Chat();
		c.setChatCnt(message);
		c.setRoomNo(roomNo);
		c.setEmpDivNo(eNo);
		
		cs.insertChat(c);
		
		ChatAndEmp ca = cs.selectChatting(c);
		
		mv.addObject("chat", ca);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("selectChatList")
	public ModelAndView selectChatList(ModelAndView mv, int roomNo, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		List<ChatAndEmp> list = cs.selectChatList(roomNo);
		
		mv.addObject("list", list);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("selectChatting")
	public ModelAndView selectChatting(ModelAndView mv, String text, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		if(text.startsWith("msg")) {
			int roomNo = Integer.parseInt(text.substring(text.indexOf("@") + 1, text.lastIndexOf("@")));
			int eNo = Integer.parseInt(text.substring(text.indexOf("$") + 1, text.lastIndexOf("$")));
			String message = text.split(":")[1];
			
			Chat c = new Chat();
			c.setChatCnt(message);
			c.setRoomNo(roomNo);
			c.setEmpDivNo(eNo);
			
			ChatAndEmp cae = cs.selectChatting(c);
			
			mv.addObject("chat", cae);
		}
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("selectReceiveChatting")
	public ModelAndView selectReceiveChatting(ModelAndView mv, String text, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		
		if(text.startsWith("msg")) {
			int eNo = Integer.parseInt(text.substring(text.indexOf("$") + 1, text.lastIndexOf("$")));
			String e = cs.selectEmployee(eNo);
			
			mv.addObject("emp", e);
		}
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("selectUnreadCount")
	public ModelAndView selectUnreadCount(ModelAndView mv, String eNo, String roomNoList) {
		int no = Integer.parseInt(eNo);
		List<Integer> unreadCountList = cs.selectUnreadCount(no, roomNoList);
		
		mv.addObject("unread", unreadCountList);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("selectUnreadChat")
	public ModelAndView selectUnreadChat(ModelAndView mv, String eNo, String roomNo, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		List<Chat> list = cs.selectUnreadChat(eNo, roomNo);
		
		mv.addObject("list", list);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("updateRead")
	public ModelAndView updateRead(ModelAndView mv, String chatList, String eNo) {
		cs.updateRead(chatList, eNo);
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("selectSender")
	public ModelAndView selectSender(ModelAndView mv, String eNo, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		
		String sender = cs.selectSender(eNo);
		
		mv.addObject("sender", sender);
		mv.setViewName("jsonView");
		return mv;
	}
	
}
