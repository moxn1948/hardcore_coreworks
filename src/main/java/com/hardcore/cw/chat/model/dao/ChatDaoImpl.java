package com.hardcore.cw.chat.model.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.chat.model.vo.Chat;
import com.hardcore.cw.chat.model.vo.ChatAndEmp;
import com.hardcore.cw.chat.model.vo.ChatAndRoom;
import com.hardcore.cw.chat.model.vo.ChatRoom;

@Repository
public class ChatDaoImpl implements ChatDao {
 
	@Override
	public int insertChatRoom(SqlSessionTemplate sqlSession, ChatRoom cr) {
		return sqlSession.insert("Chat.insertChatRoom", cr);
	}

	@Override
	public int selectChatRoomSeq(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Chat.selectChatRoomSeq");
	}

	@Override
	public List<ChatAndRoom> selectChatRoomList(SqlSessionTemplate sqlSession, int eNo) {
		return sqlSession.selectList("Chat.selectChatRoomList", eNo);
	}

	@Override
	public int insertChat(SqlSessionTemplate sqlSession, Chat c) {
		return sqlSession.insert("Chat.insertChat", c);
	}

	@Override
	public List<Integer> selectRoomNoList(SqlSessionTemplate sqlSession, List<String> empNo) {
		return sqlSession.selectList("Chat.selectRoomNoList", empNo);
	}

	@Override
	public int selectRoomUserCount(SqlSessionTemplate sqlSession, Integer i) {
		return sqlSession.selectOne("Chat.selectRoomUserCount", i);
	}

	@Override
	public List<ChatAndEmp> selectChatList(SqlSessionTemplate sqlSession, int roomNo) {
		return sqlSession.selectList("Chat.selectChatList", roomNo);
	}

	@Override
	public ChatAndEmp selectChatting(SqlSessionTemplate sqlSession, Chat c) {
		return sqlSession.selectOne("Chat.selectChatting", c);
	}

	@Override
	public String selectEmployee(SqlSessionTemplate sqlSession, int eNo) {
		return sqlSession.selectOne("Chat.selectEmployee", eNo);
	}

	@Override
	public int selectUnreadCount(SqlSessionTemplate sqlSession, int no, int roomNo) {
		Map<String, Object> map = new HashMap<>();
		map.put("eNo", no);
		map.put("roomNo", roomNo);
		
		return sqlSession.selectOne("Chat.selectUnreadCount", map);
	}

	@Override
	public List<Chat> selectUnreadChat(SqlSessionTemplate sqlSession, String eNo, String roomNo) {
		Map<String, Object> map = new HashMap<>();
		map.put("eNo", eNo);
		map.put("roomNo", roomNo);
		
		return sqlSession.selectList("Chat.selectUnreadChat", map);
	}

	@Override
	public void updateRead(SqlSessionTemplate sqlSession, int eNo, int chatNo) {
		Map<String, Object> map = new HashMap<>();
		map.put("eNo", eNo);
		map.put("chatNo", chatNo);
		
		sqlSession.insert("Chat.insertRead", map);
	}

	@Override
	public String selectSender(SqlSessionTemplate sqlSession, String eNo) {
		return sqlSession.selectOne("Chat.selectSender", eNo);
	}

}
