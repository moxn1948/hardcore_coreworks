package com.hardcore.cw.chat.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
 
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ChatRoomAndEmp {
	private int roomNo;
	private int empDivNo;
	private int empNo;
	private String empId;
	private String empPwd;
	private String empName;
	private String extPhone;
	private String phone;
	private Date birth;
	private String address;
	private String gender;
	private String fax;
	private Date enrollDate;
	private Date entDate;
	private int jobNo;
	private String jobName;
	private int posNo;
	private String posName;
	private int deptNo;
	private String deptName;
	private String auth;
	private String entYn;
	private String status;
}
