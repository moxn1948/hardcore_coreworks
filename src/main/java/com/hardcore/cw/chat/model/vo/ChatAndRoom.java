package com.hardcore.cw.chat.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
 
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ChatAndRoom {
	private int chatNo;
	private String chatDate;
	private String chatCnt;
	private int roomNo;
	private int empDivNo;
	private String roomName;
	private int userCount;
}
