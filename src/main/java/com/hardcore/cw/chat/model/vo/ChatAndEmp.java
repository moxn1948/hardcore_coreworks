package com.hardcore.cw.chat.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
 
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ChatAndEmp {
	private int chatNo;
	private String chatDate;
	private String chatCnt;
	private int roomNo;
	private int empDivNo;
	private int empNo;
	private String empId;
	private String empName;
	private String extPhone;
	private String phone;
	private String address;
	private String gender;
	private String fax;
	private int jobNo;
	private int posNo;
	private int deptNo;
}
