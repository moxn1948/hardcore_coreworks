package com.hardcore.cw.chat.model.service;

import java.util.List;

import com.hardcore.cw.chat.model.vo.Chat;
import com.hardcore.cw.chat.model.vo.ChatAndEmp;
import com.hardcore.cw.chat.model.vo.ChatAndRoom;
import com.hardcore.cw.chat.model.vo.ChatRoom;
import com.hardcore.cw.common.model.vo.Employee;

public interface ChatService {
 
	int insertChatRoom(String[] empNoList, String roomName);

	List<ChatAndRoom> selectChatRoomList(int eNo);

	int insertChat(Chat c);

	List<ChatAndEmp> selectChatList(int roomNo);

	ChatAndEmp selectChatting(Chat c);

	String selectEmployee(int eNo);

	List<Integer> selectUnreadCount(int no, String roomNoList);

	List<Chat> selectUnreadChat(String eNo, String roomNo);

	void updateRead(String chatList, String eNo);

	String selectSender(String eNo);

}
