package com.hardcore.cw.chat.model.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.chat.model.dao.ChatDao;
import com.hardcore.cw.chat.model.vo.Chat;
import com.hardcore.cw.chat.model.vo.ChatAndEmp;
import com.hardcore.cw.chat.model.vo.ChatAndRoom;
import com.hardcore.cw.chat.model.vo.ChatRoom;

@Service
public class ChatServiceImpl implements ChatService {
	 
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private ChatDao cd;

	@Override
	public int insertChatRoom(String[] empNoList, String roomName) {
		List<String> empNo = Arrays.asList(empNoList);
		List<Integer> roomNoList = cd.selectRoomNoList(sqlSession, empNo);
		
		int result = 0;
		
		int count = 0;
		for(int i = 0; i < roomNoList.size(); i++) {
			if(cd.selectRoomUserCount(sqlSession, roomNoList.get(i)) == empNoList.length) {
				count++;
			}
		}
		
		int seq = 0;
		if(count == 0) {
			seq = cd.selectChatRoomSeq(sqlSession);
			int idx = 0;
			
			for(String s : empNoList) {
				ChatRoom cr = new ChatRoom();
				cr.setRoomNo(seq);
				cr.setEmpDivNo(Integer.parseInt(s));
				String room = "";
				for(int i = 0; i < empNoList.length; i++) {
					if(i != idx) {
						if(room.equals("")) {
							room += roomName.split(",")[i].trim(); // 에러남
						} else {
							room += ", " + roomName.split(",")[i].trim();
						}
					}
				}
				cr.setRoomName(room);
				result = cd.insertChatRoom(sqlSession, cr);
				idx++;
			}
		}
		
		return seq;
	}

	@Override
	public List<ChatAndRoom> selectChatRoomList(int eNo) {
		return cd.selectChatRoomList(sqlSession, eNo);
	}

	@Override
	public int insertChat(Chat c) {
		return cd.insertChat(sqlSession, c);
	}

	@Override
	public List<ChatAndEmp> selectChatList(int roomNo) {
		return cd.selectChatList(sqlSession, roomNo);
	}

	@Override
	public ChatAndEmp selectChatting(Chat c) {
		return cd.selectChatting(sqlSession, c);
	}

	@Override
	public String selectEmployee(int eNo) {
		return cd.selectEmployee(sqlSession, eNo);
	}

	@Override
	public List<Integer> selectUnreadCount(int no, String roomNoList) {
		ArrayList<Integer> list = new ArrayList<>();
		String[] noList = roomNoList.split(", ");
		for(String s : noList) {
			if(!s.equals("")) {
				int result = cd.selectUnreadCount(sqlSession, no, Integer.parseInt(s));
				list.add(result);
			}
		}
		
		return list;
	}

	@Override
	public List<Chat> selectUnreadChat(String eNo, String roomNo) {
		return cd.selectUnreadChat(sqlSession, eNo, roomNo);
	}

	@Override
	public void updateRead(String chatList, String eNo) {
		List<String> list = new ArrayList<>();
		list = Arrays.asList(chatList.split(", "));
		
		for(String s : list) {
			cd.updateRead(sqlSession, Integer.parseInt(eNo), Integer.parseInt(s));
		}
	}

	@Override
	public String selectSender(String eNo) {
		return cd.selectSender(sqlSession, eNo);
	}

}
