package com.hardcore.cw.chat.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Chat implements java.io.Serializable{
   private int chatNo;
   private String chatDate;
   private String chatCnt;
   private int roomNo;
   private int empDivNo;
}