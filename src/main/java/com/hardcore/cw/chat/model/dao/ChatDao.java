package com.hardcore.cw.chat.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.chat.model.vo.Chat;
import com.hardcore.cw.chat.model.vo.ChatAndEmp;
import com.hardcore.cw.chat.model.vo.ChatAndRoom;
import com.hardcore.cw.chat.model.vo.ChatRoom;

public interface ChatDao {

	int insertChatRoom(SqlSessionTemplate sqlSession, ChatRoom cr);
 
	int selectChatRoomSeq(SqlSessionTemplate sqlSession);

	List<ChatAndRoom> selectChatRoomList(SqlSessionTemplate sqlSession, int eNo);

	int insertChat(SqlSessionTemplate sqlSession, Chat c);

	List<Integer> selectRoomNoList(SqlSessionTemplate sqlSession, List<String> empNo);

	int selectRoomUserCount(SqlSessionTemplate sqlSession, Integer i);

	List<ChatAndEmp> selectChatList(SqlSessionTemplate sqlSession, int roomNo);

	ChatAndEmp selectChatting(SqlSessionTemplate sqlSession, Chat c);

	String selectEmployee(SqlSessionTemplate sqlSession, int eNo);

	int selectUnreadCount(SqlSessionTemplate sqlSession, int no, int roomNo);

	List<Chat> selectUnreadChat(SqlSessionTemplate sqlSession, String eNo, String roomNo);

	void updateRead(SqlSessionTemplate sqlSession, int eNo, int chatNo);

	String selectSender(SqlSessionTemplate sqlSession, String eNo);

}
