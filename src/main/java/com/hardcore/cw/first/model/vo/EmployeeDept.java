package com.hardcore.cw.first.model.vo;

import java.sql.Date;

import com.hardcore.cw.cms.model.vo.Company;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EmployeeDept {
   private int empDivNo;
   private int empNo;
   private String empId;
   private String empName;
   private String deptName;
   private String cpm;
   private String psm;
   private String epm;
   private String clm;
   private String tm;
}