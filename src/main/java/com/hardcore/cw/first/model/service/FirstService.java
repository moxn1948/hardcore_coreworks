package com.hardcore.cw.first.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.first.model.vo.FirstProcess;

public interface FirstService {

	int insertCompany(Company c);


	int insertAttachment(Attachment at);


	int insertDeptList(HashMap<String, Object> cntMap);


	ArrayList<HashMap<String, Object>> selectDeptStruct();


	List<Object> selectJobList();


	List<Object> selectPostList();


	int updateJob(Job j);


	int deleteJob(Job j);


	int updateJobOrder(Job j);


	int updatePos(Position p);


	int deletePos(Position p);


	int updatePosOrder(Position p);


	int insertEmployee(Employee e);


	void insertCommonGroup(Employee e);


	int insertProfileImg(Attachment a);


	ArrayList<HashMap<String, Object>> selectForm();


	List<Object> selectOperAuth(PageInfo pi);




	int updateEmpAuth(Authority ah);


	int updateFirstProcess(FirstProcess fp);


	HashMap<String, Object> selectFirstYn();


	int insertOperAuth(Authority ah);


	Company selectDomain();

}
