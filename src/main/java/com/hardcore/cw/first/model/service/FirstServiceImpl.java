package com.hardcore.cw.first.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;
import com.hardcore.cw.first.model.dao.FirstDao;
import com.hardcore.cw.first.model.vo.FirstProcess;
@Service
public class FirstServiceImpl implements FirstService{
	@Autowired SqlSessionTemplate sqlSession;
	
	@Autowired
	private FirstDao fd;
	
	@Autowired DataSourceTransactionManager transactionManager;
	
	@Override
	public int insertCompany(Company c) {
		
		return fd.insertCompnay(sqlSession, c);
	}

	@Override
	public int insertAttachment(Attachment at) {
		
		return fd.insertAttachment(sqlSession, at);
	}

	@Override
	public int insertDeptList(HashMap<String, Object> cntMap) {
		int result1 = 0;
		int result2 = 0;
		int result3 = 0;
		int result4 = 0;
		
		
		
		
		//상위부서 인서트
		if(cntMap.get("deptNo").equals("0") && cntMap.get("DeptOrder").equals("-1")) {
			result1 = fd.insertDeptList(sqlSession, cntMap);
			fd.insertDeptListCommon(sqlSession, cntMap);
		}
		
		//order없는 하위부서1 인서트
		if(cntMap.get("DeptOrder").equals("-2") && cntMap.get("deptNo").equals("0") && !cntMap.get("DeptOrder").equals("-1") && !cntMap.get("DeptOrder").equals("-3")) {
			result2 = fd.insertDeptList2(sqlSession, cntMap);
			fd.insertDeptListCommon2(sqlSession, cntMap);
		}
		
		//order없는 하위부서2 인서트
		if(cntMap.get("DeptOrder").equals("-3") && cntMap.get("deptNo").equals("0") && !cntMap.get("DeptOrder").equals("-1") && !cntMap.get("DeptOrder").equals("-2")) {
			result3 = fd.insertDeptList4(sqlSession, cntMap);
			fd.insertDeptListCommon4(sqlSession, cntMap);
		}
		
		//order있는 하위부서 인서트
		if(!cntMap.get("DeptOrder").equals("-2") && cntMap.get("deptNo").equals("0") && !cntMap.get("DeptOrder").equals("-1") && !cntMap.get("DeptOrder").equals("-3")) {
			result4 = fd.insertDeptList3(sqlSession, cntMap);
			fd.insertDeptListCommon3(sqlSession, cntMap);
		}
		
		return 0;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectDeptStruct() {
List<Dept> deptList = fd.selectDeptStruct(sqlSession);
		
		ArrayList<HashMap<String, Object>> deptListEdit = new ArrayList<>();
		
		for(int i =0; i < deptList.size(); i++) {
			
			if(deptList.get(i).getDeptOrder() == 0) {
				HashMap<String, Object> cntMap = new HashMap<>();
				ArrayList<HashMap<String, Object>> deptOrderList = new ArrayList<>();
				
				cntMap.put("deptNo", deptList.get(i).getDeptNo());
				cntMap.put("deptName", deptList.get(i).getDeptName());
				cntMap.put("deptOrderList", deptOrderList);
				
				deptListEdit.add(cntMap);
			}
		}
		
		for(int i = 0; i < deptListEdit.size(); i++) {
			for(int j = 0; j < deptList.size(); j++) {
				if(deptList.get(j).getDeptOrder() == (Integer) deptListEdit.get(i).get("deptNo")) {
					HashMap<String, Object> cntMap = new HashMap<>();
					ArrayList<HashMap<String, Object>> deptOrderList = new ArrayList<>();
					
					cntMap.put("deptNo", deptList.get(j).getDeptNo());
					cntMap.put("deptName", deptList.get(j).getDeptName());
					cntMap.put("deptOrder", deptList.get(j).getDeptOrder());
					cntMap.put("deptOrderList", deptOrderList);
					
					((ArrayList<HashMap<String, Object>>) deptListEdit.get(i).get("deptOrderList")).add(cntMap);
				}
			}
		}
		
		for(int i = 0; i <deptListEdit.size(); i++) {
			ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) deptListEdit.get(i).get("deptOrderList");
			
			for(int j = 0; j < list.size(); j++) {
				
				for(int k = 0; k < deptList.size(); k++) {
					
					if(deptList.get(k).getDeptOrder() == (Integer) list.get(j).get("deptNo")) {
						HashMap<String, Object> cntMap = new HashMap<>();
						
						cntMap.put("deptNo", deptList.get(k).getDeptNo());
						cntMap.put("deptName", deptList.get(k).getDeptName());
						cntMap.put("deptOrder", deptList.get(k).getDeptOrder());
						
						
						((ArrayList<HashMap<String, Object>>) list.get(j).get("deptOrderList")).add(cntMap);
					}
				}
			}
		}
		
		return deptListEdit;
	}

	@Override
	public List<Object> selectJobList() {
		List<Object> list = null;
		
		list = fd.selectJobList(sqlSession);
		
		return list;
	}

		@Override
		public List<Object> selectPostList() {
			List<Object> list = null;
			
			list = fd.selectPosList(sqlSession);
			return list;
		}

		@Override
		public int deleteJob(Job j) {
			
			return fd.deleteJob(sqlSession, j);
		}
		
		@Override
		public int updateJobOrder(Job j) {
			
			return fd.updateJobOrder(sqlSession, j);
		}
		
		@Override
		public int updateJob(Job j) {
			
			return fd.updateJob(sqlSession, j);
		}

		@Override
		public int deletePos(Position p) {
			
			return  fd.deletePos(sqlSession, p);
		}


		@Override
		public int updatePosOrder(Position p) {
			
			return fd.updatePosOrder(sqlSession, p);
		}



		@Override
		public int updatePos(Position p) {
			
			return fd.updatePos(sqlSession, p);
		}
		
		//사원 한명 추가
		@Override
		public int insertEmployee(Employee e) {
			return fd.insertEmployee(sqlSession, e);
		}
		
		//사원 공통 그룹에 추가
		@Override
		public void insertCommonGroup(Employee e) {
			fd.insertCommonGroup(sqlSession, e);
		}
		
		//사원 프로필 사진 추가
		@Override
		public int insertProfileImg(Attachment a) {
			return fd.insertProfileImg(sqlSession, a);
		}
		@Override
		public ArrayList<HashMap<String, Object>> selectForm() {
			List<DocPath> docList = fd.selectForm(sqlSession);
			
			ArrayList<HashMap<String, Object>> docListEdit = new ArrayList<>();
			
			for (int i = 0; i < docList.size(); i++) {
				if(docList.get(i).getPathOrder() == 0) {
					HashMap<String, Object> cntMap = new HashMap<>();
					ArrayList<HashMap<String, Object>> docOrderList = new ArrayList<>();
					
					cntMap.put("pathNo", docList.get(i).getPathNo());
					cntMap.put("pathName", docList.get(i).getPathName());
					cntMap.put("docOrderList", docOrderList);
					
					docListEdit.add(cntMap);
				}
				
			}
			
			for (int i = 0; i < docListEdit.size(); i++) {
				ArrayList<HashMap<String, Object>> list = (ArrayList<HashMap<String, Object>>) docListEdit.get(i).get("docOrderList");
				
				for(int j = 0; j < docList.size(); j++) {
					if(docList.get(j).getPathOrder() == (Integer) docListEdit.get(i).get("pathNo")) {
						HashMap<String, Object> cntMap = new HashMap<>();
						
						cntMap.put("pathNo", docList.get(j).getPathNo());
						cntMap.put("pathName", docList.get(j).getPathName());
						cntMap.put("pathOrder", docList.get(j).getPathOrder());
						cntMap.put("status", docList.get(j).getStatus());
						
						((ArrayList<HashMap<String, Object>>) docListEdit.get(i).get("docOrderList")).add(cntMap);
					}
				}
			}
			
			return docListEdit;
		}

		@Override
		public List<Object> selectOperAuth(PageInfo pi) {
			List<Object> list = null;
			
			list = fd.selectOperAuth(sqlSession, pi);
			return list;
		}

		@Override
		public int insertOperAuth(Authority ah) {
			return fd.insertOperAuth(sqlSession, ah);
		}

		@Override
		public int updateEmpAuth(Authority ah) {
			return fd.updateEmpAuth(sqlSession, ah);
		}

		@Override
		public int updateFirstProcess(FirstProcess fp) {
			return fd.updateFirstProcess(sqlSession, fp);
		}

		@Override
		public HashMap<String, Object> selectFirstYn() {
			
			return fd.selectFirstYn(sqlSession);
		}

		@Override
		public Company selectDomain() {
			return fd.selectDomain(sqlSession);
		}



}
