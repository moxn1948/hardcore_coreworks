package com.hardcore.cw.first.model.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;
import com.hardcore.cw.first.model.vo.FirstProcess;

@Repository
public class FirstDaoImpl implements FirstDao{

	@Override
	public int insertCompnay(SqlSessionTemplate sqlSession, Company c) {
		
		return sqlSession.insert("First.insertCompany", c);
		
	}

	@Override
	public int insertAttachment(SqlSessionTemplate sqlSession, Attachment at) {
		
		return sqlSession.insert("First.insertAttachment", at);
	}

	@Override
	public int insertDeptList(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		return sqlSession.insert("First.insertDeptList", cntMap);
	}

	@Override
	public int insertDeptList2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		
		return sqlSession.insert("First.insertDeptList2", cntMap);
	}

	@Override
	public int insertDeptList3(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		return sqlSession.insert("First.insertDeptList3", cntMap);
	}

	@Override
	public int insertDeptList4(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		
		return sqlSession.insert("First.insertDeptList4", cntMap);
	}

	@Override
	public List<Dept> selectDeptStruct(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("First.selectDeptStruct");
	}

	@Override
	public List<Object> selectJobList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("First.selectJobList");
	}

	@Override
	public List<Object> selectPosList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("First.selectPosList");
	}


	//직급 삭제
	@Override
	public int deleteJob(SqlSessionTemplate sqlSession, Job j) {
		
		return sqlSession.update("First.deleteJob", j);
	}
	//직급 순서 변경
	@Override
	public int updateJobOrder(SqlSessionTemplate sqlSession, Job j) {
		
		return sqlSession.update("First.updateJobOrder",j);
	}
	
	@Override
	public int updateJob(SqlSessionTemplate sqlSession, Job j) {
		return sqlSession.update("First.updateJob", j);
	}
	
	@Override
	public int deletePos(SqlSessionTemplate sqlSession, Position p) {
		
		return sqlSession.update("First.deletePos", p);
	}

	@Override
	public int updatePosOrder(SqlSessionTemplate sqlSession, Position p) {
		
		return sqlSession.update("First.updatePosOrder", p);
	}

	@Override
	public int updatePos(SqlSessionTemplate sqlSession, Position p) {
		return sqlSession.update("First.updatePos", p);
	}
	//사원 한명추가
	@Override
	public int insertEmployee(SqlSessionTemplate sqlSession, Employee e) {
		return sqlSession.insert("First.insertEmployee", e);
	}
	//사원 공통그룹에 추가
	@Override
	public void insertCommonGroup(SqlSessionTemplate sqlSession, Employee e) {
		sqlSession.insert("Ems.insertCommonGroup", e.getEmpDivNo());
	}
	//사원 프로필사진 추가
	@Override
	public int insertProfileImg(SqlSessionTemplate sqlSession, Attachment a) {
		return sqlSession.insert("Ems.insertProfileImg", a);
	}
	@Override
	public List<DocPath> selectForm(SqlSessionTemplate sqlSession) {
		
		return sqlSession.selectList("Cms.selectForm");
	}

	@Override
	public List<Object> selectOperAuth(SqlSessionTemplate sqlSession, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return sqlSession.selectList("First.selectOperAuth", null, rowBounds);
	}

	@Override
	public int updateEmpAuth(SqlSessionTemplate sqlSession, Authority ah) {
		return sqlSession.update("First.updateEmpAuth", ah);
	}

	@Override
	public int insertOperAuth(SqlSessionTemplate sqlSession, Authority ah) {
		return sqlSession.insert("First.insertOperAuth", ah);
	}

	@Override
	public int updateFirstProcess(SqlSessionTemplate sqlSession, FirstProcess  fp) {
		return sqlSession.update("First.updateFirstProcess", fp);
	}

	@Override
	public HashMap<String, Object> selectFirstYn(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("First.selectFirstYn");
	}

	@Override
	public int insertDeptListCommon(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		// TODO Auto-generated method stub
		return sqlSession.insert("First.insertDeptListCommon", cntMap);
	}

	@Override
	public int insertDeptListCommon2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		// TODO Auto-generated method stub
		return sqlSession.insert("First.insertDeptListCommon4", cntMap);
	}

	@Override
	public int insertDeptListCommon4(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		// TODO Auto-generated method stub
		return sqlSession.insert("First.insertDeptListCommon3", cntMap);
	}

	@Override
	public int insertDeptListCommon3(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap) {
		// TODO Auto-generated method stub
		return sqlSession.insert("First.insertDeptListCommon2", cntMap);
	}

	@Override
	public Company selectDomain(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("First.selectDomain");
	}



}
