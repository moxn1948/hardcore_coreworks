package com.hardcore.cw.first.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;
import com.hardcore.cw.first.model.vo.FirstProcess;

public interface FirstDao {

	int insertCompnay(SqlSessionTemplate sqlSession, Company c);



	int insertAttachment(SqlSessionTemplate sqlSession, Attachment at);



	int insertDeptList(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);



	int insertDeptList2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);



	int insertDeptList4(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);



	int insertDeptList3(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);



	List<Dept> selectDeptStruct(SqlSessionTemplate sqlSession);



	List<Object> selectJobList(SqlSessionTemplate sqlSession);



	List<Object> selectPosList(SqlSessionTemplate sqlSession);



	int deleteJob(SqlSessionTemplate sqlSession, Job j);



	int updateJobOrder(SqlSessionTemplate sqlSession, Job j);



	int updateJob(SqlSessionTemplate sqlSession, Job j);



	int deletePos(SqlSessionTemplate sqlSession, Position p);



	int updatePosOrder(SqlSessionTemplate sqlSession, Position p);



	int updatePos(SqlSessionTemplate sqlSession, Position p);



	int insertEmployee(SqlSessionTemplate sqlSession, Employee e);



	void insertCommonGroup(SqlSessionTemplate sqlSession, Employee e);



	int insertProfileImg(SqlSessionTemplate sqlSession, Attachment a);



	List<DocPath> selectForm(SqlSessionTemplate sqlSession);



	List<Object> selectOperAuth(SqlSessionTemplate sqlSession, PageInfo pi);



	int updateEmpAuth(SqlSessionTemplate sqlSession, Authority ah);



	int insertOperAuth(SqlSessionTemplate sqlSession, Authority ah);

	int updateFirstProcess(SqlSessionTemplate sqlSession, FirstProcess fp);



	HashMap<String, Object> selectFirstYn(SqlSessionTemplate sqlSession);



	int insertDeptListCommon(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);



	int insertDeptListCommon2(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);



	int insertDeptListCommon4(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);



	int insertDeptListCommon3(SqlSessionTemplate sqlSession, HashMap<String, Object> cntMap);



	Company selectDomain(SqlSessionTemplate sqlSession);

}
