package com.hardcore.cw.first.controller;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.cms.controller.FormController;
import com.hardcore.cw.cms.model.service.CmsService;
import com.hardcore.cw.cms.model.service.FormService;
import com.hardcore.cw.cms.model.vo.Authority;
import com.hardcore.cw.cms.model.vo.Company;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.cms.model.vo.PriFormat;
import com.hardcore.cw.common.CommonsUtils;
import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.DocPath;
import com.hardcore.cw.ems.model.service.InsertEmpService;
import com.hardcore.cw.ems.model.service.SelectEmpService;
import com.hardcore.cw.first.model.service.FirstService;
import com.hardcore.cw.first.model.vo.FirstProcess;

@Controller
@SessionAttributes("loginUser")
public class FirstController {
	private static final Logger logger = LoggerFactory.getLogger(FormController.class);
@Autowired
private FirstService fs;

@Autowired
private BCryptPasswordEncoder passwordEncoder;

@Autowired
private SelectEmpService ses;

@Autowired
private AddrMenuService ams;

@Autowired
private InsertEmpService ies;

@Autowired
private CmsService cs;

@Autowired
private FormService forms;

FirstProcess fp = new FirstProcess();

	@RequestMapping("selectMain.fr")
	public ModelAndView selectMain(HttpServletRequest request, ModelAndView mv) {
		LoginEmp e = (LoginEmp) request.getSession().getAttribute("loginUser");
		int empDivNo = e.getEmpDivNo(); 
		String empAuth = e.getAuth();
		int deptNo = e.getDeptNo(); 
		
		int result = fs.updateFirstProcess(fp);
		
		if(result > 0) {
			mv.addObject("eno", empDivNo);
			mv.addObject("dno", deptNo);
			mv.addObject("auth", empAuth);
			mv.setViewName("redirect:main.main");
			
		}
		return mv;
	}
	
	@RequestMapping("selectFirstInfo.fr")
	public String selectFirstInfo() {
		
		return "company_info";
	}
	
	@PostMapping("insertCompany.fr")
	public ModelAndView insertCompany(ModelAndView mv, Model model, Company c,
			@RequestParam(name="attachments") MultipartFile attachments, 
			@RequestParam(name="attachments2") MultipartFile attachments2, 
			HttpServletRequest request) {
		
		//회사 정보 입력
		int result = fs.insertCompany(c);
				
		Attachment at = new Attachment();
		Attachment at2 = new Attachment();
		
		String root = request.getSession().getServletContext().getRealPath("resources");
		
		String filePath = root + "\\uploadFiles";
		String originFileName1 = attachments.getOriginalFilename();
		
		String ext1 = originFileName1.substring(originFileName1.lastIndexOf("."));
		
		String changeName = CommonsUtils.getRandomString();
		
		at.setOriginName(originFileName1);
		at.setChangeName(changeName + ext1);
		at.setFilePath(filePath);
		at.setCompanyNo(1);
		at.setSignYn("N");
		
		try {
			attachments.transferTo(new File(filePath + "\\" + changeName + ext1));
			
			
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
		
		if(c.getSignYn().equals("Y")) {
			String originFileName2 = attachments2.getOriginalFilename();
			String ext2 = originFileName2.substring(originFileName2.lastIndexOf("."));
			at2.setOriginName(originFileName2);
			at2.setChangeName(changeName + ext2);
			at2.setFilePath(filePath);
			at2.setCompanyNo(1);
			at2.setSignYn("Y");
			
			try {
				attachments2.transferTo(new File(filePath + "\\" + changeName + ext2));
				
				int result3 = fs.insertAttachment(at2);
				
			} catch (IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		int result2 = fs.insertAttachment(at);
		int result3 = fs.updateFirstProcess(fp);
		
		
		if(result > 0) {
			mv.setViewName("redirect:selectDeptStruct.fr");
		}
		
		return mv;
	}
	
	@RequestMapping("selectEmpList.fr")
	public ModelAndView selectEmpList(ModelAndView mv, HttpServletRequest request) {
		List<Dept> dList = ses.selectDeptList();

		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();
		Company domain = fs.selectDomain();
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		int listCount = ses.getListCount();
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		List<LoginEmp> eList = ses.selectAllEmpList(pi);
		
		mv.addObject("domain",domain);
		mv.addObject("pi", pi);
		mv.addObject("dList", dList);
		mv.addObject("eList", eList);
		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		mv.setViewName("emp_list");
		return mv;
	}
	
	@RequestMapping("selectDeptStruct.fr")
	public ModelAndView selectListDept(ModelAndView mv) {
		ArrayList<HashMap<String, Object>> deptList = fs.selectDeptStruct();
		
		if(deptList != null) {
			mv.addObject("deptList", deptList);
			mv.setViewName("dept_struct");
		}else {
			mv.addObject("msg", "조회 실패");
			mv.addObject("view", "index.jsp");
		}
		return mv;
	}
	

	
	@RequestMapping("selectListJob.fr")
	public ModelAndView selectJob(ModelAndView mv, Job j) {
		
		List<Object> list = fs.selectJobList();
		
		
		if(list != null) {
			mv.addObject("list", list);
			mv.setViewName("job_struct");
		}
		
		return mv;
	}
	
	@RequestMapping("selectListPos.fr")
	public ModelAndView selectPos(ModelAndView mv, Position p) {
		
		List<Object> list = fs.selectPostList();
		
		mv.addObject("list", list);
		mv.setViewName("pos_struct");
		
		return mv;
	}
	
	@PostMapping("insertDeptList.fr")
	public ModelAndView updateDeptList(HttpServletRequest request, ModelAndView mv) {
		//상위부서 이름
		String DeptNameSubmit = request.getParameter("DeptNameSubmit");
		String topDeptName[] = request.getParameterValues("topDeptName");
		String DeptOrder[] = request.getParameterValues("DeptOrder");
		//
		
		//하위부서1 이름
		String subDeptName[] = request.getParameterValues("subDeptName");
		//
		
		//하위부서1 부서순서
		//
		
		//하위부서2 이름
		String sub2DeptName[] = request.getParameterValues("sub2DeptName");
		
		//
				
		//하위부서2 부서순서
		
		//
		
		//상위 하위1 하위2 부서번호
		String deptNoList[] = request.getParameterValues("deptNoList");
		//
		
		for(int i =0; i < topDeptName.length; i++) {
			HashMap<String, Object> cntMap = new HashMap<>();
				if(!topDeptName[i].equals("-1")) {
				cntMap.put("deptNo", deptNoList[i]);
				cntMap.put("deptName", topDeptName[i]);
				cntMap.put("DeptOrder", DeptOrder[i]);
				}			
				
				if(!subDeptName[i].equals("-1") && !subDeptName[i].equals("")) {
					cntMap.put("deptNo", deptNoList[i]);
					cntMap.put("deptName", subDeptName[i]);
					cntMap.put("DeptOrder",  DeptOrder[i]);
				}
				
				if(!sub2DeptName[i].equals("-2") && !sub2DeptName[i].equals("")) {
					cntMap.put("deptNo", deptNoList[i]);
					cntMap.put("deptName", sub2DeptName[i]);
					cntMap.put("DeptOrder", DeptOrder[i]);
				}
				fs.insertDeptList(cntMap);
		}
		
		
		
 		
		
		Dept d = new Dept();
		
		int result3 = fs.updateFirstProcess(fp);
		mv.setViewName("redirect:selectDeptStruct.fr");
		
		
		return mv;
		
	}
	@PostMapping("updateJob.fr")
	public ModelAndView updateJob(HttpServletRequest request, ModelAndView mv) {
		String jobName[] = request.getParameterValues("jobName");
		String jobOrder[] = request.getParameterValues("jobOrder");
	


		List<Job> jobList = new ArrayList<>();
		
				
			for(int i = 0; i < jobName.length; i++) {	
				Job j = new Job();
				
				j.setJobName(jobName[i]);		
				j.setJobOrder(Integer.parseInt(jobOrder[i]));
				
				fs.updateJob(j);	
			}						
		
		int result3 = fs.updateFirstProcess(fp);
		mv.setViewName("redirect:selectListJob.fr");

	
	return mv;
}
	@GetMapping("selectListformStruct.fr")
	public ModelAndView selectForm(ModelAndView mv) {
		int result3 = fs.updateFirstProcess(fp);
		ArrayList<HashMap<String,Object>> docList = fs.selectForm();
		
		if(docList != null) {
			mv.addObject("docList", docList);
			mv.setViewName("form_struct");
		}else {
			mv.addObject("msg", "로드 실패!");
			mv.addObject("view", "index.jsp");
		}
		
		return mv;
		
	}
	@PostMapping("updatePos.fr")
	public ModelAndView updatePos(HttpServletRequest request, ModelAndView mv) {
		String posOrder[] = request.getParameterValues("posOrder");
		String posName[] = request.getParameterValues("posName");
			for(int i = 0; i < posOrder.length; i++) {
				Position p = new Position();	
				p.setPosName(posName[i]);		
				p.setPosOrder(Integer.parseInt(posOrder[i]));
				
				
				fs.updatePos(p);
			}
		int result3 = fs.updateFirstProcess(fp);
		mv.setViewName("redirect:selectListPos.fr");
	
		return mv;	
	}
	//사원 한명 등록
	@PostMapping("insertOneEmp.fr")
	public ModelAndView insertEmployee(ModelAndView mv, Employee e, Model m, @RequestParam("photo") MultipartFile photo, HttpServletRequest request) {
		e.setEmpPwd(passwordEncoder.encode(e.getEmpPwd()));
		fs.insertEmployee(e);
		fs.insertCommonGroup(e);
		
		if(!photo.isEmpty()) {
			String root = request.getSession().getServletContext().getRealPath("resources");
			String filePath = root + "\\uploadFiles";
			String originFileName = photo.getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String changeName = CommonsUtils.getRandomString();
			
			Attachment a = new Attachment();
			a.setOriginName(originFileName);
			a.setChangeName(changeName + ext);
			a.setFilePath(filePath);
			
			try {
				photo.transferTo(new File(filePath + "\\" + changeName + ext));
				
				a.setEmpDivNo(e.getEmpDivNo());
				
				fs.insertProfileImg(a);
				
				mv.setViewName("redirect:selectEmpList.fr");
				return mv;
			} catch (Exception ex) {
				new File(filePath + "\\" + changeName + ext).delete();
				
				m.addAttribute("msg", "회원 가입 실패");
				
				mv.setViewName("error");
				
				return mv;
			}
		}
		mv.setViewName("redirect:selectEmpList.fr");
		return mv;
	}
	
	@RequestMapping("insertOneEmpForm.fr")
	public ModelAndView insertOneEmp(ModelAndView mv) {
		
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();
		
		List<Position> posList = ses.selectPosList();
		List<Job> jobList = ses.selectJobList();
		
		mv.addObject("posList",posList);
		mv.addObject("jobList", jobList);
		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		mv.setViewName("emp_reg");
		return mv;
	}
	
	//사원 일괄등록
	@PostMapping("insertCSV.fr")
	public ModelAndView insertEmpCSV(ModelAndView mv, HttpServletRequest request, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		String[] empNo = request.getParameterValues("empNo");
		String[] empName = request.getParameterValues("empName");
		String[] empId = request.getParameterValues("empId");
		String[] empPwd = request.getParameterValues("empPwd");
		String[] enrollDate = request.getParameterValues("enrollDate");
		String[] deptName = request.getParameterValues("deptName");
		String[] posName = request.getParameterValues("posName");
		String[] jobName = request.getParameterValues("jobName");
		
		List<HashMap<String, Object>> deptNoList = ies.selectDeptNoFromDeptName(deptName);
		List<HashMap<String, Object>> posNoList = ies.selectPosNoFromPosName(posName);
		List<HashMap<String, Object>> jobNoList = ies.selectJobNoFromJobName(jobName);
		
		List<Employee> empList = new ArrayList<>();
		for(int i = 0; i < empName.length; i++) {
			Employee e = new Employee();
			try {
				e.setEmpNo(Integer.parseInt(empNo[i]));
				e.setEmpName(empName[i]);
				e.setEmpId(empId[i]);
				e.setEmpPwd(passwordEncoder.encode(empPwd[i]));
				e.setEnrollDate(java.sql.Date.valueOf(enrollDate[i]));
				
				for(int j = 0; j < deptNoList.size(); j++) {
					if(deptNoList.get(j).get("DEPT_NAME").equals(deptName[i])) {
						BigDecimal bd = (BigDecimal) deptNoList.get(j).get("DEPT_NO");
						e.setDeptNo(bd.intValue());
						break;
					}
				}
				
				for(int k = 0; k < posNoList.size(); k++) {
					if(posNoList.get(k).get("POS_NAME").equals(posName[i])) {
						BigDecimal bd = (BigDecimal) posNoList.get(k).get("POS_NO");
						e.setPosNo(bd.intValue());
						break;
					}
				}
				
				for(int l = 0; l < jobNoList.size(); l++) {
					if(jobNoList.get(l).get("JOB_NAME").equals(jobName[i])) {
						BigDecimal bd = (BigDecimal) jobNoList.get(l).get("JOB_NO");
						e.setJobNo(bd.intValue());
						break;
					}
				}
				
				if(e.getDeptNo() != 0 && e.getPosNo() != 0 && e.getJobNo() != 0) {
					empList.add(e);
				}
				
			} catch (Exception ex) {
				continue;
			}
		}
		
		int result = ies.insertEmployeeList(empList);
		
		List<Integer> nullList = ies.selectAddrGroupIsNull();
		for(int i = 0; i < nullList.size(); i++) {
			Employee e = new Employee();
			e.setEmpDivNo(nullList.get(i));
			ies.insertCommonGroup(e);
		}
		
		mv.addObject("msg", result+"명 입력 성공");
		mv.setViewName("emp_insert_result");
		return mv;
	}
	
	@RequestMapping("insertAllEmpForm.fr")
	public String insertAllEmp() {
		return "emp_all_reg";
	}
	
	@RequestMapping("selectOperAuth.fr")
	public ModelAndView selectOperAuth(HttpServletRequest request,  ModelAndView mv) {
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();

		List<HashMap<String, Object>> empList = ams.selectEmpList();
		Company domain = fs.selectDomain();
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		int listCount = cs.getListCount();
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		List<Object> list = cs.selectOperAuth(pi);
		
		mv.addObject("domain", domain);
		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		mv.addObject("pi", pi);
		mv.addObject("list", list); 
		mv.setViewName("oper_auth");
		return mv;
	}
	
	@PostMapping("updateForm.fr")
	public ModelAndView updateForm(HttpServletRequest request, DocPath d, ModelAndView mv ,String formNo[], String subformNo[], String subStatus[]) {
		String formName = request.getParameter("formName");
		String subName = request.getParameter("subName");
		String subOrder = request.getParameter("subOrder");
		String subdelNo = request.getParameter("subdelNo");
		String delformNo = request.getParameter("delformNo");
		String pathNoList[] = request.getParameterValues("pathNoList");
		String topDocPathName[] = request.getParameterValues("topDocPathName");
		String subDocPathName[] = request.getParameterValues("subDocPathName");
		String pathOrder[] = request.getParameterValues("pathOrder");
		
		for(int i = 0; i < topDocPathName.length; i++) {
			HashMap<String, Object> cntMap = new HashMap<>();
			if(!topDocPathName[i].equals("-1")) {
				cntMap.put("pathName", topDocPathName[i]);
				cntMap.put("pathNoList", pathNoList[i]);
				cntMap.put("pathOrder", pathOrder[i]);
			}
			if(!subDocPathName[i].equals("-1")) {
				cntMap.put("pathName", subDocPathName[i]);
				cntMap.put("pathNoList", pathNoList[i]);
				cntMap.put("pathOrder", pathOrder[i]);
			}
			cs.insertDocPathList(cntMap);
		}
		
		
//		for(int i = 0; i < formNameSplit.length; i++) {
//			d.setPathName(formNameSplit[i]);
//			
//			if(i < formNo.length) {
//				d.setPathNo(Integer.parseInt(formNo[i]));
//			}else {
//				d.setPathNo(0);
//			}
//			
//			//cs.updateFormList(d);
//		}
		
//		if(!subOrder.equals("") && !subName.equals("")) {
//			String subNameSplit[] = subName.split(", ");
//			String subOrderSplit[] = subOrder.split(", ");
//			
//			
//			
//		for(int i = 0; i < subOrderSplit.length; i++) {
//			d.setPathOrder(Integer.parseInt(subOrderSplit[i]));
//			d.setPathName(subNameSplit[i]);
//			d.setPathNo(Integer.parseInt(subformNo[i]));
//		
//			
//			//cs.updatesubFormList(d);
//		}
//		
//		}
//		
		if(subdelNo != null) {
			String subdelNoSplit[] = subdelNo.split(", ");
			for(int i = 0; i < subdelNoSplit.length; i++) {
				
				d.setPathNo(Integer.parseInt(subdelNoSplit[i]));
				
				cs.deleteSubPath(d);
				
			}
		}
		
//		if(delformNo != null) {
//			String delformNoSplit[] = delformNo.split(", ");
//			for(int i = 0; i < delformNoSplit.length; i++) {
//
//				d.setPathNo(Integer.parseInt(delformNoSplit[i]));
//				cs.deleteTopPath(d);
//			}
//			
//		}
		int result3 = fs.updateFirstProcess(fp);
		
		if(d != null) {
			mv.setViewName("redirect:selectFormList.fr");
		}else {
			mv.addObject("msg", "로드 실패!");
			mv.addObject("view", "index.jsp");
		}
		
		return mv;
		
	}
	
	@RequestMapping("selectFormList.fr")
	public String selectForm(Model model, Format f, HttpServletRequest request) {
		
		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		int listCount = forms.getFormatListCount();
		logger.info(String.valueOf(listCount));
		
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		
		List<Format> list = cs.selectFormList(pi);
		
		model.addAttribute("list", list);
		model.addAttribute("pi", pi);
		
		return "form_list";
	}
	
	@GetMapping("formNew.fr")
	public String showFormWriter(Model m) {
		List<PriFormat> priList = forms.selectPriFormatList();
		List<DocPath> docList = forms.selectDocPathList();
		
		m.addAttribute("docPathList", docList);
		m.addAttribute("priFormatList", priList);
		
		return "form_new";
	}
	
	@PostMapping("formInsert.fr")
	public String insertForm(Model m, HttpServletRequest request, Format f) {
		String form = request.getParameter("form");
		
		// 기본양식으로 선택했으면 기존의 작성내용은 없는채로 데이터베이스에 넣어지며
		if(form.equals("provide")) {
			f.setFormatCnt(null);
		} else {
			// 직접 입력으로 선택했으면 기본양식에서 선택한 항목은 없는걸로 함
			f.setPriFormatNo(0);
		}
		
		// PERIOD_YN -> 값이 있으면 N, 없으면 Y
		if(f.getPeriodYn() != null) {
			f.setPeriodYn("N");
		} else {
			f.setPeriodYn("Y");
		}
		
		logger.info(f.toString());
		
		forms.InsertFormat(f);
			
		return "redirect:selectFormList.fr";
	}

	@GetMapping("formatDetail.fr")
	public String detailForm(Format f, Model m) {
		logger.info(f.toString());
		
		if(f.getPriFormatNo() > 0) {
			PriFormat p = forms.selectPriFormatDetail(f.getPriFormatNo());
			
			logger.info(p.toString());
			
			m.addAttribute("p", p);
		}
		
		Format ff = forms.selectFormatDetail(f);
		
		logger.info(ff.toString());
		
		m.addAttribute("f", ff);
		
		return "form_det";
	}
	
	@PostMapping("showUpdateFormat.fr")
	public String showUpdateFormat(Format f, Model m) {
		logger.info(f.toString());
		
		Format ff = forms.selectFormatDetail(f);
		logger.info(ff.toString());
		
		List<PriFormat> priList = forms.selectPriFormatList();
		List<DocPath> docList = forms.selectDocPathList();
		
		m.addAttribute("docPathList", docList);
		m.addAttribute("priFormatList", priList);
		m.addAttribute("f", ff);
		
		return "form_up";
	}
	
	@PostMapping("updateFormat.fr")
	public String updateFormat(Format f, Model m, HttpServletRequest request) {
		logger.info(f.toString());
		
		String form = request.getParameter("form");
		
		// 기본양식으로 선택했으면 기존의 작성내용은 없는채로 데이터베이스에 넣어지며
		if(form.equals("provide")) {
			f.setFormatCnt(null);
		} else {
			// 직접 입력으로 선택했으면 기본양식에서 선택한 항목은 없는걸로 함
			f.setPriFormatNo(0);
		}
		
		if(f.getPeriodYn() != null) {
			f.setPeriodYn("N");
		} else {
			f.setPeriodYn("Y");
		}
		
		forms.updateFormat(f);
		
		return "redirect:selectListformStruct.fr";
	}
	
	@GetMapping("showDeleteFormat.fr")
	public String deleteFormat(Format f) {
		logger.info(f.toString());
		forms.deleteFormat(f);
		
		return "redirect:selectListformStruct.fr";
	}
	
	@PostMapping("updateOperAuth.fr")
	public ModelAndView updateOperAuth(HttpServletRequest request, ModelAndView mv, Authority ah, Employee e, int empDivNo[], String cpm[], String psm[], String epm[], String tm[], String clm[]) {
		int result = 0;
		int result2 = 0;
		String delNo = request.getParameter("delNo");
		
		if(delNo == null || delNo.equals("")) {
			for(int i = 0; i < cpm.length; i++) {
				ah.setCpm(cpm[i]);
				ah.setPsm(psm[i]);
				ah.setEpm(epm[i]);
				ah.setTm(tm[i]);
				ah.setClm(clm[i]);
				ah.setEmpDivNo(empDivNo[i]);
				
				result = cs.insertOperAuth(ah);
				
				result2 = fs.updateEmpAuth(ah);
				
				
			}
		
			
			}else {
			String delNoSplit[] = delNo.split(", ");
			
			for(int i = 0; i < delNoSplit.length; i++) {
				ah.setEmpDivNo(Integer.parseInt(delNoSplit[i]));
				cs.deleteOperAuth(ah);
				}
		
			}
		mv.setViewName("redirect:selectOperAuth.fr");
		
		return mv;
	}
}
