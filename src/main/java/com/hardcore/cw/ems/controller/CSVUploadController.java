package com.hardcore.cw.ems.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.ems.model.service.SelectEmpService;

@Controller
public class CSVUploadController {

	@Autowired
	private SelectEmpService ses;
	
	@PostMapping("uploadcsv.em")
	public ModelAndView uploadcsv(MultipartFile csvfile, ModelAndView mv, HttpServletResponse response) {
		List<ArrayList<String>> total = new ArrayList<>();
		try {
			InputStream is = csvfile.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is, "EUC-KR"));
			ArrayList<String> emp = null;
			String read = "";
			
			while((read = br.readLine()) != null) {
				emp = new ArrayList<>(Arrays.asList(read.split(",")));
				total.add(emp);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		List<String> deptList = ses.selectDeptNameList();
		List<String> jobList = ses.selectJobNameList();
		List<String> posList = ses.selectPosNameList();
		
		response.setCharacterEncoding("UTF-8");
		mv.addObject("list", total);
		mv.addObject("deptList",deptList);
		mv.addObject("jobList", jobList);
		mv.addObject("posList", posList);
		mv.setViewName("jsonView");
		
		return mv;
	}
}
