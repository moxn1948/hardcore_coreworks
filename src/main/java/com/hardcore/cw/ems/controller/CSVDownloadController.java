package com.hardcore.cw.ems.controller;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CSVDownloadController {

	@RequestMapping("fileDownload.em")
	public ModelAndView reDocumentDown(HttpServletRequest request) {
		String filePath = "C:/hardcore_coreworks/hardcore_coreworks/src/main/webapp/resources/files/";
		String fileName = "csv_example.csv";
		String fullPath = filePath + fileName;
		File downFile = new File(fullPath);
		
		Map<String, Object> map = new HashMap<>();
		
		map.put("downFile", downFile);
		map.put("originName", fileName);
		
		return new ModelAndView("downloadView", "downloadFile", map);
	}
}
