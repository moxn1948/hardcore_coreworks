package com.hardcore.cw.ems.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.ems.model.service.EntService;

@Controller
public class EntController {
	@Autowired
	private EntService es;
	
	@GetMapping("selectEntList.em")
	public ModelAndView selectEntList(ModelAndView mv, String srchType, String currentPage, String keyword) {
		int cp = 1;
		if(currentPage != null) {
			cp = Integer.parseInt(currentPage);
		}
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("srchType", srchType);
		map.put("keyword", keyword);

		int listCount = es.selectEntListCount(map);

		PageInfo pi = Pagination.getPageInfo(cp, listCount);
		
		List<HashMap<String, Object>> list = es.selectEntList(pi, map);

		mv.addObject("list", list);
		mv.addObject("pi", pi);
		
		mv.setViewName("emp_ret_list");
		return mv;
	}

	@GetMapping("selectEntOne.em")
	public ModelAndView selectEntOne(ModelAndView mv, String eno) {
		int empDivNo = Integer.parseInt(eno);
		
		HashMap<String, Object> info = es.selectEntOne(empDivNo);
		
		mv.addObject("info", info);
		
		mv.setViewName("emp_ret_det");
		return mv;
	}
	
	@GetMapping("updateRecover.em")
	public ModelAndView updateRecover(ModelAndView mv, String eno) {
		int empDivNo = Integer.parseInt(eno);
		
		int result = es.updateRecover(empDivNo);
		
		if(result > 0) {
			mv.addObject("msg", "사원을 복구했습니다.");
			mv.addObject("view", "selectEntList.em");
		}else {
			mv.addObject("msg", "사원을 복구에 실패했습니다.");
			mv.addObject("view", "selectEntList.em");
		}
		mv.setViewName("entAlert");
	
		return mv;
	}
	@GetMapping("updateReEnroll.em")
	public ModelAndView updateReEnroll(ModelAndView mv, String eno) {
		int empDivNo = Integer.parseInt(eno);
		
		int result = es.updateReEnroll(empDivNo);
		
		if(result > 0) {
			mv.addObject("msg", "사원을 재입사 처리했습니다.");
			mv.addObject("view", "selectEntList.em");
		}else {
			mv.addObject("msg", "사원을 재입사 처리에 실패했습니다.");
			mv.addObject("view", "selectEntList.em");
		}
		mv.setViewName("entAlert");
	
		return mv;
	}
	@GetMapping("updateDel.em")
	public ModelAndView updateDel(ModelAndView mv, String eno) {
		int empDivNo = Integer.parseInt(eno);
		
		int result = es.updateDel(empDivNo);
		
		if(result > 0) {
			mv.addObject("msg", "사원을 삭제했습니다.");
			mv.addObject("view", "selectEntList.em");
		}else {
			mv.addObject("msg", "사원을 삭제에 실패했습니다.");
			mv.addObject("view", "selectEntList.em");
		}
		mv.setViewName("entAlert");
	
		return mv;
	}
}
