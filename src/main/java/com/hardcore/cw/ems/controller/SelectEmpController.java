package com.hardcore.cw.ems.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.ems.model.service.SelectEmpService;

@Controller
public class SelectEmpController {
	
	@Autowired
	private SelectEmpService ses;
	
	@Autowired
	private AddrMenuService ams;
	
	@RequestMapping("selectEmpList.em")
	public ModelAndView selectEmpList(ModelAndView mv, HttpServletRequest request) {
		List<Dept> dList = ses.selectDeptList();

		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();

		int currentPage = 1;
		
		if(request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		
		int listCount = ses.getListCount();
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);
		List<LoginEmp> eList = ses.selectAllEmpList(pi);
		
		mv.addObject("pi", pi);
		mv.addObject("dList", dList);
		mv.addObject("eList", eList);
		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		mv.setViewName("emp_list");
		return mv;
	}
	
	@RequestMapping("insertOneEmp.em")
	public ModelAndView insertOneEmp(ModelAndView mv) {
		
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();
		
		List<Position> posList = ses.selectPosList();
		List<Job> jobList = ses.selectJobList();
		
		mv.addObject("posList",posList);
		mv.addObject("jobList", jobList);
		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		mv.setViewName("emp_reg");
		return mv;
	}
	
	@RequestMapping("insertAllEmp.em")
	public String insertAllEmp() {
		return "emp_all_reg";
	}
	 
	@RequestMapping("searchDeptEmpList.em")
	public ModelAndView searchDeptEmpList(ModelAndView mv, int deptNo, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		List<LoginEmp> list = ses.selectDeptEmpList(deptNo);
		
		mv.addObject("list", list);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("searchNameEmpList.em")
	public ModelAndView searchNameEmpList(ModelAndView mv, String searchValue, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		
		List<LoginEmp> list = ses.selectNameEmpList(searchValue);
		
		mv.addObject("list", list);
		mv.setViewName("jsonView");
		return mv;
	}
	
	@RequestMapping("selectEmpDetail.em")
	public ModelAndView selectEmpDetail(ModelAndView mv, int eNo) {
		LoginEmp e = ses.selectOneEmployee(eNo);
		
		mv.addObject("emp", e);
		mv.setViewName("emp_det");
		return mv;
	}
	
	@GetMapping("updateEntYn.em")
	public ModelAndView updateEntYn(ModelAndView mv, int eNo) {
		int result = ses.updateEntYn(eNo);
		
		if(result > 0) {
			mv.setViewName("redirect:selectEntList.em");
		}
		return mv;
	}
	
}
