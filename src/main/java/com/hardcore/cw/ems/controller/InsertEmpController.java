package com.hardcore.cw.ems.controller;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.common.CommonsUtils;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.ems.model.service.InsertEmpService;
 
@Controller
public class InsertEmpController {
	private static final Logger logger = LoggerFactory.getLogger(InsertEmpController.class);
	
	@Autowired
	private InsertEmpService ies;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@PostMapping("insertOne.em")
	public String insertEmployee(Employee e, Model m, @RequestParam("attachments") MultipartFile photo, HttpServletRequest request) {
		e.setEmpPwd(passwordEncoder.encode(e.getEmpPwd()));
		ies.insertEmployee(e);
		ies.insertCommonGroup(e);
		
		if(!photo.isEmpty()) {
			String root = request.getSession().getServletContext().getRealPath("resources");
			String filePath = root + "\\uploadFiles";
			String originFileName = photo.getOriginalFilename();
			String ext = originFileName.substring(originFileName.lastIndexOf("."));
			String changeName = CommonsUtils.getRandomString();
			
			Attachment a = new Attachment();
			a.setOriginName(originFileName);
			a.setChangeName(changeName + ext);
			a.setFilePath(filePath);
			
			try {
				photo.transferTo(new File(filePath + "\\" + changeName + ext));
				
				a.setEmpDivNo(e.getEmpDivNo());
				
				ies.insertProfileImg(a);
				
				return "redirect:selectEmpList.em";
			} catch (Exception ex) {
				new File(filePath + "\\" + changeName + ext).delete();
				
				m.addAttribute("msg", "회원 가입 실패");
				
				return "common/errorPage";
			}
			
		}
		return "redirect:selectEmpList.em";
	}
	
	@PostMapping("duplicateNo.em")
	public ModelAndView duplicateNo(String no, ModelAndView mv) {
		int empNo = Integer.parseInt(no);
		List<Integer> empNoList = ies.selectEmpNoList();
		
		mv.addObject("result", "neq");
		for(Integer i : empNoList) {
			if(i == empNo) {
				mv.addObject("result", "eq");
				break;
			} 	
		}
		
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("duplicateId.em")
	public ModelAndView duplicateId(String id, ModelAndView mv) {
		List<String> empIdList = ies.selectEmpIdList();
		
		mv.addObject("result", "neq");
		for(String s : empIdList) {
			if(s.equals(id)) {
				mv.addObject("result", "eq");
			}
		}
	
		mv.setViewName("jsonView");
		return mv;
	}
	
	@PostMapping("insertCSV.em")
	public ModelAndView insertEmpCSV(ModelAndView mv, HttpServletRequest request, HttpServletResponse response) {
		response.setCharacterEncoding("utf-8");
		String[] empNo = request.getParameterValues("empNo");
		String[] empName = request.getParameterValues("empName");
		String[] empId = request.getParameterValues("empId");
		String[] empPwd = request.getParameterValues("empPwd");
		String[] enrollDate = request.getParameterValues("enrollDate");
		String[] deptName = request.getParameterValues("deptName");
		String[] posName = request.getParameterValues("posName");
		String[] jobName = request.getParameterValues("jobName");
		
		List<HashMap<String, Object>> deptNoList = ies.selectDeptNoFromDeptName(deptName);
		List<HashMap<String, Object>> posNoList = ies.selectPosNoFromPosName(posName);
		List<HashMap<String, Object>> jobNoList = ies.selectJobNoFromJobName(jobName);
		
		List<Employee> empList = new ArrayList<>();
		for(int i = 0; i < empName.length; i++) {
			Employee e = new Employee();
			try {
				e.setEmpNo(Integer.parseInt(empNo[i]));
				e.setEmpName(empName[i]);
				e.setEmpId(empId[i]);
				e.setEmpPwd(passwordEncoder.encode(empPwd[i]));
				e.setEnrollDate(java.sql.Date.valueOf(enrollDate[i]));
				
				for(int j = 0; j < deptNoList.size(); j++) {
					if(deptNoList.get(j).get("DEPT_NAME").equals(deptName[i])) {
						BigDecimal bd = (BigDecimal) deptNoList.get(j).get("DEPT_NO");
						e.setDeptNo(bd.intValue());
						break;
					}
				}
				
				for(int k = 0; k < posNoList.size(); k++) {
					if(posNoList.get(k).get("POS_NAME").equals(posName[i])) {
						BigDecimal bd = (BigDecimal) posNoList.get(k).get("POS_NO");
						e.setPosNo(bd.intValue());
						break;
					}
				}
				
				for(int l = 0; l < jobNoList.size(); l++) {
					if(jobNoList.get(l).get("JOB_NAME").equals(jobName[i])) {
						BigDecimal bd = (BigDecimal) jobNoList.get(l).get("JOB_NO");
						e.setJobNo(bd.intValue());
						break;
					}
				}
				
				if(e.getDeptNo() != 0 && e.getPosNo() != 0 && e.getJobNo() != 0) {
					empList.add(e);
				}
				
			} catch (Exception ex) {
				continue;
			}
		}
		
		int result = ies.insertEmployeeList(empList);
		
		List<Integer> nullList = ies.selectAddrGroupIsNull();
		for(int i = 0; i < nullList.size(); i++) {
			Employee e = new Employee();
			e.setEmpDivNo(nullList.get(i));
			ies.insertCommonGroup(e);
		}
		
		mv.addObject("msg", result+"명 입력 성공");
		mv.setViewName("emp_insert_result");
		return mv;
	}
}
