package com.hardcore.cw.ems.model.service;

import java.util.HashMap;
import java.util.List;

import com.hardcore.cw.common.model.vo.PageInfo;

public interface EntService {

	List<HashMap<String, Object>> selectEntList(PageInfo pi, HashMap<String, Object> map);

	int selectEntListCount(HashMap<String, Object> map);

	HashMap<String, Object> selectEntOne(int empDivNo);

	int updateRecover(int empDivNo);

	int updateReEnroll(int empDivNo);

	int updateDel(int empDivNo);

}
