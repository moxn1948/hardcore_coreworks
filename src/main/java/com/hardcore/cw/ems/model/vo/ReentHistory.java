package com.hardcore.cw.ems.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ReentHistory implements java.io.Serializable{
	private Date entDate;
	private Date reentDate;
	private int empDivNo;
}
