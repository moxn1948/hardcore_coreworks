package com.hardcore.cw.ems.model.service;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.ems.model.dao.InsertEmpDao;
 
@Service
public class InsertEmpServiceImpl implements InsertEmpService {
	private static Logger logger = LoggerFactory.getLogger(InsertEmpServiceImpl.class);
	
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private InsertEmpDao ied;
	
	@Override
	public int insertEmployee(Employee e) {
		return ied.insertEmployee(sqlSession, e);
	}

	@Override
	public int insertProfileImg(Attachment a) {
		return ied.insertProfileImg(sqlSession, a);
	}

	@Override
	public List<Integer> selectEmpNoList() {
		return ied.selectEmpNoLiset(sqlSession);
	}

	@Override
	public List<String> selectEmpIdList() {
		return ied.selectEmpIdList(sqlSession);
	}

	@Override
	public List<HashMap<String, Object>> selectDeptNoFromDeptName(String[] deptName) {
		return ied.selectDeptNoFromDeptName(sqlSession, deptName);
	}

	@Override
	public List<HashMap<String, Object>> selectPosNoFromPosName(String[] posName) {
		return ied.selectPosNoFromPosName(sqlSession, posName);
	}

	@Override
	public List<HashMap<String, Object>> selectJobNoFromJobName(String[] jobName) {
		return ied.selectJobNoFromJobName(sqlSession, jobName);
	}

	@Override
	public int insertEmployeeList(List<Employee> empList) {
		return ied.insertEmployeeList(sqlSession, empList);
	}

	@Override
	public void insertCommonGroup(Employee e) {
		ied.insertCommonGroup(sqlSession, e);
	}

	@Override
	public List<Integer> selectAddrGroupIsNull() {
		return ied.selectAddrGroupIsNull(sqlSession);
	}

}
