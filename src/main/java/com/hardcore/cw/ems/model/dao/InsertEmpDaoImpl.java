package com.hardcore.cw.ems.model.dao;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;

@Repository
public class InsertEmpDaoImpl implements InsertEmpDao {
 
	@Override
	public int insertEmployee(SqlSessionTemplate sqlSession, Employee e) {
		return sqlSession.insert("Ems.insertEmployee", e);
	}

	@Override
	public int insertProfileImg(SqlSessionTemplate sqlSession, Attachment a) {
		return sqlSession.insert("Ems.insertProfileImg", a);
	}

	@Override
	public List<Integer> selectEmpNoLiset(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Ems.selectEmpNoList");
	}

	@Override
	public List<String> selectEmpIdList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Ems.selectEmpIdList");
	}

	@Override
	public List<HashMap<String, Object>> selectDeptNoFromDeptName(SqlSessionTemplate sqlSession, String[] deptName) {
		List<String> deptNameList = Arrays.asList(deptName);
		HashMap<String, Object> map = new HashMap<>();
		map.put("deptName", deptNameList);
		return sqlSession.selectList("Ems.selectDeptNoFromDeptName", map);
	}

	@Override
	public List<HashMap<String, Object>> selectPosNoFromPosName(SqlSessionTemplate sqlSession, String[] posName) {
		List<String> posNameList = Arrays.asList(posName);
		HashMap<String, Object> map = new HashMap<>();
		map.put("posName", posNameList);
		return sqlSession.selectList("Ems.selectPosNoFromPosName", map);
	}

	@Override
	public List<HashMap<String, Object>> selectJobNoFromJobName(SqlSessionTemplate sqlSession, String[] jobName) {
		List<String> jobNameList = Arrays.asList(jobName);
		HashMap<String, Object> map = new HashMap<>();
		map.put("jobName", jobNameList);
		return sqlSession.selectList("Ems.selectJobNoFromJobName", map);
	}

	@Override
	public int insertEmployeeList(SqlSessionTemplate sqlSession, List<Employee> empList) {
		Map<String, Object> insertMap = new HashMap<>();
		insertMap.put("list", empList);
		return sqlSession.insert("Ems.insertEmployeeList", insertMap);
	}

	@Override
	public void insertCommonGroup(SqlSessionTemplate sqlSession, Employee e) {
		sqlSession.insert("Ems.insertCommonGroup", e.getEmpDivNo());
	}

	@Override
	public List<Integer> selectAddrGroupIsNull(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Ems.selectAddrGroupIsNull");
	}

}
