package com.hardcore.cw.ems.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.common.model.vo.PageInfo;

public interface EntDao {

	List<HashMap<String, Object>> selectEntList(SqlSessionTemplate sqlSession, PageInfo pi, HashMap<String, Object> map);

	int selectEntListCount(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	HashMap<String, Object> selectEntOne(SqlSessionTemplate sqlSession, int empDivNo);

	int updateRecover(SqlSessionTemplate sqlSession, int empDivNo);

	int updateReEnroll(SqlSessionTemplate sqlSession, int empDivNo);
	
	int updateReEnrollHis(SqlSessionTemplate sqlSession, int empDivNo);

	int updateDel(SqlSessionTemplate sqlSession, int empDivNo);


}
