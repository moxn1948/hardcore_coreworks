package com.hardcore.cw.ems.model.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.PageInfo;

@Repository
public class EntDaoImpl implements EntDao{

	@Override
	public List<HashMap<String, Object>> selectEntList(SqlSessionTemplate sqlSession, PageInfo pi,
			HashMap<String, Object> map) {

		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return sqlSession.selectList("Ems2.selectEntList" , map, rowBounds);
	}

	@Override
	public int selectEntListCount(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.selectOne("Ems2.selectEntListCount", map);
	}

	@Override
	public HashMap<String, Object> selectEntOne(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.selectOne("Ems2.selectEntOne", empDivNo);
	}

	@Override
	public int updateRecover(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.update("Ems2.updateRecover", empDivNo);
	}

	@Override
	public int updateReEnroll(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.update("Ems2.updateReEnroll", empDivNo);
	}

	@Override
	public int updateReEnrollHis(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.update("Ems2.updateReEnrollHis", empDivNo);
	}

	@Override
	public int updateDel(SqlSessionTemplate sqlSession, int empDivNo) {

		return sqlSession.update("Ems2.updateDel", empDivNo);
	}

}
