package com.hardcore.cw.ems.model.dao;

import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;

@Repository
public class SelectEmpDaoImpl implements SelectEmpDao {

	@Override
	public List<LoginEmp> selectAllEmpList(SqlSessionTemplate sqlSession, PageInfo pi) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());
		return sqlSession.selectList("Ems.selectAllEmpList" , null, rowBounds);
	}

	@Override
	public List<String> selectDeptNameList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Ems.selectDeptNameList");
	}

	@Override
	public List<String> selectJobNameList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Ems.selectJobNameList");
	}

	@Override
	public List<String> selectPosNameLisst(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Ems.selectPosNameList");
	}
 
	@Override
	public List<Dept> selectDeptList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Ems.selectDeptList");
	}

	@Override
	public List<Position> selectPosList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Ems.selectPosList");
	}

	@Override
	public List<Job> selectJobList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Ems.selectJobList");
	}

	@Override
	public int getListCount(SqlSessionTemplate sqlSession) {
		return sqlSession.selectOne("Ems.getListCount");
	}

	@Override
	public List<LoginEmp> selectDeptEmpList(SqlSessionTemplate sqlSession, int deptNo) {
		return sqlSession.selectList("Ems.selectDeptEmpList",deptNo);
	}

	@Override
	public List<LoginEmp> selectNameEmpList(SqlSessionTemplate sqlSession, String searchValue) {
		return sqlSession.selectList("Ems.selectNameEmpList", searchValue);
	}

	@Override
	public LoginEmp selectOneEmployee(SqlSessionTemplate sqlSession, int eNo) {
		return sqlSession.selectOne("Ems.selectOneEmployee", eNo);
	}
	
	@Override
	public int updateEntYn(SqlSessionTemplate sqlSession, int eNo) {
		return sqlSession.update("Ems.updateEntYn", eNo);
	}

}
