package com.hardcore.cw.ems.model.service;

import java.util.List;

import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
 
public interface SelectEmpService {

	List<LoginEmp> selectAllEmpList(PageInfo pi);

	List<String> selectDeptNameList();

	List<String> selectJobNameList();

	List<String> selectPosNameList();

	List<Dept> selectDeptList();

	List<Position> selectPosList();

	List<Job> selectJobList();

	int getListCount();

	List<LoginEmp> selectDeptEmpList(int deptNo);

	List<LoginEmp> selectNameEmpList(String searchValue);

	LoginEmp selectOneEmployee(int eNo);
	
	int updateEntYn(int eNo);
}
