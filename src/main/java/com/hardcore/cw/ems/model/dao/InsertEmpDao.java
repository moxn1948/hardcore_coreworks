package com.hardcore.cw.ems.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;

public interface InsertEmpDao {
 
	int insertEmployee(SqlSessionTemplate sqlSession, Employee e);

	int insertProfileImg(SqlSessionTemplate sqlSession, Attachment a);

	List<Integer> selectEmpNoLiset(SqlSessionTemplate sqlSession);

	List<String> selectEmpIdList(SqlSessionTemplate sqlSession);

	List<HashMap<String, Object>> selectDeptNoFromDeptName(SqlSessionTemplate sqlSession, String[] deptName);

	List<HashMap<String, Object>> selectPosNoFromPosName(SqlSessionTemplate sqlSession, String[] posName);

	List<HashMap<String, Object>> selectJobNoFromJobName(SqlSessionTemplate sqlSession, String[] jobName);

	int insertEmployeeList(SqlSessionTemplate sqlSession, List<Employee> empList);

	void insertCommonGroup(SqlSessionTemplate sqlSession, Employee e);

	List<Integer> selectAddrGroupIsNull(SqlSessionTemplate sqlSession);

}
