package com.hardcore.cw.ems.model.dao;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;

public interface SelectEmpDao {
 
	List<LoginEmp> selectAllEmpList(SqlSessionTemplate sqlSession, PageInfo pi);

	List<String> selectDeptNameList(SqlSessionTemplate sqlSession);

	List<String> selectJobNameList(SqlSessionTemplate sqlSession);

	List<String> selectPosNameLisst(SqlSessionTemplate sqlSession);

	List<Dept> selectDeptList(SqlSessionTemplate sqlSession);

	List<Position> selectPosList(SqlSessionTemplate sqlSession);

	List<Job> selectJobList(SqlSessionTemplate sqlSession);

	int getListCount(SqlSessionTemplate sqlSession);

	List<LoginEmp> selectDeptEmpList(SqlSessionTemplate sqlSession, int deptNo);

	List<LoginEmp> selectNameEmpList(SqlSessionTemplate sqlSession, String searchValue);

	LoginEmp selectOneEmployee(SqlSessionTemplate sqlSession, int eNo);
	
	int updateEntYn(SqlSessionTemplate sqlSession, int eNo);
}
