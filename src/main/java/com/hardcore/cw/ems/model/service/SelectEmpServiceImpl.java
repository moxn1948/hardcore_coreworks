package com.hardcore.cw.ems.model.service;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.cms.model.vo.Dept;
import com.hardcore.cw.cms.model.vo.Job;
import com.hardcore.cw.cms.model.vo.Position;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.ems.model.dao.SelectEmpDao;

@Service
public class SelectEmpServiceImpl implements SelectEmpService {

	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private SelectEmpDao sed;
	
	@Override
	public List<LoginEmp> selectAllEmpList(PageInfo pi) {
		return sed.selectAllEmpList(sqlSession, pi);
	}

	@Override
	public List<String> selectDeptNameList() {
		return sed.selectDeptNameList(sqlSession);
	}

	@Override
	public List<String> selectJobNameList() {
		return sed.selectJobNameList(sqlSession);
	}

	@Override
	public List<String> selectPosNameList() {
		return sed.selectPosNameLisst(sqlSession);
	}
 
	@Override
	public List<Dept> selectDeptList() {
		return sed.selectDeptList(sqlSession);
	}

	@Override
	public List<Position> selectPosList() {
		return sed.selectPosList(sqlSession);
	}

	@Override
	public List<Job> selectJobList() {
		return sed.selectJobList(sqlSession);
	}

	@Override
	public int getListCount() {
		return sed.getListCount(sqlSession);
	}

	@Override
	public List<LoginEmp> selectDeptEmpList(int deptNo) {
		return sed.selectDeptEmpList(sqlSession, deptNo);
	}

	@Override
	public List<LoginEmp> selectNameEmpList(String searchValue) {
		return sed.selectNameEmpList(sqlSession, searchValue);
	}

	@Override
	public LoginEmp selectOneEmployee(int eNo) {
		return sed.selectOneEmployee(sqlSession, eNo);
	}
	
	@Override
	public int updateEntYn(int eNo) {
		return sed.updateEntYn(sqlSession, eNo);
	}

}
