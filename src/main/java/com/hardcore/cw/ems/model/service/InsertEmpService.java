package com.hardcore.cw.ems.model.service;

import java.util.HashMap;
import java.util.List;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
 
public interface InsertEmpService {

	int insertEmployee(Employee e);

	int insertProfileImg(Attachment a);

	List<Integer> selectEmpNoList();

	List<String> selectEmpIdList();

	List<HashMap<String, Object>> selectDeptNoFromDeptName(String[] deptName);

	List<HashMap<String, Object>> selectPosNoFromPosName(String[] posName);

	List<HashMap<String, Object>> selectJobNoFromJobName(String[] jobName);

	int insertEmployeeList(List<Employee> empList);

	void insertCommonGroup(Employee e);

	List<Integer> selectAddrGroupIsNull();

}
