package com.hardcore.cw.ems.model.service;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.ems.model.dao.EntDao;

@Service
public class EntServiceImpl implements EntService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	@Autowired
	private EntDao ed;

	@Override
	public List<HashMap<String, Object>> selectEntList(PageInfo pi, HashMap<String, Object> map) {

		return ed.selectEntList(sqlSession, pi, map);
	}

	@Override
	public int selectEntListCount(HashMap<String, Object> map) {
		
		return ed.selectEntListCount(sqlSession, map);
	}

	@Override
	public HashMap<String, Object> selectEntOne(int empDivNo) {

		return ed.selectEntOne(sqlSession, empDivNo);
	}

	@Override
	public int updateRecover(int empDivNo) {
		
		return ed.updateRecover(sqlSession, empDivNo);
	}

	@Override
	public int updateReEnroll(int empDivNo) {
		int result = 0;
		int result2 = ed.updateReEnrollHis(sqlSession, empDivNo);
		int result1 = ed.updateReEnroll(sqlSession, empDivNo);
		
		if(result1 > 0 && result2 > 0) {
			result++;
		}
		return result;
	}

	@Override
	public int updateDel(int empDivNo) {

		return ed.updateDel(sqlSession, empDivNo);
	}


}
