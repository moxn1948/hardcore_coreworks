package com.hardcore.cw.eas.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.common.CommonsUtils;
import com.hardcore.cw.common.FileSizeCheck;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.eas.model.service.EasDetailService;
import com.hardcore.cw.eas.model.service.EasInsertService;
import com.hardcore.cw.eas.model.service.EasSelectService;
import com.hardcore.cw.eas.model.service.EasUpdateService;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.Eas;
import com.hardcore.cw.eas.model.vo.EasPath;
import com.hardcore.cw.eas.model.vo.RelDoc;
import com.hardcore.cw.eas.model.vo.VersionHistory;

@Controller
public class EasUpdateController {
	@Autowired
	private EasSelectService ess;
	@Autowired
	private EasUpdateService eus;
	@Autowired
	private EasDetailService eds;
	@Autowired
	private EasInsertService eis;
	@Autowired
	private AddrMenuService ams;

	@RequestMapping("selectEasDetailForUpdate")
	public ModelAndView selectEasDetail(String type, String btn, String easNo, ModelAndView mv) {

		int eNo = Integer.parseInt(easNo);
		
		//Format selectFormat = eis.selectFormat(formatNo);		
		//System.out.println("Controller : " + selectFormat);

		// 사내 부서 구조 조회
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();

		List<HashMap<String, Object>> empList = ams.selectEmpList();

		HashMap<String, Object> eas = ess.selectEasDetailForUpdate(eNo);
		
		HashMap<String, Object> docCnt = ess.selectDocCnt(eNo);
		// 보안상세
		List<Object> upProtectPerson = ess.selectProtectPersonList(eNo);

		// 전자결재 제목, 내용
		HashMap<String, Object> upDocDet = eds.selectDetDocDetOne(eNo);

		// 전자결재 공람자
		List<HashMap<String, Object>> upPubList = eds.selectDetPubList(eNo);

		// 전자결재 결재경로
		List<HashMap<String, Object>> upPathList = eds.selectDetPathtList(eNo);

		// 전자결재 관련문서
		List<HashMap<String, Object>> upRelList = eds.selectDetReltList(eNo);

		// 전자결재 첨부파일
		List<HashMap<String, Object>> upAttList = eds.selectDetAttList(eNo);
		
		for (int i = 0; i < upAttList.size(); i++) {
			String filePath = (String) upAttList.get(i).get("FILE_PATH");
			String fileName = (String) upAttList.get(i).get("CHANGE_NAME");
			String fullPath = filePath + "\\" + fileName;
			File downFile = new File(fullPath);
			String size = FileSizeCheck.sizeCalculation(downFile.length());
			
			upAttList.get(i).put("size", size);
		}
		
		// 전자결재라인 : 결재
		List<HashMap<String, Object>> upPathAsList = eds.selectDetPathAsList(eNo);

		// 전자결재라인 : 합의
		List<HashMap<String, Object>> upPathAgmList = eds.selectDetPathAgmList(eNo);

		mv.addObject("eas", eas);
		mv.addObject("docCnt", docCnt);
		//mv.addObject("format", selectFormat);
		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		mv.addObject("protectPerson", upProtectPerson);
		mv.addObject("upDocDet", upDocDet);
		mv.addObject("upPubList", upPubList);
		mv.addObject("upPathList", upPathList);
		mv.addObject("upRelList", upRelList);
		mv.addObject("upAttList", upAttList);
		mv.addObject("upPathAsList", upPathAsList);
		mv.addObject("upPathAgmList", upPathAgmList);
		
		if(btn.equals("update")) {
			mv.setViewName("my_wait_det_up");		
		}else {
			mv.addObject("type", type);			
			mv.setViewName("new_eas");
		}

		return mv;
	}
	
	@PostMapping("reinsert")	//재기안
	public String reInsertEas(Eas eas, DocDetail dd, MultipartHttpServletRequest mtfRequest) {		

		Eas upEas = new Eas();
		
		upEas.setEasNo(eas.getEasNo());
		upEas.setLimitYn(eas.getLimitYn());
		upEas.setProtectYn(eas.getProtectYn());
		upEas.setEmergencyYn(eas.getEmergencyYn());
		upEas.setFormatNo(eas.getFormatNo());
		upEas.setEmpDivNo(eas.getEmpDivNo());
		java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
		upEas.setEasSdate(sqlDate);	
		upEas.setDocNo(eas.getDocNo());
		upEas.setTempYn(eas.getTempYn());
		String easPeriod = eas.getEasPeriod().substring(0, 10);
		upEas.setEasPeriod(easPeriod);
		upEas.setEasDept(eas.getEasDept());
		upEas.setSenderName(eas.getSenderName());
		upEas.setReceiverName(eas.getReceiverName());
		upEas.setType(eas.getType());
		if(eas.getProtectYn()!=null) {
	         String protectYn = mtfRequest.getParameter("protectYn");
	         eas.setProtectYn(protectYn);
	         String protectYnDetail = mtfRequest.getParameter("protectYnDetail");
	         String[] strArr = protectYnDetail.split(",");
	         
	         for(int i=0; i<strArr.length; i++) {
	            if(strArr[i].equals("up")) {
	               eas.setSeniorYn("Y");
	            }else if(strArr[i].equals("equal")) {
	               eas.setMateYn("Y");
	            }else if(strArr[i].equals("dept")) {
	               eas.setDeptYn("Y");
	            }
	         }
	         
	      }
		upEas.setStatus("N");
		
		// 결재경로 
		String easLineStr1 = mtfRequest.getParameter("easPathRank");
		String easLineStr2 = mtfRequest.getParameter("easProcHow");
		String easLineStr3 = mtfRequest.getParameter("easLineEmpNo");	

		String[] easPathRank = easLineStr1.split(", ");
		String[] easProcHow = easLineStr2.split(", ");
		String[] easLineEmpNo = easLineStr3.split(", ");
		
		List<EasPath> easPathList = new ArrayList<>();
				
		EasPath drafterEp = new EasPath();
		drafterEp.setEasNo(upEas.getEasNo());
		drafterEp.setProcHow("DRAFT");
		drafterEp.setEmpDivNo(eas.getEmpDivNo());
		drafterEp.setPathRank(0);
		drafterEp.setProcState("COMPLETE"); //기안과 동시에 상태는 완료된거임
				
		easPathList.add(drafterEp);
			
		for(int el=0; el<easPathRank.length; el++) {
			EasPath ep = new EasPath();

			if(el==easPathRank.length) {
				ep.setEasNo(upEas.getEasNo());
				ep.setProcHow(easProcHow[el]);
				ep.setEmpDivNo(Integer.parseInt(easLineEmpNo[el]));
				ep.setPathRank(Integer.parseInt(easPathRank[el]));
				ep.setProcState("PROC");	//기안과 동시에 기안자를 제외한 결재선의 첫번째 순서인 사람은 진행 상태가 '진행'
			}else {
				ep.setEasNo(upEas.getEasNo());
				ep.setProcHow(easProcHow[el]);
				ep.setEmpDivNo(Integer.parseInt(easLineEmpNo[el]));
				ep.setPathRank(Integer.parseInt(easPathRank[el]));
				ep.setProcState("WAIT");	
			}
					
			easPathList.add(ep);
		}
				
		int easPathResult = eis.insertEasPath(easPathList);
		
		//리다이렉트 - insert를 한꺼번에 다 해야하는데 아직 안되서 아무것도 안보임 ㅠㅠ
		return "redirect:selectEasDetail.ea?easNo="+upEas.getEasNo()+"&type=myWait&origin=0&proxy=0";
	}
	
	@PostMapping("updateEas")	//수정 - 버전번호 증가 , 기안번호는 그대로
	public String updateEas(Eas eas, DocDetail dd, MultipartHttpServletRequest mtfRequest) {
		//버전
		VersionHistory ver = new VersionHistory();
		ver.setVersionNo(dd.getVersionNo()+1);
		ver.setEasNo(eas.getEasNo());
		
		int verResult = eis.insertVersion(ver);
		
		//문서 제목, 문서 본문
		DocDetail updateDd = new DocDetail();
		updateDd.setDocCnt(dd.getDocCnt().substring(1));
		updateDd.setDocTit(dd.getDocTit());
		updateDd.setVersionNo(dd.getVersionNo()+1);
		updateDd.setEasNo(dd.getEasNo());
		
		int ddResult = eis.insertDocDetail(updateDd);
		
		//관련 문서
		String relDocNo = mtfRequest.getParameter("relDocNo");
		
		if(!relDocNo.equals("")) {
			String[] arr = relDocNo.split(",");
			
			ArrayList<RelDoc> upDocList = new ArrayList<>();
			
			for(int i=0; i<arr.length; i++) {
				RelDoc rd = new RelDoc();
				
				rd.setRelDocNo(Integer.parseInt(arr[i]));
				rd.setVersionNo(dd.getVersionNo()+1);
				rd.setEasNo(eas.getEasNo());
				
				upDocList.add(rd);
			}
			
			int relDocResult = eis.insertRelDoc(upDocList);
		}
		
		// 첨부파일이 있을 경우
				List<MultipartFile> fileList = mtfRequest.getFiles("file");
				
				int attachResult = 0;
				if (fileList != null) {

					String root = mtfRequest.getSession().getServletContext().getRealPath("resources");

					String filePath = root + "\\eas_uploadFiles";

					String originFileName = "";
					String ext = "";
					String changeName = "";
					
					ArrayList<Object> attachList = new ArrayList<>();

					for (MultipartFile mf : fileList) {
						originFileName = mf.getOriginalFilename();
						ext = originFileName.substring(originFileName.lastIndexOf("."));
						changeName = CommonsUtils.getRandomString();

						Attachment attachment = new Attachment();

						attachment.setOriginName(originFileName);
						attachment.setChangeName(changeName + ext);
						attachment.setFilePath(filePath);
						attachment.setType("EAS");
						attachment.setStatus("N");
						attachment.setVersionNo(dd.getVersionNo()+1);
						attachment.setEasNo(eas.getEasNo());

						attachList.add(attachment);

						try {
							mf.transferTo(new File(filePath + "\\" + changeName + ext));
						} catch (IllegalStateException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					attachResult = eis.insertAttachment(attachList);
				}
	
		return "redirect:selectEasDetail.ea?easNo="+eas.getEasNo()+"&type=myWait&origin="+eas.getEmpDivNo()+"&proxy=0";
	}

}
