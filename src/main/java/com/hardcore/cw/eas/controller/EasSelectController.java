package com.hardcore.cw.eas.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.service.EasSelectService;

@Controller
public class EasSelectController {
	@Autowired
	private EasSelectService ess;

	// 전자결재 첫 화면 내 결재 - 목록 불러오기
	@GetMapping("selectEasList")
	public ModelAndView selectMyEasWaitList(String menu, ModelAndView mv, HttpSession session,
			HttpServletRequest request) {

		LoginEmp loginUser = (LoginEmp) session.getAttribute("loginUser");
		int empDivNo = loginUser.getEmpDivNo();

		int currentPage = 1;

		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}

		Map<String, Object> listMap = new HashMap<>();
		listMap.put("menu", menu);
		listMap.put("empDivNo", empDivNo); // 로그인 유저
		// 대리결재자 여부..
		List<Object> absenceList = ess.findAbsence(empDivNo);

		if (absenceList.size() != 0) {
			listMap.put("absenceList", absenceList);
		}
		int listCount = ess.getListCount(listMap);
		PageInfo pi = Pagination.getPageInfo(currentPage, listCount);

		Map<String, Object> dataMap = new HashMap<>();
		dataMap.put("menu", menu);
		dataMap.put("empDivNo", empDivNo);
		dataMap.put("pi", pi);

		if (absenceList.size() != 0) {
			dataMap.put("absenceList", absenceList);
		}

		List<Object> list = ess.selectEasList(dataMap);

		mv.addObject("menu", menu);
		mv.addObject("listCount", listCount);
		mv.addObject("pi", pi);
		mv.addObject("list", list);
		mv.setViewName("my_wait_list");

		return mv;
	}

	// 기안자의 발신명의 + 관련문서 읽어오기
	@PostMapping("selectReceiverAndDoc")
	public ModelAndView selectReceiverAndDoc(String eNo, ModelAndView mv, HttpServletResponse response) {

		int empDivNo = Integer.parseInt(eNo);

		ArrayList<Object> receivers = ess.selectReceiver(empDivNo);
		List<HashMap<String, Object>> docList = ess.selectDoc(empDivNo);

		mv.addObject("receivers", receivers);
		mv.addObject("docList", docList);
		response.setCharacterEncoding("UTF-8");

		mv.setViewName("jsonView");

		return mv;
	}

}
