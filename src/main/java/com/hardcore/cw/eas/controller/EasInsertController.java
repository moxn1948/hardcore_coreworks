package com.hardcore.cw.eas.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.common.CommonsUtils;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.eas.model.service.EasInsertService;
import com.hardcore.cw.eas.model.service.EasSelectService;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.Eas;
import com.hardcore.cw.eas.model.vo.EasPath;
import com.hardcore.cw.eas.model.vo.Publisher;
import com.hardcore.cw.eas.model.vo.RelDoc;
import com.hardcore.cw.eas.model.vo.VersionHistory;

@Controller
public class EasInsertController {
	@Autowired
	private EasSelectService ess;
	@Autowired
	private EasInsertService eis;
	@Autowired
	private AddrMenuService ams;

	// 새 결재 진행 버튼 클릭 시 결재양식 리스트 불러오기
	@RequestMapping("selectFormatList")
	public ModelAndView selectFormatList(ModelAndView mv, HttpServletResponse response) {

		// 서식함 구조 조회
		ArrayList<HashMap<String, Object>> docPath = eis.selectDocPath();

		// 서식 조회
		List<Format> formatList = eis.selectFormatList();

		System.out.println("docPath : " + docPath);
		System.out.println("formatList : " + formatList);

		mv.addObject("docPath", docPath);
		mv.addObject("formatList", formatList);
		response.setCharacterEncoding("UTF-8");

		mv.setViewName("jsonView");

		return mv;
	}

	// 서식 정보 불러오기용 메소드
	@RequestMapping("selectOneFormat")
	public ModelAndView selectOneFormat(String fNo, String deptNo, ModelAndView mv, HttpServletResponse response) {
		int formatNo = Integer.parseInt(fNo);
		System.out.println("formatNo : " + formatNo);

		Format format = eis.selectOneFormat(formatNo);

		int dNo = Integer.parseInt(deptNo);
		String deptName = eis.findDeptName(dNo);
		System.out.println("deptName : " + deptName);

		System.out.println("format : " + format);

		mv.addObject("format", format);
		mv.addObject("deptName", deptName);
		response.setCharacterEncoding("UTF-8");
		mv.setViewName("jsonView");

		return mv;
	}

	// 결재양식 선택 후 페이지 불러오기용 메소드
	@RequestMapping("viewNewEasPage")
	public String viewNewEasPage(String formatNo, HttpSession session) {
		Format selectFormat = eis.selectFormat(formatNo);

		System.out.println("Controller : " + selectFormat);

		// 사내 부서 구조 조회
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();

		List<HashMap<String, Object>> empList = ams.selectEmpList();

		session.setAttribute("format", selectFormat);
		session.setAttribute("deptList", deptList);
		session.setAttribute("empList", empList);

		return "new_eas";
	}
	
	// 관련문서 선택 후 읽어오기
	@RequestMapping("selectRelDocList")
	public ModelAndView selectRelDocList(ModelAndView mv, String relDocNum, HttpServletResponse response) {
		String[] str = relDocNum.split(",");
		
		ArrayList<Integer> easNo = new ArrayList<>();
		for(int i=0; i<str.length; i++) {
			easNo.add(Integer.parseInt(str[i]));
		}
		
		ArrayList<HashMap<String, Object>> relDocList = eis.selectRelDocList(easNo);
		
		System.out.println("relDocList : " + relDocList);
		
		mv.addObject("relDocList", relDocList);
		response.setCharacterEncoding("UTF-8");
		mv.setViewName("jsonView");
		return mv;
	}
	
	//dept ajax
	@RequestMapping("findDeptName")
	public ModelAndView findDeptName(ModelAndView mv, String dNo, HttpServletResponse response) {
		int deptNo = Integer.parseInt(dNo);
		
		String deptName = eis.findDeptName(deptNo);
		
		mv.addObject("deptName", deptName);
		response.setCharacterEncoding("UTF-8");
		mv.setViewName("jsonView");
		return mv;
	}

	// 새 결재 insert (+ 파일첨부..)
	@PostMapping("insertNewEas")
	public String insertMember(ModelAndView mv, Eas eas, MultipartHttpServletRequest mtfRequest) {

		// 사원부서 찾기
		int deptNo = eas.getEasDept();
		String deptName = eis.findDeptName(deptNo);

		Eas newEas = new Eas();
		
		//newEas.setEasNo(eas.getEasNo());
		if (eas.getLimitYn() != null) {
			newEas.setLimitYn("Y");
		} else {
			newEas.setLimitYn("N");
		}
		if(eas.getProtectYn()!=null) {
			String protectYn = mtfRequest.getParameter("protectYn");
			newEas.setProtectYn(protectYn);
			String protectYnDetail = mtfRequest.getParameter("protectYnDetail");
			String[] strArr = protectYnDetail.split(",");
			
			for(int i=0; i<strArr.length; i++) {
				if(strArr[i].equals("up")) {
					newEas.setSeniorYn("Y");
				}else if(strArr[i].equals("equal")) {
					newEas.setMateYn("Y");
				}else if(strArr[i].equals("dept")) {
					newEas.setDeptYn("Y");
				}
			}
			
		}
		if (eas.getEmergencyYn() != null) {
			newEas.setEmergencyYn("Y");
		} else {
			newEas.setEmergencyYn("N");
		}
		newEas.setFormatNo(eas.getFormatNo());
		newEas.setEmpDivNo(eas.getEmpDivNo());
		java.sql.Date sqlDate = new java.sql.Date(new java.util.Date().getTime());
		newEas.setEasSdate(sqlDate);	
		newEas.setDocNo(eas.getDocNo());
		if (eas.getTempYn() != null) {
			newEas.setTempYn(eas.getTempYn());
		} else {
			newEas.setTempYn("N");
		}
		if(!eas.getEasPeriod().equals("1993-01-01")) {
			newEas.setEasPeriod(eas.getEasPeriod());
		}
		newEas.setEasDept(deptNo);
		newEas.setSenderName(eas.getSenderName());
		newEas.setReceiverName(eas.getReceiverName());
		newEas.setType("PROC");
		newEas.setStatus("N");

		System.out.println("가공된 eas : " + newEas);
		System.out.println("easPeriod : " + eas.getEasPeriod());

		int easResult = eis.insertNewEas(newEas);	

		// 결재경로
		String easLineStr1 = mtfRequest.getParameter("easPathRank");
		String easLineStr2 = mtfRequest.getParameter("easProcHow");
		String easLineStr3 = mtfRequest.getParameter("easLineEmpNo");

		String[] easPathRank = easLineStr1.split(",");
		String[] easProcHow = easLineStr2.split(",");
		String[] easLineEmpNo = easLineStr3.split(",");
		
		List<EasPath> easPathList = new ArrayList<>();
		
		EasPath drafterEp = new EasPath();
		drafterEp.setEasNo(newEas.getEasNo());
		drafterEp.setProcHow("DRAFT");
		drafterEp.setEmpDivNo(eas.getEmpDivNo());
		drafterEp.setPathRank(0);
		drafterEp.setProcState("COMPLETE"); //기안과 동시에 상태는 완료된거임
		
		easPathList.add(drafterEp);
		
		for(int el=0; el<easPathRank.length; el++) {
			EasPath ep = new EasPath();

			if(el==0) {
				ep.setEasNo(newEas.getEasNo());
				ep.setProcHow(easProcHow[el]);
				ep.setEmpDivNo(Integer.parseInt(easLineEmpNo[el]));
				ep.setPathRank(Integer.parseInt(easPathRank[el]));
				ep.setProcState("PROC");	//기안과 동시에 기안자를 제외한 결재선의 첫번째 순서인 사람은 진행 상태가 '진행'
			}else {
				ep.setEasNo(newEas.getEasNo());
				ep.setProcHow(easProcHow[el]);
				ep.setEmpDivNo(Integer.parseInt(easLineEmpNo[el]));
				ep.setPathRank(Integer.parseInt(easPathRank[el]));
				ep.setProcState("WAIT");	
			}
			
			easPathList.add(ep);
		}
		
		int easPathResult = eis.insertEasPath(easPathList);
		
		//전자결재 버전 - 첫 기안 시 무조건 0번.
		VersionHistory ver = new VersionHistory();
		ver.setVersionNo(0);
		ver.setEasNo(newEas.getEasNo());
		int verResult = eis.insertVersion(ver);
		
		//전자결재 본문 내용 저장
		String docCnt = mtfRequest.getParameter("docCnt");
		String docTitle = mtfRequest.getParameter("easTitle");
		DocDetail dd = new DocDetail();		
		
		dd.setDocCnt(docCnt);
		dd.setVersionNo(0);
		dd.setEasNo(newEas.getEasNo());
		dd.setDocTit(docTitle);
		
		int docDetailResult = eis.insertDocDetail(dd);

		// 보안 처리 - 보안상세 테이블
		System.out.println("protectYn : " + eas.getProtectYn());
		
		int protectResult = 0;
		int cnt = 0;
		ArrayList<List<HashMap<String, Object>>> protectEmpList = new ArrayList<>();
		
		if(eas.getProtectYn() != null) {					
			if(eas.getProtectYn().equals("RANGE")) {
				String protectYnDetail = mtfRequest.getParameter("protectYnDetail");
				System.out.println("protectYnDetail : " + protectYnDetail);
				String[] strArr = protectYnDetail.split(",");
				
				LoginEmp drafter = (LoginEmp) mtfRequest.getSession().getAttribute("loginUser");
				int drafterNo = drafter.getEmpDivNo();
				
				for(int i=0; i<strArr.length; i++) {
					List<HashMap<String, Object>> list = eis.selectProtectEmpList(drafterNo, strArr[i]);
					
					if(list != null) {
						protectEmpList.add(list);
					}	
				}
				
				System.out.println("protectEmpList : " + protectEmpList);
	
				for(int j=0; j<protectEmpList.size(); j++) {			
					if(protectEmpList.get(j) != null) {
						int temp= eis.insertProtectDetail(newEas.getEasNo(), protectEmpList.get(j));

						if(temp > 0) {
							cnt ++;
						}
					}
				}
			}else if(eas.getProtectYn().equals("PERSON")){
				String protectDetailPerson = mtfRequest.getParameter("protectDetailPerson");
				System.out.println("protectDetailPerson : " + protectDetailPerson);
				
				String[] personArr = protectDetailPerson.split(",");
				
				LoginEmp drafter = (LoginEmp) mtfRequest.getSession().getAttribute("loginUser");
				int drafterNo = drafter.getEmpDivNo();
				
				for(int i=0; i<personArr.length; i++) {
					List<HashMap<String, Object>> list = eis.selectProtectEmpList(drafterNo, personArr[i]);
					
					if(list != null) {
						protectEmpList.add(list);
					}
				}
				
				System.out.println("protectEmpList : " + protectEmpList);
				
				for(int j=0; j<protectEmpList.size(); j++) {			
					if(protectEmpList.get(j) != null) {
						int temp= eis.insertProtectDetail(newEas.getEasNo(), protectEmpList.get(j));
						
						if(temp > 0) {
							cnt ++;
						}
					}
				}
			}
			
		}
		
		if(cnt == protectEmpList.size()) {
			protectResult = 1;
		}else {
			protectResult = 0;
		}
		

		// 관련문서 - 관련문서 테이블 - 버전번호를 버전이력 테이블에서 가져와야함. --> 테이블 수정 후 다시!!!
		String relDocNo = mtfRequest.getParameter("relDocNo");
		System.out.println("relDocNo : " + relDocNo);
		
		int relDocResult = 0;
		if(!relDocNo.equals("")) {
			String[] str = relDocNo.split(",");
			List<RelDoc> relDocList = new ArrayList<>(); 
			
			for(int r=0; r<str.length; r++) {
				RelDoc rd = new RelDoc();
				
				rd.setEasNo(newEas.getEasNo());
				rd.setVersionNo(0);
				rd.setRelDocNo(Integer.parseInt(str[r]));
				
				relDocList.add(rd);
			}
			
			relDocResult = eis.insertRelDoc(relDocList);
		}

		// 임시저장 여부 - 미루기

		
		// 공람자
		String shareEmpStr = mtfRequest.getParameter("shareEmpNo");		
		System.out.println("shareEmpNo : " + shareEmpStr);
		
		int publisherResult = 0;
		if(!shareEmpStr.equals("")) {
			String[] shareEmpArr = shareEmpStr.split(",");
			
			List<Publisher> pList = new ArrayList<>();
			for(int i=0; i<shareEmpArr.length; i++) {
				Publisher p = new Publisher();
				
				p.setEasNo(newEas.getEasNo());
				p.setEmpDivNo(Integer.parseInt(shareEmpArr[i]));
				
				pList.add(p);
			}
			
			publisherResult = eis.insertPublisher(pList);
		}
		

		// 첨부파일이 있을 경우
		List<MultipartFile> fileList = mtfRequest.getFiles("file");
		
		int attachResult = 0;
		if (fileList != null) {
			System.out.println(fileList);

			String root = mtfRequest.getSession().getServletContext().getRealPath("resources");

			String filePath = root + "\\eas_uploadFiles";

			System.out.println(filePath);

			String originFileName = "";
			String ext = "";
			String changeName = "";
			
			ArrayList<Object> attachList = new ArrayList<>();
			for (MultipartFile mf : fileList) {
				originFileName = mf.getOriginalFilename();
				ext = originFileName.substring(originFileName.lastIndexOf("."));
				changeName = CommonsUtils.getRandomString();

				Attachment attachment = new Attachment();

				attachment.setOriginName(originFileName);
				attachment.setChangeName(changeName + ext);
				attachment.setFilePath(filePath);
				attachment.setType("EAS");
				attachment.setStatus("N");
				attachment.setVersionNo(0); // insert시의 버전번호는 무조건 0
				attachment.setEasNo(newEas.getEasNo());

				attachList.add(attachment);

				try {
					mf.transferTo(new File(filePath + "\\" + changeName + ext));
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			attachResult = eis.insertAttachment(attachList);
		}

		
		return "redirect:selectEasList.ea?menu=myWait";
	}

}
