package com.hardcore.cw.eas.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.common.Pagination;
import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.service.DocumentService;

@Controller
public class DocumentController {

	@Autowired
	private AddrMenuService ams;
	@Autowired
	private DocumentService ds;
	
	@GetMapping("selectDocList.ea")
	public ModelAndView selectDocList(ModelAndView mv, String eno, String dno, String menu, String srchType, String currentPage, String keyword) {
		int empDivNo = Integer.parseInt(eno);
		int deptNo = Integer.parseInt(dno);
		int cp = 1;
		
		if(currentPage != null) {
			cp = Integer.parseInt(currentPage);
		}

		// 사내 부서 구조 조회
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();

		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		
		HashMap<String, Object> map = new HashMap<>();
		map.put("empDivNo", empDivNo);
		map.put("deptNo", deptNo);
		map.put("menu", menu);
		map.put("srchType", srchType);
		map.put("keyword", keyword);
		
		

		int listCount = ds.selectDocListCount(map);

		PageInfo pi = Pagination.getPageInfo(cp, listCount);
		
		List<HashMap<String, Object>> list = ds.selectDocList(pi, map);
		
		mv.addObject("list", list);
		mv.addObject("pi", pi);
		
		mv.setViewName("doc_comp");
		
		return mv;
	}
	

	@PostMapping("chkLimitYn.ea")
	public ModelAndView selectLimitYn(ModelAndView mv, int empDivNo, int easNo, HttpServletResponse response) {
		
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("empDivNo", empDivNo);
		map.put("easNo", easNo);
		
		int chkLimitYn = ds.selectLimitYn(map);
		
		mv.addObject("chkLimitYn", chkLimitYn);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		
		return mv;
	}
}
