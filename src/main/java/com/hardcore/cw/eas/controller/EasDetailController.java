package com.hardcore.cw.eas.controller;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hardcore.cw.address.model.service.AddrMenuService;
import com.hardcore.cw.common.FileSizeCheck;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.LoginEmp;
import com.hardcore.cw.eas.model.service.EasDetailService;

@Controller
public class EasDetailController {
	@Autowired
	private EasDetailService eds;
	@Autowired
	private AddrMenuService ams;
	
	@GetMapping("selectEasDetail.ea")
	public ModelAndView selectEasDetail(ModelAndView mv, String easNo, String origin, String proxy, String type) {
		int eaNo = Integer.parseInt(easNo);
		int originNo = Integer.parseInt(origin);
		int proxyNo = Integer.parseInt(proxy);

		// 사내 부서 구조 조회
		ArrayList<HashMap<String, Object>> deptList = ams.selectDeptList();
		List<HashMap<String, Object>> empList = ams.selectEmpList();

		mv.addObject("deptList", deptList);
		mv.addObject("empList", empList);
		
		// 전자결재 기본 테이블
		HashMap<String, Object> detEas = eds.selectDetEasOne(eaNo);
		
		// 전자결재 제목, 내용
		HashMap<String, Object> detDocDet = eds.selectDetDocDetOne(eaNo);

		// 전자결재 공람자
		List<HashMap<String, Object>> detPubList = eds.selectDetPubList(eaNo);
		
		// 전자결재 보안설정
		List<HashMap<String, Object>> detProtList = eds.selectDetProtList(eaNo);
		
		// 전자결재 결재경로
		List<HashMap<String, Object>> detPathList = eds.selectDetPathtList(eaNo);
		
		// 전자결재 관련문서
		List<HashMap<String, Object>> detRelList = eds.selectDetReltList(eaNo);
		
		// 전자결재 첨부파일
		List<HashMap<String, Object>> detAttList = eds.selectDetAttList(eaNo);
		
		// 전자결재라인 : 결재
		List<HashMap<String, Object>> detPathAsList = eds.selectDetPathAsList(eaNo);

		// 전자결재라인 : 합의
		List<HashMap<String, Object>> detPathAgmList = eds.selectDetPathAgmList(eaNo);
		
		for (int i = 0; i < detAttList.size(); i++) {
			String filePath = (String) detAttList.get(i).get("FILE_PATH");
			String fileName = (String) detAttList.get(i).get("CHANGE_NAME");
			String fullPath = filePath + "\\" + fileName;
			File downFile = new File(fullPath);
			String size = FileSizeCheck.sizeCalculation(downFile.length());
			
			detAttList.get(i).put("size", size);
		}
		
		if(type.equals("easWait") || type.equals("easProc")) {
			// 결재 방식
			HashMap<String, Object> detProcHow = eds.selectDetProcHow(eaNo, originNo);
			
			mv.addObject("detProcHow", detProcHow);
		}
		
		
		int compEmpDivNo = 0;
		for (int i = 0; i < detPathList.size(); i++) {
			if(!detPathList.get(i).get("PROC_HOW").equals("REFER") && detPathList.get(i).get("EAS_STATUE").equals("COMPLETE")) {
				compEmpDivNo = Integer.parseInt(detPathList.get(i).get("ENO") + "");
				mv.addObject("compEmpDivNo", detPathList.get(i).get("ENO"));
				
				break;
			}
			
		}
		
		mv.addObject("type", type);
		
		mv.addObject("detEas", detEas);
		mv.addObject("detDocDet",detDocDet);
		mv.addObject("detPubList", detPubList);
		mv.addObject("detProtList", detProtList);
		mv.addObject("detPathList", detPathList);
		mv.addObject("detRelList", detRelList);
		mv.addObject("detAttList", detAttList);
		mv.addObject("detPathAsList", detPathAsList);
		mv.addObject("detPathAgmList", detPathAgmList);
		
		mv.setViewName("eas_det");
		return mv;
	}

	@RequestMapping("fileDownload.ea")
	public ModelAndView reDocumentDown(String fileNo) {
		int no = Integer.parseInt(fileNo);
		Attachment a = eds.selectOneAttachment(no);
		
		String filePath = a.getFilePath();
		String fileName = a.getChangeName();
		String fullPath = filePath + "\\" + fileName;
		File downFile = new File(fullPath);
		
		Map<String, Object> map = new HashMap<>();
		map.put("downFile", downFile);
		map.put("originName", a.getOriginName());
		
		return new ModelAndView("downloadView", "downloadFile", map);
	}
	
	@PostMapping("selectRelDocOne.ea")
	public ModelAndView selectRelDocOne(ModelAndView mv, int docNo, HttpServletResponse response) {
		
		// 전자결재 기본 테이블
		HashMap<String, Object> detEas = eds.selectDetEasOne(docNo);
		
		// 전자결재 제목, 내용
		HashMap<String, Object> detDocDet = eds.selectDetDocDetOne(docNo);

		// 전자결재라인 : 결재
		List<HashMap<String, Object>> detPathAsList = eds.selectDetPathAsList(docNo);

		// 전자결재라인 : 합의
		List<HashMap<String, Object>> detPathAgmList = eds.selectDetPathAgmList(docNo);
		
		mv.addObject("detEas", detEas);
		mv.addObject("detDocDet", detDocDet);
		mv.addObject("detPathAsList", detPathAsList);
		mv.addObject("detPathAgmList", detPathAgmList);
		
		response.setCharacterEncoding("UTF-8");
		
		mv.setViewName("jsonView");
		
		return mv;
	}

	@GetMapping("updateMyWaitCancel.ea")
	public ModelAndView updateMyWaitCancel(ModelAndView mv, String no) {
		int easNo = Integer.parseInt(no);
		
		int result = eds.updateMyWaitCancel(easNo);
		
		if(result > 0) {
			mv.addObject("msg", "상신이 취소되었습니다.");
			mv.addObject("view", "selectEasList.ea?menu=myWait");
			mv.setViewName("easAlert");
			
		}else {
			mv.addObject("msg", "상신 취소에 실패했습니다.");
			mv.addObject("view", "selectEasList.ea?menu=myWait");
			mv.setViewName("easAlert");
		}
		
		
		return mv;
	}
	

	@GetMapping("updateMyProcRecall.ea")
	public ModelAndView updateMyProcRecall(ModelAndView mv, String no) {
		int easNo = Integer.parseInt(no);
		
		int result = eds.updateMyProcRecall(easNo);
		
		if(result > 0) {
			mv.addObject("msg", "결재가 회수되었습니다.");
			mv.addObject("view", "selectEasList.ea?menu=myProc");
			mv.setViewName("easAlert");
			
		}else {
			mv.addObject("msg", "회수에 실패했습니다.");
			mv.addObject("view", "selectEasList.ea?menu=myProc");
			mv.setViewName("easAlert");
		}
		
		
		return mv;
	}

	@GetMapping("updateMyReDel.ea")
	public ModelAndView updateMyReDel(ModelAndView mv, String no) {
		int easNo = Integer.parseInt(no);
		
		int result = eds.updateMyReDel(easNo);
		
		if(result > 0) {
			mv.addObject("msg", "결재가 삭제되었습니다.");
			mv.addObject("view", "selectEasList.ea?menu=myRe");
			mv.setViewName("easAlert");
			
		}else {
			mv.addObject("msg", "삭제에 실패했습니다.");
			mv.addObject("view", "selectEasList.ea?menu=myRe");
			mv.setViewName("easAlert");
		}
		
		
		return mv;
	}

	@GetMapping("updateEasWaitAs.ea")
	public ModelAndView updateEasWaitAs(ModelAndView mv, String no, String origin, String proxy, String pathRank, String value, HttpServletRequest request) {
		int compare = ((LoginEmp) request.getSession().getAttribute("loginUser")).getEmpDivNo();
		int easNo = Integer.parseInt(no);
		int originNo = Integer.parseInt(origin);
		int proxyNo = Integer.parseInt(proxy);
		int pathRankNo = Integer.parseInt(pathRank);
		
		if(compare != proxyNo) {
			proxyNo = 0;
		}
		
		int result = 0;
		
		if(proxyNo != 0) {
			result = eds.updateEasWaitAsConfirmProxy(easNo, originNo, proxyNo, pathRankNo, value);
		}else {
			result = eds.updateEasWaitAsConfirm(easNo, originNo, pathRankNo, value);
		}
		
		if(result > 0) {
			if(value.equals("confirm")) {
				mv.addObject("msg", "결재 처리가 되었습니다.");
			}else {
				mv.addObject("msg", "반려 처리가 되었습니다.");
			}
		}else {
			if(value.equals("confirm")) {
				mv.addObject("msg", "결재에 실패했습니다.");
			}else {
				mv.addObject("msg", "반려에 실패했습니다.");
			}	
		}

		mv.addObject("view", "selectEasList.ea?menu=easWait");
		mv.setViewName("easAlert");
		
		return mv;
	}

	@GetMapping("updateEasWaitRefer.ea")
	public ModelAndView updateEasWaitRefer(ModelAndView mv, String no, String origin, String proxy, HttpServletRequest request) {
		int compare = ((LoginEmp) request.getSession().getAttribute("loginUser")).getEmpDivNo();
		int easNo = Integer.parseInt(no);
		int originNo = Integer.parseInt(origin);
		int proxyNo = Integer.parseInt(proxy);

		if(compare != proxyNo) {
			proxyNo = 0;
		}
		
		int result = eds.updateEasWaitRefer(easNo, originNo, proxyNo);
		
		if(result > 0) {
			mv.addObject("msg", "참조 확인이 되었습니다.");
			mv.addObject("view", "selectEasList.ea?menu=myRe");
			mv.setViewName("easAlert");
			
		}else {
			mv.addObject("msg", "참조 확인에 실패했습니다.");
			mv.addObject("view", "selectEasList.ea?menu=myRe");
			mv.setViewName("easAlert");
		}
		
		
		return mv;
	}

	@GetMapping("updateEasWaitConfirm.ea")
	public ModelAndView updateEasWaitConfirm(ModelAndView mv, String no, String origin, String proxy, String pathRank, HttpServletRequest request) {
		int compare = ((LoginEmp) request.getSession().getAttribute("loginUser")).getEmpDivNo();
		int easNo = Integer.parseInt(no);
		int originNo = Integer.parseInt(origin);
		int proxyNo = Integer.parseInt(proxy);
		int pathRankNo = Integer.parseInt(pathRank);

		if(compare != proxyNo) {
			proxyNo = 0;
		}
		
		int result = eds.updateEasWaitConfirm(easNo, originNo, pathRankNo, proxyNo);
		
		
		if(result > 0) {
			mv.addObject("msg", "확인 처리가 되었습니다.");
		}else {
			mv.addObject("msg", "확인 처리에 실패했습니다.");
		}

		mv.addObject("view", "selectEasList.ea?menu=easWait");
		mv.setViewName("easAlert");
		
		return mv;
	}

	@GetMapping("updateEasWaitAgm.ea")
	public ModelAndView updateEasWaitAgm(ModelAndView mv, String no, String origin, String proxy, String pathRank, String value, HttpServletRequest request) {
		int compare = ((LoginEmp) request.getSession().getAttribute("loginUser")).getEmpDivNo();
		int easNo = Integer.parseInt(no);
		int originNo = Integer.parseInt(origin);
		int proxyNo = Integer.parseInt(proxy);
		int pathRankNo = Integer.parseInt(pathRank);

		if(compare != proxyNo) {
			proxyNo = 0;
		}
		
		int result = eds.updateEasWaitAgmConfirm(easNo, originNo, proxyNo, pathRankNo, value);
		
		if(result > 0) {
			if(value.equals("agree")) {
				mv.addObject("msg", "찬성 처리가 되었습니다.");
			}else {
				mv.addObject("msg", "반대 처리가 되었습니다.");
			}
		}else {
			if(value.equals("agree")) {
				mv.addObject("msg", "찬성 처리에 실패했습니다.");
			}else {
				mv.addObject("msg", "반대 처리에 실패했습니다.");
			}	
		}

		mv.addObject("view", "selectEasList.ea?menu=easWait");
		mv.setViewName("easAlert");
		
		return mv;
	}
	
	@GetMapping("updateEasProcCan.ea")
	public ModelAndView updateEasProcCan(ModelAndView mv, String no, String origin, String proxy, String pathRank, HttpServletRequest request) {
		int compare = ((LoginEmp) request.getSession().getAttribute("loginUser")).getEmpDivNo();
		int easNo = Integer.parseInt(no);
		int originNo = Integer.parseInt(origin);
		int proxyNo = Integer.parseInt(proxy);
		int pathRankNo = Integer.parseInt(pathRank);

		if(compare != proxyNo) {
			proxyNo = 0;
		}
		
		int result = eds.updateEasProcCan(easNo, originNo, proxyNo, pathRankNo);
		
		
		if(result > 0) {
			mv.addObject("msg", "결재 취소가 되었습니다.");
		}else {
			mv.addObject("msg", "결재 취소에 실패했습니다.");
		}

		mv.addObject("view", "selectEasList.ea?menu=easWait");
		mv.setViewName("easAlert");
		
		return mv;
	}
}
