package com.hardcore.cw.eas.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.common.model.vo.PageInfo;

public interface DocumentDao {

	int selectDocListCount(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	List<HashMap<String, Object>> selectDocList(SqlSessionTemplate sqlSession, PageInfo pi,
			HashMap<String, Object> map);

	int selectLimitYn(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

}
