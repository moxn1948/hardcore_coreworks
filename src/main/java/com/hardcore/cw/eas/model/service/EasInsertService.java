package com.hardcore.cw.eas.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.DocPath;
import com.hardcore.cw.eas.model.vo.Eas;
import com.hardcore.cw.eas.model.vo.EasPath;
import com.hardcore.cw.eas.model.vo.Publisher;
import com.hardcore.cw.eas.model.vo.RelDoc;
import com.hardcore.cw.eas.model.vo.VersionHistory;
import com.hardcore.cw.mypage.model.vo.Absence;

public interface EasInsertService {

	Format selectFormat(String formatNo);

	String findDeptName(int dNo);

	int insertAttachment(ArrayList<Object> attachList);

	ArrayList<HashMap<String, Object>> selectDocPath();
	
	List<Format> selectFormatList();

	Format selectOneFormat(int formatNo);

	List<HashMap<String, Object>> selectProtectEmpList(int drafterNo, String strArr);

	ArrayList<HashMap<String, Object>> selectRelDocList(ArrayList<Integer> easNo);

	int insertVersion(VersionHistory ver);

	int insertRelDoc(List<RelDoc> relDocList);

	int insertEasPath(List<EasPath> easPathList);

	int insertPublisher(List<Publisher> pList);

	int insertDocDetail(DocDetail dd);

	Absence selectOneAbsence(String empDivNo);

	int insertNewEas(Eas eas);

	int insertProtectDetail(int easNo, List<HashMap<String, Object>> list);


}
