package com.hardcore.cw.eas.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class EasPath implements java.io.Serializable{
	private int easNo;
	private String procHow;
	private int empDivNo;
	private int pathRank;
	private String procState;
}
