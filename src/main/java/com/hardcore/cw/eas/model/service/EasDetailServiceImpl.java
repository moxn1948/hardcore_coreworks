package com.hardcore.cw.eas.model.service;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.common.model.vo.Attachment;
import com.hardcore.cw.eas.model.dao.EasDetailDao;

@Service
public class EasDetailServiceImpl implements EasDetailService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private EasDetailDao edd;
	
	@Override
	public HashMap<String, Object> selectDetEasOne(int easNo) {
		
		return edd.selectDetEasOne(sqlSession, easNo);
	}
	@Override
	public HashMap<String, Object> selectDetDocDetOne(int easNo) {
		
		return edd.selectDetDocDetOne(sqlSession, easNo);
	}
	@Override
	public List<HashMap<String, Object>> selectDetPubList(int easNo) {
		
		return edd.selectDetPubList(sqlSession, easNo);
	}
	@Override
	public List<HashMap<String, Object>> selectDetProtList(int easNo) {
		
		return edd.selectDetProtList(sqlSession, easNo);
	}
	@Override
	public List<HashMap<String, Object>> selectDetPathtList(int easNo) {
		
		return edd.selectDetPathtList(sqlSession, easNo);
	}
	@Override
	public List<HashMap<String, Object>> selectDetReltList(int easNo) {
		
		return edd.selectDetReltList(sqlSession, easNo);
	}
	@Override
	public List<HashMap<String, Object>> selectDetAttList(int easNo) {
		
		return edd.selectDetAttList(sqlSession, easNo);
		
	}
	@Override
	public List<HashMap<String, Object>> selectDetPathAsList(int easNo) {
		
		return edd.selectDetPathAsList(sqlSession, easNo);
	}
	@Override
	public List<HashMap<String, Object>> selectDetPathAgmList(int easNo) {
		
		return edd.selectDetPathAgmList(sqlSession, easNo);
	}
	@Override
	public Attachment selectOneAttachment(int no) {
		
		return edd.selectOneAttachment(sqlSession, no);
	}
	@Override
	public int updateMyWaitCancel(int easNo) {
		
		return edd.updateMyWaitCancel(sqlSession, easNo);
	}
	@Override
	public int updateMyProcRecall(int easNo) {
		
		return edd.updateMyProcRecall(sqlSession, easNo);
	}
	@Override
	public int updateMyReDel(int easNo) {
		
		return edd.updateMyReDel(sqlSession, easNo);
	}
	@Override
	public HashMap<String, Object> selectDetProcHow(int easNo, int originNo) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("easNo", easNo);
		map.put("originNo", originNo);
		
		return edd.selectDetProcHow(sqlSession, map);
	}
	
	@Override
	public int updateEasWaitAsConfirm(int easNo, int originNo, int pathRankNo, String value) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("easNo", easNo);
		map.put("originNo", originNo);
		map.put("pathRankNo", pathRankNo);
		map.put("value", value);
		
		int result = 0;
		int result0 = 0;
		int result1 = 0;
		int result2 = 0;
		
		result0 = edd.updateEasWaitAsConfirm(sqlSession, map);
		result2 = edd.insertEasWaitAsConfirmHis(sqlSession, map);
		
		if(!value.equals("return")) {

			if(result0 > 0) {
				int pathRank = pathRankNo;
				map.put("nextPathRank", pathRank);
				while(true) {
					HashMap<String, Object> next = edd.selectEasPathNextStr(sqlSession, map);
					
					if(next == null) {
						System.out.println("최종결재권자");
						result1 = edd.updateEasDocNo(sqlSession, easNo);
						result1 = edd.updateEasTypeComp(sqlSession, easNo);
						break;
					}else {
						if(next.get("PROC_HOW").equals("REFER")) {
							System.out.println("참조..");
							result1 = edd.updateEasWaitAsProcRefer(sqlSession, map);
							System.out.println(next);
							//result2 = edd.insertEasWaitAsConfirmHisNext(sqlSession, next);
							pathRank++;
							map.put("nextPathRank", pathRank);
							
						}else {
							System.out.println("업데이트");
							result1 = edd.updateEasWaitAsConfirmNext(sqlSession, map);
							//result2 = edd.insertEasWaitAsConfirmHisNext(sqlSession, next);
							
							break;
						}
						
				 	}
					
				}

			}
		}else {
			result1 = edd.updateEasTypeReturn(sqlSession, easNo);
			
		}
		
		if(result0 > 0 && result1 > 0) {
			result++;
		}
		
		return result;
	}
	
	@Override
	public int updateEasWaitAsConfirmProxy(int easNo, int originNo, int proxyNo, int pathRankNo, String value) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("easNo", easNo);
		map.put("originNo", originNo);
		map.put("proxyNo", proxyNo);
		map.put("pathRankNo", pathRankNo);
		map.put("value", value);
		
		int result = 0;
		int result0 = 0;
		int result1 = 0;
		int result2 = 0;
		
		result0 = edd.updateEasWaitAsConfirm(sqlSession, map);
		result2 = edd.insertEasWaitAsConfirmHisProxy(sqlSession, map);
		
		if(!value.equals("return")) {

			if(result0 > 0) {
				int pathRank = pathRankNo;
				map.put("nextPathRank", pathRank);
				while(true) {
					HashMap<String, Object> next = edd.selectEasPathNextStr(sqlSession, map);
					
					if(next == null) {
						System.out.println("최종결재권자");
						result1 = edd.updateEasDocNo(sqlSession, easNo);
						result1 = edd.updateEasTypeComp(sqlSession, easNo);
						break;
					}else {
						if(next.get("PROC_HOW").equals("REFER")) {
							System.out.println("참조..");
							result1 = edd.updateEasWaitAsProcRefer(sqlSession, map);
							System.out.println(next);
							//result2 = edd.insertEasWaitAsConfirmHisNext(sqlSession, next);
							pathRank++;
							map.put("nextPathRank", pathRank);
							
						}else {
							System.out.println("업데이트");
							result1 = edd.updateEasWaitAsConfirmNext(sqlSession, map);
							//result2 = edd.insertEasWaitAsConfirmHisNext(sqlSession, next);
							
							break;
						}
						
				 	}
					
				}

			}
		}else {
			result1 = edd.updateEasTypeReturn(sqlSession, easNo);
			
		}
		
		if(result0 > 0 && result1 > 0) {
			result++;
		}
		
		return result;
	}
	@Override
	public int updateEasWaitRefer(int easNo, int originNo, int proxyNo) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("easNo", easNo);
		map.put("originNo", originNo);
		map.put("proxyNo", proxyNo);
		
		int result1 = edd.updateEasWaitRefer(sqlSession, map);
		int result2 = edd.insertEasWaitReferHis(sqlSession, map);
		
		int result = 0;
		if(result1 > 0 && result2 > 0) {
			result++;
		}
		return result;
	}
	@Override
	public int updateEasWaitConfirm(int easNo, int originNo, int pathRankNo, int proxyNo) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("easNo", easNo);
		map.put("originNo", originNo);
		map.put("proxyNo", proxyNo);
		map.put("pathRankNo", pathRankNo);
		
		int result = 0;
		int result0 = 0;
		int result1 = 0;
		int result2 = 0;
		
		result0 = edd.updateEasWaitAsConfirm(sqlSession, map);
		result2 = edd.insertEasWaitReferHis(sqlSession, map);
		

		if(result0 > 0) {
			int pathRank = pathRankNo;
			map.put("nextPathRank", pathRank);
			while(true) {
				HashMap<String, Object> next = edd.selectEasPathNextStr(sqlSession, map);
				
				if(next == null) {
					System.out.println("최종결재권자");
					result1 = edd.updateEasDocNo(sqlSession, easNo);
					result1 = edd.updateEasTypeComp(sqlSession, easNo);
					break;
				}else {
					if(next.get("PROC_HOW").equals("REFER")) {
						System.out.println("참조..");
						result1 = edd.updateEasWaitAsProcRefer(sqlSession, map);
						System.out.println(next);
						//result2 = edd.insertEasWaitAsConfirmHisNext(sqlSession, next);
						pathRank++;
						map.put("nextPathRank", pathRank);
						
					}else {
						System.out.println("업데이트");
						result1 = edd.updateEasWaitAsConfirmNext(sqlSession, map);
						//result2 = edd.insertEasWaitAsConfirmHisNext(sqlSession, next);
						
						break;
					}
					
			 	}
				
			}

		}
	
		
		if(result0 > 0 && result1 > 0 && result2 > 0) {
			result++;
		}
		
		return result;
	}
	@Override
	public int updateEasWaitAgmConfirm(int easNo, int originNo, int proxyNo, int pathRankNo, String value) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("easNo", easNo);
		map.put("originNo", originNo);
		map.put("proxyNo", proxyNo);
		map.put("pathRankNo", pathRankNo);
		map.put("value", value);
		
		int result = 0;
		int result0 = 0;
		int result1 = 0;
		int result2 = 0;
		
		result0 = edd.updateEasWaitAsConfirm(sqlSession, map);
		result2 = edd.insertEasWaitAgmConfirmHis(sqlSession, map);
	
		if(result0 > 0) {
			int pathRank = pathRankNo;
			map.put("nextPathRank", pathRank);
			while(true) {
				HashMap<String, Object> next = edd.selectEasPathNextStr(sqlSession, map);
				
				if(next == null) {
					System.out.println("최종결재권자");
					result1 = edd.updateEasDocNo(sqlSession, easNo);
					result1 = edd.updateEasTypeComp(sqlSession, easNo);
					break;
				}else {
					if(next.get("PROC_HOW").equals("REFER")) {
						System.out.println("참조..");
						result1 = edd.updateEasWaitAsProcRefer(sqlSession, map);
						System.out.println(next);
						//result2 = edd.insertEasWaitAsConfirmHisNext(sqlSession, next);
						pathRank++;
						map.put("nextPathRank", pathRank);
						
					}else {
						System.out.println("업데이트");
						result1 = edd.updateEasWaitAsConfirmNext(sqlSession, map);
						//result2 = edd.insertEasWaitAsConfirmHisNext(sqlSession, next);
						
						break;
					}
					
			 	}
				
			}

		}

		
		if(result0 > 0 && result1 > 0 && result2 > 0) {
			result++;
		}
		
		return result;
	}
	
	@Override
	public int updateEasProcCan(int easNo, int originNo, int proxyNo, int pathRankNo) {

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("easNo", easNo);
		map.put("originNo", originNo);
		map.put("proxyNo", proxyNo);
		map.put("pathRankNo", pathRankNo);
		
		int result1 = edd.updateEasProcCan(sqlSession, map);
		int result2 = edd.insertEasProcCanHisNext(sqlSession, map);

		if(result1 > 0) {
			int pathRank = pathRankNo;
			map.put("nextPathRank", pathRank);
			while(true) {
				HashMap<String, Object> next = edd.selectEasPathNextStr(sqlSession, map);
				
				if(next == null) {
					System.out.println("최종결재권자");
					
					break;
				}else {
					if(next.get("PROC_HOW").equals("REFER")) {
						System.out.println("참조..");
						result1 = edd.updateEasProcCanAfter(sqlSession, map);
						System.out.println(next);
						// result2 = edd.insertEasProcCanHisNext(sqlSession, next);
						pathRank++;
						map.put("nextPathRank", pathRank);
						
					}else {
						System.out.println("업데이트");
						result1 = edd.updateEasProcCanAfter(sqlSession, map);
						
						break;
					}
					
			 	}
				
			}

		}
		
		
		int result = 0;
		if(result1 > 0 && result2 > 0) {
			result++;
		}
		return result;
	}
}
