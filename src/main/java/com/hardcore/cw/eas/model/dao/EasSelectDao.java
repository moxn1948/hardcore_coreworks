package com.hardcore.cw.eas.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.Eas;


public interface EasSelectDao {

	List<Object> selectEasList(SqlSessionTemplate sqlSession, Map<String, Object> dataMap);

	ArrayList<Object> selectReceiver(SqlSessionTemplate sqlSession, int empDivNo);

	List<HashMap<String, Object>> selectDoc(SqlSessionTemplate sqlSession, int empDivNo);

	int getListCount(SqlSessionTemplate sqlSession, Map<String, Object> listMap);

	HashMap<String, Object> selectEasDetailForUpdate(SqlSessionTemplate sqlSession, int easNo);

	List<Object> findAbsence(SqlSessionTemplate sqlSession, int empDivNo);

	List<Object> selectProtectPersonList(SqlSessionTemplate sqlSession, int easNo);

	HashMap<String, Object> selectDocCnt(SqlSessionTemplate sqlSession, int eNo);


}
