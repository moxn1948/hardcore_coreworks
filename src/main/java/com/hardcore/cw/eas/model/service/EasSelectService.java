package com.hardcore.cw.eas.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.Eas;

public interface EasSelectService {

	List<Object> selectEasList(Map<String, Object> dataMap);

	ArrayList<Object> selectReceiver(int empDivNo);

	List<HashMap<String, Object>> selectDoc(int empDivNo);

	int getListCount(Map<String, Object> listMap);

	HashMap<String, Object> selectEasDetailForUpdate(int eNo);

	List<Object> findAbsence(int empDivNo);

	List<Object> selectProtectPersonList(int eNo);

	HashMap<String, Object> selectDocCnt(int eNo);



}
