package com.hardcore.cw.eas.model.service;

import java.util.List;

import com.hardcore.cw.eas.model.vo.Eas;
import com.hardcore.cw.eas.model.vo.EasPath;

public interface EasUpdateService {

	int updateEas(Eas upEas);


}
