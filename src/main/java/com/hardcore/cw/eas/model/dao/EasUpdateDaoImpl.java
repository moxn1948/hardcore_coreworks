package com.hardcore.cw.eas.model.dao;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.eas.model.vo.Eas;

@Repository
public class EasUpdateDaoImpl implements EasUpdateDao{

	@Override
	public int updateEas(SqlSessionTemplate sqlSession, Eas upEas) {
		return sqlSession.insert("Eas.updateEas", upEas);
	}

}
