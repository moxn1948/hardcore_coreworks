package com.hardcore.cw.eas.model.dao;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.eas.model.vo.Eas;

public interface EasUpdateDao {

	int updateEas(SqlSessionTemplate sqlSession, Eas upEas);

}
