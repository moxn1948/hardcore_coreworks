package com.hardcore.cw.eas.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.Attachment;

@Repository
public class EasDetailDaoImpl implements EasDetailDao{

	@Override
	public HashMap<String, Object> selectDetEasOne(SqlSessionTemplate sqlSession, int easNo) {
		
		return sqlSession.selectOne("Eas2.selectDetEasOne", easNo);
	}

	@Override
	public HashMap<String, Object> selectDetDocDetOne(SqlSessionTemplate sqlSession, int easNo) {
		
		return sqlSession.selectOne("Eas2.selectDetDocDetOne", easNo);
	}

	@Override
	public List<HashMap<String, Object>> selectDetPubList(SqlSessionTemplate sqlSession, int easNo) {
		
		return sqlSession.selectList("Eas2.selectDetPubList", easNo);
	}

	@Override
	public List<HashMap<String, Object>> selectDetProtList(SqlSessionTemplate sqlSession, int easNo) {
		
		return sqlSession.selectList("Eas2.selectDetProtList", easNo);
	}

	@Override
	public List<HashMap<String, Object>> selectDetPathtList(SqlSessionTemplate sqlSession, int easNo) {
		
		return sqlSession.selectList("Eas2.selectDetPathtList", easNo);
	}

	@Override
	public List<HashMap<String, Object>> selectDetReltList(SqlSessionTemplate sqlSession, int easNo) {
		
		return sqlSession.selectList("Eas2.selectDetReltList", easNo);
	}

	@Override
	public List<HashMap<String, Object>> selectDetAttList(SqlSessionTemplate sqlSession, int easNo) {
		
		return sqlSession.selectList("Eas2.selectDetAttList", easNo);
	}

	@Override
	public List<HashMap<String, Object>> selectDetPathAsList(SqlSessionTemplate sqlSession, int easNo) {

		return sqlSession.selectList("Eas2.selectDetPathAsList", easNo);
	}

	@Override
	public List<HashMap<String, Object>> selectDetPathAgmList(SqlSessionTemplate sqlSession, int easNo) {

		return sqlSession.selectList("Eas2.selectDetPathAgmList", easNo);
	}

	@Override
	public Attachment selectOneAttachment(SqlSessionTemplate sqlSession, int no) {
		
		return sqlSession.selectOne("Eas2.selectOneAttachment", no);
	}

	@Override
	public int updateMyWaitCancel(SqlSessionTemplate sqlSession, int easNo) {
		
		return sqlSession.update("Eas2.updateMyWaitCancel", easNo);
	}

	@Override
	public int updateMyProcRecall(SqlSessionTemplate sqlSession, int easNo) {

		return sqlSession.update("Eas2.updateMyProcRecall", easNo);
	}

	@Override
	public int updateMyReDel(SqlSessionTemplate sqlSession, int easNo) {

		return sqlSession.update("Eas2.updateMyReDel", easNo);
	}

	@Override
	public HashMap<String, Object> selectDetProcHow(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.selectOne("Eas2.selectDetProcHow", map);
	}

	@Override
	public int updateEasWaitAsConfirm(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.update("Eas2.updateEasWaitAsConfirm", map);
	}

	@Override
	public int updateEasWaitAsConfirmNext(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.update("Eas2.updateEasWaitAsConfirmNext", map);
	}

	@Override
	public int insertEasWaitAsConfirmHis(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.insert("Eas2.insertEasWaitAsConfirmHis", map);
	}

	@Override
	public int updateEasTypeComp(SqlSessionTemplate sqlSession, int easNo) {

		return sqlSession.update("Eas2.updateEasTypeComp", easNo);
	}

	@Override
	public int insertEasWaitAsConfirmHisProxy(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.insert("Eas2.insertEasWaitAsConfirmHisProxy", map);
	}

	@Override
	public int updateEasTypeReturn(SqlSessionTemplate sqlSession, int easNo) {

		return sqlSession.update("Eas2.updateEasTypeReturn", easNo);
	}

	@Override
	public HashMap<String, Object> selectEasPathNextStr(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.selectOne("Eas2.selectEasPathNextStr", map);
	}

	@Override
	public int updateEasWaitAsProcRefer(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.update("Eas2.updateEasWaitAsProcRefer", map);
	}

	@Override
	public int insertEasWaitAsConfirmHisNext(SqlSessionTemplate sqlSession, HashMap<String, Object> next) {

		return sqlSession.insert("Eas2.insertEasWaitAsConfirmHisNext", next);
	}

	@Override
	public int updateEasWaitRefer(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.update("Eas2.updateEasWaitRefer", map);
	}

	@Override
	public int insertEasWaitReferHis(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.insert("Eas2.insertEasWaitReferHis", map);
	}

	@Override
	public int insertEasWaitAgmConfirmHis(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.insert("Eas2.insertEasWaitAgmConfirmHis", map);
	}

	@Override
	public int updateEasProcCan(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.update("Eas2.updateEasProcCan", map);
	}

	@Override
	public int updateEasProcCanAfter(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.update("Eas2.updateEasProcCanAfter", map);
	}

	@Override
	public int insertEasProcCanHisNext(SqlSessionTemplate sqlSession, HashMap<String, Object> next) {

		return sqlSession.insert("Eas2.insertEasProcCanHisNext", next);
	}

	@Override
	public int updateEasDocNo(SqlSessionTemplate sqlSession, int easNo) {

		return sqlSession.update("Eas2.updateEasDocNo", easNo);
	}

}
