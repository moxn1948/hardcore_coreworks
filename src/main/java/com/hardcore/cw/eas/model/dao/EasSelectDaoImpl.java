package com.hardcore.cw.eas.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.vo.Eas;

@Repository
public class EasSelectDaoImpl implements EasSelectDao{
	
	@Override
	public int getListCount(SqlSessionTemplate sqlSession, Map<String, Object> listMap) {		
		return sqlSession.selectOne("Eas.getListCount", listMap);
	}

	@Override
	public List<Object> selectEasList(SqlSessionTemplate sqlSession, Map<String, Object> dataMap) {												
		return sqlSession.selectList("Eas.selectEasList", dataMap);
	}

	@Override
	public ArrayList<Object> selectReceiver(SqlSessionTemplate sqlSession, int empDivNo) {		
		return (ArrayList<Object>) sqlSession.selectList("Eas.selectReceiver", empDivNo);
	}

	@Override
	public List<HashMap<String, Object>> selectDoc(SqlSessionTemplate sqlSession, int empDivNo) {
		return sqlSession.selectList("Eas.selectDoc", empDivNo);
	}

	@Override
	public HashMap<String, Object> selectEasDetailForUpdate(SqlSessionTemplate sqlSession, int easNo) {
		return sqlSession.selectOne("Eas.selectEasDetailForUpdate", easNo);
	}

	@Override
	public List<Object> findAbsence(SqlSessionTemplate sqlSession, int empDivNo) {
		return sqlSession.selectList("Eas.findAbsence", empDivNo);
	}

	@Override
	public List<Object> selectProtectPersonList(SqlSessionTemplate sqlSession, int easNo) {
		return sqlSession.selectList("Eas.selectProtectPersonList", easNo);
	}

	@Override
	public HashMap<String, Object> selectDocCnt(SqlSessionTemplate sqlSession, int eNo) {
		return sqlSession.selectOne("Eas.selectDetDocDetOne", eNo);
	}



}
