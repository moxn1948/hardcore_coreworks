package com.hardcore.cw.eas.model.service;

import java.util.HashMap;
import java.util.List;

import com.hardcore.cw.common.model.vo.Attachment;

public interface EasDetailService {

	HashMap<String, Object> selectDetEasOne(int easNo);

	HashMap<String, Object> selectDetDocDetOne(int easNo);

	List<HashMap<String, Object>> selectDetPubList(int easNo);

	List<HashMap<String, Object>> selectDetProtList(int easNo);

	List<HashMap<String, Object>> selectDetPathtList(int easNo);

	List<HashMap<String, Object>> selectDetReltList(int easNo);

	List<HashMap<String, Object>> selectDetAttList(int easNo);

	List<HashMap<String, Object>> selectDetPathAsList(int easNo);

	List<HashMap<String, Object>> selectDetPathAgmList(int easNo);

	Attachment selectOneAttachment(int no);

	int updateMyWaitCancel(int easNo);

	int updateMyProcRecall(int easNo);

	int updateMyReDel(int easNo);

	HashMap<String, Object> selectDetProcHow(int easNo, int originNo);

	int updateEasWaitAsConfirm(int easNo, int originNo, int pathRankNo, String value);

	int updateEasWaitAsConfirmProxy(int easNo, int originNo, int proxyNo, int pathRankNo, String value);

	int updateEasWaitRefer(int easNo, int originNo, int proxyNo);

	int updateEasWaitConfirm(int easNo, int originNo, int pathRankNo, int proxyNo);

	int updateEasWaitAgmConfirm(int easNo, int originNo, int proxyNo, int pathRankNo, String value);

	int updateEasProcCan(int easNo, int originNo, int proxyNo, int pathRankNo);

}
