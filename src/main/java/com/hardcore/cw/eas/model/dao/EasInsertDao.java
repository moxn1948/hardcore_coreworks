package com.hardcore.cw.eas.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.DocPath;
import com.hardcore.cw.eas.model.vo.Eas;
import com.hardcore.cw.eas.model.vo.EasPath;
import com.hardcore.cw.eas.model.vo.Publisher;
import com.hardcore.cw.eas.model.vo.RelDoc;
import com.hardcore.cw.eas.model.vo.VersionHistory;
import com.hardcore.cw.mypage.model.vo.Absence;

public interface EasInsertDao {

	Format selectFormat(SqlSessionTemplate sqlSession, String formatNo);

	int insertNewEas(SqlSessionTemplate sqlSession, Object object);

	String findDeptName(SqlSessionTemplate sqlSession, int deptNo);

	int insertAttachment(SqlSessionTemplate sqlSession, Object object);

	List<Format> selectFormatList(SqlSessionTemplate sqlSession);

	List<DocPath> selectDocPath(SqlSessionTemplate sqlSession);

	Format selectOneFormat(SqlSessionTemplate sqlSession, int formatNo);

	List<HashMap<String, Object>> selectProtectEmpList(SqlSessionTemplate sqlSession, int drafterNo, String condition);

	int insertProtectDetail(SqlSessionTemplate sqlSession, int easNo, HashMap<String, Object> hashMap);

	HashMap<String, Object> selectRelDocList(SqlSessionTemplate sqlSession, Integer integer);

	int insertVersion(SqlSessionTemplate sqlSession, Object object);

	int insertRelDoc(SqlSessionTemplate sqlSession, Object object);

	int insertEasPath(SqlSessionTemplate sqlSession, Object object);

	int insertPublisher(SqlSessionTemplate sqlSession, List<Publisher> pList);

	int insertDocDetail(SqlSessionTemplate sqlSession, Object object);

	Absence selectOneAbsence(SqlSessionTemplate sqlSession, String empDivNo);

}
