package com.hardcore.cw.eas.model.vo;

import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Eas implements java.io.Serializable {
   private int easNo;
   private String limitYn;
   private String protectYn;
   private String emergencyYn;
   private int formatNo;
   private int empDivNo;
   private Date easSdate;
   private int docNo;
   private String tempYn;
   private String easPeriod;
   private int easDept;
   private String senderName;
   private int receiverName;
   private String type;
   private String mateYn;
   private String deptYn;
   private String seniorYn;
   private String status;
}