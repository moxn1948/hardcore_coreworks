package com.hardcore.cw.eas.model.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.eas.model.dao.EasInsertDao;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.DocPath;
import com.hardcore.cw.eas.model.vo.Eas;
import com.hardcore.cw.eas.model.vo.EasPath;
import com.hardcore.cw.eas.model.vo.Publisher;
import com.hardcore.cw.eas.model.vo.RelDoc;
import com.hardcore.cw.eas.model.vo.VersionHistory;
import com.hardcore.cw.mypage.model.vo.Absence;

@Service
public class EasInsertServiceImpl implements EasInsertService{
	@Autowired
	private EasInsertDao eid;
	@Autowired
	private SqlSessionTemplate sqlSession;

	@Override
	public Format selectFormat(String formatNo) {
		Format format = eid.selectFormat(sqlSession, formatNo);
		
		return format;
	}
	
	@Override 
	public int insertNewEas(Eas eas) { 
		int result = eid.insertNewEas(sqlSession, eas);
		return result; 
	}
	 

	@Override
	public String findDeptName(int deptNo) {
		String deptName = eid.findDeptName(sqlSession, deptNo);
		
		return deptName;
	}

	@Override
	public int insertAttachment(ArrayList<Object> attachList) {
		int result = 0;
		
		for(int i=0; i<attachList.size(); i++) {			
			result += eid.insertAttachment(sqlSession, attachList.get(i));		
		}
		
		return result;
	}
	
	@Override
	public ArrayList<HashMap<String, Object>> selectDocPath() {	
		List<DocPath> docPathList = eid.selectDocPath(sqlSession);
		
		ArrayList<HashMap<String, Object>> docPathListEdit = new ArrayList<>();
		
		for(int i=0; i<docPathList.size(); i++) {
			
			if(docPathList.get(i).getPathOrder() == 0) {
				HashMap<String, Object> cntMap = new HashMap<>();
				ArrayList<HashMap<String, Object>> pathOrderList = new ArrayList<>();
				
				cntMap.put("pathNo", docPathList.get(i).getPathNo());
				cntMap.put("pathName", docPathList.get(i).getPathName());
				cntMap.put("pathOrderList", pathOrderList);
				
				docPathListEdit.add(cntMap);
			}		
		}
		
		System.out.println("service docPathListEdit 1: " + docPathListEdit);
		
		for(int i=0; i<docPathListEdit.size(); i++) {
			for(int j=0; j<docPathList.size(); j++) {
				if(docPathList.get(j).getPathOrder() == (Integer) docPathListEdit.get(i).get("pathNo")) {
					HashMap<String, Object> cntMap = new HashMap<>();
					ArrayList<HashMap<String, Object>> pathOrderList = new ArrayList<>();
					
					cntMap.put("pathNo", docPathList.get(j).getPathNo());
					cntMap.put("pathName", docPathList.get(j).getPathName());
					cntMap.put("pathOrderList", pathOrderList);
					
					((ArrayList<HashMap<String, Object>>) docPathListEdit.get(i).get("pathOrderList")).add(cntMap);
				}
			}
		}
		
		System.out.println("service docPathListEdit 2: " + docPathListEdit);
		
		return docPathListEdit;
	}

	@Override
	public List<Format> selectFormatList() {
		List<Format> list = eid.selectFormatList(sqlSession);
		
		return list;
	}

	@Override
	public Format selectOneFormat(int formatNo) {
		return eid.selectOneFormat(sqlSession, formatNo);
	}

	@Override
	public List<HashMap<String, Object>> selectProtectEmpList(int drafterNo, String condition) {
		return eid.selectProtectEmpList(sqlSession, drafterNo, condition);
	}	
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, isolation=Isolation.SERIALIZABLE)
	public int insertProtectDetail(int easNo, List<HashMap<String, Object>> list) {
		
		try {
			sqlSession.getConnection().setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Service list : " + list);
		int result = 0;
		int cnt = 0;

		for(int i=0; i<list.size(); i++) {
			int tempResult = eid.insertProtectDetail(sqlSession, easNo, list.get(i));
			if(tempResult > 0) {
				cnt++;
			}
		}
		
		System.out.println("cnt : " + cnt);
		System.out.println("listSize : " + list.size());
		if(cnt == list.size()) {
			result = 1;
		}else {
			result = 0;
		}
		
		return result;
	}

	@Override
	public ArrayList<HashMap<String, Object>> selectRelDocList(ArrayList<Integer> easNo) {
		ArrayList<HashMap<String, Object>> list = new ArrayList<>();
		for(int i=0; i<easNo.size(); i++) {
			list.add(eid.selectRelDocList(sqlSession, easNo.get(i)));
		}
		return list;
	}
	
	@Override
	public int insertRelDoc(List<RelDoc> relDocList) {		
		return eid.insertRelDoc(sqlSession, relDocList);
	}

	@Override
	public int insertVersion(VersionHistory ver) {
		return eid.insertVersion(sqlSession, ver);
	}

	@Override
	public int insertEasPath(List<EasPath> easPathList) {
		return eid.insertEasPath(sqlSession, easPathList);
	}

	@Override
	public int insertPublisher(List<Publisher> pList) {
		return eid.insertPublisher(sqlSession, pList);
	}

	@Override
	public int insertDocDetail(DocDetail dd) {
		return eid.insertDocDetail(sqlSession, dd);
	}

	@Override
	public Absence selectOneAbsence(String empDivNo) {
		return eid.selectOneAbsence(sqlSession, empDivNo);
	}




}