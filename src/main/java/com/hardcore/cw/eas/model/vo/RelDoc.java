package com.hardcore.cw.eas.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RelDoc implements java.io.Serializable{
	private int relDocNo;
	private int versionNo;
	private int easNo;
}
