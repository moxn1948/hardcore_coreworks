package com.hardcore.cw.eas.model.dao;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;

import com.hardcore.cw.common.model.vo.Attachment;

public interface EasDetailDao {

	HashMap<String, Object> selectDetEasOne(SqlSessionTemplate sqlSession, int easNo);

	HashMap<String, Object> selectDetDocDetOne(SqlSessionTemplate sqlSession, int easNo);

	List<HashMap<String, Object>> selectDetPubList(SqlSessionTemplate sqlSession, int easNo);

	List<HashMap<String, Object>> selectDetProtList(SqlSessionTemplate sqlSession, int easNo);

	List<HashMap<String, Object>> selectDetPathtList(SqlSessionTemplate sqlSession, int easNo);

	List<HashMap<String, Object>> selectDetReltList(SqlSessionTemplate sqlSession, int easNo);

	List<HashMap<String, Object>> selectDetAttList(SqlSessionTemplate sqlSession, int easNo);

	List<HashMap<String, Object>> selectDetPathAsList(SqlSessionTemplate sqlSession, int easNo);

	List<HashMap<String, Object>> selectDetPathAgmList(SqlSessionTemplate sqlSession, int easNo);

	Attachment selectOneAttachment(SqlSessionTemplate sqlSession, int no);

	int updateMyWaitCancel(SqlSessionTemplate sqlSession, int easNo);

	int updateMyProcRecall(SqlSessionTemplate sqlSession, int easNo);

	int updateMyReDel(SqlSessionTemplate sqlSession, int easNo);

	HashMap<String, Object> selectDetProcHow(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int updateEasWaitAsConfirm(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int updateEasWaitAsConfirmNext(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int insertEasWaitAsConfirmHis(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int updateEasTypeComp(SqlSessionTemplate sqlSession, int easNo);

	int insertEasWaitAsConfirmHisProxy(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int updateEasTypeReturn(SqlSessionTemplate sqlSession, int easNo);

	HashMap<String, Object> selectEasPathNextStr(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int updateEasWaitAsProcRefer(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int insertEasWaitAsConfirmHisNext(SqlSessionTemplate sqlSession, HashMap<String, Object> next);

	int updateEasWaitRefer(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int insertEasWaitReferHis(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int insertEasWaitAgmConfirmHis(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int updateEasProcCan(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int updateEasProcCanAfter(SqlSessionTemplate sqlSession, HashMap<String, Object> map);

	int insertEasProcCanHisNext(SqlSessionTemplate sqlSession, HashMap<String, Object> next);

	int updateEasDocNo(SqlSessionTemplate sqlSession, int easNo);

}
