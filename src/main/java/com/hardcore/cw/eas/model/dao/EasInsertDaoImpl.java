package com.hardcore.cw.eas.model.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.cms.model.vo.Format;
import com.hardcore.cw.common.model.vo.Employee;
import com.hardcore.cw.eas.model.vo.DocDetail;
import com.hardcore.cw.eas.model.vo.DocPath;
import com.hardcore.cw.eas.model.vo.Eas;
import com.hardcore.cw.eas.model.vo.EasPath;
import com.hardcore.cw.eas.model.vo.Publisher;
import com.hardcore.cw.eas.model.vo.RelDoc;
import com.hardcore.cw.eas.model.vo.VersionHistory;
import com.hardcore.cw.mypage.model.vo.Absence;

@Repository
public class EasInsertDaoImpl implements EasInsertDao{

	@Override
	public Format selectFormat(SqlSessionTemplate sqlSession, String formatNo) {
		return sqlSession.selectOne("Eas.selectFormat", formatNo);
	}

	@Override
	public int insertNewEas(SqlSessionTemplate sqlSession, Object newEas) {
		return sqlSession.insert("Eas.insertNewEas", newEas);
	}

	@Override
	public String findDeptName(SqlSessionTemplate sqlSession, int deptNo) {
		return sqlSession.selectOne("Eas.findDeptName", deptNo);
	}

	@Override
	public int insertAttachment(SqlSessionTemplate sqlSession, Object attachList) {
		return sqlSession.insert("Eas.insertAttachment", attachList);
	}
	
	@Override
	public List<DocPath> selectDocPath(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Eas.selectDocPath");
	}

	@Override
	public List<Format> selectFormatList(SqlSessionTemplate sqlSession) {
		return sqlSession.selectList("Eas.selectFormatList");
	}

	@Override
	public Format selectOneFormat(SqlSessionTemplate sqlSession, int formatNo) {
		return sqlSession.selectOne("Eas.selectOneFormat", formatNo);
	}

	@Override
	public List<HashMap<String, Object>> selectProtectEmpList(SqlSessionTemplate sqlSession, int drafterNo, String condition) {
		HashMap<String, Object> hmap = new HashMap<>();
		hmap.put("drafterNo", drafterNo);
		hmap.put("condition", condition);
		
		return sqlSession.selectList("Eas.selectProtectEmpList", hmap);
	}

	@Override
	public int insertProtectDetail(SqlSessionTemplate sqlSession, int easNo, HashMap<String, Object> hmap) {
		hmap.put("easNo", easNo);
		return sqlSession.insert("Eas.insertProtectDetail", hmap);
	}

	@Override
	public HashMap<String, Object> selectRelDocList(SqlSessionTemplate sqlSession, Integer easNo) {
		return (HashMap<String, Object>) sqlSession.selectOne("Eas.selectRelDocList", easNo);
	}

	@Override
	public int insertRelDoc(SqlSessionTemplate sqlSession, Object relDocList) {
		return sqlSession.insert("Eas.insertRelDoc", relDocList);
	}
	
	@Override
	public int insertVersion(SqlSessionTemplate sqlSession, Object ver) {
		return sqlSession.insert("Eas.insertVersion", ver);
	}

	@Override
	public int insertEasPath(SqlSessionTemplate sqlSession, Object easPath) {
		return sqlSession.insert("Eas.insertEasPath", easPath);
	}

	@Override
	public int insertPublisher(SqlSessionTemplate sqlSession, List<Publisher> pList) {
		return sqlSession.insert("Eas.insertPublisher", pList);
	}

	@Override
	public int insertDocDetail(SqlSessionTemplate sqlSession, Object docDetail) {
		return sqlSession.insert("Eas.insertDocDetail", docDetail);
	}

	@Override
	public Absence selectOneAbsence(SqlSessionTemplate sqlSession, String empDivNo) {
		return sqlSession.selectOne("Eas.selectOneAbsence", empDivNo);
	}



	


}
