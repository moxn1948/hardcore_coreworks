package com.hardcore.cw.eas.model.service;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.eas.model.dao.EasUpdateDao;
import com.hardcore.cw.eas.model.vo.Eas;

@Service
public class EasUpdateServiceImpl implements EasUpdateService {
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private EasUpdateDao eud;
	
	@Override
	public int updateEas(Eas upEas) {
		return eud.updateEas(sqlSession, upEas);
	}

}