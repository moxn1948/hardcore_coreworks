package com.hardcore.cw.eas.model.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import com.hardcore.cw.common.model.vo.PageInfo;

@Repository
public class DocumentDaoImpl implements DocumentDao{

	@Override
	public int selectDocListCount(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.selectOne("Eas2.selectDocListCount", map);
	}

	@Override
	public List<HashMap<String, Object>> selectDocList(SqlSessionTemplate sqlSession, PageInfo pi,
			HashMap<String, Object> map) {
		int offset = (pi.getCurrentPage() - 1) * pi.getLimit();
		RowBounds rowBounds = new RowBounds(offset, pi.getLimit());

		return sqlSession.selectList("Eas2.selectDocList", map, rowBounds);
	}

	@Override
	public int selectLimitYn(SqlSessionTemplate sqlSession, HashMap<String, Object> map) {

		return sqlSession.selectOne("Eas2.selectLimitYn", map);
	}

}
