package com.hardcore.cw.eas.model.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DocDetail implements java.io.Serializable{
   private String docCnt;
   private String docTit;
   private int versionNo;
   private int easNo;
}