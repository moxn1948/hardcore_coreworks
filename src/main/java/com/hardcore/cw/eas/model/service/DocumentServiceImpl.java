package com.hardcore.cw.eas.model.service;

import java.util.HashMap;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.dao.DocumentDao;

@Service
public class DocumentServiceImpl implements DocumentService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private DocumentDao dd;
	@Override
	public int selectDocListCount(HashMap<String, Object> map) {
		
		return dd.selectDocListCount(sqlSession, map);
	}
	@Override
	public List<HashMap<String, Object>> selectDocList(PageInfo pi, HashMap<String, Object> map) {

		return dd.selectDocList(sqlSession, pi, map);
	}
	@Override
	public int selectLimitYn(HashMap<String, Object> map) {

		return dd.selectLimitYn(sqlSession, map);
	}

}
