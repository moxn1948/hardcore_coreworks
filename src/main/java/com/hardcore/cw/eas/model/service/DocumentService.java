package com.hardcore.cw.eas.model.service;

import java.util.HashMap;
import java.util.List;

import com.hardcore.cw.common.model.vo.PageInfo;

public interface DocumentService {

	int selectDocListCount(HashMap<String, Object> map);

	List<HashMap<String, Object>> selectDocList(PageInfo pi, HashMap<String, Object> map);

	int selectLimitYn(HashMap<String, Object> map);

}
