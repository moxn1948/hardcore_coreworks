package com.hardcore.cw.eas.model.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hardcore.cw.common.model.vo.PageInfo;
import com.hardcore.cw.eas.model.dao.EasSelectDao;
import com.hardcore.cw.eas.model.vo.Eas;

@Service
public class EasSelectServiceImpl implements EasSelectService{
	@Autowired
	private SqlSessionTemplate sqlSession;
	@Autowired
	private EasSelectDao esd;	
	
	@Override
	public int getListCount(Map<String, Object> listMap) {
		return esd.getListCount(sqlSession, listMap);
	}
	
	@Override
	public List<Object> selectEasList(Map<String, Object> dataMap) {
		return esd.selectEasList(sqlSession, dataMap);
	}

	@Override
	public ArrayList<Object> selectReceiver(int empDivNo) {		
		return esd.selectReceiver(sqlSession, empDivNo);
	}

	@Override
	public List<HashMap<String, Object>> selectDoc(int empDivNo) {		
		return esd.selectDoc(sqlSession, empDivNo);
	}

	@Override
	public HashMap<String, Object> selectEasDetailForUpdate(int easNo) {
		return esd.selectEasDetailForUpdate(sqlSession, easNo);
	}

	@Override
	public List<Object> findAbsence(int empDivNo) {
		return esd.findAbsence(sqlSession, empDivNo);
	}

	@Override
	public List<Object> selectProtectPersonList(int easNo) {
		return esd.selectProtectPersonList(sqlSession, easNo);
	}

	@Override
	public HashMap<String, Object> selectDocCnt(int eNo) {
		return esd.selectDocCnt(sqlSession, eNo);
	}






}
